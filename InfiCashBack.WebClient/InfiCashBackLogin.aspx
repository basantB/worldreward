﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InfiCashBackLogin.aspx.cs" Inherits="InfiCashBackLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <link href="css/cb-login-register.css" type="text/css" rel="stylesheet" />
    <link href="css/slicknav.css" type="text/css" rel="stylesheet" />


    <script src='http://codepen.io/assets/libs/fullpage/jquery.js'></script>
    <script src="js/jquery.slicknav.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#top').slicknav({
                prependTo: '#home',
            });
        });
    
    
        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        };

        function LoginOnEnter(e) {
            var ENTER_KEY = 13;
            var code = "";
            var code = (e.keyCode ? e.keyCode : e.which);

            if (code == ENTER_KEY) {
                LoginValidation();
                return false;
            }
            return true;
        };


        function redirectLocation(url) {
            window.location.href = url;
            return false;
        };

        function LoginValidation() {
            var msg = "";
            $("#ErrorMsgContainer").hide();
            $("#LoginValidation")[0].innerHTML = "";
            $("#lblLoginError").html('');
            var vdf = $("#txtUserName").val();

            if ($("#txtUserName").val().length == 0) {
                msg += "Please enter UserName.<br/>";
            }
            if ($("#txtPassword").val().length == 0) {
                msg += "Please enter password.<br/>";
            }

            if (($('#txtPassword').val().length < 8) && ($('#txtPassword').val().length > 1)) {
                msg += "Password length has to be minimum eight characters.";
            }

            if (msg.length > 0) {
                $("#LoginValidation").css('color', 'red');
                $("#ErrorMsgContainer").show();
                $("#LoginValidation")[0].innerHTML = msg;
            }

            else {
                var MemberId = $("#txtUserName").val();
                var password = $("#txtPassword").val();
                var RememberMe = new Boolean();
                RememberMe = false;//$("#CP_chkRememberMe")[0].checked;
                $.ajax({
                    url: 'InfiCashBackLogin.aspx/SigninUser',
                    type: 'POST',  // or get
                    contentType: 'application/json; charset =utf-8',
                    data: "{'pstrMemberid':'" + MemberId.toString() + "'" + "," + "'pstrPassword':'" + password.toString() + "'" + "," + "'pboolRememberMe':'" + RememberMe + "'}",
                    dataType: 'json',
                    success: function (data) {
                        try {

                            var newData = data.d;
                           
                            if ((newData != "") || newData == null) {
                                if (data.d == "AccountLock") {
                                    $("#LoginValidation")[0].innerHTML = "";
                                    $("#ErrorMsgContainer").show();
                                    $("#lblLoginError").html('Your Account is Locked.Please <a class="ErrorMessageLink" onclick="showOTPDiv();">click here</a> to unlock the same.');
                                }
                                else if (data.d == "AuthenticationFailed") {
                                    $("#LoginValidation").css('color', 'red');
                                    $("#LoginValidation")[0].innerHTML = "";
                                    $("#ErrorMsgContainer").show();
                                    $("#lblLoginError").html('Please check the login details you have entered and try again.');
                                }
                                else if (data.d == "IncorrectFormat") {
                                    $("#LoginValidation")[0].innerHTML = "";
                                    $("#ErrorMsgContainer").show();
                                    $("#lblLoginError").html('Password must contain one non-alpha character,one upper case character,one lower case character.');
                                }
                                else {
                                    $("#LoginValidation")[0].innerHTML = "";
                                    window.location.href = data.d;
                                }
                            }
                            else {
                                $("#LoginValidation")[0].innerHTML = "";
                                $("#ErrorMsgContainer").show();
                                $("#lblLoginError").html('Please check the login details you have entered and try again.');
                            }
                        }
                        catch (e) {
                            alert(e);
                            return false;
                        }
                        return false;
                    },
                    error: function (errmsg) {
                        alert(errmsg);
                    }
                });
            }
            return false;
        };
        </script>
</head>

    <body>
<form runat="server">
    <div>

        <header>
            <div class="hdr">
                <div class="chbk-logo">
                    <img src="images/chbk-logo.jpg" />
                </div>

                <div class="hdrlnks">
                    <ul>
                        <li><a href="#">Login</a></li>
                        <li class="last"><a href="#">Register</a></li>
                        <li class="last">
                            <select></select></li>
                    </ul>
                    <div class="clr"></div>
                    <!--Menu Starts-->
                    <nav id="home">
                       <!-- <div class="slicknav_menu">
                            <a class="slicknav_btn slicknav_collapsed" tabindex="0" aria-haspopup="true" href="#" style=""><span class="slicknav_menutxt">MENU</span><span class="slicknav_icon"><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span></span></a><ul class="slicknav_nav slicknav_hidden" style="display: none;" aria-hidden="true" role="menu">
                                <li><a href="#" role="menuitem" tabindex="-1">Collect Miles</a></li>
                                <li><a href="#" role="menuitem" tabindex="-1">Spend Miles</a></li>
                                <li><a href="#" role="menuitem" tabindex="-1">Bonus Offers</a></li>
                                <li class="last"><a href="#" role="menuitem" tabindex="-1">Miles Calculator</a></li>
                                <li class="select">
                                    <select>
                                        <option>India</option>
                                        <option>Dubai</option>
                                        <option>Bahrain</option>
                                        <option>Srilanka</option>
                                    </select>
                                </li>
                            </ul>
                        </div>-->
                        <div class="menu">
                            <ul id="top">
                                <li><a href="#">Collect Miles</a></li>
                                <li><a href="#">Spend Miles</a></li>
                                <li><a href="#">Bonus Offers</a></li>
                                <li class="last"><a href="#">Miles Calculator</a></li>
                                 <div class="clr"></div>

                            </ul>
                             <div class="clr"></div>
                        </div>
                        <div class="clr"></div>
                    </nav>
                    <!--Menu Ends-->
                </div>
                <div class="clr"></div>
            </div>
        </header>

        <div class="cbLR-pgwrap">

            <!--  Starts-->
            


            <div class="cbLR-row">
                <h1 class="cbLR-h1">Login</h1>
                <div class="cbLR-inp">
                    <div class="cbLR-title">Username</div>
                    <%--<asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>--%>
                    <input type="text" class="robot" id="txtUserName" runat="server" />
                </div>
                <div class="cbLR-inp">
                    <div class="cbLR-title">Password</div>
                     <%--<asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>--%>
                    <input type="password" runat="server" class="robot" id="txtPassword" />
                    <div class="fgps"><a href="#">Forgot password ?</a></div>
                </div>
                <div class="clr"></div>
            </div>

            <!--  Ends-->
            <div>
                <div class="cbLR-remme">
                    <%--<input type="checkbox" runat="server" id="chkRemember" />--%>
                    <asp:CheckBox runat="server" ID="chkRemember" />
                    <a href="#">Remember me</a>
                </div>
                
                <%--<input type="button" class="cbLR-btn" value="Continue" onclick="var retvalue = LoginValidation(); event.returnValue = retvalue; return retvalue;" />--%>
                 <asp:Button CssClass="cbLR-btn"  Text="Continue" ID="btnLogin" runat="server" OnClick="btnLogin_Click" />
            </div>
            <div id="ErrorMsgContainer" class=" ErrorMsgContainer mtop_10" runat="server">
                                    <asp:Label runat="server" ID="lblLoginError" Text="" ForeColor="Red"></asp:Label>
                                    <div id="LoginValidation">
                                    </div>
                                </div>



        </div>

        <footer>
            <div class="footer">
                <div class="cbLR-pgwrap">
                    <div class="copyryts">
                        &copy; World<span class="redcol">RewardZ,</span> All Rights Reserved. 2015-2016.
                    </div>
                    <div>
                        <ul class="foot">

                            <li><a href="#" target="_blank">About Us</a></li>
                            <li><a href="#" target="_blank">FAQs</a></li>
                            <li><a href="#" target="_blank">Terms & Conditions</a></li>
                            <li class="last"><a class="last" href="#">Contact Us</a></li>
                        </ul>
                        <div class="clr"></div>
                    </div>
                </div>
            </div>
        </footer>


    </div>
    </form>
</body>

</html>
