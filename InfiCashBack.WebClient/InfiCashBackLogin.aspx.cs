﻿using Core.Platform.Member.Entites;
using Framework.EnterpriseLibrary.Adapters;
using InfiPlanetModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.ProgramMaster.Entities;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.SessionState;
using Core.Platform.MemberActivity.Entities;
using Core.Platform.MemberActivity.Constants;
using System.Reflection;
using JWT;
using System.Configuration;

public partial class InfiCashBackLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string SigninUser(string pstrMemberid, string pstrPassword, bool pboolRememberMe)
    {
        InfiModel lobjInfiModel = new InfiModel();
        MemberDetails lobjMemberDetails = null;
        string mstrRedirectEmptyURL = string.Empty;
        try
        {
            string lstrEncryptPassword = string.Empty;
            string lstrDecryptPassword = string.Empty;
            //ProgramMaster lobjProgramMaster = lobjCBQModel.GetProgramDetails(lstrProgramName);
            //ProgramMaster lobjProgramMaster = HttpContext.Current.Application["ProgramMaster"] as ProgramMaster;

            MemberDetails lobjEnrollMemberDetails = new MemberDetails();
            SearchMember lobjSearchMember = new SearchMember();
            //lobjSearchMember.RelationReference = pstrMemberid;
            // lobjSearchMember.RelationType = Convert.ToInt32(RelationType.Login);
            lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
            lobjSearchMember.SearchColumnName = "Created_By";
            lobjSearchMember.SearchColumnValue = pstrMemberid;
            LoggingAdapter.WriteLog("Search Details :- User Id:" + pstrMemberid + " Relation Type:" + lobjSearchMember.RelationType);
            // lobjEnrollMemberDetails = lobjInfiModel.GetEnrollMemberDetails(lobjSearchMember);
            lobjEnrollMemberDetails = lobjInfiModel.GetEnrollMemberDetailsByPrimaryReference(lobjSearchMember);


            if (lobjEnrollMemberDetails != null)
            {
                ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramDetailsByID(lobjEnrollMemberDetails.ProgramId);

                if (lobjProgramDefinition != null)
                {
                    SystemParameter lobjPasswordPolicy = lobjInfiModel.GetSystemParametres(lobjProgramDefinition.ProgramId);
                    if (lobjPasswordPolicy != null)
                    {
                        Regex regex = new Regex((lobjPasswordPolicy.PasswordPolicy));
                        if (regex.Match(pstrPassword).Success)
                        {
                            lstrEncryptPassword = lobjInfiModel.EncryptPassword(pstrPassword);
                            string lstrRelationRef = lobjEnrollMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                            lobjMemberDetails = lobjInfiModel.CheckLoginRelationMembershipCrediantials(lstrRelationRef, lstrEncryptPassword, lobjProgramDefinition.ProgramId);
                            if (lobjMemberDetails != null)
                            {
                                HttpContext.Current.Session["ProgramMaster"] = lobjProgramDefinition;
                                List<ProgramCurrencyDefinition> lobjListOfProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
                                lobjListOfProgramCurrencyDefinition = lobjInfiModel.GetProgramCurrencyDefinition(lobjProgramDefinition.ProgramId);
                                HttpContext.Current.Session["ProgramCurrency"] = lobjListOfProgramCurrencyDefinition;
                                string lstrCurrency = lobjListOfProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals("REWARDZ")).Currency;
                                HttpContext.Current.Session["DefaultCurrency"] = lstrCurrency;




                                updateSessionId(HttpContext.Current);
                                if (pboolRememberMe)
                                {
                                    HttpCookie cookie = new HttpCookie("InfiLogin");
                                    cookie.Values.Add("MembershipRef", pstrMemberid);
                                    cookie.Expires = DateTime.Now.AddDays(15);
                                    HttpContext.Current.Response.Cookies.Add(cookie);
                                }
                                Core.Platform.MemberActivity.Entities.MemberActivitySession lobjMemberActivitySession = HttpContext.Current.Session["MemberActivitySession"] as Core.Platform.MemberActivity.Entities.MemberActivitySession;
                                lobjMemberActivitySession.ReferenceNumber = pstrMemberid;
                                lobjMemberActivitySession.ActivityType = ActivityType.loginsuccessful;
                                bool isMembershipUpdated = lobjInfiModel.UpdateMemberShipActivitySession(lobjMemberActivitySession);
                                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "successful"), ActivityType.loginsuccessful);

                                HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                if (HttpContext.Current.Session["ShopSelected"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = "InfiShop.aspx?id=" + lobjMemberDetails.MemberRelationsList[0].RelationReference;
                                }
                                else if (HttpContext.Current.Session["SelectedItinerary"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = "FlightPassenger.aspx";
                                }
                                else if (HttpContext.Current.Session["HotelSelected"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = HttpContext.Current.Session["HotelSelected"].ToString();
                                }
                                else if (HttpContext.Current.Session["CarRefId"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = HttpContext.Current.Session["CarRefId"].ToString();
                                    mstrRedirectEmptyURL = "CarDetails.aspx?id=" + ((MemberDetails)HttpContext.Current.Session["MemberDetails"]).MemberRelationsList[0].RelationReference;
                                }

                                else if (HttpContext.Current.Session["Insurance"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = "Insurance.aspx";
                                }


                                else if (HttpContext.Current.Session["MemberDetails"] != null)
                                {
                                    //if (lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.Login)).ForceChangePassword)
                                    if (lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).ForceChangePassword)
                                    {
                                        HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                        mstrRedirectEmptyURL = "profile.aspx";
                                    }
                                    else if (lobjMemberDetails.MemberRelationsList[0].LoginAttempt >= 5)
                                    {
                                        mstrRedirectEmptyURL = "AccountLock";
                                    }
                                    else if (HttpContext.Current.Session["CallbackUrl"] != null)
                                    {
                                        mstrRedirectEmptyURL = Convert.ToString(HttpContext.Current.Session["CallbackUrl"]);
                                    }
                                    else
                                    {
                                       // mstrRedirectEmptyURL = "StatementSummary.aspx";
                                        LoggingAdapter.WriteLog("JWT Session Start:");
                                        Customer lobj = new Customer(lobjMemberDetails.Email, lobjMemberDetails.FirstName, lobjMemberDetails.MemberRelationsList[0].RelationReference, lobjMemberDetails.LastName);
                                        LoggingAdapter.WriteLog("JWT Customer:" + lobjMemberDetails.Email + "," + lobjMemberDetails.FirstName + "," + lobjMemberDetails.MemberRelationsList[0].RelationReference + "," + lobjMemberDetails.LastName);

                                        jwtClass lobjjwtClass = new jwtClass(lobj);

                                        string key = "MIIJKAIBAAKCAgEApLXSkLKQY2t1GJf4A2ZHKSZ83eOK683AZfpyDElARsoyRURaaeL9U2ZobUqfojp5DDXIVTmOokMSj8SpwtqgXqalqSFRb9eRYEGATn/x5WvCwd8D6BDk7KZre9XxkbOUkfv7rT4PsxZwfQsAolNhBImm9lFWMIfdogjj5VT4mtx0yyfR92YHJgqIWrIDXl0YfMdnEmKLSuJ36fAVL+synCw0LA3EFja90ykyEGz8AgxGH/GmurwmwL4WhoK9ABT6s+tk+rDlyFwWuTreqRrO38sxj1WkoLxcuQhMZmK7as4czoJPoegDDxWBtFtn2s4h7busX6hQ8BW+u1fgzv1kd3Fl1Bqo2w9qF1lRry0zXvOkJO6k3Q7MhwojDUUrts62ikjPU7wTsYzodL3E5CpmSOBGylmhro09xtA9CFEuuCdFt1gwoDvJPOddnpC3nipdBaiJsait/PgxfBybh0bNko0BDSzqE07fc/ZOCciUC7x93P6w50N4bj4YmD06BVx6ufS3wMWDKLTDg2b6wIotejTvxoxKsKjbKfy7GFu6pNbohM5RMwkEnAJ2NFJ+n/+9Z+zmf5kM3irSTMgYjmPsa3WCgVFHR8V3Zz1ZpZ8rZ46sHlnDaA8B+/FQuJxPIJ5FVLvCK0dhfJFfmuWQIOU/3YO7ppWxvRkcqTIbsSEVUO8CAwEAAQKCAgAm9nf0ELmFOzM8+R4LN9OXkWoyVxRSyZ48wWzfKSjSVhxkNFIrAC4q32vOhruSdcCSudpFBtON4M3wQnZiCLOtEEDZ7G7GRTk3iuceKrf/LGmq0OWsrOpqbMe6SJbXf6KikfgsOCUgmStM+SBSReUKXMGpPP5lho5QOUgnxNz2TTY3pJjPrEv+nLk6syeG2ADl64mfWL5YnY/+P/PTHREgzngc/7imBJcM747PQeivV1XzUoe9XqrQJ2dOtHifH1da3lFJ2DTTG62obZukZxoO6JM1s+24tDY+8w6Xnb11+4Hm+/54b1jh5ZWEI6dCpEZ0ZVPWYQucb5hp2tTsQhbUpTcALFvvr91t/5P3Vh1jQxMtGcZwbU4jLSJ08F6M5Ke2KuiNnb3jHfXowCQYlXho5w+nvewR2lKuy76mcOsWuIMnqjxXHXPOaE74RcmOCRtN+zBtIpxlowxMAIXm3cnG8IfYuvRMPTqfMKOHe1p4cylsUPgvIclc65Du0Gdc4wN+zNo4yWLKe6KHseHXsgQgj6Re2bSKU23/bgLpGGXFUOg99tvRuqpz4LVnAILmfgOjczWwfVvKlDgOcHdrisua0gcUZ7sNoqWiGQVj4m/nrdZxBBYvX9eR/ZVqNMCyOj9H82IlsJqAjgEgXlaspE3RKMvVQ7ddZUE/gaoBNn2nyQKCAQEA1vHmiqhbIjKptieanjSn6Hei3Z20Oeuq6vQXCZm8SE69Ms1sn6TlPmRrW1erHVsX9te1nfNNDX8lyysYSTHSt7RmmnJo0uMq3euzSrtmZ2zi+0EXM5eoYTP6Ni3jRyHTV0OS1c1Q8Gt2UWjakRGVBcHFoxwrWAFN3wpP2iiU9RgXNEvtWIVFO1IG2KHPw6m5tYwXxFY1jFkxRSNznZ60LdB0N/UXL5nnCH/7n3Y3IBSSz+r5jwsKcpjh0QjbXqrDitTmNvJXxAtTqnBvomRAvvlVWJjRRnRQuJEwgcOGbGi88auGGlt6geGwEF5A5Dw+UV4swGwEIdY/i0pgROR/LQKCAQEAxCuchxEPuXrFMFWCv39+P38A+B4XtjzP+vXK+MA14rgjs9vyiHwxQLXk22cmkEnib0MTyEidiJY0D3dCBmkP2Pz+SSDsyo64J7T8J1LUwWnI54jVU0sAqepJGT17g513vKNIuOKMR8h7BcPG22a5qc6k1YVPLr9TafkoDJIJePcbSlIBb/AircoBxoxvUP67EZRaz8JY3TEX8WYFOPvtHZViMU3hplKjfI7WusM0MuYnYhY63xXbtjL//FTOR6LTCb2ZiYnwIEfJBhBqBcOyz+11Qj2SPe2g1R5zk0ocw3KkhsN8IPt8mDlmJMlU+2ArULuIVsGiO+ArJtFEssuCCwKCAQACUMTNBZdFvQn6/l46ChMs++zzOjmwzgsfUNXWsqNGnCKJo5Dh/2LA01AcwcsEG/EcJaK9Otu/o3kCwUoXr8O3WugoDcd3Th8uGNXrni0yPAgN8NuRILcdpPOzFbd3ErQ2InI4f2e5z06M/fYszeWvFDFO2oTVA96U4oWNshY4tbO6Sn8aDwV56B5Owx972h44CHRRFySoiGVxezpJnmYOgEpgN/Gx/5RvkQFi7GEXpWEXgE6D7fiE14THPbMvR+OnYTfBq8aqQ/z4vxFUzIOuCv2SYMMQYTxlYJsHKtYN4Or4J4LgUc/2CjpdUsacLtH1CnCs+fzpaGdy1rBGzbdZAoIBAG4+0C38AalMfMtIEz3JycqndoT6bHsHPB8eyV34r4/lynLHFktr+EYUszQ2iS6nxzCeMtzpKldbipByhlGR880PiVzRuyGvCYGPLfJvk4/LApRUiwbPbOUQ/NnJesOh2GvUHAeDkjC5U6IOv5AH8CmlL2+sBq8P6THyS3adD+PCg7zFz3b4LpUapNrPi7OqJLmhdVOzNjvA3mLU4fR/aGeqz09EJJ9Ta/uRTs4Q7CH6xIFUWuuKnjqL+N73TiJmOFIOEt5//co2bZwR13O74p8GJVL/xVM2W5/KCEvrZ7TTlfECpZznmML35xafazoGdvFdIycWlTUvKzEahJAy3LcCggEBAK1j2CE/Ptzsd1uspAbTFUIBjAa+S4kWwfI6idVSDHrT8jrHtZ+b1u1+1qAGg/YyCL19AUZi4a06NYlMAw4LKO+mY/LD3rf35wk9TePnqU1ZqJlA7y+AiA9JOpZzMClJxQdbp+wDZ/sA9xTbP1v+kC7En+btU8tFuBm5evFlU81Xfh6FMvFL/X89qB7ULNTdEEHc9AjkbnKM9AUggouUcVsCTBjWVr+I5g393p92z50hKZfT1e5mx21GyN9KEvlXKQWbDt0ixoNWX0WLbTngridjCQ7+AMtaePeJV8RwAY9fqrSFCP26ebNuDGvsUwWoP6NwtC2PIlTpFh25iFHczkc=";

                                        string lstrJWT = JsonWebToken.Encode(lobjjwtClass, key, JwtHashAlgorithm.RS256);
                                        LoggingAdapter.WriteLog("JWT JsonWebToken Encode:" + lstrJWT);

                                        requestData lobjrequestData = new requestData();
                                        lobjrequestData.jwt = lstrJWT;
                                        lobjrequestData.redirect_url = ConfigurationManager.AppSettings["RedirectURL"].ToString();

                                        LoggingAdapter.WriteLog("JWT RequestData:" + lobjrequestData);

                                        string lstrPostUrl = "https://api.ominto.com/auth/inficashback/sso";
                                        string lstrPostData = JsonWebToken.JsonSerializer.Serialize(lobjrequestData);

                                        LoggingAdapter.WriteLog("JWT lstrPostData:" + lstrPostData);

                                        StringBuilder sb = new StringBuilder();
                                        sb.Append("<html>");
                                        sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
                                        sb.AppendFormat("<form name='form' action='{0}' method='post'>", lstrPostUrl);
                                        sb.AppendFormat("<label><input type='text' runat='server' name='requestData' value='{0}'></label>", lstrPostData);
                                        sb.Append("</form>");
                                        sb.Append("</body>");
                                        sb.Append("</html>");
                                        LoggingAdapter.WriteLog("stringBuilder strart:" + sb.ToString());
                                        HttpContext.Current.Response.Write(sb.ToString());
                                        //HttpContext.Current.Response.End();
                                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                                        LoggingAdapter.WriteLog("stringBuilder strart:" + sb.ToString());
                                    }
                                }
                            }
                            else
                            {
                                mstrRedirectEmptyURL = "AuthenticationFailed";
                            }
                        }
                        else
                        {
                            lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "IncorrectFormat"), ActivityType.loginsuccessful);
                            return mstrRedirectEmptyURL = "IncorrectFormat";
                        }
                    }
                }
            }
            else
            {
                mstrRedirectEmptyURL = "AuthenticationFailed";
            }
        }
        catch (ApplicationException ex)
        {
            if (ex.Message.Equals("AccountLock"))
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AccountLock"), ActivityType.loginsuccessful);
                mstrRedirectEmptyURL = "AccountLock";
            }
            else
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AuthenticationFailed"), ActivityType.loginsuccessful);
                mstrRedirectEmptyURL = "AuthenticationFailed";
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.Equals("AccountLock"))
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AccountLock"), ActivityType.loginsuccessful);
                lobjInfiModel.UpdateUserLoginAttempts(pstrMemberid);
                mstrRedirectEmptyURL = "AccountLock";
            }
            else if (ex.Message.Equals("Login Authentication Failed"))
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AuthenticationFailed"), ActivityType.loginsuccessful);
                lobjInfiModel.UpdateUserLoginAttempts(pstrMemberid);
                mstrRedirectEmptyURL = "AuthenticationFailed";
            }
        }
        lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, mstrRedirectEmptyURL), ActivityType.Login);
        return mstrRedirectEmptyURL;
    }

    private static void updateSessionId(HttpContext Context)
    {
        System.Web.SessionState.SessionIDManager manager = new System.Web.SessionState.SessionIDManager();
        string oldId = manager.GetSessionID(Context);
        string newId = manager.CreateSessionID(Context);
        bool isAdd = false, isRedir = false;
        manager.SaveSessionID(Context, newId, out isRedir, out isAdd);
        HttpApplication ctx = (HttpApplication)HttpContext.Current.ApplicationInstance;
        HttpModuleCollection mods = ctx.Modules;
        System.Web.SessionState.SessionStateModule ssm = (SessionStateModule)mods.Get("Session");
        System.Reflection.FieldInfo[] fields = ssm.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
        SessionStateStoreProviderBase store = null;
        System.Reflection.FieldInfo rqIdField = null, rqLockIdField = null, rqStateNotFoundField = null;
        foreach (System.Reflection.FieldInfo field in fields)
        {
            if (field.Name.Equals("_store")) store = (SessionStateStoreProviderBase)field.GetValue(ssm);
            if (field.Name.Equals("_rqId")) rqIdField = field;
            if (field.Name.Equals("_rqLockId")) rqLockIdField = field;
            if (field.Name.Equals("_rqSessionStateNotFound")) rqStateNotFoundField = field;
        }
        object lockId = rqLockIdField.GetValue(ssm);
        if ((lockId != null) && (oldId != null)) store.ReleaseItemExclusive(Context, oldId, lockId);
        rqStateNotFoundField.SetValue(ssm, true);
        rqIdField.SetValue(ssm, newId);
    }


    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string pstrMemberid = txtUserName.Value;
     string pstrPassword=txtPassword.Value;
     bool pboolRememberMe = chkRemember.Checked;
        InfiModel lobjInfiModel = new InfiModel();
        MemberDetails lobjMemberDetails = null;
        string mstrRedirectEmptyURL = string.Empty;
        try
        {
            string lstrEncryptPassword = string.Empty;
            string lstrDecryptPassword = string.Empty;
            //ProgramMaster lobjProgramMaster = lobjCBQModel.GetProgramDetails(lstrProgramName);
            //ProgramMaster lobjProgramMaster = HttpContext.Current.Application["ProgramMaster"] as ProgramMaster;

            MemberDetails lobjEnrollMemberDetails = new MemberDetails();
            SearchMember lobjSearchMember = new SearchMember();
            //lobjSearchMember.RelationReference = pstrMemberid;
            // lobjSearchMember.RelationType = Convert.ToInt32(RelationType.Login);
            lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
            lobjSearchMember.SearchColumnName = "Created_By";
            lobjSearchMember.SearchColumnValue = pstrMemberid;
            LoggingAdapter.WriteLog("Search Details :- User Id:" + pstrMemberid + " Relation Type:" + lobjSearchMember.RelationType);
            // lobjEnrollMemberDetails = lobjInfiModel.GetEnrollMemberDetails(lobjSearchMember);
            lobjEnrollMemberDetails = lobjInfiModel.GetEnrollMemberDetailsByPrimaryReference(lobjSearchMember);


            if (lobjEnrollMemberDetails != null)
            {
                ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramDetailsByID(lobjEnrollMemberDetails.ProgramId);

                if (lobjProgramDefinition != null)
                {
                    SystemParameter lobjPasswordPolicy = lobjInfiModel.GetSystemParametres(lobjProgramDefinition.ProgramId);
                    if (lobjPasswordPolicy != null)
                    {
                        Regex regex = new Regex((lobjPasswordPolicy.PasswordPolicy));
                        if (regex.Match(pstrPassword).Success)
                        {
                            lstrEncryptPassword = lobjInfiModel.EncryptPassword(pstrPassword);
                            string lstrRelationRef = lobjEnrollMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                            lobjMemberDetails = lobjInfiModel.CheckLoginRelationMembershipCrediantials(lstrRelationRef, lstrEncryptPassword, lobjProgramDefinition.ProgramId);
                            if (lobjMemberDetails != null)
                            {
                                HttpContext.Current.Session["ProgramMaster"] = lobjProgramDefinition;
                                List<ProgramCurrencyDefinition> lobjListOfProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
                                lobjListOfProgramCurrencyDefinition = lobjInfiModel.GetProgramCurrencyDefinition(lobjProgramDefinition.ProgramId);
                                HttpContext.Current.Session["ProgramCurrency"] = lobjListOfProgramCurrencyDefinition;
                                string lstrCurrency = lobjListOfProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals("REWARDZ")).Currency;
                                HttpContext.Current.Session["DefaultCurrency"] = lstrCurrency;




                                updateSessionId(HttpContext.Current);
                                if (pboolRememberMe)
                                {
                                    HttpCookie cookie = new HttpCookie("InfiLogin");
                                    cookie.Values.Add("MembershipRef", pstrMemberid);
                                    cookie.Expires = DateTime.Now.AddDays(15);
                                    HttpContext.Current.Response.Cookies.Add(cookie);
                                }
                                Core.Platform.MemberActivity.Entities.MemberActivitySession lobjMemberActivitySession = HttpContext.Current.Session["MemberActivitySession"] as Core.Platform.MemberActivity.Entities.MemberActivitySession;
                                lobjMemberActivitySession.ReferenceNumber = pstrMemberid;
                                lobjMemberActivitySession.ActivityType = ActivityType.loginsuccessful;
                                bool isMembershipUpdated = lobjInfiModel.UpdateMemberShipActivitySession(lobjMemberActivitySession);
                                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "successful"), ActivityType.loginsuccessful);

                                HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                if (HttpContext.Current.Session["ShopSelected"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = "InfiShop.aspx?id=" + lobjMemberDetails.MemberRelationsList[0].RelationReference;
                                }
                                else if (HttpContext.Current.Session["SelectedItinerary"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = "FlightPassenger.aspx";
                                }
                                else if (HttpContext.Current.Session["HotelSelected"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = HttpContext.Current.Session["HotelSelected"].ToString();
                                }
                                else if (HttpContext.Current.Session["CarRefId"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = HttpContext.Current.Session["CarRefId"].ToString();
                                    mstrRedirectEmptyURL = "CarDetails.aspx?id=" + ((MemberDetails)HttpContext.Current.Session["MemberDetails"]).MemberRelationsList[0].RelationReference;
                                }

                                else if (HttpContext.Current.Session["Insurance"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = "Insurance.aspx";
                                }


                                else if (HttpContext.Current.Session["MemberDetails"] != null)
                                {
                                    //if (lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.Login)).ForceChangePassword)
                                    if (lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).ForceChangePassword)
                                    {
                                        HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                        mstrRedirectEmptyURL = "profile.aspx";
                                    }
                                    else if (lobjMemberDetails.MemberRelationsList[0].LoginAttempt >= 5)
                                    {
                                        mstrRedirectEmptyURL = "AccountLock";
                                    }
                                    else if (HttpContext.Current.Session["CallbackUrl"] != null)
                                    {
                                        mstrRedirectEmptyURL = Convert.ToString(HttpContext.Current.Session["CallbackUrl"]);
                                    }
                                    else
                                    {
                                        // mstrRedirectEmptyURL = "StatementSummary.aspx";
                                        LoggingAdapter.WriteLog("JWT Session Start:");
                                        Customer lobj = new Customer(lobjMemberDetails.Email, lobjMemberDetails.FirstName, lobjMemberDetails.MemberRelationsList[0].RelationReference, lobjMemberDetails.LastName);
                                        LoggingAdapter.WriteLog("JWT Customer:" + lobjMemberDetails.Email + "," + lobjMemberDetails.FirstName + "," + lobjMemberDetails.MemberRelationsList[0].RelationReference + "," + lobjMemberDetails.LastName);

                                        jwtClass lobjjwtClass = new jwtClass(lobj);

                                        string key = "MIIJKAIBAAKCAgEApLXSkLKQY2t1GJf4A2ZHKSZ83eOK683AZfpyDElARsoyRURaaeL9U2ZobUqfojp5DDXIVTmOokMSj8SpwtqgXqalqSFRb9eRYEGATn/x5WvCwd8D6BDk7KZre9XxkbOUkfv7rT4PsxZwfQsAolNhBImm9lFWMIfdogjj5VT4mtx0yyfR92YHJgqIWrIDXl0YfMdnEmKLSuJ36fAVL+synCw0LA3EFja90ykyEGz8AgxGH/GmurwmwL4WhoK9ABT6s+tk+rDlyFwWuTreqRrO38sxj1WkoLxcuQhMZmK7as4czoJPoegDDxWBtFtn2s4h7busX6hQ8BW+u1fgzv1kd3Fl1Bqo2w9qF1lRry0zXvOkJO6k3Q7MhwojDUUrts62ikjPU7wTsYzodL3E5CpmSOBGylmhro09xtA9CFEuuCdFt1gwoDvJPOddnpC3nipdBaiJsait/PgxfBybh0bNko0BDSzqE07fc/ZOCciUC7x93P6w50N4bj4YmD06BVx6ufS3wMWDKLTDg2b6wIotejTvxoxKsKjbKfy7GFu6pNbohM5RMwkEnAJ2NFJ+n/+9Z+zmf5kM3irSTMgYjmPsa3WCgVFHR8V3Zz1ZpZ8rZ46sHlnDaA8B+/FQuJxPIJ5FVLvCK0dhfJFfmuWQIOU/3YO7ppWxvRkcqTIbsSEVUO8CAwEAAQKCAgAm9nf0ELmFOzM8+R4LN9OXkWoyVxRSyZ48wWzfKSjSVhxkNFIrAC4q32vOhruSdcCSudpFBtON4M3wQnZiCLOtEEDZ7G7GRTk3iuceKrf/LGmq0OWsrOpqbMe6SJbXf6KikfgsOCUgmStM+SBSReUKXMGpPP5lho5QOUgnxNz2TTY3pJjPrEv+nLk6syeG2ADl64mfWL5YnY/+P/PTHREgzngc/7imBJcM747PQeivV1XzUoe9XqrQJ2dOtHifH1da3lFJ2DTTG62obZukZxoO6JM1s+24tDY+8w6Xnb11+4Hm+/54b1jh5ZWEI6dCpEZ0ZVPWYQucb5hp2tTsQhbUpTcALFvvr91t/5P3Vh1jQxMtGcZwbU4jLSJ08F6M5Ke2KuiNnb3jHfXowCQYlXho5w+nvewR2lKuy76mcOsWuIMnqjxXHXPOaE74RcmOCRtN+zBtIpxlowxMAIXm3cnG8IfYuvRMPTqfMKOHe1p4cylsUPgvIclc65Du0Gdc4wN+zNo4yWLKe6KHseHXsgQgj6Re2bSKU23/bgLpGGXFUOg99tvRuqpz4LVnAILmfgOjczWwfVvKlDgOcHdrisua0gcUZ7sNoqWiGQVj4m/nrdZxBBYvX9eR/ZVqNMCyOj9H82IlsJqAjgEgXlaspE3RKMvVQ7ddZUE/gaoBNn2nyQKCAQEA1vHmiqhbIjKptieanjSn6Hei3Z20Oeuq6vQXCZm8SE69Ms1sn6TlPmRrW1erHVsX9te1nfNNDX8lyysYSTHSt7RmmnJo0uMq3euzSrtmZ2zi+0EXM5eoYTP6Ni3jRyHTV0OS1c1Q8Gt2UWjakRGVBcHFoxwrWAFN3wpP2iiU9RgXNEvtWIVFO1IG2KHPw6m5tYwXxFY1jFkxRSNznZ60LdB0N/UXL5nnCH/7n3Y3IBSSz+r5jwsKcpjh0QjbXqrDitTmNvJXxAtTqnBvomRAvvlVWJjRRnRQuJEwgcOGbGi88auGGlt6geGwEF5A5Dw+UV4swGwEIdY/i0pgROR/LQKCAQEAxCuchxEPuXrFMFWCv39+P38A+B4XtjzP+vXK+MA14rgjs9vyiHwxQLXk22cmkEnib0MTyEidiJY0D3dCBmkP2Pz+SSDsyo64J7T8J1LUwWnI54jVU0sAqepJGT17g513vKNIuOKMR8h7BcPG22a5qc6k1YVPLr9TafkoDJIJePcbSlIBb/AircoBxoxvUP67EZRaz8JY3TEX8WYFOPvtHZViMU3hplKjfI7WusM0MuYnYhY63xXbtjL//FTOR6LTCb2ZiYnwIEfJBhBqBcOyz+11Qj2SPe2g1R5zk0ocw3KkhsN8IPt8mDlmJMlU+2ArULuIVsGiO+ArJtFEssuCCwKCAQACUMTNBZdFvQn6/l46ChMs++zzOjmwzgsfUNXWsqNGnCKJo5Dh/2LA01AcwcsEG/EcJaK9Otu/o3kCwUoXr8O3WugoDcd3Th8uGNXrni0yPAgN8NuRILcdpPOzFbd3ErQ2InI4f2e5z06M/fYszeWvFDFO2oTVA96U4oWNshY4tbO6Sn8aDwV56B5Owx972h44CHRRFySoiGVxezpJnmYOgEpgN/Gx/5RvkQFi7GEXpWEXgE6D7fiE14THPbMvR+OnYTfBq8aqQ/z4vxFUzIOuCv2SYMMQYTxlYJsHKtYN4Or4J4LgUc/2CjpdUsacLtH1CnCs+fzpaGdy1rBGzbdZAoIBAG4+0C38AalMfMtIEz3JycqndoT6bHsHPB8eyV34r4/lynLHFktr+EYUszQ2iS6nxzCeMtzpKldbipByhlGR880PiVzRuyGvCYGPLfJvk4/LApRUiwbPbOUQ/NnJesOh2GvUHAeDkjC5U6IOv5AH8CmlL2+sBq8P6THyS3adD+PCg7zFz3b4LpUapNrPi7OqJLmhdVOzNjvA3mLU4fR/aGeqz09EJJ9Ta/uRTs4Q7CH6xIFUWuuKnjqL+N73TiJmOFIOEt5//co2bZwR13O74p8GJVL/xVM2W5/KCEvrZ7TTlfECpZznmML35xafazoGdvFdIycWlTUvKzEahJAy3LcCggEBAK1j2CE/Ptzsd1uspAbTFUIBjAa+S4kWwfI6idVSDHrT8jrHtZ+b1u1+1qAGg/YyCL19AUZi4a06NYlMAw4LKO+mY/LD3rf35wk9TePnqU1ZqJlA7y+AiA9JOpZzMClJxQdbp+wDZ/sA9xTbP1v+kC7En+btU8tFuBm5evFlU81Xfh6FMvFL/X89qB7ULNTdEEHc9AjkbnKM9AUggouUcVsCTBjWVr+I5g393p92z50hKZfT1e5mx21GyN9KEvlXKQWbDt0ixoNWX0WLbTngridjCQ7+AMtaePeJV8RwAY9fqrSFCP26ebNuDGvsUwWoP6NwtC2PIlTpFh25iFHczkc=";

                                        string lstrJWT = JsonWebToken.Encode(lobjjwtClass, key, JwtHashAlgorithm.RS256);
                                        LoggingAdapter.WriteLog("JWT JsonWebToken Encode:" + lstrJWT);

                                        requestData lobjrequestData = new requestData();
                                        lobjrequestData.jwt = lstrJWT;
                                        lobjrequestData.redirect_url = ConfigurationManager.AppSettings["RedirectURL"].ToString();

                                        LoggingAdapter.WriteLog("JWT RequestData:" + lobjrequestData);

                                        string lstrPostUrl = "https://api.ominto.com/auth/inficashback/sso";
                                        string lstrPostData = JsonWebToken.JsonSerializer.Serialize(lobjrequestData);

                                        LoggingAdapter.WriteLog("JWT lstrPostData:" + lstrPostData);

                                        StringBuilder sb = new StringBuilder();
                                        sb.Append("<html>");
                                        sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
                                        sb.AppendFormat("<form name='form' action='{0}' method='post'>", lstrPostUrl);
                                        sb.AppendFormat("<label><input type='text' runat='server' name='requestData' value='{0}'></label>", lstrPostData);
                                        sb.Append("</form>");
                                        sb.Append("</body>");
                                        sb.Append("</html>");
                                        LoggingAdapter.WriteLog("stringBuilder strart:" + sb.ToString());
                                        HttpContext.Current.Response.Write(sb.ToString());
                                        //HttpContext.Current.Response.End();
                                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                                        LoggingAdapter.WriteLog("stringBuilder strart:" + sb.ToString());
                                    }
                                }
                            }
                            else
                            {
                                mstrRedirectEmptyURL = "AuthenticationFailed";
                            }
                        }
                        else
                        {
                            lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "IncorrectFormat"), ActivityType.loginsuccessful);
                            //return mstrRedirectEmptyURL = "IncorrectFormat";
                        }
                    }
                }
            }
            else
            {
                mstrRedirectEmptyURL = "AuthenticationFailed";
            }
        }
        catch (ApplicationException ex)
        {
            if (ex.Message.Equals("AccountLock"))
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AccountLock"), ActivityType.loginsuccessful);
                mstrRedirectEmptyURL = "AccountLock";
            }
            else
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AuthenticationFailed"), ActivityType.loginsuccessful);
                mstrRedirectEmptyURL = "AuthenticationFailed";
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.Equals("AccountLock"))
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AccountLock"), ActivityType.loginsuccessful);
                lobjInfiModel.UpdateUserLoginAttempts(pstrMemberid);
                mstrRedirectEmptyURL = "AccountLock";
            }
            else if (ex.Message.Equals("Login Authentication Failed"))
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AuthenticationFailed"), ActivityType.loginsuccessful);
                lobjInfiModel.UpdateUserLoginAttempts(pstrMemberid);
                mstrRedirectEmptyURL = "AuthenticationFailed";
            }
        }
        lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, mstrRedirectEmptyURL), ActivityType.Login);
    }
}