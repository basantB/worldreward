﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchTour.aspx.cs" Inherits="SearchTour" %>--%>

<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="SearchTour.aspx.cs" Inherits="SearchTour" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <script src="js/TourSearchScript.js"></script>
    <link href="CSS/jquery.ui.autocomplete.css" rel="stylesheet" type="text/css" />
    <link href="css/Tour.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="js/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <div class="Tour-bg">
        <div class="contain">
            <asp:HiddenField runat="server" ID="hdnDestinationid" />
            <div class="containHead">
                Enter Your Favourite Destination</div>
            <div class="txtDest">
                <asp:TextBox ID="txtDestination" runat="server" Text="Enter Destination" onfocus="placeholderOnFocus(this,'Enter Destination');"
                    CssClass="inputtext" onblur="placeholderOnFocus(this,'Enter Destination');"></asp:TextBox>
                <input type="button" class="btnSearching" value="LET'S GO" onclick="goTOProducts();" />
                <div class="clr">
                </div>
                <div id="requiredValidation" style="color:red;text-align:left"></div>
            </div>
        </div>
    </div>
</asp:Content>
