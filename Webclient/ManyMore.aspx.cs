﻿using Framework.EnterpriseLibrary.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Member.Entites;
using InfiPlanetModel.Model;
using Core.Platform.OTP.Entities;
using Core.Platform.Transactions.Entites;
using System.Configuration;
using Framework.EnterpriseLibrary.UniqueNumberGenerator;
using System.Text;
using System.Security.Cryptography;
using CB.IBE.Platform.ClientEntities;
using CB.IBE.Platform.Hotels.ClientEntities;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;

public partial class ManyMore : System.Web.UI.Page
{
    List<TransactionDetails> LstGetMemberStatementTransaction = new List<TransactionDetails>();
    InfiModel lobjModel = new InfiModel();
    static List<ProgramCurrencyDefinition> lobjListProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
    static ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();
    static ProgramDefinition lobjProgramMaster = null;
    static int DisplayBlock = 0;
    static decimal ldeAEDRate = Convert.ToDecimal(ConfigurationSettings.AppSettings["QARToAED"]);/// QAR To AED
    protected void Page_Load(object sender, EventArgs e)
    {
        //Stop Caching in IE
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        //Stop Caching in Firefox
        Response.Cache.SetNoStore();
        if (!IsPostBack)
        {
            MemberDetails lobjMemberDetails = new MemberDetails();
            if (Session["MemberDetails"] != null || Convert.ToString(Session["MemberDetails"]) == string.Empty)
            {
                Session["PurchaseMilesClick"] = null;
                lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
                //lblMemberId.Text = lobjMemberDetails.MemberRelationsList[0].RelationReference;

                lobjProgramMaster = lobjModel.GetProgramMaster();
                lobjListProgramCurrencyDefinition = lobjModel.GetProgramCurrencyDefinition(lobjProgramMaster.ProgramId);

                lobjProgramCurrencyDefinition = lobjListProgramCurrencyDefinition.Find(lobj => lobj.IsDefault.Equals(true));

                DisplayBlock = 1000;

                double doublePurchaserate = Convert.ToDouble(lobjProgramCurrencyDefinition.CustomerPurchaseRate);
                int ddlPoint = Convert.ToInt32(ddlPointBlocks.Text);
                InfiModel objModel = new InfiModel();
                lblTotalPoints.Text = Convert.ToString(Convert.ToString(objModel.FloatToThousandSeperated(Convert.ToSingle(ddlPoint * DisplayBlock))));
                lblAmount.Text = Convert.ToString(objModel.FloatToThousandSeperated(Convert.ToSingle(Math.Round((doublePurchaserate * (ddlPoint * DisplayBlock)), 2)))) + " ";
                lblAEDAmount.Text = Convert.ToString(objModel.FloatToThousandSeperated(Convert.ToSingle(Math.Ceiling(Convert.ToDecimal(lblAmount.Text) * ldeAEDRate))) );

            }
            else
            {
                Session["PurchaseMilesClick"] = "1";
                Response.Redirect("Login.aspx");
            }
        }

    }

    protected void ddlPointBlocks_SelectedIndexChanged(object sender, EventArgs e)
    {
        double doublePurchaserate = Convert.ToDouble(lobjProgramCurrencyDefinition.CustomerPurchaseRate);
        int ddlPoint = Convert.ToInt32(ddlPointBlocks.Text);
        InfiModel objModel = new InfiModel();
        lblTotalPoints.Text = Convert.ToString(objModel.FloatToThousandSeperated(Convert.ToSingle(ddlPoint * DisplayBlock)));
        lblAmount.Text = Convert.ToString(objModel.FloatToThousandSeperated(Convert.ToSingle(Math.Round((doublePurchaserate * (ddlPoint * DisplayBlock)), 2)))) + " ";
        lblAEDAmount.Text = Convert.ToString(objModel.FloatToThousandSeperated(Convert.ToSingle(Math.Ceiling(Convert.ToDecimal(lblAmount.Text) * ldeAEDRate))));
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string strAmount = string.Empty;
            if (Session["MemberDetails"] != null || Convert.ToString(Session["MemberDetails"]) != "")
            {
                MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
                double doublePurchaserate = Convert.ToDouble(lobjProgramCurrencyDefinition.CustomerPurchaseRate);

                int ddlPoint = Convert.ToInt32(ddlPointBlocks.Text);
                strAmount = Convert.ToString(doublePurchaserate * (ddlPoint * DisplayBlock));

                if (strAmount != null && strAmount != "")
                {
                    string lstrPGTotalPoints = Convert.ToInt32(ddlPoint * DisplayBlock).ToString();
                    string lstSecretkey = ConfigurationSettings.AppSettings["PGSecretkey"].ToString();
                    string lstTxnId = Convert.ToString(GenerateUniqueNumber.Get12DigitNumberDateTime());

                    string lstSecureData = lstSecretkey + "|" + ConfigurationSettings.AppSettings["PGMerchantId"].ToString() + "|" + lblAEDAmount.Text + "|" + lstrPGTotalPoints + "|" + lstTxnId;

                    MD5 md5HashAlgo = MD5.Create();
                    byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(lstSecureData);
                    byte[] hashBytes = md5HashAlgo.ComputeHash(inputBytes);

                    StringBuilder secureData = new StringBuilder();
                    for (int i = 0; i < hashBytes.Length; i++)
                    {
                        secureData.Append(hashBytes[i].ToString("X2"));
                    }

                    TransactionDetails lobjTransactionDetails = new TransactionDetails();
                    lobjTransactionDetails.Amounts = Convert.ToSingle(strAmount);
                    lobjTransactionDetails.Points = Convert.ToInt32(lstrPGTotalPoints);
                    lobjTransactionDetails.TransactionDate = DateTime.Now;
                    lobjTransactionDetails.TransactionCurrency = lobjProgramCurrencyDefinition.Currency;
                    lobjTransactionDetails.RelationReference = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    lobjTransactionDetails.TransactionDetailBreakage.Amount = Convert.ToSingle(strAmount);
                    Session["PurchasePoints"] = lobjTransactionDetails;

                    CreateItineraryResponse lobjFlightList = (CreateItineraryResponse)Session["ItineraryResponse"];
                    string lstrSessionId = Convert.ToString(Session["SessionId"]);

                    string lstrHotelSearchUrl = Convert.ToString(Session["HotelSelected"]);
                    HotelSearchResponse lobjSearchResponse = Session["hotels"] as HotelSearchResponse;
                    string lstrURL = HttpContext.Current.Session["URL"] as string;

                    List<object> lobjCache = new List<object>() { lobjProgramMaster, lobjMemberDetails, lobjTransactionDetails, lobjFlightList, lstrSessionId, lstrHotelSearchUrl, lobjSearchResponse, lstrURL };
                    CachingAdapter.Add(lstTxnId, lobjCache);


                    //Encrypted Data
                    string lstSecureHash = Convert.ToString(secureData);

                    Response.Clear();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<html>");
                    sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
                    sb.AppendFormat("<form name='form' action='{0}' method='post'>", ConfigurationSettings.AppSettings["POSTURLConfig"].ToString());
                    sb.AppendFormat("<input type='hidden' runat='server' name='PG_MerchantId' value='{0}'>", ConfigurationSettings.AppSettings["PGMerchantId"].ToString());
                    sb.AppendFormat("<input type='hidden' runat='server' name='PG_MerchantName' value='{0}'>", ConfigurationSettings.AppSettings["PGMerchantUserName"].ToString());
                    sb.AppendFormat("<input type='hidden' runat='server' name='PG_Amount' value='{0}'>", lblAEDAmount.Text);
                    sb.AppendFormat("<input type='hidden' runat='server' name='PG_Points' value='{0}'>", lstrPGTotalPoints);
                    sb.AppendFormat("<input type='hidden' runat='server' name='PG_PaymentType' value='{0}'>", 4);
                    sb.AppendFormat("<input type='hidden' runat='server' name='PG_RedemptionType' value='{0}'>", 5);
                    sb.AppendFormat("<input type='hidden' runat='server' name='PG_TxnCurrency' value='{0}'>", "AED");
                    sb.AppendFormat("<input type='hidden' runat='server' name='PG_Narration' value='{0}'>", "Purchase Points");
                    sb.AppendFormat("<input type='hidden' runat='server' name='PG_TxnId' value='{0}'>", lstTxnId);
                    sb.AppendFormat("<input type='hidden' runat='server' name='PG_SecureHash' value='{0}'>", lstSecureHash);
                    sb.Append("</form>");
                    sb.Append("</body>");
                    sb.Append("</html>");
                    Response.Write(sb.ToString());
                    Response.End();
                    LoggingAdapter.WriteLog("PaymentRegisteration RelationRef###PostDetails-" + lobjTransactionDetails.RelationReference + "###" + sb.ToString());

                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception ex)
        {

        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string CalCulateCashBackPoints(string strPoints, string strMultval)
    {
        try
        {
            //ProgramDefinition lobjProgramMaster = null;
            //lobjProgramMaster = HttpContext.Current.Application["ProgramMaster"] as ProgramDefinition;
            //double doubleCashBackRate = Convert.ToDouble(lobjProgramMaster.lobjProgramAttribute.);
            //LoggingAdapter.WriteLog("CalculeCadhBackPoints lobjProgramMaster:" + lobjProgramMaster.ProgramName);
            //int ddlPoint = Convert.ToInt32(strPoints);
            //int pintmultval = Convert.ToInt32(strMultval);
            //string strTotalPoints = Convert.ToString(ddlPoint * pintmultval);
            //string strAmount = Convert.ToString(Math.Floor(Convert.ToDouble(strTotalPoints) * doubleCashBackRate));
            //return strAmount;

            string strAmount = "0";
            if (HttpContext.Current.Session["MemberDetails"] != null ||Convert.ToString(HttpContext.Current.Session["MemberDetails"])== string.Empty)
            {
                InfiModel lobjCBIModel = new InfiModel();
                MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
                List<ProgramCurrencyDefinition> lobjListProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>()  ;
                lobjListProgramCurrencyDefinition = lobjCBIModel.GetProductProgramCurrencyDefinition(lobjMemberDetails) ;
                double doubleCashBackRate = Convert.ToDouble(lobjListProgramCurrencyDefinition[0].CashBackRate);
                LoggingAdapter.WriteLog("CashbackRate: " + lobjListProgramCurrencyDefinition[0].CashBackRate);
                int ddlPoint = Convert.ToInt32(strPoints);
                LoggingAdapter.WriteLog("ddlPoint: " + ddlPoint);
                int pintmultval = Convert.ToInt32(strMultval);
                LoggingAdapter.WriteLog("pintmultval: " + pintmultval);
                string strTotalPoints = Convert.ToString(ddlPoint * pintmultval);
                LoggingAdapter.WriteLog("strTotalPoints: " + strTotalPoints);
                strAmount = Convert.ToString(Math.Floor(Convert.ToDouble(strTotalPoints) * doubleCashBackRate));
                LoggingAdapter.WriteLog("strAmount: " + strAmount);
                HttpContext.Current.Session["CashBackPointsRequired"] = strTotalPoints;
                HttpContext.Current.Session["CashBackAmount"] = strAmount;
            }
            return strAmount;
            LoggingAdapter.WriteLog("strAmountByBasant: " + strAmount);
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("CalculeCadhBackPoints exception:" + ex.Message + "|" + ex.StackTrace + ex.InnerException);
            return null;
        }

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string CashBackConfirm(string strAmount, string strtotalPoints)
    {
        bool Status = false;
        string lstrResult = string.Empty;
        InfiModel lobjInfiModel  = new InfiModel();
        //CBIModel lobjCBIModel = new CBIModel();
        MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
        try
        {
            if (lobjMemberDetails != null)
            {
                LoggingAdapter.WriteLog("CashBackConfirm started:" + lobjMemberDetails.LastName + "|" + strAmount + "|" + strtotalPoints);
                string lstrCurrency = lobjInfiModel.GetDefaultCurrency();
                HttpContext.Current.Session["MemberMiles"] = Convert.ToInt32(lobjInfiModel.CheckAvailbility(lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency));
                LoggingAdapter.WriteLog("CashBackConfirm CheckAvailbility:" + Convert.ToInt32(HttpContext.Current.Session["MemberMiles"]).ToString());
                if (Convert.ToInt32(HttpContext.Current.Session["MemberMiles"]) >= Convert.ToInt32(strtotalPoints))
                {
                    LoggingAdapter.WriteLog("CashBackConfirm CheckAvailbility validation:");
                    Core.Platform.OTP.Entities.OTPDetails lobjOTPDetails = new OTPDetails() ;
                    lobjOTPDetails.UniquerefID = lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.CASHBACKCONFIRM;
                    Status = lobjInfiModel.GenerateReviewnConfirmOTP(lobjOTPDetails, lobjMemberDetails);
                    LoggingAdapter.WriteLog("CashBackConfirm GenerateReviewnConfirmOTP :" + Status);
                    if (Status)
                    {
                        AdditionalRedemptionDetails lobjAdditionalRedemptionDetails = new AdditionalRedemptionDetails();
                        lobjAdditionalRedemptionDetails.Amounts = Convert.ToSingle(strAmount);
                        lobjAdditionalRedemptionDetails.Points = Convert.ToInt32(strtotalPoints);
                        HttpContext.Current.Session["AdditionalRedemptionDetails"] = lobjAdditionalRedemptionDetails;
                        lstrResult = "Success";
                    }
                    else
                    {
                        lstrResult = "OtpError";
                    }
                    return lstrResult;
                }
                else
                {
                    lstrResult = "InsufficientPoints";
                    return lstrResult;
                }

            }
            else
            {
                lstrResult = "Login";
                return lstrResult;
            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("CashBackConfirm Exception :" + ex.Message + ex.StackTrace + ex.InnerException);

            return null;
        }



    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod()]
    public static string ValidateUser(string pstrMemberId)
    {
        string mstrRedirectEmptyURL = string.Empty;
        InfiModel lobjWordRewardModel  = new InfiModel();
        MemberDetails lobjMemberDetails = new MemberDetails();
        try
        {
            LoggingAdapter.WriteLog("ValidateUser Start :" + pstrMemberId);

            MemberDetails lobjLoginMemberDetails = new MemberDetails();
            lobjLoginMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
            
              if (Convert.ToString(pstrMemberId) != Convert.ToString(lobjLoginMemberDetails.MemberRelationsList[0].RelationReference))
                {
                    lobjMemberDetails = lobjWordRewardModel.GetMemberDetails(pstrMemberId);
                    if (lobjMemberDetails != null)
                    {
                        LoggingAdapter.WriteLog("ValidateUser Member Details" + lobjMemberDetails.LastName);
                        lobjWordRewardModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberId, lobjMemberDetails.LastName), ActivityType.loginsuccessful);
                        mstrRedirectEmptyURL = lobjMemberDetails.LastName;
                        HttpContext.Current.Session["RecipientMemberDetails"] = lobjMemberDetails;
                        if (HttpContext.Current.Session["MemberDetails"] == null)
                        {
                            mstrRedirectEmptyURL = "Login";
                        }
                    }
                    else
                    {
                        lobjWordRewardModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberId, "AuthenticationFailed"), ActivityType.loginFail);
                        mstrRedirectEmptyURL = "No Member";
                    }
                }
                else
                {
                    lobjWordRewardModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberId, "AuthenticationFailed"), ActivityType.loginFail);
                    mstrRedirectEmptyURL = "MemberIdSame";
                }
            
        }
        catch (ApplicationException ex)
        {
            lobjWordRewardModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberId, "AuthenticationFailed"), ActivityType.loginFail);
            mstrRedirectEmptyURL = "AuthenticationFailed";
        }
        lobjWordRewardModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberId, mstrRedirectEmptyURL), ActivityType.Login);
        return mstrRedirectEmptyURL;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod()]
    public static string ValidateandTransferPoints(string pstrTransferPoints)
    {

        LoggingAdapter.WriteLog("ValidateandTransferPoints " + pstrTransferPoints);
        string lstrStatus = string.Empty;
        Int64 pstrTransferAmt =0;
        InfiModel lobjWordRewardModel = new InfiModel();
        MemberDetails lobjSenderMemberDetails = new MemberDetails();
        MemberDetails lobjRecipientMemberDetails = new MemberDetails();
        try
        {
            LoggingAdapter.WriteLog("ValidateandTransferPoints start" + pstrTransferPoints);
            if (HttpContext.Current.Session["MemberDetails"] != null || Convert.ToString(HttpContext.Current.Session["MemberDetails"]) == string.Empty)
            {
                if (Convert.ToInt32(pstrTransferPoints) >= 10 && Convert.ToInt32(pstrTransferPoints) <= 100000)
                {
                    lobjSenderMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
                    lobjRecipientMemberDetails = HttpContext.Current.Session["RecipientMemberDetails"] as MemberDetails;
                    pstrTransferAmt = Convert.ToInt64(HttpContext.Current.Session["CalculatedAmount"]);
                    LoggingAdapter.WriteLog("ValidateandTransferPoints Sender" + lobjSenderMemberDetails.MobileNumber + "|Receiver: " + lobjRecipientMemberDetails.MobileNumber);
                    string Sender = lobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    string Reciever = lobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    LoggingAdapter.WriteLog("ValidateandTransferPoints Final Sender" + Sender + "|Receiver: " + Reciever);
                    if (Sender == Reciever)
                    {
                        lstrStatus = "SenderReceiverrSame";
                    }
                    else
                    {
                        ProgramDefinition lobjProgramDefinition = lobjWordRewardModel.GetProgramMaster();
                        List<ProgramCurrencyDefinition> lobjListProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
                        ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();

                        LoggingAdapter.WriteLog("ValidateandTransferPoints Program Definition" + lobjProgramDefinition.ProgramId);

                        lobjListProgramCurrencyDefinition = lobjWordRewardModel.GetProgramCurrencyDefinition(lobjProgramDefinition.ProgramId);
                        lobjProgramCurrencyDefinition = lobjListProgramCurrencyDefinition.Find(obj => obj.IsDefault.Equals(true));
                        if (lobjProgramCurrencyDefinition != null)
                        {
                            LoggingAdapter.WriteLog("ValidateandTransferPoints ProgramCurrencyDefinition" + lobjProgramCurrencyDefinition.Currency);
                            string pstrProgramCurrency = "";
                            lstrStatus = lobjWordRewardModel.TransferPoints(lobjSenderMemberDetails, lobjRecipientMemberDetails, Convert.ToInt32(pstrTransferAmt), lobjProgramCurrencyDefinition.Currency);

                        }
                        else
                        {
                            lstrStatus = "NOProgramurrency";
                        }

                    }
                }
                else
                {
                    lstrStatus = "PointsLimitExceed";
                }

            }
            else
            {
                lstrStatus = "PleaseMemberLogin";
            }
        }
       
        catch (Exception ex)
        {
            lstrStatus = "Error";
            LoggingAdapter.WriteLog("ValidateandTransferPoints Exception" + ex.Message);
        }
        LoggingAdapter.WriteLog("ValidateandTransferPoints End" + lstrStatus);
        // lstrStatus="Success";
        return lstrStatus;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string GetCalculatedAmount(string pstrPoints)
    {
        string lstrAmount = "0";
        lstrAmount = CalculatedAmount(pstrPoints);
        return lstrAmount;

    }

    public static string CalculatedAmount(string pstrPoints)
    {
        
        InfiModel lobjModel = new InfiModel();
        string lstrAmount = "0";
        //HttpContext.Current.Session["CalculatedAmount"] = null;
        if (HttpContext.Current.Session["MemberDetails"] != null)
        {
            MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
            if (!string.IsNullOrEmpty(pstrPoints))
            {
                int lintPoints = Convert.ToInt32(pstrPoints);
                List<ProgramCurrencyDefinition> lobjlistProgramCurrencyDefinition = lobjModel.GetProductProgramCurrencyDefinition(lobjMemberDetails);
                if (lobjlistProgramCurrencyDefinition != null && lobjlistProgramCurrencyDefinition.Count > 0)
                {
                    ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();

                    //float lfltRedemptionRate = lobjModel.GetProgramRedemptionRate(lobjlistProgramCurrencyDefinition[0].Currency, RedemptionCodeKeys.UTL.ToString(), lobjProgramDefinition.ProgramId);
                    float lfltRedemptionRate = lobjlistProgramCurrencyDefinition[0].RedemptionRate;
                    LoggingAdapter.WriteLog("RedemptionRate : Amount Calculation - TransferPoints - " + lfltRedemptionRate);

                    double ldblAmount = Convert.ToDouble(Math.Floor(lintPoints * lfltRedemptionRate));
                    //double ldblAmount = Convert.ToDouble(Math.Floor(lintPoints * lobjlistProgramCurrencyDefinition[0].RedemptionRate));
                    lstrAmount = ldblAmount.ToString();
                    HttpContext.Current.Session["CalculatedAmount"] = lstrAmount;


                }
            }
        }
        return lstrAmount;
    }





}