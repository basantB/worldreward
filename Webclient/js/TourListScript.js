﻿$.fn.digits = function () {
    return this.each(function () {
        $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    })
},
$.fn.stars = function () {
    return $(this).each(function () {
        // Get the value
        var val = parseFloat($(this).html());
        //val = Math.round(val * 4) / 4
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;
        // Create stars holder
        var $span = $('<span />').width(size);
        // Replace the numerical value with stars
        $(this).html($span);
    });
}

$(window).load(function () {
    //$("#CP_hfPageCount").val(1);

    //  var destId = getQuerystring("destId");
    $('.modify').click(function () {
        $('.modify-box').slideToggle();
    });
    $("#CP_hfCategoryId").val(0);
    GetProductList($("#CP_hfPageCount").val(), $("#CP_hfRecordCount").val());
    PaintCategoryList()

});


function getQuerystring(key, default_) {
    if (default_ == null) default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}

function GetProductList(pstrPageCount, pstrRecordCount) {
    var PageCount = pstrPageCount;
    var RecordCount = pstrRecordCount;

    var startRecord = (((PageCount - 1) * RecordCount) + 1);
    var EndRecord = (PageCount * RecordCount);
    var TopX = startRecord + "-" + EndRecord;
    var CategoryId = "0";
    if ($("#CP_hfCategoryId").val() > 0) {
        CategoryId = $("#CP_hfCategoryId").val();
    }



    var StartDate = "";
    var EndDate = "";
    // var DestId = destId;

    var SubCatId = "0";
    var IsDeals = false;
    var productList = "";
    $(".srchcircle").show();
    $(".tour_list_a").hide();
    $.ajax({
        type: 'POST',
        url: 'ProductList.aspx/GetProductList',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "{'pstrTopX':'" + TopX.toString() + "','pstrStartDate':'" + StartDate.toString() + "','pstrEndDate':'" + EndDate.toString() + "','pstrCatId':'" + CategoryId.toString() + "','pstrSubCatId':'" + SubCatId.toString() + "','pstrIsDeals':'" + IsDeals.toString() + "'}",
        success: function (msg) {

            if (msg.d != null) {
                var Template = msg.d[1];
                $("#CP_LoadTemplate")[0].innerHTML = Template.toString();
                productList = $.parseJSON(msg.d[0]);
                $("#result1OneWay").setTemplateElement("ProductListTemplate");
                $("#result1OneWay").processTemplate(productList);
                $("span.stars").stars();
                $(".pointmiles").digits();
                $("#lblSearchSummary").html("Your search destination is: " + productList[0].PrimaryDestinationName);
                $("#CP_txtDestination").val(productList[0].PrimaryDestinationName);
                $(".srchcircle").hide();
                $(".tour_list_a").show();
            }
        }


    });

}

function loadNextRecord() {
    //var destId = getQuerystring("destId");
    var hfPageCount = parseInt(parseInt($("#CP_hfPageCount").val()) + 1);
    GetProductList(hfPageCount, $("#CP_hfRecordCount").val());
    return false;
}

function PaintCategoryList() {
    var CategotyList = "";
    var CategoryResponse = "";
    $.ajax({
        type: 'POST',
        url: 'ProductList.aspx/PaintCategories',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "",
        success: function (msg) {
            CategotyList = msg.d;
            if (msg.d != null) {
                for (var i = 0; i < CategotyList.length; i++) {
                    CategoryResponse += "<div class='airlinelist'>";
                    //CategoryResponse += "<li class='listHotel'> <a rel='1' class='menu_link' href='' onclick='getProductByCategotyId(" + CategotyList[i].Id + ");'>" + CategotyList[i].GroupName + " (" + CategotyList[i].ProductCount + ") </a><input id='chk" + CategotyList[i].Id + "' type='checkbox' onclick='getProductByCategotyId(" + CategotyList[i].Id + ");' /> " + CategotyList[i].GroupName + " (" + CategotyList[i].ProductCount + ")</li>";
                    CategoryResponse += "<li class='listHotel'> <a class='menu_link' href='#' onclick='getProductByCategotyId(" + CategotyList[i].Id + ");'>" + CategotyList[i].GroupName + "</a></li>";
                    //CategoryResponse += "<a style='line-height: 14px;height: auto;width:auto;cursor:pointer;' onClick ='getProductByCategotyId(" + CategotyList[i].Id + ");'>" + CategotyList[i].GroupName + "</a>";

                    CategoryResponse += "</div>";
                }
            }

            $("#DivCategoryList").html(CategoryResponse);
        }
    });

}
function BookTour(pstrTourCode) {
    $.ajax({
        type: 'POST',
        url: 'ProductList.aspx/BookNowTourProduct',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "{'pstrTourCode':'" + pstrTourCode + "'}",
        cache: false,
        success: function (msg) {
            if (msg.d)
                window.location = "ViewProductInfo.aspx";
            else {
                window.location = "Login.aspx";
            }
        },
        error: function (errmsg) {

        }
    });
}




function getProductByCategotyId(pstrCategoryId) {
    $("#CP_hfCategoryId").val(pstrCategoryId);
    GetProductList(1, 30);
    $("#CP_hfPageCount").val(1);
    $("#CP_hfRecordCount").val(30);
    //var PageCount = $("#CP_hfPageCount").val();
    //var RecordCount = $("#CP_hfRecordCount").val();

    //var startRecord = (((PageCount - 1) * RecordCount) + 1);
    //var EndRecord = (PageCount * RecordCount);
    //var TopX = startRecord + "-" + EndRecord;

    //var CategoryId = pstrCategoryId;

    //$.ajax({
    //    type: 'POST',
    //    url: 'ProductList.aspx/BindProductByCategory',
    //    contentType: 'application/json; charset=utf-8',
    //    dataType: 'json',
    //    data: "{'pstrCategoryId':'" + CategoryId + "','pstrTopX':'" + TopX + "'}",
    //    cache: false,
    //    success: function (msg) {
    //        if (msg.d != null) {
    //            var Template = msg.d[1];
    //            $("#CP_LoadTemplate")[0].innerHTML = Template.toString();
    //            productList = $.parseJSON(msg.d[0]);
    //            $("#result1OneWay").setTemplateElement("ProductListTemplate");
    //            $("#result1OneWay").processTemplate(productList);
    //            //$("span.stars").stars();
    //        }
    //    },
    //    error: function (errmsg) {

    //    }
    //});

}