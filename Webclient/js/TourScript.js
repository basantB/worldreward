﻿app.controller('MainCtrl', function ($scope, $http) {
    

    $scope.getAllHotelCities = function (val) {
        debugger;
        return $http({
            url: "SearchTour.aspx/GetDestinationList",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            //alert('hi')
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                var list = item.split('|')
                var name = list[1]
                addresses.push(name);
            });
            return addresses;
        });
    };

    $scope.TourValidation = function () {

        debugger;
        var cnt = 0;
        if (($("#CP_txtDestination").val() == '' || $("#CP_txtDestination").val() == null) || $("#CP_txtDestination").val() == "Destination" || $("#CP_txtDestination").val() == "*Required") {
            
            $("#CP_txtDestination").addClass("schfldserr");
            $("#CP_txtDestination").val("*Required");
            cnt++;
        }

        if (cnt == 0) {
            debugger;
            return clicktotoursearch();
        }
    }

    clicktotoursearch = function () {
        debugger;
        $.ajax({
            type: 'POST',
            url: 'SearchTour.aspx/SetDestinationId',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'pstrDestinationId':'" + $("#CP_hdnDestinationid").val() + "'}",
            cache: false,
            success: function (msg) {
                if (msg.d) {
                    window.location.href = "ProductList.aspx";
                }
            },
            error: function (errmsg) {
            }
        });

    }

});