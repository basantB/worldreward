﻿function PickupCityChange() {
    
    $("#CP_txtCountry").removeClass("schfldserr");
    BindGlobalLocationList($('#CP_txtCountry').val(), $('#CP_DDlCity option:selected').text(), $('#CP_DDLLocation'), '','false');
    ClearDateTime();
    ClearDropDateTime();
    
    $('#CP_ddlDropCountry').empty();
    $('#CP_ddlDropCountry').append("<option value='select'>Select</option>");

    $('#CP_ddlDropCity').empty();
    $('#CP_ddlDropCity').append("<option value='select'>Select</option>");

    $('#CP_ddlDropLocation').empty();
    $('#CP_ddlDropLocation').append("<option value='select'>Select</option>");

}
function DropoffCitySelect() {
    $("#CP_ddlDropCountry").removeClass("schfldserr");
    DropoffCityChange($('#CP_ddlDropCountry option:selected').val(), $('#CP_ddlDropCity option:selected').text(), $('#CP_ddlDropLocation'), "","false");
    ClearDropDateTime();
}
function PickupLocationSelect() {
    //$("#CP_DDLLocation").removeClass("schfldserr");
    $("#CP_DDLLocation").removeClass("schfldserr");
    $("#CP_DDLLocation option[value='*Required']").remove();
    $('#CP_ddlDropCountry').empty();
    $('#CP_ddlDropCountry').append("<option value='" + $('#CP_txtCountry').val() + "'>" + $('#CP_txtCountry').val() + "</option>");
    PickUpLocationChange($('#CP_txtCountry').val(), $('#CP_DDlCity option:selected'), $('#CP_DDLLocation option:selected'), $('#CP_ddlDropCountry'), $('#CP_ddlDropCity'), $('#CP_ddlDropLocation'), "false");
    ClearDateTime();
    ClearDropDateTime();
    $('#CP_ddlDropCountry').empty();
    $('#CP_ddlDropCountry').append("<option value='select'>Select</option>");

    $('#CP_ddlDropCity').empty();
    $('#CP_ddlDropCity').append("<option value='select'>Select</option>");

    $('#CP_ddlDropLocation').empty();
    $('#CP_ddlDropLocation').append("<option value='select'>Select</option>");
    $("#divDDLLocation").removeClass("schfldserr");
}
function DropoffLocationSelect() {
    //PickUpLocationChange($('#CP_txtCountry').val(), $('#CP_DDlCity option:selected'), $('#CP_DDLLocation option:selected'), $('#CP_ddlDropCountry'), $('#CP_ddlDropCity'), $('#CP_ddlDropLocation'));
    ClearDateTime();
    ClearDropDateTime();
}
function DropOffCountryChange() {
    BindDropOffCity($('#CP_ddlDropCity'), $('#CP_ddlDropLocation'), $('#CP_txtCountry').val(), $('#CP_DDlCity option:selected').text(), "false");
    ClearDateTime();
    ClearDropDateTime();
}
function ClearDateTime() {
    $("#CP_CarTextBoxCheckin").val("");
    $("#CP_ddlFromTime").html('');
}
function ClearDropDateTime() {
    $('#CP_CarTextBoxCheckout').val('');
    $("#CP_ddlDropFromTime").html('');
}
function BindGlobalCountryList(ddlCountry, ddlCity, ddlLocation, SelectedVal) {
    var args = arguments.length

    ddlCountry.html('');
    ddlCountry.append("<option value='select'>Select Country</option>");

    ddlCity.html('');
    ddlCity.append("<option value='select'>Select City</option>");

    ddlLocation.html('');
    ddlLocation.append("<option value='select'>Select Location</option>");
    $("#CarValidationError").html("");
    $("#CarValidationError").hide();
    $.ajax({
        type: 'POST',
        url: 'Index.aspx/GetAllCountryList',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "{}",
        cache: false,
        async: false,
        success: function (msg) {
            var Country = $.parseJSON(msg.d);
            if (Country != null) {
                for (var CountCountry = 0; CountCountry < Country.length; CountCountry++) {
                    if (SelectedVal != '' && SelectedVal != '' && SelectedVal == $.trim(Country[CountCountry].Value)) {
                        ddlCountry.append('<option value="' + $.trim(Country[CountCountry].Value) + '" selected="selected">' + Country[CountCountry].Value + '</option>');
                    }
                    else {
                        ddlCountry.append('<option value="' + $.trim(Country[CountCountry].Value) + '">' + Country[CountCountry].Value + '</option>');
                    }
                }
                //ddlCountry.selectric('refresh');
                //if (Country.length == 1) {
                //    BindGlobalCityList(ddlCity, ddlLocation, $.trim(Country[0].Value), '')
                //}
            }
            else {

            }
        },
        error: function (errmsg) {
        }
    });

    return false;
}
function BindGlobalCityList(pCtrlCity, pCtrlLocation, pstrCountry, pstrSelectedCityValue, session) {
    if (pstrCountry.toLowerCase() != "select" && pstrCountry.toLowerCase() != "select") {
        $.ajax({
            type: 'POST',
            //url: 'CarSearch.aspx/GetPickupCity',
            url: 'Index.aspx/GetPickupCity',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'Country':'" + pstrCountry + "','session':'" + session + "'}",
            cache: false,
            async: false,
            success: function (msg) {
                if (msg.d != "Error_pickupcity") {
                    $("#CP_DDlCity").removeClass("schfldserr");
                    $("#CP_DDlCity option[value='*Required']").remove();
                    var location = $.parseJSON(msg.d);
                    //bind all city data to the city drop down control.
                        pCtrlCity.empty();
                   
                        pCtrlCity.append("<option value='select' selected='selected'>Select</option>");

                        for (var Countlocation = 0; Countlocation < location.CarPickUpCityListResponce.PickUpCityListResponce.Items[0].City.length; Countlocation++) {
                            if (pstrSelectedCityValue == location.CarPickUpCityListResponce.PickUpCityListResponce.Items[0].City[Countlocation].Value) {
                                pCtrlCity.append("<option selected='selected' value=" + location.CarPickUpCityListResponce.PickUpCityListResponce.Items[0].City[Countlocation].Value + ">" + location.CarPickUpCityListResponce.PickUpCityListResponce.Items[0].City[Countlocation].Value + "</option>");
                            }
                            else {
                                pCtrlCity.append("<option value=" + location.CarPickUpCityListResponce.PickUpCityListResponce.Items[0].City[Countlocation].Value + ">" + location.CarPickUpCityListResponce.PickUpCityListResponce.Items[0].City[Countlocation].Value + "</option>");
                            }
                        }
                        //pCtrlCity.selectric('refresh')
                        pCtrlLocation.empty();
                        pCtrlLocation.append("<option value='select' selected='selected'>Select</option>");

                        $("#CP_ddlDropCountry").empty();
                        $("#CP_ddlDropCountry").append("<option value='select' selected='selected'>Select</option>");
                        $("#CP_ddlDropCity").empty();
                        $("#CP_ddlDropCity").append("<option value='select' selected='selected'>Select</option>");
                        $("#CP_ddlDropLocation").empty();
                        $("#CP_ddlDropLocation").append("<option value='select' selected='selected'>Select</option>");
                        ClearDateTime();
                        ClearDropDateTime();

                        //pCtrlLocation.selectric('refresh')
                        //$(".CarTextBoxCheckin").val("");
                        //$(".CarTextBoxCheckout").val("");
                        //}
                }
                else {
                    $("#CarValidationError").html("No City Found, Please Try Again");
                    $("#CarValidationError").show();
                }
                return false;
            },
            error: function (errmsg) {
            }
        });
    }
    else {
        pCtrlCity.empty();
        pCtrlLocation.empty();
        pCtrlCity.append("<option value='select' selected='selected'>Select</option>");
        pCtrlLocation.append("<option value='select' selected='selected'>Select</option>");
        //pCtrlCity.selectric('refresh')
        //pCtrlLocation.selectric('refresh')
    }
}
function BindGlobalLocationList(pstrCountry, pstrCity, pCtrlLocation, pstrSelectedLocationText, session) {

    pCtrlLocation.empty();
    pCtrlLocation.append("<option value='select' selected='selected'>Select</option>");
    pCtrlLocation.removeClass("schfldserr");
    if (pstrCountry != 'Select' && pstrCountry != 'select' &&
        pstrCity != 'select' && pstrCity != 'Select') {
        $.ajax({
            type: 'POST',
            url: 'Index.aspx/GetPickupLocation',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'Country':'" + pstrCountry + "','City':'" + pstrCity + "','session':'" + session + "'}",
            cache: false,
            async: false,
            success: function (msg) {
                if (msg.d != "Error_PickupLocation") {
                    var location = $.parseJSON(msg.d);
                    $('.CheckInTextClear').empty();
                    $('.CheckOutTextClear').empty();
                    $("#CP_CarTextBoxCheckin").val("");
                    $("#CP_CarTextBoxCheckout").val("");
                    pCtrlLocation.empty();
                    //if (location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location.length == 1) {
                    //    pCtrlLocation.empty();
                    //    pCtrlLocation.html(location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location[0].Value);
                    //}
                    //else {

                        if (pstrSelectedLocationText == '' || pstrSelectedLocationText == undefined || pstrSelectedLocationText == null) {
                            pCtrlLocation.append("<option value='select' selected='selected'>Select</option>");
                        }
                        else {
                            pCtrlLocation.append("<option value='select'>Select</option>");
                        }
                        for (var Countlocation = 0; Countlocation < location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location.length; Countlocation++) {

                            if ($.trim(location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location[Countlocation].Value) == pstrSelectedLocationText) {
                                pCtrlLocation.append("<option selected='selected' value=" + location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location[Countlocation].id + ">" + location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location[Countlocation].Value + "</option>");
                            } else {
                                pCtrlLocation.append("<option value=" + location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location[Countlocation].id + ">" + location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location[Countlocation].Value + "</option>");
                            }


                        }
                    //}
                    //pCtrlLocation.selectric('refresh');
                }
                else {
                    $("#CarValidationError").html("No Location Found, Please Try Again");
                    $("#CarValidationError").show();
                }
                return false;
            },
            error: function (errmsg) {
            }
        });
    }
    //pCtrlLocation.selectric('refresh');
    return false;
}
function PickUpLocationChange(pCtrlCountry, pCtrlCity, pCtrlLocation, pCtrlDropCountry, pCtrlDropCity, pCtrlDropLocation, session) {

    //pCtrlDropCountry.selectric('destroy');
    //pCtrlDropCity.selectric('destroy');
    //pCtrlDropLocation.selectric('destroy');
    pCtrlDropCountry.empty();
    pCtrlDropCity.empty();
    pCtrlDropLocation.empty();
    if (pCtrlCountry != "select country" && pCtrlLocation.val().toLowerCase() != "select" && pCtrlCity.text().toLowerCase() != "select city") {
        $.ajax({
            type: 'POST',
            url: 'Index.aspx/GetDropOffDetails',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'Country':'" + pCtrlCountry + "','locationId':'" + pCtrlLocation.val() + "','City':'" + pCtrlCity.text() + "','session':'" + session + "'}",
            cache: false,
            success: function (msg) {

                var location = $.parseJSON(msg.d);
                if (location.CarDropOffCountryListResponse != null && location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items != null && location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country != null) {
                    for (var Countlocation = 0; Countlocation < location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country.length; Countlocation++) {
                        pCtrlDropCountry.append("<option value=" + location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country[Countlocation].Value + ">" + location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country[Countlocation].Value + "</option>");

                    }

                    if (location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country.length > 1) {
                        pCtrlDropCountry.val(pCtrlCountry);
                        //                    $("#ContentPlaceHolder1_ddlDropCountry option[value='" + $('#ContentPlaceHolder1_DivDDLCountry').text() + "']").attr("selected", true);
                    }
                    else {
                        pCtrlDropCountry.val(location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country[0].Value)
                    }
                    if (location.CarDropOffCityListResponce != null && location.CarDropOffCityListResponce.DropOffCityListResponce.Items != null && location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City != null) {
                        for (var Countlocation = 0; Countlocation < location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City.length; Countlocation++) {
                            pCtrlDropCity.append("<option value=" + location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City[Countlocation].Value + ">" + location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City[Countlocation].Value + "</option>");
                        }

                        if (location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City.length > 1) {
                            pCtrlDropCity.val(pCtrlCity.val());
                            //                    $("#ContentPlaceHolder1_ddlDropCity option[value='" + $('#ContentPlaceHolder1_DivDDLCity').text() + "']").attr("selected", true);
                        }
                        else {
                            pCtrlDropCity.val(location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City[0].Value);
                        }

                        if (location.CarDropOffLocationListResponce != null && location.CarDropOffLocationListResponce.DropOffLocationListResponce.Items != null && location.CarDropOffLocationListResponce.DropOffLocationListResponce.Items[0].Location != null) {
                            for (var Countlocation = 0; Countlocation < location.CarDropOffLocationListResponce.DropOffLocationListResponce.Items[0].Location.length; Countlocation++) {
                                pCtrlDropLocation.append("<option value=" + location.CarDropOffLocationListResponce.DropOffLocationListResponce.Items[0].Location[Countlocation].id + ">" + location.CarDropOffLocationListResponce.DropOffLocationListResponce.Items[0].Location[Countlocation].Value + "</option>");
                            }
                            if (location.CarDropOffLocationListResponce.DropOffLocationListResponce.Items[0].Location.length == 1) {
                                pCtrlDropLocation.val(pCtrlLocation.val());
                                //$('#ContentPlaceHolder1_ddlDropLocation option:nth(0)').attr("selected", "selected");
                            }
                            else {
                                //var indx = $("#ContentPlaceHolder1_DDLLocation").prop("selectedIndex");
                                ///$('#ContentPlaceHolder1_ddlDropLocation option:nth(' + indx + ')').attr("selected", "selected");
                                pCtrlDropLocation.val(pCtrlLocation.val());
                            }
                        }
                        else {
                            $("#CarValidationError").html("No DropOffLocation Found, Please Try Again");
                        }
                    }
                    else {
                        $("#CarValidationError").html("No DropOffLocation Found, Please Try Again");
                    }

                }
                else {
                    $("#CarValidationError").html("No DropOffCountry Found, Please Try Again");
                }
               

                
                //$('select').selectric({
                //    maxHeight: 100,
                //    disableOnMobile: false,
                //    responsive: true
                //});
                return false;
            },
            error: function (errmsg) {
            }
        });
    }
}
function DropoffCityChange(pstrCountry, pstrCity, pCtrlLocation, pstrSelectedLocationText, session) {//GetDropOffLocation
    pCtrlLocation.empty();
    pCtrlLocation.append("<option value='select' selected='selected'>Select Location</option>");
    if (pstrCountry != 'Select Country' && pstrCountry != 'select' && pstrCity != 'select' && pstrCity != 'Select City') {
        $("#CarValidationError").html("");
        $("#CarValidationError").hide();
        $.ajax({
            type: 'POST',
            url: 'Index.aspx/GetDropOffLocationChange',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'Country':'" + pstrCountry + "','locationId':'" + $("#CP_DDLLocation").find('option:selected').prop("value") + "','City':'" + pstrCity + "','session':'" + session + "'}",
            cache: false,
            success: function (msg) {
                if (msg.d != "Error_PickupLocation") {
                    var location = $.parseJSON(msg.d);
                    $('.CheckInTextClear').empty();
                    $('.CheckOutTextClear').empty();
                    $("#CarTextBoxCheckin").val("");
                    $("#CarTextBoxCheckout").val("");
                    pCtrlLocation.empty();
                    //if (location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location.length == 1) {
                    //    pCtrlLocation.append("<option value=" + location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location[0].id + ">" + location.CarPickUpLocationListResponce.PickUpLocationListResponce.Items[0].Location[0].Value + "</option>");
                    //}
                    //else {

                    if (pstrSelectedLocationText == '' || pstrSelectedLocationText == undefined || pstrSelectedLocationText == null) {
                        pCtrlLocation.append("<option value='select' selected='selected'>Select Location</option>");
                    }
                    else {
                        pCtrlLocation.append("<option value='select'>Select Location</option>");
                    }
                    for (var Countlocation = 0; Countlocation < location.DropOffLocationListResponce.Items[0].Location.length; Countlocation++) {

                        if ($.trim(location.DropOffLocationListResponce.Items[0].Location[Countlocation].Value) == pstrSelectedLocationText) {
                            pCtrlLocation.append("<option selected='selected' value=" + location.DropOffLocationListResponce.Items[0].Location[Countlocation].id + ">" + location.DropOffLocationListResponce.Items[0].Location[Countlocation].Value + "</option>");
                        } else {
                            pCtrlLocation.append("<option value=" + location.DropOffLocationListResponce.Items[0].Location[Countlocation].id + ">" + location.DropOffLocationListResponce.Items[0].Location[Countlocation].Value + "</option>");
                        }
                    }
                    //}
                   // pCtrlLocation.selectric('refresh');
                }
                else {
                    $("#CarValidationError").html("No Location Found, Please Try Again");
                    $("#CarValidationError").show();
                }
                return false;
            },
            error: function (errmsg) {
            }
        });
    }
    //pCtrlLocation.selectric('refresh');
    return false;
}
function BindDropOffCountry(ddlCountry, ddlCity, ddlLocation, SelectedVal, session) {//GetDropOffCountry
    var args = arguments.length;
    ddlCountry.html('');
    ddlCountry.append("<option value='select'>Select Country</option>");
    //ddlCity.html('');
    //ddlCity.append("<option value='select'>Select City</option>");
    //ddlLocation.html('');
    //ddlLocation.append("<option value='select'>Select Location</option>");
    $("#CarValidationError").html("");
    $("#CarValidationError").hide();
    $.ajax({
        type: 'POST',
        url: 'Index.aspx/BindDropOffCountry',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "{'Country':'" + $('#CP_DDLCountry option:selected').text() + "','locationId':'" + $('#CP_DDLLocation option:selected').val() + "','City':'" + $('#CP_DDlCity option:selected').text() + "','session':'" + session + "'}",
        cache: false,
        async: false,
        success: function (msg) {
            var location = $.parseJSON(msg.d);
            //dropoffcountrybinding starts here
            if (location.CarDropOffCountryListResponse != null && location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items != null && location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country != null) {
                for (var Countlocation = 0; Countlocation < location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country.length; Countlocation++) {
                    ddlCountry.append('<option value="' + $.trim(location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country[Countlocation].Value) + '">' + location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country[Countlocation].Value + '</option>');
                }
                if (location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country.length > 1) {
                    ddlCountry.val(pCtrlCountry.val());
                    //                    $("#ContentPlaceHolder1_ddlDropCountry option[value='" + $('#ContentPlaceHolder1_DivDDLCountry').text() + "']").attr("selected", true);
                }
                else {
                    ddlCountry.val(location.CarDropOffCountryListResponse.DropOffCountryListResponce.Items[0].Country[0].Value)
                }
                //ddlCountry.selectric('refresh');
            }
        },
        error: function (errmsg) {
        }
    });

    return false;
}
function BindDropOffCity(pCtrlCity, pCtrlLocation, pstrCountry, pstrSelectedCityValue, session) {//GetDropOffCity
    if (pstrCountry.toLowerCase() != "select country" && pstrCountry.toLowerCase() != "select") {
        $("#CarValidationError").html("");
        $("#CarValidationError").hide();
        $.ajax({
            type: 'POST',
            url: 'Index.aspx/GetDropOffCityByDropCountry',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'Country':'" + pstrCountry + "','locationId':'" + $('#CP_DDLLocation option:selected').val() + "','City':'" + $('#CP_DDlCity option:selected').text() + "','dropOffCountry':'" + $('#CP_ddlDropCountry option:selected').text() + "','session':'" + session + "'}",
            cache: false,
            async: false,
            success: function (msg) {
                if (msg.d != "Error_DropOffCity") {
                    var location = $.parseJSON(msg.d);
                    //bind all city data to the city drop down control.
                    pCtrlCity.empty();


                    var initialized = 0;
                    for (var Countlocation = 0; Countlocation < location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City.length; Countlocation++) {
                        if (pstrSelectedCityValue == location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City[Countlocation].Value) {
                            pCtrlCity.append("<option selected='selected' value=" + location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City[Countlocation].Value + ">" + location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City[Countlocation].Value + "</option>");
                            initialized = 1;
                        }
                        else {
                            pCtrlCity.append("<option value=" + location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City[Countlocation].Value + ">" + location.CarDropOffCityListResponce.DropOffCityListResponce.Items[0].City[Countlocation].Value + "</option>");
                        }
                    }
                    if (initialized == 0) {
                        pCtrlCity.append("<option value='select' selected='selected'>Select City</option>");
                    }
                    //pCtrlCity.selectric('refresh')
                    pCtrlLocation.empty();
                    pCtrlLocation.append("<option value='select' selected='selected'>Select Location</option>");
                    //pCtrlLocation.selectric('refresh')
                    $(".CarTextBoxCheckin").val("");
                    $(".CarTextBoxCheckout").val("");
                }
                else {
                    $("#CarValidationError").html("No City Found, Please Try Again");
                    $("#CarValidationError").show();
                }
                return false;
            },
            error: function (errmsg) {
            }
        });
    }
    else {
        pCtrlCity.empty();
        pCtrlLocation.empty();
        pCtrlCity.append("<option value='select' selected='selected'>Select City</option>");
        pCtrlLocation.append("<option value='select' selected='selected'>Select Location</option>");
        pCtrlCity.selectric('refresh')
        pCtrlLocation.selectric('refresh')
    }
}
function showreturnCar() {
    if ($.trim($("#CP_txtCountry").val()) == "Select" || $.trim($("#CP_txtCountry").val()) == "" || $.trim($("#CP_txtCountry").val()) == "*Required" 
        || $.trim($("#CP_DDlCity option:selected").text()) == "Select" || $.trim($("#CP_DDlCity option:selected").text()) == "" || $.trim($("#CP_DDlCity option:selected").text()) == "*Required"
        || $.trim($("#CP_DDLLocation option:selected").text()) == "Select" || $.trim($("#CP_DDLLocation option:selected").text()) == "" || $.trim($("#CP_DDLLocation option:selected").text()) == "*Required") {
        $("#CP_CarValidationError").show();
        $("#CP_CarValidationError")[0].innerHTML = 'Please first provide your pick-up location.';
        $("#CP_droploction").prop('checked', true);
    } else {
        $("#CP_CarValidationError").hide();
        $("#CP_CarValidationError")[0].innerHTML = '';
        if (document.getElementById('CP_droploction').checked) {
            $('.dropDetails').hide();

            if ($(window).width() > 999) {
            }
        }
        else {
            $('.dropDetails').show();
            //copy only selected Country dropdown values.
            //$('#CP_ddlDropCountry').empty();
            //$('#CP_ddlDropCountry').append("<option value='" + $('#CP_txtCountry').val() + "'>" + $('#CP_txtCountry').val() + "</option>");
            ////debugger;
            //PickUpLocationChange($('#CP_txtCountry').val(), $('#CP_DDlCity option:selected'), $('#CP_DDLLocation option:selected'), $('#CP_ddlDropCountry'), $('#CP_ddlDropCity'), $('#CP_ddlDropLocation'));
            ////$('select').selectric('refresh');

            ////-----------------end copy values---------------------//
        }
    }
}
function showdriverAge() {
    $(".driverAge").toggle();
}
function getDrophourDetails(dateReturnText, locationId) {
    $.ajax({
        type: 'POST',
        url: 'Index.aspx/GetDropOffTime',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        //data: "{'DropOffDate':'" + dateReturnText + "','LocationId':'" + $("#CP_DDLLocation").find('option:selected').prop("value") + "'}",
        data: "{'DropOffDate':'" + dateReturnText + "','LocationId':'" + locationId + "'}",
        cache: false,
        success: function (data) {

            var dateList = data.d;
            var Time = -1
            var hdnddlToTime
            var count = 0;
            $("#CP_CarValidationError").hide();
            if (data.d != '000000000000000000000000') {
                var initialized = 0;
                $("#CP_ddlDropFromTime").html('');
                for (var count = 0; count <= dateList.length; count++) {
                    var times;
                    Time++;
                    if (data.d.charAt(count) == '1') {
                        if (Time < 10) {
                            $("#CP_ddlDropFromTime").append($("<option>" + "0" + Time + "</option>"));
                            times = "0" + Time;
                        }
                        else {
                            $("#CP_ddlDropFromTime").append($("<option>" + Time + "</option>"));
                            times = Time;
                        }
                        if (initialized == 0) {
                            hdnddlToTime = times;
                            initialized = 1;
                        }
                        else
                            hdnddlToTime = hdnddlToTime + "," + times;
                    }
                }
                document.getElementById('CP_ddlDropFromTime').selectedIndex = 0;
            }
            else {
                $("#CP_CarValidationError").show();
                $("#CP_CarValidationError")[0].innerHTML = 'Car is not Available for this Date';
            }
            $("#CP_hdnddlToTime").val(hdnddlToTime);
            //$("#CP_ddlDropFromTime").selectric('refresh');
        },
        error: function (errmsg) {
            alert(errmsg.d);
        }
    });
}
function LocationSearchCarSearch() {
    var cnt = 0;
    //var msg = "";
    if ($.trim($("#CP_txtCountry").val()) == "Select" || $.trim($("#CP_txtCountry").val()) == "" || $.trim($("#CP_txtCountry").val()) == "*Required") {
        //  msg = "Please Select PickUp Country." + "<br/>";
        cnt++;
        $("#CP_txtCountry").addClass("schfldserr");
        $("#CP_txtCountry").val("*Required");
    }
    if ($.trim($("#CP_DDlCity option:selected").text()) == "Select" || $.trim($("#CP_DDlCity option:selected").text()) == "" || $.trim($("#CP_DDlCity option:selected").text()) == "*Required") {
        //msg = "Please Select PickUp City." + "<br/>";
         $("#CP_DDlCity").addClass("schfldserr");
         //$('#CP_DDlCity').empty();
         $('#CP_DDlCity').append("<option value='*Required'>*Required</option>");
         $('#CP_DDlCity').val('*Required');
         cnt++;
    }
    if ($.trim($("#CP_DDLLocation option:selected").text()) == "Select" || $.trim($("#CP_DDLLocation option:selected").text()) == "" || $.trim($("#CP_DDLLocation option:selected").text()) == "*Required") {
        //msg = "Please Select Pickup Location." + "<br/>";
         $("#CP_DDLLocation").addClass("schfldserr");
         //$('#CP_DDLLocation').empty();
         $('#CP_DDLLocation').append("<option value='*Required'>*Required</option>");
         $('#CP_DDLLocation').val('*Required');
         cnt++;
    }
    if ($.trim($("#CP_CarTextBoxCheckin").val()) == "" || $.trim($("#CP_CarTextBoxCheckin").val()) == "*Required") {
        //msg = "Please Select Pickup Date." + "<br/>";
         $("#CP_CarTextBoxCheckin").addClass("schfldserr");
         $("#CP_CarTextBoxCheckin").val("*Required");
         cnt++;
    }
    if ($.trim($("#CP_CarTextBoxCheckout").val()) == "" || $.trim($("#CP_CarTextBoxCheckout").val()) == "*Required") {
        //msg = "Please Select Drop Off Date." + "<br/>";
         $("#CP_CarTextBoxCheckout").addClass("schfldserr");
         $("#CP_CarTextBoxCheckout").val("*Required");
         cnt++;
    }
     if (!$("#CP_chkDriver").is(":checked")) {
        if ($.trim($("#CP_txtDiverAge").val()) < 21 || $.trim($("#CP_txtDiverAge").val()) > 70) {
         //   msg = "Driver age is not valid." + "<br/>";
            $("#CP_txtDiverAge").addClass("schfldserr");
            cnt++;
        }
    }
     if (!$("#CP_droploction").is(":checked")) {
        if ($.trim($("#CP_ddlDropCountry").val()) == "Select" || $.trim($("#CP_ddlDropCountry").val()) == "") {
           // msg = "Please Select Drop Country." + "<br/>";
            $("#CP_droploction").addClass("schfldserr");
        $('#CP_ddlDropCountry').empty();
        $('#CP_ddlDropCountry').append("<option value='*Required'>*Required</option>");
            cnt++;
        }
        if ($.trim($("#CP_ddlDropCity").text()) == "Select" || $.trim($("#CP_ddlDropCity").text()) == "*Required") {
           // msg = "Please Select Drop City." + "<br/>";
            $("#CP_ddlDropCity").addClass("schfldserr");
            cnt++;
        }
        if ($.trim($("#CP_ddlDropLocation").text()) == "Select" || $.trim($("#CP_ddlDropLocation").text()) == "" || $.trim($("#CP_ddlDropLocation").text()) == "*Required") {
            //msg = "Please Select Drop Location." + "<br/>";
            $("#CP_ddlDropLocation").addClass("schfldserr");
            cnt++;
        }
    }
    else if ($.trim($("#CP_CarTextBoxCheckin").val()) != "" || $.trim($("#CP_CarTextBoxCheckout").val()) != "") {
        var PickUptDate = $("#CP_CarTextBoxCheckin").val();
        var PickUpHr = $.trim($("#CP_ddlFromTime option:selected").text());
        var PickUpMin = $.trim($("#CP_ddlMinFrom option:selected").text());

        var DropDate = $("#CP_CarTextBoxCheckout").val();
        var DropHr = $.trim($("#CP_ddlDropFromTime option:selected").text());
        var DropMin = $.trim($("#CP_ddlDropMinFrom option:selected").text());

        var PickUpDateArr = PickUptDate.split('/');
        var PickupformatedDate = PickUpDateArr[1] + '/' + PickUpDateArr[0] + '/' + PickUpDateArr[2];
        var DropDateArr = DropDate.split('/');
        var DropformatedDate = DropDateArr[1] + '/' + DropDateArr[0] + '/' + DropDateArr[2];

        var PickupDateTime = new Date(PickupformatedDate + ' ' + PickUpHr + ':' + PickUpMin);
        var DropOffDateTime = new Date(DropformatedDate + ' ' + DropHr + ':' + DropMin);

        var diff = DropOffDateTime - PickupDateTime;

        var diffSeconds = diff / 1000;
        var HH = Math.floor(diffSeconds / 3600);
        var MM = Math.floor(diffSeconds % 3600) / 60;

        if (HH < 1) {
            //msg += "your rental must be for at least 1 hours or more";
            cnt++;
        }
    }
    else {
    }
    //if (msg.length > 0) {

    //    $("#CP_CarValidationError")[0].innerHTML = msg;
    //    $("#CP_divCarValidationError").show();
    //    setTimeout(blank('CP_divCarValidationError'), 2000);
    //    return !(msg.length > 0);
    //}
    //else {
    if(cnt==0){
        strPickupCountry = "pickupCountry=" + $.trim($('#CP_txtCountry').val()) + "&";
        strPickupCity = "pickupCity=" + $.trim($('#CP_DDlCity option:selected').text()) + "&";
        var strPickupLocation = "pickupLocation=" + $.trim($("#CP_DDLLocation").find('option:selected').prop("value")) + "&";
        var strPickupLocationName = "pickupLocationName=" + $.trim($("#CP_DDLLocation").find('option:selected').text()) + "&";
        var strDropofCountry = "";
        var strDropofCity = "";
        var strDropoffLocation = "";
        if ($('#CP_droploction').attr('checked')) {
            strDropofCountry = "dropofCountry=" + $.trim($('#CP_txtCountry').val()) + "&";
            strDropofCity = "dropofCity=" + $.trim($('#CP_DDlCity option:selected').text()) + "&";
            strDropoffLocation = "dropoffLocation=" + $("#CP_DDLLocation").val() + "&";
            strDropoffLocationName = "dropoffLocationName=" + $.trim($("#CP_DDLLocation").find('option:selected').text()) + "&";
        }
        else {
            strDropofCountry = "dropofCountry=" + $('#CP_ddlDropCountry option:selected').text() + "&";
            strDropofCity = "dropofCity=" + $.trim($('#CP_ddlDropCity option:selected').text()) + "&";
            strDropoffLocation = "dropoffLocation=" + $.trim($("#CP_ddlDropLocation").val()) + "&";
            strDropoffLocationName = "dropoffLocationName=" + $.trim($("#CP_ddlDropLocation").find('option:selected').text()) + "&";
        }

        var isdropoff = "isdropoff=" + $("#CP_droploction").is(":checked").toString() + "&";
        var strDepartDate = "departDate=" + $.trim($("#CP_CarTextBoxCheckin").val()) + "&";
        var strDepartTime = "departTime=" + $.trim($("#CP_ddlFromTime option:selected").text()) + ":" + $.trim($("#CP_ddlMinFrom option:selected").text()) + "&";
        var strReturnDate = "arrDate=" + $.trim($("#CP_CarTextBoxCheckout").val()) + "&";
        var strReturnTime = "arrTime=" + $.trim($("#CP_ddlDropFromTime option:selected").text()) + ":" + $.trim($("#CP_ddlDropMinFrom option:selected").text()) + "&";
        var isdriverage = "isdriverage=" + $("#CP_chkDriver").is(":checked").toString() + "&";
        var strdriverage = "";
        if ($("#CP_chkDriver").is(":checked")) {
            strdriverage = "driverage=" + 25 + "&";
        }
        else {
            strdriverage = "driverage=" + $("#CP_txtDiverAge").val() + "&";
        }
        var strRedeemPoints = "isRedeemMiles=" + "true" + "&";
        var hdnddlFromTime = "hdnddlFromTime=" + $("#CP_hdnddlFromTime").val() + "&";
        var hdnddlToTime = "hdnddlToTime=" + $("#CP_hdnddlToTime").val();
        var queryString = strPickupCountry + strDropofCountry + strPickupCity + strDropofCity + strPickupLocation + strPickupLocationName + strDropoffLocation + strDropoffLocationName + isdropoff + strDepartDate + strDepartTime + strReturnDate + strReturnTime + isdriverage + strdriverage + hdnddlFromTime + strRedeemPoints + hdnddlToTime;
        window.location = "CarSearchWait.aspx?" + queryString;
        return false;
    }
}


