﻿function LoginValidation() {
     var msg = "";
    //$("#CP_txtMemberId")[0].innerHTML = "";
    var vdf = $("#CP_txtMemberId").val();
    
    if ($("#CP_txtMemberId").val().length == 0) {
        msg += "Please enter Member Id.<br/>";
        
    }

    if (msg.length > 0) {
        $("#LoginValidation").css('color', 'red');
        $("#LoginValidation")[0].innerHTML = msg;
       
    }

    else {
        var MemberId = $("#CP_txtMemberId").val();
        //var MemberId = $("#CP_txtMemberName").val();

        $.ajax({
            url: 'ManyMore.aspx/ValidateUser',
            type: 'POST',  // or get
            contentType: 'application/json; charset =utf-8',
            data: "{'pstrMemberId':'" + MemberId.toString() + "'}",
            dataType: 'json',
            success: function (data) {
                try {

                    var newData = data.d;
                   
                    if ((newData != "") || newData == null) {
                        if (data.d == "AuthenticationFailed") {
                            $("#LoginValidation").css('color', 'red');
                            $("#LoginValidation")[0].innerHTML = "";
                            $("#LoginValidation")[0].innerHTML = "Please check the Recipient details you have entered and try again.";
                        }
                        else if (data.d == "NoMember") {
                            $("#LoginValidation").css('color', 'red');
                            $("#LoginValidation")[0].innerHTML = "";
                            $("#LoginValidation")[0].innerHTML = "The Member Id does not exist on world Rewards Program.";
                        }
                        else if (data.d == "IncorrectFormat") {
                            $("#LoginValidation").css('color', 'red');
                            $("#LoginValidation")[0].innerHTML = "";
                            $("#LoginValidation")[0].innerHTML = "Incorrect password format";
                        }
                        else if (data.d == "MemberIdSame") {
                            $("#LoginValidation").css('color', 'red');
                            $("#LoginValidation")[0].innerHTML = "";
                            $("#LoginValidation")[0].innerHTML = "Recipient Id and Login Id same";
                        }

                        else if (data.d == "Login") {
                            window.location = "Login.aspx";
                        }
                        else if (data.d == "PleaseMemberLogin") {
                            window.location = "Login.aspx";
                        }
                         else {
                            $("#LoginValidation")[0].innerHTML = "";
                            //$("#CP_divLogin").hide();
                            //$("#divOTP").hide();
                            //$("#divForceChangePassword").hide();
                            $("#divTransferPoint").show();
                            $("#txtrecipientName").val(data.d);
                            //$("#CP_lblLoginError").html('Password must be of 4 digits.');

                        }
                    }
                    else {
                        $("#LoginValidation").css('color', 'red');
                        $("#LoginValidation")[0].innerHTML = "";
                        $("#LoginValidation")[0].innerHTML = "Please check the login details you have entered and try again.";
                    }
                }
                catch (e) {
                    alert(e);
                    return false;
                }
                return false;
            },
            error: function (errmsg) {

            }
        });
    }
    return false;
};

function TransferPoints() {

  //  $("#divLoader").css('display', 'block');
    var msg = "";
    $("#LoginValidation").hide();
    var TransferPoints = parseInt($("#CP_txtTransferPoint").val());
    // var CalculatedAmt = parseInt($("#txtTransferConvAmt").val());
    if (!RegexFunc(/^[0-9]+$/, TransferPoints)) {
        msg += "Please Enter valid  amount.</br>";
    }
    //if ($("#CP_txtTransferPoint").val().length == 0) {
    //    $("#CP_txtTransferPoint").val("0");
    //}
    if ($("#CP_txtTransferPoint").val().length == 0) {
        msg += "Please Enter Points.<br/>";
    }
    if (TransferPoints < 500 || TransferPoints > 100000) {
        msg += "Points need to be  in the range of 500 and 100000.";
    }
    if (msg.length > 0) {
        $("#LoginValidation").css('color', 'red');
        $("#LoginValidation")[0].innerHTML = msg;
        $("#LoginValidation").show();
    }
    else {
        $.ajax({
            url: 'ManyMore.aspx/ValidateandTransferPoints',
            type: 'POST',
            contentType: 'application/json; charset= utf-8',
            //  data: "{'pstrTransferPoints':'" + $("#CP_txtTransferPoint").val().toString() + "','pstrTransferAmt':'" + $("#txtTransferConvAmt").val().toString() + "'}",
            data: "{'pstrTransferPoints':'" + $("#CP_txtTransferPoint").val().toString() + "'}",
            dataType: 'json',
            beforeSend: function () {
                $('.wait_overlay').show();  // <----show before sending
            },
            success: function (data) {
                //$("#divLoader").hide();
                try {

                    var newData = data.d;
                    
                    if ((newData != "") || (newData != null)) {
                        if (newData == "Success") {
                           // $("#CP_divLogin").hide();
                            $("#divTransferPoint").hide();
                            $("#LoginValidation").show();
                            $("#LoginValidation").css('color', 'green');
                            $("#LoginValidation").css('font-size', '25px');
                            $("#LoginValidation").css('padding', '5% 0 10%');
                            $("#LoginValidation").css('text-align', 'center');
                            $("#LoginValidation")[0].innerHTML = "Your points are transferred successfully.";
                        }
                        else if (newData == "InSufficientPoints") {
                           // $("#CP_divLogin").hide();
                            $("#divTransferPoint").hide();
                            $("#LoginValidation").show();
                            $("#LoginValidation").css('color', 'red');
                            $("#LoginValidation").css('font-size', '20px');
                            $("#LoginValidation").css('padding', '5% 0 10%');
                            $("#LoginValidation").css('text-align', 'center');
                            $("#LoginValidation")[0].innerHTML = "You do not have Sufficient Points to transfer.";
                        }
                        else if (newData == "PointsLimitExceed") {
                           // $("#CP_divLogin").hide();
                            $("#divTransferPoint").hide();
                            $("#LoginValidation").show();
                            $("#LoginValidation").css('color', 'red');
                            $("#LoginValidation").css('font-size', '20px');
                            $("#LoginValidation").css('padding', '5% 0 10%');
                            $("#LoginValidation").css('text-align', 'center');
                            $("#LoginValidation")[0].innerHTML = "Points need to be  in the range of 500 and 100000.";
                        }
                        else if (newData == "SenderReceiverSame") {
                           // $("#CP_divLogin").hide();
                            $("#divTransferPoint").hide();
                            $("#LoginValidation").show();
                            $("#LoginValidation").css('color', 'red');
                            $("#LoginValidation").css('font-size', '20px');
                            $("#LoginValidation").css('padding', '5% 0 10%');
                            $("#LoginValidation").css('text-align', 'center');
                            $("#LoginValidation")[0].innerHTML = "Sender and Receiver are same.";
                        }
                        else if (newData = "PleaseMemberLogin")
                        {
                            window.location = "Login.aspx";
                        }
                        else {
                            //$("#CP_divLogin").hide();
                            $("#divTransferPoint").hide();
                            $("#LoginValidation").show();
                            $("#LoginValidation").css('color', 'red');
                            $("#LoginValidation").css('font-size', '25px');
                            $("#LoginValidation").css('padding', '5% 0 10%');
                            $("#LoginValidation").css('text-align', 'center');
                            $("#LoginValidation")[0].innerHTML = "Your points transfer failed.Please try again later.";
                        }
                    }
                }
                catch (e) {
                    alert(e);
                  //  return false;
                }
               // return false;
            },
            complete: function () {
                $('.wait_overlay').hide();
            },
            error: function (errmsg) {

            }
        });
    }

};

function calcTransferPAmount() {

    var points = $("#CP_txtTransferPoint").val();
    
    var amount = "";
    var msg = "";
    $("#LoginValidation").hide();
    if (points != '' || points != null) {
        if (!RegexFunc(/^-?\d*\.?\d*$/, points)) {
            msg += "Please Enter valid WorldReward Points.</br>";
        }
        if (points.length = 1 && points.length > 10) {
            msg += "Please enter valid WorldReward Points.</br>";
        }
    }
    else {
        $("#CP_txtTransferConvAmt").val(0);
    }
    if (msg.length > 0) {
        $("#LoginValidation").show();
        $("#LoginValidation")[0].innerHTML = msg;
        $("#LoginValidation").css({ color: "#ff0000" });
    }
    else {
        $.ajax({
            type: 'POST',
            url: 'ManyMore.aspx/GetCalculatedAmount',
            contentType: 'application/json; charset=utf-8',
            datatype: 'json',
            data: "{'pstrPoints':'" + points + "'}",
            cache: false,
            async: false,
            success: function (msg) {

                amount = $.parseJSON(msg.d);

                $("#txtTransferConvAmt").val(amount);
               
                return false;
            }
        });
    }


    return false;
}

function RegexFunc(regExprs, value) {
    var pattern = new RegExp(regExprs);
    return pattern.test(value);
};
