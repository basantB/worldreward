﻿$(function () {
    $('#CP_txtDepart').datepicker({
        startDate: new Date(),
        format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {
        $("#CP_txtDepart").removeClass("schfldserr");
        $("#CP_txtReturn").val('');
        $("#CP_txtReturn").datepicker({
            startDate: e.date,
            format: "dd/mm/yyyy"
        }).on('changeDate', function (e) {
            $("#CP_txtReturn").removeClass("schfldserr");
        }

      ); return false;
    });
    return false;
});

$(window).scroll(function () {
    var WinWidth = $(window).width();
    if (WinWidth > 1100) {

        if ($(this).scrollTop() > 70) {
            //$('#UserInfo').css({
                //margin: "48px 0 20px",
                //position: "fixed",
                //right: "5%",
                //top: "0"
            //});
            //if ($('#modifyDetails').css('display') === 'block') {
            //    $("#modifyDetails").slideUp("1000");
            //}
            
            //$("#fliterDetails").css({ margin: "300px 0 0px", });
            $(".pgcol2").css({ width: "24.70%" });
        }
        else {
            $('#UserInfo').css({
                margin: "0px 0 20px 0", position: "inherit", right: "",
                top: ""
            });
            //$("#modifyDetails").slideDown("5000");
            $("#fliterDetails").css({ margin: "0", });
            $(".pgcol2").css({ width: "auto" });
        }
        $("#modifyDetails").slideUp("1000");
    }
});


var app = angular.module("DanamonFlights", ['ui-rangeSlider', 'ngAnimate', 'ui.bootstrap'])
//var app = angular.module('AlRayan', ['ngAnimate', 'ui.bootstrap']);
app.controller("MainCtrl", function ($scope, $http) {
    $scope.empty = function (id) {
        $("#CP_" + id).val('');
    }
    $scope.getLocation = function (val) {
        return $http({
            url: "FlightLBMSServices.aspx/GetAirfields",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                addresses.push(item);
            });
            return addresses;
        });
    };
    $scope.getAllAirlines = function (val) {
        return $http({
            url: "FlightLBMSServices.aspx/GetCarriers",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var carriers = [];
            //var IATACode = [];
            angular.forEach(res.data.d, function (item) {
                carriers.push({ name: item.split('-')[0], iata: item.split('-')[1] });
            });
            return carriers;
        });
    };
    $scope.onSelectAirlines = function ($item, $model, $label) {
        $("#CP_hdnCarrier").val($item.iata);
    };

    $http({
        url: "FlightList.aspx/GetFilterCriteria",
        dataType: 'json',
        method: 'POST',
        data: '',
        headers: {
            "Content-Type": "application/json"
        }
    })
       .success(function (response) {
           //$scope.names = response.d[2];
       });

    $http({
        url: "FlightList.aspx/FlightData",
        dataType: 'json',
        method: 'POST',
        data: '',
        headers: {
            "Content-Type": "application/json"
        }
    }).success(function (msg) {
        var Counts = msg.d[0];


        var lobjSearchRequest = $.parseJSON(msg.d[1]);
        var searchdetails = "";
        var DepartDate = msg.d[2];
        var ArrivalDate = msg.d[3];
        var mobSearchSummary = "";
        if (lobjSearchRequest.SearchDetails.IsReturn.toString() == 'true') {
            searchdetails = '<li class="frt"><div class="us-from"><p>' + lobjSearchRequest.SearchDetails.DepCode.City + '</p><h1>' + lobjSearchRequest.SearchDetails.OriginLocation + '</h1></div><div class="us-arr"><img src="images/arr-rght.png" /></div><div class="us-to"><p>' + lobjSearchRequest.SearchDetails.ArrCode.City + '</p><h1>' + lobjSearchRequest.SearchDetails.DestinationLocation + '</h1></div><div class="clr"></div><p class="us-date">' + DepartDate + '</p></li>';
            searchdetails += '<li><div class="us-from"><p>' + lobjSearchRequest.SearchDetails.ArrCode.City + '</p><h1>' + lobjSearchRequest.SearchDetails.DestinationLocation + '</h1></div><div class="us-arr"><img src="images/arr-rght.png" /></div><div class="us-to"><p>' + lobjSearchRequest.SearchDetails.DepCode.City + '</p><h1>' + lobjSearchRequest.SearchDetails.OriginLocation + '</h1></div><div class="clr"></div><p class="us-date">' + ArrivalDate + '</p></li>';
            mobSearchSummary = "<li><ul><li><h2>" + lobjSearchRequest.SearchDetails.OriginLocation + " to " + lobjSearchRequest.SearchDetails.DestinationLocation + "</h2></li><li>Dep Date:<br/>" + DepartDate + "</li></ul></li>";
            mobSearchSummary += "<li><ul><li><h2>" + lobjSearchRequest.SearchDetails.DestinationLocation + " to " + lobjSearchRequest.SearchDetails.OriginLocation + "</h2></li><li>Arr Date:<br/>" + ArrivalDate + "</li></ul></li>";
        }
        else {
            searchdetails = '<li class="frt"><div class="us-from"><p>' + lobjSearchRequest.SearchDetails.DepCode.City + '</p><h1>' + lobjSearchRequest.SearchDetails.OriginLocation + '</h1></div><div class="us-arr"><img src="images/arr-rght.png" /></div><div class="us-to"><p>' + lobjSearchRequest.SearchDetails.ArrCode.City + '</p><h1>' + lobjSearchRequest.SearchDetails.DestinationLocation + '</h1></div><div class="clr"></div><p class="us-date">' + DepartDate + '</p></li>';
            mobSearchSummary = "<li><ul><li><h2>" + lobjSearchRequest.SearchDetails.OriginLocation + " to " + lobjSearchRequest.SearchDetails.DestinationLocation + "</h2></li><li>Dep Date:<br/>" + DepartDate + "</li></ul></li>";
        }
        searchdetails += '<li class="lst"><h1>';
        mobSearchSummary += '<li>';
        if (lobjSearchRequest.SearchDetails.Adults != 0) {
            searchdetails += lobjSearchRequest.SearchDetails.Adults + "Adult<br>";
            mobSearchSummary += lobjSearchRequest.SearchDetails.Adults + "Adult";
        }
        if (lobjSearchRequest.SearchDetails.Childrens != 0) {
            searchdetails += lobjSearchRequest.SearchDetails.Childrens + "Child<br>";
            mobSearchSummary += lobjSearchRequest.SearchDetails.Childrens + "Child";
        }
        if (lobjSearchRequest.SearchDetails.Infants != 0) {
            searchdetails += lobjSearchRequest.SearchDetails.Infants + "Infant<br>";
            mobSearchSummary += lobjSearchRequest.SearchDetails.Infants + "Infant";
        }
        searchdetails += '</h1></li>';
        mobSearchSummary += '</li>';
        //searchdetails += "<p>Total Flight(s) Found: " + Counts.toString() + "</p>";
        //$("#CP_lblNoofFlight").text(Counts.toString());


        $("#mobSearchSummary").html(mobSearchSummary);
        $("#LabelYourSearchDetails").html(searchdetails);
    });

    GetFlightData = function () {
        $http({
            url: "FlightList.aspx/FlightLoadData",
            dataType: 'json',
            method: 'POST',
            data: '',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .success(function (response) {
                $("#CP_lblNoofFlight").text(response.d[5]);
                if (response.d[2] != "") {
                    $scope.results = $.parseJSON(response.d[2]);
                    $('#resultInterNational').show();
                    $('#DomesticOneWay').hide();
                    $('#DomesticTwoWay').hide();
                    $('#mainList').show();
                }
                else if (response.d[1] != "") { // domestic return flight
                    $scope.resultsReturnData = $.parseJSON(response.d[1]);
                    $scope.resultsOnwardData = $.parseJSON(response.d[0]);

                    if (response.d[1] != "[]" && response.d[0] != "[]") {
                        showTripSummaryOnward($scope.resultsOnwardData[0].SequenceNo);
                        showTripSummaryReturn($scope.resultsReturnData[0].SequenceNo);
                        $('#Trip_Summary_Main').show();
                    } else {
                        $('#Trip_Summary_Main').hide();
                    }

                    $('#DomesticTwoWay').show();
                    $('#DomesticOneWay').hide();
                    $('#resultInterNational').hide();
                    $('#mainList').show();
                }
                else { //domestic onwardflights
                    $scope.resultsOnwardData = $.parseJSON(response.d[0]);
                    $('#DomesticTwoWay').hide();
                    $('#DomesticOneWay').show();
                    $('#resultInterNational').hide();
                    $('#mainList').show();
                }
                if (response.d[4].toString() == 'True') {
                    $("#LoadNext").show();

                } else {
                    $("#LoadNext").hide();
                }
            })
           .error(function (error) {
               alert(error);
           });
    }
    GetFilterFlightData = function () {
        $http({
            url: "FlightList.aspx/FilteringFlightLoadData",
            dataType: 'json',
            method: 'POST',
            data: '',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .success(function (response) {
                $("#CP_lblNoofFlight").text(response.d[5]);
                if (response.d[2] != "") {
                    $scope.results = $.parseJSON(response.d[2]);
                    $('#International').show();
                    $('#DomesticOneWay').hide();
                    $('#DomesticTwoWay').hide();
                }
                else if (response.d[1] != "") { // domestic return flight
                    $scope.resultsReturnData = $.parseJSON(response.d[1]);
                    $scope.resultsOnwardData = $.parseJSON(response.d[0]);

                    if (response.d[1] != "[]" && response.d[0] != "[]") {
                        showTripSummaryOnward($scope.resultsOnwardData[0].SequenceNo);
                        showTripSummaryReturn($scope.resultsReturnData[0].SequenceNo);
                    } else {
                        $('#Trip_Summary_Main').hide();
                    }

                    $('#DomesticTwoWay').show();
                    $('#DomesticOneWay').hide();
                    $('#International').hide();
                }
                else { //domestic onwardflights
                    $scope.resultsOnwardData = $.parseJSON(response.d[0]);
                    $('#DomesticTwoWay').hide();
                    $('#DomesticOneWay').show();
                    $('#International').hide();
                }
                if (response.d[4].toString() == 'True') {
                    $("#LoadNext").show();
                } else {
                    $("#LoadNext").hide();
                }
            })
           .error(function (error) {
               alert(error);
           });
    }
    $http({
        url: "FlightList.aspx/PaintAirlines",
        dataType: 'json',
        method: 'POST',
        data: '',
        headers: {
            "Content-Type": "application/json"
        }
    })
     .success(function (msg) {
         $("#DivAirLinesList").html(msg.d);
     })
    $http({
        url: "FlightList.aspx/PaintStops",
        dataType: 'json',
        method: 'POST',
        data: '',
        headers: {
            "Content-Type": "application/json"
        }
    })
     .success(function (msg) {
         $(".divStops").html(msg.d);
     })

    $http({
        url: "FlightList.aspx/GetFilterData",
        dataType: 'json',
        method: 'POST',
        data: '',
        headers: {
            "Content-Type": "application/json"
        }
    })
 .success(function (msg) {
     objFilterRange = $.parseJSON(msg.d);
     $scope.priceSlider = {
         range: {
             min: objFilterRange.MinPoints,
             max: objFilterRange.MaxPoints
         },
         minPrice: objFilterRange.MinPoints,
         maxPrice: objFilterRange.MaxPoints

     };
     $scope.priceSliderStop = function () {
         return FilterSlider(1, $scope.priceSlider.minPrice, $scope.priceSlider.maxPrice);
     }

     $scope.durationSlider = {
         range: {
             min: objFilterRange.MinDuration,
             max: objFilterRange.MaxDuration
         },
         minduration: objFilterRange.MinDuration,
         maxduration: objFilterRange.MaxDuration

     };
     $scope.durationSliderStop = function () {
         return FilterSlider(3, $scope.durationSlider.minduration, $scope.durationSlider.maxduration);
     }
 })
    $http({
        url: "FlightList.aspx/ModifySearchData",
        dataType: 'json',
        method: 'POST',
        data: '',
        headers: {
            "Content-Type": "application/json"
        }
    })
     .success(function (msg) {
         var objModifySerach = $.parseJSON(msg.d[0]);
         var DepartDate = msg.d[1];
         var ArrivalDate = msg.d[2];
         $("#CP_textBoxFrom").val(objModifySerach.SearchDetails.OriginLocation + ',' + objModifySerach.SearchDetails.DepCode.AirportName + ',' + objModifySerach.SearchDetails.DepCode.City + ',' + objModifySerach.SearchDetails.DepCountryName);
         $("#CP_textBoxTo").val(objModifySerach.SearchDetails.DestinationLocation + ',' + objModifySerach.SearchDetails.ArrCode.AirportName + ',' + objModifySerach.SearchDetails.ArrCode.City + ',' + objModifySerach.SearchDetails.ArrCountryName);
         $("#CP_txtAirline").val(objModifySerach.SearchDetails.FlightType);
         $("#CP_DropDownListAdult option[value='" + objModifySerach.SearchDetails.Adults.toString() + "']").attr('selected', 'selected');
         $("#CP_DropDownListChild option[value='" + objModifySerach.SearchDetails.Childrens.toString() + "']").attr('selected', 'selected');
         $("#CP_DropDownListInfant option[value='" + objModifySerach.SearchDetails.Infants.toString() + "']").attr('selected', 'selected');
         $("#CP_dropDownListEconomy option[value='" + objModifySerach.SearchDetails.Cabin.toString() + "']").attr('selected', 'selected');
         $("#CP_chkboxRedeem").prop("checked", true);
         $("#CP_txtDepart").val(DepartDate);
         //$("#chkinchkout").append("Departure Date: " + DepartDate + " | Return Date: " + ArrivalDate + " | Class: " + objModifySerach.SearchDetails.Cabin.toString());
         if (objModifySerach.SearchDetails.IsReturn.toString() == 'true') {
             $("#CP_txtReturn").val(ArrivalDate);
             $("#CP_hdnreturnDate").val(DepartDate);
             $("#CP_rdbRoundTrip").prop("checked", true);
             $("#CP_rdbOneWay").prop("checked", false);
             $("#retnli").show();
         } else {
             $("#CP_rdbRoundTrip").prop("checked", false);
             $("#CP_rdbOneWay").prop("checked", true);
             $("#retnli").hide();
         }
     })
    $scope.departureSlider = {
        range: {
            min: 0,
            max: 1440
        },
        minduration: 0,
        maxduration: 1440

    };
    $scope.Digits = function (nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    $scope.departureSliderStop = function () {
        return FilterSlider(4, $scope.departureSlider.minduration, $scope.departureSlider.maxduration);
    }
    $scope.arrivalSlider = {
        range: {
            min: 0,
            max: 1440
        },
        minduration: 0,
        maxduration: 1440
    };
    //$scope.showLoading = function() {
    //    $("#LoadingResult").show();
    //}
    //$scope.hideImage = function () {
    //    $("#LoadingResult").slideUp(1000);
    //    return true;
    //}
    $scope.arrivalSliderStop = function () {
        return FilterSlider(5, $scope.arrivalSlider.minduration, $scope.arrivalSlider.maxduration);
    };
    $scope.getdetails = function (val) {
        $(".detseq_" + val).slideToggle();
        return false;
    }
    $scope.BookDomesticOneWay = function (pstrSequenceNo) {
        $http({
            url: "FlightLBMSServices.aspx/BookNowClickDomesticOneWay",
            dataType: 'json',
            method: 'POST',
            data: "{'pstrSequenceNo':'" + pstrSequenceNo + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        })
     .success(function (msg) {
         if (msg.d)
             window.location = "FlightPassenger.aspx";
         else
             window.location = "Login.aspx";
     })
    }

    $scope.BookDomestic = function (pstrSequenceNo) {
        var OnwardSequenceNo = $("#hdnOnwardSelectedFlight").val();
        var ReturnSequenceNo = $("#hdnReturnSelectedFlight").val();
        $http({
            url: "FlightLBMSServices.aspx/BookNowClickDomestic",
            dataType: 'json',
            method: 'POST',
            data: "{'pstrOnwardSequenceNo':'" + OnwardSequenceNo + "','pstrReturnSequnceNo':'" + ReturnSequenceNo + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        })
     .success(function (msg) {
         if (msg.d)
             window.location = "FlightPassenger.aspx";
         else
             window.location = "Login.aspx";
     })
    }
    $scope.empty = function (id) {
        $("#CP_" + id).val('');
        $("#CP_" + id).removeClass("schfldserr");
    }
    $scope.BookNowClick = function (pstrSequenceNo) {
        $http({
            url: "FlightLBMSServices.aspx/BookNowClick",
            dataType: 'json',
            method: 'POST',
            data: "{'pstrSequenceNo':'" + pstrSequenceNo + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        })
     .success(function (msg) {
         if (msg.d)
             window.location = "FlightPassenger.aspx";
         else
             window.location = "Login.aspx";
     })
    }
    $scope.LoadNext = function () {
        GetFlightData();
    }
    $scope.showModifyFlight = function () {
        $("#modifyDetails").slideToggle();

    }
    GetFlightData();
    $scope.showOneway = function () {
        debugger;
        $("#retnli").hide();
        $("#hdntrip").val('false');
        $("#oneway").addClass('rdbselect');
        $("#return").removeClass('rdbselect');

    }
    $scope.showReturn = function () {
        $("#retnli").show();
        $("#hdntrip").val('true');
        $("#oneway").removeClass('rdbselect');
        $("#return").addClass('rdbselect');
    }
    
    showTripSummaryReturn = function (sequenceNo) {
        showTripSummaryDetails("Return", sequenceNo);
    }
    showTripSummaryOnward = function (sequenceNo) {
        showTripSummaryDetails("Onward", sequenceNo);
    }
    $scope.FlightValidation = function () {
       
        //var msg = "";
        var cnt = 0;
        if (($("#CP_textBoxFrom").val() == '' || $("#CP_textBoxFrom").val() == null) || $("#CP_textBoxFrom").val() == "Enter City or Airport" || $("#CP_textBoxFrom").val() == "*Required" ) {
            // msg = "Please Provide The Departure of the Flight.";
            $("#CP_textBoxFrom").addClass("schfldserr");
            $("#CP_textBoxFrom").val("*Required");
            cnt++;
        }
        if (($("#CP_textBoxTo").val() == '' || $("#CP_textBoxTo").val() == null) || $("#CP_textBoxTo").val() == "Enter City or Airport" || $("#CP_textBoxTo").val() == "*Required") {
            //  msg = "Please Provide The Destination of the Flight.";
            $("#CP_textBoxTo").addClass("schfldserr");
            $("#CP_textBoxTo").val("*Required");
            cnt++;
        }
        if (($("#CP_txtDepart").val() == '' || $("#CP_txtDepart").val() == null || $("#CP_txtDepart").val() == 'dd/mm/yyyy' || $("#CP_txtDepart").val() == "*Required")) {
            //msg = "Please Provide The Departure Date.";
            $("#CP_txtDepart").addClass("schfldserr");
            $("#CP_txtDepart").val("*Required");
            cnt++;
        }
        if ($("#hdntrip").val() == 'true' && ($("#CP_txtReturn").val() == '' || $("#CP_txtReturn").val() == null) || $("#CP_txtReturn").val() == 'dd/mm/yyyy' || $("#CP_txtReturn").val() == "*Required") {
            //msg = "Please Provide The Return Date";
            $("#CP_txtReturn").addClass("schfldserr");
            $("#CP_txtReturn").val("*Required");
            cnt++;
        }
        if (parseInt($("#CP_lblInfantCount").text()) > parseInt($("#CP_lblAdultCount").text())) {
            //msg = "Number of infants is more than adults.";
            $("#CP_lblInfantCount").addClass("schfldserr");
            cnt++;
        }
        //if (msg.length > 0) {
        //    $("#requiredValidation")[0].innerHTML = msg;
        //    showdiv('divrequiredValidation');
        //    setTimeout(blank('divrequiredValidation'), 2000);
        //    return false;
        //}
        //else {
        //    return clicktoflightsearch();
        //}
        if (cnt == 0) {
            return clicktoflightsearch();
        }
    }
    clicktoflightsearch = function () {
        var strcityfrom = "cityfrom=" + $("#CP_textBoxFrom").val().split(',')[2] + "&";
        var strcityto = "cityto=" + $("#CP_textBoxTo").val().split(',')[2] + "&";
        var strFrom = "departure=" + $("#CP_textBoxFrom").val().split(',')[0] + "&";
        var strTo = "arrival=" + $("#CP_textBoxTo").val().split(',')[0] + "&";
        var strDepartDate = "departuredate=" + $("#CP_txtDepart").val() + "&";
        var strReturnDate = "arrivaldate=" + $("#CP_txtReturn").val() + "&";
        //var isReturn = "isReturn=" + $("#CP_rdbRoundTrip").is(":checked").toString() + "&";
        var isReturn = "isReturn=" + $("#hdntrip").val().toString() + "&";
        var strAirlinePrefernce = "airline=" + $("#CP_txtAirline").val() + "&";
        var strAirlineIATACode = "airlineIATACode=" + $("#CP_hdnCarrier").val() + "&";
        var strAdultNo = "adult=" + $("#CP_DropDownListAdult").val() + "&";
        var strChildNo = "child=" + $("#CP_DropDownListChild").val() + "&";
        var strInfantNo = "infant=" + $("#CP_DropDownListInfant").val() + "&";
        var strEconomy = "economy=" + $("#CP_dropDownListEconomy").val();
        var queryString = strcityfrom + strcityto + strFrom + strTo + strDepartDate + strReturnDate + isReturn + strAirlinePrefernce + strAirlineIATACode + strAdultNo + strChildNo + strInfantNo + strEconomy;
        window.location = "SearchPage.aspx?" + queryString;
        return false;
    }
});

app.filter('hourMinFilter', function () {
    return function (value) {
        var h = parseInt(value / 60);
        var m = parseInt(value % 60);

        var hStr = (h >= 0) ? h + 'hr' : '';
        var mStr = (m > 0) ? m + 'min' : '';
        var glue = (hStr && mStr) ? ' ' : '';

        return hStr + glue + mStr;
    };
});

function FilterAirlines(Airline) {
    var tag = Airline.toString().replace(/\s+/g, '');
    tag = tag.toString().replace(/["'()]/g, '');

    var ischecked = $("#chk" + tag).is(":checked").toString();
    if (tag == 'SelectAll') {
        var $checkBoxList = $("#DivAirLinesList input[type='checkbox']");
        for (var i = 0; i < $checkBoxList.length; i++) {
            if (ischecked == 'false') {
                $checkBoxList.eq(i).prop("checked", false);
            } else {
                $checkBoxList.eq(i).prop("checked", true);
            }
        }
    }

    $.ajax({
        type: 'POST',
        url: 'FlightList.aspx/FilterAirlines',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "{'pstrAirline':'" + Airline.toString() + "','pstrischecked':'" + ischecked.toString() + "'}",
        success: function (msg) {
            GetFilterFlightData();
        }
    });

}
function FilterNoOfStops(id) {
    var ischecked = $("#chkStops" + id).is(":checked").toString();
    $.ajax({
        type: 'POST',
        url: 'FlightList.aspx/FilterNoOfStops',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "{'pstrid':'" + id.toString() + "','pstrischecked':'" + ischecked.toString() + "'}",
        success: function (msg) {
            GetFilterFlightData();
        }
    });

}
function FilterSlider(id, MinVal, MaxVal) {
    $.ajax({
        type: 'POST',
        url: 'FlightList.aspx/FilterSlider',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "{'pstrid':'" + id.toString() + "','pstrMinVal':'" + MinVal.toString() + "','pstrMaxVal':'" + MaxVal.toString() + "'}",
        success: function (msg) {
            GetFilterFlightData();
        }
    });
}

var flightDepAmt = 0;
var flightRetAmt = 0;
function showTripSummaryDetails(summaryType, sequenceNo) {
    $.ajax({
        type: 'POST',
        url: 'FlightList.aspx/ShowTripSummary',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "{'pstrSummaryType':'" + summaryType.toString() + "'" + "," + "'pstrSequenceNo':'" + sequenceNo.toString() + "'}",
        success: function (msg) {
            var strOnwardsSummary = "";
            var strOnwardFlightInfo = "";
            var strReturnFlightInfo = "";
            var strReturnSummary = "";
            var Onwardindex = 0;
            var Returndindex = 0;
            if (summaryType == "Onward") {
                var flights = msg.d[0];
                $("#hdnOnwardSelectedFlight").val(sequenceNo);
            }
            else {
                var flights = msg.d[0];
                $("#hdnReturnSelectedFlight").val(sequenceNo);
            }
            flight = $.parseJSON(flights);
            var strSummary = "";

            var strTotal = "";
            if (summaryType == "Onward") {
                for (count = 0; count < flight.ListOfFlightSegments.length; count++) {
                    if (flight.ListOfFlightSegments.length > 1) {
                        if (Onwardindex == 0) {
                            Onwardindex = 1;
                            lastcount = flight.ListOfFlightSegments.length - 1;
                            strOnwardsSummary += "<ul class='fL'><li><label class='trpsum-city'>" + flight.ListOfFlightSegments[count].DepartureAirField.City + " (" + flight.ListOfFlightSegments[count].OriginLocation + ")";
                            strOnwardsSummary += "</label><span class='trpsum-To'>to</span><label class='trpsum-city'>" + flight.ListOfFlightSegments[lastcount].ArrivalAirField.City + " (" + flight.ListOfFlightSegments[lastcount].DestinationLocation + ")" + "</label></li></ul><div class='fR'><ul><li><label class='trpsum-miles'>" + flight.FareDetails.TotalPoints + "</label></li><li><label class='trpsum-miles-type'>Happy Points</label></li></ul></div>"
                        }
                    } else {
                        strOnwardsSummary += "<ul class='fL'><li><label class='trpsum-city'>" + flight.ListOfFlightSegments[count].DepartureAirField.City + " (" + flight.ListOfFlightSegments[count].OriginLocation + ")";
                        strOnwardsSummary += "</label><span class='trpsum-To'>to</span><label class='trpsum-city'>" + flight.ListOfFlightSegments[count].ArrivalAirField.City + " (" + flight.ListOfFlightSegments[count].DestinationLocation + ")" + "</label></li></ul><div class='fR'><ul><li><label class='trpsum-miles'>" + flight.FareDetails.TotalPoints + "</label></li><li><label class='trpsum-miles-type'>Happy Points</label></li></ul></div>"
                    }

                    strOnwardFlightInfo += "<div class='trpsum-flt-logo'><img src=" + flight.ListOfFlightSegments[count].Carrier.CarrierLogoPath + " /></div><div class='fltsumm-col-1'><ul class='fltsumm-dtrow'><li><span class='trpsum-fltname robotbold'>" + flight.ListOfFlightSegments[count].Carrier.CarrierName + "</span></li><li><span class='trpsum-fltcode'>(" + flight.ListOfFlightSegments[count].Carrier.CarrierCode + flight.ListOfFlightSegments[count].FlightNo + ")</span></li></ul></div><div class='fltsumm-col-2'><ul class='fltsumm-dtrow'><li><span class='trpsum-c-code robotbold'>" + flight.ListOfFlightSegments[count].OriginLocation + "</span>&rarr;<span class='trpsum-c-code robotbold'>" + flight.ListOfFlightSegments[count].DestinationLocation + "</span></li><li><label class='trpsum-dprtime'>" + flight.ListOfFlightSegments[count].DisplayDepartureTime + "</label><label class='trpsum-arrvtime'>" + flight.ListOfFlightSegments[count].DisplayArrivalTime + "</label></li></ul></div><div class='fltsumm-col-3 alR'><ul class='fltsumm-dtrow'><li><label class='trpsum-dprtdate robotbold'>" + flight.ListOfFlightSegments[count].DisplayDepartureDate + "</label></li><li>" + flight.ListOfFlightSegments[count].TotalDurationHrs + " hrs " + flight.ListOfFlightSegments[count].TotalDurationMins + " mins</li><li>1 stop</li></ul></div>";
                }
                $("#divOnwardFlightDetails").html(strOnwardsSummary);
                $("#divOnwardFlightInfo").html(strOnwardFlightInfo);
            }
            if (summaryType == "Return") {
                for (count = 0; count < flight.ListOfFlightSegments.length; count++) {
                    if (flight.ListOfFlightSegments.length > 1) {
                        if (Returndindex == 0) {
                            Returndindex = 1;
                            {
                                lastcount = flight.ListOfFlightSegments.length - 1;
                                strReturnSummary += "<ul class='fL'><li><label class='trpsum-city'>" + flight.ListOfFlightSegments[count].DepartureAirField.City + " (" + flight.ListOfFlightSegments[count].OriginLocation + ")";
                                strReturnSummary += "</label><span class='trpsum-To'>to</span><label class='trpsum-city'>" + flight.ListOfFlightSegments[lastcount].ArrivalAirField.City + " (" + flight.ListOfFlightSegments[lastcount].DestinationLocation + ")" + "</label></li></ul><div class='fR'><ul><li><label class='trpsum-miles'>" + flight.FareDetails.TotalPoints + "</label></li><li><label class='trpsum-miles-type'>Happy Points</label></li></ul></div>"

                            }
                        }
                    } else {
                        strReturnSummary += "<ul class='fL'><li><label class='trpsum-city'>" + flight.ListOfFlightSegments[count].DepartureAirField.City + " (" + flight.ListOfFlightSegments[count].OriginLocation + ")";
                        strReturnSummary += "</label><span class='trpsum-To'>to</span><label class='trpsum-city'>" + flight.ListOfFlightSegments[count].ArrivalAirField.City + " (" + flight.ListOfFlightSegments[count].DestinationLocation + ")" + "</label></li></ul><div class='fR'><ul><li><label class='trpsum-miles'>" + flight.FareDetails.TotalPoints + "</label></li><li><label class='trpsum-miles-type'>Happy Points</label></li></ul></div>"
                    }
                    strReturnFlightInfo += "<div class='trpsum-flt-logo'><img src=" + flight.ListOfFlightSegments[count].Carrier.CarrierLogoPath + " /></div><div class='fltsumm-col-1'><ul class='fltsumm-dtrow'><li><span class='trpsum-fltname'>" + flight.ListOfFlightSegments[count].Carrier.CarrierName + "</span></li><li><span class='trpsum-fltcode'>(" + flight.ListOfFlightSegments[count].Carrier.CarrierCode + flight.ListOfFlightSegments[count].FlightNo + ")</span></li></ul></div><div class='fltsumm-col-2'><ul class='fltsumm-dtrow'><li><span class='trpsum-c-code'>" + flight.ListOfFlightSegments[count].OriginLocation + "</span>&rarr;<span class='trpsum-c-code'>" + flight.ListOfFlightSegments[count].DestinationLocation + "</span></li><li><label class='trpsum-dprtime'>" + flight.ListOfFlightSegments[count].DisplayDepartureTime + "</label><label class='trpsum-arrvtime'>" + flight.ListOfFlightSegments[count].DisplayArrivalTime + "</label></li></ul></div><div class='fltsumm-col-3 alR'><ul class='fltsumm-dtrow'><li><label class='trpsum-dprtdate'>" + flight.ListOfFlightSegments[count].DisplayDepartureDate + "</label></li><li>" + flight.ListOfFlightSegments[count].TotalDurationHrs + " hrs " + flight.ListOfFlightSegments[count].TotalDurationMins + " mins</li><li>1 stop</li></ul></div>";
                }
                $("#divReturnFlightDetails").html(strReturnSummary);
                $("#divReturnFlightInfo").html(strReturnFlightInfo);
            }


            if (summaryType == "Onward") {
                flightDepAmt = flight.FareDetails.TotalPoints;
            }
            else {
                flightRetAmt = flight.FareDetails.TotalPoints;
            }
            $(".fixedBot-tripSumm").show();

            strTotal += "<ul class='fR'><li><label class='trpsumm-totalmiles'>" + parseInt(flightDepAmt + flightRetAmt) + "</label></li><li><label class='trpsum-miletype'> Total Happy Points</label></li></ul>";
            $("#divTotal").html(strTotal);
            var strheight = $("#Trip_Summary_Main").height();
            $("#domesticTwoWay").css("padding-bottom", strheight + "px");
            //$(".trpsumm-totalmiles").digits();
            //$(".trpsum-miles").digits();
            //$("#divTotal").html(strSummary);
            return false;

        }
    });
}
function toggleInfo() {
    $("#DomflightInfo").slideToggle("slow");
    return false;
}