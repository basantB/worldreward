﻿$(window).scroll(function () {
    var WinWidth = $(window).width();
    if (WinWidth > 1100) {
        if ($(this).scrollTop() > 10) {
            $('#UserInfo').css({
                margin: "48px 0 20px",
                position: "fixed",
                right: "5%",
                top: "0"
            });
            //$("#modifyDetails").slideUp("1000");
            $("#fliterDetails").css({ margin: "300px 0 0px", });
            $(".pgcol2").css({ width: "24.70%" });
        }
        else {
            $('#UserInfo').css({
                margin: "0px 0 20px 0", position: "inherit", right: "",
                top: ""
            });
            //$("#modifyDetails").slideDown("1000");
            $("#fliterDetails").css({ margin: "0", });
            $(".pgcol2").css({ width: "auto" });
        }
        $("#modifyDetails").slideUp("1000");
    }
});

function minimumtime() {
    var currentDate = new Date();
    currentDate.setHours(currentDate.getHours() + 48, currentDate.getMinutes(), currentDate.getSeconds(), currentDate.getMilliseconds());
    return currentDate;
}
Array.prototype.contains = function (needle) {
    for (i in this) {
        if (this[i] == needle) return true;
    }
    return false;
}
function OrderBy(obj, direction) {
    var old_Car_list = [];
    old_Car_list = $("#Cars").find(".Car-block");
    CarDetails = $("#divCarDetails");
    CarDetailsFade = $("#CarFade");

    var strList = "";

    $("#hdnOrderByObject").val(obj);
    $("#hdnOrderByDirection").val(direction);

    var new_Car_list = [];
    new_Car_list = old_Car_list.sort(SortCar);

    for (var i = 0; i < new_Car_list.length; i++) {
        strList += "<div class='Car-block'>" + new_Car_list.eq(i).html() + "</div>";
    }

    strList += "<div id='divCarDetails' class='hidecontent'>" + CarDetails.html() + "</div>";
    strList += "<div id='CarFade' class='Hotel_Details_back'>" + CarDetailsFade.html() + "</div>";
    $("#Cars").html(strList);
    //filtersData.ShowHideRows();
    showImage();
    return false;
}
function SortCar(a, b) {

    var Car_A = GetJson_A(a);
    var Car_B = GetJson_B(b);

    if ($("#hdnOrderByObject").val() == 'duration') {
        if ($("#hdnOrderByDirection").val() == 'up') {
            return Car_A.Vehicle[0].Name.toString().toLowerCase() > Car_B.Vehicle[0].Name.toString().toLowerCase() ? 1 : -1;
        }
        if ($("#hdnOrderByDirection").val() == 'down') {
            return Car_A.Vehicle[0].Name.toString().toLowerCase() < Car_B.Vehicle[0].Name.toString().toLowerCase() ? 1 : -1;
        }
    } else if ($("#hdnOrderByObject").val() == 'departuretime') {
        if ($("#hdnOrderByDirection").val() == 'up') {
            return Car_A.Vehicle[0].seats > Car_B.Vehicle[0].seats ? 1 : -1;
        }
        if ($("#hdnOrderByDirection").val() == 'down') {
            return Car_A.Vehicle[0].seats < Car_B.Vehicle[0].seats ? 1 : -1;
        }
    } else if ($("#hdnOrderByObject").val() == 'miles') {
        if ($("#hdnOrderByDirection").val() == 'up') {
            return Car_A.Price[0].TotalPoints > Car_B.Price[0].TotalPoints ? 1 : -1;
        }
        if ($("#hdnOrderByDirection").val() == 'down') {
            return Car_A.Price[0].TotalPoints < Car_B.Price[0].TotalPoints ? 1 : -1;
        }
    } else return 1;


}
function GetJson_A(obj) {
    var Car_A = $(obj).find(".jsondata input[type='hidden']").val();
    if (Car_A != undefined) {
        Car_A = Car_A.replace(" \ ", " ");
        Car_A = $.parseJSON(Car_A);
    }
    return Car_A;
}
function GetJson_B(obj) {
    var Car_B = $(obj).find(".jsondata input[type='hidden']").val();
    if (Car_B != undefined) {
        Car_B = Car_B.replace(" \ ", " ");
        Car_B = $.parseJSON(Car_B);
    }
    return Car_B;
}
$(document).ready(function () {

    $("#CP_Image1").click(function () {
        $("#divCarType").hide();
        $("#CP_Image1").hide();
        $("#hideCarType").show();
    });
    $("#CP_Image2").click(function () {
        $("#divCarType").show();
        $("#hideCarType").hide();
        $("#CP_Image1").show();
    });
    $("#CP_Image7").click(function () {
        $("#divCarClass").hide();
        $("#CP_Image7").hide();
        $("#hideCarClass").show();
    });
    $("#CP_Image6").click(function () {
        $("#divCarClass").show();
        $("#CP_Image7").show();
        $("#hideCarClass").hide();
    });

    $("#CP_Image3").click(function () {
        $("#divCarTransmission").hide();
        $("#hideCarTransmission").show();
        $("#CP_Image3").hide();
    });
    $("#CP_Image8").click(function () {
        $("#divCarTransmission").show();
        $("#hideCarTransmission").hide();
        $("#CP_Image3").show();
    });
    $("#CP_Image4").click(function () {
        $("#divCarAirCondition").hide();
        $("#hideCarAirCondition").show();
        $("#CP_Image4").hide();
    });
    $("#CP_Image9").click(function () {
        $("#divCarAirCondition").show();
        $("#hideCarAirCondition").hide();
        $("#CP_Image4").show();
    });
});
function gotoBooking(obj) {

    var pstrRefId = obj;
    var LocationId = $(".hdnLocationId").find("input[type='hidden']").val()
    $.ajax({
        url: 'CarList.aspx/IsMemberLoggedIn',
        type: 'POST',  // or get
        contentType: 'application/json; charset =utf-8',
        data: "{'pstrRefId':'" + pstrRefId.toString() + "'}",
        dataType: 'json',
        success: function (data) {
            try {
                var newData = data.d;

                if (newData == true) {
                    window.location.href = 'CarDetails.aspx';
                }
                else {
                    window.location.href = 'Login.aspx';
                }
            }
            catch (e) {
                //      alert(e);
                return false;
            }
            return true;
            // do procedure if success 
        },
        error: function (errmsg) {
            // do procedure if fail
            // may be send a message to the server side to display a message that     shows session timeout
        }
    });
}
//Code Done by javed sir
$(function () {
    $("#CP_CarTextBoxCheckin").datepicker({
        startDate: new Date(),
        format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {
        $("#CP_CarTextBoxCheckin").removeClass("schfldserr");
        $.ajax({
            type: 'POST',
            url: 'Index.aspx/GetPickupTime',
            //url: 'CarSearch.aspx/GetPickupTime',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'Pickupdate':'" + $("#CP_CarTextBoxCheckin").val() + "','LocationId':'" + $("#CP_DDLLocation").find('option:selected').prop("value") + "'}",
            cache: false,
            success: function (data) {
                //show Timepicker
                //debugger;
                $("#CP_ddlFromTime").empty();
                var dateList = data.d;
                var Time = -1
                var hdnddlFromTime
                var count = 0;

                $("#CP_CarValidationError").hide();
                if (data.d != '000000000000000000000000') {
                    var initialized = 0;
                    for (var count = 0; count <= dateList.length; count++) {
                        var times;
                        Time++;
                        if (data.d.charAt(count) == '1') {
                            if (Time < 10) {
                                $("#CP_ddlFromTime").append($("<option>" + "0" + Time + "</option>"));
                                times = "0" + Time;
                            }
                            else {
                                $("#CP_ddlFromTime").append($("<option>" + Time + "</option>"));
                                times = Time;
                            }
                            if (initialized == 0) {
                                hdnddlFromTime = times;
                                initialized = 1;
                            }
                            else {
                                hdnddlFromTime = hdnddlFromTime + "," + times;
                            }
                        }
                    }
                    document.getElementById('CP_ddlFromTime').selectedIndex = 0;
                    //$("#CP_DivFromHour").html($("#CP_ddlFromTime option:first-child").val());
                }
                else {
                    $("#CP_CarValidationError").show();
                    $("#CP_CarValidationError")[0].innerHTML = 'Car is not Available for this Date';
                }
                $("#CP_hdnddlFromTime").val(hdnddlFromTime);
                //$("#CP_ddlFromTime").selectric('refresh');
            },
            error: function (errmsg) {

                alert(errmsg.d);

            }
        });

        $("#CP_CarTextBoxCheckout").val('');
        $("#CP_CarTextBoxCheckout").datepicker({
            startDate: e.date,
            format: "dd/mm/yyyy"
        }).on('changeDate', function (e) {
            $("#CP_CarTextBoxCheckout").removeClass("schfldserr");
            if ($('#CP_droploction').attr('checked')) {
                locationId = $("#CP_DDLLocation").find('option:selected').prop("value");
            }
            else {
                locationId = $("#CP_ddlDropLocation").find('option:selected').prop("value");
            }
            getDrophourDetails($("#CP_CarTextBoxCheckout").val(), locationId);

        });
        //  getDrophourDetails(e.date, $('#CP_txtCountry').val());
        return false;
    });
    return false;
});
var app = angular.module("Danamoncar", ['ui-rangeSlider', 'ngAnimate', 'ui.bootstrap'])
//var app = angular.module('AlRayan', ['ngAnimate', 'ui.bootstrap']);
app.controller("MainCtrl", function ($scope, $http, $compile) {
    $scope.empty = function (id) {
        $("#CP_" + id).val('');
        $("#CP_" + id).removeClass("schfldserr");
    }
    GetCarList = function () {
        $http({
            url: "CarList.aspx/SetCarTemplate",
            dataType: 'json',
            method: 'POST',
            data: '',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .success(function (response) {
                $('#CP_hdnCarFilterRange').val(response.d[2]);
                //$('#spnSearchdetail')[0].innerHTML = response.d[3]
                if (response.d[1] != "") {
                    var CarList = $.parseJSON(response.d[1]);
                    //$("#carinfo").html(response.d[3] + "<p>Total Car(s) Found: " + CarList.length + "</p>");
                    $("#carinfo").html(response.d[3]);
                    $("#mobcarinfo").html(response.d[4]);
                    $scope.results = CarList;
                    $("#carmain").show();
                }
            })
           .error(function (error) {
               alert(error);
           });
    }
    MapModify = function () {
        {
            $http({
                url: "CarList.aspx/ModifySearchData",
                dataType: 'json',
                method: 'POST',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .success(function (msg) {
                    var objModifySerachRequest = $.parseJSON(msg.d[0]);
                    objModifySerachResponse = $.parseJSON(msg.d[1]);

                    $('#CP_txtCountry').val(objModifySerachRequest.SearchRequest.PickUp[0].Location[0].country);
                  //  BindGlobalCountryList($('#CP_txtCountry'), $('#CP_DDlCity'), $('#CP_DDLLocation'), objModifySerachRequest.SearchRequest.PickUp[0].Location[0].country);
                    BindGlobalCityList($('#CP_DDlCity'), $('#CP_DDLLocation'), objModifySerachRequest.SearchRequest.PickUp[0].Location[0].country, objModifySerachRequest.SearchRequest.PickUp[0].Location[0].city,"true");
                    BindGlobalLocationList(objModifySerachRequest.SearchRequest.PickUp[0].Location[0].country, objModifySerachRequest.SearchRequest.PickUp[0].Location[0].city, $('#CP_DDLLocation'), $.trim(objModifySerachRequest.SearchRequest.PickUp[0].Location[0].locName), "true")

                    BindDropOffCountry($('#CP_ddlDropCountry'), $('#CP_ddlDropCity'), $('#CP_ddlDropLocation'), objModifySerachRequest.SearchRequest.DropOff[0].Location[0].country, "true");
                    BindDropOffCity($('#CP_ddlDropCity'), $('#CP_ddlDropLocation'), objModifySerachRequest.SearchRequest.DropOff[0].Location[0].country, objModifySerachRequest.SearchRequest.DropOff[0].Location[0].city, "true");
                    DropoffCityChange(objModifySerachRequest.SearchRequest.DropOff[0].Location[0].country, objModifySerachRequest.SearchRequest.DropOff[0].Location[0].city, $('#CP_ddlDropLocation'), $.trim(objModifySerachRequest.SearchRequest.DropOff[0].Location[0].locName), "true")

                    if (objModifySerachRequest.SearchRequest.DropOff[0].Location[0].id == objModifySerachRequest.SearchRequest.PickUp[0].Location[0].id) {

                        $('#CP_droploction').attr('checked', true);
                        $('.divDropContainer').hide();
                    }
                    else {
                        $('#CP_droploction').prop('checked', false);
                        $('.dropDetails').show();
                    }


                    var splitFromTime = msg.d[2].split(',');
                    var splitToTime = msg.d[3].split(',');

                    $("#CP_hdnddlFromTime").val(splitFromTime);
                    $("#CP_hdnddlToTime").val(splitToTime);

                    for (var i = 0; i < splitFromTime.length; i++) {
                        splitFromTime[i] = splitFromTime[i].replace(/^\s*/, "").replace(/\s*$/, "");
                        if (objModifySerachRequest.SearchRequest.PickUp[0].Date[0].hour == splitFromTime[i]) {
                            $('#CP_ddlFromTime').append('<option selected="selected">' + splitFromTime[i] + '</option>');
                        }
                        else {
                            $('#CP_ddlFromTime').append('<option>' + splitFromTime[i] + '</option>');
                        }
                    }

                    for (var i = 0; i < splitToTime.length; i++) {
                        splitToTime[i] = splitToTime[i].replace(/^\s*/, "").replace(/\s*$/, "");
                        if (objModifySerachRequest.SearchRequest.DropOff[0].Date[0].hour == splitToTime[i]) {
                            $('#CP_ddlDropFromTime').append('<option selected="selected">' + splitToTime[i] + '</option>');
                        }
                        else {
                            $('#CP_ddlDropFromTime').append('<option>' + splitToTime[i] + '</option>');
                        }
                    }

                    $('#CP_CarTextBoxCheckin').val(objModifySerachRequest.SearchRequest.PickUp[0].Date[0].day + "/" + objModifySerachRequest.SearchRequest.PickUp[0].Date[0].month + "/" + objModifySerachRequest.SearchRequest.PickUp[0].Date[0].year)

                    $('#CP_ddlMinFrom').val(objModifySerachRequest.SearchRequest.PickUp[0].Date[0].minute);
                    $('#CP_CarTextBoxCheckout').val(objModifySerachRequest.SearchRequest.DropOff[0].Date[0].day + "/" + objModifySerachRequest.SearchRequest.DropOff[0].Date[0].month + "/" + objModifySerachRequest.SearchRequest.DropOff[0].Date[0].year)
                    $('#CP_ddlDropMinFrom').val(objModifySerachRequest.SearchRequest.DropOff[0].Date[0].minute);
                    FilterCarList();
                    //$('#chkinchkout').append("Pick Up Date: " + objModifySerachRequest.SearchRequest.PickUp[0].Date[0].day + "/" + objModifySerachRequest.SearchRequest.PickUp[0].Date[0].month + "/" + objModifySerachRequest.SearchRequest.PickUp[0].Date[0].year + " | Pick Up Time:" + objModifySerachRequest.SearchRequest.PickUp[0].Date[0].hour + " " + objModifySerachRequest.SearchRequest.PickUp[0].Date[0].minute + " | Drop Off Date: " + objModifySerachRequest.SearchRequest.DropOff[0].Date[0].day + "/" + objModifySerachRequest.SearchRequest.DropOff[0].Date[0].month + "/" + objModifySerachRequest.SearchRequest.DropOff[0].Date[0].year + " | Drop Off Time: " + objModifySerachRequest.SearchRequest.DropOff[0].Date[0].hour + " " + objModifySerachRequest.SearchRequest.DropOff[0].Date[0].minute);
                })

        .error(function (error) {
            alert(error);
        });
        }
    }
    GetCarList();
    $scope.showModifyCar = function () {
        $("#modifyDetails").slideToggle();
    }
    $scope.SetCarClass = function (CarType) {
        var regExpData = '[{"RegExCollection":[{"RegExpression":"((X...)|(.V..)).*","RegExVal":"Special"},{"RegExpression":"[EM][^V].*","RegExVal":"Mini"},{"RegExpression":"[E][^V].*","RegExVal":"Economy"},{"RegExpression":"[C][^V].*","RegExVal":"Compact"},{"RegExpression":"[I][^V].*","RegExVal":"Midsize"},{"RegExpression":"[S][^V].*","RegExVal":"Standard"},{"RegExpression":"[F][^V].*","RegExVal":"Full"},{"RegExpression":"[P][^V].*","RegExVal":"Premium"},{"RegExpression":"[L][^V].*","RegExVal":"Luxury"},{"RegExpression":".*","RegExVal":"All Cars"}]}]';
        regExpData = $.parseJSON(regExpData);

        for (i = 0; i < regExpData[0].RegExCollection.length; i++) {
            var NewCarType = new RegExp(regExpData[0].RegExCollection[i].RegExpression);
            if (NewCarType.test(CarType.toString())) {
                return regExpData[0].RegExCollection[i].RegExVal.toString();
            }
        }
    }
    $scope.Digits = function (nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    $scope.SetFuelPolicy = function (fuelType) {
        if (fuelType == "0") return "Undefine";
        if (fuelType == "1") return "Full to Full";
        if (fuelType == "2") return "Prepay, no Refunds";
        if (fuelType == "3") return "Prepay, with Refunds";
        if (fuelType == "4") return "Free Tank";
    }
    $scope.SetTransmission = function (Transmissiontype) {
        if (Transmissiontype == "Manual") {
            return "Manual";
        }
        else {
            return "Automatic";
        }
    }
    $scope.SetConditioning = function (Conditioning) {
        if (Conditioning == "Yes") {
            return "AC";
        }
        else {
            return "Non-AC";
        }
    }
    $scope.SetAirport = function (Airport) {
        if (Airport == "Yes") {
            return "off airport";
        }
        else {
            return "At airport";
        }
    }
    $scope.ViewCarInfo = function (refId) {
        $http({
            url: 'CarList.aspx/GetCarTermsCondtion',
            dataType: 'json',
            method: 'POST',
            data: "{'pstrrefId':'" + refId.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .success(function (msg) {
                var carvehical = $.parseJSON(msg.d);
                var html = "";
                for (var ItermGroup = 0; ItermGroup < carvehical.RentalTermsRS.Items[0].termGroupField.length; ItermGroup++) {
                    if (carvehical.RentalTermsRS.Items[0].termGroupField[ItermGroup].Term != null) {
                        for (var Iterm = 0; Iterm < carvehical.RentalTermsRS.Items[0].termGroupField[ItermGroup].Term.length; Iterm++) {
                            html += "<div class='w95'><p><b>" + carvehical.RentalTermsRS.Items[0].termGroupField[ItermGroup].Term[Iterm].Caption + "<br /></b>";
                            html += carvehical.RentalTermsRS.Items[0].termGroupField[ItermGroup].Term[Iterm].Body + "</p></div>";
                        }
                    }
                }
                $("#divFurtherInfo").append(html);
                $("html").scrollTop(0);
                $("#divCarDetails").addClass("Car_Details_Cotent");
                $("#divCarDetails").removeClass("hidecontent");
                $("#CarFade").show();
            })
           .error(function (error) {
               alert(error);
           });
    }
    $scope.CloseCarInfo = function () {
        $("#CarFade").hide();
        $("#divCarDetails").removeClass("Car_Details_Cotent");
        $("#divCarDetails").addClass("hidecontent");
        $("#divFurtherInfo").empty();
    }
    $scope.gotoBooking = function (obj) {
        gotoBooking(obj);
    }
    $scope.onCountrySelect = function ($item, $model, $label) {
        $("#CP_DDlCity").empty();
        $("#CP_DDlCity").append("<option value='select'>Select</option>");
        $("#CP_DDLLocation").empty();
        $("#CP_DDLLocation").append("<option value='select'>Select</option>");
        $('#CP_ddlDropCountry').empty();
        $('#CP_ddlDropCountry').append("<option value='select'>Select</option>");
        $('#CP_ddlDropCity').empty();
        $('#CP_ddlDropCity').append("<option value='select'>Select</option>");
        $('#CP_ddlDropLocation').empty();
        $('#CP_ddlDropLocation').append("<option value='select'>Select</option>");
        BindGlobalCityList($('#CP_DDlCity'), $('#CP_DDLLocation'), $item, '','false');
        ClearDateTime();
        ClearDropDateTime();
    }
    $scope.CarSearch = function () {
        LocationSearchCarSearch();
    }
    $scope.CarcountryList = function (val) {
        return $http({
            url: "Index.aspx/GetAllCountryList",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                addresses.push(item);
            });
            return addresses;
        });
    };
    FilterCarList = function () {
        var FilterRange = $('#CP_hdnCarFilterRange').val();
        FilterRange = $.parseJSON(FilterRange);
        $scope.priceSlider = {
            range: {
                min: FilterRange.MinRewards,
                max: FilterRange.MaxRewards,
            },
            minPrice: FilterRange.MinRewards,
            maxPrice: FilterRange.MaxRewards,
        };
        $scope.priceSliderStop = function () {
            $scope.showImage();
        }
        var htmlcartype = ""
        for (var i = 0; i < FilterRange.CarType.length; i++) {
            htmlcartype += "<li class='cartype'><input type='checkbox' ng-click='showImage();' ng-checked='CarType' />" + FilterRange.CarType[i].toString() + "</li>";
        }
        var $cartype = $(htmlcartype).appendTo('#divCarType');
        $compile($cartype)($scope);
        var htmlcarclass = "";
        for (var i = 0; i < FilterRange.CarClass.length; i++) {
            var carCondition = "";
            if (FilterRange.CarClass[i].toString() == "Yes") {
                carCondition = "Air Conditioning";
            }
            else {
                carCondition = "Non-Air Conditioning";
            }
            htmlcarclass += "<li class='FloatingCarSearchNormal'>" + "<div id='carconidtion' style='display:none;'>" + FilterRange.CarClass[i].toString() + "</div>" + "<input type='checkbox' ng-click='showImage();' ng-checked='carclass'  id='carclass'" + FilterRange.CarClass[i].toString().replace(" ", "") + "' />" + carCondition + "</li>";
        }
        var $carclass = $(htmlcarclass).appendTo('#divCarClass');
        $compile($carclass)($scope);
        var htmlcartransmission = "";
        for (var i = 0; i < FilterRange.CarTransmission.length; i++) {
            htmlcartransmission += "<li class='cartransmission'>" + "<input type='checkbox' ng-click='showImage();' ng-checked='cartransmission'  id='cartrans" + FilterRange.CarTransmission[i].toString().replace(" ", "") + "' />" + FilterRange.CarTransmission[i].toString() + "</li>";//+ FilterRange.CarTransmission[i].toString().replace(" ", "").toLowerCase() +
        }
        //<label for='Points'></label>" + "
        var $cartransmission = $(htmlcartransmission).appendTo('#divCarTransmission');
        $compile($cartransmission)($scope);
    }
    getSlidervalues = function (value1, value2) {
        return this.getSlidervalue(value1) + "-" + this.getSlidervalue(value2)
    }
    getSlidervalue = function (value) {
        var returnvalue = "";

        var valueMinutes = value % 60;
        var valueHrs = (value - valueMinutes) / 60;
        if (valueHrs < 10) {
            returnvalue = "0" + valueHrs.toString() + ":";
        }
        else {
            returnvalue = valueHrs.toString() + ":";
        }
        if (valueMinutes == 0) {
            returnvalue += "00";
        }
        else {
            returnvalue += valueMinutes.toString();
        }
        return returnvalue;
    }
    ShowHideRows = function () {
        var TotalCar = 0;
        var FilterRange = $("#CP_hdnCarFilterRange").val();
        FilterRange = $.parseJSON(FilterRange);
        var rows = $("#carmain .carbox");
        var listOfCarType = [];
        var $carTypeCheckBoxLength = $("#divCarType input[type='checkbox']");
        var $carTypeValue = $("#divCarType .cartype");
        for (var i = 0; i < $carTypeCheckBoxLength.length; i++) {
            if ($carTypeCheckBoxLength.eq(i).is(":checked")) {
                listOfCarType.push($.trim($carTypeValue.eq(i).text()));
            }
        }
        var listOfCarClass = [];
        var $carClassCheckBoxLength = $("#divCarClass input[type='checkbox']");
        var $carClassValue = $("#divCarClass .FloatingCarSearchNormal");
        for (var i = 0; i < $carClassCheckBoxLength.length; i++) {
            if ($carClassCheckBoxLength.eq(i).is(":checked")) {
                if ($carClassValue.eq(i).text() == "YesAir Conditioning") {
                    listOfCarClass.push("AC");
                }
                else if ($carClassValue.eq(i).text() == "noNon-Air Conditioning") {
                    listOfCarClass.push("Non-AC");
                }
            }
        }


        var listOfCarTransmission = [];
        var $carTransmissionCheckBoxLength = $("#divCarTransmission input[type='checkbox']");
        var $carTransmission = $("#divCarTransmission .cartransmission");
        for (var i = 0; i < $carTransmissionCheckBoxLength.length; i++) {
            if ($carTransmissionCheckBoxLength.eq(i).is(":checked")) {
                listOfCarTransmission.push($carTransmission.eq(i).text());
            }
        }

        for (var i = 0; i < rows.length; i++) {

            var row = $(rows).eq(i);
            var carType = $(row).find("input[type='hidden']")[0].value;
            var carClass = $(row).find("input[type='hidden']")[1].value;
            var Tmp = $(row).find("input[type='hidden']")[1].value;
            var carTransmission = $(row).find("input[type='hidden']")[2].value;
            var TotalMiles = $(row).find("input[type='hidden']")[3].value;
            var hasType = listOfCarType.contains(carType);
            var hasClass = listOfCarClass.contains(carClass);
            var hasTransmission = listOfCarTransmission.contains(carTransmission);

            //var Car = $(row).find(".jsondata input[type='hidden']").val();
            //Car = $.parseJSON(Car);
            var hasMiles = (TotalMiles >= $scope.priceSlider.minPrice && TotalMiles <= $scope.priceSlider.maxPrice) ? true : false;
            // var result = hasMiles && hasType && hasClass && hasTransmission;
            var result = hasType && hasClass && hasTransmission;
            if (result) {
                $(row).show();
                TotalCar = TotalCar + 1;
            }
            else {
                $(row).hide();
            }
        }


        $("#TotalCarCount").text("TOTAL CARS FOUND - " + TotalCar);

        if (TotalCar == "0") {
            $("#divresult").show();
        }
        else {
            $("#divresult").hide();
        }
    }
    $scope.showImage = function () {
        $("#LoadingResult").show();
        $("#Fadebg").show();
        $("#Fadebg").css("margin-left", "-1px");
        if ($(this).scrollTop() > 200) {
            $("#Fadebg").css("top", "0px");
            $("#LoadingResult").css("top", "40%");
        }
        else {
            $("#Fadebg").css("top", 200 - parseInt($(this).scrollTop()) + "px");
            $("#LoadingResult").css("top", "60%");
        }
        setTimeout(function () {
            ShowHideRows();
            hideImage();
        }, 0);
    }
    hideImage = function () {
        $("#LoadingResult").fadeOut(100);
        $("#Fadebg").hide();
        return true;
    }
    MapModify();
    $scope.CarType = "true";
    $scope.cartransmission = "true";
    $scope.carclass = "true";
    $scope.removeclass = function (id) {
        $("#CP_" + id).removeClass("schfldserr");
    }
});


