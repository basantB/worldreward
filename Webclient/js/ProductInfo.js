﻿$.fn.digits = function () {
    return this.each(function () {
        $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    })
},
$.fn.stars = function () {
    return $(this).each(function () {
        // Get the value
        var val = parseFloat($(this).html());
        //val = Math.round(val * 4) / 4
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;
        // Create stars holder
        var $span = $('<span />').width(size);
        // Replace the numerical value with stars
        $(this).html($span);
    });
}

$(document).ready(function () {
    debugger;
    var $window = $(window);
    function checkDatePickerWidth() {
        var windowsize = $window.width();
        if (windowsize > 550) {
            bindDatepicker();
        }
        else {
            bindMobDatepicker();
        }
    }
    // Execute on load
    checkDatePickerWidth();
    // Bind event listener
    $(window).resize(checkDatePickerWidth);




    BindAdditinalInfo();


});

function bindDatepicker() {
    $("#CP_txtBookingDate").datepicker({
        numberOfMonths: 2,
        minDate: 0,
        dateFormat: 'dd/mm/yy'
    });
}
function bindMobDatepicker() {
    $("#CP_txtBookingDate").datepicker({
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy'
    });
}
function CheckProductAvailabilityRequest() {

    var BookingDate = $("#CP_txtBookingDate").val();
    var AdultCount = $("#CP_ddlAdult").val();
    var ChildCount = $("#CP_ddlChild").val();
    var InfantCount = $("#CP_ddlInfant").val();
    var SeniorCount = $("#CP_ddlSenior").val();
    var msg = "";
    if (($("#CP_txtBookingDate").val() == null) || ($("#CP_txtBookingDate").val().length == 0)) {
        msg = "Please Select Travel Date";
    }
    if (parseInt($("#CP_ddlInfant").val()) > parseInt($("#CP_ddlAdult").val())) {
        msg = "Infants is more than Adults. <br>";
    }
    if (msg.length > 0) {
        $("#divProducNotAvailable").show();
        $("#divProducNotAvailable")[0].innerHTML = msg;
        return false;
    }
    else {

        $.ajax({
            type: 'POST',
            url: 'ViewProductInfo.aspx/CheckProductAvailability',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'pstrBookingDate':'" + BookingDate + "','pstrAdultCount':'" + AdultCount + "','pstrChildCount':'" + ChildCount + "','pstrInfantCount':'" + InfantCount + "','pstrSeniorCount':'" + SeniorCount + "'}",
            cache: false,
            success: function (msg) {
                if (msg.d == "") {
                    window.location = "TourGradeList.aspx";
                }
                else {
                    $("#divProducNotAvailable").css("display", "block");
                    $("#divProducNotAvailable")[0].innerHTML = msg.d;

                }
            },
            error: function (errmsg) {

            }
        });
    }
}

function BindAdditinalInfo() {

    $.ajax({
        type: 'POST',
        url: 'ViewProductInfo.aspx/BindAdditinalInfo',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "",
        cache: false,
        success: function (msg) {
            if (msg.d != null) {
                var result = msg.d;
                var AdditionalInfoList = "<h3 class='vatrhdr'>Additional info</h3><ul>";
                for (icount = 0; icount < result.Product.AdditionalInfoList.length; icount++) {
                    AdditionalInfoList += "<li>" + result.Product.AdditionalInfoList[icount] + "</li>";
                }
                AdditionalInfoList += "</ul>";
                $("#AdditionalInfo").append(AdditionalInfoList);
                $("#DeparturePoint").append("<h3 class='vatrhdr'>Departure point</h3>" + result.Product.DeparturePoint);

                var InclusionsList = "<h3 class='vatrhdr'>Inclusions</h3><ul>";
                for (icount = 0; icount < result.Product.InclusionsList.length; icount++) {
                    InclusionsList += "<li>" + result.Product.InclusionsList[icount] + "</li>";
                }
                InclusionsList += "</ul>";
                $("#InclusionsList").append(InclusionsList);

                var SalesPointsList = "<h3 class='vatrhdr'>Sales Points</h3><ul>";
                for (icount = 0; icount < result.Product.SalesPointsList.length; icount++) {
                    SalesPointsList += "<li>" + result.Product.SalesPointsList[icount] + "</li>";
                }
                SalesPointsList += "</ul>";
                $("#SalesPointsList").append(SalesPointsList);




                var ExclusionsList = "<h3 class='vatrhdr' >Exclusions</h3><ul>";
                for (icount = 0; icount < result.Product.ExclusionsList.length; icount++) {
                    ExclusionsList += result.Product.ExclusionsList[icount];
                }
                ExclusionsList += "</ul>";
                $("#ExclusionsList").append(ExclusionsList);

                $("#Duration").append("Duration: " + result.Product.Duration);

                $("#Itinerary").append("<h3 class='vatrhdr'>Itinerary</h3>" + result.Product.Itinerary);
                //$("#Itinerary").append("<ul><li><div class='accordion_head'>Itinerary<span class='plusminus'>+</span></div>" + "<div class='accordion_body' style='display: block;'>" +  result.Product.Itinerary + "</div>" + "</li></ul>");

                $("#ShortDescription").append("<h3 class='vatrhdr'>Short Description</h3>" + result.Product.ShortDescription);
                $("#ReturnDetails").append("<h3 class='vatrhdr'>Return Details</h3>" + result.Product.ReturnDetails);

                $("#discription").append(result.Product.Description);
                $("#Location").append(result.Product.Location);
                $("#ShortTitle").append(result.Product.ShortTitle);
                /*code for Slider*/
                if (result.Product.UserPhotosList != null) {
                    var sliderImage = "";
                    var SliderThumbImg = "";
                    for (i = 0; i < result.Product.UserPhotosList.length; i++) {
                        //newString = result.Product.UserPhotosList[i].Caption.replace(/(<">|<\/">)/g, '');
                        sliderImage += "<li><img src=" + result.Product.UserPhotosList[i].PhotoHiResURL + " /></li>";
                        //SliderThumbImg += "<a data-slide-index=" + i + " href=''><img src=" + result.Product.UserPhotosList[i].ThumbnailURL + " /></a>";
                    }
                }
                else {
                    var sliderImage = "";
                    var SliderThumbImg = "";
                    sliderImage += "<li><img src=" + result.Product.ThumbnailHiResURL + " /></li>";
                    //SliderThumbImg += "<a data-slide-index=" + 0 + " href=''><img src=" + result.Product.ThumbnailURL + " /></a>";
                }
                $("#UserPhotos").append(sliderImage);
                //$("#bx-pager").append(SliderThumbImg);

                $('.bxslider').bxSlider({
                    auto: true,
                    mode: 'fade',
                    controls: true, pager: false,
                    nextText: 'Next',
                    prevText: 'Prev'
                    //pagerCustom: '#bx-pager'
                });


                $("#Divoverview, #DivImportant, #bx-pager, #discription").niceScroll({
                    cursorcolor: "#e3443e",
                    cursoropacitymin: 0.3,
                    background: "#bbb",
                    cursorborder: "0",
                    autohidemode: false,
                    cursorminheight: 30
                });

                $("#CP_lblPoints").text(result.Product.FareDetails.TotalBaseFare);
                $("#CP_lblRating").text(result.Product.Rating);
                $("span.stars").stars();
                $(".pointmiles").digits();
                for (icount = 0; icount < result.Product.AgeBandsList.length; icount++) {
                    if (result.Product.AgeBandsList[icount].BandId == 1) {
                        $("#divAdult").show();
                        $("#ddlAdultVal").text("(" + result.Product.AgeBandsList[icount].AgeFrom + "-" + result.Product.AgeBandsList[icount].AgeTo + " Years)");
                    }
                    else if (result.Product.AgeBandsList[icount].BandId == 2) {
                        $("#divChild").show();
                        $("#ddlChildVal").text("(" + result.Product.AgeBandsList[icount].AgeFrom + "-" + result.Product.AgeBandsList[icount].AgeTo + " Years)");
                    }
                    else if (result.Product.AgeBandsList[icount].BandId == 5) {
                        $("#divSenior").show();
                        $("#ddlSeniorVal").text("(" + result.Product.AgeBandsList[icount].AgeFrom + "-" + result.Product.AgeBandsList[icount].AgeTo + " Years)");
                    }
                    else if (result.Product.AgeBandsList[icount].BandId == 3) {
                        $("#divInfant").show();
                        $("#ddlInfantVal").text("(" + result.Product.AgeBandsList[icount].AgeFrom + "-" + result.Product.AgeBandsList[icount].AgeTo + " Years)");
                    }
                    else { }
                }
            }
            else {
                window.location.href = "SearchTour.aspx";
            }
        },
        error: function (errmsg) {

        }
    });
}

function showtourInfo(para1, para2) {
    $("#Div" + para1).fadeIn();
    $("#Div" + para2).hide();
    $("#tab" + para1).addClass("select");
    $("#tab" + para2).removeClass("select");
    return false;
}