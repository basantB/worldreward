﻿
$(window).scroll(function () {
    var WinWidth = $(window).width();
    if (WinWidth > 1100) {
        if ($(this).scrollTop() > 70) {
            //$('#UserInfo').css({
            //    margin: "48px 0 20px",
            //    position: "fixed",
            //    right: "5%",
            //    top: "0"
            //});
            ////$("#modifyDetails").slideUp("1000");
            //$("#fliterDetails").css({ margin: "300px 0 0px", });
            $(".pgcol2").css({ width: "24.70%" });
        }
        else {
            $('#UserInfo').css({
                margin: "0px 0 20px 0", position: "inherit", right: "",
                top: ""
            });
            //$("#modifyDetails").slideDown("1000");
            $("#fliterDetails").css({ margin: "0", });
            $(".pgcol2").css({ width: "auto" });
        }
        //$("#modifyDetails").slideUp("1000");
    }
});
$(function () {
    $('#CP_TextBoxCheckin').datepicker({
        startDate: '+3d',
        format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {
        var toDate = new Date(e.date.getFullYear(), e.date.getMonth(), e.date.getDate());
        var oneDay = new Date(toDate.getTime() + 86400000);
        $("#CP_TextBoxCheckin").removeClass("schfldserr");
        if ((oneDay.getMonth() + 1) < 10) {
            var Month = "0" + (oneDay.getMonth() + 1);
        }
        else {
            var Month = (oneDay.getMonth() + 1);
        }
        oneDay = oneDay.getDate() + "/" + Month + "/" + oneDay.getFullYear();
        $("#CP_TextBoxCheckout").val(oneDay);
        $("#CP_TextBoxCheckout").removeClass("schfldserr");
        $("#CP_TextBoxCheckout").datepicker({
            startDate: e.date,
            format: "dd/mm/yyyy"
        }).on('changeDate', function (e) {
            $("#CP_TextBoxCheckout").removeClass("schfldserr");
        });
        return false;
    });

    $('#ddlRoomno').on('change', function () {
        selectRooms();
    });
    return false;
});
function getHotelDetails(hotelId) {
    $.ajax({
        type: "POST",
        url: "HotelResults.aspx/SetHotelId",
        data: "{'pstrHotelId':'" + hotelId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            var linkurl = "HotelDetails.aspx";
            window.location.href = linkurl;
        }
    });


}
function hideshowFitler(obj) {
    $("#" + obj).toggle('slow');
    return false;
}

function gotoHotelBooking(obj) {
    var linkurl = $("#" + obj.id).parent().find("input[type='hidden']").val();
    $.ajax({
        url: 'HotelResults.aspx/BookHotel',
        type: 'POST',  // or get
        contentType: 'application/json; charset=utf-8',
        data: "{'pstrForwardURL':'" + linkurl + "'}",
        dataType: 'json',
        success: function (data) {
            if (data.d)
                window.location = linkurl;
        },
        error: function (errmsg) {
        }
    });
}

function SelectAll(obj, divElement) {
    var checkBox = $("#" + obj);
    var divElement = $("#" + divElement);

    if (checkBox.is(":checked")) {
        for (var i = 0; i <= divElement.find("input[type='checkbox']").length; i++) {
            $(divElement.find("input[type='checkbox']")[i]).data("checked", "checked");
            //$(divElement.find("input[type='checkbox']")[i]).attr("checked", "checked");
            $(divElement.find("input[type='checkbox']")[i]).prop('checked');
        }
    } else {
        for (var i = 0; i <= divElement.find("input[type='checkbox']").length; i++) {
            $(divElement.find("input[type='checkbox']")[i]).removeAttr("checked");
        }
    }
    showImage();
    return false;
}

function uniqueArrayElemets(arrayName) {
    var newArray = new Array();
    label: for (var i = 0; i < arrayName.length; i++) {
        for (var j = 0; j < newArray.length; j++) {
            if (newArray[j] == arrayName[i])
                continue label;
        }
        newArray[newArray.length] = arrayName[i];
    }
    return newArray;
}

Array.max = function (array) {
    return Math.max.apply(Math, array);
};
Array.min = function (array) {
    return Math.min.apply(Math, array);
};
Array.prototype.contains = function (needle) {
    for (i in this) {
        if (this[i] == needle) return true;
    }
    return false;
}
function selectRooms() {
    var noofRooms = $('#ddlRoomno :selected').text();
    var strTemplate = "<ul>";
    $(".append").html('');
    for (var count = 0; count <= noofRooms - 1; count++) {

        if (count % 2 != 0) {
            strTemplate += '<li class="room1 mrno">';
        }
        else {
            strTemplate += '<li class="room1">';

        }
        strTemplate += "Room" + (count + 1) + "<ul><li>";
        strTemplate += "<label>Adult</label><select><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></li>";
        strTemplate += "<li class='mrno'><label>Child</label><select><option value='0'>0</option><option value='1'>1</option><option value='2'>2</option></select><span>(0-11 years)</span></li></ul>";
        strTemplate += "</li>";

    }
    strTemplate += "</ul>";
    $(".append").html(strTemplate);

}
var app = angular.module("Danamon", ['ui-rangeSlider', 'ngAnimate', 'ui.bootstrap'])

//var app = angular.module('AlRayan', ['ngAnimate', 'ui.bootstrap']);
app.controller("MainCtrl", function ($scope, $http, $compile) {
    $("#ChkSelectAll").prop("checked", true);
    $scope.getAllHotelCities = function (val) {
        return $http({
            url: "FlightLBMSServices.aspx/GetAllHotelCities",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                addresses.push(item);
            });
            return addresses;
        });
    };
    $scope.SearchRoom = function () {
        SearchRooms();
    }
    $scope.empty = function (id) {
        $("#CP_" + id).val('');
        $("#CP_" + id).removeClass("schfldserr");
    }
    SliderFilter = function () {
        var FilterHotelRange = $("#CP_hdnHotelFilterRange").val();
        FilterHotelRange = $.parseJSON(FilterHotelRange);
        $scope.priceSlider = {
            range: {
                min: FilterHotelRange.MinPrice,
                max: FilterHotelRange.MaxPrice,
            },
            minPrice: FilterHotelRange.MinPrice,
            maxPrice: FilterHotelRange.MaxPrice,

        };
        $scope.priceSliderStop = function () {
            //return FilterSlider(1, $scope.priceSlider.minPrice, $scope.priceSlider.maxPrice);
            $scope.showImage();
        }
    }
    FilterHotels = function () {
        $('.modify').click(function () {
            $('.modify-box').slideToggle();
        });
        var FilterHotelRange = $("#CP_hdnHotelFilterRange").val();
        FilterHotelRange = $.parseJSON(FilterHotelRange);
        var strRating = "";
        var flag1star = false;
        var flag2star = false;
        var flag3star = false;
        var flag4star = false;
        var flag5star = false;
        var flagNorated = false;
        var rowcount = $(".hotels .hotelrow").length;
        for (var hotelcount = 0; hotelcount < rowcount; hotelcount++) {

            var hotelrow = ($(".hotels .hotelrow")[hotelcount]);
            var rating = $(hotelrow).find(".ratings").attr('ratings');
            rating = parseInt(rating);

            strrating = "";

            for (var count = 0; count < rating; count++) {
                if (rating <= 5) {
                    strrating += "<img src='Images/star.png' />";
                }
                else {
                    strrating = "Not Rated";
                }
            }

            $(hotelrow).find('.ratings').html(strrating);
            if (rating == 5) {
                flag5star = true;
            }
            if (rating == 4) {
                flag4star = true;
            }
            if (rating == 3) {
                flag3star = true;
            }
            if (rating == 2) {
                flag2star = true;
            }
            if (rating == 1) {
                flag1star = true;
            }
            if (rating > 5) {
                flagNorated = false;
            }
        }
        var htmlstr = "";
        /*Code for */
        if (flag5star == true) {
            htmlstr += "<li class='listHotel'><input type='checkbox' ng-checked='hotelstars' value='5' display='5' ng-click='showImage()' checked='checked' /><img src='Images/star.png' style='FL'/><img src='Images/star.png' style='FL'/><img src='Images/star.png' style='FL'/><img src='Images/star.png' style='FL'/><img src='Images/star.png' style='FL'/></li>";
        }
        if (flag4star == true) {
            htmlstr += "<li class='listHotel'><input type='checkbox' ng-checked='hotelstars' value='4' display='4' ng-click='showImage()' checked='checked' /><img src='Images/star.png' style='FL'/><img src='Images/star.png' style='FL'/><img src='Images/star.png' style='FL'/><img src='Images/star.png' style='FL'/></li>";
        }
        if (flag3star == true) {
            htmlstr += "<li class='listHotel'><input type='checkbox' ng-checked='hotelstars' value='3' display='3'  ng-click='showImage()' checked='checked' /><img src='Images/star.png' style='FL'/><img src='Images/star.png' style='FL'/><img src='Images/star.png' style='FL'/></li>";
        }
        if (flag2star == true) {
            htmlstr += "<li class='listHotel'><input type='checkbox' ng-checked='hotelstars' value='2' display='2'  ng-click='showImage()' checked='checked' /><img src='Images/star.png' style='FL'/><img src='Images/star.png' style='FL'/></li>";
        }
        if (flag1star == true) {
            htmlstr += "<li class='listHotel'><input type='checkbox' ng-checked='hotelstars' value='1' display='1'  ng-click='showImage()' checked='checked' /><img src='Images/star.png' style='FL'/></li>";
        }
        if (flagNorated == true) {
            htmlstr += "<li class='listHotel'><input type='checkbox' id='chkRatingNotAvailble' display='NOTAVAILABLE' ng-click='showImage()' checked='checked' />Not Rated<img src='Images/star.png' style='FL'/></li>";
        }
        var $divratings = $(htmlstr).appendTo('#divRatings');
        $compile($divratings)($scope);

        //$("#priceRange").text(FilterHotelRange.MinPrice.toString() + "-" + FilterHotelRange.MaxPrice.toString());
        var hotellocations = "";
        for (var count = 0; count < FilterHotelRange.ListOfLocation.length; count++) {
            if (FilterHotelRange.ListOfLocation[count].toString() != "") {
                hotellocations += "<li><input type='checkbox' display='" + FilterHotelRange.ListOfLocation[count] + "' checked='checked' ng-checked='hotellocations'  ng-click='showImage()' />" + FilterHotelRange.ListOfLocation[count] + "</li>";
            }
        }
        var $divlocations = $(hotellocations).appendTo('#divLocation');
        $compile($divlocations)($scope);
        $("#totalHotel").html("Total Hotels Found: " + rowcount + "");
        var HotelChain = "<li class='listHotel'><input type='checkbox' display='None' checked='checked' ng-checked='hotelchains' ng-click='showImage()' /> None</li>";
        for (var count = 0; count < FilterHotelRange.ListOfHotelChain.length; count++) {
            if (FilterHotelRange.ListOfHotelChain[count].toString() != "") {
                HotelChain += "<li class='listHotel'><input type='checkbox' ng-checked='hotelchains' ng-click='showImage()' display='" + FilterHotelRange.ListOfHotelChain[count] + "' checked='checked' /> " + FilterHotelRange.ListOfHotelChain[count] + "</li>";
            }
        }
        var $hotelchains = $(HotelChain).appendTo('#divHotelChain');
        $compile($hotelchains)($scope);
    }
    ShowHideRows = function () {
        var TotalHotel = 0;
        var ListOfLocation = [];

        ListOfLocation = $("#divLocation").find("input[type='checkbox']");
        var Location = [];
        for (var count = 0; count < ListOfLocation.length; count++) {
            if (ListOfLocation.eq(count).is(":checked")) {
                Location.push(ListOfLocation.eq(count).attr('display'));
            }
        }
        Location.push("");

        var ListOfChain = [];
        ListOfChain = $("#divHotelChain").find("input[type='checkbox']");
        var Chains = [];
        for (var count = 0; count < ListOfChain.length; count++) {
            if (ListOfChain.eq(count).is(":checked")) {
                Chains.push(ListOfChain.eq(count).attr('display'));
            }
        }

        var ListOfBasicAmentites = [];
        ListOfBasicAmentites = $("#divBasicAmenities").find("input[type='checkbox']");
        var Amenities = [];
        for (var count = 0; count < ListOfBasicAmentites.length; count++) {
            if (ListOfBasicAmentites.eq(count).is(":checked")) {
                Amenities.push(ListOfBasicAmentites.eq(count).attr('display'));
            }
        }

        var ListOfRating = [];
        ListOfRating = $("#divRatings").find("input[type='checkbox']");
        var Ratings = [];
        for (var count = 0; count < 5; count++) {
            if (ListOfRating.eq(count).is(":checked")) {
                Ratings.push(ListOfRating.eq(count).attr('display'));
            }
        }
        var notAvailableChecked = $("#chkRatingNotAvailble").is(":checked");
        var hotels = $(".hotels").find(".hotelrow");
        var counter = 0;
        for (var count = 0; count < hotels.length; count++) {
            hotel = $.parseJSON(hotels.eq(count).find(".jsondata input[type='hidden']").val());
            if (hotel != null) {
                var roomPrice = 0;
                //var milesrange = $("#priceSlider").slider("option", "values");
                for (var priceCount = 0; priceCount < hotel.roomrates.RoomRate.length; priceCount++) {
                    roomPrice += parseInt(hotel.roomrates.RoomRate[priceCount].TotalPrice);
                }

                roomPrice = parseInt(hotel.roomrates.RoomRate[0].TotalPoints);
                var hasMiles = false;
                if (roomPrice >= $scope.priceSlider.minPrice && roomPrice <= $scope.priceSlider.maxPrice) {
                    hasMiles = true;
                }
                //            
                var hasRating = false;
                if (hotel.basicinfo.hotelratings.HotelRating != null && hotel.basicinfo.hotelratings.HotelRating.length >= 1) {
                    if (hotel.basicinfo.hotelratings.HotelRating[0].rating <= 5) {
                        hasRating = Ratings.contains(parseInt(hotel.basicinfo.hotelratings.HotelRating[0].rating));
                    }
                    if (notAvailableChecked && hotel.basicinfo.hotelratings.HotelRating[0].rating > 5) {
                        hasRating = true;
                    }
                }
                var hasLocation = false;
                hasLocation = Location.contains(hotel.basicinfo.locality);
                var hasChain = false;
                hasChain = Chains.contains(hotel.basicinfo.chain);
                if (Chains.contains('None')) {
                    if (hotel.basicinfo.chain == '')
                        hasChain = true;
                }
                var AmenitiesCategory = hotel.basicinfo.hotelamenities.Amenities;
                var hasAmenities = false;

                if (AmenitiesCategory != null) {
                    for (var amnCount = 0; amnCount < AmenitiesCategory.length && !hasAmenities; amnCount++) {
                        for (var selectedAmenities = 0; selectedAmenities < Amenities.length && !hasAmenities; selectedAmenities++) {
                            for (var basicAmenity = 0; basicAmenity < AmenitiesCategory[amnCount].Amenities.hotelamenity.length; basicAmenity++) {
                                if (AmenitiesCategory[amnCount].Amenities.hotelamenity[basicAmenity].Value.toString().toLowerCase().indexOf(Amenities[selectedAmenities].toString().toLowerCase()) != "-1") {
                                    hasAmenities = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                var result = hasMiles && hasLocation && hasChain && hasRating && hasAmenities;
                if (result) {
                    hotels.eq(count).show(); counter++;
                    TotalHotel = TotalHotel + 1;
                } else {
                    hotels.eq(count).hide();

                }
                $("#totalHotel").html('');
                $("#DivTotalHotel").html('');
                $("#totalHotel").html("Totat Hotels Found: " + TotalHotel + "");
                if (TotalHotel == "0") {
                    $("#divresult").show();
                }
                else {
                    $("#divresult").hide();
                }
            }
        }
    }
    SetModifyDetails = function () {
        $http({
            url: "HotelResults.aspx/SetModifyDetails",
            dataType: 'json',
            method: 'POST',
            data: '',
            headers: {
                "Content-Type": "application/json"
            }
        })
       .success(function (response) {
           $("#CP_txtCity").val(response.d[0]);
           $("#CP_TextBoxCheckin").val(response.d[1]);
           $("#CP_TextBoxCheckout").val(response.d[2]);
           //$("#chkinchkout").append(response.d[1] + " | " + response.d[2] + " | No. of Rooms: " + response.d[3])
       });
    }

    $scope.showImage = function () {
        ShowHideRows();
    }
    $scope.showModifyHotel = function () {
        //alert('hhh');
        $("#modifyDetails").slideToggle();
    }
    $scope.showFilterHotel = function () {
        $("#fliterDetails").slideToggle();
    }
    hideImage = function () {
        $("#LoadingResult").fadeOut(100);
        $("#Fadebg").hide();
        return true;
    }
    SetModifyDetails();
    loadRoomsOnPage($("#CP_HFNoOfRooms").val());

    SliderFilter();
    FilterHotels();
    $scope.master = true;
    $scope.hotelchains = true;
    $scope.hotellocations = true;
    $scope.hotelstars = true;
    function SearchRooms() {
        var room = [];
        room = $(".append .room1");
        var strRoomAdult = "";
        var strRoomChild = "";
        var strRoom = "";
        for (var count = 0; count < room.length; count++) {
            var selectTag = [];
            selectTag = $(room[count]).find('select');
            strRoomAdult += $(selectTag[0]).val() + ",";
            strRoomChild += $(selectTag[1]).val() + ",";
        }
        strRoom = strRoomAdult + ":" + strRoomChild;
        $("#CP_hdnRoomString").val(strRoom);
        if (validateHotelFields()) {
            var strCity = "city=" + $("#CP_txtCity").val() + "&";
            var strCheckIn = "checkin=" + $("#CP_TextBoxCheckin").val() + "&";
            var strCheckout = "checkout=" + $("#CP_TextBoxCheckout").val() + "&";
            var strRoomstring = "roomstring=" + $("#CP_hdnRoomString").val() + "&";
            var isRedeemMiles = "isRedeemMiles=True";
            var queryString = strCity + strCheckIn + strCheckout + strRoomstring + isRedeemMiles;
            window.location = "HotelsSearchWait.aspx?" + queryString;
            return false;
        } else {
            return false;
        }
    }
    validateHotelFields = function () {
        //var msg = "";
        var cnt = 0;
        if ($("#CP_txtCity").val().length == 0 || $("#CP_txtCity").val() == "Enter City Name" || $("#CP_txtCity").val() == "*Required") {
            //  msg = "Please Enter City Name";
            $("#CP_txtCity").addClass("schfldserr");
            $("#CP_txtCity").val("*Required");
            cnt++;
        }
        if ($("#CP_TextBoxCheckin").val().length == 0 || $("#CP_TextBoxCheckin").val() == "Check In" || $("#CP_TextBoxCheckin").val() == "*Required") {
            //msg = "Please Enter CheckIn Date";
            $("#CP_TextBoxCheckin").addClass("schfldserr");
            $("#CP_TextBoxCheckin").val("*Required");
            cnt++;
        }
        if ($("#CP_TextBoxCheckout").val().length == 0 || $("#CP_TextBoxCheckout").val() == "Check out" || $("#CP_TextBoxCheckout").val() == "*Required") {
            //msg = "Please Enter CheckOut Date";
            $("#CP_TextBoxCheckout").addClass("schfldserr");
            $("#CP_TextBoxCheckout").val("*Required");
            cnt++;
        }
        if (cnt == 0) {
            return true;
        }
        else {
            return false;
        }
        //if (msg.length > 0) {
        //    $("#HotelModifyValidation")[0].innerHTML = msg;

        //    showdiv("divHotelModifyValidation");
        //    setTimeout(blank('divHotelModifyValidation'), 3000);
        //    return false;
        //}
        //else
        //    return true;
    }
});

function loadRoomsOnPage(noofRooms) {
    $('#ddlRoomno').val(noofRooms);
    var strTemplate = "";
    var room = [];
    var AdultPerRoom = $("#CP_hdnNoAdult").val();
    var ChildPerRoom = $("#CP_hdnNoChild").val();
    var SplitAdult = [];
    var SplitChild = [];
    var lintAdultCount = 0;
    var lintChildCount = 0;
    SplitAdult = AdultPerRoom.split(',');
    SplitChild = ChildPerRoom.split(',');
    for (var count = 1; count <= noofRooms; count++) {
        for (lintAdultCount; lintAdultCount <= SplitAdult.length - 1; lintAdultCount++) {

            strTemplate += GenrateAdultScript(SplitAdult[lintAdultCount], count);
            lintAdultCount = lintAdultCount + 1;
            break;
        }
        for (lintChildCount; lintChildCount <= SplitChild.length - 1; lintChildCount++) {

            strTemplate += GenrateChildScript(SplitChild[lintChildCount], count);
            lintChildCount = lintChildCount + 1;
            break;
        }

        if (count > lintAdultCount) {

            strTemplate += GenrateAdultScript(1, count);
            strTemplate += GenrateChildScript(0, count);

        }

    }
    strTemplate = "<ul>" + strTemplate + "</ul>";
    $(".append").append(strTemplate);
}

function GenrateAdultScript(adult, count) {

    var htmlString = "";
    if (count % 2 == 0) {
        htmlString += '<li class="room1 mrno">';
    }
    else {
        htmlString += '<li class="room1">';
    }
    htmlString += "Room" + count + "<ul><li>";
    htmlString += "<label>Adult</label>";
    htmlString += "<select id='ddlAdultModify" + count + "' class='noofrooms input3'>";
    for (var i = 1; i < 5; i++) {
        if (i == adult) {
            htmlString += "<option value='" + i + "' selected >" + i + "</option>";
        }
        else {
            htmlString += "<option value='" + i + "'>" + i + "</option>";
        }
    }
    htmlString += "</select></li>";
    return htmlString;
}

function GenrateChildScript(child, count) {

    var htmlString = "<li class='mrno'><label>Child</label>";
    htmlString += "<select id='ddlChildModify" + count + "' class='noofrooms input3'>";
    for (var i = 0; i < 3; i++) {
        if (i == child) {
            htmlString += "<option value='" + i + "' selected >" + i + "</option>";
        }
        else {
            htmlString += "<option value='" + i + "'>" + i + "</option>";
        }
    }
    htmlString += "</select><span>(0-11 years)</span></li></ul></li>";
    return htmlString;
}

