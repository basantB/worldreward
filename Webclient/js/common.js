﻿$(document).ready(function () {
    var pathname = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
    var strQuery = window.location.href.substr(window.location.href.lastIndexOf("=") + 1);
    if (strQuery == "1") {
        TabSlider('flt', 'htl', 'car', 'lou', 'tou');
    }
    if (strQuery == "2") {
        TabSlider('htl', 'flt', 'car', 'lou', 'tou');
    }
    if (strQuery == "3") {
        TabSlider('car', 'htl', 'flt', 'lou', 'tou');
    }
    if (strQuery == "4") {
        TabSlider('lou', 'htl', 'car', 'flt', 'tou');
    }
    if (strQuery == "5") {
        TabSlider('tou', 'htl', 'car', 'lou', 'flt');
    }
});

function TabSlider(para, para1, para2, para3, para4) {
    $('.scrollmenu a').removeClass("select");
    $("#a" + para).addClass("select");

    $("#divS" + para).show();
    $("#divS" + para1).hide();
    $("#divS" + para2).hide();
    $("#divS" + para3).hide();
    $("#divS" + para4).hide();

    $(".bxslider li").hide();
    $("#li" + para).fadeIn();
    
    return false;
};