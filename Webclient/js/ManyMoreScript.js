﻿function GetCashBack() {
    debugger;
    var msg = "";
    if ($("#CP_txtBundles").val() == "") {
       
        msg = "Please enter the number of points bundles you wish to redeem";
    }
    else if ($("#CP_LabelAmount").text() == "0" || $("#CP_LabelAmount").text() == "") {
        msg = "Insufficient Amount For Cash Back.";
       
    }
    else if ($("#CP_txtPoints").val() == "0") {
        msg = "Number of points bundles should be greater than 0";
       
    }
    else if (!$("#CP_chkCashBack")[0].checked) {
        msg = "Please &#x2611; confirm your cash back value.";
       
    }

    if (msg.length > 0) {
        $("#ValidateCashBack").show();
        $("#ValidateCashBack")[0].innerHTML = msg;
        return false;
    }
    else {
        return GetCashBckAmount();
    }
}

function GetCashBckAmount() {
    var strAmount = $("#CP_LabelAmount").text().split(',')[0];
    var totalPoints = $("#CP_txtPoints").val();

    $.ajax({
        url: 'ManyMore.aspx/CashBackConfirm',
        type: 'POST',  // or get
        contentType: 'application/json; charset =utf-8',

        data: "{'strAmount':'" + strAmount + "','strtotalPoints':'" + totalPoints + "'}",
        dataType: 'json',
        success: function (msg) {
            if (msg.d == 'Success') {
                var Cashback = "flag=" + "Cashback";
                var queryString = Cashback;
                window.location = "CashBackPointGateway.aspx?flag=Cashback";
                return false;
            }
            else if (msg.d == 'Login')
                window.location = "Login.aspx";
            else if (msg.d == 'OtpError') {
                $("#ValidateCashBack").show();
                $("#ValidateCashBack")[0].innerHTML = "We are unable to CashBackPointGateway transaction, please try again later.";
            }
            else if (msg.d == 'InsufficientPoints') {
                $("#ValidateCashBack").show();
                $("#ValidateCashBack")[0].innerHTML = "Insufficient reward points balance. Visit your account menu to purchase reward points and complete this redemption.";
            }
        },
        error: function (errmsg) {

        }
    });

}

