﻿

$(function () {
    $("#CP_CarTextBoxCheckin").datepicker({
        startDate: new Date(),
        format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {
        $("#CP_CarTextBoxCheckin").removeClass("schfldserr");
        $.ajax({
            type: 'POST',
            url: 'Index.aspx/GetPickupTime',
            //url: 'CarSearch.aspx/GetPickupTime',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'Pickupdate':'" + $("#CP_CarTextBoxCheckin").val() + "','LocationId':'" + $("#CP_DDLLocation").find('option:selected').prop("value") + "'}",
            cache: false,
            success: function (data) {
                //show Timepicker
                //debugger;
                $("#CP_ddlFromTime").empty();
                var dateList = data.d;
                var Time = -1
                var hdnddlFromTime
                var count = 0;

                $("#CP_CarValidationError").hide();
                if (data.d != '000000000000000000000000') {
                    for (var count = 0; count <= dateList.length; count++) {
                        var times;
                        Time++;
                        if (data.d.charAt(count) == '1') {
                            if (Time < 10) {
                                $("#CP_ddlFromTime").append($("<option>" + "0" + Time + "</option>"));
                                times = "0" + Time;
                            }
                            else {
                                $("#CP_ddlFromTime").append($("<option>" + Time + "</option>"));
                                times = Time;
                            }
                            if (count == 0) {
                                hdnddlFromTime = times;
                            }
                            else {
                                hdnddlFromTime = hdnddlFromTime + "," + times;
                            }
                        }
                    }
                    document.getElementById('CP_ddlFromTime').selectedIndex = 0;
                    //$("#CP_DivFromHour").html($("#CP_ddlFromTime option:first-child").val());
                }
                else {
                    $("#CP_CarValidationError").show();
                    $("#CP_CarValidationError")[0].innerHTML = 'Car is not Available for this Date';
                }
                $("#CP_hdnddlFromTime").val(hdnddlFromTime);
                //$("#CP_ddlFromTime").selectric('refresh');
            },
            error: function (errmsg) {

                alert(errmsg.d);

            }
        });

        $("#CP_CarTextBoxCheckout").val('');
        $("#CP_CarTextBoxCheckout").datepicker({
            startDate: e.date,
            format: "dd/mm/yyyy"
        }).on('changeDate', function (e) {
            $("#CP_CarTextBoxCheckout").removeClass("schfldserr");
            if ($('#CP_droploction').attr('checked')) {
                locationId = $("#CP_DDLLocation").find('option:selected').prop("value");
            }
            else {
                locationId = $("#CP_ddlDropLocation").find('option:selected').prop("value");
            }
            getDrophourDetails($("#CP_CarTextBoxCheckout").val(), locationId);

        });
        //  getDrophourDetails(e.date, $('#CP_txtCountry').val());
        return false;
    });
    return false;
});

$(function () {
    $("#CP_rdbRoundTrip").prop("checked", true);
    $('#CP_txtDepart').datepicker({
        startDate: new Date(),
        format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {
        $("#CP_txtReturn").val('');
        $("#CP_txtDepart").removeClass("schfldserr");
        $("#CP_txtReturn").datepicker({
            startDate: e.date,
            format: "dd/mm/yyyy",

        }).on('changeDate', function (e) {
            $("#CP_txtReturn").removeClass("schfldserr");
        })
        return false;
    });
    return false;
});

$(function () {
    $('#CP_TextBoxCheckin').datepicker({
        startDate: '+3d',
        format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {
        var toDate = new Date(e.date.getFullYear(), e.date.getMonth(), e.date.getDate());
        var oneDay = new Date(toDate.getTime() + 86400000);
        $("#CP_TextBoxCheckin").removeClass("schfldserr");
        if ((oneDay.getMonth() + 1) < 10) {
            var Month = "0" + (oneDay.getMonth() + 1);
        }
        else {
            var Month = (oneDay.getMonth() + 1);
        }
        if (oneDay.getDate() < 10) {
            var Day = "0" + oneDay.getDate();
        }
        else {
            var Day = oneDay.getDate();
        }
        oneDay = Day + "/" + Month + "/" + oneDay.getFullYear();
        $("#CP_TextBoxCheckout").val(oneDay);
        $("#CP_TextBoxCheckout").datepicker({
            startDate: e.date,
            format: "dd/mm/yyyy"
        }).on('changeDate', function (e) {
            $("#CP_TextBoxCheckout").removeClass("schfldserr");
        });
        return false;
    });

    $('#ddlRoomno').on('change', function () {
        selectRooms();
    });

    return false;
});

var app = angular.module('WDRewards', ['ngAnimate', 'ui.bootstrap']);

//app.directive('slideit', function () {
//    return {
//        restrict: 'A',
//        replace: true,
//        scope: {
//            slideit: '='
//        },
//        template: '<ul class="bxslider">' +
//                    '<li ng-repeat="slide in slides">' +
//                      '<img ng-src="{{slide.src}}" alt="" />' +
//                    '</li>' +
//                   '</ul>',
//        link: function (scope, elm, attrs) {
//            elm.ready(function () {
//                scope.$apply(function () {
//                    scope.slides = scope.slideit;
//                });
//                elm.bxSlider({
//                    adaptiveHeight: true,
//                    mode: 'horizontal',
//                    auto: true,
//                    controls: true,
//                    hideControlOnEnd: false,
//                    autoControls: true,
//                    pager: false
//                });
//            });
//        }
//    };
//});
app.controller('MainCtrl', function ($scope, $http) {
    //$scope.base = 'http://bxslider.com';
    //$scope.images = [
    //     //{ src: 'Images/banr1.jpg' },
    //     { src: 'Images/banr2.jpg' },
    //     { src: 'Images/banr3.jpg' },
    //     { src: 'Images/banr4.jpg' },
    //     { src: 'Images/banr5.jpg' },
    //     { src: 'Images/banr6.jpg' },
    //];
    selectRooms();
   
    $scope.getLocation = function (val) {
        return $http({
            url: "FlightLBMSServices.aspx/GetAirfields",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                addresses.push(item);
            });
            return addresses;
        });
    };
    $scope.getAllAirlines = function (val) {
        return $http({
            url: "FlightLBMSServices.aspx/GetCarriers",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var carriers = [];
            //var IATACode = [];
            angular.forEach(res.data.d, function (item) {
                carriers.push({ name: item.split('-')[0], iata: item.split('-')[1] });
            });
            return carriers;
        });
    };
    $scope.onSelectAirlines = function ($item, $model, $label) {
        $("#CP_hdnCarrier").val($item.iata);
    };

    $scope.getAllHotelCities = function (val) {
        return $http({
            url: "FlightLBMSServices.aspx/GetAllHotelCities",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                addresses.push(item);
            });
            return addresses;
        });
    };
    $scope.CarcountryList = function (val) {
        return $http({
            url: "Index.aspx/GetAllCountryList",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                addresses.push(item);
            });
            return addresses;
        });
    };
    GetShopProductList();
    BindLoungeMasterCountry();
    $scope.showOneway = function () {
        $("#retnli").hide();
    }
    $scope.showReturn = function () {
        $("#retnli").show();
    }
    showdiv = function (divname) {
        $("#" + divname).show();
    }
    var blank = function (divid) {
        $("#" + divid).delay(1000).fadeOut(1000);
    };
    $scope.FlightValidation = function () {
        var msg = "";
        if ($("#CP_rdbRoundTrip")[0].checked && ($("#CP_txtReturn").val() == '' || $("#CP_txtReturn").val() == null) || $("#CP_txtReturn").val() == 'dd/mm/yyyy') {
            msg = "Please enter valid information and retry";
            $("#CP_txtReturn").addClass("schfldserr");
        }
        if (($("#CP_textBoxFrom").val() == '' || $("#CP_textBoxFrom").val() == null) || $("#CP_textBoxFrom").val() == "Enter City or Airport") {
            msg = "Please enter valid information and retry";
            $("#CP_textBoxFrom").addClass("schfldserr");
        }
        if (($("#CP_textBoxTo").val() == '' || $("#CP_textBoxTo").val() == null) || $("#CP_textBoxTo").val() == "Enter City or Airport") {
            msg = "Please enter valid information and retry";
            $("#CP_textBoxTo").addClass("schfldserr");
        }
        if (($("#CP_txtDepart").val() == '' || $("#CP_txtDepart").val() == null || $("#CP_txtDepart").val() == 'dd/mm/yyyy')) {
            msg = "Please enter valid information and retry";
            $("#CP_txtDepart").addClass("schfldserr");
        }
        if (parseInt($("#CP_lblInfantCount").text()) > parseInt($("#CP_lblAdultCount").text())) {
            msg = "Please enter valid information and retry";
            $("#CP_lblInfantCount").addClass("schfldserr");
        }
        if (msg.length > 0) {
            $("#requiredValidation")[0].innerHTML = msg;
            showdiv('divrequiredValidation');
            setTimeout(blank('divrequiredValidation'), 2000);
            return false;
        }
        else {
            return clicktoflightsearch();
        }
    }
    $scope.SearchRoom = function () {
        SearchRooms();
    }
    $scope.CarSearch = function () {
        LocationSearchCarSearch();
    }
    clicktoflightsearch = function () {
        var strcityfrom = "cityfrom=" + $("#CP_textBoxFrom").val().split(',')[2] + "&";
        var strcityto = "cityto=" + $("#CP_textBoxTo").val().split(',')[2] + "&";
        var strFrom = "departure=" + $("#CP_textBoxFrom").val().split(',')[0] + "&";
        var strTo = "arrival=" + $("#CP_textBoxTo").val().split(',')[0] + "&";
        var strDepartDate = "departuredate=" + $("#CP_txtDepart").val() + "&";
        var strReturnDate = "arrivaldate=" + $("#CP_txtReturn").val() + "&";
        var isReturn = "isReturn=" + $("#CP_rdbRoundTrip").is(":checked").toString() + "&";
        var strAirlinePrefernce = "airline=" + $("#CP_txtAirline").val() + "&";
        var strAirlineIATACode = "airlineIATACode=" + $("#CP_hdnCarrier").val() + "&";
        var strAdultNo = "adult=" + $("#CP_DropDownListAdult").val() + "&";
        var strChildNo = "child=" + $("#CP_DropDownListChild").val() + "&";
        var strInfantNo = "infant=" + $("#CP_DropDownListInfant").val() + "&";
        var strEconomy = "economy=" + $("#CP_dropDownListEconomy").val();
        var queryString = strcityfrom + strcityto + strFrom + strTo + strDepartDate + strReturnDate + isReturn + strAirlinePrefernce + strAirlineIATACode + strAdultNo + strChildNo + strInfantNo + strEconomy;
        window.location = "SearchPage.aspx?" + queryString;
        return false;
    }
    $scope.showTab = function (par1, par2, par3, par4) {
        $("." + par1 + "flds").slideDown();
        $("." + par2 + "flds").hide();
        $("." + par3 + "flds").hide();
        $("." + par4 + "flds").hide();
        $("#" + par1 + "sch_tab a").addClass(par1 + "sch-select");
        $("#" + par2 + "sch_tab a").removeClass(par2 + "sch-select");
        $("#" + par3 + "sch_tab a").removeClass(par3 + "sch-select");
        $("#" + par3 + "sch_tab a").addClass(par3 + "sch");
        $("#" + par2 + "sch_tab a").addClass(par2 + "sch");
        $("#" + par1 + "sch_tab a").removeClass(par1 + "sch");
        $("#" + par4 + "sch_tab a").removeClass(par4 + "sch-select");
        $("#" + par4 + "sch_tab a").addClass(par4 + "sch");
        $("#pannelSearch").slideDown();
        $("html, body").animate({ scrollTop: 500 }, 1000);
        return false;
    }
    $scope.onCountrySelect = function ($item, $model, $label) {
        BindGlobalCityList($('#CP_DDlCity'), $('#CP_DDLLocation'), $item, '');
    }
    $scope.removeclass = function (id) {
        $("#CP_" + id).removeClass("schfldserr");
    }
    validateHotelFields = function () {
        var msg = "";
        if ($("#CP_txtCity").val().length == 0 || $("#CP_txtCity").val() == "Enter City Name") {
            msg = "Please enter valid information and retry";
            $("#CP_txtCity").addClass("schfldserr");
        }
        if ($("#CP_TextBoxCheckin").val().length == 0 || $("#CP_TextBoxCheckin").val() == "Check In") {
            msg = "Please enter valid information and retry";
            $("#CP_TextBoxCheckin").addClass("schfldserr");
        }
        if ($("#CP_TextBoxCheckout").val().length == 0 || $("#CP_TextBoxCheckout").val() == "Check out") {
            msg = "Please enter valid information and retry";
            $("#CP_TextBoxCheckout").addClass("schfldserr");
        }

        if (msg.length > 0) {
            $("#HotelModifyValidation")[0].innerHTML = msg;

            showdiv("divHotelModifyValidation");
            setTimeout(blank('divHotelModifyValidation'), 3000);
            return false;
        }
        else
            return true;
    }
});

function SearchRooms() {
    var room = [];
    room = $(".append .norooms");
    var strRoomAdult = "";
    var strRoomChild = "";
    var strRoom = "";
    for (var count = 0; count < room.length; count++) {
        var selectTag = [];
        selectTag = $(room[count]).find('select');
        strRoomAdult += $(selectTag[0]).val() + ",";
        strRoomChild += $(selectTag[1]).val() + ",";
    }
    strRoom = strRoomAdult + ":" + strRoomChild;
    $("#CP_hdnRoomString").val(strRoom);
    if (validateHotelFields()) {
        var strCity = "city=" + $("#CP_txtCity").val() + "&";
        var strCheckIn = "checkin=" + $("#CP_TextBoxCheckin").val() + "&";
        var strCheckout = "checkout=" + $("#CP_TextBoxCheckout").val() + "&";
        var strRoomstring = "roomstring=" + $("#CP_hdnRoomString").val() + "&";
        var isRedeemMiles = "isRedeemMiles=True";
        var queryString = strCity + strCheckIn + strCheckout + strRoomstring + isRedeemMiles;
        window.location = "HotelsSearchWait.aspx?" + queryString;
        return false;
    } else {
        return false;
    }
}
function selectRooms() {
    var noofRooms = $('#ddlRoomno :selected').text();
    var strTemplate = "";
    $(".append").html('');
    for (var count = 0; count <= noofRooms - 1; count++) {

        if (count == 3) {
            strTemplate += "<li class='norooms lst'>";
        }
        else {
            strTemplate += "<li class='norooms'>";
        }
        strTemplate += "<div><label>Room" + (count + 1) + "</label></div>";
        strTemplate += "<div class='inputbg'><select><option value='1'>1 Adult</option><option value='2'>2 Adults</option><option value='3'>3 Adults</option><option value='4'>4 Adults</option></select></div>";
        strTemplate += "<div class='inputbg'><select><option value='0'>0 Child</option><option value='1'>1 Child</option><option value='2'>2 Children</option></select><span>(0-11 years)</span></div><div class='clr'></div>";
        strTemplate += "</li>";

    }
    strTemplate += "<div class='clr'> </div>";
    $(".append").html(strTemplate);

}
