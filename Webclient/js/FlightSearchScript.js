﻿$(function () {
    $("#rdbRoundTrip").prop("checked", true);
    $('#CP_txtDepart').datepicker({
        startDate: new Date(),
        format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {
        $("#CP_txtReturn").val('');
        $("#CP_txtDepart").removeClass("schfldserr");
        $("#CP_txtReturn").datepicker({
            startDate: e.date,
            format: "dd/mm/yyyy",

        }).on('changeDate', function (e) {
            $("#CP_txtReturn").removeClass("schfldserr");
        })
        return false;
    });
    return false;
});

$(function () {
    $('#CP_TextBoxCheckin').datepicker({
        startDate: '+3d',
        format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {
        var toDate = new Date(e.date.getFullYear(), e.date.getMonth(), e.date.getDate());
        var oneDay = new Date(toDate.getTime() + 86400000);
        $("#CP_TextBoxCheckin").removeClass("schfldserr");
        if ((oneDay.getMonth() + 1) < 10) {
            var Month = "0" + (oneDay.getMonth() + 1);
        }
        else {
            var Month = (oneDay.getMonth() + 1);
        }
        if (oneDay.getDate() < 10) {
            var Day = "0" + oneDay.getDate();
        }
        else {
            var Day = oneDay.getDate();
        }
        oneDay = Day + "/" + Month + "/" + oneDay.getFullYear();
        $("#CP_TextBoxCheckout").removeClass("schfldserr");
        $("#CP_TextBoxCheckout").val(oneDay);
        $("#CP_TextBoxCheckout").datepicker({
            startDate: e.date,
            format: "dd/mm/yyyy"
        }).on('changeDate', function (e) {
            $("#CP_TextBoxCheckout").removeClass("schfldserr");
        });
        return false;
    });

    $('#ddlRoomno').on('change', function () {
        selectRooms();
    });

    return false;
});


var app = angular.module('WDRewards', ['ngAnimate', 'ui.bootstrap']);

//app.directive('slideit', function () {
//    return {
//        restrict: 'A',
//        replace: true,
//        scope: {
//            slideit: '='
//        },
//        template: '<ul class="bxslider">' +
//                    '<li ng-repeat="slide in slides">' +
//                      '<img ng-src="{{slide.src}}" alt="" />' +
//                    '</li>' +
//                   '</ul>',
//        link: function (scope, elm, attrs) {
//            elm.ready(function () {
//                scope.$apply(function () {
//                    scope.slides = scope.slideit;
//                });
//                elm.bxSlider({
//                    adaptiveHeight: true,
//                    mode: 'horizontal',
//                    auto: true,
//                    controls: true,
//                    hideControlOnEnd: false,
//                    autoControls: true,
//                    pager: false
//                });
//            });
//        }
//    };
//});
app.controller('MainCtrl', function ($scope, $http) {
    //$scope.base = 'http://bxslider.com';
    //$scope.images = [
    //     //{ src: 'Images/banr1.jpg' },
    //     { src: 'Images/banr2.jpg' },
    //     { src: 'Images/banr3.jpg' },
    //     { src: 'Images/banr4.jpg' },
    //     { src: 'Images/banr5.jpg' },
    //     { src: 'Images/banr6.jpg' },
    //];
    $scope.ModifyHotel = function () {
       // alert('hhh');
        $("#modifyDetails").slideToggle();
    }

    selectRooms();
    $scope.getLocation = function (val) {
        return $http({
            url: "FlightLBMSServices.aspx/GetAirfields",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                addresses.push(item);
            });
            return addresses;
        });
    };
    $scope.getAllAirlines = function (val) {
        return $http({
            url: "FlightLBMSServices.aspx/GetCarriers",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var carriers = [];
            //var IATACode = [];
            angular.forEach(res.data.d, function (item) {
                carriers.push({ name: item.split('-')[0], iata: item.split('-')[1] });
            });
            return carriers;
        });
    };
    $scope.onSelectAirlines = function ($item, $model, $label) {
        $("#CP_hdnCarrier").val($item.iata);
    };
    $scope.showOneway = function () {
        $("#retnli").hide();
        $("#hdntrip").val('false');
        $("#oneway").addClass('rdbselect');
        $("#return").removeClass('rdbselect');
        
    }
    $scope.empty = function (id) {
        $("#CP_" + id).val('');
        $("#CP_" + id).removeClass("schfldserr");
    }
    $scope.showReturn = function () {
        $("#retnli").show();
        $("#hdntrip").val('true');
        $("#oneway").removeClass('rdbselect');
        $("#return").addClass('rdbselect');
    }
    $scope.FlightValidation = function () {
        //var msg = "";
        var cnt = 0;
        if (($("#CP_textBoxFrom").val() == '' || $("#CP_textBoxFrom").val() == null) || $("#CP_textBoxFrom").val() == "Enter City or Airport" || $("#CP_textBoxFrom").val() == "*Required") {
            // msg = "Please Provide The Departure of the Flight.";
            $("#CP_textBoxFrom").addClass("schfldserr");
            $("#CP_textBoxFrom").val("*Required");
            cnt++;
        }
        if (($("#CP_textBoxTo").val() == '' || $("#CP_textBoxTo").val() == null) || $("#CP_textBoxTo").val() == "Enter City or Airport" || $("#CP_textBoxTo").val() == "*Required") {
            //  msg = "Please Provide The Destination of the Flight.";
            $("#CP_textBoxTo").addClass("schfldserr");
            $("#CP_textBoxTo").val("*Required");
            cnt++;
        }
        if (($("#CP_txtDepart").val() == '' || $("#CP_txtDepart").val() == null || $("#CP_txtDepart").val() == 'dd/mm/yyyy' || $("#CP_txtDepart").val() == "*Required")) {
            //msg = "Please Provide The Departure Date.";
            $("#CP_txtDepart").addClass("schfldserr");
            $("#CP_txtDepart").val("*Required");
            cnt++;
        }
        if ($("#hdntrip").val() == 'true' && ($("#CP_txtReturn").val() == '' || $("#CP_txtReturn").val() == null) || $("#CP_txtReturn").val() == 'dd/mm/yyyy' || $("#CP_txtReturn").val() == "*Required") {
            //msg = "Please Provide The Return Date";
            $("#CP_txtReturn").addClass("schfldserr");
            $("#CP_txtReturn").val("*Required");
            cnt++;
        }
        if (parseInt($("#CP_lblInfantCount").text()) > parseInt($("#CP_lblAdultCount").text())) {
            //msg = "Number of infants is more than adults.";
            $("#CP_lblInfantCount").addClass("schfldserr");
            cnt++;
        }
        //if (msg.length > 0) {
        //    $("#requiredValidation")[0].innerHTML = msg;
        //    showdiv('divrequiredValidation');
        //    setTimeout(blank('divrequiredValidation'), 2000);
        //    return false;
        //}
        //else {
        //    return clicktoflightsearch();
        //}
        if (cnt == 0) {
            debugger;
            return clicktoflightsearch();
        }
    }
    clicktoflightsearch = function () {
        debugger;
        var strcityfrom = "cityfrom=" + $("#CP_textBoxFrom").val().split(',')[2] + "&";
        var strcityto = "cityto=" + $("#CP_textBoxTo").val().split(',')[2] + "&";
        var strFrom = "departure=" + $("#CP_textBoxFrom").val().split(',')[0] + "&";
        var strTo = "arrival=" + $("#CP_textBoxTo").val().split(',')[0] + "&";
        var strDepartDate = "departuredate=" + $("#CP_txtDepart").val() + "&";
        var strReturnDate = "arrivaldate=" + $("#CP_txtReturn").val() + "&";
        var isReturn = "isReturn=" + $("#rdbRoundTrip").is(":checked").toString() + "&";
        //var isReturn = "isReturn=" + $("#hdntrip").val().toString() + "&";
        var strAirlinePrefernce = "airline=" + $("#CP_txtAirline").val() + "&";
        var strAirlineIATACode = "airlineIATACode=" + $("#CP_hdnCarrier").val() + "&";
        var strAdultNo = "adult=" + $("#CP_DropDownListAdult").val() + "&";
        var strChildNo = "child=" + $("#CP_DropDownListChild").val() + "&";
        var strInfantNo = "infant=" + $("#CP_DropDownListInfant").val() + "&";
        var strEconomy = "economy=" + $("#CP_dropDownListEconomy").val();
        var queryString = strcityfrom + strcityto + strFrom + strTo + strDepartDate + strReturnDate + isReturn + strAirlinePrefernce + strAirlineIATACode + strAdultNo + strChildNo + strInfantNo + strEconomy;
        window.location = "SearchPage.aspx?" + queryString;
        return false;
    }

    $scope.getAllHotelCities = function (val) {
        //alert('hotel');
        return $http({
            url: "FlightLBMSServices.aspx/GetAllHotelCities",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                addresses.push(item);
            });
            return addresses;
        });
    };

    $scope.SearchRoom = function () {
        //alert('serch')
        SearchRooms();
    }
   
    ///**************Tour 

    $scope.getAllTourCities = function (val) {
        debugger;
       
        return $http({
            url: "SearchTour.aspx/GetDestinationList",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            //alert('hi')
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                var list = item.split('|')
                var tourid=list[0]
                var name = list[1]
                $("#txtDestinationtourId").val(tourid);
                addresses.push(name);


            });
            return addresses;
        });
    };

    $scope.TourValidation = function () {

        debugger;
        var cnt = 0;
        if (($("#CP_txtDestination").val() == '' || $("#CP_txtDestination").val() == null) || $("#CP_txtDestination").val() == "Destination" || $("#CP_txtDestination").val() == "*Required") {

            $("#CP_txtDestination").addClass("schfldserr");
            $("#CP_txtDestination").val("*Required");
            cnt++;
        }

        if (cnt == 0) {
            debugger;
            return clicktotoursearch();
        }
    }
    
    clicktotoursearch = function () {
        debugger;
        $.ajax({
            type: 'POST',
            url: 'SearchTour.aspx/SetDestinationId',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'pstrDestinationId':'" + $("#txtDestinationtourId").val() + "'}",
            cache: false,
            success: function (msg) {
                if (msg.d) {
                    window.location.href = "ProductList.aspx";
                }
            },
            error: function (errmsg) {
            }
        });

    }
});

//*********Hotel Search**********

function SearchRooms() {
    debugger;
    //alert('SearchRooms')
    var room = [];
    room = $("#RoomCount .clearfix");
    var strRoomAdult = "";
    var strRoomChild = "";
    var strRoom = "";
    for (var count = 0; count < room.length; count++) {
        var selectTag = [];
        selectTag = $(room[count]).find('select');
        strRoomAdult += $(selectTag[0]).val() + ",";
        strRoomChild += $(selectTag[1]).val() + ",";
    }
    strRoom = strRoomAdult + ":" + strRoomChild;
    $("#CP_hdnRoomString").val(strRoom);
    if (validateHotelFields()) {
        var strCity = "city=" + $("#CP_txtCity").val() + "&";
        var strCheckIn = "checkin=" + $("#CP_TextBoxCheckin").val() + "&";
        var strCheckout = "checkout=" + $("#CP_TextBoxCheckout").val() + "&";
        var strRoomstring = "roomstring=" + $("#CP_hdnRoomString").val() + "&";
        var isRedeemMiles = "isRedeemMiles=True";
        var queryString = strCity + strCheckIn + strCheckout + strRoomstring + isRedeemMiles;
        window.location = "HotelsSearchWait.aspx?" + queryString;
        return false;
    } else {
        return false;
    }
}

validateHotelFields = function () {
    var cnt = 0;
    if ($("#CP_txtCity").val().length == 0 || $("#CP_txtCity").val() == "Enter City Name" || $("#CP_txtCity").val() == "*Required") {
        //  msg = "Please Enter City Name";
        $("#CP_txtCity").addClass("schfldserr");
        $("#CP_txtCity").val("*Required");
        cnt++;
    }
    if ($("#CP_TextBoxCheckin").val().length == 0 || $("#CP_TextBoxCheckin").val() == "Check In" || $("#CP_TextBoxCheckin").val() == "*Required") {
        //msg = "Please Enter CheckIn Date";
        $("#CP_TextBoxCheckin").addClass("schfldserr");
        $("#CP_TextBoxCheckin").val("*Required");
        cnt++;
    }
    if ($("#CP_TextBoxCheckout").val().length == 0 || $("#CP_TextBoxCheckout").val() == "Check out" || $("#CP_TextBoxCheckout").val() == "*Required") {
        //msg = "Please Enter CheckOut Date";
        $("#CP_TextBoxCheckout").addClass("schfldserr");
        $("#CP_TextBoxCheckout").val("*Required");
        cnt++;
    }
    if (cnt == 0) {
        return true;
    }
    else {
        return false;
    }
}
//****** End Hotel Search**********

function selectRooms() {
    var noofRooms = $('#ddlRoomno :selected').val();
    var strTemplate = "";
    $("#RoomCount").html('');
    for (var count = 0; count <= noofRooms - 1; count++) {

        if (count != 4) {
            strTemplate += '<div class="room1 clearfix">';
        }
        else {
            strTemplate += '<div class="room4 clearfix lst">';

        }
        strTemplate += "<label>Room " + (count + 1) + "</label>";
        strTemplate += "<div class='radult'><div class='inputbg'><select><option value='1'>1 Adult</option><option value='2'>2 Adult</option><option value='3'>3 Adult</option><option value='4'>4 Adult</option></select></div></div>";
        strTemplate += "<div class='rchild'><div class='inputbg'><select><option value='0'>0 Child</option><option value='1'>1 Child</option><option value='2'>2 Child</option></select></div></div>";
        strTemplate += "</div>";

    }
    $("#RoomCount").html(strTemplate);

}