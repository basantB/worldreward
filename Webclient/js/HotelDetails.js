﻿
$(document).ready(function () {
    GetHotelInfo()
    BindHotelDetails();
    BindNextHotel();
});

//$(window).scroll(function () {
//    var WinWidth = $(window).width();
//    if (WinWidth > 800) {
//        if ($(this).scrollTop() > 70) {
//            $('#MasterUser').css({
//                margin: "48px 0 20px",
//                position: "fixed",
//                right: "5%",
//                top: "0"
//            });
//            //$("#modifyDetails").slideUp("1000");
//            $("#NextHotelList").css({ margin: "280px 0 0px", });
//        }
//        else {
//            $('#MasterUser').css({
//                margin: "0px 0 20px 0", position: "inherit", right: "",
//                top: ""
//            });
//            //$("#modifyDetails").slideDown("1000");
//            $("#NextHotelList").css({ margin: "100px 0 0 0", });
//        }
//    }
//});
function onddlhotelchange() {
    var hotelroomcode = $("#ddlroomtype option:selected").val();
    $.ajax({
        type: "POST",
        url: "HotelDetails.aspx/GetHotelDetails",
        data: "{'hotelroomcode':'" + hotelroomcode + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            var data = msg.d;
            var objHotel = $.parseJSON(data);

            var index;
            for (i = 0; i < objHotel.roomrates.RoomRate.length; i++) {
                if (objHotel.roomrates.RoomRate[i].roomtype.roomtypecode == hotelroomcode) {
                    index = i;
                }

            }

            $("#minrate").html("Average Price " +  objHotel.roomrates.RoomRate[index].ratebreakdown.rate[0].RatePoint  + " WorldRewardZ X " + objHotel.roomrates.RoomRate[index].ratebreakdown.rate.length + " night(s)");
            $("#totalprice").html( objHotel.roomrates.RoomRate[index].TotalPoints + "<span> WorldRewardZ</span>");

            //$("#minrate").html(CommaSep(objHotel.roomrates.RoomRate[index].ratebreakdown.rate[0].RatePoint) + "<span> Points</span><p style='display: inline;'> (per room per night)</p>");
            //$("#totalprice").html(CommaSep(objHotel.roomrates.RoomRate[index].TotalPoints) + "<span> WorldRewardZ</span><p style='display: inline;'> (per room for " + objHotel.roomrates.RoomRate[index].ratebreakdown.rate.length + " nights)</p>");
            var html = "";
            html += "<div class='button' onclick='return Bookroom(&quot;" + hotelroomcode + "&quot;);'>Book Now</div>";
            $("#appendbtn").html(html);
        },
        error: function (response) {
            alert(response);
        }
    });
    return false;
}

function BindNextHotel() {
    
    $.ajax({
        type: "POST",
        url: "HotelDetails.aspx/BindNextHotel",
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
           
            HotelNextList = msg.d;
            var HotelList = '';
            HotelList = '<h2>Similar Hotels</h2>';
            HotelList += '<ul>';
            for (icount = 0; icount < HotelNextList.length; icount++) {
                HotelList += '<li><a onclick="return getHotelDetails(' + HotelNextList[icount].hotelid + ')"><div class="smrhtlimg"><img src="http://www.cleartrip.com/places/hotels' + HotelNextList[icount].basicinfo.thumbnailimage + '" /></div>';
                HotelList += '<div><p class="smrhtlnm">' + HotelNextList[icount].basicinfo.hotelname + '</p>';
                HotelList += '<p class="pnts">' + HotelNextList[icount].roomrates.RoomRate[0].TotalPoints+ ' WorldRewardZ </p>';
                //HotelList += '<p class="pnts">' + CommaSep(HotelNextList[icount].roomrates.RoomRate[0].TotalPoints) + ' WorldRewardZ </p>';
                HotelList += '</div></a>';
                for (irating = 0; irating < parseInt(HotelNextList[icount].basicinfo.starrating) ; irating++) {
                    HotelList += '<img src="Images/star.png">';
                }

                HotelList += '</li>';
            }
            HotelList += '<div class="clr"></div></ul>';
            $("#NextHotelList").append(HotelList);
        }
    });
    return false;
}
function BindHotelDetails() {
    $.ajax({
        type: "POST",
        url: "HotelDetails.aspx/BindHotelDetails",
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            HotelResponse = msg.d;
            var html2 = "";
            var HotelRating = "";
            var rptRoom = "";
            rptRoom += "<label>Select Room Type: </label><div class='slrmty'><select id='ddlroomtype' onchange ='return onddlhotelchange();'>";
            for (icount = 0; icount < HotelResponse[0].roomrates.RoomRate.length; icount++) {
                rptRoom += "<option value='" + HotelResponse[0].roomrates.RoomRate[icount].roomtype.roomtypecode + "'>" + HotelResponse[0].roomrates.RoomRate[icount].roomtype.roomdescription + "</option>";
            }
            rptRoom += "</select></div>";
            $("#minrate").html("Average Price " +  HotelResponse[0].roomrates.RoomRate[0].ratebreakdown.rate[0].RatePoint + " WorldRewardZ X " + HotelResponse[0].roomrates.RoomRate[0].ratebreakdown.rate.length + " night(s)");
            $("#totalprice").html( HotelResponse[0].roomrates.RoomRate[0].TotalPoints + "<span> WorldRewardZ</span>");
            html2 += "<div class='button' onclick='return Bookroom(&quot;" + HotelResponse[0].roomrates.RoomRate[0].roomtype.roomtypecode + "&quot;);'>Book Now</div>";
            $("#ddlhotelroom").append(rptRoom);
            for (irating = 0; irating < parseInt(HotelResponse[0].basicinfo.starrating) ; irating++) {
                HotelRating += '<img src="Images/star.png">';
            }
            $("#appendbtn").html(html2);
            $("#divrating").append(HotelRating);
        }
    });
    return false;
}
function Bookroom(roomtypecode) {
    var pstrroomtypecode = roomtypecode;
    $.ajax({
        type: "POST",
        url: "HotelDetails.aspx/Bookroom",
        data: "{'pstrroomtypecode':'" + pstrroomtypecode + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            window.location.href = data.d;
            return false;
        },
        error: function (response) {
            alert(response);
        }
    });
    return false;
}
function GetHotelInfo() {
    $.ajax({
        type: "POST",
        url: "HotelDetails.aspx/GetHotelInfo",
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            //Call
            value = $.parseJSON(msg.d);
            $("#HotelName").text(value.basicinfo[0].hotelname);
            $("#HotelAddress").append(value.basicinfo[0].address + ", ");
            $("#HotelCity").append(value.basicinfo[0].city + '&nbsp;' + value.basicinfo[0].zip + value.basicinfo[0].country);

            if (value.basicinfo[0].communicationinfo.email != "") {
                $("#contact2").append('<li><span class="ico_email"></span><a href="mailto:' + value.basicinfo[0].communicationinfo.email + '">' + value.basicinfo[0].communicationinfo.email + '</a></li>');
            }
            else {
                $("#contact2").append('<li><span class="ico_email"></span><a href="#">Not Available</a></li>');
            }
            if (value.basicinfo[0].communicationinfo.fax != "") {
                $("#contact2").append('<li><span class="ico_fax"></span>' + value.basicinfo[0].communicationinfo.fax + '</li>');
            }
            else {
                $("#contact2").append('<li><span class="ico_fax"></span>Not Available</li>');
            }
            if (value.basicinfo[0].communicationinfo.phone != "") {
                $("#contact1").append('<li><span class="ico_phone"></span>' + value.basicinfo[0].communicationinfo.phone + '</li>');
            }
            else {
                $("#contact1").append('<li><span class="ico_phone"></span>Not Available</li>');
            }
            if (value.basicinfo[0].communicationinfo.website != "") {
                $("#contact1").append('<li><span class="ico_weblink"></span><a href="' + value.basicinfo[0].communicationinfo.website + '">' + value.basicinfo[0].communicationinfo.website + '</a></li>');
            }
            else {
                $("#contact1").append('<li><span class="ico_weblink"></span><a href="#">Not Available</a></li>');
            }
            if (value.basicinfo[0].overview != "" || value.otherinfo.Description != "") {
                $("#OverView").append('<p><b>About This Hotel</b></p><p>' + value.basicinfo[0].overview + "</p><br/><p>" + value.otherinfo.Description + "</p>");
            }
            else {
                $("#OverView").append('<h2 class="Header">About This Hotel</h2><p> Not Available</p>');
            }
            $("#divGoogleMaps").append("<div class='amen-popup'><img src='http://maps.googleapis.com/maps/api/staticmap?&size=300x200&markers=" + value.otherinfo.locationinfo.latitude + "," + value.otherinfo.locationinfo.longitude + "&maptype=roadmap&zoom=14&sensor=false'&quot; /></div>");

            var imageid = 0;
            var counter = 1;
            var wideangleimageurl = value.otherinfo.imageinfo[0].wideangleimageurl;
            if (wideangleimageurl.indexOf("http") == -1) {
                wideangleimageurl = "http://www.cleartrip.com/places/hotels" + wideangleimageurl;
            }
            var html = '<img id="Imgwide" class="img_Hotelslider" src="' + wideangleimageurl + '" />';
            $("#BannerImage").append(html);
            $('#divpre').click(function () {
                if (imageid > 0) {
                    imageid = imageid - 1;
                    var wideangleimagePopupurl = value.otherinfo.imageinfo[imageid].wideangleimageurl;
                    if (wideangleimagePopupurl.indexOf("http") == -1) {
                        wideangleimagePopupurl = "http://www.cleartrip.com/places/hotels" + wideangleimagePopupurl;
                    }
                    var temppresrc = wideangleimagePopupurl;
                    $("#Imgwide").attr('src', temppresrc);
                }

            });
            $('#divNext').click(function () {
                if (imageid <= value.otherinfo.imageinfo.length - 1) {
                    imageid = imageid + 1;
                    if (imageid < value.otherinfo.imageinfo.length) {
                        var wideangleimagePopupurl = value.otherinfo.imageinfo[imageid].wideangleimageurl;
                        if (wideangleimagePopupurl.indexOf("http") == -1) {
                            wideangleimagePopupurl = "http://www.cleartrip.com/places/hotels" + wideangleimagePopupurl;
                        }
                        var tempnextsrc = wideangleimagePopupurl;
                        $("#Imgwide").attr('src', tempnextsrc);
                    }
                }
            });
            $('#Imgwide').click(function () { return false; }); // Adds another click event
            $('#Imgwide').off('click');

            //Code for Binding All Amenities in popup
            var html2 = '<div style="float:left;width:100%">'
            var AllAmenities = "";
            for (var countAmenities = 0; countAmenities < value.basicinfo[0].hotelamenities.Amenities.length; countAmenities++) {
                AllAmenities += value.basicinfo[0].hotelamenities.Amenities[countAmenities].category + ",";
                html2 += '<div class="amen-popup"> <table cellspacing="0" cellpadding="0" width="100%" border="0px"><tr><td><b>' + value.basicinfo[0].hotelamenities.Amenities[countAmenities].category + '</b></td></tr>';
                for (var countAmenitiesType = 0; countAmenitiesType < value.basicinfo[0].hotelamenities.Amenities[countAmenities].Amenities.hotelamenity.length - 1; countAmenitiesType++) {
                    AllAmenities += value.basicinfo[0].hotelamenities.Amenities[countAmenities].Amenities.hotelamenity[countAmenitiesType].Value + ",";
                    html2 += '<tr><td>' + value.basicinfo[0].hotelamenities.Amenities[countAmenities].Amenities.hotelamenity[countAmenitiesType].Value + '</td></tr>';
                }
                html2 += '</table></div>';
            }
            html2 += '</div>';
            $("#DivAmenities").append(html2);
            //Code for Binding Basic Amenities
            BindBasicAmenities(AllAmenities);


        },
        error: function (response) {
            alert(response);
        }
    });
    return false;
}
function BindBasicAmenities(AllAmenities) {
    var AmenitiesValue = AllAmenities.split(",");
    var BasicAmenities = "<ul class='htlservs'>";
    for (i = 0; i < AmenitiesValue.length - 1; i++) {
        if (AmenitiesValue[i].toLowerCase() == "Air conditioning".toLowerCase()) {
            BasicAmenities += "<li><img src='Images/AC_Car_Icon.png'></li>";
        }
        if (AmenitiesValue[i].toLowerCase() == "meeting facilities" || AmenitiesValue[i].toLowerCase() == "conference suite" || AmenitiesValue[i].toLowerCase() == "meeting rooms" || AmenitiesValue[i].toLowerCase() == "business center") {
            BasicAmenities += "<li><img src='Images/business_center.png'></li>";
        }
        if (AmenitiesValue[i].toLowerCase() == "restau" || AmenitiesValue[i].toLowerCase() == "restaurant" || AmenitiesValue[i].toLowerCase() == "coffee shop") {
            BasicAmenities += "<li><img src='Images/coffee_shop.png'></li>";
            BasicAmenities += "<li><img src='Images/restaurant.png'></li>";
        }
        if (AmenitiesValue[i].toLowerCase() == "spa" || AmenitiesValue[i].toLowerCase() == "gym" || AmenitiesValue[i].toLowerCase() == "fitness center" || AmenitiesValue[i].toLowerCase() == "health club") {
            BasicAmenities += "<li><img src='Images/gym.png'></li>";
        }
        if (AmenitiesValue[i].toLowerCase() == "pool" || AmenitiesValue[i].toLowerCase() == "outdoor pool") {
            BasicAmenities += "<li><img src='Images/pool.png'></li>";
        }
        if (AmenitiesValue[i].toLowerCase() == "complimentary wi-fi access" || AmenitiesValue[i].toLowerCase() == "internet" || AmenitiesValue[i].toLowerCase() == "wi-fi" || AmenitiesValue[i].toLowerCase() == "wi-fi access") {
            BasicAmenities += "<li><img src='Images/network_blue.png'></li>";
        }
    }
    BasicAmenities += "</ul><div><a onclick='return showAmenities()' class='link'>More Amenities</a></div>";
    $("#BasicAmenities").append(BasicAmenities);
    return false;
}
function showAmenities() {
    $("html").scrollTop(0);
    $("#DivAmenities").addClass("Hotel_Details_Cotent");
    $("#DivAmenities").removeClass("hidecontent");
    $("#Fade").show();
    return false;
}
function showimage() {
    $("html").scrollTop(0);
    $("#divGoogleMaps").addClass("Hotel_Details_Cotent");
    $("#divGoogleMaps").removeClass("hidecontent");
    $("#Fade").show();
    return false;
}
function CloseDetails() {
    $("#DivAmenities").addClass("hidecontent");
    $("#DivAmenities").removeClass("Hotel_Details_Cotent");
    $("#divGoogleMaps").addClass("hidecontent");
    $("#divGoogleMaps").removeClass("Hotel_Details_Cotent");
    $("#Fade").hide();
    return false;
}
function getHotelDetails(hotelId) {
    $.ajax({
        type: "POST",
        url: "HotelResults.aspx/SetHotelId",
        data: "{'pstrHotelId':'" + hotelId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            var linkurl = "HotelDetails.aspx";
            window.location.href = linkurl;
        }
    });
}