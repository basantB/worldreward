﻿function placeholderOnFocus(obj, defaultVal) {
    if (obj.value == "") {
        obj.value = defaultVal;
    } else if (obj.value == defaultVal) {
        obj.value = "";
    } else { }
}
$(document).ready(function () {
    $("#CP_txtDestination").autocomplete({
        source: function (request, response) {
            var list = [];
            $.ajax({
                type: 'POST',
                url: 'SearchTour.aspx/GetDestinationList',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: "{'prefixText':'" + request.term.toString() + "'}",
                cache: false,
                success: function (msg) {
                    response(msg.d);
                },
                error: function (errmsg) {
                }
            });
            response(list);
        },
        select: function (event, ui) {
            var arr = ui.item.label.split('|');

            $("#CP_txtDestination").val(arr[1]);
            $("#CP_hdnDestinationid").val(arr[0]);
            return false;
        },
        focus: function (event, ui) {
            var arr = ui.item.label.split('|');

            $("#CP_txtDestination").val(arr[1]);
            $("#CP_hdnDestinationid").val(arr[0]);
            return false;
            event.preventDefault(); // Prevent the default focus behavior.
        },
        minLength: 3,
        scroll: true,
        scrollHeight: 300

    });

    $.ui.autocomplete.prototype._renderItem = function (ul, item) {
        var strResult = item.label.toString();
        var strArr = [];
        strArr = strResult.split('|');
        var countryname = strArr[1];
        var autoResult = "<span style='font-size:11px;padding-botom:5px'>" + countryname + "</span>";
        return $("<li style='width:100%'></li>").data("item.autocomplete", item).append("<a style='line-height: 14px;height: auto;width:auto'>" + autoResult + "</a>").appendTo(ul);
    };


});

function goTOProducts() {
    var msg = "";
    if (($("#CP_txtDestination").val() == '' || $("#CP_txtDestination").val() == null) || $("#CP_txtDestination").val() == "Enter Destination") {
        msg = "Please Enter Destination";
    }
    if (msg.length > 0) {
        $("#requiredValidation")[0].innerHTML = msg;
        return false;
    }
    else {
        $.ajax({
            type: 'POST',
            url: 'SearchTour.aspx/SetDestinationId',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: "{'pstrDestinationId':'" + $("#CP_hdnDestinationid").val() + "'}",
            cache: false,
            success: function (msg) {
                if (msg.d) {
                    window.location.href = "ProductList.aspx";
                }
            },
            error: function (errmsg) {
            }
        });

    }
}