﻿function LoginValidation() {
    var msg = "";
    $("#CP_ErrorMsgContainer").hide();
    $("#LoginValidation")[0].innerHTML = "";
    $("#CP_lblLoginError").html('');
    var vdf = $("#CP_txtMemberName").val();

    //if ($("#CP_MemberName").val().length == 0) {

    //    msg += "Please enter CBI Rewards Member No.<br/>";
    //}
    if ($("#CP_txtPassword").val().length == 0) {
        msg += "Please enter password.<br/>";
    }
    if (($('#CP_txtPassword').val().length < 8) && ($('#CP_txtPassword').val().length > 1)) {
        msg += "Password length has to be minimum eight characters.";
    }
    if (msg.length > 0) {
        $("#CP_ErrorMsgContainer").show();
        $("#LoginValidation")[0].innerHTML = msg;
    }

    else {
        var MemberId = $("#CP_txtMemberName").val();
        var password = $("#CP_txtPassword").val();
        jQuery("#overlay").css('display', 'block');
        jQuery("#popup").css('display', 'block');
        jQuery("#popup").fadeIn(500);
        var RememberMe = new Boolean();
        RememberMe = $("#CP_chkRememberMe")[0].checked;
        $.ajax({
            url: 'Login.aspx/SigninUser',
            type: 'POST',  // or get
            contentType: 'application/json; charset =utf-8',
            data: "{'pstrMemberid':'" + MemberId.toString() + "'" + "," + "'pstrPassword':'" + password.toString() + "'" + "," + "'pboolRememberMe':'" + RememberMe + "'}",
            dataType: 'json',
            success: function (data) {
                try {

                    var newData = data.d;

                    if ((newData != "") || newData == null) {

                        if (data.d == "AccountLock") {
                            $("#LoginValidation")[0].innerHTML = "";
                            $("#CP_ErrorMsgContainer").show();
                            $("#CP_lblLoginError").html('Your Account is Locked.Please <a class="ErrorMessageLink" onclick="showMyLoginDiv();">click here</a> to unlock the same.');
                        }
                        else if (data.d == "AuthenticationFailed") {
                            $("#LoginValidation")[0].innerHTML = "";
                            $("#CP_ErrorMsgContainer").show();
                            $("#CP_lblLoginError").html('Please check the login details you have entered and try again.');
                        }
                        else if (data.d == "IncorrectFormat") {
                            $("#LoginValidation")[0].innerHTML = "";
                            $("#CP_ErrorMsgContainer").show();
                            $("#CP_lblLoginError").html('Password must contain one non-alpha character,one upper case character,one lower case character.');
                        }
                        else {
                            $("#LoginValidation")[0].innerHTML = "";
                            window.location.href = data.d;
                        }
                    }
                    else {
                        $("#LoginValidation")[0].innerHTML = "";
                        $("#CP_ErrorMsgContainer").show();
                        $("#CP_lblLoginError").html('Please check the login details you have entered and try again.');

                    }
                }
                catch (e) {
                    alert(e);
                    jQuery("#overlay").css('display', 'none');
                    jQuery("#popup").css('display', 'none');
                    return false;
                }
                return false;
                jQuery("#overlay").css('display', 'none');
                jQuery("#popup").css('display', 'none');
            },
            error: function (errmsg) {

            }
        });
    }
    jQuery("#overlay").css('display', 'none');
    jQuery("#popup").css('display', 'none');
    return false;
};


function redirectLocation(url) {
    window.location.href = url;
    return false;
};

function ActivationOTPValidation() {
    $("#CP_ErrorMsgContainer").hide();
    var msg = "";
    $('#CP_lblActivationError')[0].InnerText = '';
    if ($('#CP_txtAccountNumber').val() == '') {
        msg += "Please enter your CBI Rewards Member No.<br/>";
    }
    if ($('#CP_txtEmail').val() == '') {
        msg += "Please enter your CBI Rewards Email Address.<br/>";
    }
    if ($('#CP_txtMobNo').val() == '') {
        msg += "Please enter your CBI Rewards Mobile Number.<br/>";
    }
    if (msg.length > 0) {
        $("#CP_ErrorMsgContainer").show();
        $("#validationResult")[0].innerHTML = msg;
    }
    else {
        var MemberId = $("#CP_txtAccountNumber").val();
        var EmailId = $("#CP_txtEmail").val();
        var MobNo = $("#CP_txtMobNo").val();
        jQuery("#overlay").css('display', 'block');
        jQuery("#popup").css('display', 'block');
        jQuery("#popup").fadeIn(500);
        $.ajax({
            url: 'ActivateMembership.aspx/GenerateOTP',
            type: 'POST',  // or get
            contentType: 'application/json; charset =utf-8',
            data: "{'pstrMemberid':'" + MemberId.toString() + "'" + "," + "'pstrEmailId':'" + EmailId.toString() + "'" + "," + "'pstrMobNo':'" + MobNo.toString() + "'}",
            dataType: 'json',
            success: function (data) {
                try {
                    var newData = data.d;
                    if (data.d != "") {
                        if (data.d == "Success") {
                            //                            $.ajax({
                            //                                url: 'ActivateMembership.aspx/GetmemberEmailID',
                            //                                type: 'POST',  // or get
                            //                                contentType: 'application/json; charset =utf-8',
                            //                                data: "{'pstrMemberid':'" + MemberId.toString() + "'" + "," + "'pstrNationalId':'" + NationalId.toString() + "'}",
                            //                                dataType: 'json',
                            //                                success: function (data) {
                            //                                    try {
                            //                                        var newData = data.d;
                            //                                        if (data.d != "") {
                            //                                            $("#ct_CBI_txtEmailId").val(data.d);
                            //                                        }
                            //                                    }
                            //                                    catch (e) {
                            //                                    }
                            //                                    return false;
                            //                                },
                            //                                error: function (errmsg) {

                            //                                }
                            //                            });

                            $("#CP_lblMessagesDetails").text('');
                            $("#CP_ErrorMsgContainer").hide();
                            $("#CP_lblMessagesDetails")[0].innerHTML = "Thank you. OTP has been sent to both your registered email address and Mobile number. <br/>Once password received please click <a class='ErrorMessageLink' style='cursor:pointer' onclick='showMyDiv();'>here</a> to enter your CBI Rewards Member Number and OTP. You will then be prompted to create new password of your choice, which you will use to login to CBI Rewards everytime you wish to view your account or redeem points.";
                            $("#OTPRId").hide();
                            $("#DivSapceMsg").hide();
                            $("#OTPRId2").hide();
                            $("#OTPRId3").hide();
                            $("#CP_txtAccountNumber").val('');
                            $("#LoginDiv").hide();
                        }
                        else {
                            $("#CP_lblMessagesDetails").text('');
                            $("#CP_ErrorMsgContainer").show();
                            $("#CP_ErrorMsgContainer")[0].innerHTML = data.d;
                        }
                        //   $("#validationResult")[0].innerHTML = "";
                        //   $("#divMessge").show();
                    }
                    else {

                    }
                }
                catch (e) {
                    alert(e);
                    jQuery("#overlay").css('display', 'none');
                    jQuery("#popup").css('display', 'none');
                    return false;
                }
                jQuery("#overlay").css('display', 'none');
                jQuery("#popup").css('display', 'none');
                return false;
            },
            error: function (errmsg) {

            }
        });
        jQuery("#overlay").css('display', 'none');
        jQuery("#popup").css('display', 'none');
        return false;

    }
};

function ActivationValidation() {

    var msg = "";
    $("#validationResult")[0].innerHTML = '';
    if ($('#CP_txtAccountNumber').val() == '') {
        msg += "Please enter CBI Rewards Member No.<br/>";
    }
    if ($('#CP_txtOTP').val() == '') {
        msg += "Please enter received One Time Password.<br/>";
    }
    if ($('#CP_txtPassword').val() == '') {
        msg += "Please enter new password.<br/>";
    }
    if (($('#CP_txtPassword').val().length < 8) && ($('#CP_txtPassword').val().length > 1)) {
        msg += "New password field has to be minimum eight characters." + "<br/>";
    }
    if ($('#CP_txtConfirmPassword').val() == '') {
        msg += "Please enter confirm new password.<br/>";
    }
    if (($('#CP_txtConfirmPassword').val().length < 8) && ($('#CP_txtConfirmPassword').val().length > 1)) {
        msg += "Confirm new password field has to be minimum eight characters." + "<br/>";
    }
    if (($('#CP_txtPassword').val() != $('#CP_txtConfirmPassword').val()) && ($('#CP_txtPassword').val().length > 1)) {
        msg += "The new password and the confirmed new password must be same." + "<br/>";
    }
    if ((!CheckPassword(($('#CP_txtConfirmPassword').val()))) && (!CheckPassword(($('#CP_txtConfirmPassword').val())))) {
        msg += "Please refer guidelines to create your password.<br/>";
    }
    if ($('#CP_txtEmailId').val() == '') {
        msg += "Please enter EmailAddress.<br/>";
    }
    if (msg.length > 0) {

        $("#validationResult")[0].innerHTML = msg;
        $("#CP_ErrorMsgContainer").show();
        return false;
    }
    else {
        var MemberId = $("#CP_txtAccountNumber").val();
        var pstrOTP = $("#CP_txtOTP").val();
        var pstrPwd = $("#CP_txtPassword").val();
        jQuery("#overlay").css('display', 'block');
        $("#CP_ErrorMsgContainer").hide();
        jQuery("#popup").css('display', 'block');
        jQuery("#popup").fadeIn(500);
        $.ajax({
            url: 'ActivateMembership.aspx/MemberActivation',
            type: 'POST',  // or get
            contentType: 'application/json; charset =utf-8',
            data: "{'pstrMemberid':'" + MemberId.toString() + "'" + "," + "'pstrOTP':'" + pstrOTP.toString() + "'" + "," + "'pstrPwd':'" + pstrPwd.toString() + "'}",
            dataType: 'json',
            success: function (data) {
                try {
                    var newData = data.d;
                    if (data.d != "") {
                        $("#validationResult")[0].innerHTML = "";
                        $("#CP_lblMessagesDetails").text('');
                        $("#CP_ErrorMsgContainer").hide();
                        //    $("#ct_CBI_lblMessagesDetails")[0].innerHTML = data.d + ' <br />';
                        if (data.d == "Success") {
                            '';
                            $("#CP_lblMessagesDetails")[0].innerHTML += 'Thank you for activating your CBI Rewards account. Your account is all setup and you can now redeem your CBI Rewards points online whenever you wish by logging onto <a href="http://103.8.125.215/cbirewards/" target="_blank" style="color: #1c1718;padding: 0;"><strong>cbirewards.cbiuae.com</strong></a>. The exciting redemption options available to you include:<ul><li>Airline tickets &ndash; any airline, any date</li><li>Hotel bookings &ndash; anywhere in the world,no blackout dates</li><li>Car hire &ndash; helping you get around whenever you’re travelling</li><li>Online shopping &ndash; including electrical,electronic and designer goods</li><li>CashBack &ndash; credited straight back to your credit card</li></ul>';
                            $("#CP_lblMessagesDetails")[0].innerHTML += '<p>To change your password, please visit the <strong>&ldquo;Change Password&rdquo;</strong> section under <strong>&ldquo;My Account&rdquo;</strong> available on <a href="http://103.8.125.215/cbirewards/" target="_blank" style="color: #1c1718; padding: 0;"><strong>cbirewards.cbiuae.com</strong></a> and follow the instructions for creating a secure password.</p><p>Remember, earning CBI Rewards points is as easy as 1,2,3 with our simple earning structure:</p>';
                            $("#CP_lblMessagesDetails")[0].innerHTML += '<ul><li>1 point per AED 1.00 on all transactions made in UAE.</li><li>2 points per AED 1.00 on all transactions made in UAE greater than AED 2,000.</li><li>3 points per AED 1.00 on all transactions in foreign currency made outside UAE.</li></ul>';
                            $("#CP_lblMessagesDetails")[0].innerHTML += '<p>If you have any queries related to the program please check the Frequently Asked Questions section of our website<a href="FAQs.aspx" target="_blank" style="color: #1c1718; padding: 0;"><strong> cbireards.cbiuae.com</strong></a> or alternatively call our on contact centre on 800 224.</p>';
                            $("#CP_lblMessagesDetails")[0].innerHTML += '<br><br><p>Please click <a class="ErrorMessageLink" href="StatementSummary.aspx">here</a> to access your CBI Rewards account.</p>';
                            $("#divMessge").show();
                            $("#ActivateOTP").hide();
                            $("#terms").hide();
                            $("#ActivateButton").hide();
                            $("#divPwd").hide();
                            $("#divConfiPwd").hide();
                            $("#DivSapceMsg").hide();
                            $("#OTPRId").hide();
                            $("#OTPRId2").hide();
                            $("#CP_ErrorMsgContainer").hide();
                            $("#validationInfo").hide();
                            $("#CP_lblMessagesDetails").css("color", "black");
                        }
                        else if (data.d == "IncorrectFormat") {
                            $("#validationResult")[0].innerHTML = '';
                            $("#validationResult")[0].innerHTML = 'Password must contain one numeric digit,one upper case character,one lower case character,one special character (!@#$%*()?).';
                            $("#CP_lblMessagesDetails").text('');
                        }
                        else {
                            $("#divMessge").show();
                            // $("#ct_CBI_lblMessagesDetails").css("color", "#ff0000");
                            //$("#validationInfo").hide();
                            $("#CP_ErrorMsgContainer").show();
                            $("#validationResult")[0].innerHTML = 'One or more of the credentials you have entered is incorrect and does not match with our records. Please reenter your details so that we can activate your Account. Should you continue to receive this message please call 800 224 where we can provide further assistance.';
                        }
                    }
                    else {

                    }
                }
                catch (e) {
                    alert(e);
                    jQuery("#overlay").css('display', 'none');
                    jQuery("#popup").css('display', 'none');
                    return false;
                }
                jQuery("#overlay").css('display', 'none');
                jQuery("#popup").css('display', 'none');
                return false;
            },
            error: function (errmsg) {

            }
        });
        jQuery("#overlay").css('display', 'none');
        jQuery("#popup").css('display', 'none');
        return false;

    }
};

function ForgotPassword() {
    var msg = "";
    $("#CP_ErrorMsgContainer").hide();
    $("#ForgotPasswordValidation")[0].innerHTML = "";
    if ($("#CP_txtMemberShipReference").val().length == 0) {
        msg = "Please Enter CBI Rewards Member No.";
    }
    if (msg.length > 0) {
        $("#ForgotPasswordValidation")[0].innerHTML = msg;
        $("#CP_ErrorMsgContainer").show();
        return false;
    }
    else {

        var MemberId = $("#CP_txtMemberShipReference").val();
        jQuery("#overlay").css('display', 'block');
        jQuery("#popup").css('display', 'block');
        jQuery("#popup").fadeIn(500);

        $.ajax({
            url: 'ForgotPassword.aspx/MemberShipReference',
            type: 'POST',  // or get
            contentType: 'application/json; charset =utf-8',
            data: "{'pstrMemberid':'" + MemberId.toString() + "'}",
            dataType: 'json',
            success: function (data) {
                try {

                    var newData = data.d;
                    if (data.d == "Success") {

                        $("#PasswordValidation").html();
                        $("#CP_ErrorMsgContainer").show();
                        $("#CP_lblForgotPasswordError").text('Your New Password reset link has been sent to your registered Email Address.');
                        $("#CP_txtMemberShipReference")[0].value = "";
                        $("#ForgetPasswordDiv").hide();
                        $("#ForgetButtonDiv").hide();
                    }
                    else if (data.d == "Failure") {
                        $("#PasswordValidation").html();
                        $("#CP_ErrorMsgContainer").show();
                        $("#CP_lblForgotPasswordError").text('Please ensure your account is activated.');
                    }
                }
                catch (e) {
                    jQuery("#overlay").css('display', 'none');
                    jQuery("#popup").css('display', 'none');
                    return false;
                }
                jQuery("#overlay").css('display', 'none');
                jQuery("#popup").css('display', 'none');
                return false;
            },
            error: function (errmsg) {

            }
        });
    }
    jQuery("#overlay").css('display', 'none');
    jQuery("#popup").css('display', 'none');
    return false;
};

function UnLockMemberByOTP() {
    var msg = "";
    $("#LoginValidation")[0].innerHTML = "";
    $("#CP_ErrorMsgContainer").hide();

    if ($('#CP_txtMemberName').val() == '') {
        msg += "Please provide a valid CBI Rewards Member No.<br/>";
    }
    if (msg.length > 0) {
        $("#LoginValidation")[0].innerHTML = msg;
        $("#CP_ErrorMsgContainer").show();
    }
    else {
        var MemberId = $("#CP_txtMemberName").val();
        var OTP = $("#CP_txtOTP").val();
        jQuery("#overlay").css('display', 'block');
        jQuery("#popup").css('display', 'block');
        jQuery("#popup").fadeIn(500);
        $.ajax({
            url: 'Login.aspx/UnlockMember',
            type: 'POST',  // or get
            contentType: 'application/json; charset =utf-8',
            data: "{'pstrMemberid':'" + MemberId.toString() + "'" + "," + "'pstrOTP':'" + OTP.toString() + "'}",
            dataType: 'json',
            success: function (data) {
                try {
                    var newData = data.d;
                    if (data.d != "Invalid Credentials") {
                        $("#LoginValidation").css('color', '#000');
                        $("#LoginValidation")[0].innerHTML = "";
                        $("#CP_ErrorMsgContainer").show();
                        $("#LoginValidation")[0].innerHTML = '<br/><br/>' + data.d + ' <br />';
                        $("#OTPDiv").css('display', 'none');
                        $("#UnlockDiv").css('display', 'none');
                        $("#divUserid").css('display', 'none');
                    }
                    else {
                        $("#LoginValidation").css('color', '#ff0000');
                        $("#LoginValidation")[0].innerHTML = "";
                        $("#CP_ErrorMsgContainer").show();
                        $("#LoginValidation")[0].innerHTML = data.d + ' <br />';
                        $("#OTPDiv").css('display', 'block');
                        $("#UnlockDiv").css('display', 'block');
                        $("#divUserid").css('display', 'block');
                    }
                }
                catch (e) {
                    alert(e);
                    jQuery("#overlay").css('display', 'none');
                    jQuery("#popup").css('display', 'none');
                    return false;
                }
                jQuery("#overlay").css('display', 'none');
                jQuery("#popup").css('display', 'none');
                return false;
            },
            error: function (errmsg) {

            }
        });
        jQuery("#overlay").css('display', 'none');
        jQuery("#popup").css('display', 'none');
        return false;

    }
};

function GenerateOTPonLogin() {
    var msg = "";
    $("#LoginValidation")[0].innerHTML = "";
    $("#CP_ErrorMsgContainer").hide();
    if ($('#CP_txtMemberName').val() == '') {
        msg += "Please provide a valid CBI Rewards Member No.<br/>";
    }
    if (msg.length > 0) {
        $("#CP_ErrorMsgContainer").show();
        $("#LoginValidation")[0].innerHTML = msg;
    }
    else {
        var MemberId = $("#ct_CBI_txtMemberName").val();
        jQuery("#overlay").css('display', 'block');
        jQuery("#popup").css('display', 'block');
        jQuery("#popup").fadeIn(500);
        $.ajax({
            url: 'Login.aspx/GenerateOTP',
            type: 'POST',  // or get
            contentType: 'application/json; charset =utf-8',
            data: "{'pstrMemberid':'" + MemberId.toString() + "'}",
            dataType: 'json',
            success: function (data) {
                try {
                    var newData = data.d;
                    if (data.d != "") {
                        $("#LoginValidation")[0].innerHTML = "";
                        $("#LoginValidation").css('color', 'black');
                        $("#CP_ErrorMsgContainer").show();
                        $("#LoginValidation")[0].innerHTML = data.d + ' <br />';
                    }
                    else {

                    }
                }
                catch (e) {
                    alert(e);
                    jQuery("#overlay").css('display', 'none');
                    jQuery("#popup").css('display', 'none');
                    return false;
                }
                jQuery("#overlay").css('display', 'none');
                jQuery("#popup").css('display', 'none');
                return false;
            },
            error: function (errmsg) {

            }
        });
        jQuery("#overlay").css('display', 'none');
        jQuery("#popup").css('display', 'none');
        return false;

    }
};


function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
};

function LoginOnEnter(e) {
    var ENTER_KEY = 13;
    var code = "";
    var code = (e.keyCode ? e.keyCode : e.which);

    if (code == ENTER_KEY) {
        LoginValidation();
        return false;
    }
    return true;
}

function ActivationOnEnter(e) {
    var ENTER_KEY = 13;
    var code = "";
    var code = (e.keyCode ? e.keyCode : e.which);

    if (code == ENTER_KEY) {
        ActivationOTPValidation();
        return false;
    }
    return true;
}

function ForgetPwdOnEnter(e) {
    var ENTER_KEY = 13;
    var code = "";
    var code = (e.keyCode ? e.keyCode : e.which);

    if (code == ENTER_KEY) {
        ForgotPassword();
        return false;
    }
    return true;
}

function ResetPwdOnEnter(e) {
    var ENTER_KEY = 13;
    var code = "";
    var code = (e.keyCode ? e.keyCode : e.which);

    if (code == ENTER_KEY) {
        ResetPassword();
        return false;
    }
    return true;
}



//js for Change Password

function ChangePassword() {
    var msg = "";
    $('#CP_ErrorMsgContainer').hide();
    $("#ChangePasswordValidation")[0].innerHTML = "";
    if ($('#CP_txtOldPassword ').val() == "") {
        msg += "Please provide a valid Current password." + "<br/>";
    }
    if ($('#CP_txtOldPassword ').val() != "") {
        if (($('#CP_txtOldPassword').val().length < 8) && ($('#CP_txtOldPassword').val().length > 1)) {
            msg += "Current password field has to be minimum eight characters." + "<br/>";
        }
        if (($('#CP_txtOldPassword').val() == $('#CP_txtPassword').val()) && ($('#CP_txtOldPassword').val().length > 1)) {
            msg += "New Password should not be same as Current password." + "<br/>";
        }
    }
    if ($('#CP_txtPassword').val() == "") {
        msg += "Please enter your new password." + "<br/>";
    }
    if (($('#CP_txtPassword').val().length < 8) && ($('#CP_txtPassword').val().length > 1)) {
        msg += "New password field has to be minimum eight characters." + "<br/>";
    }

    if ($('#CP_txtNewPassword').val() == "") {
        msg += "Please confirm your new password." + "<br/>";
    }

    if (($('#CP_txtNewPassword').val().length < 8) && ($('#CP_txtNewPassword').val().length > 1)) {
        msg += "Confirm password field has to be minimum eight characters." + "<br/>";
    }
    if ((!CheckPassword(($('#CP_txtNewPassword').val()))) && (!CheckPassword(($('#CP_txtPassword').val())))) {
        msg += "Please refer guidelines to create your password.<br/>";
    }
    if (msg.length > 0) {
        $("#ChangePasswordValidation")[0].innerHTML = msg;
        $('#CP_ErrorMsgContainer').show();
        return false;
    }
    else {
        $("#CP_txtOldPassword").text('');
        $("#CP_txtPassword").text('');
        $("#CP_txtPassword").text('');
        return true;
    }
};

function DateNotNullValidation() {
    var msg = "";
    $('#CP_ErrorMsgContainer').hide();
    $("#DateValidationMsg")[0].innerHTML = "";
    if ($('#CP_FromDate').val() == "") {
        msg += "Please provide a valid From Date." + "<br/>";
    }
    if ($('#CP_Todate').val() == "") {
        msg += "Please provide a valid To Date." + "<br/>";
    }
    if (msg.length > 0) {
        $("#DateValidationMsg")[0].innerHTML = msg;
        $('#CP_ErrorMsgContainer').show();
        return false;
    }
    else {

        return true;
    }
};

function CheckPassword(inputtxt) {
    var decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if (!inputtxt.match(decimal)) {
        return false;
    }
    else {
        return true;
    }
}
function ValidateOnEnter(e) {

    var ENTER_KEY = 13;
    var code = "";
    var code = (e.keyCode ? e.keyCode : e.which);

    if (code == ENTER_KEY) {
        ChangePassword();
        return false;
    }
    return true;
};
function ChangePassword() {

    var msg = "";
    $('#CP_ErrorMsgContainer').hide();
    $("#ChangePasswordValidation")[0].innerHTML = "";
    if ($('#CP_txtOldPassword ').val() == "") {
        msg += "Please provide a valid Current password." + "<br/>";
    }
    if ($('#CP_txtOldPassword ').val() != "") {
        if (($('#CP_txtOldPassword').val().length < 8) && ($('#CP_txtOldPassword').val().length > 1)) {
            msg += "Current password field has to be minimum eight characters." + "<br/>";
        }
        if (($('#CP_txtOldPassword').val() == $('#CP_txtPassword').val()) && ($('#CP_txtOldPassword').val().length > 1)) {
            msg += "New Password should not be same as Current password." + "<br/>";
        }
    }
    if ($('#CP_txtNewPassword').val() != $('#CP_txtPassword').val()) {
        msg += "New Password and confirm password should be same." + "<br/>";
    }
    if ($('#CP_txtPassword').val() == "") {
        msg += "Please enter your new password." + "<br/>";
    }
    if (($('#CP_txtPassword').val().length < 8) && ($('#CP_txtPassword').val().length > 1)) {
        msg += "New password field has to be minimum eight characters." + "<br/>";
    }

    if ($('#CP_txtNewPassword').val() == "") {
        msg += "Please confirm your new password." + "<br/>";
    }

    if (($('#CP_txtNewPassword').val().length < 8) && ($('#ct_CBI_txtNewPassword').val().length > 1)) {
        msg += "Confirm password field has to be minimum eight characters." + "<br/>";
    }
    if (msg.length > 0) {
        $("#CP_lblSuccessOrFailure").html('');
        $("#ChangePasswordValidation")[0].innerHTML = msg;
        $('#CP_ErrorMsgContainer').show();
        return false;
    }
    else {

        var OldPassword = $("#CP_txtOldPassword").val();
        var NewPassword = $("#CP_txtPassword").val();
        $.ajax({
            url: 'ViewMemberProfile.aspx/ChangePassword',
            type: 'POST',
            contentType: 'application/json; charset =utf-8',
            data: "{'pstrOldPassword':'" + OldPassword.toString() + "'" + "," + "'pstrNewPassword':'" + NewPassword.toString() + "'}",
            dataType: 'json',
            success: function (data) {
                try {

                    var newData = data.d;
                    if (data.d == "Success") {

                        $("#CP_ErrorMsgContainer").show();
                        $("#CP_txtOldPassword").val('');
                        $("#CP_txtPassword").val('');
                        $("#CP_txtNewPassword").val('');
                        $("#CP_lblSuccessOrFailure").html('Your password has been changed.');
                    }
                    else if (data.d == "Failure") {

                        $("#CP_ErrorMsgContainer").show();
                        $("#CP_lblSuccessOrFailure").html('Password Changed Failure.');
                    }
                    else if (data.d == "IncorrectFormat") {

                        $("#CP_ErrorMsgContainer").show();
                        $("#CP_lblSuccessOrFailure").html('Password must contain one non-alpha character,one upper case character,one lower case character.');
                    } else if (data.d == "") {
                        $("#CP_ErrorMsgContainer").show();
                        $("#CP_lblSuccessOrFailure").html('Please enter correct password.');
                    }

                }
                catch (e) {
                    alert(e);

                    return false;
                }
                return false;

            },
            error: function (errmsg) {

            }
        });
    }
    jQuery("#overlay").css('display', 'none');
    jQuery("#popup").css('display', 'none');
    return false;
};
