﻿$(function () {
    $('#CP_TextBoxCheckin').datepicker({
        startDate: '+3d',
        format: "dd/mm/yyyy"
    }).on('changeDate', function (e) {
        var toDate = new Date(e.date.getFullYear(), e.date.getMonth(), e.date.getDate());
        var oneDay = new Date(toDate.getTime() + 86400000);
        $("#CP_TextBoxCheckin").removeClass("schfldserr");
        
        if ((oneDay.getMonth() + 1) < 10) {
            var Month = "0" + (oneDay.getMonth() + 1);
        }
        else {
            var Month = (oneDay.getMonth() + 1);
        }
        if (oneDay.getDate() < 10) {
            var Day = "0" + oneDay.getDate();
        }
        else {
            var Day = oneDay.getDate();
        }
        oneDay = Day + "/" + Month + "/" + oneDay.getFullYear();
        $("#CP_TextBoxCheckout").val(oneDay);
        $("#CP_TextBoxCheckout").removeClass("schfldserr");
        $("#CP_TextBoxCheckout").datepicker({
            startDate: e.date,
            format: "dd/mm/yyyy"
        }).on('changeDate', function (e) {
            $("#CP_TextBoxCheckout").removeClass("schfldserr");
        });
        return false;
    });

    $('#ddlRoomno').on('change', function () {
        selectRooms();
    });

    return false;
});

var app = angular.module('Danamon', ['ngAnimate', 'ui.bootstrap']);


//app.directive('slideit', function () {
//    return {
//        restrict: 'A',
//        replace: true,
//        scope: {
//            slideit: '='
//        },
//        template: '<ul class="bxslider">' +
//                    '<li ng-repeat="slide in slides">' +
//                      '<img ng-src="{{slide.src}}" alt="" />' +
//                    '</li>' +
//                   '</ul>',
//        link: function (scope, elm, attrs) {
//            elm.ready(function () {
//                scope.$apply(function () {
//                    scope.slides = scope.slideit;
//                });
//                elm.bxSlider({
//                    adaptiveHeight: true,
//                    mode: 'horizontal',
//                    auto: true,
//                    controls: true,
//                    hideControlOnEnd: false,
//                    autoControls: true,
//                    pager: false
//                });
//            });
//        }
//    };
//});
app.controller('MainCtrl', function ($scope, $http) {
    //$scope.base = 'http://bxslider.com';
    //$scope.images = [
    //     //{ src: 'Images/banr1.jpg' },
    //     { src: 'Images/banr2.jpg' },
    //     { src: 'Images/banr3.jpg' },
    //     { src: 'Images/banr4.jpg' },
    //     { src: 'Images/banr5.jpg' },
    //     { src: 'Images/banr6.jpg' },
    //];
    selectRooms();

    $scope.getAllHotelCities = function (val) {
        return $http({
            url: "FlightLBMSServices.aspx/GetAllHotelCities",
            dataType: 'json',
            method: 'POST',
            data: "{'prefixText':'" + val.toString() + "'}",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (res) {
            var addresses = [];
            angular.forEach(res.data.d, function (item) {
                addresses.push(item);
            });
            return addresses;
        });
    };

    $scope.SearchRoom = function () {
        
        SearchRooms();
    }
    $scope.empty = function (id) {
        $("#CP_" + id).val('');
        $("#CP_" + id).removeClass("schfldserr");
    }
    validateHotelFields = function () {
        //var msg = "";
        var cnt=0;
        if ($("#CP_txtCity").val().length == 0 || $("#CP_txtCity").val() == "Enter City Name" || $("#CP_txtCity").val() == "*Required") {
          //  msg = "Please Enter City Name";
            $("#CP_txtCity").addClass("schfldserr");
            $("#CP_txtCity").val("*Required");
            cnt++;
        }
        if ($("#CP_TextBoxCheckin").val().length == 0 || $("#CP_TextBoxCheckin").val() == "Check In" || $("#CP_TextBoxCheckin").val() == "*Required") {
            //msg = "Please Enter CheckIn Date";
            $("#CP_TextBoxCheckin").addClass("schfldserr");
            $("#CP_TextBoxCheckin").val("*Required");
            cnt++;
        }
        if ($("#CP_TextBoxCheckout").val().length == 0 || $("#CP_TextBoxCheckout").val() == "Check out" || $("#CP_TextBoxCheckout").val() == "*Required") {
            //msg = "Please Enter CheckOut Date";
            $("#CP_TextBoxCheckout").addClass("schfldserr");
            $("#CP_TextBoxCheckout").val("*Required");
            cnt++;
        }
         if (cnt == 0) {
             return true;
         }
         else {
             return false;
         }
        //if (msg.length > 0) {
        //    $("#HotelModifyValidation")[0].innerHTML = msg;

        //    showdiv("divHotelModifyValidation");
        //    setTimeout(blank('divHotelModifyValidation'), 3000);
        //    return false;
        //}
        //else
        //    return true;
    }
});

function SearchRooms() {

    var room = [];
    room = $(".append .room1");
    var strRoomAdult = "";
    var strRoomChild = "";
    var strRoom = "";
    for (var count = 0; count < room.length; count++) {
        var selectTag = [];
        selectTag = $(room[count]).find('select');
        strRoomAdult += $(selectTag[0]).val() + ",";
        strRoomChild += $(selectTag[1]).val() + ",";
    }
    strRoom = strRoomAdult + ":" + strRoomChild;
    $("#CP_hdnRoomString").val(strRoom);
    if (validateHotelFields()) {
        var strCity = "city=" + $("#CP_txtCity").val() + "&";
        var strCheckIn = "checkin=" + $("#CP_TextBoxCheckin").val() + "&";
        var strCheckout = "checkout=" + $("#CP_TextBoxCheckout").val() + "&";
        var strRoomstring = "roomstring=" + $("#CP_hdnRoomString").val() + "&";
        var isRedeemMiles = "isRedeemMiles=True";
        var queryString = strCity + strCheckIn + strCheckout + strRoomstring + isRedeemMiles;
        window.location = "HotelsSearchWait.aspx?" + queryString;
        return false;
    } else {
        return false;
    }
}
function selectRooms() {
    var noofRooms = $('#ddlRoomno :selected').text();
    var strTemplate = "<ul>";
    $(".append").html('');
    for (var count = 0; count <= noofRooms - 1; count++) {

        if (count % 2 != 0) {
            strTemplate += '<li class="room1 mrno">';
        }
        else {
            strTemplate += '<li class="room1">';
            
        }
        strTemplate += "Room" + (count + 1) + "<ul><li>";
        strTemplate += "<label>Adult</label><select><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></li>";
        strTemplate += "<li class='mrno'><label>Child</label><select><option value='0'>0</option><option value='1'>1</option><option value='2'>2</option></select><span>(0-11 years)</span></li></ul>";
        strTemplate += "</li>";

    }
    strTemplate += "</ul>";
    $(".append").html(strTemplate);

}
