﻿using Core.Platform.Member.Entites;
using Core.Platform.ProgramMaster.Entities;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using InfiPlanetModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string GenerateOTPForRegistration(string pstrCustomerId, string pstrProgramId)
    {
        bool lblstatus = false;
        string lstrMsg = "Please Try Again";
        InfiModel lobjmodel = new InfiModel();
        MemberDetails lobjMemberDetails = null;
        SearchMember lobjSearchMember = new SearchMember();
        lobjSearchMember.RelationReference = pstrCustomerId;
        lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
        try
        {
            lobjMemberDetails = lobjmodel.GetEnrollMemberDetails(lobjSearchMember);

            if (lobjMemberDetails != null)
            {
                SearchMember lobjSearchMemberForLogin = new SearchMember();
                lobjSearchMemberForLogin.RelationReference = pstrCustomerId;
                lobjSearchMemberForLogin.RelationType = Convert.ToInt32(RelationType.LBMS);

                // if (!lobjmodel.CheckMemberLoginDetails(lobjSearchMemberForLogin))
                MemberRelation lobjMemberEnrollRelation = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS));

                if (!lobjMemberEnrollRelation.IsAccountActivated && lobjMemberEnrollRelation.Status != Status.Active)
                {
                    if (lobjMemberDetails.ProgramId == Convert.ToInt32(pstrProgramId))
                    {
                        ProgramDefinition lobjProgramDefinition = lobjmodel.GetProgramDetailsByID(lobjMemberDetails.ProgramId);
                        if (lobjProgramDefinition != null)
                        {
                            SystemParameter lobjSystemParameter = lobjmodel.GetSystemParametres(lobjProgramDefinition.ProgramId);
                            if (lobjSystemParameter != null)
                            {
                                MemberRelation lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobjRelation => lobjRelation.RelationType.Equals(RelationType.LBMS));
                                if (lobjMemberRelation.Status.Equals(Status.InActive) && lobjMemberRelation.IsAccountActivated.Equals(false))
                                {

                                    string lstrSourceIpAddress = HttpContext.Current.Request.UserHostAddress;
                                    lblstatus = lobjmodel.GenerateOTPForRegistration(lobjMemberRelation.RelationReference, lstrSourceIpAddress, lobjMemberDetails.ProgramId);
                                    if (lblstatus)
                                    {
                                        //lstrMsg = "Your OTP has been sent to your registered mobile number " + "XXXX" + lobjMemberDetails.MobileNumber.Remove(0, 4) + " and registered email Id " + "XXXXX" + lobjMemberDetails.Email.Remove(0, 5) + ".";
                                        lstrMsg = "Success";
                                        HttpContext.Current.Session["MemberDetailsForLogin"] = lobjMemberDetails;
                                    }
                                    else
                                    {
                                        lstrMsg = "Please check the details";
                                    }
                                }
                                else
                                {
                                    lstrMsg = "Customer Id is already registered.";
                                }

                            }
                            else
                            {
                                lstrMsg = "Please enter Valid Customer Id";
                            }
                        }
                        else
                        {
                            lstrMsg = "Please enter Valid Customer Id";
                        }
                    }
                    else
                    {
                        lstrMsg = "Please enter Valid Customer Id";
                    }
                }
                else
                {
                    lstrMsg = "Customer Id is already registered.";
                }
            }
            else
            {
                lstrMsg = "Please enter Valid Customer Id";
            }
        }
        catch (Exception ex)
        {
            lstrMsg = "Application could not process your request successfully. Please try again.";
            LoggingAdapter.WriteLog("GenerateOTPForRegistration: " + ex.Message);

        }

        return lstrMsg;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string BindProgramDetails()
    {
        InfiModel lobjModel = new InfiModel();
        List<ProgramDefinition> lobjInfiplanetProgramList = null;
        List<ProgramDefinition> lobjListProgramDefinition = null;
        lobjInfiplanetProgramList = lobjModel.GetInfiplanetProgramList();
        if (lobjInfiplanetProgramList != null && lobjInfiplanetProgramList.Count > 0)
        {
            lobjListProgramDefinition = lobjModel.GetProgramDefinitionList();
            if (lobjListProgramDefinition != null && lobjListProgramDefinition.Count > 0)
            {
                lobjListProgramDefinition = lobjListProgramDefinition.Where(program =>
   lobjInfiplanetProgramList.Any(infiprogram => infiprogram.ProgramId.Equals(program.ProgramId))).ToList();
            }
            else
            {

            }
        }
        return JSONSerialization.Serialize(lobjListProgramDefinition);
    }
}