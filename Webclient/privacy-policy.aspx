﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="privacy-policy.aspx.cs" Inherits="privacy_policy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" Runat="Server">
 <link href="css/jquery.css" rel="stylesheet" />
 <link href="css/myAccount.css" rel="stylesheet" />
<article id="article">
    <div class="wrap staticPage faqPage">
        <h1>Privacy Policy</h1>
		<div class="innerTxt">
			<p>Internet Privacy Policy 
			<br><br>
This Privacy Statement relates to information supplied by you on this Website.
<br><br>
Information transmitted over the Internet is inherently insecure. However, at WorldRewardZ we are committed to safeguarding all your personal privacy and information.
<br><br>
Any personal information provided by you to WorldRewardZ through this Website will be used for the purpose of providing and operating WorldRewardZ products and services and for other related purposes which may include updating records, understanding your financial needs, conducting credit checks, reviewing credit worthiness, advising you of other products and services which may be of interest to you, for crime/fraud prevention and debt collection purposes, for purposes required by law or regulation, and to plan, conduct and monitor WorldRewardZ’s business.
<br><br>
WorldRewardZ may disclose information about you to WorldRewardZ’s partners, agents, subsidiaries, affiliates or contractors for the above purposes or in accordance with any WorldRewardZ Terms & Conditions.
<br><br>
It may also be possible for customers to review and change contact information such as address, phone and e-mail information by signing on and updating their personal profile. It is your responsibility to maintain the secrecy of any user ID and login password you hold. To protect your privacy, proof of identity or other authentication is required any time you contact us via the contact center.
<br><br>
WorldRewardZ reserves the right to amend its Website Usage Terms & Conditions and this Privacy Statement at any time. This Privacy Statement is not intended to, nor does it create any contractual rights whatsoever or any other legal rights, nor does it create any obligations on WorldRewardZ in respect of any other party or on behalf of any party.
</p>
</div>		
</div>

</article>
</asp:Content>

