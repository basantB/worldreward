﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Member.Entites;
using CB.IBE.Platform.Car.ClientEntities;
using CB.IBE.Platform.Car.Entities;
using Core.Platform.MemberActivity.Entities;
using Core.Platform.MemberActivity.Constants;
using CB.IBE.Platform.Hotels.ClientEntities;
using Framework.EnterpriseLibrary.Adapters;
using CB.IBE.Platform.ClientEntities;
using CB.IBE.Platform.Masters.Entities;
using CB.IBE.Platform.Entities;
using Core.Platform.Booking.Entities;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Transactions.Entites;
using InfiPlanetModel.Model;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;
using InfiPlanet.Entities;
using Framework.Integrations.Hotels.Entities;
public partial class CashBackPointGateway : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool GetCashBack()
    {
        LoggingAdapter.WriteLog("GetCashBack Request:");
        InfiModel lobjModel = new InfiModel();
        bool lblnStatus = false;

        ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();

        if (HttpContext.Current.Session["MemberDetails"] != null && lobjProgramDefinition != null)
        {
            string lstrCurrency = lobjModel.GetDefaultCurrency();
            MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
            AdditionalRedemptionDetails lobjAdditionalRedemptionDetails = new AdditionalRedemptionDetails();
            lobjAdditionalRedemptionDetails = (AdditionalRedemptionDetails)HttpContext.Current.Session["AdditionalRedemptionDetails"];
            HttpContext.Current.Session["MemberMiles"] = Convert.ToInt32(lobjModel.CheckAvailbility(lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency));
            if (lobjAdditionalRedemptionDetails != null)
            {
                if (!lobjAdditionalRedemptionDetails.Amounts.Equals(0) && !lobjAdditionalRedemptionDetails.Points.Equals(0))
                {
                    LoggingAdapter.WriteLog("GetCashBack Request1:");
                    lobjAdditionalRedemptionDetails.LoyaltyTxnType = LoyaltyTxnType.CashBack;
                    lobjAdditionalRedemptionDetails.RedemptionStatus = RedemptionStatus.Pending;
                    //lobjAdditionalRedemptionDetails.Amounts = Convert.ToSingle(strAmount);
                    //lobjAdditionalRedemptionDetails.Points = Convert.ToInt32(strTotalPoints);
                    lobjAdditionalRedemptionDetails.RelationReference = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    lobjAdditionalRedemptionDetails.SourceCurrency = lobjProgramDefinition.lobjProgramAttribute.BaseCurrency;
                    lobjAdditionalRedemptionDetails.TxnCurrency = lobjProgramDefinition.lobjProgramAttribute.BaseCurrency;
                    lobjAdditionalRedemptionDetails.TransactionCurrency = lstrCurrency;
                    lobjAdditionalRedemptionDetails.CreatedBy = "SYSTEM";
                    HttpContext.Current.Session["CashBackData"] = lobjAdditionalRedemptionDetails;
                    lblnStatus = lobjModel.CashBackRedemtions(lobjAdditionalRedemptionDetails, lobjMemberDetails);
                    HttpContext.Current.Session["AdditionalRedemptionDetails"] = null;
                }
                else
                {
                    LoggingAdapter.WriteLog("AdditionalRedemptionDetails Amount and Points Zero");
                    HttpContext.Current.Session["AdditionalRedemptionDetails"] = null;
                }
            }
            else
            {
                LoggingAdapter.WriteLog("AdditionalRedemptionDetails Null");
            }
        }
        LoggingAdapter.WriteLog("GetCashBack Response" + lblnStatus);
        return lblnStatus;
    }
}