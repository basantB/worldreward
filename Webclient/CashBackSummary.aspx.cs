﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Member.Entites;
using Core.Platform.Transactions.Entites;
//using CommercialBankModel.Model;

public partial class CashBackSummary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //CBIModel lobjCBIModel = new CBIModel();
        if (Session["MemberDetails"] != null && Session["MemberMiles"] != null)
        {
            MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
            AdditionalRedemptionDetails lobjAdditionalRedemptionDetails = Session["CashBackData"] as AdditionalRedemptionDetails;


            if (Request.QueryString["request"] != null && Request.QueryString["request"].ToString() == "true")
            {
                divCashBackSuccess.Attributes.Add("style", "display:block");
                lblAmount.Text = "<b>Total CashBack </b>: " + Convert.ToString(lobjAdditionalRedemptionDetails.Amounts) + " AED.";
                lblPoints.Text += "<b>Total Points </b>: " + Convert.ToString(lobjAdditionalRedemptionDetails.Points) + " Points.";
                lblPoints.Text += "<b>Total Points Available </b>: " + Convert.ToString(Session["MemberMiles"]) + " Points.<br/>";
                lblSuccess.Visible = true;
                lblFail.Visible = false;
            }
            else
            {
                divCashBackSuccess.Attributes.Add("style", "display:block");
                lblAmount.Text = "<b>Total Points Required </b>: " + Convert.ToString(lobjAdditionalRedemptionDetails.Points) + " Points.";
                lblPoints.Text = "<b>Total Points Available </b>: " + Convert.ToString(Session["MemberMiles"]) + " Points.<br/>";
                lblSuccess.Visible = false;
                lblFail.Visible = true;
            }
            Session["CashBackClick"] = null;
        }
        else
        {
            Session["CashBackClick"] = null;
            Response.Redirect("Index.aspx");
        }
    }
}