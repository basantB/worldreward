﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HotelsSearchWait.aspx.cs" MasterPageFile="~/InfiPlanetMaster.master"
    Inherits="HotelsSearchWait" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <%--<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <title>Search Wait</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <link href="Css/main.css" rel="stylesheet" />--%>

    <script type="text/javascript" language="javascript">
        function getQuerystring(key, default_) {
            if (default_ == null) default_ = "";
            key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
            var qs = regex.exec(window.location.href);
            if (qs == null)
                return default_;
            else
                return qs[1];
        }
        $(document).ready(function () {

            var strCity = getQuerystring("city");
            var strCheckIn = getQuerystring("checkin");
            var strCheckOut = getQuerystring("checkout");
            var strRoomString = getQuerystring("roomstring");
            var strisRedeemMiles = getQuerystring("isRedeemMiles");
            var SearchDetails = strCity; //+ "," + strCheckIn + " to " + strCheckOut + ".";
            //            var SearchDetails = strCity + "," + strCheckIn + " to " + strCheckOut + ", " + strRoomString + ".";
            SearchDetails = SearchDetails.replace(/\%20/g, ' ');
            $("#CP_LabelYourSearchDetails").text(SearchDetails);
            $.ajax({
                type: 'POST',
                url: document.URL.toString().replace("HotelsSearchWait.aspx", "HotelsSearchWait.aspx/GetHotelSearchResponse"),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                timeout: 80000,
                data: "{'pstrCity':'" + strCity + "','pCheckIn':'" + strCheckIn + "','pCheckOut':'" + strCheckOut + "','pRoomString':'" + strRoomString + "','pisRedeemMiles':'" + strisRedeemMiles + "'}",
                success: function (msg) {
                    if (msg.d)
                        window.location = "HotelResults.aspx";
                    else
                        window.location = "NoResultFound.aspx?ERR=HotelNOTFOUND";


                },
                error: function (jqXHR, status, errorThrown) {
                    window.location = "ErrorPage.aspx";

                }
            });
        });

    </script>
    <%--</head>

    <body class="scrhpg">
        <form id="form2" runat="server">--%>
    <div class="srchcircle">
        <div class="scrhcnt">
            <div>
                <img src="images/icohtl.png" />
            </div>
            <h1>Searching Hotels</h1>
            <p>
                <asp:Label ID="LabelYourSearchDetails" runat="server"></asp:Label>
            </p>
            <div id="cssload-wrapper">
                <div id="cssload-border">
                    <div id="cssload-whitespace">
                        <div id="cssload-line">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<%--        </form>
    </body>
</html>--%>
