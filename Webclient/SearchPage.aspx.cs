﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CB.IBE.Platform.ClientEntities;
using InfiPlanetModel.Model;
using Core.Platform.ProgramMaster.Entities;
using CB.IBE.Platform.Entities;
using CB.IBE.Platform.Masters.Entities;
using Core.Platform.Member.Entites;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.MemberActivity.Entities;

public partial class SearchPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            InfiModel lobjModel = new InfiModel();
            string lstrRelativePath = this.Page.AppRelativeVirtualPath;
            int lintActivityId = lobjModel.LogActivity(lstrRelativePath.Replace("~", string.Empty).Replace("/", string.Empty), ActivityType.PageLoad);

        }
      
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool FlightSearch()
    {
        LoggingAdapter.WriteLog("FlightSearch");
        InfiModel lobjModel = new InfiModel();
        ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();
        if (lobjProgramDefinition != null)
        {

            string departure = string.Empty;
            if (HttpContext.Current.Request.QueryString["departure"] != null && HttpContext.Current.Request.QueryString["departure"] != "")
                departure = HttpContext.Current.Request.QueryString["departure"].ToString();
            string arrival = string.Empty;
            if (HttpContext.Current.Request.QueryString["arrival"] != null && HttpContext.Current.Request.QueryString["arrival"] != "")
                arrival = HttpContext.Current.Request.QueryString["arrival"].ToString();
            string departuredate = string.Empty;
            if (HttpContext.Current.Request.QueryString["departuredate"] != null && HttpContext.Current.Request.QueryString["departuredate"] != "")
                departuredate = HttpContext.Current.Request.QueryString["departuredate"].ToString();
            departuredate = departuredate.Replace(" ", string.Empty);

            string isReturn = string.Empty;
            if (HttpContext.Current.Request.QueryString["isReturn"] != null && HttpContext.Current.Request.QueryString["isReturn"] != "")
                isReturn = HttpContext.Current.Request.QueryString["isReturn"].ToString();
            string arrivaldate = string.Empty;
            if (isReturn == "true")
            {
                if (HttpContext.Current.Request.QueryString["arrivaldate"] != null && HttpContext.Current.Request.QueryString["arrivaldate"] != "")
                    arrivaldate = HttpContext.Current.Request.QueryString["arrivaldate"].ToString();
                arrivaldate = arrivaldate.Replace(" ", string.Empty);
            }

            string airline = string.Empty;
            if (HttpContext.Current.Request.QueryString["airline"] != null && HttpContext.Current.Request.QueryString["airline"] != "")
                airline = HttpContext.Current.Request.QueryString["airline"].ToString();
            string airlineIATACode = string.Empty;
            if (HttpContext.Current.Request.QueryString["airlineIATACode"] != null && HttpContext.Current.Request.QueryString["airlineIATACode"] != "")
                airlineIATACode = HttpContext.Current.Request.QueryString["airlineIATACode"].ToString();
            string adult = string.Empty;
            if (HttpContext.Current.Request.QueryString["adult"] != null && HttpContext.Current.Request.QueryString["adult"] != "")
                adult = HttpContext.Current.Request.QueryString["adult"].ToString();
            string child = string.Empty;
            if (HttpContext.Current.Request.QueryString["child"] != null && HttpContext.Current.Request.QueryString["child"] != "")
                child = HttpContext.Current.Request.QueryString["child"].ToString();
            string infant = string.Empty;
            if (HttpContext.Current.Request.QueryString["infant"] != null && HttpContext.Current.Request.QueryString["infant"] != "")
                infant = HttpContext.Current.Request.QueryString["infant"].ToString();
            string economy = string.Empty;
            if (HttpContext.Current.Request.QueryString["economy"] != null && HttpContext.Current.Request.QueryString["economy"] != "")
                economy = HttpContext.Current.Request.QueryString["economy"].ToString();
            //string isRedeemMiles = string.Empty;
            //if (HttpContext.Current.Request.QueryString["isRedeemMiles"] != null && HttpContext.Current.Request.QueryString["isRedeemMiles"] != "")
            //    isRedeemMiles = HttpContext.Current.Request.QueryString["isRedeemMiles"].ToString();

            if (departure != string.Empty && arrival != string.Empty && departuredate != string.Empty && isReturn != string.Empty && adult != string.Empty && child != string.Empty && infant != string.Empty && economy != string.Empty)
            {
                List<AirField> lobjListOfAirfield = HttpContext.Current.Application["AllAirfields"] as List<AirField>;
                AirField lobjDepartureAirField = lobjListOfAirfield.Find(airfileds => airfileds.IATACode.Equals(departure));
                AirField lobjArrivalAirField = lobjListOfAirfield.Find(airfileds => airfileds.IATACode.Equals(arrival));

                RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererData"] as RefererDetails;
                SearchRequest lobjSearchRequest = new SearchRequest();

                lobjSearchRequest.SearchDetails.OriginLocation = departure;
                lobjSearchRequest.SearchDetails.DestinationLocation = arrival;
                lobjSearchRequest.RefererDetails = lobjRefererDetails;
                lobjSearchRequest.SearchDetails.DepartureDate = Convert.ToDateTime(lobjModel.StringToDateTime(departuredate));
                if (Convert.ToBoolean(isReturn))
                {
                    lobjSearchRequest.SearchDetails.ArrivalDate = Convert.ToDateTime(lobjModel.StringToDateTime(arrivaldate));
                    lobjSearchRequest.SearchDetails.IsReturn = true;
                }
                HttpContext.Current.Session["FlightSearchPaymode"] = PaymentType.Points;

                lobjSearchRequest.SearchDetails.Adults = Convert.ToInt32(adult);
                lobjSearchRequest.SearchDetails.Childrens = Convert.ToInt32(child);
                lobjSearchRequest.SearchDetails.Infants = Convert.ToInt32(infant);
                lobjSearchRequest.SearchDetails.Cabin = Convert.ToString(economy);
                lobjSearchRequest.SearchDetails.AirlinePrefCode = airlineIATACode.Equals(string.Empty) ? "Any" : airlineIATACode;
                lobjSearchRequest.SearchDetails.DepCountryName = lobjDepartureAirField.CountryName;
                lobjSearchRequest.SearchDetails.ArrCountryName = lobjArrivalAirField.CountryName;
                lobjSearchRequest.SearchDetails.ArrCode.AirportName = lobjArrivalAirField.AirportName;
                lobjSearchRequest.SearchDetails.DepCode.AirportName = lobjDepartureAirField.AirportName;
                lobjSearchRequest.SearchDetails.DepCode.City = lobjDepartureAirField.City;
                lobjSearchRequest.SearchDetails.ArrCode.City = lobjArrivalAirField.City;
                lobjSearchRequest.SearchDetails.FlightType = airline;
                lobjSearchRequest.IPAddress = HttpContext.Current.Request.UserHostAddress;

                lobjSearchRequest.SearchDetails.SessionId = Convert.ToString(HttpContext.Current.Session["SessionId"]);

                MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
                string lstrMemberId = string.Empty;
               // string lstrMemberId = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;

                if (lobjMemberDetails != null)
                {
                    lstrMemberId = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    lobjSearchRequest.SearchDetails.MemberId = lstrMemberId;
                }

                string lstrCurrency = lobjModel.GetDefaultCurrency();
                HttpContext.Current.Session["SearchCurrency"] = lstrCurrency;

                lobjSearchRequest.PointRate = lobjModel.GetProgramRedemptionRate(lstrCurrency, RedemptionCodeKeys.AIR.ToString(), lobjProgramDefinition.ProgramId);

                SearchResponse lobjSearchResponse = new SearchResponse();
                LoggingAdapter.WriteLog("Before MapSearchResponse");
                lobjSearchResponse = lobjModel.MapSearchResponse(lobjSearchRequest);
                if (lobjSearchResponse.ItineraryDetailsList.Count().Equals(0))//Domestic
                {
                    lobjSearchRequest.SearchDetails.SearchType = "0";
                }
                else
                {
                    lobjSearchRequest.SearchDetails.SearchType = "1";
                }

                int SearchId = 0;
                SearchId = lobjModel.InsertSearchFlight(lobjSearchRequest);
                if (SearchId > 0)
                    lobjSearchRequest.SearchDetails.SearchId = SearchId;


                HttpContext.Current.Session["SearchFlight"] = lobjSearchRequest;

                HttpContext.Current.Session["FlightSearchDetails"] = lobjSearchRequest;

                if (lobjSearchResponse.ItineraryDetailsList.Count() > 0 || lobjSearchResponse.OnwardFlightList.Count() > 0 || lobjSearchResponse.ReturnFlightList.Count() > 0)
                {
                    lobjModel.LogActivity(string.Format("Flights Found : Member Id : {0}, Departure DateTime : {1}, Arrival DateTime : {2}, Departure Location : {3}, Arrival Location : {4}, Adult : {5}, Child : {6}, Infant : {7}", lstrMemberId, departuredate, arrivaldate, lobjDepartureAirField.AirportName + " " + lobjDepartureAirField.City + " " + lobjDepartureAirField.CountryName, lobjArrivalAirField.AirportName + " " + lobjArrivalAirField.City + " " + lobjArrivalAirField.CountryName, adult, child, infant), ActivityType.FlightSearch);
                    HttpContext.Current.Session["Flights"] = lobjSearchResponse;
                    return true;
                }
                else
                {
                    lobjModel.LogActivity(string.Format("Flights Not Found : Member Id : {0}, Departure DateTime : {1}, Arrival DateTime : {2}, Departure Location : {3}, Arrival Location : {4}, Adult : {5}, Child : {6}, Infant : {7}", lstrMemberId, departuredate, arrivaldate, lobjDepartureAirField.AirportName + " " + lobjDepartureAirField.City + " " + lobjDepartureAirField.CountryName, lobjArrivalAirField.AirportName + " " + lobjArrivalAirField.City + " " + lobjArrivalAirField.CountryName, adult, child, infant), ActivityType.FlightSearch);
                    return false;
                }

            }
            else
                return false;
        }
        else
            return false;

    }
    private static string getAirfieldName(string psrtPar)
    {
        if (psrtPar.Length > 3)
            return psrtPar.Substring(0, 3);
        else
            return psrtPar.ToUpper();
    }
}