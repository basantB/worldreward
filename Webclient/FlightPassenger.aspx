﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlightPassenger.aspx.cs" Inherits="FlightPassenger" MasterPageFile="~/InfiPlanetMaster.master" %>

<%@ Register Src="~/UserControl/AdultPassangerDetails.ascx" TagName="Adult" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ChildPassangerDetails.ascx" TagName="Children" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/InfantPassangerDetails.ascx" TagName="Infant" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="Css/flight.css" rel="Stylesheet" type="text/css" />
    <link href="Css/lounge.css" rel="Stylesheet" type="text/css" />
    <script src="js/angular.js" type="text/javascript"></script>
    <script src="js/angular-animate.js" type="text/javascript"></script>
    <script src="js/ui-bootstrap-tpls-1.3.3.js" type="text/javascript"></script>
    <script src="js/bootstrap-datepicker.js" type="text/javascript"></script>
    <link href="css/datepicker.css" rel="stylesheet" />
    <script type="text/javascript">
        $.fn.digits = function () {
            return this.each(function () {
                $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            })
        }
        $(document).ready(function () {
            $("#CP_lblTotalPoints").digits();
        });
        function numbersOnly(obj, e) {
            if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9) {
                return true;
            }
            else {
                if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                    return false;
                }
                return true;
            }
        }
    </script>
    <div class="inrpg">
        <div class="pgcontent tourSection ">

            <!--CheckOut Page Starts-->
            <div class="widget-srhpnl flight" id="MasterUser">
                    <div class="widget-user" style="display:none;">
                        <ul id="UserDetails">
                        </ul>
                    </div>
                        <h3>Itinerary
                        <!--<span>(1 Item)</span>-->
                            <span class="edit"><a href="FlightList.aspx">Edit</a> </span>
                        </h3>
                        <div class="shopping-list">
                            <ul>
                                <li>
                                    <h2>Departure Flight</h2>
                                    <asp:Repeater ID="rptDeparture" runat="server">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="col-1">
                                                <img src='<%#Eval("Carrier.CarrierLogoPath")%>' />
                                            </div>
                                            <div class="col-2">
                                                <div class="item-Name">
                                                    <div>
                                                        <%#Eval("DepartureAirField.City")%>
                                                    </div>
                                                    <div>
                                                        <%#Eval("DepartureAirField.IATACode")%>
                                                    </div>
                                                </div>
                                                <p>
                                                    <%#Convert.ToDateTime(Eval("DepartureDate").ToString()).ToString("dd/MM/yyyy")%>
                                                </p>
                                                <p>
                                                    <%#Convert.ToDateTime(Eval("DisplayDepartureTime").ToString()).ToString("hh:mm tt")%>
                                                </p>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <img src="images/time.png">
                                                <p>
                                                    <%#Eval("TotalDurationHrs")%>
                                                h
                                                <%#Eval("TotalDurationMins")%>
                                                m
                                                </p>
                                            </div>
                                            <div class="col-4">
                                                <div class="item-Name">
                                                    <div>
                                                        <%#Eval("ArrivalAirField.City")%>
                                                    </div>
                                                    <div>
                                                        <%#Eval("ArrivalAirField.IATACode")%>
                                                    </div>
                                                </div>
                                                <p>
                                                    <%#Convert.ToDateTime(Eval("ArrivalDate").ToString()).ToString("dd/MM/yyyy")%>
                                                </p>
                                                <p>
                                                    <%#Convert.ToDateTime(Eval("DisplayArrivalTime").ToString()).ToString("hh:mm tt")%>
                                                </p>
                                                <div class="clr">
                                                </div>
                                            </div>
                                            <div class="clr">
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <br />
                                    <br />
                                    <h2>
                                        <asp:Label ID="lblarrival" runat="server" Text="Arrival Flight" Visible="false"></asp:Label></h2>

                                    <asp:Repeater ID="rptArrival" runat="server">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="col-1">
                                                <img src='<%#Eval("Carrier.CarrierLogoPath")%>' />
                                            </div>
                                            <div class="col-2">
                                                <div class="item-Name">
                                                    <div>
                                                        <%#Eval("DepartureAirField.City")%>
                                                    </div>
                                                    <div>
                                                        <%#Eval("DepartureAirField.IATACode")%>
                                                    </div>
                                                </div>
                                                <p>
                                                    <%#Convert.ToDateTime(Eval("DepartureDate").ToString()).ToString("dd/MM/yyyy")%>
                                                </p>
                                                <p>
                                                    <%#Convert.ToDateTime(Eval("DisplayDepartureTime").ToString()).ToString("hh:mm tt")%>
                                                </p>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <img src="images/time.png">
                                                <p>
                                                    3h 5m
                                                </p>
                                            </div>
                                            <div class="col-4">
                                                <div class="item-Name">
                                                    <div>
                                                        <%#Eval("ArrivalAirField.City")%>
                                                    </div>
                                                    <div>
                                                        <%#Eval("ArrivalAirField.IATACode")%>
                                                    </div>
                                                </div>
                                                <p>
                                                    <%#Convert.ToDateTime(Eval("ArrivalDate").ToString()).ToString("dd/MM/yyyy")%>
                                                </p>
                                                <p>
                                                    <%#Convert.ToDateTime(Eval("DisplayArrivalTime").ToString()).ToString("hh:mm tt")%>
                                                </p>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </li>
                                <div class="clr">
                                </div>
                                <li class="total">
                                    <div class="ftr">
                                        <span>Total WorldRewardZ</span>
                                        <span class="amt">
                                            <asp:Label ID="lblTotalPoints" runat="server" />
                                        </span>
                                        <div class="clr">
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                <!--StepsPage-->
                    <div class="checkoutheader">
                        <h1>Passenger Details</h1>
                        <ul class="passenger-block passenger_align">
                           <h2>Email
                                        </h2>
                            <li class="chkemail">
                                <asp:TextBox ID="txtEmailID" runat="server" AutoComplete="off"></asp:TextBox>
                            </li>
                            <li>
                                <asp:RequiredFieldValidator ValidationGroup="WebValidation" ID="RequiredFieldValidator2"
                                    runat="server" ControlToValidate="txtEmailID" Display="Dynamic" ErrorMessage="Enter E-mail"
                                    CssClass="errUCred"></asp:RequiredFieldValidator>&nbsp;
    <asp:RegularExpressionValidator ID="revAdultEmail" Display="Dynamic" runat="server"
        ControlToValidate="txtEmailID" ValidationGroup="WebValidation" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
        CssClass="errUCred" ErrorMessage="Enter Valid E-mail">
    </asp:RegularExpressionValidator>&nbsp;
                            </li>
                        </ul>
                         <div class="passenger-block passenger_align">
                        <div id="AdultInfo" runat="server">
                            <asp:Repeater ID="rptAdultControl" runat="server">
                                <HeaderTemplate>
                                        <h2>Adult
                                        </h2>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Panel ID="panelAdultControlHolder" CssClass="Flight_Ticket_holder" runat="server">
                                        <uc:Adult ID="adultdetails" runat="server" />
                                    </asp:Panel>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <div class="clr">
                                    </div>
                                    
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                              
                        </div>
                        <div class="clr">
                        </div>
                        <div id="ChildInfo" runat="server" visible="false">
                            <asp:Repeater ID="rptChildControl" runat="server">
                                <HeaderTemplate>
                                    <div class="passenger-block passenger_align">
                                        <h2>Child</h2>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Panel ID="panelChildControlHolder" CssClass="Flight_Ticket_holder" runat="server">
                                        <uc:Children ID="childdetails" runat="server" />
                                    </asp:Panel>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <div class="clr">
                                    </div>                                     
                                   
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="InfantInfo" runat="server" visible="false">
                            <asp:Repeater ID="rptInfantControl" runat="server">
                                <HeaderTemplate>
                                    <div class="passenger-block">
                                        <h2>Infant</h2>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Panel ID="panelInfantControlHolder" CssClass="Flight_Ticket_holder" runat="server">
                                        <uc:Infant ID="infantdetails" runat="server" />
                                    </asp:Panel>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <div class="clr">
                                    </div>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="col-3">
                            <asp:Button ValidationGroup="WebValidation" ID="btnSubmit" OnClick="btnBookFlight_Click"
                                Text="Proceed To Pay" runat="server" CssClass="btnRed proc_albtn" />
                        </div>
                       
                    </div>
                    <!--StepsPage-->
           
            <div class="clr">
            </div>
        </div>
    </div>
    <div class="clr">
    </div>
</asp:Content>
