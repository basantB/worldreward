﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Transactions.Entites;
using InfiPlanetModel.Model;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Member.Entites;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.Loan.Entities;

public partial class StatementSummary : System.Web.UI.Page
{
    List<TransactionDetails> LstGetMemberStatementTransaction = new List<TransactionDetails>();

    List<LoanTransactionDetails> LstGetMemberStatementLoanTransaction = new List<LoanTransactionDetails>();
    int lstrCBQPoints = 0;
    int lstrTotalCBQPoints = 0;
    InfiModel lobjModel = new InfiModel();
    protected void Page_Load(object sender, EventArgs e)
    {

        // Stop Caching in IE
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        // Stop Caching in Firefox
        Response.Cache.SetNoStore();

        if (Session["MemberDetails"] == null)
        {
            HttpContext.Current.Session["MyAccount"] = "MyAccount";
            Response.Redirect("Login.aspx");
        }
        if (!Page.IsPostBack)
        {
            HttpContext.Current.Session["MyAccount"] = null;
            List<ProgramCurrencyDefinition> lobjlistProgramCurrencyDefinition = GetProgramCurrency();
            MemberDetails objMemberDetails = new MemberDetails();
            objMemberDetails = Session["MemberDetails"] as MemberDetails;
            List<string> lstCurrencyPoints = new List<string>();

            if (lobjlistProgramCurrencyDefinition.Count > 0)
            {
                LoggingAdapter.WriteLog("ProgramCurrencyDefinition.Currency :" + lobjlistProgramCurrencyDefinition[0].Currency);
                BindMemberPoolingDetails();
                GetStatementSummary(lobjlistProgramCurrencyDefinition[0].Currency);
                if (lobjlistProgramCurrencyDefinition.Count > 1)
                {
                    LoggingAdapter.WriteLog("ProgramCurrencyDefinition Count > 1");
                    ddlProgramCurrency.DataSource = lobjlistProgramCurrencyDefinition;
                    ddlProgramCurrency.DataTextField = "Currency";
                    ddlProgramCurrency.DataValueField = "Id";
                    ddlProgramCurrency.DataBind();
                    ddlProgramCurrency.Visible = true;
                    divCurrency.Visible = true;
                }
                else
                {
                    LoggingAdapter.WriteLog("ProgramCurrencyDefinition Count < 1");
                    divCurrency.Visible = false;
                    ddlProgramCurrency.Visible = false;
                }
            }
            for (int i = 0; i < lobjlistProgramCurrencyDefinition.Count; i++)
            {
                lstrCBQPoints = CheckAvailbility(objMemberDetails.MemberRelationsList[0].RelationReference, Convert.ToInt32(RelationType.LBMS), lobjlistProgramCurrencyDefinition[i].Currency);
                lstrTotalCBQPoints = lstrTotalCBQPoints + lstrCBQPoints;
                lstCurrencyPoints.Add(lobjlistProgramCurrencyDefinition[i].Currency);
            }
            if (lstCurrencyPoints.Count == 1)
            {

                lblCurrencyName.Text = (lstCurrencyPoints[0].ToString());
                lblPoints.Text = Convert.ToString(lobjModel.FloatToThousandSeperated(CheckAvailbility(objMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lobjlistProgramCurrencyDefinition[0].Currency)));
                lblFinalPoints.Text = lblPoints.Text;
                LoggingAdapter.WriteLog("POINTS-" + lobjModel.FloatToThousandSeperated(CheckAvailbility(objMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lobjlistProgramCurrencyDefinition[0].Currency)));
            }
            else
            {

                lblCurrencyName.Text = (lstCurrencyPoints[0].ToString());
                lblPoints.Text = Convert.ToString(lobjModel.FloatToThousandSeperated(CheckAvailbility(objMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lobjlistProgramCurrencyDefinition[0].Currency)));
                lblFinalPoints.Text = lblPoints.Text;
                lblSecondPoints.Text = " and " + Convert.ToString(lobjModel.FloatToThousandSeperated(CheckAvailbility(objMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lobjlistProgramCurrencyDefinition[1].Currency)));
                lblSecondCurrencyName.Text = (lstCurrencyPoints[1].ToString());
            }
        }

    }

    protected void ddlPoolingDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        string pstrMemberId = ddlPoolingDetails.SelectedValue.ToString();
        List<MemberDetails> lstlobjMemberDetails = Session["MemberDetailsList"] as List<MemberDetails>;
        MemberDetails lobjNewMemberDetails = new MemberDetails();
        lobjNewMemberDetails = lstlobjMemberDetails.Find(obj => obj.Id.Equals(Convert.ToInt32(pstrMemberId)));
        string pstrRelationReference = lobjNewMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).RelationReference;
        System.Threading.Thread.Sleep(1000);

        InfiModel lobjInfiModel = new InfiModel();
        List<ProgramCurrencyDefinition> lobjlistProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
        lobjlistProgramCurrencyDefinition = lobjInfiModel.GetProductProgramCurrencyDefinition(lobjNewMemberDetails);
        GetStatementSummary(lobjlistProgramCurrencyDefinition[0].Currency, pstrRelationReference);
    }

    public void GetStatementSummary(string pstrCurrencyName, string pstrRelationReference)
    {
        InfiModel lobjInfiModel = new InfiModel();
        lblUserName.Text = "Welcome you have ";
        SearchTransactions objSearchTransactions = new SearchTransactions();
        objSearchTransactions.RelationReference = pstrRelationReference;
        ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramMaster();
        objSearchTransactions.ProgramId = lobjProgramDefinition.ProgramId;
        objSearchTransactions.RelationType = Convert.ToInt32(RelationType.LBMS);
        objSearchTransactions.TransactionCurrency = pstrCurrencyName;
        LstGetMemberStatementTransaction = lobjInfiModel.GetMemberStatementTransaction(objSearchTransactions);
        lblBonusmile.Text = Convert.ToString(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Credit && lobj.LoyaltyTxnType == LoyaltyTxnType.Bonus).Sum(sumObj => sumObj.Points));
        lblSpendmile.Text = Convert.ToString(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Credit && (lobj.LoyaltyTxnType == LoyaltyTxnType.Spend || lobj.LoyaltyTxnType == LoyaltyTxnType.Reversal)).Sum(sumObj => sumObj.Points));
        lblPurchasemile.Text = Convert.ToString(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Credit && lobj.LoyaltyTxnType == LoyaltyTxnType.Purchase).Sum(sumObj => sumObj.Points));
        lblPartnermile.Text = Convert.ToString(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Credit && lobj.LoyaltyTxnType == LoyaltyTxnType.Partner).Sum(sumObj => sumObj.Points));
        lblRedeemedmile.Text = Convert.ToString(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Debit).Sum(sumObj => sumObj.Points));
        lblClosingmile.Text = Convert.ToString(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Credit).Sum(sumObj => sumObj.Points) - LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Debit).Sum(sumObj => sumObj.Points));

        //LoggingAdapter.WriteLog("LoanSearchTransactions StmtSummary:");
        LoanSearchTransactions objLoanSearchTransactions = new LoanSearchTransactions();
        objLoanSearchTransactions.ProgramId = lobjProgramDefinition.ProgramId;
        objLoanSearchTransactions.RelationType = Convert.ToInt32(RelationType.LBMS);
        objLoanSearchTransactions.TransactionCurrency = pstrCurrencyName;
        objLoanSearchTransactions.RelationReference = pstrRelationReference;
        LoggingAdapter.WriteLog("LoanSearchTransactions GetStmtSummary:" + objLoanSearchTransactions.ProgramId + "|" + pstrCurrencyName + "|" + pstrRelationReference);
        LstGetMemberStatementLoanTransaction = lobjInfiModel.GetMemberStatementLoanTransaction(objLoanSearchTransactions);
        if (LstGetMemberStatementLoanTransaction != null)
        {
            LoggingAdapter.WriteLog("LoanSearchTransactions LstGetMemberStatementLoanTransaction:" + LstGetMemberStatementLoanTransaction.Count);
            lblLoanBonus.Text = Convert.ToString(LstGetMemberStatementLoanTransaction.FindAll(lobj => lobj.LoanTransactionType == LoanTransactionType.Credit).Sum(sumObj => sumObj.LoanPoints)); ;
            lblLoanRedemption.Text = Convert.ToString(LstGetMemberStatementLoanTransaction.FindAll(lobj => lobj.LoanTransactionType == LoanTransactionType.Debit).Sum(sumObj => sumObj.LoanPoints)); ;
            lblLoanClosing.Text = Convert.ToString(LstGetMemberStatementLoanTransaction.FindAll(lobj => lobj.LoanTransactionType == LoanTransactionType.Credit).Sum(sumObj => sumObj.LoanPoints) - LstGetMemberStatementLoanTransaction.FindAll(lobj => lobj.LoanTransactionType == LoanTransactionType.Debit).Sum(sumObj => sumObj.LoanPoints));
        }
    }

    public void BindMemberPoolingDetails()
    {
        try
        {
            InfiModel lobjModel = new InfiModel();

            List<MemberDetails> lobjListMemberDetails = new List<MemberDetails>();
            MemberDetails lobjMemberDetails = null;
            lobjMemberDetails = Session["MemberDetails"] as MemberDetails;

            SearchMember lobjSearchMember = new SearchMember();

            lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
            lobjSearchMember.ProgramId = lobjMemberDetails.ProgramId;

            string pstrPrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).PrimaryReference;


            if (pstrPrimaryReference == null || pstrPrimaryReference.Equals(string.Empty))
            {
                lobjSearchMember.PrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).RelationReference;
            }
            else
            {
                lobjSearchMember.PrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).PrimaryReference;
            }

            lobjListMemberDetails = lobjModel.GetMemberDetailByPrimaryReference(lobjSearchMember);

            lobjListMemberDetails = lobjListMemberDetails.GroupBy(c => c.Id, (key, c) => c.FirstOrDefault()).ToList();

            var index = lobjListMemberDetails.FindIndex(x => x.LastName.Equals(lobjMemberDetails.LastName));
            var topItem = lobjListMemberDetails[index];
            lobjListMemberDetails.RemoveAt(index);
            lobjListMemberDetails.Insert(0, topItem);
            Session["MemberDetailsList"] = lobjListMemberDetails;

            if (lobjListMemberDetails.Count > 1)
            {
                DivPool.Visible = true;
                ddlPoolingDetails.DataSource = lobjListMemberDetails;
                ddlPoolingDetails.DataTextField = "LastName";
                ddlPoolingDetails.DataValueField = "Id";
                ddlPoolingDetails.DataBind();
                ddlPoolingDetails.Visible = true;
            }
            else
            {
                DivPool.Visible = false;
            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("BindMemberPoolingDetails Ex-" + ex.Message + ex.StackTrace + ex.InnerException);
        }
    }

    public int CheckAvailbility(string pstrRelationReference, int pintRelationType, string pstrCurrency)
    {
        int lint = 0;
        InfiModel lobjModel = new InfiModel();
        lint = lobjModel.CheckAvailbility(pstrRelationReference, pintRelationType, pstrCurrency);
        LoggingAdapter.WriteLog("CheckAvailbility Points-" + lint);
        return lint;
    }
    private List<ProgramCurrencyDefinition> GetProgramCurrency()
    {
        InfiModel lobjModel = new InfiModel();
        MemberDetails lobjMemberDetails = null;
        lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
        ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramDetailsByID(lobjMemberDetails.ProgramId);
        List<ProgramCurrencyDefinition> lobjlistProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
        lobjlistProgramCurrencyDefinition = lobjModel.GetProductProgramCurrencyDefinition(lobjMemberDetails);

        return lobjlistProgramCurrencyDefinition;

    }

    public void GetStatementSummary(string pstrCurrencyName)
    {
        LoggingAdapter.WriteLog("GetStatementSummary CurrencyName: " + pstrCurrencyName);
        MemberDetails objMemberDetails = new MemberDetails();
        objMemberDetails = Session["MemberDetails"] as MemberDetails;
        InfiModel objInfiModel = new InfiModel();
        lblUserName.Text = objMemberDetails.LastName;
        SearchTransactions objSearchTransactions = new SearchTransactions();
        objSearchTransactions.RelationReference = objMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;

        objSearchTransactions.ProgramId = objMemberDetails.ProgramId;
        objSearchTransactions.RelationType = Convert.ToInt32(RelationType.LBMS);
        objSearchTransactions.TransactionCurrency = pstrCurrencyName;
        LstGetMemberStatementTransaction = objInfiModel.GetMemberStatementTransaction(objSearchTransactions);

        lblBonusmile.Text = Convert.ToString(lobjModel.FloatToThousandSeperated(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Credit && lobj.LoyaltyTxnType == LoyaltyTxnType.Bonus).Sum(sumObj => sumObj.Points)));
        lblSpendmile.Text = Convert.ToString(lobjModel.FloatToThousandSeperated(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Credit && (lobj.LoyaltyTxnType == LoyaltyTxnType.Spend || lobj.LoyaltyTxnType == LoyaltyTxnType.Reversal)).Sum(sumObj => sumObj.Points)));
        lblPurchasemile.Text = Convert.ToString(lobjModel.FloatToThousandSeperated(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Credit && lobj.LoyaltyTxnType == LoyaltyTxnType.Purchase).Sum(sumObj => sumObj.Points)));
        lblPartnermile.Text = Convert.ToString(lobjModel.FloatToThousandSeperated(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Credit && lobj.LoyaltyTxnType == LoyaltyTxnType.Partner).Sum(sumObj => sumObj.Points)));
        lblRedeemedmile.Text = Convert.ToString(lobjModel.FloatToThousandSeperated(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Debit).Sum(sumObj => sumObj.Points)));
        lblClosingmile.Text = Convert.ToString(lobjModel.FloatToThousandSeperated(LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Credit).Sum(sumObj => sumObj.Points) - LstGetMemberStatementTransaction.FindAll(lobj => lobj.TransactionType == TransactionType.Debit).Sum(sumObj => sumObj.Points)));
        //lblLastLogin.Text = Convert.ToString(objMemberDetails.MemberRelationsList.Find(lobj=> lobj.RelationType.Equals(RelationType.Login)).LastLoggedIn.ToString("F"));
        lblLastLogin.Text = Convert.ToString(objMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).LastLoggedIn.ToString("F"));

        LoanSearchTransactions objLoanSearchTransactions = new LoanSearchTransactions();
        objLoanSearchTransactions.ProgramId = objMemberDetails.ProgramId;
        objLoanSearchTransactions.RelationType = Convert.ToInt32(RelationType.LBMS);
        objLoanSearchTransactions.TransactionCurrency = pstrCurrencyName;
        objLoanSearchTransactions.RelationReference = objMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
        LstGetMemberStatementLoanTransaction = objInfiModel.GetMemberStatementLoanTransaction(objLoanSearchTransactions);
        if (LstGetMemberStatementLoanTransaction != null)
        {
            lblLoanBonus.Text = Convert.ToString(LstGetMemberStatementLoanTransaction.FindAll(lobj => lobj.LoanTransactionType == LoanTransactionType.Credit).Sum(sumObj => sumObj.LoanPoints)); ;
            lblLoanRedemption.Text = Convert.ToString(LstGetMemberStatementLoanTransaction.FindAll(lobj => lobj.LoanTransactionType == LoanTransactionType.Debit).Sum(sumObj => sumObj.LoanPoints)); ;
            lblLoanClosing.Text = Convert.ToString(LstGetMemberStatementLoanTransaction.FindAll(lobj => lobj.LoanTransactionType == LoanTransactionType.Credit).Sum(sumObj => sumObj.LoanPoints) - LstGetMemberStatementLoanTransaction.FindAll(lobj => lobj.LoanTransactionType == LoanTransactionType.Debit).Sum(sumObj => sumObj.LoanPoints));
        }
        else
        {
            lblLoanBonus.Text = "0";
            lblLoanRedemption.Text = "0";
            lblLoanClosing.Text = "0";
        }
    }

    protected void ddlProgramCurrency_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetStatementSummary(ddlProgramCurrency.SelectedItem.Text);
    }
}