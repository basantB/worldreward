﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AirPrintReceipt.aspx.cs"
    Inherits="AirPrintReceipt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UserControl/UCCTItinerayDetails.ascx" TagName="ItineraryDetails"
    TagPrefix="UC" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Air Ticket</title>
    <link href="Css/main.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0 auto; width: 768px;">
    <script language="javascript" type="text/javascript">
        window.print();
    </script>
    <form id="form1" runat="server">

        <div class="revncofm">
            <div class="wrapper">
                <div class="topStrip">
                    <a href="Index.aspx" class="logo">
                        <img src="Images/VoucherHead.jpg" /></a>
                    <!--referece no Starts-->
                    <!--referece no Ends-->
                </div>
                <!--Infi Destination Starts-->
                <div class="body-bdr">
                    <h2>Congratulations for your Danamon Rewards Ticket!</h2>
                    <p>
                        This is your E-ticket. Do present it with a valid photo identification at the airport
                        check-in counter.
                    </p>
                    <p>
                        For international travel: The check-in counters are open 4 hours prior to departure
                        and close strictly 2 hours prior to departure.
                    </p>
                    <div class="reference">
                        <div class="col1">
                            <div>
                                Reference No.
                            </div>
                            <asp:Label ID="lblTransactionRefNo" runat="server" />
                        </div>
                        <div class="col2">
                            <div>
                                DPoints
                            </div>
                            <asp:Label ID="lblTotalMiles" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <UC:ItineraryDetails ID="ucItinarary" runat="server" />
                    <div class="pass-details">
                        <h3>Additional Details</h3>
                        <table width="100%" style="text-align: left;" cellpadding="0" cellspacing="0">
                            <tr>
                                <th>Membership No.
                                </th>
                                <th>Name
                                </th>
                                <th>Mobile
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblMembershipReferenceNo" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerName" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerMobileNo" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2">Address
                                </th>
                                <th>Email
                                </th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblCustomerAddress" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerEmail" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                    <div class="pass-details mtop2">
                        <h3>Fair Conditions</h3>
                        <br />
                        <ul>
                            <li>1. Use your reference number for all communication with us on this booking.</li>
                            <li>2. Your PNR number serves as a comfirmation of your ticket status.</li>
                            <li>3. Carry a print out of this E-Ticket and present it at the airlines counter at
                                    the time of ceck-in.</li>
                            <li>4. Kindly carry a valid photo identification along with your E-Ticket.</li>
                            <li>5. No Cancellation and modification is allowed on a Ticket.</li>
                        </ul>
                        <div class="clr">
                        </div>
                        <br />
                        <h3 style="text-align: center;">Danamon Bank wishes you a pleasant journey & hopes to serve you again the future.</h3>
                        <div class="clr">
                        </div>
                    </div>
                </div>
                <!--Infi Destination Ends-->
            </div>
        </div>

    </form>
</body>
</html>
