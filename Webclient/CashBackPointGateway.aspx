﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CashBackPointGateway.aspx.cs" Inherits="CashBackPointGateway" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WorldRewardZ</title>
    <script src="js/jquery-1.7.1.min.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(window).load(function () {
            var flag = getParameterByName("flag");
         
            //Hotel
            //if (flag == "Hotel") {
            //    $.ajax({
            //        type: 'POST',
            //        url: 'PointGateway.aspx/BookHotel',
            //        contentType: 'application/json; charset=utf-8',
            //        dataType: 'json',
            //        data: "",
            //        cache: false,
            //        success: function (msg) {
            //            if (msg.d) {
            //                window.location = "HotelVoucher.aspx";
            //            } else {
            //                window.location = "BookingFailure.aspx";
            //            }
            //        },
            //        error: function (errmsg) {
            //            window.location = "BookingFailure.aspx";
            //        }
            //    });
            //}
            //CashBack
             if (flag == "Cashback") {
                $.ajax({
                    type: 'POST',
                    url: document.URL.toString().replace("PointGateway.aspx", "PointGateway.aspx/GetCashBack"),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: "",
                    timeout: 80000,
                    success: function (msg) {
                       
                        if (msg.d)
                            window.location = "CashBackSummary.aspx?request=true";
                        
                        else
                            window.location = "CashBackSummary.aspx?request=false";
                    },
                    error: function (jqXHR, status, errorThrown) {
                        window.location = "ErrorPage.aspx";
                    }
                });
            }
            //Car
            //else if (flag == "Car") {
            //    $.ajax({
            //        type: 'POST',
            //        url: 'PointGateway.aspx/BookCar',
            //        contentType: 'application/json; charset=utf-8',
            //        dataType: 'json',
            //        data: "",
            //        cache: false,
            //        success: function (msg) {
            //            if (msg.d) {
            //                window.location = "CarVoucher.aspx";
            //            } else {
            //                window.location = "BookingFailure.aspx";
            //            }
            //        },
            //        error: function (errmsg) {
            //            window.location = "BookingFailure.aspx";
            //        }
            //    });
            //}
            //else {
            //    //Flight
            //    $.ajax({
            //        type: 'POST',
            //        url: 'PointGateway.aspx/BookFlight',
            //        contentType: 'application/json; charset=utf-8',
            //        dataType: 'json',
            //        data: "",
            //        cache: false,
            //        success: function (msg) {
            //            if (msg.d) {
            //                window.location = "AirReceipt.aspx";
            //            } else {
            //                window.location = "BookingFailure.aspx";
            //            }
            //        },
            //        error: function (errmsg) {
            //            window.location = "BookingFailure.aspx";
            //        }
            //    });
            //}
        });
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.href);
            if (results == null)
                return "";
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="masthead" style="margin: 0 auto; width: 100%" class="StandardFontGlobal">
            <div style="width: 100%; text-align: center; margin: 0 auto; float: left; font-size: 100%;
                padding: 2% 0 1% 0;">
                Please wait while we process your request......
            </div>
            <div style="width: 100%; text-align: center; margin: 0 auto; padding: 5% 0 4% 0;">
                <img alt="" src="Images/Wait.GIF" border="0" style="width: 30%" />
            </div>
            <div style="width: 50%; text-align: center; margin: 0 auto;">
                <img alt="" src="Images/JointLogo.gif" border="0" style="width: 100%" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
