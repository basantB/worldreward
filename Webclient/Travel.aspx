﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="Travel.aspx.cs" Inherits="Travel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
<script>
    $(document).ready(function () {
        $("#dnarr").on('click', function (event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function () {
                    window.location.hash = hash;
                });
            }
        });
    });
    </script>
    <div class="banner">
        <ul class="bxslider">
            <li>
                <img src="images/bnr-travel.jpg" /></li>
        </ul>
    </div>
    <div style="text-align: center; margin-top: 10px;">
        <a href="#article" id="dnarr">
            <img style="opacity: .5; height: 50px;" src="images/down-arrow.gif" /></a>
    </div>
    <article id="article">
        <div class="wrap">
            <h1>Travel - Live Ultimate Experiences.</h1>
            <p>There are limitless reasons to travel the globe and time may always seem short. To make it easier for you to enjoy maximum from your travel, WorldRewardZ has selected a wide range of the best in airlines, hotels and car rentals to make most memorable holidays for you. Redeem your points from choices among 900+ airlines, 450,000 hotel stays and 150,000 car rentals.</p>
        </div>
    </article>
    <div class="bnrlinks">
        <ul class="type2">
            <li>
                <a>
                    <h2>900+ Airlines Worldwide</h2>
                    </a><a class="btnRed" href="TravellerSearch.aspx?Id=1">Explore</a>
                    <img src="images/bnrflight_thumbnail.jpg">
                
            </li>
        </ul>
        <ul class="clearfix">
            <li>
                <a href="#">
                    <h2>450,000 Hotels WorldWide</h2>   </a>
                    <a class="btnRed" href="TravellerSearch.aspx?Id=2">Explore</a>
                    <img src="images/bnrhotel_thumbnail.jpg">
             
            </li>
            <li class="bnrcol2">
                <div class="bnrcolbnr">
                    <a href="#">
                        <h2>Cars</h2></a>
                        <a class="btnRed" href="TravellerSearch.aspx?Id=3">Explore</a>
                        <img src="images/bnrcar_thumbnail.jpg">
                  
                </div>
                <div>
                    <a href="#">
                        <h2>700+ Lounge Worldwide</h2></a>
                         <a class="btnRed" href="TravellerSearch.aspx?Id=4">Explore</a>
                        <img src="images/bnrlounges.jpg">
                     
                </div>
            </li>
        </ul>
        <ul class="type2 clearfix">
            <li class="meetg-tours clearfix">
                <div class="tra-meetg">
                    <a href="#">
                        <h2>Meet & Greet</h2></a>
                        <a class="btnRed" href="TravellerSearch.aspx?Id=5">Explore</a>
                        <img src="images/bnrmeetg.jpg">
                    
                </div>
                <div class="tra-tours">
                    <a href="#">
                        <h2>Tours</h2></a>
                         <a class="btnRed" href="TravellerSearch.aspx?Id=6">Explore</a>
                        <img src="images/bnrtours.jpg">
                  
                </div>
            </li>
        </ul>
    </div>
</asp:Content>

