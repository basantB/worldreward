﻿
<%@ page language="C#" autoeventwireup="true" CodeFile="SearchPage.aspx.cs" inherits="SearchPage" masterpagefile="~/InfiPlanetMaster.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>

    <%--<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Search Wait</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <link href="Css/main.css" rel="stylesheet" />
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>--%>
    
    <script type="text/javascript" language="javascript">

        function getQuerystring(key, default_) {
            if (default_ == null) default_ = "";
            key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
            var qs = regex.exec(window.location.href);
            if (qs == null)
                return default_;
            else
                return qs[1];
        }
           //$(document).ready(function () {
          
            var cityfrom = getQuerystring("cityfrom");
            var cityto = getQuerystring("cityto");
            var departure = getQuerystring("departure");
            var arrival = getQuerystring("arrival")
            var departuredate = getQuerystring("departuredate");
            var isReturn = "";
            isReturn = getQuerystring("isReturn");
            var routetype = "";
            if (isReturn == "true") {
                routetype = "Round Trip";
            } else {
                routetype = "One Way";
            }
            var arrivaldate = getQuerystring("arrivaldate");
            if (routetype == "One Way") {
                arrivaldate = "";
            }
            var passengerType = "";
            var adult = getQuerystring("adult");
            if (parseInt(adult) > 0) {
                passengerType = getQuerystring("adult") + " adult ";
            }
            var child = getQuerystring("child");
            if (parseInt(child) > 0) {
                passengerType += getQuerystring("child") + " child ";
            }
            var infant = getQuerystring("infant");
            if (parseInt(infant) > 0) {
                passengerType += getQuerystring("infant") + " infant";
            }


            var classType = getQuerystring("economy");
            cityfrom = cityfrom.replace('%20', ' ');
            cityfrom = cityfrom.replace('%20', ' ');
            cityto = cityto.replace('%20', ' ');
            cityto = cityto.replace('%20', ' ');
            var searchDetails = "";
            if (arrivaldate == "" || arrivaldate == null) {
                searchDetails = cityfrom + " - " + cityto; //+ ", " + routetype + ", " + classType + ", " + departuredate + ", " + passengerType;
                searchDetails = searchDetails.replace('%20', ' ');
            }
            else {
                searchDetails = cityfrom + " - " + cityto;//departure + "-" + arrival; //+ ", " + routetype + ", " + classType + ", " + departuredate + "-" + arrivaldate + ", " + passengerType;
                searchDetails = searchDetails.replace('%20', ' ');
            }

            $("#CP_LabelYourSearchDetails").text(searchDetails);
            $.ajax({
                type: 'POST',
                url: document.URL.toString().replace("SearchPage.aspx", "SearchPage.aspx/FlightSearch"),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: "",
                timeout: 80000,
                success: function (msg) {
                    debugger;
                    //alert(msg.d);
                    if (msg.d)
                        window.location = "FlightList.aspx";
                    else
                        window.location = "NoResultFound.aspx?ERR=RESULTNOTFOUND";
                },
                error: function (jqXHR, status, errorThrown) {
                    alert(msg);
                    window.location = "ErrorPage.aspx";
                }
            });
        //});
    </script>

    <%--</head>--%>

    <%--<body class="scrhpg">
    <form id="form1" runat="server">--%>

    <div class="srchcircle">
        <div class="scrhcnt">
            <%--<div>
                <img src="images/icoflt.png" />
            </div>--%>
            <h1>Searching Flights</h1>
            <p>
                <asp:Label ID="LabelYourSearchDetails" runat="server"></asp:Label>
            </p>
            <div id="cssload-wrapper">
                <div id="cssload-border">
                    <div id="cssload-whitespace">
                        <div id="cssload-line">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <%--    </form>
</body>
</html>--%>
</asp:Content>

