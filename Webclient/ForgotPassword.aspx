﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <script src="js/Validation.js" type="text/javascript"></script>
    <link href="css/mystyle.css" rel="stylesheet" />
    <style>
        .myacc-login ul 
        {
         margin-top:25px;
        }
    </style>
    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>
<div class="inr-wrap">
    <section class="statcpgCont">
      <aside class="pg-banr">
            <img src="images/forget-psw.png" />
      </aside>
        <div class="flt-contehldr">
        <div class="flt-divR">
                    <div class="row">
                        <h2 class="robot">Forgot Password</h2>
                        <h5 class="robot mandatory-info">Please provide your WorldRewardZ Member ID and we will send a link to your registered email id for you to reset password. </h5>
                    </div>
                    <div class="clear"></div>
                    <div class="myacc-login">
                           <ul>
                               <li class="label">WorldRewardZ ID:</li>
                               <li class="input"><input type="text" class="robot" id="txtMemberShipReference" runat="server" onkeypress="var retValue = LoginOnEnter(event); event.returnValue = retValue; return retValue;"/></li>
                              <li class="clear disblk">&nbsp;</li>
                              <li class="label">&nbsp;</li>
                              <li class="input loginbtn">
                              <asp:Button ID="btnContinue" runat="server" CssClass="button" OnClientClick="var retValue = ForgotPassword(); event.returnValue = retValue; if(event.preventDefault)event.preventDefault(); return retValue;" Text="Continue" />   
                              </li> 
                              <li>
                               <li class="clear disblk">&nbsp;</li>
                               <div id="ErrorMsgContainer" class=" ErrorMsgContainer mtop_10" runat="server">
                                <asp:Label runat="server" ID="lblForgotPasswordError" Text=""></asp:Label>
                                <div id="ForgotPasswordValidation" style="color:red;">
                                </div>
                            </div>
                              </li>    
                           </ul>
                       </div>  
                    </div>
        </div>      
    </section> <div class="clr">
        </div>
    </div>
</asp:Content>
