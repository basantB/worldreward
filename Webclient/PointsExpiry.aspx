﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="PointsExpiry.aspx.cs" Inherits="PointsExpiry" %>

<%@ Register Src="~/UserControl/ActivationMenu.ascx" TagName="ActivationMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="css/dropDown.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.selectric.min.js" type="text/javascript"></script>
    <link href="css/myAccount.css" rel="stylesheet" />
    <script>

        window.onload = function () {
            ApplyCss();
        };
        function ApplyCss() {
            $('select').selectric({
                maxHeight: 100,
                disableOnMobile: false,
                responsive: true
            });
        };
        //On PostBack
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    ApplyCss();
                }
            });
        };
    </script>
    <style type="text/css">
        .DDLCover
        {
            float: left;
            border: 1px solid #ccc;
            width: 18%;
            margin-top: 0 0 5px 0;
        }
        @media screen and (max-device-width :540px)
        {
            .DDLCover
            {
                float: left;
                border: 1px solid #ccc;
                width: 94%;
                margin: -9px 0 5px 0;
            }
        }
    </style>
    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>

    <article id="article">
         <div class="wrap">
            <h1>Experience Abundance with WorldRewardZ</h1>
            <p>Below is a categorized listing of your activities at WorldRewardZ. As you glance through them, you can immerse in the joy of having the best choices in the world available to you simply at a click, with WorldRewardZ. Go ahead and multiply your unique experiences.</p>
			<div class="ManyWrap MyAccount">
            <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
            <div class="InnCont">
             <div id="divCurrency" class="myacc-user" runat="server">
              </div>
           <ul class="tabInfo">
		    <li>
		    <label>Select Year</label>
            <div class="selectdiv input-bg DDLCover">
                <asp:DropDownList ID="dtYear" CssClass="fL" runat="server" OnSelectedIndexChanged="dtYear_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
                </div>
			</li>
	        </ul>
            <div id="rptExpirySchedule" runat="server">
                    </div>
                    <br />
                    <div class="shortdetails" id="divExpiredon" runat="server">
                        <b>Your
                            <asp:Label ID="lblMiles" runat="server" Text=""></asp:Label>
                            WorldRewardZ are going to expire on:
                            <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>.</b>
                    </div>
             </div>
            </div>
             </div>
         </article>
    <!--end article-->

    <%--<div class="inr-wrap">
      <section class="myaccContent">
      <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
      <div class="myacc-conthdr">
       <div id="divCurrency" class="myacc-user" runat="server">
             My Account WorldRewardZ Expiry:
        </div>
        <div class="acc-transactnsumry">
                    <br />
                    Your WorldRewardZ expiry schedule:
                    <br /><br />
                   <span class="fL" style="padding: 1%;">Select Year:</span> 
                   <div class="input-bg DDLCover">
                    <asp:DropDownList ID="dtYear" CssClass="fL" runat="server" OnSelectedIndexChanged="dtYear_SelectedIndexChanged"
                        AutoPostBack="true">
                    </asp:DropDownList>
                    </div>
                    <br />
                    <br />
                    <div id="rptExpirySchedule" runat="server">
                    </div>
                    <br />
                    <div class="shortdetails" id="divExpiredon" runat="server">
                        <b>Your
                            <asp:Label ID="lblMiles" runat="server" Text=""></asp:Label>
                            WorldRewardZ are going to expire on:
                            <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>.</b>
                    </div>
                </div>
    </div>
     </section>
        <div class="clr">
        </div>
    </div>--%>
    <style>
        td
        {
            border: 1px solid #aaaaaa;
            padding: 1% 0;
            text-align: center;
        }
        table
        {
            width: 80%;
        }
    </style>
</asp:Content>