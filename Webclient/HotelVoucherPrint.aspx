﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HotelVoucherPrint.aspx.cs"
    Inherits="HotelVoucherPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Voucher</title>
    <link href="Css/main.css" rel="stylesheet" type="text/css" />
    <link href="Css/HotelMain.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        window.print();
    </script>
</head>
<body style="margin: 0 auto; width: 768px;">
    <form id="form1" runat="server">
        <div class="revncofm">
            <div class="wrapper">
                <div class="topStrip">
                    <a href="Index.aspx" class="logo">
                        <img src="Images/VoucherHead.jpg" /></a>
                    <!--referece no Starts-->

                    <div class="clr">
                    </div>
                    <!--referece no Ends-->
                </div>
                <!--Infi Destination Starts-->
                <div class="body-bdr">
                    <h2>Thank you
                    <asp:Label ID="lblPersonName" runat="server" Text="Nil"></asp:Label>, Your booking
                    is now <strong>confirmed.</strong>
                    </h2>
                    <div class="reference">
                        <div class="col1">
                            Reference No.
                            <asp:Label ID="lblTransactionReference" runat="server" Text=""></asp:Label>
                        </div>
                        <div id="divTotalMiles" runat="server" class="col2">
                        </div>
                    </div>
                    <div class="mt20x">
                        Voucher No. : <strong>
                            <asp:Label ID="lblExternalRefId" runat="server" Text=""></asp:Label>
                        </strong>
                    </div>
                    <div class="mt20x">
                        Booking ID : <strong>
                            <asp:Label ID="lblBookingID" runat="server" Text="Nil"></asp:Label>
                        </strong>
                    </div>
                    <br /><br />
                    <div class="pass-details">
                        <div class="w50P FL">
                            <h3>Booking Details
                            </h3>
                            <div class="tblbody">
                                <ul class="">
                                    <li>
                                        <div class="label">
                                            Your Reservation
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblTotalRoom" runat="server" Text="Nil"></asp:Label><asp:Label ID="lblTotalnight"
                                            runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Check-in
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblCheckinDate" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Check-out
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblCheckoutDate" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Booked by
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblBookPersonName" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            EMail
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblPersonEmail" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Address
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblPersonAddress" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="w50P FL ml1p">
                            <h3 class="tblhdr-blk">Hotel Details
                            </h3>
                            <div class="tblbody">
                                <ul class="">
                                    <li>
                                        <div class="label">
                                            Hotel Name
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblHotelName" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Address
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblAddress" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Contact
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblHotelPhone" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Fax
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblHotelFax" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Website
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblWebsite" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Rating
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblRating" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                    <div class="otherinfo w100P">

                        <div class="pass-details">
                            <div class="w50P FL">
                                <h3>Room Details</h3>
                                <p>
                                    <asp:Label ID="lblRoomtype" runat="server" Text="Nil"></asp:Label>
                                </p>
                            </div>
                            <div class="w50P FL">
                                <h3>Special Request</h3>
                                <p>
                                    <asp:Label ID="lblSpecialRequest" runat="server" Text="Nil"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="pass-details">
                            <div class="w50P FL">
                                <h3>Payment Type</h3>
                                <div style="width: 100%;" id="divPaymentDetails" runat="server">
                                </div>
                                <div class="note">
                                    Please Note: Additional Supplements (e.g. extra bed) are not added
                                to this total.
                                </div>
                            </div>
                            <div class="w50P FL">
                                <asp:Image ID="imgMap" runat="server" />
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                </div>
                <!--Infi Destination Ends-->
            </div>
        </div>
    </form>
</body>
</html>
