﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="faqs.aspx.cs" Inherits="faqs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" Runat="Server">
 <script>
$(document).ready(function(){
	//toggle the componenet with class accordion_body
	$(".accordion_head").click(function(){
		if ($('.accordion_body').is(':visible')) {
			$(".accordion_body").slideUp(300);
			$(".plusminus").text('+');
		}
		$(this).next(".accordion_body").slideDown(300); 
		$(this).children(".plusminus").text('-');
	});
});
</script>
 <link href="css/jquery.css" rel="stylesheet" />
 <link href="css/myAccount.css" rel="stylesheet" />
 <article id="article">
        <div class="wrap staticPage faqPage">
            <h1>FAQs</h1>
			<div class="accordion_container">
		<ul>
        <li>
				<div class="accordion_head">FLIGHTS<span class="plusminus">-</span></div>
				<div class="accordion_body" style="display: block;">
				<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				<p class="ques">Can I do an online check-in?</p>
				<p>Online check-in is not always offered by every airline on its website. If the website does permit online  check-in, you may do so using your Airline Booking Reference number.</p>
				<p class="ques">Check-in:</p>
				<ul>
					<li>You need to check-in at least 3 hours prior to departure for all International flights, and 2 hours prior to departure for all Domestic flights. However, we would always recommend you call the respective airline to understand their check-in timing policy. </li>
					<li>As per airline rules, the standard check-in time begins 3 hours prior to departure.</li>
					<li>Infants must have valid proof of age documents showing that the infant is less than 24 months old. You need to carry appropriate travel permissions (valid passport, visa, immigration clearance etc.) acquired before departure. WorldRewardZ or Infinia Services & Solutions DMCC is not responsible for lack of documents produced during check-in.</li>
				</ul>
				<h2>Is there a Customer Support number?  </h2>
				<p>Yes, you may address your queries to  WorldRewardZ’s 24-hour Customer Support at +97144277600 or write to support@infiniasns.com.</p>
				
				<br>
				<h2>Check-in:</h2>
				<ul>
					<li>The passenger needs to check-in at least 3 hours prior to departure for all International flights, and 2 hours prior to departure for all Domestic flights. However, we would always recommend you call the respective airline to understand its check-in policy timings. </li>
					<li>As per airline rules, the standard check-in time begins 3 hours prior to departure.</li>
					<li>Infants must have valid proof of age documents showing that the infant is less than 24 months old. You will be required to carry appropriate travel permissions (valid passport, visa, immigration clearance etc.) acquired before departure. WorldRewardZ or Infinia Services & Solutions DMCC is not responsible for lack of documents produced during check-in.</li>
				</ul>
				<br>
				<p class="ques">Is there a Customer Support number?  </p>
				<p>Yes, you may address your queries to  WorldRewardZ’s 24-hour Customer Support at +97144277600 or write to support@infiniasns.com. </p>
				
				<p class="ques">How do I print my ticket?</p>
				<p>You can view all your upcoming and completed trips by selecting the ‘Manage Bookings’ section on the Website. You can view/print your ticket by clicking on the Booking Reference number.</p>
				
				<p class="ques">I did a search for flight tickets and selected my flight. However, after providing the passenger details, I see that the fares have increased. Why?</p>
				<p>The airline fares are dynamic in nature and are based on availability of the seats on the particular flight. There are always chances that the seats selected by you may get sold till the time you complete your booking. Therefore, to minimize the chances of booking failures, we check the availability of the seats before you proceed with the payment. If you find that the fare has increased at this step, you have the option of going ahead with the booking or to refresh the search.</p>
				
				<p class="ques">Can I enter my Frequent Flyer number at the time of booking? </p>
				<p>Currently we do not accept the Frequent Flyer number. You can provide your Frequent Flyer number at the airline counter at the time of check-in.</p>
				
				<p class="ques">How do I get my e–ticket details? </p>
				<p>We will send your e–ticket details to the registered e-mail address provided by you to us when you made your reservation.</p>
				
				<p class="ques">How do I confirm my seat assignments?</p>
				<p>Kindly call your airline directly to check whether you can choose your seat by quoting your airline Booking Reference number.</p>
				
				<p class="ques">How do I cancel a flight reservation?</p>
				<p>For bookings made on WorldRewardZ’s Website, no cancellations are allowed if done voluntarily. Hence, no WorldRewardZ will be refunded back into your account. For any reasons not initiated by you, i.e. flights being grounded/cancelled/or any other unforeseen circumstances wherein you are denied travel, applicable WorldRewardZ will be refunded back into your account. This again would be as per the discretion of WorldRewardZ and Infinia Services & Solutions DMCC.</p>
				
				<p class="ques">How do I amend my booking?</p>
				<p><b>WorldRewardZ</b> can assist you with amendments to most bookings. In some cases, though, you will need to contact the airline directly. Every booking made on WorldRewardZ is subject to amendment charges levied by the airline, which may vary by flight and booking class.</p>
				<p>If you amend your booking, you will be charged the difference in fare, if any, and applicable when the amendment is made. However, if the new fare is lower than the original fare, the difference in the fare amount will not be refunded. The rebooking charges as applicable will be collected and charged to your WorldRewardZ account in equivalent WorldRewardZ. In addition to the airline's amendment charges, <b>Infinia Services & Solutions DMCC</b> charges an amendment handling fee payable in WorldRewardZ equivalent to USD 50.</p>
				<p><b>Infinia Services & Solutions</b> DMCC will collect these charges from you when we make the changes to your travel plans. We will also collect the difference in fare, if any is applicable when the amendment is made. Depending on the airline policy, some booked fares may not allow an amendment.</p>
				<p class="ques">Can I book a multi–city trip? </p>
				<p>No, you cannot do a multi-city booking at this juncture. To book a multi-city travel, you will have to book individual sectors, separately.</p>
				
				<p class="ques">I did not get an e-mail confirmation. What do I do?</p>
				<p>If you do not receive an e-mail confirming your booking, there is a possibility that an improper e-mail address was registered in our records or your Internet Service Provider blocked the e-mail as a ‘spam’, in which case we suggest you check the address and your spam folder. You can also call WorldRewardZ’s 24-hour Customer Support at +97144277600 or write to support@infiniasns.com.</p>
				
				<p class="ques">What is the maximum number of seats I can book?</p>
				<p>A maximum of 9 seats can be booked at one time. If you need to book for more than 9 travelers, you will have to repeat the booking process for the additional travelers. Also, some airlines do not allow booking for more than 4 passengers (adult + children) at one time.</p>
				
				<p class="ques">Can I book tickets for infants?</p>
				<p>Yes, you can book for one infant per adult on WorldRewardZ. The age of the infant must be below 24 months on the date of travel. Make sure you carry valid proof of age documents at the time of check-in. The infant is not awarded a seat on the flight and has to travel with the accompanying adult.</p>
				
				<p class="ques">How do I find out my baggage limit?</p>
				<p>Limits for both, cabin and checked-in baggage vary by airline rules and sometimes by criteria of baggage weight or numbers. Please contact the airline directly or visit the airline’s website for accurate details regarding baggage limits, as rules vary from time to time without prior notice.</p>
				
				<p class="ques">I’ve booked my tickets but now need to add my child’s tickets to my booking. How do I proceed for this? </p>
				<p>Infinia Services & Solutions DMCC does not do individual bookings for children below 12 years of age. You would need to contact your airline directly to book tickets for your child. </p>
				
				<p class="ques">Do I need to confirm my flight reservation before I fly? </p>
				<p>No, you do not have to. However, if you wish, you may contact the airline directly. </p>
				<p class="ques">How do I confirm special services like seats/meals, wheelchair, bassinets etc.? </p>
				
				<p>WorldRewardZ does not do a pre–seating or confirm meals. You will have to call the airline directly for any seat or meal requirements. Also, airlines require to be notified a minimum of 24–48 hours prior in case any special meal or service needs to be confirmed. All special requests are subject to confirmation from the airline and in some cases may be chargeable as per airline policy.</p>
				
				<p class="ques">Do I have to show my e–ticket confirmation e-mail at the airline check–in counter? </p>
				<p>Yes, you do. Some airports do not allow you to enter without a printout of your e–ticket, so be sure to carry one with you. If you have forgotten to carry your e–ticket printout, you can contact the airline ticketing counters at the airport to issue a duplicate itinerary receipt.</p>
				
				<p class="ques">Do you issue paper tickets? </p>
				<p>No, we do not issue paper tickets.</p>
				
				<p class="ques">I misspelled my name while booking a ticket. How do I get it changed? </p>
				<p>You can call us to check if the airline which you have booked with entertains change–of–name requests. However, if the airline does not allow it, you will have to cancel and rebook the ticket. Normal amendment charges apply in such cases. Certain airlines also do not allow a reissue of ticket on such occasions and a new ticket has to be issued. WorldRewardZ will not entertain any refund or reversal of WorldRewardZ in such cases.</p>
				
				<p class="ques">Do I have to pay anything extra at the airport?</p>
				<p>Normally, all taxes for the airport are collected on the ticket at the time of booking itself. However, many airports do collect development fees/taxes at the time of departure. Please check with the respective airline before you travel.</p>
				
				
				
				

			</div>
            </li>
            
        <li>
				<div class="accordion_head">HOTELS<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
				<div class="description" id="discription" style="overflow: hidden;" tabindex="2">			
				<p class="ques">Can I book a room where more than two adults will occupy it?</p>
				<p>Most of the hotels allow additonal guests in a room for an extra charge, but the number of occupants should not exceed the maximum number of guests allowed per room by the hotel. You would need to directly check with the hotel for such bookings, as it varies as per the terms and conditions of the hotel.</p>
				
				<p class="ques">Our children will be traveling with us; will there be any hotel charge for them as well?</p>
				<p>When making your booking, select the number of children traveling with you from the 'Children' drop-down box. If you select one child, our search will give you the price of a Double Room with a child, not including an extra bed. If you want an extra bed in the room, you will need to increase the number of passengers in your search. </p>
				
				<p class="ques">Can I request a room preference such as smoking/wheelchair friendly etc.?</p>
				<p>Yes, you can. Although, you will have to check with the hotel directly for such requests and book online. </p>
				
				<p class="ques">I did not receive an e-mail confirmation. What do I do?</p>
				<p>If you do not receive a confirmation e-mail from us, there is a possibility that an improper e-mail address was registered in our records or your Internet Service Provider blocked the e-mail as a ‘spam’, in which case we suggest you check the address and your spam folder. You can also contact WorldRewardZ’s 24-hour Customer Support at  +97144277600 or write to support@infiniasns.com. It is important at the time of contacting us that you convey information on:</p>
				<ul>
					<li>Name against which the reservation was made</li>
					<li>Location (city) and name of the hotel</li>
					<li>Dates of check-in/check-out  </li>
				</ul>
				<p class="ques">How long will it take for the hotel to receive my booking information?</p>
				<p>The time taken for the hotel to receive your reservation information varies by hotel and arrival date, although most hotels should receive this information within 12 hours from the time you make your booking (excluding night timings and weekends, as this is when the hotel reservation desks are closed). Please note that this does not apply to bookings made for the same day.</p>
				
				<p class="ques">What is the standard check-in time for hotels?</p>
				<p>The standard check-in time is after 1400 hours (local time). But it may vary according to season and/or city. Please check with the hotel for clarity.</p>
				
				<p class="ques">I am arriving late; will the hotel hold my room until I arrive?</p>
				<p>Yes, the hotel will hold your room booking until 7am the day after your planned arrival date, as your reservation is a confirmed booking. However, please check with the hotel for details.</p>
				
				<p class="ques">I am arriving earlier than planned; will the hotel accommodate me? </p>
				<p>In such case, we suggest you contact the hotel in advance to check for accomodation, as terms and conditions vary with different hotels. </p>
				
				<p class="ques">How can I get a receipt or invoice for my hotel booking?</p>
				<p>You can view all your upcoming and completed trips by selecting the ‘Manage Bookings’ section. Further, you can view/print your receipt by selecting the Booking Reference number. </p>
				
				<p class="ques">What are the cancellation charges on redemption bookings? </p>
				<p>For bookings made through WorldRewardZ, no cancellations are allowed, if done voluntarily by you; hence no WorldRewardZ will be refunded back into your account. For any reasons not initiated by you i.e. hotels sold out and/or any other unforeseen circumstances wherein you are denied stay, applicable WorldRewardZ will be refunded back into your account. This again is as per the discretion of WorldRewardZ.</p>
				
				<p class="ques">What is the difference between a Double and a Twin Room?</p>
				<p>A Double Room has 1 King-sized bed, whereas a Twin Room has 2 single or Queen-sized beds.</p>
				
				<p class="ques">Do I need to reconfirm my hotel booking?</p>
				<p>No, this is not necessary. However if you wish, you may contact the hotel directly.</p>
				
				<p class="ques">Can I change the dates of my hotel booking?</p>
				<p>WorldRewardZ does not support changes/modifications to bookings once they are made. Please do not call the hotel directly for reservation changes or cancellations, as the same would not be entertained.</p>
				
				<p class="ques">I need to avail an early check-in/late check-out. Can this be done?</p>
				<p>This depends completely on the hotel’s room availability on that particular date. WorldRewardZ cannot guarantee anything to this regard. </p>
				
				<p class="ques">Are meals/breakfast included in the hotels booked?</p>
				<p>Not all hotels include complimentary breakfast.</p>
				
				<p class="ques">Can I make a hotel booking for today’s check-in?</p>
				<p>Sorry this is not possible. Hotel reservations have to be booked a minimum of 3 days in advance. </p>
				
				<p class="ques">Can I request for connected rooms if I am booking 2 rooms, or a smoking room preference etc.?</p>
				<p>Most hotels are booked directly through international suppliers. We can add these requests as a comment to the hotel, however, WorldRewardZ does not guarantee the confirmation of the same. These requests would have to be made directly at the hotel at the time of check-in.		 </p>
				
				
				</div>
				</div>
        </li>
		<li>
				<div class="accordion_head">CAR RENTALS<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				
				<p class="ques">Do I get an immediate booking confirmation?</p>
				<p>Once car redemption is made, the confirmation is not immediate. It takes a minimum of 24-48 hours for a confirmation. For any queries regarding confirmation, you can call WorldRewardZ’s 24-hour Customer Support at +97144277600 or write to support@infiniasns.com.<br><br>At times when your booking is not confirmed, the WorldRewardZ will be refuned back into your account and you can repeat your attempt to book online.    </p>
				
				<p class="ques">Can I make an amendment to my booking?</p>
				<p>Amendments are possible for most bookings. However, the request needs to come in atleast 72 hours prior to the date of booking. Confirmation would depend on the service provider's availability. In some cases, amendments may not be possible, as per the car rental policy.</p>
				
				<p class="ques">Will there be any extra charges if I wish to make an amendment to my booking? </p>
				<p>Extra amendment surcharges would be incurred if there is a change in the duration, location, or the type of car. Over and above the amendment charges, Infinia Services & Solutions DMCC will charge a handling fee payable in WorldRewardZ equivalent to USD 50. </p>
				
				<p class="ques">Can I cancel my booking?</p>
				<p>For bookings made through WorldRewardZ, no cancellations are allowed if initiated voluntarily by you. Hence no WorldRewardZ will be refunded back into your account. However, if the reason for cancellation is from the service provider/supplier’s end, the WorldRewardZ will be duly credited into your account within 45 business days.</p>
				
				<p class="ques">How old do I have to be to rent a car?</p>
				<p>For most car hire companies, the age requirement is between 25 and 70 years. If the driver is under 25 or over 70 years of age, you may have to pay an additional fee (charges may vary depending on city/country and type of car).</p>
				
				<p class="ques">Is it possible to rent a car for another person through my account? </p>
				<p>Yes, as long as they meet the stipulated requirements. You would need to mention their details while making the reservation.</p>
				
				<p class="ques">Does my car redemption include all charges or is there anything extra I need to pay?</p>
				<p>Most car rental services include in their price - Theft Protection, Collision Damage Waiver (CDW), local taxes, airport surcharges and any road fees. However, you would be responsible for any ‘extra’ charges at the time of car pick-up, fees for a young/additional driver, one-way fees. The same will be explained to you, in addition to ways to reduce costs like carrying child seats and GPS before you make your car booking. You may read more about this in the ‘terms and conditions’ section of the car rental service you are booking with. </p>
				
				<p class="ques">Do I have to pay any deposit?</p>
				<p>Yes, the time you pick your car for rental, you will be required to leave a security deposit against possible damage to the car during your rental period. The deposit will be charged directly by the car hire company. Quite often, a credit card in the name of the main driver is required. If the condition of the car and extras is the same on return as at the time of rental, and is in accordance with the fuel policy, the security deposit will be refunded upon returning the car (please note that it may take up to 30 business days for the WorldRewardZ to be refunded into your account).</p>	
			</div>
			</div>
            </li>
			
		<li>
				<div class="accordion_head">SHOP<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				
				<p class="ques">How many days will it take to get my reward merchandise delivered?</p>
				<p>Your reward merchandise will be delivered within 4-5 working weeks (subject to availability of the stock).</p>
				
				<p class="ques">What should I do if the reward merchandise doesn’t reach me within 21 working days?</p>
				<p>Please call WorldRewardZ’s 24-hour Customer Support at +97144277600 or write to support@infiniasns.com, to check on the status of your order.</p>
				
				<p class="ques">What if the product reaches my mentioned delivery address when I was away or not available?</p>
				<p>Should this occur, the delivery service would attempt to deliver the product in the next 5 business days once again, so kindly ensure your availability. The process will be repeated only once, after which delivery charges will be levied and would be payable by you directly to the delivery service.</p>
				
				<p class="ques">What if I want to exchange/upgrade my order?</p>
				<p>Once an order has been placed it cannot be exchanged or upgraded.</p>
				
				<p class="ques">What should I do if the reward merchandise delivered to me is wrong or damaged?</p>
				<p>In the unfortunate event that the merchandise is either incorrect or damaged, please call WorldRewardZ’s Customer Support at +97144277600 or write to support@infiniasns.com, within 24 hours of the merchandise being delivered. In case we do not receive the request within 24 hours, no replacement will be permitted. WorldRewardZ will not be refunded to you. </p>
				
				<p class="ques">What should I do if I ordered the wrong item?</p>
				<p>In such a case your WorldRewardZ will not be refunded back to you. </p>
				
				<p class="ques">Is the Delivery FREE of charge?</p>
				<p>The cost of shipping would be included in the WorldRewardZ utilized for redemption. However, it would vary according to the ‘WorldRewardZ’ merchandise selected. However, applicable taxes, customer fees and charges may be applied by the relevant local government.</p>
				
				<p class="ques">Do you deliver items outside the state/country from where the order is placed?</p>
				<p>No, products are delivered only within the country/state where the booking has been made from.</p>
				
				<p class="ques">What if I want to cancel my order?</p>
				<p>Upon cancellation, your WorldRewardZ will not be refunded back to you. To cancel your order please call WorldRewardZ’s 24-hour Customer Support at +97144277600 or write to support@infiniasns.com. </p>
				
			
				
			</div>
			</div>
            </li>
			
				<li>
				<div class="accordion_head">MEET & GREET SERVICES<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				<p class="ques">How will I receive the confirmation of my booking? </p>
				<p>You will be intimated about the confirmation of your booking via e-mail. </p>
				
				<p class="ques">I did not receive an e-mail confirmation. What do I do?</p>
				<p>If you do not receive an e-mail confirming your booking, there is a possibility that an improper e-mail address was registered in our records or your Internet Service Provider blocked the e-mail as a ‘spam’, in which case we suggest you check the address and your spam folder. You can also call WorldRewardZ’s 24-hour Customer Support at +97144277600 or write to support@infiniasns.com.</p>
				
				<p class="ques">I find that the list of Meet & Greet services has been amended. Why? </p>
				<p>The list of Meet & Greet services on WorldRewardZ is dynamic and can be based on the availability, therefore choices offered may differ from time to time. Kindly refer to the list on WorldRewardZ before making a booking. </p>
				
				<p class="ques">Is it possible to make amendments to my booking?   </p>
				<p>No. WorldRewardZ does not accept amendments to bookings for Meet & Greet services.  </p>
				
			</div>
			</div>
            </li>
			
			<li>
				<div class="accordion_head">TOURS<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				<p class="ques">Can I do a multi–tour booking? </p>
				<p>The same is not possible at this juncture. To do a multi-tour booking, you will have to book individual tours, separately.</p>
				
				<p class="ques">How will I receive the confirmation of my booking? </p>
				<p>You will be intimated about the confirmation of your booking via e-mail. </p>
				
				<p class="ques">I did not receive an e-mail confirmation. What do I do?</p>
				<p>If you do not receive an e-mail confirming your booking, there is a possibility that an improper e-mail address was registered in our records or your Internet Service Provider blocked the e-mail as a ‘spam’, in which case we suggest you check the address and your spam folder. You can also call WorldRewardZ’s 24-hour Customer Support at +97144277600 or write to support@infiniasns.com.</p>
				
				<p class="ques">Can I make changes to my booking?  </p>
				<p>WorldRewardZ does not accept amendment requests for tour bookings. </p>
				
				<p class="ques">Can I request for special facilities for elders, children etc while booking my tour? </p>
				<p>WodlRewardZ provides you the experinece of tours in association with third party operators. We suggest you contact the tour operator directly to know of their policy.  </p>
				
				<p class="ques">Is there a standard limit to the number of people per tour? </p>
				<p>No. Every tour on WorldRewardZ has a specific criteria. Please check the features of each tour before making your booking.   </p>

			</div>
			</div>
            </li>
		
			<li>
				<div class="accordion_head">LOUNGE<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				<p class="ques">What if I forget my lounge voucher?</p>
				<p>Although you would receive an e-mail confirmation of your lounge booking, it is recommended to carry a print copy of your lounge voucher before entering. Certain airport rules mandate submitting a print copy of the same. </p>
				
				<p class="ques">Can I book a lounge for another guest? </p>
				<p>Yes. It is possible to book a lounge for any guest provided the guest carries and provides a copy of the confirmation bearing his/her name at the lounge reception. </p>
				
				<p class="ques">Can I cancel my lounge booking?</p>
				<p>For bookings made through WorldRewardZ, no cancellations are allowed, if done voluntarily. Hence no WorldRewardZ will be refunded back into your account. For any reasons not initiated by you i.e. hotels sold out and/or any other unforeseen circumstances wherein you are denied stay, applicable WorldRewardZ will be refunded back into your account. This again is as per the discretion of WorldRewardZ or Infinia Services and Solutions DMCC. </p>
				
				<p class="ques">Can I make changes to my booking?</p>
				<p>Once a lounge booking is completed on WorldRewardZ, no amendments to the same will be considered. </p>
				

			</div>
			</div>
            </li>
	</div>
        </div>
		</li>
	    </ul>	
	    </div>

</article>
</asp:Content>

