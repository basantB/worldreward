﻿using Core.Platform.Member.Entites;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Transactions.Entites;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.UniqueNumberGenerator;
using InfiPlanetModel.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PayByPoints : System.Web.UI.Page
{

    #region Private Properties
    private int CurrentPage
    {
        get
        {
            object objPage = ViewState["_CurrentPage"];
            int _CurrentPage = 0;
            if (objPage == null)
            {
                _CurrentPage = 0;
            }
            else
            {
                _CurrentPage = (int)objPage;
            }
            return _CurrentPage;
        }
        set { ViewState["_CurrentPage"] = value; }
    }
    private int fistIndex
    {
        get
        {

            int _FirstIndex = 0;
            if (ViewState["_FirstIndex"] == null)
            {
                _FirstIndex = 0;
            }
            else
            {
                _FirstIndex = Convert.ToInt32(ViewState["_FirstIndex"]);
            }
            return _FirstIndex;
        }
        set { ViewState["_FirstIndex"] = value; }
    }
    private int lastIndex
    {
        get
        {

            int _LastIndex = 0;
            if (ViewState["_LastIndex"] == null)
            {
                _LastIndex = 0;
            }
            else
            {
                _LastIndex = Convert.ToInt32(ViewState["_LastIndex"]);
            }
            return _LastIndex;
        }
        set { ViewState["_LastIndex"] = value; }
    }
    #endregion

    #region PagedDataSource
    PagedDataSource _PageDataSource = new PagedDataSource();
    #endregion
    int TotalPagecount = 10;
    string ddlvalue = string.Empty;
    List<TransactionDetails> LobjTransactionDetails = null;
    List<string> LobjTotalpage = new List<string>();

    protected void Page_Load(object sender, EventArgs e)
    {
        // Stop Caching in IE
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);

        // Stop Caching in Firefox
        Response.Cache.SetNoStore();
        if (Session["MemberDetails"] == null)
        {
            Response.Redirect("Login.aspx");
        }
        if (!Page.IsPostBack)
        {
            try
            {
                RdlSearchtype.SelectedIndex = 0;
                ViewState["CurrentPage"] = 1;
                CurrentPage = Convert.ToInt32(ViewState["CurrentPage"]);
                BindDataRangeFilter();
                BindAccrualSummaryInfo();
                //divOldTrnx.Attributes.Add("style", "display:none");                
                //divNavigation.Attributes.Add("style", "display:none");
                //divNewTrnx.Attributes.Add("style", "display:none");
                //divConfirmationMsg.Attributes.Add("style", "display:none");
                //divAvailability.Attributes.Add("style", "display:none");
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(ex.Message);
            }
        }
    }

    protected void RdlSearchtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        //visible_table_status();
        if (RdlSearchtype.SelectedIndex == 0)
        {
            divNavigation.Attributes.Add("style", "display:block");
            divOldTrnx.Attributes.Add("style", "display:block");
            divNewTrnx.Attributes.Add("style", "display:none");
            divConfirmationMsg.Attributes.Add("style", "display:none");
            divAvailability.Attributes.Add("style", "display:none");

            BindDataRangeFilter();
            BindAccrualSummaryInfo();
        }
        else if (RdlSearchtype.SelectedIndex == 1)
        {
            divNavigation.Attributes.Add("style", "display:none");
            divOldTrnx.Attributes.Add("style", "display:none");
            divNewTrnx.Attributes.Add("style", "display:block");
            divRedeem.Attributes.Add("style", "display:none");
        }
        //Getredemptioninfo();
    }

    public void BindAccrualSummaryInfo()
    {
        SearchTransactions objSearchTransactions = new SearchTransactions();
        objSearchTransactions.RelationReference = ((Session["MemberDetails"] as MemberDetails).MemberRelationsList.Find(lobj=>lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
        objSearchTransactions.MinimumRange = Convert.ToInt32(ddlvalue.ToString().Split('-')[0]);
        objSearchTransactions.MaximumRange = Convert.ToInt32(ddlvalue.ToString().Split('-')[1]);
        objSearchTransactions.ProgramId = Convert.ToInt32((Session["MemberDetails"] as MemberDetails).ProgramId);
        objSearchTransactions.RelationType = Convert.ToInt32(RelationType.LBMS);
        InfiModel lobjInfiModel = new InfiModel();
        LobjTransactionDetails = new List<TransactionDetails>();

        LobjTransactionDetails = lobjInfiModel.GetMemberAccrualTransaction(objSearchTransactions);

        if (LobjTransactionDetails != null && LobjTransactionDetails.Count > 0)
        {
            divOldTrnx.Attributes.Add("style", "display:block");
            LobjTransactionDetails = LobjTransactionDetails.FindAll(lobj => lobj.LoyaltyTxnType == LoyaltyTxnType.Spend && lobj.TransactionType == TransactionType.Credit);
            if (LobjTransactionDetails != null && LobjTransactionDetails.Count > 0)
            {
                if (LobjTransactionDetails.Count >= 10)
                    LobjTransactionDetails = LobjTransactionDetails.OrderByDescending(lobj => lobj.ProcessingDate).Take(10).ToList();

                RepAccrualSummaryInfo.DataSource = LobjTransactionDetails;
            }
            else
            {
                RepAccrualSummaryInfo.DataSource = null;
            }
        }
        else
        {
            RepAccrualSummaryInfo.DataSource = null;
        }
        RepAccrualSummaryInfo.DataBind();
        if (LobjTransactionDetails.Count > 0)
        {
            if (CurrentPage == 1)
            {
                linkbuttonPrevious.Enabled = false;
                linkbuttonNext.Enabled = true;
                linkbuttonFirst.Enabled = false;
                linkbuttonLast.Enabled = true;
            }
            if (Convert.ToInt32(LobjTotalpage.Count) == 1)
            {
                linkbuttonPrevious.Enabled = false;
                linkbuttonNext.Enabled = false;
                linkbuttonFirst.Enabled = false;
                linkbuttonLast.Enabled = false;
            }
            else if (CurrentPage > 1 && CurrentPage < Convert.ToInt32(LobjTotalpage.Count))
            {
                linkbuttonPrevious.Enabled = true;
                linkbuttonNext.Enabled = true;
                linkbuttonFirst.Enabled = true;
                linkbuttonLast.Enabled = true;
            }
            else if (CurrentPage == Convert.ToInt32(LobjTotalpage.Count))
            {
                linkbuttonPrevious.Enabled = true;
                linkbuttonNext.Enabled = false;
                linkbuttonFirst.Enabled = true;
                linkbuttonLast.Enabled = false;
            }
            doPaging();
        }
        else
        {
            linkbuttonPrevious.Visible = false;
            linkbuttonNext.Visible = false;
            linkbuttonFirst.Visible = false;
            linkbuttonLast.Visible = false;
        }
    }

    public List<string> BindDataRangeFilter()
    {

        MemberDetails objMemberDetails = new MemberDetails();
        objMemberDetails = Session["MemberDetails"] as MemberDetails;
        int TotalRowCount = 10;
        SearchTransactions objSearchTransactions = new SearchTransactions();
        objSearchTransactions.RelationReference = ((Session["MemberDetails"] as MemberDetails).MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
        InfiModel lobjInfiModel = new InfiModel();
        LobjTransactionDetails = new List<TransactionDetails>();
        ViewState["TotalPages"] = TotalRowCount;
        List<string> lobjstringList = new List<string>();
        lobjstringList = lobjInfiModel.PageDataRangeList(TotalRowCount, TotalPagecount);
        LobjTotalpage = lobjstringList;
        if (lobjstringList.Count > 0)
        {
            ddlvalue = lobjstringList[0];
        }
        return lobjstringList;
    }

    public void doPaging()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ID");
        dt.Columns.Add("PageIndex");

        fistIndex = CurrentPage - 5;
        if (CurrentPage > 5)
        {
            lastIndex = CurrentPage + 5;
        }
        else
        {
            lastIndex = 10;
        }
        if (lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
        {
            lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
            fistIndex = lastIndex - 10;
        }

        if (fistIndex < 0)
        {
            fistIndex = 0;
        }
        for (int i = fistIndex; i < lastIndex; i++)
        {
            if (LobjTotalpage.Count == i)
            {
                break;
            }
            else
            {
                dt.Rows.Add((i + 1).ToString(), LobjTotalpage[i]);
            }
        }
        this.dlPaging.DataSource = dt;
        this.dlPaging.DataBind();
    }

    protected void dlPaging_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName.Equals("Paging"))
        {
            LobjTotalpage = BindDataRangeFilter();
            LinkButton objlnkbtnPaging = (LinkButton)e.Item.FindControl("lnkbtnPaging");
            ddlvalue = e.CommandArgument.ToString();
            CurrentPage = Convert.ToInt16(objlnkbtnPaging.Text);
            this.BindAccrualSummaryInfo();
        }
    }
    protected void dlPaging_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        LinkButton lnkbtnPage = (LinkButton)e.Item.FindControl("lnkbtnPaging");
        if (lnkbtnPage.Text.ToString() == CurrentPage.ToString())
        {
            lnkbtnPage.Enabled = false;
            lnkbtnPage.Style.Add("fone-size", "14px");
            lnkbtnPage.Font.Bold = true;

        }
    }

    protected void lbtnFirst_Click(object sender, EventArgs e)
    {
        LobjTotalpage = BindDataRangeFilter();
        CurrentPage = 1;
        if (LobjTotalpage.Count > 0)
        {
            ddlvalue = LobjTotalpage[CurrentPage - 1];
        }
        else
        {
            ddlvalue = "0-0";
        }
        this.BindAccrualSummaryInfo();
    }

    protected void lbtnLast_Click(object sender, EventArgs e)
    {
        //CurrentPage = (Convert.ToInt32(ViewState["TotalPages"]));
        LobjTotalpage = BindDataRangeFilter();
        if (LobjTotalpage.Count > 0)
        {
            ddlvalue = LobjTotalpage[LobjTotalpage.Count - 1];
            CurrentPage = LobjTotalpage.Count;
        }
        else
        {
            ddlvalue = "0-0";
        }
        this.BindAccrualSummaryInfo();

    }

    protected void lbtnNext_Click(object sender, EventArgs e)
    {
        LobjTotalpage = BindDataRangeFilter();
        CurrentPage += 1;
        if (LobjTotalpage.Count > 0)
        {
            ddlvalue = LobjTotalpage[CurrentPage - 1];
        }
        else
        {
            ddlvalue = "0-0";
        }
        this.BindAccrualSummaryInfo();
    }
    protected void lbtnPrevious_Click(object sender, EventArgs e)
    {
        LobjTotalpage = BindDataRangeFilter();
        CurrentPage -= 1;
        if (LobjTotalpage.Count > 0)
        {
            ddlvalue = LobjTotalpage[CurrentPage - 1];
        }
        else
        {
            ddlvalue = "0-0";
        }
        this.BindAccrualSummaryInfo();
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string RedeemPoints(string pstrPoints, string pstrAmount)
    {
        string lstrMsg = string.Empty;
        if (HttpContext.Current.Session["MemberDetails"] != null)
        {
            string lstrRequiredPoints = Convert.ToString(HttpContext.Current.Session["RequiredPoints"]);
            float lfltRequiredAmount = Convert.ToSingle(HttpContext.Current.Session["RequiredAmount"]);
            if (lstrRequiredPoints.Equals(pstrPoints))
            {
                MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
                InfiModel lobjInfiModel = new InfiModel();

                string lstrReference = string.Empty;

                int lintPoints = Convert.ToInt32(pstrPoints);
                float lftAmount = Convert.ToSingle(pstrAmount);
                int lintAvailablePoints = lobjInfiModel.CheckAvailbility(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), ConfigurationManager.AppSettings["PayCurrency"]);
                LoggingAdapter.WriteLog("Check Avail = " + lintAvailablePoints + " | " + lintPoints);
                if (lintAvailablePoints >= lintPoints)
                {
                    lstrReference = lobjInfiModel.RedeemPoints(lftAmount, lintPoints, lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference,
                                              lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, "PayByPoints", Convert.ToInt32(LoyaltyTxnType.Merchant), ConfigurationManager.AppSettings["PayCurrency"]);
                    LoggingAdapter.WriteLog("WORLDREWARDZ RedeemPoints = " + lstrReference);
                    if (!string.IsNullOrEmpty(lstrReference))
                    {
                        lstrMsg = "Success";
                    }
                    else
                    {
                        lstrMsg = "TransactionFailed";
                    }
                }
                else
                {
                    lstrMsg = "InsufficientPoints";
                }
            }
            else
            {
                lstrMsg = "TransactionFailed";
            }
        }
        return lstrMsg;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string NewTransaction(string pstrPoints, string pstrAmount)
    {
        int lintPoints = Convert.ToInt32(HttpContext.Current.Session["RequiredPoints"]);
        float lfltAmount = Convert.ToSingle(HttpContext.Current.Session["RequiredAmount"]);
        string lstrMsg = string.Empty;
        if (HttpContext.Current.Session["MemberDetails"] != null)
        {
            MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
            InfiModel lobjCBQModel = new InfiModel();

            string lstrReference = GenerateUniqueNumber.Get12DigitNumberDateTime();

            lstrMsg = lstrReference;
        }


        return lstrMsg;
    }


    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string GetCalculatedPoints(string pstrAmount)
    {
        InfiModel lobjInfiModel = new InfiModel();
        string lstrPoints = "0";
        if (HttpContext.Current.Session["MemberDetails"] != null)
        {
            MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
            if (!string.IsNullOrEmpty(pstrAmount))
            {
                double ldblntAmount = Convert.ToDouble(pstrAmount);
                List<ProgramCurrencyDefinition> lobjlistProgramCurrencyDefinition = lobjInfiModel.GetProductProgramCurrencyDefinition(lobjMemberDetails);
                if (lobjlistProgramCurrencyDefinition != null && lobjlistProgramCurrencyDefinition.Count > 0)
                {
                    ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramMaster();

                    int lintPointsRequired = Convert.ToInt32(Math.Round(ldblntAmount / lobjlistProgramCurrencyDefinition[0].RedemptionRate));
                    lstrPoints = lintPointsRequired.ToString();
                }
            }
        }

        HttpContext.Current.Session["RequiredPoints"] = lstrPoints;
        HttpContext.Current.Session["RequiredAmount"] = pstrAmount;
        return lstrPoints;
    }
}