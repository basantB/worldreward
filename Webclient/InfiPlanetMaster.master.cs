﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Member.Entites;
using InfiPlanetModel.Model;
using Core.Platform.MemberActivity.Entities;

public partial class InfiPlanetMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            List<MemberSubRelation> listMemberSubRelation = new List<MemberSubRelation>();

            //if (HttpContext.Current.Session["MemberDetails"] == null)
            //{
            //    MemberDetails lobjMemberDetails1 = new MemberDetails();
            //    lobjMemberDetails1.Email = "santosh.upadhyay@commercebay.com";
            //    lobjMemberDetails1.LastName = "santosh";
            //    MemberRelation lobjMemberRelation = new MemberRelation();
            //    lobjMemberRelation.RelationReference = "123456789";
            //    lobjMemberRelation.WebPassword = "SnNLSYznq8FTtz7UyiE5XJzPw0h2hVBKQOAwAFHKh2Kd5hs28/rAZNvJNyKeHyeqB103h9BWkheYjmUdGRqoYQSGR2LtFtP70NCTND9jZzXEGu8eoY3eOgStKx/W+SNtZgaVuZmDS/TLDtExikikadRLbmbu/9eUeezlXdgsXn1wb+5qOpx/rBh9MY6o9FAbwPosbvXp1xqIXP10G4TOWQ4SypWM7DBbItxSmYyj9A+oqITyt6bDymQmbLSeObXVZBjNM4yTRVQzhTWYIjqgpVBQeftzw7sJXtKJR6fpRQeUDKmZQw0IJ3kXQ2cuO17fpojHhe8Ny1/Zz8TXq7ymaA==";
            //    lobjMemberRelation.RelationType = RelationType.LBMS;
            //    MemberSubRelation lobjMemberSubRelation1 = new MemberSubRelation();
            //    lobjMemberSubRelation1.ProductCode = "DINERS CHARGE";
            //    listMemberSubRelation.Add(lobjMemberSubRelation1);
            //    MemberSubRelation lobjMemberSubRelation2 = new MemberSubRelation();
            //    lobjMemberSubRelation2.ProductCode = "WOW";
            //    listMemberSubRelation.Add(lobjMemberSubRelation2);
            //    lobjMemberRelation.MemberSubRelationList = listMemberSubRelation;
            //    List<MemberRelation> listofMemberRelation = new List<MemberRelation>();
            //    listofMemberRelation.Add(lobjMemberRelation);
            //    lobjMemberDetails1.MemberRelationsList = listofMemberRelation;
            //    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails1;
            //}
            if (HttpContext.Current.Session["MemberDetails"] != null)
            {
                lnkLogin.Visible = false;
                lnkRegister.Visible = false;
                liUserName.Visible = true;
                MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
                lblMemberName.Text = "Welcome " + lobjMemberDetails.LastName;
                liLogout.Visible = true;
            }
        }
    }
    protected void SignOut(object sender, EventArgs e)
    {
        InfiModel objInfiModel = new InfiModel();
        objInfiModel.LogActivity(string.Format("Member Logout"), ActivityType.Logout);
        Session.Abandon();
        Response.Redirect("Index.aspx");

    }
}
