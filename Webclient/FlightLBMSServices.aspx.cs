﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfiPlanetModel.Model;
using CB.IBE.Platform.Entities;
using Core.Platform.Member.Entites;
using Core.Platform.Master.Entities;
using CB.IBE.Platform.ClientEntities;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;
using Framework.EnterpriseLibrary.Adapters;
using System.IO;
using CB.IBE.Platform.Hotels.ClientEntities;
//using Framework.Integrations.Hotels.Entities;

public partial class FlightLBMSServices : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            InfiModel lobjModel = new InfiModel();
            string lstrRelativePath = this.Page.AppRelativeVirtualPath;
            int lintActivityId = lobjModel.LogActivity(lstrRelativePath.Replace("~", string.Empty).Replace("/", string.Empty), ActivityType.PageLoad);

        }
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAirfields(string prefixText)
    {
        //string prefixText = "Mum";
        InfiModel lobjInfiModel = new InfiModel();
        prefixText = prefixText.ToLower();
        List<AirField> lobjAirfieldList = HttpContext.Current.Application["AllAirfields"] as List<AirField>;
        List<string> lobjRetValue = new List<string>();
        List<string> lobjDubai = new List<string>();
        List<string> lobjLondon = new List<string>();
        List<string> lobjROW = new List<string>();

        lobjAirfieldList.ForEach(airfield =>
        {
            if (airfield.City.Equals("Dubai") && airfield.IATACode.ToLower().IndexOf(prefixText) >= 0 && airfield.IsActive)
            {
                lobjRetValue.Add(airfield.SearchAirfieldDetails);
            }
            else if (airfield.City.Equals("London") && airfield.IATACode.ToLower().IndexOf(prefixText) >= 0 && airfield.IsActive)
            {
                lobjRetValue.Add(airfield.SearchAirfieldDetails);
            }
            else if (airfield.City.Equals("Dubai") && airfield.SearchAirfieldDetails.ToLower().IndexOf(prefixText) >= 0 && airfield.IsActive)
            {
                if (airfield.SearchAirfieldDetails == "DXB, Dubai International, Dubai, UNITED ARAB EMIRATES")
                {
                    lobjDubai.Insert(0, airfield.SearchAirfieldDetails);
                }
                else if (airfield.SearchAirfieldDetails == "DWC, Dubai Al Maktoum International Airport, Dubai, UNITED ARAB EMIRATES")
                {
                    lobjDubai.Insert(1, airfield.SearchAirfieldDetails);
                }
                else
                {
                    lobjDubai.Add(airfield.SearchAirfieldDetails);
                }
            }
            else if (airfield.City.Equals("London") && airfield.SearchAirfieldDetails.ToLower().IndexOf(prefixText) >= 0 && airfield.IsActive)
            {

                if (airfield.SearchAirfieldDetails == "LHR, Heathrow International, London, UNITED KINGDOM")
                {
                    lobjLondon.Insert(0, airfield.SearchAirfieldDetails);
                }
                else
                {
                    lobjLondon.Add(airfield.SearchAirfieldDetails);
                }
            }
            else if (airfield.SearchAirfieldDetails.ToLower().IndexOf(prefixText) >= 0 && airfield.IsActive)
            {
                lobjROW.Add(airfield.SearchAirfieldDetails);
            }


        });

        lobjRetValue.AddRange(lobjDubai);
        lobjRetValue.AddRange(lobjLondon);
        lobjRetValue.AddRange(lobjROW);
        return lobjRetValue;
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCarriers(string prefixText)
    {
        //string prefixText = "Mum";
        InfiModel lobjInfiModel = new InfiModel();
        prefixText = prefixText.ToLower();
        List<Carrier> lobjCarrierList = HttpContext.Current.Application["CarrierList"] as List<Carrier>;
        List<string> lobjRetValue = new List<string>();
        if (lobjCarrierList != null && lobjCarrierList.Count > 0)
        {
            lobjCarrierList.ForEach(Carrierfield =>
            {
                if (Carrierfield.CarrierName.ToLower().IndexOf(prefixText) >= 0)
                {
                    lobjRetValue.Add(string.Format("{0}-{1}", Carrierfield.CarrierName, Carrierfield.CarrierCode));
                }
            });
        }
        return lobjRetValue;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAllHotelCities(string prefixText)
    {
        List<string> lobjListOfCity = HttpContext.Current.Application["HotelCities"] as List<string>;
        prefixText = prefixText.ToLower();
        List<string> lobjRetValue = new List<string>();
        lobjListOfCity.ForEach(x => { if (x.ToLower().IndexOf(prefixText) >= 0) { lobjRetValue.Add(x); } });
        return lobjRetValue;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool BookNowClick(string pstrSequenceNo)
    {
        InfiModel lobjInfiModel = new InfiModel();
        lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlightClick, pstrSequenceNo), ActivityType.FlightBooking);
        SearchResponse lobjSearchResponse = HttpContext.Current.Session["Flights"] as SearchResponse;
        ItineraryDetails lobjSelectedFlights = lobjSearchResponse.ItineraryDetailsList.Find(lobj => lobj.SequenceNo.Equals(Convert.ToInt32(pstrSequenceNo)));
        HttpContext.Current.Session["SelectedItinerary"] = lobjSelectedFlights;
        if (HttpContext.Current.Session["MemberDetails"] == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool BookNowClickDomestic(string pstrOnwardSequenceNo, string pstrReturnSequnceNo)
    {
        InfiModel lobjInfiModel = new InfiModel();
        lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlightClick, pstrOnwardSequenceNo + "/" + pstrReturnSequnceNo), ActivityType.FlightBooking);
        SearchResponse lobjSearchResponse = HttpContext.Current.Session["Flights"] as SearchResponse;
        ItineraryDetails lobjSelectedFlights = new ItineraryDetails();
        FlightDetails lobjOnwardSelectedFlights = lobjSearchResponse.OnwardFlightList.Find(lobjOnward => lobjOnward.SequenceNo.Equals(Convert.ToInt32(pstrOnwardSequenceNo)));
        FlightDetails lobjReturnelectedFlights = lobjSearchResponse.ReturnFlightList.Find(lobjReturn => lobjReturn.SequenceNo.Equals(Convert.ToInt32(pstrReturnSequnceNo)));
        if (lobjOnwardSelectedFlights != null)
        {
            HttpContext.Current.Session["DomesticOnwardFlights"] = lobjOnwardSelectedFlights;
        }
        if (lobjReturnelectedFlights != null)
        {
            HttpContext.Current.Session["DomesticReturnFlights"] = lobjReturnelectedFlights;
        }

        if (lobjOnwardSelectedFlights != null)
        {
            lobjSelectedFlights.ListOfFlightDetails.Add(lobjOnwardSelectedFlights);

            if (lobjReturnelectedFlights != null)
            {
                lobjSelectedFlights.ListOfFlightDetails.Add(lobjReturnelectedFlights);
            }
            lobjSelectedFlights.OriginLocation = lobjOnwardSelectedFlights.ListOfFlightSegments[0].OriginLocation;
            lobjSelectedFlights.DestinationLocation = lobjOnwardSelectedFlights.ListOfFlightSegments[lobjOnwardSelectedFlights.ListOfFlightSegments.Count - 1].DestinationLocation;
            lobjSelectedFlights.DepartureDate = lobjOnwardSelectedFlights.ListOfFlightSegments[0].DepartureDate;
            lobjSelectedFlights.ArrivalDate = lobjReturnelectedFlights.ListOfFlightSegments[0].DepartureDate;
            lobjSelectedFlights.FareKey = lobjOnwardSelectedFlights.PaxPricingInfoList.PaxPricingInfo[0].PricingInfoList.PricingInfo[0].farekey;
            lobjSelectedFlights.Type = lobjOnwardSelectedFlights.Type;
        }

        if (lobjSelectedFlights != null)
        {
            HttpContext.Current.Session["SelectedItinerary"] = lobjSelectedFlights;
        }

        if (HttpContext.Current.Session["MemberDetails"] == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool BookNowClickDomesticOneWay(string pstrSequenceNo)
    {
        InfiModel lobjInfiModel = new InfiModel();
        lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlightClick, pstrSequenceNo), ActivityType.FlightBooking);
        SearchResponse lobjSearchResponse = HttpContext.Current.Session["Flights"] as SearchResponse;
        //ItineraryDetails lobjSelectedFlights = lobjSearchResponse.ItineraryDetailsList.Find(lobj => lobj.SequenceNo.Equals(Convert.ToInt32(pstrSequenceNo)));

        FlightDetails lobjOnwardSelectedFlights = lobjSearchResponse.OnwardFlightList.Find(lobjOnward => lobjOnward.SequenceNo.Equals(Convert.ToInt32(pstrSequenceNo)));

        ItineraryDetails lobjSelectedFlights = new ItineraryDetails();
        if (lobjOnwardSelectedFlights != null)
        {
            lobjSelectedFlights.ListOfFlightDetails.Add(lobjOnwardSelectedFlights);
            lobjSelectedFlights.OriginLocation = lobjOnwardSelectedFlights.ListOfFlightSegments[0].OriginLocation;
            lobjSelectedFlights.DestinationLocation = lobjOnwardSelectedFlights.ListOfFlightSegments[lobjOnwardSelectedFlights.ListOfFlightSegments.Count - 1].DestinationLocation;
            lobjSelectedFlights.DepartureDate = lobjOnwardSelectedFlights.ListOfFlightSegments[0].DepartureDate;
            lobjSelectedFlights.FareKey = lobjOnwardSelectedFlights.PaxPricingInfoList.PaxPricingInfo[0].PricingInfoList.PricingInfo[0].farekey;
            lobjSelectedFlights.Type = lobjOnwardSelectedFlights.Type;
        }
        if (lobjOnwardSelectedFlights != null)
        {
            HttpContext.Current.Session["DomesticOnwardFlights"] = lobjOnwardSelectedFlights;
        }
        //ItineraryDetails lobjSelectedFlights = lobjSearchResponse.ItineraryDetailsList.Find(lobj => lobj.SequenceNo.Equals(Convert.ToInt32(pstrSequenceNo)));
        if (lobjSelectedFlights != null)
        {
            HttpContext.Current.Session["SelectedItinerary"] = lobjSelectedFlights;
        }

        if (HttpContext.Current.Session["MemberDetails"] == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}