﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="ViewProductInfo.aspx.cs" Inherits="ViewProductInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <%--<link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css" />--%>
    <link href="CSS/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <link href="css/tour.css" rel="stylesheet" type="text/css" />
    <%--<script src="js/jquery.ui.core.js" type="text/javascript"></script>--%>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/JTemplateCreator.js" type="text/javascript"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="js/jquery.mCustomScrollbar.css" type="text/css" />
    <script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>--%>
    <script type="text/javascript" src="js/ProductInfo.js"></script>
<script>
$(document).ready(function(){
	//toggle the componenet with class accordion_body
	$(".accordion_head").click(function(){
		if ($('.accordion_body').is(':visible')) {
			$(".accordion_body").slideUp(300);
			$(".plusminus").text('+');
		}
		$(this).next(".accordion_body").slideDown(300); 
		$(this).children(".plusminus").text('-');
	});
});
</script>

    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>
    <div class="pgcontent tourSection">
    <div class="inr-wrap mtop20 vator">
        <div class="prpg-gall">
            <h1 class="totpoint">WorldRewardZ :<span class="milesPrice" style="font-size: 14px;font-weight: normal;">From</span>
                <asp:Label ID="lblPoints" runat="server" CssClass="pointmiles milesPrice" Text=""></asp:Label></h1>
            <hr />
            <h3 class="vatrhdr">Check Availability
            </h3>
            <p>
                <span id="Span2"></span>
            </p>
            <div class="clr">
            </div>
            <div>
                <%--<div class="subHeading">
                    Enter Total Numbers of Travelers
                </div>--%>
                <ul class="ultraveler">
                    <li class="seltrvdt">
                        <label>
                            Select Travel Date
                        </label>
                        <asp:TextBox ID="txtBookingDate" runat="server"></asp:TextBox>
                    </li>
                    <div class="clr">
                    </div>
                    <li>
                        <div id="divAdult" style="display: none;">
                            <label>
                                Adult</label>
                            <label id="ddlAdultVal" class="f10"></label>
                            <asp:DropDownList ID="ddlAdult" runat="server">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </li>

                    <li>
                        <div id="divChild" style="display: none;">
                            <label>
                                Child</label>
                            <label id="ddlChildVal" class="f10"></label>
                            <asp:DropDownList ID="ddlChild" runat="server">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </li>
                    <li>
                        <div id="divSenior" style="display: none;">
                            <label>
                                Senior
                            </label>
                            <label id="ddlSeniorVal" class="f10"></label>
                            <asp:DropDownList ID="ddlSenior" runat="server">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </li>
                    <li>
                        <div id="divInfant" style="display: none;">
                            <label>
                                Infant</label>
                            <label id="ddlInfantVal" class="f10"></label>
                            <asp:DropDownList ID="ddlInfant" runat="server">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </li>
                    <div class="clr">
                    </div>
                </ul>
            </div>
            <input type="button" class="btnRed" value="Check Availability" onclick="CheckProductAvailabilityRequest();" />
            <br />
            <div id="divProducNotAvailable" style="color: red; width: 100%; margin: 5px 2px 0px 2px; font-size: 16px; display: none;">
            </div>
        </div>
        <div class="prpg-dtls">
            <div class="hd-col3 bdr-right">
            <ul class="bxslider" id="UserPhotos">
            </ul>
             <h1><span id="ShortTitle"></span></h1>
             <span id="Location"></span>
             <asp:Label ID="lblRating" runat="server" CssClass="stars" Text=""></asp:Label>
             <span id="Duration" class="Duration"></span>
            <%--<div id="bx-pager">
            </div>--%>

            <!---Accordoan-->
             <div class="accordion_container">
		     <ul>
        	<li>
            <div class="accordion_head">About Tour<span class="plusminus">-</span></div>
			<div class="accordion_body" style="display: block;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
			</div>
            </div></li>
            
        	<li>
            <div class="accordion_head">Overview<span class="plusminus">+</span></div>
			<div class="accordion_body" style="display: none;">
			<div class="description" id="" style="overflow: hidden;" tabindex="2">
            <div class="about-hotel" id="Divoverview">
                    <ul>
                        <li>
                            <div id="Itinerary">
                            </div>
                        </li>
                        <li>
                            <div id="InclusionsList">
                            </div>
                        </li>
                        <li>
                            <div id="ExclusionsList">
                            </div>
                        </li>
                        <li>
                            <div id="DeparturePoint">
                            </div>
                        </li>
                        <li>
                            <div id="ReturnDetails">
                            </div>
                        </li>
                    </ul>
                </div>
			</div>
			</div>
            </li>
		<li>
		<div class="accordion_head">Important Info<span class="plusminus">+</span></div>
			<div class="accordion_body" style="display: none;">
			<div class="description" id="" style="overflow: hidden;" tabindex="2">
            <div class="about-hotel" id="DivImportant">
                    <ul>
                        <li>
                            <div id="ShortDescription">
                            </div>
                        </li>
                        <li>
                            <div id="AdditionalInfo">
                            </div>
                        </li>
                        <li>
                            <div id="SalesPointsList">
                            </div>
                        </li>
                    </ul>
                </div>
			</div>
			</div>
            </li>
		</ul>		
	</div>
            <!--End of Accordion-->

            <%--<div class="accordion_container">
            <div id="discription">
            </div>
            <div id="taboverview" onclick="return showtourInfo('overview','Important');">
             <div id="Itinerary">
                </div>
                <div class="description">
                     <div class="about-hotel" id="Divoverview">
                    <ul>
                        <li>
                            <div id="InclusionsList">
                            </div>
                        </li>
                        <li>
                            <div id="ExclusionsList">
                            </div>
                        </li>
                        <li>
                            <div id="DeparturePoint">
                            </div>
                        </li>
                        <li>
                            <div id="ReturnDetails">
                            </div>
                        </li>
                    </ul>
                </div>
                </div>
             </div>
            </div>--%>
            </div>
        </div>
        <%--<div class="hd-col2">
            <div class="hotelinfo">
                <div class="clr">
                </div>
                <hr />
                <div class="vtrdtstbs">
                    <div id="taboverview" class="dtstbs select" onclick="return showtourInfo('overview','Important');">
                        Overview
                    </div>
                    <div id="tabImportant" class="dtstbs" onclick="return showtourInfo('Important','overview');">
                        Important Info
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <!--About Hotel Starts-->
                <div class="about-hotel" id="Divoverview">
                    <ul>
                        <li>
                            <div id="Itinerary">
                            </div>
                        </li>
                        <li>
                            <div id="InclusionsList">
                            </div>
                        </li>
                        <li>
                            <div id="ExclusionsList">
                            </div>
                        </li>
                        <li>
                            <div id="DeparturePoint">
                            </div>
                        </li>
                        <li>
                            <div id="ReturnDetails">
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="about-hotel" id="DivImportant" style="display: none">
                    <ul>
                        <li>
                            <div id="ShortDescription">
                            </div>
                        </li>
                        <li>
                            <div id="AdditionalInfo">
                            </div>
                        </li>
                        <li>
                            <div id="SalesPointsList">
                            </div>
                        </li>
                    </ul>
                </div>
                <!--About Hotel Ends-->
                <div class="clr">
                </div>
            </div>
        </div>--%>
        
        
        <div class="clr">
        </div>
        </div>
        <!--Hotel Info Ends-->
        <div class="clr">
        </div>
    </div>
    </div>
    <style>
        span.stars, span.stars span {
            display: block;
            background: url(images/stars.png) 0 -16px repeat-x;
            width: 80px;
            clear: both;
            height: 16px; /*margin: 4px 0;*/
        }

            span.stars span {
                background-position: 0 0;
            }
    </style>
</asp:Content>
