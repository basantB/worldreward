﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CashBackSummary.aspx.cs"
    Inherits="CashBackSummary" MasterPageFile="~/InfiPlanetMaster.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <div class="LoginOuter">
        <div class="common_div ">
            <div style="height: 290px; float: left; width: 100%; margin-top: 50px; font-size: 14px;">
                <div runat="server" id="divCashBackSuccess" style="border: none; text-align: left;
                    float: left; width: 100%; display: block;">
                    <br />
                    <asp:Label ID="lblSuccess" runat="server" Text="Your CashBack redemption request has been processed successfully and the amount will be credited to your credit card account within the next two working days. A confirmation has also been sent to your registered email address."
                        Visible="true" />
                    <asp:Label ID="lblFail" runat="server" Text="Your account balance is insufficient for the request.Please select a lower amount."
                        Visible="false" />
                    <br />
                    <br />
                    <br />
                    <asp:Label ID="lblPoints" runat="server" Text="" />
                    <br />
                    <br />
                    <br />
                    <asp:Label ID="lblAmount" runat="server" Text="" />
                    <br />
                    <br />
                    <br />
                </div>
            </div>
            <div style="float: right; width: 10%; text-align: right">
                <a href="Index.aspx" class="btnhome" id="btnBack">&nbsp;</a>
            </div>
        </div>
    </div>
</asp:Content>
