﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="StatementSummary.aspx.cs" Inherits="StatementSummary" %>

<%@ Register Src="~/UserControl/ActivationMenu.ascx" TagName="ActivationMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <style>
        ul .arrowlist3 {
            list-style-type: circle;
            margin-left: 2.5%;
        }
    </style>
    <link href="css/myAccount.css" rel="stylesheet" />
    <script>

        $(document).ready(function () {
            var strCurrency = $("#CP_lblCurrencyName").text();
            $("#CP_lblCurrencyName").text(strCurrency.replace('REWARDZ', 'WorldRewardZ'));
        });
    </script>

    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>

    <article id="article">
        <div class="wrap">
            <h1>Experience Abundance with WorldRewardZ</h1>
            <p>Below is a categorized listing of your activities at WorldRewardZ. As you glance through them, you can immerse in the joy of having the best choices in the world available to you simply at a click, with WorldRewardZ. Go ahead and multiply your unique experiences.</p>
            <div class="ManyWrap MyAccount">
                <uc1:ActivationMenu ID="ActivationMenu1" runat="server" />
                <div class="InnCont">
                     <div id="divCurrency" class="myacc-milesdiv" runat="server">
                    Select Currency :
                    <asp:DropDownList ID="ddlProgramCurrency" runat="server" AppendDataBoundItems="true"
                    OnSelectedIndexChanged="ddlProgramCurrency_SelectedIndexChanged" AutoPostBack="true"
                    Style="padding-top: 0.1vw; padding-bottom: 0.1vw; margin-top: 1vw; margin-bottom: 0.7vw;
                    color: #bbbcbe">
                </asp:DropDownList>
                </div>
				<table cellspacing="0" cellpadding="0" border="0">
                <thead>
                    <tr>
                      <th colspan="2" class="txtLeft">Welcome, <asp:Label runat="server" ID="lblUserName" Text="">
                        </asp:Label> <label class="logindtl">Last Login: <asp:Label runat="server" ID="lblLastLogin" Text=""></asp:Label></th>
                       <th class="txtRight"> 
                        <div class="myacc-user" Id="DivPool" runat="server">
                        <asp:DropDownList ID="ddlPoolingDetails" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlPoolingDetails_SelectedIndexChanged" AutoPostBack="true" />
                        </div>
                    <div class="myacc-milesdiv">
                        You have
                         <asp:Label ID="lblPoints" runat="server"></asp:Label>
                         <asp:Label ID="lblCurrencyName" runat="server"></asp:Label>
                         <asp:Label ID="lblSecondPoints" runat="server"></asp:Label>
                         <asp:Label ID="lblSecondCurrencyName" runat="server"></asp:Label>
                          </div>
                       </th>
                    </tr>
                </thead>
        
            <tbody>
                <tr>
                    <td rowspan="4">Earn WorldRewardZ</td>
                  <td>Bonus WorldRewardZ<br></td>
                    <td class="txtRight"><asp:Label ID="lblBonusmile" runat="server" Text="0"></asp:Label><br></td>
                </tr>
                <tr>
                  <td>Spend WorldRewardZ</td>
                  <td class="txtRight"><asp:Label ID="lblSpendmile" runat="server" Text="0"></asp:Label></td>
                </tr>
                <tr>
                  <td>Purchase WorldRewardZ</td>
                  <td class="txtRight"><asp:Label ID="lblPurchasemile" runat="server" Text="0"></asp:Label></td>
                </tr>
                <tr>
                  <td>Partner WorldRewardZ</td>
                  <td class="txtRight"><asp:Label ID="lblPartnermile" runat="server" Text="0"></asp:Label></td>
                </tr>
                <tr>
                  <td>Redeemed WorldRewardZ</td>
                  <td>&nbsp;</td>
                  <td class="txtRight"><asp:Label ID="lblRedeemedmile" runat="server" Text="0"></asp:Label></td>
                </tr>
                <tr>
                  <th class="txtLeft">Closing WorldRewardZ</th>
                  <th>&nbsp;</th>
                  <th class="txtRight"><asp:Label ID="lblClosingmile" runat="server" Text="0"></asp:Label></th>
                </tr>
                 <tr>
                  <th class="txtLeft">Earn LOAN</th>
                  <th>Bonus LOAN </th>
                  <th class="txtRight"><asp:Label ID="lblLoanBonus" runat="server" Text="0"></asp:Label></th>
                </tr>
                <tr>
                  <th class="txtLeft">Redemption LOAN</th>
                  <th>&nbsp;</th>
                  <th class="txtRight"><asp:Label ID="lblLoanRedemption" runat="server" Text="0"></asp:Label></th>
                </tr>
                 <tr>
                  <th class="txtLeft">Closing LOAN</th>
                  <th>&nbsp;</th>
                  <th class="txtRight"><asp:Label ID="lblLoanClosing" runat="server" Text="0"></asp:Label></th>
                </tr>

                <tr>
                  <th class="txtLeft">Final Closing WorldRewardZ</th>
                  <th>&nbsp;</th>
                  <th class="txtRight"><asp:Label ID="lblFinalPoints" runat="server" Text="0"></asp:Label></th>
                </tr>
            </tbody>
        </table>
				<ul class="list">
					<h2>YOUR STATEMENT EXPLAINED:</h2>
					<li>Bonus WorldRewardZ: The points given by the Bank for account activation.</li>
					<li>Spend WorldRewardZ: The points earned when using the WorldRewardZ debit/credit card.</li>
					<li>Partner WorldRewardZ: The add-on points given by the program partner for the transaction done at its outlets or online stores.</li>
					<li>Redeemed WorldRewardZ: The points utilized by the WorldRewardZ account holder.</li>
					<li>Closing WorldRewardZ: The points available in one's WorldRewardZ account.</li>
				</ul>
		</div>
             </div>
          </div>
        </article>
     <!--end of article-->

    <%--<div class="inr-wrap">
        <section class="myaccContent">
      <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
      <div class="myacc-conthdr" >
                <div class="myacc-user"><label class="username">Welcome</label>&nbsp;<span><strong><asp:Label runat="server" ID="lblUserName" Text="">
                        </asp:Label></strong></span>   <label class="logindtl">Last Login: <asp:Label runat="server" ID="lblLastLogin" Text=""></asp:Label></label>
                    <div class="clr"></div>
                </div>
          <div class="myacc-user" Id="DivPool" runat="server">
              <asp:DropDownList ID="ddlPoolingDetails" runat="server" AppendDataBoundItems="true"
                                        OnSelectedIndexChanged="ddlPoolingDetails_SelectedIndexChanged" AutoPostBack="true" />
          </div>
                <div class="myacc-milesdiv">
                    You have
                     <asp:Label ID="lblPoints" runat="server"></asp:Label>
                     <asp:Label ID="lblCurrencyName" runat="server"></asp:Label>
                     <asp:Label ID="lblSecondPoints" runat="server"></asp:Label>
                     <asp:Label ID="lblSecondCurrencyName" runat="server"></asp:Label>
                </div>
                <div id="divCurrency" class="myacc-milesdiv" runat="server">
                    Select Currency :
                    <asp:DropDownList ID="ddlProgramCurrency" runat="server" AppendDataBoundItems="true"
                    OnSelectedIndexChanged="ddlProgramCurrency_SelectedIndexChanged" AutoPostBack="true"
                    Style="padding-top: 0.1vw; padding-bottom: 0.1vw; margin-top: 1vw; margin-bottom: 0.7vw;
                    color: #bbbcbe">
                </asp:DropDownList>
                </div>
                   <div class="sumry-tbl">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="td_bordrght vmid" rowspan="4"><div class="td-block-left">Earn WorldRewardZ</div></td>
                            <td class="row-earn"><div class="td-block-left">Bonus WorldRewardZ </div></td>
                            <td class="row-earn"><div class="td-block-rght"> <asp:Label ID="lblBonusmile" runat="server" Text="0"></asp:Label></div></td>
                        </tr>
                        <tr>
                            <td class="bord_top"><div class="td-block-left">Spend WorldRewardZ </div></td>
                            <td class="bord_top"><div class="td-block-rght"> <asp:Label ID="lblSpendmile" runat="server" Text="0"></asp:Label></div></td>
                        </tr>
                        <tr>
                            <td class="bord_top row-earn row-earn"><div class="td-block-left">Purchase WorldRewardZ </div></td>
                            <td class="bord_top row-earn row-earn"><div class="td-block-rght"> <asp:Label ID="lblPurchasemile" runat="server" Text="0"></asp:Label></div></td>
                        </tr>
                        <tr>
                            <td class="bord_top"><div class="td-block-left">Partner WorldRewardZ </div></td>
                            <td class="bord_top"><div class="td-block-rght"> <asp:Label ID="lblPartnermile" runat="server" Text="0"></asp:Label></div></td>
                        </tr>
                        <tr>
                            <td class="bord_top"><div class="td-block-left">Redemption WorldRewardZ</div></td>
                            <td class="bord_top">&nbsp;</td>
                            <td class="bord_top"><div class="td-block-rght"><asp:Label ID="lblRedeemedmile" runat="server" Text="0"></asp:Label></div></td>
                        </tr>
                        <tr class="row-closemiles">
                            <td class="bord_top"><div class="td-block-left txt-wht">Closing WorldRewardZ</div></td>
                            <td class="bord_top">&nbsp;</td>
                            <td class="bord_top"><div class="td-block-rght txt-wht"> <asp:Label ID="lblClosingmile" runat="server" Text="0"></asp:Label></div></td>
                        </tr>

<tr>
			   <td class="bord_top"><div class="td-block-left"></div></td>
                            <td class="bord_top">&nbsp;</td>
                            <td class="bord_top"><div class="td-block-rght"></div></td>
			</tr>
			<tr>
                            <td class="td_bordrght vmid" rowspan="1"><div class="td-block-left">Earn LOAN</div></td>
                            <td class="row-earn"><div class="td-block-left">Bonus LOAN </div></td>
                            <td class="row-earn"><div class="td-block-rght"> <asp:Label ID="lblLoanBonus" runat="server" Text="0"></asp:Label></div></td>
                        </tr>
                        
                        <tr>
                            <td class="bord_top"><div class="td-block-left">Redemption LOAN</div></td>
                            <td class="bord_top">&nbsp;</td>
                            <td class="bord_top"><div class="td-block-rght"><asp:Label ID="lblLoanRedemption" runat="server" Text="0"></asp:Label></div></td>
                        </tr>
                        <tr class="row-closemiles">
                            <td class="bord_top"><div class="td-block-left txt-wht">Closing LOAN</div></td>
                            <td class="bord_top">&nbsp;</td>
                            <td class="bord_top"><div class="td-block-rght txt-wht"> <asp:Label ID="lblLoanClosing" runat="server" Text="0"></asp:Label></div></td>
                        </tr>
                        <tr>
			   <td class="bord_top"><div class="td-block-left"></div></td>
                            <td class="bord_top">&nbsp;</td>
                            <td class="bord_top"><div class="td-block-rght"></div></td>
			</tr>
                        <tr class="row-closemiles">
                            <td class="bord_top"><div class="td-block-left txt-wht">Final Closing WorldRewardZ</div></td>
                            <td class="bord_top">&nbsp;</td>
                            <td class="bord_top"><div class="td-block-rght txt-wht"> <asp:Label ID="lblFinalPoints" runat="server" Text="0"></asp:Label></div></td>
                        </tr>
                    </table>

                </div>
          
                <!-- accordian start-->
                <div class="adCntnr">
                    <!-- blk start-->
                    <div class="acco2">
                        <div class="myacc-statmntdtl expand openAd"> Your Statement Explained</div>
                        <div class="summary-txt collapse">
                            <ul>
                                <li class="arrowlist3">Bonus WorldRewardZ are the points given by the Bank for account Activiation.</li>
                                <li class="arrowlist3">Spend WorldRewardZ are the points earned when using the WorldRewardZ
                                debit and credit card.</li>
                                <li class="arrowlist3">Partner WorldRewardZ are the add-on points given by the program partner
                                for the transactions done in its outlets or online stores.</li>
                                <li class="arrowlist3">Redeemed WorldRewardZ are the points utilized by the WorldRewardZ account
                                holder.</li>
                                <li class="arrowlist3">Closing WorldRewardZ are the points available in the WorldRewardZWorldRewardZ account.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- blk end-->
                </div>
                <!-- accordian end-->
            </div>
             <div class="clr"></div>
     </section>
        <div class="clr">
        </div>
    </div>--%>
</asp:Content>
