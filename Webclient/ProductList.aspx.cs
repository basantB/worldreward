﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using System.Configuration;
using System.IO;
using Core.Platform.ProgramMaster.Entities;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.MemberActivity.Constants;
using InfiPlanetModel.Model;
using Viator.Platform.ClientEntities;
using InfiPlanet.SessionConstants;

public partial class ProductList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            hfPageCount.Value = " ";
            hfPageCount.Value = "1";
            hfRecordCount.Value = ConfigurationManager.AppSettings["TourRecordCount"].ToString();
        }
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] GetProductList(string pstrTopX, string pstrStartDate, string pstrEndDate, string pstrCatId, string pstrSubCatId, string pstrIsDeals)
    {
        string lstrDestId = string.Empty;
        try
        {
            lstrDestId = Convert.ToString(HttpContext.Current.Session[SessionFields.SelectedTourDestinationId]);
            if (!string.IsNullOrEmpty(lstrDestId))
            {
                ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
                ViatorProductRequest lobjViatorProductRequest = new ViatorProductRequest();
                string[] lobjListOfData = new string[2];
                string lstrProductResult = string.Empty;
                string lstrTemplateResult = string.Empty;

                ViatorProductResponse lobjViatorProductResponse = new ViatorProductResponse();
                LoggingAdapter.WriteLog("GetProductList pram :-TopX: " + pstrTopX + ",StartDate: " + pstrStartDate + ",EndDate : " + pstrEndDate + " ,CategoryId :" + pstrCatId + ", SubCategoryId :" + pstrSubCatId + ", IsDealsOnly : " + pstrIsDeals);
                InfiModel lobjModel = new InfiModel();
                lobjProgramDefinition = lobjModel.GetProgramMaster();
                if (lobjProgramDefinition != null)
                {
                    LoggingAdapter.WriteLog("GetProductList - lobjProgramDefinition not null");
                    lobjViatorProductRequest.Top = pstrTopX;

                    //lobjViatorProductRequest.StartDate = pstrStartDate;
                    //lobjViatorProductRequest.EndDate = pstrEndDate;

                    lobjViatorProductRequest.StartDate = DateTime.Now.ToString("yyyy-MM-dd");
                    lobjViatorProductRequest.EndDate = DateTime.Now.AddMonths(12).ToString("yyyy-MM-dd");
                    lobjViatorProductRequest.CatId =Convert.ToInt32(pstrCatId);
                    lobjViatorProductRequest.SubCatId = Convert.ToInt32(pstrSubCatId);
                    lobjViatorProductRequest.DestinationId = Convert.ToInt32(lstrDestId);
                    lobjViatorProductRequest.CurrencyCode = ConfigurationManager.AppSettings["ViatorSourceCurrency"];
                    lobjViatorProductRequest.IsDealsOnly = false;
                    string lstrCurrency = lobjModel.GetDefaultCurrency();
                    HttpContext.Current.Session["SearchCurrency"] = lstrCurrency;
                    lobjViatorProductRequest.PointRate = lobjModel.GetProgramRedemptionRate(lstrCurrency, RedemptionCodeKeys.TOUR.ToString(), lobjProgramDefinition.ProgramId);
                    lobjViatorProductResponse = lobjModel.SearchProduct(lobjViatorProductRequest);
                    LoggingAdapter.WriteLog("Get Product response" + lobjViatorProductResponse.ProductList.Count);
                    if (lobjViatorProductResponse.ProductList != null && lobjViatorProductResponse.ProductList.Count > 0)
                    {
                        HttpContext.Current.Session[SessionFields.TourPackageList] = lobjViatorProductResponse;
                        lstrProductResult = JSONSerialization.Serialize(lobjViatorProductResponse.ProductList);
                        lstrTemplateResult = File.ReadAllText(HttpContext.Current.Server.MapPath("Templates/ProductListTemplate.html"));
                        lobjListOfData[0] = lstrProductResult;
                        lobjListOfData[1] = lstrTemplateResult;
                        return lobjListOfData;
                    }
                    else
                    {
                        LoggingAdapter.WriteLog("Product list is null");
                        return null;
                    }
                }
                else
                {
                    LoggingAdapter.WriteLog("GetProductList - lobjProgramDefinition is null");
                    return null;

                }
            }
            else
            {
                LoggingAdapter.WriteLog("GetProductList - destination is null");
                return null;
            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("GetProductList :-" + ex.Message + ex.InnerException + "Stack Trace-" + ex.StackTrace);
            return null;
        }
    }


    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<Category> PaintCategories()
    {
        try
        {
            string lstrDestId = string.Empty;
            lstrDestId = Convert.ToString(HttpContext.Current.Session[SessionFields.SelectedTourDestinationId]);
            if (!string.IsNullOrEmpty(lstrDestId))
            {
                InfiModel lobjModel = new InfiModel();
                ViatorCategoryResponse lobjViatorCategoryResponse = new ViatorCategoryResponse();
                lobjViatorCategoryResponse = lobjModel.GetCategoryList(Convert.ToInt32(lstrDestId));
                return lobjViatorCategoryResponse.CategoryList;
            }
            else
            {
                LoggingAdapter.WriteLog("PaintCategories - destination is null");
                return null;
            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("PaintCategories Exception :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace);
            return null;
        }

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool BookNowTourProduct(string pstrTourCode)
    {
        try
        {
            InfiModel lobjModel = new InfiModel();
            ViatorProductResponse lobjViatorProductDetailsResponse = new ViatorProductResponse();
            lobjViatorProductDetailsResponse = HttpContext.Current.Session[SessionFields.TourPackageList] as ViatorProductResponse;
            ProductDetails lobjProductDetails = lobjViatorProductDetailsResponse.ProductList.Find(lobjProductList => lobjProductList.Code.Equals(pstrTourCode));

            if (lobjProductDetails != null)
            {
                LoggingAdapter.WriteLog("BookNowTourProduct :- Product not null for code: -" + pstrTourCode);
                HttpContext.Current.Session[SessionFields.SelectedTourPackage] = lobjProductDetails;
                return true;
            }
            else
            {
                LoggingAdapter.WriteLog("BookNowTourProduct :- Product null for code: -" + pstrTourCode);
                return false;
            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("BookNowTourProduct Exception :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace);
            return false;
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] BindProductByCategory(string pstrCategoryId, string pstrTopX)
    {
        string lstrDestId = string.Empty;
        try
        {
            lstrDestId = Convert.ToString(HttpContext.Current.Session[SessionFields.SelectedTourDestinationId]);
            InfiModel lobjModel = new InfiModel();
            ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();
            if (lobjProgramDefinition != null)
            {
                string[] lobjListOfData = new string[2];
                string lstrProductResult = string.Empty;
                string lstrTemplateResult = string.Empty;
                ViatorProductRequest lobjViatorProductRequest = new ViatorProductRequest();
                ViatorProductResponse lobjViatorProductResponse = new ViatorProductResponse();
                lobjViatorProductRequest.Top = pstrTopX;
                lobjViatorProductRequest.CatId = Convert.ToInt32(pstrCategoryId);
                //lobjViatorProductRequest.StartDate = pstrStartDate;
                //lobjViatorProductRequest.EndDate = pstrEndDate;

                lobjViatorProductRequest.StartDate = DateTime.Now.ToString("yyyy-MM-dd");
                lobjViatorProductRequest.EndDate = DateTime.Now.AddMonths(12).ToString("yyyy-MM-dd");

                lobjViatorProductRequest.DestinationId = Convert.ToInt32(lstrDestId);
                lobjViatorProductRequest.CurrencyCode = ConfigurationManager.AppSettings["ViatorSourceCurrency"];
                lobjViatorProductRequest.IsDealsOnly = false;
                string lstrCurrency = lobjModel.GetDefaultCurrency();
                HttpContext.Current.Session["SearchCurrency"] = lstrCurrency;
                lobjViatorProductRequest.PointRate = lobjModel.GetProgramRedemptionRate(lstrCurrency, RedemptionCodeKeys.TOUR.ToString(), lobjProgramDefinition.ProgramId);
                lobjViatorProductResponse = lobjModel.SearchProduct(lobjViatorProductRequest);
                LoggingAdapter.WriteLog("Get Product response" + lobjViatorProductResponse.ProductList.Count);
                if (lobjViatorProductResponse.ProductList != null && lobjViatorProductResponse.ProductList.Count > 0)
                {

                    HttpContext.Current.Session[SessionFields.TourPackageList] = lobjViatorProductResponse;
                    lstrProductResult = JSONSerialization.Serialize(lobjViatorProductResponse.ProductList);
                    lstrTemplateResult = File.ReadAllText(HttpContext.Current.Server.MapPath("Templates/ProductListTemplate.html"));
                    lobjListOfData[0] = lstrProductResult;
                    lobjListOfData[1] = lstrTemplateResult;
                    return lobjListOfData; ;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("BindProductByCategory Exception :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace);
            return null;
        }
    }
}