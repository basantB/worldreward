﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Merchant.Entities;
using InfiPlanetModel.Model;
using Framework.EnterpriseLibrary.Security;
using Framework.EnterpriseLibrary.Security.Constants;
using Core.Platform.Member.Entites;
using System.Text.RegularExpressions;
using System.Text;
using Core.Platform.PointGateway.Service.Helper;
using Core.Platform.PointGateway.Service.Helper.PGService;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.ProgramMaster.Entities;
using Framework.EnterpriseLibrary.UniqueNumberGenerator;
using Core.Platform.Authentication.Entities;
using System.Configuration;

public partial class InfiCashbackRequest : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["MemberDetails"] != null)
            {
                string lstrAuthenticationToken = string.Empty;
                InfiModel lobjModel = new InfiModel();
                MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;

                if (lobjMemberDetails != null)
                {
                    ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramDetailsByID(lobjMemberDetails.ProgramId);
                    if (lobjProgramDefinition != null)
                    {
                        MerchantDetails lobjMerchantDetails = new MerchantDetails();
                        string lstrCurrency = lobjModel.GetDefaultCurrency();
                        lobjMerchantDetails.MerchantCode = ConfigurationSettings.AppSettings["INFICashBackMerchantId"].ToString();
                        lobjMerchantDetails.Password = ConfigurationSettings.AppSettings["INFICashBackMerchantPwd"].ToString();

                        MerchantDetails lobjCurrentMerchantDetails = null;
                        LoggingAdapter.WriteLog("MerchantDetails -" + lobjMerchantDetails.MerchantCode + " / " + lobjMerchantDetails.Password);
                        lobjCurrentMerchantDetails = lobjModel.GetMerchantDetails(lobjMerchantDetails);
                        if (lobjCurrentMerchantDetails == null)
                        {
                            Response.Redirect("InvalidCredential.aspx");
                        }
                        //string lstrSecurityKey = Convert.ToString(Guid.NewGuid());

                        //UpdateAuthenticationTokenStatus(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
                       // lstrAuthenticationToken = GenerateUniqueNumber.Get12DigitNumberDateTime();

                        string lstrResponseDetails = string.Empty;
                        Response lobjResponse = new Response();
                        PGServiceHelper lobjPGServiceHelper = new PGServiceHelper();
                        PGManagerClient lobjPGManagerClient = new PGManagerClient();
                        lobjResponse = lobjPGManagerClient.CheckPointsAvailability(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency, lobjCurrentMerchantDetails.MerchantCode, lobjCurrentMerchantDetails.UserName, lobjCurrentMerchantDetails.Password);
                        string lstrAvailablePoints = string.Empty;
                        if (lobjResponse.IsSucessful)
                        {
                            lstrAvailablePoints = lobjResponse.ReturnObject;
                            lstrResponseDetails = "0" + "|" + lobjProgramDefinition.ProgramCode + lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + "|" + lobjMemberDetails.LastName + "|" + lobjMemberDetails.Email + "|" + lstrAvailablePoints + "|" + lstrAuthenticationToken + "|" + "/";
                            //lstrResponseDetails = "0" + "|" + lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + "|" + lobjMemberDetails.LastName + "|" + lobjMemberDetails.Email + "|" + lobjResponse.ReturnObject + "|" + lstrAuthenticationToken + "|" + "Default";
                        }
                        else
                        {
                            lstrResponseDetails = "1" + "|" + lobjResponse.ExceptionMessage;
                        }
                        LoggingAdapter.WriteLog("INFICashBack = " + lstrResponseDetails);

                        int Token = InsertAuthenticationToken(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, lstrAuthenticationToken);
                        if (Token > 0)
                        {
                            string lstrEncryptedResponseDetails = RSAEncryptor.EncryptString(lstrResponseDetails, RSASecurityConstant.KeySize, RSASecurityConstant.RSAPublicKey);
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<html>");
                            sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
                            sb.AppendFormat("<form name='form' action='{0}' method='post'>", lobjCurrentMerchantDetails.CallBackURL);
                            sb.AppendFormat("<input type='hidden' runat='server' name='Response_Details' value='{0}'>", lstrEncryptedResponseDetails);
                            sb.Append("</form>");
                            sb.Append("</body>");
                            sb.Append("</html>");
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                    }
                }

            }
            else
            {
                string lstrINFICashBackURL = ConfigurationManager.AppSettings["INFICashBackURL"].ToString();
                Response.Redirect(lstrINFICashBackURL, false);
            }
        }
    }

    public int InsertAuthenticationToken(string pstrMemberID, string pstrAuthenticationToken)
    {
        int lintStatus = 0;
        try
        {
            int lintMinutes = Convert.ToInt32(ConfigurationSettings.AppSettings["ExpiryMinutes"]);
            InfiModel lobjInfiModel = new InfiModel();
            AuthenticationDetails lobjAuthenticationDetails = new AuthenticationDetails();
            lobjAuthenticationDetails.MemberId = pstrMemberID;
            lobjAuthenticationDetails.AuthenticationToken = pstrAuthenticationToken;
            lobjAuthenticationDetails.AuthenticationType = AuthenticationType.PG;
            lobjAuthenticationDetails.CreationDate = DateTime.Now;
            lobjAuthenticationDetails.ExpiryDate = DateTime.Now.AddMinutes(lintMinutes);
            lobjAuthenticationDetails.IsActive = true;
            lobjAuthenticationDetails.CreatedBy = "System";
            lintStatus = lobjInfiModel.InsertAuthenticationDetails(lobjAuthenticationDetails);
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("InsertAuthenticationToken exception - " + ex.Message + Environment.NewLine + ex.StackTrace);
        }
        return lintStatus;
    }
}