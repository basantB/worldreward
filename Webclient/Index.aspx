﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <script>
        $(document).ready(function () {
            $("#dnarr").on('click', function (event) {
                if (this.hash !== "") {
                    event.preventDefault();
                    var hash = this.hash;
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 800, function () {
                        window.location.hash = hash;
                    });
                }
            });
        });
    </script>
    <div class="banner">
        <ul class="bxslider" id="Indexbanner">
            <li>
                <img src="images/bnr1.jpg" /></li>
            <li>
                <img src="images/bnr2.jpg" /></li>
            <li>
                <img src="images/bnr3.jpg" /></li>


        </ul>
    </div>
    <div style="text-align: center; margin-top: 10px;">
        <a href="#article" id="dnarr">
            <img style="opacity: .5; height: 50px;" src="images/down-arrow.gif" /></a>
    </div>
    <article id="article">
        <div class="wrap">
            <h1>Welcome to WorldRewardZ... There’s Nothing Beyond It.</h1>
            <p>To offer constant and infinite redemption opportunities, choices among the best in the world and possibly the widest selection of choices, is the purpose of WorldRewardZ.
             <br />
                <br />
            WorldRewardZ is a redemption platform integrated with multiple choices in travel, shopping, gifting to name a few. Aligned with global partners, it is a single source to fulfill every aspiration in redemption. This makes WorldRewardZ a highly gratifying experience. WorldRewardZ welcomes everyone seeking redemption of the highest satisfaction.</p>
        </div>
        <div class="bnrlinks">
            <ul>
                <li>
                    <a href="#">
                        <h2>Travel</h2>
                        <button>Explore</button>
                        <img src="images/bnrtravel.jpg" />
                    </a>
                </li>
                <li class="bnrcol2">
                    <div class="bnrcolbnr">
                        <a href="#">
                            <h2>Shopping</h2>
                            <button>Explore</button>
                            <img src="images/bnrshop.jpg" />
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <h2>Gifting</h2>
                            <button>Explore</button>
                            <img src="images/bnrgifting.jpg" />
                        </a>
                    </div>
                </li>
                <li class="clr"></li>

            </ul>
            <ul class="fullbnr">
                <li>
                    <a href="#">
                        <p>There’s every reason to explore more!</p>
                        <button>Explore</button>
                        <img src="images/bnrxplr.jpg" />
                    </a>
                </li>
                <li class="clr"></li>
            </ul>

        </div>
    </article>
    <div class="testi">
        <h1>TESTIMONIALS</h1>
        <ul>
            <li>
                <img src="images/imgpro.jpg" />
                <p class="triangle-border left">
                    “Lorem ipsum dolor sit amet, consectetuer elit, sed adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat elitvolutpat. Ut wisi enim ad minim veniam, quis exerci tation ad minim veniam, quis nostrud ”
                        <br />
                    <br />
                    <b>- Mark Johnson</b>
                </p>
            </li>
            <li class="marnone">
                <img src="images/imgpro.jpg" />
                <p class="triangle-border left">
                    “Lorem ipsum dolor sit amet, consectetuer elit, sed adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat elitvolutpat. Ut wisi enim ad minim veniam, quis exerci tation ad minim veniam, quis nostrud ”
                        <br />
                    <br />
                    <b>- Mark Johnson</b>
                </p>
            </li>
        </ul>
    </div>
</asp:Content>

