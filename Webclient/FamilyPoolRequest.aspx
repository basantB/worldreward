﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="FamilyPoolRequest.aspx.cs" Inherits="FamilyPoolRequest" %>

<%@ Register Src="~/UserControl/ActivationMenu.ascx" TagName="ActivationMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="css/myAccount.css" rel="stylesheet" />
    <style type="text/css">
        .red {
        color:red;
        }
        @media screen and (max-device-width :768px), screen and (max-width:768px) {

            .pass-desktop1 {
                display: none;
            }

            .pass-mobile1 {
                display: block;
            }
        }

        @media only screen and (min-width:769px) and (orientation:landscape) {
            .pass-desktop1 {
                display: block;
            }

            .pass-mobile1 {
                display: none;
            }
        }
    </style>
    <script src="js/FamilyPoolValidation.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajax({
                url: 'FamilyPoolRequest.aspx/CheckFamilyMember',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: "",
                dataType: 'json',
                success: function (data) {
                    try {
                        var newData = data.d;
                        if (newData != "") {
                            if (newData == "Success") {
                                $("#OTPRId").show();
                            }
                            else if (newData == "Exceed") {
                                $("#OTPRId").hide();
                                $("#errordiv").css("display", "block");
                                $("#AccountValidation")[0].innerHTML = "Family Members limit Exceed.";
                            }
                            else if (newData == "Child") {
                                $("#OTPRId").hide();
                                $("#errordiv").css("display", "block");
                                $("#AccountValidation")[0].innerHTML = "You can not add any family Member.";
                            }
                            else {
                                $("#OTPRId").hide();
                                $("#errordiv").css("display", "block");
                                $("#AccountValidation")[0].innerHTML = "Please try again.";
                            }
                        }
                        else {
                            $("#OTPRId").hide();
                            $("#errordiv").css("display", "block");
                            $("#AccountValidation")[0].innerHTML = "Please try again.";
                        }
                    }
                    catch (e) {
                        return false;
                    }
                    return false;
                },
                error: function (errmsg) {

                }
            });
            return false;
        });

        function showMyDiv() {
            $("#CP_txtAccountNumber").removeAttr('disabled');
            return false;
        }

    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>
    <!--familypool management-->
     <article id="article">
         <div class="wrap">
            <h1>Experience Abundance with WorldRewardZ</h1>
            <p>Below is a categorized listing of your activities at WorldRewardZ. As you glance through them, you can immerse in the joy of having the best choices in the world available to you simply at a click, with WorldRewardZ. Go ahead and multiply your unique experiences.</p>
			<div class="ManyWrap MyAccount">
            <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
            <div class="InnCont" id="divCurrency" runat="server">
             <div id="divRepPoolRecords" runat="server">
              <asp:Label ID="Label2" runat="server"><h1>Member Pooling Details</h1></asp:Label>
               <br />
               <asp:Repeater ID="RepPoolRecords" runat="server" OnItemCommand="RepPoolRecords_ItemCommand" OnItemDataBound="RepPoolRecords_ItemDataBound">
                            <HeaderTemplate>
                                <table width="98%" border="1">
                                    <tr class="stylegridheader">
                                        <td style="vertical-align: middle !important"><b>Member Name</b>
                                        </td>
                                        <td style="vertical-align: middle !important"><b>Relation Reference</b>
                                        </td>
                                        <td></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="stylealternate1" style="height: 32px; vertical-align: middle">
                                    <td style="vertical-align: middle !important; padding-top: 0">
                                        <%#Eval("LastName")%>
                                    </td>
                                    <td style="vertical-align: middle !important; padding-top: 0">
                                        <%#Eval("MemberRelationsList[0].RelationReference")%>
                                        <asp:HiddenField ID="poolId" runat="server" Value='<%#Eval("Id")%>' />
                                    </td>
                                    <td style="vertical-align: middle !important; padding-top: 0">
                                        <%--<asp:Button runat="server" Text="UnMerge" OnClick="Button_Click" CommandName="UnMerge" CommandArgument='<%# Eval("Id")%>' />--%>
                                        <%--<a id="LINKME" href="UnmergeFamilyPool.aspx?id=<%# Eval("Id") %>" style="text-decoration: none">UnMerge</a>--%>
                                        <asp:HyperLink ID="HyperLink1" CssClass="LinkRemoveHY" runat="server" Text="UnMerge" NavigateUrl='<%#Eval("Id","~/UnmergeFamilyPool.aspx?Id={0}") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
            <div id="OTPRId">
            <h2><asp:Label ID="Label1" runat="server"><h1>Add Member Details</h1></asp:Label></h2>
            <ul class="tabInfo">
			<li>
			<label>Family Member UID</label>
			<%--<input type="text" name="">--%>
            <asp:TextBox ID="txtAccountNumber" runat="server" MaxLength="20" Enabled="true" ValidationGroup="Group"></asp:TextBox>
			</li>
			<li><label>&nbsp;</label>
             <%--   <a class="btnRed" href="#">ADD NOW</a>--%>
            <asp:Button CssClass="buttonContainer btnRed btnred_width" runat="server" ID="btnlogin" Text="Add Now"
                 OnClientClick="var retvalue = ActivationOTPValidation();  event.returnValue= retvalue; if(event.preventDefault)event.preventDefault();return retvalue;" />
			</li>
	        </ul>
                   <div id="validationResult" class="ErrorLogin">
                    </div>
                    <br />
                    <br />
                    <div id="ActivateOTP" style="display: none;">
                        <div>
                            <asp:Label ID="lblOTP" runat="server" Text="One Time Password"></asp:Label>
                        </div>
                        <div>
                            <div>
                                <asp:TextBox ID="txtOTP" runat="server" ValidationGroup="Activate" MaxLength="4"
                                    TextMode="Password" CssClass="Login_TextBox_Style" onkeypress="var retValue = ActivationOnEnter(event); event.returnValue= retValue; return retValue;"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <asp:Button runat="server" ID="btnContinue" Text="Continue"
                                OnClientClick="var retvalue = ShowPasswordDiv();  event.returnValue= retvalue; if(event.preventDefault)event.preventDefault();return retvalue;" />
                        </div>
                    </div>
                    <div id="divMessge" style="display: none;" class="red">
                        <asp:Label runat="server" ID="lblMessagesDetails"></asp:Label>
                    </div>
            </div>
            </div>
            </div>
           </div>
         </article>
    <!--End of familypool management-->

    <%--<div class="inr-wrap">
        <section class="myaccContent">
        <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
        <div class="myacc-conthdr">
            <div id="divCurrency" class="myacc-user" runat="server">
                Family Pool Request
            </div>
            <div class="acc-transactnsumry">
                <div>
                    <div id="divRepPoolRecords" runat="server">
                        <asp:Label ID="Label2" runat="server"><h1>Member Pooling Details</h1></asp:Label>
                        <br />
                        <asp:Repeater ID="RepPoolRecords" runat="server" OnItemCommand="RepPoolRecords_ItemCommand" OnItemDataBound="RepPoolRecords_ItemDataBound">
                            <HeaderTemplate>
                                <table width="98%" border="1">
                                    <tr class="stylegridheader">
                                        <td style="vertical-align: middle !important"><b>Member Name</b>
                                        </td>
                                        <td style="vertical-align: middle !important"><b>Relation Reference</b>
                                        </td>
                                        <td></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="stylealternate1" style="height: 32px; vertical-align: middle">
                                    <td style="vertical-align: middle !important; padding-top: 0">
                                        <%#Eval("LastName")%>
                                    </td>
                                    <td style="vertical-align: middle !important; padding-top: 0">
                                        <%#Eval("MemberRelationsList[0].RelationReference")%>
                                        <asp:HiddenField ID="poolId" runat="server" Value='<%#Eval("Id")%>' />
                                    </td>
                                    <td style="vertical-align: middle !important; padding-top: 0">
                                       <asp:Button runat="server" Text="UnMerge" OnClick="Button_Click" CommandName="UnMerge" CommandArgument='<%# Eval("Id")%>' />
                                        <a id="LINKME" href="UnmergeFamilyPool.aspx?id=<%# Eval("Id") %>" style="text-decoration: none">UnMerge</a>
                                        <asp:HyperLink ID="HyperLink1" CssClass="LinkRemoveHY" runat="server" Text="UnMerge" NavigateUrl='<%#Eval("Id","~/UnmergeFamilyPool.aspx?Id={0}") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <div id="OTPRId">
                        <br />
                        <asp:Label ID="Label1" runat="server"><h1>Add Member Details</h1></asp:Label>
                        <br />
                        <div>
                            <b>
                                <asp:Label ID="lblMemberId" runat="server" Text="Family Member UID"></asp:Label></b>
                        </div>
                        <div>
                            <div>
                                <asp:TextBox ID="txtAccountNumber" runat="server" MaxLength="20"
                                    Enabled="true" ValidationGroup="Group"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <asp:Button CssClass="buttonContainer" runat="server" ID="btnlogin" Text="Add"
                                OnClientClick="var retvalue = ActivationOTPValidation();  event.returnValue= retvalue; if(event.preventDefault)event.preventDefault();return retvalue;" />
                        </div>
                    </div>
                    <div id="validationResult" class="ErrorLogin">
                    </div>
                    <br />
                    <br />
                    <div id="ActivateOTP" style="display: none;">
                        <div>
                            <asp:Label ID="lblOTP" runat="server" Text="One Time Password"></asp:Label>
                        </div>
                        <div>
                            <div>
                                <asp:TextBox ID="txtOTP" runat="server" ValidationGroup="Activate" MaxLength="4"
                                    TextMode="Password" CssClass="Login_TextBox_Style" onkeypress="var retValue = ActivationOnEnter(event); event.returnValue= retValue; return retValue;"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <asp:Button runat="server" ID="btnContinue" Text="Continue"
                                OnClientClick="var retvalue = ShowPasswordDiv();  event.returnValue= retvalue; if(event.preventDefault)event.preventDefault();return retvalue;" />
                        </div>
                    </div>
                    <div id="divMessge" style="display: none;" class="red">
                        <asp:Label runat="server" ID="lblMessagesDetails"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        </section>
        <div class="clr"></div>
    </div>--%>
</asp:Content>
