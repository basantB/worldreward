﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="css/myAccount.css" rel="stylesheet" />
    <script src="js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="js/jquery.mCustomScrollbar.css" type="text/css" />
    <script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>

    <!--About Us-->
        <article id="article">
        <div class="wrap staticPage faqPage">
            <h1>About Us</h1>
			<div class="innerTxt">
               <p class="contestyle robot">
                    Welcome to WorldRewardZ, serving
                    the UAE since 1975. WorldRewardZ  is the first comprehensive Bank Loyalty Program
                    that caters to all your lifestyle needs. It is the only program in the country where
                    you earn points for transactions performed on variety of banking products, both
                    conventional & Islamic, and can be redeemed over the following options:
                </p>
                <p style="font-weight:bold;">Airlines</p>
                 <p>Over 900 airlines including 92 Low cost airlines (such as FlyDubai, Air Arabia,
                    Ryan Air and many more)
                
                        <p>Any Airline: With WorldRewardZ , you’re not restricted to one airline or one departure
                            point, and you can redeem your tickets online directly from more than 900 airlines
                            around the world. </p>
                        <p>Any Time: You can now book and travel any time and any date you want to travel.
                            NO BLACK - OUT DATES</p>
                        <p>Any Place: Redeem your WorldRewardZ  for a flight from London to Moscow, while residing
                            in Dubai </p>
                   
                </p>
                <p>Furthermore, enjoy the double benefit of extra  by earning airline frequent
                    flier miles while redeeming your WorldRewardZ . WorldRewardZ  also gives you the cheapest
                    flights for any class you may wish to travel - Anytime, Anyplace, Anywhere.
                </p>
                <p style="font-weight:bold;">hotels</p>
                <p>Over 200,000 hotels across the Globe, instantly at a click of a button (booking and redemption process similar to airlines)</p>
                <br />
                <p style="font-weight:bold;">Electronic items</p>
                <p>Over 2,000 of products to choose:</p>
                <p>All leading brands available – like Apple, Sony, Samsung, Sony, Toshiba and many more.</p>
                <p>Products will be home delivered anywhere in the UAE</p>
                <br />
                <p style="font-weight:bold;">Official Barça Merchandise</p>
                <p>Over hundred's of Official FC Barcelona licensed merchandise.</p>
                <br />
                <p style="font-weight:bold;">Car Rental</p>
                <p>Car rental available in 150 countries worldwide.</p>
                 <br />
                <p style="font-weight:bold; color:#f00">Enrollment</p>
                <p>You are automatically enrolled in the WorldRewardZ  program through your WorldRewardZ product
                            relationship and you can access all the features and functionalities of the WorldRewardZ
                             website. Please log on to <a href="https://www.WorldRewardz.ae/Activation.aspx">www.WorldRewardZ.ae</a> and enter your membership number,
                            email id, mobile number and Mother’s maiden name to successfully activate your WorldRewardZ
                             account.</p>
                <p>You will receive an email with a system generated password and a link to the WorldRewardZ website. Follow the link to change your password. Once completed, you will be able to access your WorldRewardZ account with your new password.</p>
                <p>To experience the Ultimate  Program: please visit – <a href="http://www.WorldRewardz.ae/index.aspx">www.WorldRewardZ.ae</a></p>
             </div>
            </div>
        </article>
     <!--end of About Us-->

    
    <script>
        $(document).ready(function () {
            $(".flt-divR").niceScroll({
                cursorcolor: "#e3443e",
                cursoropacitymin: 0.3,
                background: "#bbb",
                cursorborder: "0",
                autohidemode: false,
                cursorminheight: 30
            });
        });
    </script>
</asp:Content>

