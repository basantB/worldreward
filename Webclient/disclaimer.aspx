﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="disclaimer.aspx.cs" Inherits="disclaimer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" Runat="Server">
 <link href="css/jquery.css" rel="stylesheet" />
 <link href="css/myAccount.css" rel="stylesheet" />

    <article id="article">
        <div class="wrap staticPage faqPage">
            <h1>Disclaimer</h1>
			<div class="innerTxt">
				<p>These online Terms & Conditions are a legally binding agreement between you and WorldRewardZ. Use of this Website implies that you accept these Terms & Conditions.
<br><br>
WorldRewardZ makes no representation or warranty of any kind, express, implied or statutory regarding this Website or the materials and information contained or referred to on each page associated with this Website. The material and information contained on this Website is provided for general information only and should not be used as a basis for making business decisions. Any advice or information received via this Website should not be relied upon without consulting primary or more accurate or more up-to-date sources of information or specific professional advice. It is recommended to obtain professional advice and not rely on the information on the Website.
<br><br>
WorldRewardZ accepts no liability for any loss or damage arising directly or indirectly from action taken, or not taken, in reliance on material or information contained on this Website. In particular, no warranty is given that economic reporting information, material or data, is accurate, reliable or up-to-date. The data and information do not constitute specific advice but are for information purposes only.
<br><br>
WorldRewardZ accepts no liability and will not be liable for any loss or damage arising directly or indirectly (including special, incidental or consequential loss or damage) from your use of this Website, howsoever arising, and including any loss, damage or expense arising from, but not limited to, any defect, error, imperfection, fault, mistake or inaccuracy with this Website, its contents or associated services, or due to any unavailability of the Website or any part thereof or any contents or associated services.
<br><br>
Products and services are available only at the discretion of WorldRewardZ and subject to the products’ and services’ individual contractual terms and conditions on which they are offered and such products and services may be withdrawn or amended at any time without notice. The full range of products or services may not be available at all locations.
	</p>
		</div>
			
			</div>

</article>
</asp:Content>

