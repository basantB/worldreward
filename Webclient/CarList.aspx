﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CarList.aspx.cs" Inherits="CarList"
    MasterPageFile="~/InfiPlanetMaster.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <script src="js/angular.js"></script>
    <script src="js/angular-animate.js"></script>
    <script src="js/ui-bootstrap-tpls-1.3.3.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/angular.rangeSlider.js"></script>
    <link href="css/datepicker.css" rel="stylesheet" />
    <link href="css/angular.rangeSlider.css" rel="stylesheet" />
    <link href="css/Car.css" type="text/css" rel="stylesheet" />
    <script src="js/CarListScript.js" type="text/javascript"></script>
    <script src="js/CarSearchScript.js"></script>
    <asp:HiddenField ID="hdnddlToTime" runat="server" />
    <asp:HiddenField ID="hdnddlFromTime" runat="server" />
    <asp:HiddenField Value="" ID="hdnPaymentType" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnCarFilterRange" Value="" runat="server" />
    <asp:HiddenField ID="hdnModifySearchData" runat="server" />
    <style>
        .dropDetails {
            display: none;
        }

        .dates select option, .rooms select option, .pkdtm select option {
            padding: 1px 10% 9px 10px !important;
            text-align: left !important;
        }
    </style>
    <div ng-app="Danamoncar" ng-controller="MainCtrl">
        <div class="inrpg">
            <div class="pgcontent flight">
                <div>
                    <div class="btn-filter" id="btnfliter">
                        <img src="Images/ico-fltr.png" />
                    </div>
                    <div id="divBlockAll">
                    </div>
                    <div class="pgcol1">
                        <%--<div class="srchtitle" id="carinfo">
                        </div>--%>
                        <div class="alrt"></div>

                        <div class="usrched">
                            <ul id="carinfo">
                            </ul>
                        </div>
                        <div class="mobusrched">
                            <ul id="mobcarinfo">
                            </ul>
                        </div>
                        <hr />

                        <div class="clr"></div>
                        <div style="display: none;" id="carmain">
                            <div class="carbox" ng-repeat="x in results">
                                <input type="hidden" value="{{SetCarClass(x.Vehicle[0].group)}}" />
                                <input type="hidden" value='{{SetConditioning(x.Vehicle[0].aircon)}}' />
                                <input type="hidden" value="{{x.Vehicle[0].automatic}}" />
                                <input type="hidden" value="{{x.Price[0].TotalPoints}}" />
                                <div class="carinfo">
                                    <div class="ftlimgnm">
                                        <div class="carimg">
                                            <img src='{{x.Vehicle[0].LargeImageURL}}' width="80%" onerror="this.onerror=null;this.src='images/no-image.png';" />
                                        </div>
                                        <div class="carname">
                                            <h2>{{x.Vehicle[0].Name}}</h2>
                                            <div class="carclass">Car Class : {{SetCarClass(x.Vehicle[0].group)}}</div>
                                            <a href="#" ng-click='ViewCarInfo(x.Route[0].PickUp[0].id)'>Terms & Conditions
                                            </a>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                    <div class="abtcar">
                                        <ul class="carservs">
                                            <li>
                                                <img src="images/ico-offarpt.png" />{{SetAirport(x.Route[0].PickUp[0].onAirport)}}</li>
                                            <li class="brdrtnone">
                                                <img src="images/ico-fl2fl.png" />{{SetFuelPolicy(x.Vehicle[0].fuelPolicy)}}</li>
                                            <li>
                                                <img src="images/ico-4pple.png" />{{x.Vehicle[0].seats}} People</li>
                                            <li class="brdrtnone">
                                                <img src="images/ico-4drs.png" />{{x.Vehicle[0].doors}} Doors</li>
                                            <li class="brdbtnone">
                                                <img src="images/ico-amtc.png" />{{SetTransmission(x.Vehicle[0].automatic)}}
                                            </li>
                                            <li class="brdrtnone brdbtnone lnht40">
                                                <img src="images/ico-nonac.png" />{{SetConditioning(x.Vehicle[0].aircon)}}</li>
                                        </ul>
                                    </div>
                                    <div class="clr"></div>
                                </div>

                                <div class="pntsbtn">
                                    <div class="dpnts">
                                        <ul>
                                            <li class="pnts">
                                                <span id="CP_rptHotelList_lblmiles_0">{{Digits(x.Price[0].TotalPoints)}}</span></li>
                                            <li>DPoints
                                                      </li>
                                        </ul>
                                    </div>
                                    <div class="dbtns">
                                        <div class="button" id="btngo" ng-click="gotoBooking(x.Vehicle[0].id)">Book a Ride</div>
                                    </div>
                                    <div class="clr"></div>

                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                    <div class="pgcol2" id="divSearch">
                        <div class="widget-prfl" id="UserInfo">
                            <div class="widget-user">
                                <ul id="UserDetails">
                                </ul>
                            </div>
                            <div id="Content_Wrapper">
                                <div class="widget-srhpnl">
                                    <h3 ng-click="showModifyCar()">CAR SEARCH</h3>
                                    <div id="modifyDetails" style="display: none">
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                        <div class="widget-flds">
                                            <ul>
                                                Pick-Up
                        <li class="autogen">
                            <label>Country</label>
                            <input class="space1" id="txtCountry" autocomplete="off" runat="server" type="text" ng-model="CarPickcountry" typeahead-min-length='3' typeahead-on-select="onCountrySelect($item, $model, $label)" uib-typeahead="CarCountry for CarCountry in CarcountryList($viewValue)" />

                            <li class="dates">
                                <ul>
                                    <li>
                                        <label>City</label>
                                        <select id="DDlCity" runat="server" onchange="return PickupCityChange()">
                                            <option selected="selected" value="Select City">Select City</option>
                                        </select>
                                    </li>
                                    <li class="mrno">
                                        <label>Location</label>
                                        <select id="DDLLocation" runat="server" onchange="return PickupLocationSelect()">
                                            <option selected="selected" value="Select Location">Select Location</option>
                                        </select>
                                    </li>
                                </ul>

                            </li>
                            <li class="pkdtm">

                                <ul>

                                    <li class="pkdt">
                                        <label>Date</label>
                                        <input class="space2" type="text" autocomplete="off" id="CarTextBoxCheckin" runat="server" />
                                    </li>
                                    <li>
                                        <label>Hours</label>
                                        <select class="space2" id="ddlFromTime" runat="server">
                                            <option value="0" selected="selected">00</option>
                                        </select>
                                    </li>
                                    <li class="mrno">
                                        <label>Mins</label>
                                        <select class="space2" id="ddlMinFrom" runat="server">
                                            <option selected="selected" value="00">00</option>
                                            <option value="15">15</option>
                                            <option value="30">30</option>
                                            <option value="45">45</option>
                                        </select>
                                    </li>

                                </ul>
                            </li>
                            Drop Off
                            <li class="dropDetails">

                                <label>Country</label><select id="ddlDropCountry" runat="server" class="space3" onchange="return DropOffCountryChange();"></select></li>

                            <li class="dates dropDetails">
                                <ul>
                                    <li>
                                        <label>City</label>
                                        <select id="ddlDropCity" runat="server" onchange="return DropoffCitySelect();">
                                            <option selected="selected" value="Select City">Select City</option>
                                        </select>
                                    </li>
                                    <li class="mrno">
                                        <label>Location</label>
                                        <select id="ddlDropLocation" runat="server" onchange="return DropoffLocationSelect();">
                                            <option selected="selected" value="Select Location">Select Location</option>
                                        </select>
                                    </li>
                                </ul>
                            </li>
                            <li class="pkdtm">
                                <ul>
                                    <li class="pkdt">
                                        <label>Date</label>
                                        <input class="space2" type="text" autocomplete="off" id="CarTextBoxCheckout" runat="server" />
                                    </li>
                                    <li>
                                        <label>Hours</label>
                                        <select id="ddlDropFromTime" runat="server" class="space2">
                                            <option value="0" selected="selected">00</option>
                                        </select>

                                    </li>
                                    <li class="mrno">
                                        <label>Mins</label>
                                        <select id="ddlDropMinFrom" runat="server" class="space2">
                                            <option selected="selected" value="00">00</option>
                                            <option value="15">15</option>
                                            <option value="30">30</option>
                                            <option value="45">45</option>
                                        </select>
                                    </li>
                                </ul>
                            </li>
                                            </ul>
                                        </div>
                                        <div class="chckbx">
                                            <input id="droploction" type="checkbox" value="1" runat="server" checked="checked" onchange="return showreturnCar();" />
                                            Return from same location 
                                        </div>
                                        <div>
                                            <input id="chkDriver" type="checkbox" value="1" runat="server" checked="checked" onchange="return showdriverAge();" />
                                            Driver aged between 25-70 years?
                                        </div>
                                        <div class="driverAge" style="display: none">
                                            <asp:TextBox autocomplete="off" ID="txtDiverAge" runat="server" Text="25"></asp:TextBox>
                                        </div>
                                        <span id="CarValidationError" runat="server"></span>
                                        <div class="button" ng-click="CarSearch()">Search</div>
                                        <div class="clr"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="rfnsrch" id="fliterDetails">
                            <h3>Refine your Search
                            </h3>

                            <%-- <div class="rwdpnts">
                                <h3>Rayan Points</h3>
                                <div range-slider min="priceSlider.range.min" max="priceSlider.range.max" model-min="priceSlider.minPrice" model-max="priceSlider.maxPrice" step="1" on-handle-up="priceSliderStop()"></div>
                                <%--      <span id="priceRange"></span>
                            </div>--%>
                            <div class="carcls">
                                <h3>Car Class</h3>
                                <ul id="divCarType">
                                </ul>
                            </div>

                            <div class="carcls">
                                <h2>Car Conditioning</h2>
                                <ul id="divCarClass">
                                    <%--  <li>
                                        <input type="checkbox" /><img src="images/ico-nonac.png" />
                                        Air Conditioning                                    </li>
                                    <li>
                                        <input type="checkbox" /><img src="images/ico-nonac.png" />
                                        non AC                                    </li>--%>
                                </ul>
                            </div>

                            <div class="carcls">
                                <h2>Car Transmission</h2>
                                <ul id="divCarTransmission">
                                    <%-- <li>
                                        <input type="checkbox" /><img src="images/ico-amtc.png" />
                                        Automatic</li>
                                    <li>
                                        <input type="checkbox" /><img src="images/ico-amtc.png" />
                                        Manual</li>--%>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div id="divCarDetails" class="hidecontent">
                        <div style="float: right; text-align: right; margin-top: 0.5%;">
                            <img id="linkbuttonclose" src="Images/ico_close.png" ng-click="CloseCarInfo();"
                                style="cursor: pointer" />
                        </div>
                        <div class="otherInfo_upperdiv">
                            <div id="divFurtherInfo" class="rentalInfo_div ArialNarrow"></div>
                        </div>
                    </div>
                    <div id="CarFade" class="Car_Details_back"></div>

                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
