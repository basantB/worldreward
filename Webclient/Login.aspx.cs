﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfiPlanetModel.Model;
using Core.Platform.Member.Entites;
using System.Configuration;

using System.Text.RegularExpressions;
using System.Web.SessionState;
using System.Reflection;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.OTP.Entities;
using Framework.EnterpriseLibrary.Adapters;
using System.Security.Cryptography;
using System.IO;
using System.Text;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["CallbackUrl"] != null)
        {
            Session["CallbackUrl"] = Decrypt(HttpUtility.UrlDecode(Request.QueryString["CallbackUrl"]));
        }
        if (Request.QueryString["id"] != null)
        {
            if (Request.QueryString["id"].Equals("1"))//Normal login
            {

            }
            else if (Request.QueryString["id"].Equals("2"))//Activation
            {
                //  lblLoginError.Text = "Please enter your ID number and password that has been sent to your registered email and mobile number.";
            }
            else if (Request.QueryString["id"].Equals("3"))//Block
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Call my function", "showMyLoginDiv()", true);
            }
        }
        if (Request.Cookies["InfiLogin"] != null)
        {
            string username = Request.Cookies["InfiLogin"].Values["MembershipRef"];
            txtMemberName.Value = username;
        }
        if (Session["MemberDetails"] != null)
        {
            if (Session["PurchaseMilesClick"] != null)
            {
                Response.Redirect("PurchasePoints.aspx");
            }
            else if (Session["SelectedItinerary"] != null)
            {
                Response.Redirect("FlightPassenger.aspx");
            }
            else if (Session["SelectedTourPackage"] != null)
            {
                LoggingAdapter.WriteLog("Loginpage");
                Response.Redirect("TravellerDetails.aspx");
            }
            else if (Session["LoungeSearchResponse"] != null && Session["LoungeBookingRequest"] != null)
            {
                Response.Redirect("LoungeReviewAndConfirm.aspx");
            }
            else
            {
                Response.Redirect("StatementSummary.aspx");
            }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string SigninUser(string pstrMemberid, string pstrPassword, bool pboolRememberMe)
    {
        InfiModel lobjInfiModel = new InfiModel();
        MemberDetails lobjMemberDetails = null;
        string mstrRedirectEmptyURL = string.Empty;
        try
        {
            LoggingAdapter.WriteLog("Login call SigninUser");
            string lstrEncryptPassword = string.Empty;
            string lstrDecryptPassword = string.Empty;
            //ProgramMaster lobjProgramMaster = lobjCBQModel.GetProgramDetails(lstrProgramName);
            //ProgramMaster lobjProgramMaster = HttpContext.Current.Application["ProgramMaster"] as ProgramMaster;

            MemberDetails lobjEnrollMemberDetails = new MemberDetails();
            SearchMember lobjSearchMember = new SearchMember();
            //lobjSearchMember.RelationReference = pstrMemberid;
           // lobjSearchMember.RelationType = Convert.ToInt32(RelationType.Login);
            lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
            lobjSearchMember.SearchColumnName = "Created_By";
            lobjSearchMember.SearchColumnValue = pstrMemberid;
            LoggingAdapter.WriteLog("Search Details :- User Id:" + pstrMemberid + " Relation Type:" + lobjSearchMember.RelationType);
           // lobjEnrollMemberDetails = lobjInfiModel.GetEnrollMemberDetails(lobjSearchMember);
            lobjEnrollMemberDetails = lobjInfiModel.GetEnrollMemberDetailsByPrimaryReference(lobjSearchMember);


            if (lobjEnrollMemberDetails != null)
            {
                ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramDetailsByID(lobjEnrollMemberDetails.ProgramId);
                
                if (lobjProgramDefinition != null)
                {
                    SystemParameter lobjPasswordPolicy = lobjInfiModel.GetSystemParametres(lobjProgramDefinition.ProgramId);
                    if (lobjPasswordPolicy != null)
                    {
                        Regex regex = new Regex((lobjPasswordPolicy.PasswordPolicy));
                        if (regex.Match(pstrPassword).Success)
                        {
                            lstrEncryptPassword = lobjInfiModel.EncryptPassword(pstrPassword);
                            string lstrRelationRef=lobjEnrollMemberDetails.MemberRelationsList.Find(lobj=>lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                            lobjMemberDetails = lobjInfiModel.CheckLoginRelationMembershipCrediantials(lstrRelationRef, lstrEncryptPassword, lobjProgramDefinition.ProgramId);                           
                            if (lobjMemberDetails != null)
                            {
                                LoggingAdapter.WriteLog("LoginPage member details found");
                                HttpContext.Current.Session["ProgramMaster"] = lobjProgramDefinition;
                                List<ProgramCurrencyDefinition> lobjListOfProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
                                lobjListOfProgramCurrencyDefinition = lobjInfiModel.GetProgramCurrencyDefinition(lobjProgramDefinition.ProgramId);
                                HttpContext.Current.Session["ProgramCurrency"] = lobjListOfProgramCurrencyDefinition;
                                string lstrCurrency = lobjListOfProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals("REWARDZ")).Currency;
                                HttpContext.Current.Session["DefaultCurrency"] = lstrCurrency;

                                updateSessionId(HttpContext.Current);
                                if (pboolRememberMe)
                                {
                                    HttpCookie cookie = new HttpCookie("InfiLogin");
                                    cookie.Values.Add("MembershipRef", pstrMemberid);
                                    cookie.Expires = DateTime.Now.AddDays(15);
                                    HttpContext.Current.Response.Cookies.Add(cookie);
                                }
                                Core.Platform.MemberActivity.Entities.MemberActivitySession lobjMemberActivitySession = HttpContext.Current.Session["MemberActivitySession"] as Core.Platform.MemberActivity.Entities.MemberActivitySession;
                                lobjMemberActivitySession.ReferenceNumber = pstrMemberid;
                                lobjMemberActivitySession.ActivityType = ActivityType.loginsuccessful;
                                bool isMembershipUpdated = lobjInfiModel.UpdateMemberShipActivitySession(lobjMemberActivitySession);
                                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "successful"), ActivityType.loginsuccessful);

                                HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;

                                if (HttpContext.Current.Session["ShopSelected"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = "InfiShop.aspx?id=" + lobjMemberDetails.MemberRelationsList[0].RelationReference;
                                }
                                else if (HttpContext.Current.Session["BillPay"] != null)
                                {
                                    HttpContext.Current.Session["BillPay"] = null;
                                    mstrRedirectEmptyURL = "UtilityPayment.aspx";
                                }
                                else if (HttpContext.Current.Session["SelectedItinerary"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = "FlightPassenger.aspx";
                                }
                                else if (HttpContext.Current.Session["HotelSelected"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = HttpContext.Current.Session["HotelSelected"].ToString();
                                }
                                else if (HttpContext.Current.Session["CarRefId"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = HttpContext.Current.Session["CarRefId"].ToString();
                                    mstrRedirectEmptyURL = "CarDetails.aspx?id=" + ((MemberDetails)HttpContext.Current.Session["MemberDetails"]).MemberRelationsList[0].RelationReference;
                                }

                                else if (HttpContext.Current.Session["Insurance"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = "Insurance.aspx";
                                }
                                else if (HttpContext.Current.Session["SelectedTourPackage"] != null)
                                {
                                    HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                    mstrRedirectEmptyURL = "TravellerDetails.aspx";
                                }
                                else if (HttpContext.Current.Session["LoungeSearchResponse"] != null && HttpContext.Current.Session["LoungeBookingRequest"] != null)
                                {
                                    mstrRedirectEmptyURL = "LoungeReviewAndConfirm.aspx";
                                }


                                else if (HttpContext.Current.Session["MemberDetails"] != null)
                                {
                                    //if (lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.Login)).ForceChangePassword)
                                    if (lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).ForceChangePassword)
                                    {
                                        HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                        mstrRedirectEmptyURL = "profile.aspx";
                                    }
                                    else if (lobjMemberDetails.MemberRelationsList[0].LoginAttempt >= 5)
                                    {
                                        mstrRedirectEmptyURL = "AccountLock";
                                    }
                                    else if (HttpContext.Current.Session["CallbackUrl"] != null)
                                    {
                                        mstrRedirectEmptyURL = Convert.ToString(HttpContext.Current.Session["CallbackUrl"]);
                                    }
                                    else
                                    {
                                        mstrRedirectEmptyURL = "StatementSummary.aspx";
                                        //HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                                        //if (HttpContext.Current.Session["PurchaseMilesClick"] != null)
                                        //{
                                        //    mstrRedirectEmptyURL = "StatementSummary.aspx";
                                        //}
                                        //else
                                        //{
                                        //    mstrRedirectEmptyURL = "profile.aspx";
                                        //}
                                    }
                                }
                            }
                            else
                            {
                                mstrRedirectEmptyURL = "AuthenticationFailed";
                            }
                        }
                        else
                        {
                            lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "IncorrectFormat"), ActivityType.loginsuccessful);
                            return mstrRedirectEmptyURL = "IncorrectFormat";
                        }
                    }
                }
            }
            else
            {
                mstrRedirectEmptyURL = "AuthenticationFailed";
            }
        }
        catch (ApplicationException ex)
        {
            if (ex.Message.Equals("AccountLock"))
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AccountLock"), ActivityType.loginsuccessful);
                mstrRedirectEmptyURL = "AccountLock";
            }
            else
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AuthenticationFailed"), ActivityType.loginsuccessful);
                mstrRedirectEmptyURL = "AuthenticationFailed";
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.Equals("AccountLock"))
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AccountLock"), ActivityType.loginsuccessful);
                lobjInfiModel.UpdateUserLoginAttempts(pstrMemberid);
                mstrRedirectEmptyURL = "AccountLock";
            }
            else if (ex.Message.Equals("Login Authentication Failed"))
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, "AuthenticationFailed"), ActivityType.loginsuccessful);
                lobjInfiModel.UpdateUserLoginAttempts(pstrMemberid);
                mstrRedirectEmptyURL = "AuthenticationFailed";
            }
        }
        lobjInfiModel.LogActivity(string.Format(ActivityConstants.Login, pstrMemberid, mstrRedirectEmptyURL), ActivityType.Login);
        return mstrRedirectEmptyURL;
    }

    private static void updateSessionId(HttpContext Context)
    {
        System.Web.SessionState.SessionIDManager manager = new System.Web.SessionState.SessionIDManager();
        string oldId = manager.GetSessionID(Context);
        string newId = manager.CreateSessionID(Context);
        bool isAdd = false, isRedir = false;
        manager.SaveSessionID(Context, newId, out isRedir, out isAdd);
        HttpApplication ctx = (HttpApplication)HttpContext.Current.ApplicationInstance;
        HttpModuleCollection mods = ctx.Modules;
        System.Web.SessionState.SessionStateModule ssm = (SessionStateModule)mods.Get("Session");
        System.Reflection.FieldInfo[] fields = ssm.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
        SessionStateStoreProviderBase store = null;
        System.Reflection.FieldInfo rqIdField = null, rqLockIdField = null, rqStateNotFoundField = null;
        foreach (System.Reflection.FieldInfo field in fields)
        {
            if (field.Name.Equals("_store")) store = (SessionStateStoreProviderBase)field.GetValue(ssm);
            if (field.Name.Equals("_rqId")) rqIdField = field;
            if (field.Name.Equals("_rqLockId")) rqLockIdField = field;
            if (field.Name.Equals("_rqSessionStateNotFound")) rqStateNotFoundField = field;
        }
        object lockId = rqLockIdField.GetValue(ssm);
        if ((lockId != null) && (oldId != null)) store.ReleaseItemExclusive(Context, oldId, lockId);
        rqStateNotFoundField.SetValue(ssm, true);
        rqIdField.SetValue(ssm, newId);
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string GenerateOTP(string pstrMemberid)
    {
        InfiModel lobjmodel = new InfiModel();
        string mstrRedirectEmptyURL = string.Empty;
        MemberDetails lobjMemberDetails = null;
        bool lblstatus = false;
        try
        {
            SearchMember lobjSearchMember = new SearchMember();
            //lobjSearchMember.RelationReference = pstrMemberid;
            //lobjSearchMember.RelationType = Convert.ToInt32(RelationType.Login);
            lobjSearchMember.SearchColumnName = "Created_By";
            lobjSearchMember.SearchColumnValue = pstrMemberid;
            lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
            //lobjMemberDetails = lobjmodel.GetEnrollMemberDetails(lobjSearchMember);
            lobjMemberDetails = lobjmodel.GetEnrollMemberDetailsByPrimaryReference(lobjSearchMember);
            if (lobjMemberDetails.LastName != "")
            {
                ProgramDefinition lobjProgramDefinition = lobjmodel.GetProgramDetailsByID(lobjMemberDetails.ProgramId);
                if (lobjProgramDefinition != null)
                {
                    SystemParameter lobjSystemParameter = lobjmodel.GetSystemParametres(lobjProgramDefinition.ProgramId);
                    if (lobjSystemParameter != null)
                    {
                       // MemberRelation lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobjRelation => lobjRelation.RelationType.Equals(RelationType.Login));
                        MemberRelation lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobjRelation => lobjRelation.RelationType.Equals(RelationType.LBMS));
                        if (lobjMemberRelation.Status.Equals(Status.Blocked))
                        {
                            
                            string lstrSourceIpAddress = HttpContext.Current.Request.UserHostAddress;
                            lblstatus = lobjmodel.GenerateOTP(lobjMemberRelation.RelationReference, lstrSourceIpAddress, lobjMemberDetails.ProgramId);
                            if (lblstatus)
                            {
                                mstrRedirectEmptyURL = "Your OTP has been sent to your registered mobile number " + "XXXX" + lobjMemberDetails.MobileNumber.Remove(0, 4) + " and registered email Id " + "XXXXX" + lobjMemberDetails.Email.Remove(0, 5) + ".";
                            }
                            else
                            {
                                mstrRedirectEmptyURL = "Please check the details";
                            }
                        }
                        
                    }
                    else
                    {
                        mstrRedirectEmptyURL = "OTP Generation Failure.";
                    }
                }
                else
                {
                    mstrRedirectEmptyURL = "OTP Generation Failure.";
                }
            }
            else
            {
                mstrRedirectEmptyURL = "Invalid credentials.";
            }
        }
        catch
        {
            mstrRedirectEmptyURL = "OTP Generation Failure.";
        }
        return mstrRedirectEmptyURL;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string UnlockMember(string pstrMemberid, string pstrOTP)
    {
        InfiModel lobjmodel = new InfiModel();
        string mstrRedirectEmptyURL = string.Empty;
        bool status = false;
        try
        {
            status = lobjmodel.UnlcokMemberByOTP(pstrMemberid, pstrOTP);
            if (status)
            {
                mstrRedirectEmptyURL = "Account Unlocked successfully.<a class='ErrorMessageLink' onclick='showLoginDiv();'>Click here</a> to continue.";
            }
            else
            {
                mstrRedirectEmptyURL = "Invalid Credentials";
            }
        }
        catch
        {
            mstrRedirectEmptyURL = "Invalid Credentials";
        }
        return mstrRedirectEmptyURL;
    }
    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }
}