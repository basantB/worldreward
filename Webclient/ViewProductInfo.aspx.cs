﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Core.Platform.ProgramMaster.Entities;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using System.IO;
using InfiPlanetModel.Model;
using Viator.Platform.ClientEntities;
using InfiPlanet.SessionConstants;
using Framework.EnterpriseLibrary.Adapters;

public partial class ViewProductInfo : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        InfiModel lobjInfiModel = new InfiModel();
        try
        {

            //ddlAdult.Items.AddRange(Enumerable.Range(1, 15).Select(d => new ListItem(d.ToString())).ToArray());
            //ddlChild.Items.AddRange(Enumerable.Range(0, 15).Select(d => new ListItem(d.ToString())).ToArray());
            //ddlInfant.Items.AddRange(Enumerable.Range(0, 15).Select(d => new ListItem(d.ToString())).ToArray());
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("ViewProductInfo pageload :-" + ex.Message + ex.InnerException + "Stack Trace-" + ex.StackTrace);

        }

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static ViatorProductDetailsResponse BindAdditinalInfo()
    {
        InfiModel lobjInfiModel = new InfiModel();
        ViatorProductDetailsResponse lobjViatorProductDetailsResponse = new ViatorProductDetailsResponse();
        ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramMaster();
        if (lobjProgramDefinition != null)
        {
            if (HttpContext.Current.Session[SessionFields.SelectedTourPackage] != null)
            {
                ProductDetails lobjProductDetails = new ProductDetails();
                lobjProductDetails = HttpContext.Current.Session[SessionFields.SelectedTourPackage] as ProductDetails;
                ViatorProductRequest lobjViatorProductRequest = new ViatorProductRequest();
                lobjViatorProductRequest.Top = "";
                lobjViatorProductRequest.StartDate = "";
                lobjViatorProductRequest.EndDate = "";
                lobjViatorProductRequest.DestinationId = lobjProductDetails.DestinationId;
                lobjViatorProductRequest.ProductCodeList.Add(lobjProductDetails.Code);
                lobjViatorProductRequest.CurrencyCode = ConfigurationManager.AppSettings["ViatorSourceCurrency"];
                lobjViatorProductRequest.IsDealsOnly = false;
                string lstrCurrency = lobjInfiModel.GetDefaultCurrency();
                HttpContext.Current.Session["SearchCurrency"] = lstrCurrency;
                lobjViatorProductRequest.PointRate = lobjInfiModel.GetProgramRedemptionRate(lstrCurrency, RedemptionCodeKeys.TOUR.ToString(), lobjProgramDefinition.ProgramId);
                lobjViatorProductDetailsResponse = lobjInfiModel.GetProductDetails(lobjViatorProductRequest);
                HttpContext.Current.Session[SessionFields.SelectedTourPackage] = lobjViatorProductDetailsResponse.Product;
            }
            else
            {
                LoggingAdapter.WriteLog("BindAdditinalInfo - session SelectedTourPackage is null");
                lobjViatorProductDetailsResponse = null;
            }
            return lobjViatorProductDetailsResponse;
        }
        else
        {
            LoggingAdapter.WriteLog("BindAdditinalInfo - lobjProgramDefinition is null");
            return null;
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string CheckProductAvailability(string pstrBookingDate, string pstrAdultCount, string pstrChildCount, string pstrInfantCount, string pstrSeniorCount)
    {
        try
        {
            InfiModel lobjModel = new InfiModel();
            LoggingAdapter.WriteLog("CheckProductAvailability :- BookingDate :" + pstrBookingDate + " Adult: " + pstrAdultCount + " Child : " + pstrChildCount + " Infant: " + pstrInfantCount);
            ProductDetails lobjProductDetails = new ProductDetails();
            DateTime ldtBookingDate = lobjModel.StringToDateTime(pstrBookingDate.Trim());
            LoggingAdapter.WriteLog("CheckProductAvailability ldtBookingDate :" + Convert.ToString(ldtBookingDate));
            //2016-05-04
            string _dateString = ldtBookingDate.ToString("yyyy-MM-dd");
            LoggingAdapter.WriteLog("CheckProductAvailability _dateString :" + _dateString);

            ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();
            if (lobjProgramDefinition != null)
            {
                LoggingAdapter.WriteLog("CheckProductAvailability:- Program details not null");
                if (HttpContext.Current.Session[SessionFields.SelectedTourPackage] != null)
                {
                    LoggingAdapter.WriteLog("CheckProductAvailability:- SelectedTourPackage session details not null");
                    ProductDetails lobjProduct = HttpContext.Current.Session[SessionFields.SelectedTourPackage] as ProductDetails;
                    ViatorBookingAvailabilityRequest lobjViatorBookingAvailabilityRequest = new ViatorBookingAvailabilityRequest();
                    lobjViatorBookingAvailabilityRequest.ProductCode = lobjProduct.Code;
                    lobjViatorBookingAvailabilityRequest.Month = Convert.ToString(ldtBookingDate.Month);

                    LoggingAdapter.WriteLog("CheckProductAvailability Month:-" + Convert.ToString(ldtBookingDate.Month));

                    lobjViatorBookingAvailabilityRequest.Year = Convert.ToString(ldtBookingDate.Year);
                    LoggingAdapter.WriteLog("CheckProductAvailability Year:-" + Convert.ToString(ldtBookingDate.Year));

                    lobjViatorBookingAvailabilityRequest.CurrencyCode = ConfigurationManager.AppSettings["ViatorSourceCurrency"];


                    if (Convert.ToInt16(pstrAdultCount) > 0)
                    {
                        AgeBandDetails lobjAgeBandAdultCount = new AgeBandDetails();
                        lobjAgeBandAdultCount.BandId = 1;
                        lobjAgeBandAdultCount.count = Convert.ToInt32(pstrAdultCount);
                        lobjViatorBookingAvailabilityRequest.Ageband.Add(lobjAgeBandAdultCount);
                    }
                    
                    if (Convert.ToInt32(pstrChildCount) > 0)
                    {
                        AgeBandDetails lobjAgeBandChildCount = new AgeBandDetails();
                        lobjAgeBandChildCount.BandId = 2;
                        lobjAgeBandChildCount.count = Convert.ToInt32(pstrChildCount);
                        lobjViatorBookingAvailabilityRequest.Ageband.Add(lobjAgeBandChildCount);
                    }
                    if (Convert.ToInt32(pstrInfantCount) > 0)
                    {
                        AgeBandDetails lobjAgeBandInfantCount = new AgeBandDetails();
                        lobjAgeBandInfantCount.BandId = 3;
                        lobjAgeBandInfantCount.count = Convert.ToInt32(pstrInfantCount);
                        lobjViatorBookingAvailabilityRequest.Ageband.Add(lobjAgeBandInfantCount);
                    }
                    if (Convert.ToInt32(pstrSeniorCount) > 0)
                    {
                        AgeBandDetails lobjAgeBandInfantCount = new AgeBandDetails();
                        lobjAgeBandInfantCount.BandId = 5;
                        lobjAgeBandInfantCount.count = Convert.ToInt32(pstrSeniorCount);
                        lobjViatorBookingAvailabilityRequest.Ageband.Add(lobjAgeBandInfantCount);
                    }
                    HttpContext.Current.Session[SessionFields.BookingAvailabilityRequest] = lobjViatorBookingAvailabilityRequest;
                    LoggingAdapter.WriteLog("CheckProductAvailability BookingAvailabilityRequest session:-" + JSONSerialization.Serialize(lobjViatorBookingAvailabilityRequest));

                    ViatorBookingAvailabilityResponse lobjBookingAvailabilityResponse = new ViatorBookingAvailabilityResponse();
                    lobjBookingAvailabilityResponse = lobjModel.GetBookingAvailabilityInfo(lobjViatorBookingAvailabilityRequest);
                    LoggingAdapter.WriteLog("ViewProduct errmsg1 ");

                    if (string.IsNullOrEmpty(lobjBookingAvailabilityResponse.ErrorMessage))
                    {
                        LoggingAdapter.WriteLog("ViewProduct errmsg 2 ");
                        if (lobjBookingAvailabilityResponse.BookingData.BookingAvailability != null && lobjBookingAvailabilityResponse.BookingData.BookingAvailability.Count > 0)
                        {
                            LoggingAdapter.WriteLog("CheckProductAvailability lobjBookingAvailabilityResponse not null ");

                            ViatorBookingAvailability lobjBookingAvailability = lobjBookingAvailabilityResponse.BookingData.BookingAvailability.Find(obj => obj.BookingDate.Equals(_dateString));

                            if (lobjBookingAvailability != null)
                            {
                                LoggingAdapter.WriteLog("CheckProductAvailability lobjBookingAvailability not null " + JSONSerialization.Serialize(lobjBookingAvailability));
                                HttpContext.Current.Session[SessionFields.BookingAvailabilityResponse] = lobjBookingAvailability;

                                return string.Empty;
                            }
                            else
                            {
                                LoggingAdapter.WriteLog("CheckProductAvailability date found null");
                                return "Product not available on given date";
                            }
                        }
                        else
                        {
                            LoggingAdapter.WriteLog("ViewProduct errmsg 3 ");
                            return "Product not available on given date";
                        }
                    }
                    else
                    {
                        LoggingAdapter.WriteLog("ViewProduct errmsg 4 " + lobjBookingAvailabilityResponse.ErrorMessage);
                        return lobjBookingAvailabilityResponse.ErrorMessage;
                    }
                }
                else
                {
                    LoggingAdapter.WriteLog("CheckProductAvailability SelectedTourPackage found null");
                    return string.Empty;
                }
            }
            else
            {
                LoggingAdapter.WriteLog("CheckProductAvailability Program found null");
                return string.Empty;

            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("CheckProductAvailability  :-" + ex.Message + ex.InnerException + "Stack Trace-" + ex.StackTrace);
            return string.Empty;

        }
    }


}

