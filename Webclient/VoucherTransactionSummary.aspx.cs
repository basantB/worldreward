﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Transactions.Entites;
using InfiPlanetModel.Model;
using Core.Platform.Member.Entites;
using System.Data;
using System.Configuration;

public partial class VoucherTransactionSummary : System.Web.UI.Page
{
    #region Private Properties
    private int CurrentPage
    {
        get
        {
            object objPage = ViewState["_CurrentPage"];
            int _CurrentPage = 0;
            if (objPage == null)
            {
                _CurrentPage = 0;
            }
            else
            {
                _CurrentPage = (int)objPage;
            }
            return _CurrentPage;
        }
        set { ViewState["_CurrentPage"] = value; }
    }
    private int fistIndex
    {
        get
        {

            int _FirstIndex = 0;
            if (ViewState["_FirstIndex"] == null)
            {
                _FirstIndex = 0;
            }
            else
            {
                _FirstIndex = Convert.ToInt32(ViewState["_FirstIndex"]);
            }
            return _FirstIndex;
        }
        set { ViewState["_FirstIndex"] = value; }
    }
    private int lastIndex
    {
        get
        {

            int _LastIndex = 0;
            if (ViewState["_LastIndex"] == null)
            {
                _LastIndex = 0;
            }
            else
            {
                _LastIndex = Convert.ToInt32(ViewState["_LastIndex"]);
            }
            return _LastIndex;
        }
        set { ViewState["_LastIndex"] = value; }
    }
    #endregion

    #region PagedDataSource
    PagedDataSource _PageDataSource = new PagedDataSource();
    #endregion
    int TotalPageCount = 6;
    List<TransactionDetails> lstlobjTransactionDetails = null;
    string ddlvalue = string.Empty;
    List<string> LobjTotalPage = new List<string>();
    static string lstrCurrencyName = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //stop Caching in IE
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //Stop Caching in Firefox
        Response.Cache.SetNoStore();
        if (Session["MemberDetails"] == null)
        {
            Response.Redirect("Login.aspx");
        }
        if (!Page.IsPostBack)
        {
            try
            {
                Voucher.Visible = false;
                List<ProgramCurrencyDefinition> lstlobjProgramCurrencyDefinition = GetProgramCurrency();
                if (lstlobjProgramCurrencyDefinition != null)
                {
                    if (lstlobjProgramCurrencyDefinition.Count > 0)
                    {
                        //lblNoResult.Text = string.Empty;
                        lstrCurrencyName = lstlobjProgramCurrencyDefinition[0].Currency;
                        //BindDataRangeFilter(lstrCurrencyName);
                        //BindTransactionSummaryInfo(lstrCurrencyName);

                        GetVoucherSummary();
                        divVoucherSummary.Attributes.Add("style", "display:block;");
                        //divVoucherSummary.Visible = true;
                        //divVouchers.Visible = false;
                        //divVouchers.Attributes.Add("style", "display:none;");
                    }
                }
            }
            catch (Exception ex)
            {
                Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog(ex.Message);
            }
        }
    }

    public void GetVoucherSummary()
    {

        InfiModel lobjInfiModel = new InfiModel();
        ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramMaster();
        SearchTransactions lobjSearchTransactions = new SearchTransactions();
        lobjSearchTransactions.RelationReference = ((Session["MemberDetails"] as MemberDetails).MemberRelationsList.Find(obj => obj.RelationType.Equals(RelationType.LBMS)).RelationReference);
        lobjSearchTransactions.ProgramId = lobjProgramDefinition.ProgramId;
        lobjSearchTransactions.RelationType = Convert.ToInt32(RelationType.Voucher);
        // lobjSearchTransactions.TransactionCurrency = pstrCurrencyName;
        //lobjSearchTransactions.MinimumRange = Convert.ToInt32(ddlvalue.ToString().Split('-')[0]);
        //lobjSearchTransactions.MaximumRange = Convert.ToInt32(ddlvalue.ToString().Split('-')[1]);

        // lobjTransactionDetails = lobjCBQModel.GetVoucherSummary(5, "CBQREWARDS", "123456789");
        lstlobjTransactionDetails = lobjInfiModel.GetVoucherSummary(lobjSearchTransactions.ProgramId, lobjSearchTransactions.RelationReference, lobjSearchTransactions.RelationType);
        if (lstlobjTransactionDetails != null)
        {
            Session["TransactionDetails"] = lstlobjTransactionDetails;
            if (lstlobjTransactionDetails.Count == 0)
            {
                lblResult.Text = "No Record found";
                divVoucherSummary.Visible = false;
            }
            else
            {
                lblResult.Text = "";
                divVouchers.Attributes.Add("style", "display:block;");
            }
            List<TransactionDetails> lobjCreditTransactionDetails = null;
            lobjCreditTransactionDetails = lstlobjTransactionDetails.FindAll(lobj => lobj.TransactionType.Equals(TransactionType.Credit));
            List<TransactionDetails> lobjDistinctCreditTransactionDetails = null;
            lobjDistinctCreditTransactionDetails = lobjCreditTransactionDetails.Distinct().ToList();
            if (lobjCreditTransactionDetails.Count == 0)
                lblResult.Text = "No Record found";
            else
            {
                lblResult.Text = "";
                divVouchers.Attributes.Add("style", "display:block;");
            }
            RepVoucherSummaryInfo.DataSource = lobjDistinctCreditTransactionDetails;
            RepVoucherSummaryInfo.DataBind();

            int lintTransactioncount;
            lintTransactioncount = lobjDistinctCreditTransactionDetails.Count;
            if (lintTransactioncount > 10)
            {
                RepeaterPaging.Visible = true;
                PagedDataSource pagedData = new PagedDataSource();
                pagedData.DataSource = lobjDistinctCreditTransactionDetails;
                pagedData.AllowPaging = true;
                pagedData.PageSize = 10;
                ViewState["TotalPages"] = pagedData.PageCount;
                pagedData.CurrentPageIndex = CurrentPage;
                int vcnt = lintTransactioncount / pagedData.PageSize;
                if (CurrentPage < 1)
                    linkbuttonPrevious.Visible = false;
                else if (CurrentPage > 0)
                    linkbuttonPrevious.Visible = true;
                if (CurrentPage == vcnt)
                    linkbuttonNext.Visible = false;
                else if (CurrentPage < vcnt)
                    linkbuttonNext.Visible = true;
                RepVoucherSummaryInfo.DataSource = pagedData;
                RepVoucherSummaryInfo.DataBind();
                this.doPaging();
            }
        }
        else
        {
            lblResult.Text = "No Records found ";
        }

        Voucher.Visible = false;
        Fade.Attributes.Add("style", "display:none");

    }

    private List<ProgramCurrencyDefinition> GetProgramCurrency()
    {
        InfiModel lobjModel = new InfiModel();
        ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();
        MemberDetails lobjMemberDetails = null;
        lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
        List<ProgramCurrencyDefinition> lobjlistProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
        lobjlistProgramCurrencyDefinition = lobjModel.GetProductProgramCurrencyDefinition(lobjMemberDetails);
        return lobjlistProgramCurrencyDefinition;
    }

    protected void ddlProgramCurrency_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void lnkVoucherSummary_Click(object sender, EventArgs e)
    {
    }

    protected void lnkVoucherCode_Click(object sender, EventArgs e)
    {
        divVouchers.Attributes.Add("style", "display:block;");
        LinkButton lnk = (LinkButton)sender;
        var VoucherCode = lnk.CommandArgument;
        Session["VoucherCode"] = VoucherCode;
        List<TransactionDetails> lobjListAllTransactionDetails = Session["TransactionDetails"] as List<TransactionDetails>;
        if (lobjListAllTransactionDetails != null)
        {
            List<TransactionDetails> lobjListTransactionDetails = null;
            lobjListTransactionDetails = lobjListAllTransactionDetails.FindAll(lobj => lobj.RelationReference.Equals(VoucherCode) && lobj.TransactionType.Equals(TransactionType.Debit));

            TransactionDetails lobjCreditTransactionDetails = new TransactionDetails();
            lobjCreditTransactionDetails = lobjListAllTransactionDetails.Find(lobj => lobj.RelationReference.Equals(VoucherCode) && lobj.TransactionType.Equals(TransactionType.Credit));
            lobjCreditTransactionDetails.AdditionalDetails1 = Convert.ToString(lobjCreditTransactionDetails.Amounts);

            int lintCreditAmount = Convert.ToInt32(lobjCreditTransactionDetails.Amounts);
            float lintDebitAmount = 0;

            for (int i = lobjListTransactionDetails.Count - 1; i >= 0; i--)
            {
                lintDebitAmount = lintCreditAmount - lobjListTransactionDetails[i].Amounts;
                lintCreditAmount = Convert.ToInt32(lintDebitAmount);
                lobjListTransactionDetails[i].AdditionalDetails1 = Convert.ToString(lintDebitAmount);
            }

            string lstrVoucherPath = ConfigurationSettings.AppSettings["VoucherURL"].ToString();
            imgVoucher.Src = lstrVoucherPath + VoucherCode + ".png";
            
            //if (lobjListTransactionDetails.Count != 0)
            //{
            lobjListTransactionDetails.Add(lobjCreditTransactionDetails);
            List<TransactionDetails> lobjListTransactionDetailsOrdered = new List<TransactionDetails>();
            lobjListTransactionDetailsOrdered = lobjListTransactionDetails.FindAll(lobj => lobj.RelationReference.Equals(VoucherCode)).OrderByDescending(lobj => lobj.ProcessingDate).ToList();

            RptVoucherDetails.DataSource = lobjListTransactionDetailsOrdered;
            RptVoucherDetails.DataBind();

            //Voucher.CssClass = "black_overlay";
            Voucher.Visible = true;
            Fade.Attributes.Add("style", "display:block");
            if (lobjListTransactionDetails.Count == 0)
                lblErrorMsg.Visible = true;
            else
                lblErrorMsg.Visible = false;
            //}
            //else
            //{
            //    Voucher.CssClass = "hidecontent";
            //    Fade.Attributes.Add("style", "display:none");
            //}
        }
        else
        {
            lblErrorMsg.Visible = true;
        }

    }

    protected void RepVoucherSummaryInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        List<TransactionDetails> lobjAllTransactionDetails = Session["TransactionDetails"] as List<TransactionDetails>;
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            return;
        Label lblStatus = (Label)e.Item.FindControl("lblStatus");
        Label lblSrNo = (Label)e.Item.FindControl("lblSrNo");
        Label lblDateOfIssue = (Label)e.Item.FindControl("lblDateOfIssue");
        Label lblExpiryDate = (Label)e.Item.FindControl("lblExpiryDate");
        LinkButton lnkVoucherCode = (LinkButton)e.Item.FindControl("lnkVoucherCode");
        // ImageButton IMgBtn = (ImageButton)e.Item.FindControl("ImageButtonPrint");
        List<TransactionDetails> lobjCreditTransactionDetails = lobjAllTransactionDetails.FindAll(lobj => lobj.TransactionType.Equals(TransactionType.Credit) && lobj.RelationReference.Equals(lnkVoucherCode.Text));
        List<TransactionDetails> lobjDebitTransactionDetails = lobjAllTransactionDetails.FindAll(lobj => lobj.TransactionType.Equals(TransactionType.Debit) && lobj.RelationReference.Equals(lnkVoucherCode.Text));
        float lfltRemainValue = (lobjCreditTransactionDetails.Sum(lobjCR => lobjCR.Amounts) + lobjCreditTransactionDetails.Sum(lobjCR => lobjCR.TransactionDetailBreakage.SpoilagePoints)) - (lobjDebitTransactionDetails.Sum(lobjDR => lobjDR.Amounts) + lobjDebitTransactionDetails.Sum(lobjDR => lobjDR.TransactionDetailBreakage.SpoilagePoints));

        lblSrNo.Text = (Convert.ToInt32(HFSrno.Value) + Convert.ToInt32(lblSrNo.Text)).ToString();
        if (lfltRemainValue <= 0)
        {
            lblStatus.Text = "Utilized";
            // IMgBtn.Visible = false;
        }
        else if (lfltRemainValue > 0)
        {
            InfiModel lobjInfiModel = new InfiModel();
            // if (Convert.ToDateTime(lblExpiryDate.Text) <= Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy")))
            if ((Convert.ToDateTime(lobjInfiModel.StringToDateTime(lblExpiryDate.Text)) < Convert.ToDateTime(lobjInfiModel.StringToDateTime(DateTime.Now.ToString("dd/MM/yyyy")))))
            {
                lblStatus.Text = "Expired";
                // IMgBtn.Visible = false;
            }
            else
            {
                lblStatus.Text = "Active";
                //IMgBtn.Visible = true;
            }
        }

    }

    protected void linkbuttonFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        GetVoucherSummary();
    }

    protected void linkbuttonPrevious_Click(object sender, EventArgs e)
    {
        CurrentPage -= 1;
        GetVoucherSummary();
    }

    protected void linkbuttonNext_Click(object sender, EventArgs e)
    {
        CurrentPage += 1;
        GetVoucherSummary();
    }


    protected void linkbuttonLast_Click(object sender, EventArgs e)
    {
        CurrentPage = Convert.ToInt32(ViewState["TotalPages"]) - 1; ;
        GetVoucherSummary();
    }


    protected void dlPaging_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName.Equals("Paging"))
        {
            CurrentPage = Convert.ToInt32(e.CommandArgument.ToString());
            HFSrno.Value = Convert.ToString(CurrentPage * 10);
            this.GetVoucherSummary();

        }
    }

    protected void dlPaging_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        LinkButton lnkbtnPage = (LinkButton)e.Item.FindControl("lnkbtnPaging");
        if (lnkbtnPage.CommandArgument.ToString() == CurrentPage.ToString())
        {
            lnkbtnPage.Enabled = false;
            lnkbtnPage.Style.Add("fone-size", "14px");
            lnkbtnPage.Font.Bold = true;

        }
    }

    protected void RptVoucherDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        {
            if (e.Item.ItemType != ListItemType.Footer)
            {
                imgStatus.Src = "~/Images/Voucher_Active.jpg";
            }
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            InfiModel lobjInfiModel = new InfiModel();
            Label lblStatusDetails = (Label)e.Item.FindControl("lblStatusDetails");
            //HiddenField HFExpiryDate = (HiddenField)e.Item.FindControl("HFExpiryDate");
            Label lblVoucherCode = (Label)e.Item.FindControl("lblVoucherCode");
            Label lblRemainingValue = (Label)e.Item.FindControl("lblRemainingValue");
            Label lblTranType = (Label)e.Item.FindControl("lblTranType");

            List<TransactionDetails> lobjAllTransactionDetails = Session["TransactionDetails"] as List<TransactionDetails>;
            List<TransactionDetails> lobjCreditTransactionDetails = lobjAllTransactionDetails.FindAll(lobj => lobj.TransactionType.Equals(TransactionType.Credit) && lobj.RelationReference.Equals(lblVoucherCode.Text));
            List<TransactionDetails> lobjDebitTransactionDetails = lobjAllTransactionDetails.FindAll(lobj => lobj.TransactionType.Equals(TransactionType.Debit) && lobj.RelationReference.Equals(lblVoucherCode.Text));
            float lfltRemainValue = (lobjCreditTransactionDetails.Sum(lobjCR => lobjCR.Amounts) + lobjCreditTransactionDetails.Sum(lobjCR => lobjCR.TransactionDetailBreakage.SpoilagePoints)) - (lobjDebitTransactionDetails.Sum(lobjDR => lobjDR.Amounts) + lobjDebitTransactionDetails.Sum(lobjDR => lobjDR.TransactionDetailBreakage.SpoilagePoints));
            //lblRemainingValue.Text = Convert.ToString(lfltRemainValue);
            lblTranType.Text = "REWARDZ" + lobjCreditTransactionDetails[0].Amounts + " Gift Voucher";

            if (lfltRemainValue <= 0)
            {
                lblStatusDetails.Text = "Utilized";
                imgStatus.Src = "~/Images/Voucher_Utilized.jpg";
            }
            else if (lfltRemainValue > 0)
            {
                //if (Convert.ToDateTime(HFExpiryDate.Value) <= Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy")))
                //if ((Convert.ToDateTime(lobjCBQModel.StringToDateTime(HFExpiryDate.Value)) < Convert.ToDateTime(lobjCBQModel.StringToDateTime(DateTime.Now.ToString("dd/MM/yyyy")))))
                if ((lobjCreditTransactionDetails[0].ExpiryDate < Convert.ToDateTime(lobjInfiModel.StringToDateTime(DateTime.Now.ToString("dd/MM/yyyy")))))
                {
                    lblStatusDetails.Text = "Expired";
                    imgStatus.Src = "~/Images/Voucher_Expired.jpg";
                }
                else
                {
                    lblStatusDetails.Text = "Active";
                    imgStatus.Src = "~/Images/Voucher_Active.jpg";
                }
            }
        }

    }

    private void doPaging()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("PageIndex");
        dt.Columns.Add("PageText");
        PagedDataSource objpagedData = new PagedDataSource();
        objpagedData = ViewState["TotalPages"] as PagedDataSource;

        fistIndex = CurrentPage - 5;
        if (CurrentPage > 5)
        {
            lastIndex = CurrentPage + 5;
        }
        else
        {
            lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
        }
        if (lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
        {
            lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
            fistIndex = lastIndex - lastIndex;
        }

        if (fistIndex < 0)
        {
            fistIndex = 0;
        }
        for (int i = fistIndex; i < lastIndex; i++)
        {
            DataRow dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }
        this.dlPaging.DataSource = dt;
        this.dlPaging.DataBind();
    }
}