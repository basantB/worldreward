﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Core.Platform.OTP.Entities;
using Core.Platform.Member.Entites;
using InfiPlanetModel.Model;
using Framework.EnterpriseLibrary.Adapters;

public partial class ValidateOTP : System.Web.UI.Page
{
    string flag = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Panel breadCrumbDiv = (Panel)Master.FindControl("breadCrumbDiv");
        //breadCrumbDiv.Visible = false;
        if (!IsPostBack)
        {
            Session["OTPCount"] = 0;
            if (Session["MemberDetails"] == null)
            {

                Response.Redirect("Login.aspx");
            }

        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string CheckOTP(string pstrOTP, string strFlag)
    {
        int lstrOTPCount = Convert.ToInt32(HttpContext.Current.Session["OTPCount"]);
        lstrOTPCount += 1;
        HttpContext.Current.Session["OTPCount"] = lstrOTPCount;
        InfiModel lobjModel = new InfiModel();
        OTPDetails lobjOTPDetails = new OTPDetails();
        MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
        lobjOTPDetails.UniquerefID = lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
        lobjOTPDetails.OTP = Convert.ToInt32(pstrOTP);
        if (strFlag == "Air")
        {
            lobjOTPDetails.OtpType = OTPEnumTypes.AIRREVIEWNCONFIRM.ToString();
            lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.AIRREVIEWNCONFIRM;
        }
        else if (strFlag == "Hotel")
        {
            lobjOTPDetails.OtpType = OTPEnumTypes.HOTELREVIEWNCONFIRM.ToString();
            lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.HOTELREVIEWNCONFIRM;
        }
        else if (strFlag == "Car")
        {
            lobjOTPDetails.OtpType = OTPEnumTypes.CARREVIEWNCONFIRM.ToString();
            lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.CARREVIEWNCONFIRM;
        }
        else if (strFlag == "Cashback")
        {
            lobjOTPDetails.OtpType = OTPEnumTypes.CASHBACKCONFIRM.ToString();
            lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.CASHBACKCONFIRM;
        }

        bool Status = lobjModel.CheckWhetherOTPExists(lobjOTPDetails);

        if (lstrOTPCount < 5)
        {
            if (Status)
            {
                return strFlag;
                LoggingAdapter.WriteLog("OTP:" + strFlag);
            }
            else
            {
                strFlag = "Invalid Credentials";
                return strFlag;
                LoggingAdapter.WriteLog("OTP:" + strFlag);
            }
        }
        else
        {
            strFlag = "Exceed OTP limit";
            HttpContext.Current.Session.Abandon();
            return strFlag;
            LoggingAdapter.WriteLog("OTP:" + strFlag);
        }
    }
}