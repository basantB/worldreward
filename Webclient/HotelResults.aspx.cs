﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CB.IBE.Platform.Hotels.ClientEntities;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using Core.Platform.Member.Entites;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using Framework.Integrations.Hotels.Entities;
using CB.IBE.Platform.Masters.Entities;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.MemberActivity.Entities;
using InfiPlanetModel.Model;

public partial class HotelResults : System.Web.UI.Page
{
    private FilterRange lobjFilterRange = new FilterRange();
    private List<int> lobjListOfPrice = new List<int>();
    private List<string> lobjListOfChain = new List<string>();
    private List<string> lobjListOfProertyType = new List<string>();
    private List<string> lobjListOfLocations = new List<string>();
    private List<string> lobjListOfHotelAmenities = new List<string>();
    private List<string> lobjListOfBasicAmenities = new List<string>();
    private List<string> lobjListOfBussinessService = new List<string>();
    private List<string> lobjListOfRoomAmenities = new List<string>();
    private HotelInfoRequest lobjhotelinforequest = new HotelInfoRequest();
    private HotelInfo lobjHotelInfo = new HotelInfo();
    public int count = 0;
    public int countJson = 0;
    public string lstrCurrency = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["SearchDetails"] != null)
            {
                HotelSearchRequest lobjSearchRequest = Session["SearchDetails"] as HotelSearchRequest;
                if (lobjSearchRequest != null)
                {


                    MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;

                    InfiModel lobjModel = new InfiModel();
                    string lstrRelativePath = this.Page.AppRelativeVirtualPath;
                    int lintActivityId = lobjModel.LogActivity(lstrRelativePath.Replace("~", string.Empty).Replace("/", string.Empty), ActivityType.PageLoad);

                    //txtCity.Text = lobjSearchRequest.SearchRequest.CountryISOCode.ToString() + ", " + lobjSearchRequest.SearchRequest.Country.ToString() + ", " + lobjSearchRequest.SearchRequest.CityName.ToString();///changed bymuds
                    //TextBoxCheckin.Text = lobjSearchRequest.SearchRequest.CheckInDate.ToString("dd/MM/yyyy");
                    ////string ChkinDate = SetviewDate(lobjSearchRequest.SearchRequest.CheckInDate);
                    ////TextBoxCheckin.Text = ChkinDate;
                    //TextBoxCheckout.Text = lobjSearchRequest.SearchRequest.CheckOutDate.ToString("dd/MM/yyyy");
                    //string ChkOutDate = SetviewDate(lobjSearchRequest.SearchRequest.CheckOutDate);
                    //TextBoxCheckout.Text = ChkOutDate;
                }

                hdnPaymentType.Value = "Points";
            }
            SetHotelTemplate();
        }
    }
    public void SetHotelTemplate()
    {
        if (Session["Hotels"] != null)
        {
            HotelSearchResponse lobjSearchResponse = Session["Hotels"] as HotelSearchResponse;
            HotelSearchRequest lobjSearchRequest = Session["SearchDetails"] as HotelSearchRequest;

            if (hdnPaymentType.Value.Equals(Convert.ToString(PaymentType.Points)))
            {
                //FilterPoints.InnerHtml = "By Points";
                //DivSorterForPoints.Style.Add("display", "block");
                //DivSorterForCash.Style.Add("display", "none");
            }
            else
            {
                //FilterPoints.InnerHtml = "By BHD";
                //DivSorterForPoints.Style.Add("display", "none");
                //DivSorterForCash.Style.Add("display", "block");
            }

            if (lobjSearchResponse != null)
            {
                lobjSearchResponse.SearchResponse.hotels.hotel = lobjSearchResponse.SearchResponse.hotels.hotel.OrderBy(lobj => lobj.roomrates.RoomRate[0].TotalPoints).ToArray<Hotel>();
                rptHotelList.DataSource = lobjSearchResponse.SearchResponse.hotels.hotel;
                rptHotelList.DataBind();
            }

            string ChkinDate = SetviewDate(lobjSearchRequest.SearchRequest.CheckInDate);
            string ChkOutDate = SetviewDate(lobjSearchRequest.SearchRequest.CheckOutDate);
            SearchSummary.InnerHtml = "<li class='frt'><div class='us-from'><p>Hotels in</p><h1>" + lobjSearchResponse.SearchResponse.searchcriteria.city + "</h1></div><div class='clr'></div><p class='us-date'>Total Hotel(s) Found:" + lobjSearchResponse.SearchResponse.hotels.hotel.Count() + "</p></li> ";
            SearchSummary.InnerHtml += "<li><div class='us-from'>" + ChkinDate + "</div><div class='us-arr'><img src='images/arr-rght.png'></div><div class='us-to'>" + ChkOutDate + "</div><div class='clr'></div></li>";
            SearchSummary.InnerHtml += "<li class='lst'><h1>" + lobjSearchRequest.SearchRequest.NoOfRooms.ToString() + " Room(s)</h1>";

            mobSearchSummary.InnerHtml = "<li><ul><li>Hotels in<h2>" + lobjSearchResponse.SearchResponse.searchcriteria.city + "</h2></li><li>Found:" + lobjSearchResponse.SearchResponse.hotels.hotel.Count() + " Hotel(s)<h2>" + lobjSearchRequest.SearchRequest.NoOfRooms.ToString() + " Room(s)</h2></li></ul></li>";
            mobSearchSummary.InnerHtml += "<li><b>From: </b>" + lobjSearchRequest.SearchRequest.CheckInDate.ToString("dd MMM, yyyy") + "<b> - To: </b> " + lobjSearchRequest.SearchRequest.CheckOutDate.ToString("dd MMM, yyyy") + "</li>";

            HFNoOfRooms.Value = lobjSearchRequest.SearchRequest.NoOfRooms.ToString();
            hdnNoAdult.Value = lobjSearchRequest.SearchRequest.AdultPerRoom.ToString();
            hdnNoChild.Value = lobjSearchRequest.SearchRequest.ChildrenPerRoom.ToString();

            for (int i = 0; i < lobjSearchResponse.SearchResponse.hotels.hotel.Count(); i++)
            {
                lobjListOfChain.Add(lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.chain);
                lobjListOfLocations.Add(lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.locality);
                if (lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities != null)
                {
                    for (int k = 0; k < lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities.Count(); k++)
                    {
                        for (int l = 0; l < lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities[k].Amenities.hotelamenity.Count(); l++)
                        {
                            if (lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities[k].category.Equals("Basics"))
                                lobjListOfBasicAmenities.Add(lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities[k].Amenities.hotelamenity[l].Value);
                            else if (lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities[k].category.Equals("Hotel Amenities"))
                                lobjListOfHotelAmenities.Add(lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities[k].Amenities.hotelamenity[l].Value);
                            else if (lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities[k].category.Equals("Business Services"))
                                lobjListOfBussinessService.Add(lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities[k].Amenities.hotelamenity[l].Value);
                            else if (lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities[k].category.Equals("Room Amenities"))
                                lobjListOfRoomAmenities.Add(lobjSearchResponse.SearchResponse.hotels.hotel[i].basicinfo.hotelamenities.Amenities[k].Amenities.hotelamenity[l].Value);
                        }
                    }
                }

                lobjhotelinforequest.hotelid = Convert.ToInt32(lobjSearchResponse.SearchResponse.hotels.hotel[i].hotelid);
                if (lobjSearchResponse.SearchResponse.hotels.hotel[i].roomrates.RoomRate != null && lobjSearchResponse.SearchResponse.hotels.hotel[i].roomrates.RoomRate.Count() >= 0)
                {
                    lobjSearchRequest.PaymentType = PaymentType.Points;
                    if (lobjSearchRequest.PaymentType.Equals(PaymentType.Points))
                        if (hdnPaymentType.Value.Equals(Convert.ToString(PaymentType.Points)))
                        {
                            lobjListOfPrice.Add(Convert.ToInt32(Convert.ToSingle(lobjSearchResponse.SearchResponse.hotels.hotel[i].roomrates.RoomRate[0].TotalPoints)));
                        }
                        else
                        {
                            lobjListOfPrice.Add(Convert.ToInt32(Convert.ToSingle(lobjSearchResponse.SearchResponse.hotels.hotel[i].roomrates.RoomRate[0].TotalBaseAmount)));
                        }

                }
                lobjFilterRange.MaxPrice = lobjListOfPrice.Max();
                lobjFilterRange.MinPrice = lobjListOfPrice.Min();
                lobjListOfLocations = lobjListOfLocations.Distinct().ToList();
                lobjListOfChain = lobjListOfChain.SkipWhile(x => string.IsNullOrEmpty(x)).ToList();
                lobjFilterRange.ListOfHotelChain = lobjListOfChain.Distinct().ToList();
                lobjFilterRange.ListOfLocation = lobjListOfLocations;
                lobjFilterRange.ListOfBasicAmenities = lobjListOfBasicAmenities.Distinct().ToList();
                lobjFilterRange.ListOfBussinessAmenities = lobjListOfBussinessService.Distinct().ToList();
                lobjFilterRange.ListOfHotelAmenities = lobjListOfHotelAmenities.Distinct().ToList();
                lobjFilterRange.ListOfRoomAmenities = lobjListOfRoomAmenities.Distinct().ToList();
                hdnHotelFilterRange.Value = JSONSerialization.Serialize(lobjFilterRange);

            }
        }
        else
        {
            Response.Redirect("index.aspx");
        }
    }
    private string SetviewDate(DateTime dateTime)
    {
        string CalendarDate;
        string Day = dateTime.ToString("dddd");
        string NowDate = dateTime.ToString("dd");
        string Month = dateTime.ToString("MMMM");
        CalendarDate = "<p>" + Month + "</p><h1>" + NowDate + "</h1><p>" + Day + "</p>";
        return CalendarDate;
    }
    protected void rptHotelList_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        InfiModel lobjModel = new InfiModel();
        if (Session["hotels"] != null)
        {
            HotelSearchResponse lobjSearchResponse = Session["hotels"] as HotelSearchResponse;
            HotelSearchRequest lobjSearchRequest = Session["SearchDetails"] as HotelSearchRequest;
            if (lobjSearchResponse != null)
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (hdnPaymentType.Value.Equals(Convert.ToString(PaymentType.Points)))
                    {
                        (e.Item.FindControl("lblNoOfNights") as Label).Text = lobjSearchResponse.SearchResponse.searchcriteria.numberofnights;
                        (e.Item.FindControl("lblmiles") as Label).Text = lobjModel.IntToThousandSeperated(lobjSearchResponse.SearchResponse.hotels.hotel[e.Item.ItemIndex].roomrates.RoomRate[0].TotalPoints);
                    }
                }
            }
            if (lobjSearchResponse != null)
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (countJson <= lobjSearchResponse.SearchResponse.hotels.hotel.Count())
                    {
                        (e.Item.FindControl("hdnHotelJsonData") as HiddenField).Value = JSONSerialization.Serialize(lobjSearchResponse.SearchResponse.hotels.hotel[countJson]);

                        //Amenities Images
                        if (lobjSearchResponse.SearchResponse.hotels.hotel[countJson].basicinfo.hotelamenities.Amenities != null)
                        {
                            if (lobjSearchResponse.SearchResponse.hotels.hotel[countJson].basicinfo.hotelratings.HotelRating != null && lobjSearchResponse.SearchResponse.hotels.hotel[countJson].basicinfo.hotelratings.HotelRating.Count() >= 0 && lobjSearchResponse.SearchResponse.hotels.hotel[countJson].basicinfo.hotelratings.HotelRating[0] != null)
                            {
                                if (Convert.ToString(lobjSearchResponse.SearchResponse.hotels.hotel[countJson].basicinfo.hotelratings.HotelRating[0].rating).Equals("5"))
                                {
                                    (e.Item.FindControl("imgMeeting") as Image).ImageUrl = "Images/ico-meet.png";
                                    (e.Item.FindControl("imgMeeting") as Image).ToolTip = "Meeting Facilities";

                                    (e.Item.FindControl("imgGym") as Image).ImageUrl = "Images/ico-gym.png";
                                    (e.Item.FindControl("imgGym") as Image).ToolTip = "Gym/Spa";

                                    (e.Item.FindControl("imgInternet") as Image).ImageUrl = "Images/ico-wifi.png";
                                    (e.Item.FindControl("imgInternet") as Image).ToolTip = "Internet/Wi-Fi";

                                    (e.Item.FindControl("imgResturent") as Image).ImageUrl = "Images/ico-restnt.png";
                                    (e.Item.FindControl("imgResturent") as Image).ToolTip = "Restaurant/Coffee Shop";

                                    (e.Item.FindControl("imgswimmingPool") as Image).ImageUrl = "ico-swim.png";
                                    (e.Item.FindControl("imgswimmingPool") as Image).ToolTip = "Swimming Pool";
                                }
                                else
                                {
                                    List<HotelAmenity> lobjHotelAmenityList = lobjSearchResponse.SearchResponse.hotels.hotel[countJson].basicinfo.hotelamenities.Amenities.ToList<HotelAmenity>();
                                    (e.Item.FindControl("imgMeeting") as Image).ImageUrl = (lobjHotelAmenityList.Find(lobj => lobj.Amenities.hotelamenity.ToList<Amenity>().Find(lobjAmenity => lobjAmenity.Value.ToLower().IndexOf("business center") >= 0 || lobjAmenity.Value.ToLower().IndexOf("meeting rooms") >= 0 || lobjAmenity.Value.ToLower().IndexOf("meeting facilities") >= 0) != null)) != null ? "Images/ico-meet.png" : "Images/ico-meet-dsable.png";
                                    if ((e.Item.FindControl("imgMeeting") as Image).ImageUrl == "Images/ico-meet.png")
                                    {
                                        (e.Item.FindControl("imgMeeting") as Image).ToolTip = "Meeting Facilities";

                                    }
                                    (e.Item.FindControl("imgGym") as Image).ImageUrl = (lobjHotelAmenityList.Find(lobj => lobj.Amenities.hotelamenity.ToList<Amenity>().Find(lobjAmenity => lobjAmenity.Value.ToLower().IndexOf("gym") >= 0 || lobjAmenity.Value.ToLower().IndexOf("spa") >= 0 || lobjAmenity.Value.ToLower().IndexOf("fitness center") >= 0 || lobjAmenity.Value.ToLower().IndexOf("health club") >= 0) != null)) != null ? "Images/ico-gym.png" : "Images/ico-gym-dsable.png";
                                    if ((e.Item.FindControl("imgGym") as Image).ImageUrl == "Images/ico-gym.png")
                                    {
                                        (e.Item.FindControl("imgGym") as Image).ToolTip = "Gym/Spa";
                                    }

                                    (e.Item.FindControl("imgInternet") as Image).ImageUrl = (lobjHotelAmenityList.Find(lobj => lobj.Amenities.hotelamenity.ToList<Amenity>().Find(lobjAmenity => lobjAmenity.Value.ToLower().IndexOf("internet") >= 0 || lobjAmenity.Value.ToLower().IndexOf("wi-fi") >= 0) != null)) != null ? "Images/ico-wifi.png" : "Images/ico-wifi-dsable.png";
                                    if ((e.Item.FindControl("imgInternet") as Image).ImageUrl == "Images/ico-wifi.png")
                                    {
                                        (e.Item.FindControl("imgInternet") as Image).ToolTip = "Internet/Wi-Fi";
                                    }
                                    (e.Item.FindControl("imgResturent") as Image).ImageUrl = (lobjHotelAmenityList.Find(lobj => lobj.Amenities.hotelamenity.ToList<Amenity>().Find(lobjAmenity => lobjAmenity.Value.ToLower().IndexOf("restau") >= 0 || lobjAmenity.Value.ToLower().IndexOf("coffee shop") >= 0) != null)) != null ? "Images/ico-restnt.png" : "Images/ico-restnt-dsable.png";
                                    if ((e.Item.FindControl("imgResturent") as Image).ImageUrl == "Images/ico-restnt.png")
                                    {
                                        (e.Item.FindControl("imgResturent") as Image).ToolTip = "Restaurant/Coffee Shop";
                                    }
                                    (e.Item.FindControl("imgswimmingPool") as Image).ImageUrl = (lobjHotelAmenityList.Find(lobj => lobj.Amenities.hotelamenity.ToList<Amenity>().Find(lobjAmenity => lobjAmenity.Value.ToLower().IndexOf("pool") >= 0) != null)) != null ? "Images/ico-swim.png" : "Images/ico-swim-dsable.png";
                                    if ((e.Item.FindControl("imgswimmingPool") as Image).ImageUrl == "Images/ico-swim.png")
                                    {
                                        (e.Item.FindControl("imgswimmingPool") as Image).ToolTip = "Swimming Pool";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (lobjSearchResponse != null)
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (countJson <= lobjSearchResponse.SearchResponse.hotels.hotel.Count())
                    {
                        (e.Item.FindControl("hdnHotelJsonData") as HiddenField).Value = JSONSerialization.Serialize(lobjSearchResponse.SearchResponse.hotels.hotel[countJson]);
                        countJson++;
                    }
                }
            }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool SetHotelId(string pstrHotelId)
    {
        InfiModel lobjInfiModel = new InfiModel();
        lobjInfiModel.LogActivity(string.Format("Hotel selected for booking"), ActivityType.HotelBooking);
        HttpContext.Current.Session["hotelId"] = pstrHotelId;
        return true;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SetModifyDetails()
    {
        string[] strres = new string[4];
        if (HttpContext.Current.Session["SearchDetails"] != null)
        {
            HotelSearchRequest lobjSearchRequest = HttpContext.Current.Session["SearchDetails"] as HotelSearchRequest;
            strres[0] = lobjSearchRequest.SearchRequest.CountryISOCode.ToString() + ", " + lobjSearchRequest.SearchRequest.Country.ToString() + ", " + lobjSearchRequest.SearchRequest.CityName.ToString();
            strres[1] = lobjSearchRequest.SearchRequest.CheckInDate.ToString("dd/MM/yyyy");
            strres[2] = lobjSearchRequest.SearchRequest.CheckOutDate.ToString("dd/MM/yyyy");
            strres[3] = lobjSearchRequest.SearchRequest.NoOfRooms.ToString();
        }
        else
        {
            strres = null;
        }
        return strres;
    }
}