﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfiPlanetModel.Model;
using Core.Platform.Member.Entites;
using CB.IBE.Platform.Masters.Entities;
using CB.IBE.Platform.Entities;
using CB.IBE.Platform.Hotels.ClientEntities;
using CB.IBE.Platform.Car.ClientEntities;
using CB.IBE.Platform.ClientEntities;
using CB.IBE.Platform.Car.Entities;
using Viator.Platform.MasterEntities;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using Viator.Platform.ClientEntities;
using InfiPlanet.SessionConstants;
public partial class ManageBooking : System.Web.UI.Page
{
    InfiModel lobjModel = new InfiModel();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["MemberDetails"] != null)
        {
            MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
            RefererDetails lobjRefererData = Application["RefererData"] as RefererDetails;
            BindFlightBooking(lobjMemberDetails);
            BindHotelBooking(lobjMemberDetails);
            BindCarBooking(lobjMemberDetails, lobjRefererData.Id);
            BindPackageBookingDetails(lobjMemberDetails);
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    private void BindPackageBookingDetails(MemberDetails pobjMemberDetails)
    {
        try
        {
            Session["TravelBookingList"] = null;
            LoggingAdapter.WriteLog("Managebooking pobjMemberDetails :-" + JSONSerialization.Serialize(pobjMemberDetails));
            LoggingAdapter.WriteLog("Managebooking call:- BindPackageBookingDetails");

            if (Session["MemberDetails"] != null)
            {

                LoggingAdapter.WriteLog("Managebooking call:- Member session not null ");
                List<TravelBookingDetails> lobjTravelBookingList = lobjModel.GetPackageBookingDetails(pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
                if (lobjTravelBookingList != null && lobjTravelBookingList.Count > 0)
                {
                    //LoggingAdapter.WriteLog("Managebooking lobjTravelBookingList not null " + JSONSerialization.Serialize(lobjTravelBookingList));
                    Session["TravelBookingList"] = lobjTravelBookingList;
                    rptPackageBookingDetails.DataSource = lobjTravelBookingList;
                    //divrptflight.Attributes.Add("style", "Display:block");
                }
                else
                {
                    rptPackageBookingDetails.DataSource = null;
                    lblPackagerecord.Visible = true;
                    lblPackagerecord.Text = "No Records Found.";
                    // divFlight.Visible = false;
                    divPackagetrecord.Visible = true;
                    //divrptflight.Attributes.Add("style", "Display:none");
                }
                rptPackageBookingDetails.DataBind();
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("BindPackageBookingDetails Exception :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace);
        }

    }
    public void BindFlightBooking(MemberDetails pobjMemberDetails)
    {
        if (Session["MemberDetails"] != null)
        {
            List<ItineraryDetails> lobjListItineraryDetails = lobjModel.GetFlightBookingDetails(pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
            if (lobjListItineraryDetails != null && lobjListItineraryDetails.Count > 0)
            {
                rptBookingDetails.DataSource = lobjListItineraryDetails;
                //divrptflight.Attributes.Add("style", "Display:block");
            }
            else
            {
                rptBookingDetails.DataSource = null;
                lblFlightrecord.Visible = true;
                lblFlightrecord.Text = "No Records Found.";
                // divFlight.Visible = false;
                divFlightrecord.Visible = true;
                //divrptflight.Attributes.Add("style", "Display:none");
            }
            rptBookingDetails.DataBind();
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    public void BindHotelBooking(MemberDetails pobjMemberDetails)
    {
        GetHotelInfoDetails lobjGetHotelInfoDetails = new GetHotelInfoDetails();

        lobjGetHotelInfoDetails = lobjModel.GetHotelDetails(pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);

        if (lobjGetHotelInfoDetails.HotelDetails != null && lobjGetHotelInfoDetails.HotelDetails.Count > 0)
        {
            rptHotelCancelBookingDetails.DataSource = lobjGetHotelInfoDetails.HotelDetails;
            //divrpthotel.Attributes.Add("style", "Display:block");
        }
        else
        {
            rptHotelCancelBookingDetails.DataSource = null;
            lblHotelrecord.Visible = true;
            lblHotelrecord.Text = "No Records Found.";
            divHotelrecord.Visible = true;
            //divrpthotel.Visible = false;
            //divrpthotel.Attributes.Add("style", "Display:none");
        }
        rptHotelCancelBookingDetails.DataBind();
    }

    public void BindCarBooking(MemberDetails pobjMemberDetails, int refId)
    {
        if (Session["MemberDetails"] != null)
        {
            List<CarBookingDetails> lobjListItineraryDetails = lobjModel.GetCarBookingDetails(pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, refId);
            if (lobjListItineraryDetails != null && lobjListItineraryDetails.Count > 0)
            {
                rptCarBookingDetails.DataSource = lobjListItineraryDetails;
                //divrptCar.Attributes.Add("style", "Display:block");
            }
            else
            {
                rptCarBookingDetails.DataSource = null;
                lblCarrecord.Visible = true;
                lblCarrecord.Text = "No Records Found.";
                divCarrecord.Visible = true;
                //divrptCar.Visible = false;
                //divrptCar.Attributes.Add("style", "Display:none");
            }
            rptCarBookingDetails.DataBind();
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool ShowAirReceipt(string TripId)
    {
        InfiModel lobjModel = new InfiModel();
        HttpContext.Current.Session["RetriveBookingInfo"] = null;
        RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererData"] as RefererDetails;
        RetriveItineraryDetails lobjRetriveItineraryDetails = lobjModel.RetriveItineraryDetails(TripId, Convert.ToInt32(lobjRefererDetails.Id));
        if (lobjRetriveItineraryDetails != null)
        {
            HttpContext.Current.Session["RetriveBookingInfo"] = lobjRetriveItineraryDetails.ItineraryDetails;
            HttpContext.Current.Session["FlightBooked"] = lobjRetriveItineraryDetails.ItineraryDetails;
        }
        return true;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool ShowPackage(string BookingReferenceId)
    {

        List<TravelBookingDetails> lobjTravelBookingList = HttpContext.Current.Session["TravelBookingList"] as List<TravelBookingDetails>;
        TravelBookingDetails lobjTravelBooking = new TravelBookingDetails();
        ViatorBookResponse lobjViatorBookResponse = new ViatorBookResponse();
        lobjTravelBooking = lobjTravelBookingList.Find(x => x.DistributorRef.Equals(BookingReferenceId));
        ViatorBookRequest lobjViatorBookRequest = new ViatorBookRequest();

        string strAddtoinalDetails = lobjTravelBooking.AdditionalDetails1;

        ViatorBookRequest lobjAddViatorBookRequest = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<ViatorBookRequest>(strAddtoinalDetails);

        lobjViatorBookRequest.ProductDetails.ThumbnailURL = lobjAddViatorBookRequest.ProductDetails.ThumbnailURL;
        lobjViatorBookRequest.ProductDetails.Rating = lobjAddViatorBookRequest.ProductDetails.Rating;

        ViatorBooker lobjViatorBooker = new ViatorBooker();
        lobjViatorBooker.CellPhone = lobjTravelBooking.MemberContactNo;
        lobjViatorBooker.Email = lobjTravelBooking.MemberEmailId;
        lobjViatorBooker.Firstname = lobjTravelBooking.MemberName;

        lobjViatorBookRequest.ViatorBooker = lobjViatorBooker;

        List<BookItem> lobjlstBookItem = new List<BookItem>();
        BookItem lobjBookItem = new BookItem();

        lobjBookItem.TravelDate = lobjTravelBooking.BookingDate;

        List<Traveller> lobjlstTraveller = new List<Traveller>();


        for (int i = 0; i < lobjTravelBooking.TravelAdditionalBookingDetails.Count; i++)
        {
            if (lobjTravelBooking.TravelAdditionalBookingDetails[i].IsBooker == false)
            {
                Traveller lobjTraveller = new Traveller();
                lobjTraveller.Firstname = lobjTravelBooking.TravelAdditionalBookingDetails[i].TravellerName;
                lobjTraveller.Surname = "";
                lobjTraveller.BandId = lobjTravelBooking.TravelAdditionalBookingDetails[i].BandId;
                lobjTraveller.TravellerAge = lobjTravelBooking.TravelAdditionalBookingDetails[i].TravellerAge;
                lobjTraveller.LeadTraveller = lobjTravelBooking.TravelAdditionalBookingDetails[i].IsLeadTraveller;
                lobjlstTraveller.Add(lobjTraveller);
            }
        }
        lobjBookItem.Travellers = lobjlstTraveller;

        lobjlstBookItem.Add(lobjBookItem);
        lobjViatorBookRequest.BookItems = lobjlstBookItem;

        lobjViatorBookRequest.MemberId = lobjTravelBooking.MemberId;
        lobjViatorBookRequest.ProductDetails.FareDetails.TotalPoints = lobjTravelBooking.TotalPoints;
        lobjViatorBookRequest.ProductDetails.ShortTitle = lobjTravelBooking.TravelSearchDetails.SupplierName;

        ViatorBookStatus lobjViatorBookStatus = new ViatorBookStatus();
        lobjViatorBookStatus = (ViatorBookStatus)lobjTravelBooking.BookingStatus;
        ViatorBookingItinerary lobjViatorBookingItinerary = new ViatorBookingItinerary();

        lobjViatorBookingItinerary.DistributorRef = lobjTravelBooking.DistributorItemRef;
        lobjViatorBookingItinerary.ItineraryId = lobjTravelBooking.ItineraryId;

        lobjViatorBookingItinerary.BookingStatus.Text = lobjViatorBookStatus.ToString();
        lobjViatorBookingItinerary.VoucherKey = lobjTravelBooking.VoucherKey;
        lobjViatorBookingItinerary.BookingRefId = lobjTravelBooking.BookingRefId;
        string strTravelDate = string.Empty;
        strTravelDate = lobjTravelBooking.CreateDate.Year + "-" + lobjTravelBooking.CreateDate.Month + "-" + lobjTravelBooking.CreateDate.Day;

        lobjViatorBookingItinerary.BookingDate =strTravelDate;
        lobjViatorBookResponse.BookingItinerary = lobjViatorBookingItinerary;

        HttpContext.Current.Session[SessionFields.ViatorSearchRequest] = lobjViatorBookRequest;
        HttpContext.Current.Session[SessionFields.ViatorBookResponse] = lobjViatorBookResponse;

        return true;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool ShowHotelVoucher(string TransactionReferenceCode)
    {
        InfiModel lobjModel = new InfiModel();
        HttpContext.Current.Session["HotelBooked"] = null;
        HttpContext.Current.Session["CustomerDetails"] = null;
        HttpContext.Current.Session["BookingResponse"] = null;

        HotelItineraryResponse lobjHotelItineraryResponse = lobjModel.GetBookedHotelInfo(TransactionReferenceCode);
        if (lobjHotelItineraryResponse != null)
        {
            HttpContext.Current.Session["HotelBooked"] = lobjHotelItineraryResponse.HotelSearchResponse;
            HttpContext.Current.Session["CustomerDetails"] = lobjHotelItineraryResponse.Customer;
            HttpContext.Current.Session["BookingResponse"] = lobjHotelItineraryResponse.HotelBookingResponse;

        }
        return true;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool ShowCarVoucher(string BookingReferenceId)
    {
        InfiModel lobjModel = new InfiModel();

        CarBookingDetails lobjCarBookingResponse = lobjModel.GetCarInfoDetailsByBookingRefId(BookingReferenceId);
        if (lobjCarBookingResponse != null)
        {
            CarMakeBookingResponse lobjCarMakeBookingResponse = new CarMakeBookingResponse();
            Booking lobjBooking = new Booking();
            lobjBooking.id = lobjCarBookingResponse.Booking.id;
            lobjBooking.status = lobjCarBookingResponse.Booking.status;


            lobjCarMakeBookingResponse.MakeBookingRS.Booking = lobjBooking;
            lobjCarMakeBookingResponse.MakeBookingRS.Booking = lobjBooking;
            lobjCarMakeBookingResponse.BookingReferenceId = lobjCarBookingResponse.BookingReferenceId;
            lobjCarMakeBookingResponse.BookingPaymentDetails = lobjCarBookingResponse.BookingPaymentDetails;
            HttpContext.Current.Session["CarMakeBookingResponse"] = lobjCarMakeBookingResponse;

            CarMakeBookingRequest lobjCarMakeBookingRequest = new CarMakeBookingRequest();
            List<DriverInfo> lobjDriverInfoList = new List<DriverInfo>();
            DriverInfo lobjDriverInfo = new DriverInfo();
            List<DriverName> lobjListDriverName = new List<DriverName>();
            DriverName lobjDriverName = new DriverName();
            lobjDriverName.title = lobjCarBookingResponse.Booking.DriverInfo[0].DriverName[0].title;
            lobjDriverName.firstname = lobjCarBookingResponse.Booking.DriverInfo[0].DriverName[0].firstname;
            lobjDriverName.lastname = lobjCarBookingResponse.Booking.DriverInfo[0].DriverName[0].lastname;
            //lobjDriverName.title = "Mr";
            //lobjDriverName.firstname = "Test";
            //lobjDriverName.lastname = "Tesre";
            lobjListDriverName.Add(lobjDriverName);
            lobjDriverInfo.DriverName = lobjListDriverName.ToArray();
            lobjDriverInfoList.Add(lobjDriverInfo);
            lobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo = lobjDriverInfoList.ToArray();
            lobjCarMakeBookingRequest.BookingPaymentDetails.PaymentType = lobjCarBookingResponse.BookingPaymentDetails.PaymentType;
            HttpContext.Current.Session["CarMadeBookingRequest"] = lobjCarMakeBookingRequest;

            List<Location> lobjPickUpLocationList = new List<Location>();
            Location lobjPickUpLocation = new Location();
            lobjPickUpLocation.locName = lobjCarBookingResponse.Booking.PickUp[0].Location[0].locName;
            lobjPickUpLocation.city = lobjCarBookingResponse.Booking.PickUp[0].Location[0].city;
            lobjPickUpLocation.country = lobjCarBookingResponse.Booking.PickUp[0].Location[0].country;
            //lobjPickUpLocation.locName = "XYZ";
            //lobjPickUpLocation.city = "XYZ";
            //lobjPickUpLocation.country = "XYZ";
            lobjPickUpLocationList.Add(lobjPickUpLocation);

            List<PickUp> lobjPickUpList = new List<PickUp>();
            PickUp lobjPickUp = new PickUp();
            lobjPickUp.Location = lobjPickUpLocationList.ToArray();
            lobjPickUpList.Add(lobjPickUp);

            List<Date> lobjListOfPickUpDate = new List<Date>();
            Date lobjPickUpDate = new Date();
            lobjPickUpDate.day = lobjCarBookingResponse.Booking.PickUp[0].Date[0].day;
            lobjPickUpDate.month = lobjCarBookingResponse.Booking.PickUp[0].Date[0].month;
            lobjPickUpDate.year = lobjCarBookingResponse.Booking.PickUp[0].Date[0].year;
            lobjPickUpDate.minute = lobjCarBookingResponse.Booking.PickUp[0].Date[0].minute;
            lobjPickUpDate.hour = lobjCarBookingResponse.Booking.PickUp[0].Date[0].hour;
            //lobjPickUpDate.day = "10";
            //lobjPickUpDate.month = "10";
            //lobjPickUpDate.year = "2011";
            //lobjPickUpDate.minute = "10";
            //lobjPickUpDate.hour = "10";
            lobjListOfPickUpDate.Add(lobjPickUpDate);
            lobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp = lobjPickUpList.ToArray();
            lobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Date = lobjListOfPickUpDate.ToArray();

            List<Location> lobjDropOffLocationList = new List<Location>();
            Location lobjDropOffLocation = new Location();
            lobjDropOffLocation.locName = lobjCarBookingResponse.Booking.DropOff[0].Location[0].locName;
            lobjDropOffLocation.city = lobjCarBookingResponse.Booking.DropOff[0].Location[0].city;
            lobjDropOffLocation.country = lobjCarBookingResponse.Booking.DropOff[0].Location[0].country;
            //lobjDropOffLocation.locName = "XYZ";
            //lobjDropOffLocation.city = "XYZ";
            //lobjDropOffLocation.country = "XYZ";

            lobjDropOffLocationList.Add(lobjDropOffLocation);
            List<DropOff> lobjDropOffList = new List<DropOff>();
            DropOff lobjDropOff = new DropOff();

            lobjDropOff.Location = lobjDropOffLocationList.ToArray();
            lobjDropOffList.Add(lobjDropOff);
            List<Date> lobjListOfDropOffDate = new List<Date>();
            Date lobjDropOffDate = new Date();
            lobjDropOffDate.day = lobjCarBookingResponse.Booking.DropOff[0].Date[0].day;
            lobjDropOffDate.month = lobjCarBookingResponse.Booking.DropOff[0].Date[0].month;
            lobjDropOffDate.year = lobjCarBookingResponse.Booking.DropOff[0].Date[0].year;
            lobjDropOffDate.minute = lobjCarBookingResponse.Booking.DropOff[0].Date[0].minute;
            lobjDropOffDate.hour = lobjCarBookingResponse.Booking.DropOff[0].Date[0].hour;
            //lobjDropOffDate.day = "10";
            //lobjDropOffDate.month = "10";
            //lobjDropOffDate.year = "2012";
            //lobjDropOffDate.minute = "10";
            //lobjDropOffDate.hour = "10";

            lobjListOfDropOffDate.Add(lobjDropOffDate);
            lobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff = lobjDropOffList.ToArray();
            lobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff[0].Date = lobjListOfDropOffDate.ToArray();


            Match lobjMatch = new Match();
            List<Route> lobjListOfRoute = new List<Route>();
            Route lobjRoute = new Route();
            lobjRoute.PickUp = lobjPickUpLocationList.ToArray();
            lobjRoute.DropOff = lobjDropOffLocationList.ToArray();
            lobjListOfRoute.Add(lobjRoute);
            lobjMatch.Route = lobjListOfRoute.ToArray();

            List<Vehicle> lobjListVehicle = new List<Vehicle>();
            Vehicle lobjVehicle = new Vehicle();
            lobjVehicle.aircon = lobjCarBookingResponse.Booking.Vehicle[0].aircon;
            lobjVehicle.automatic = lobjCarBookingResponse.Booking.Vehicle[0].automatic;
            lobjVehicle.Name = lobjCarBookingResponse.Booking.Vehicle[0].Name;
            //lobjVehicle.aircon = "AC";
            //lobjVehicle.automatic = "automatic";
            //lobjVehicle.Name = "TATA";
            lobjListVehicle.Add(lobjVehicle);
            lobjMatch.Vehicle = lobjListVehicle.ToArray();

            List<Price> lobjListOfPrice = new List<Price>();
            Price lobjPrice = new Price();
            lobjPrice.TotalPoints = lobjCarBookingResponse.FinalPoints;
            lobjPrice.TotalBaseFare = lobjCarBookingResponse.BookingPaymentDetails.CashAmount;
            lobjListOfPrice.Add(lobjPrice);
            lobjPrice.ExtraTotalPoints = 0;
            lobjMatch.Price = lobjListOfPrice.ToArray();


            HttpContext.Current.Session["CarSelected"] = lobjMatch;

            CarSearchRequest pobjCarSearchRequest = new CarSearchRequest();
            pobjCarSearchRequest.SearchRequest.PickUp = lobjPickUpList.ToArray();
            pobjCarSearchRequest.SearchRequest.DropOff = lobjDropOffList.ToArray();

            HttpContext.Current.Session["CarSearchRequest"] = pobjCarSearchRequest;


            CarExtrasListResponse pobjCarExtrasListResponse = new CarExtrasListResponse();
            List<ExtraInfoList> lobjListOfExtraInfoList = new List<ExtraInfoList>();
            ExtraInfoList lobjExtraInfoList = new ExtraInfoList();
            List<ExtraInfo> lobjListOfExtraInfo = new List<ExtraInfo>();
            ExtraInfo lobjExtraInfo = new ExtraInfo();
            lobjExtraInfo.Price = lobjListOfPrice.ToArray();
            lobjListOfExtraInfo.Add(lobjExtraInfo);
            lobjExtraInfoList.ExtraInfo = lobjListOfExtraInfo.ToArray();
            lobjListOfExtraInfoList.Add(lobjExtraInfoList);

            //lobjListOfExtraInfoList.Add(lobjListOfExtraInfo);
            pobjCarExtrasListResponse.ExtrasListRS.Items = lobjListOfExtraInfoList.ToArray();

            HttpContext.Current.Session["CarExtraListResponse"] = pobjCarExtrasListResponse;

        }
        return true;
    }
}