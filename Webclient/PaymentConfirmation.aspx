﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="PaymentConfirmation.aspx.cs" Inherits="PaymentConfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <style type="text/css">
        #side-menu_tab {
            float: left;
            width: 179px;
            font-size: 12px;
            color: #FFF;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
            padding: 8px 10px 10px;
        }

        #left {
            float: left;
            width: 202px;
            z-index: 1000;
            position: relative;
        }

        #passengerdiv {
            background: #fff;
            float: left;
            width: 96%;
            border: 1px solid #ccc;
            padding-top:100px;
            padding-bottom:100px;
        }

        .al_Left {
            text-align: left !important;
        }

        .divLeft {
            float: left;
            width: 60%;
            margin-left: 7%;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .Border_Black {
            border: 1px solid #000000;
        }
    </style>
    <div style="float: left; width: 100%;">
        <div id="passengerdiv" class="al_Left" style="background: #fff; float: left; width: 100%">
            <div class="divLeft Border_Black" style="height: 162px; margin-left: 19%">
                <div style="padding:10px;">
                    <h2 class="robot" style="color:#e30e27;font-size:25px;text-align:center">Select Payment Mode</h2>
                </div>
                <div style="float: left; margin-left: 75px; margin-top:25px;width: 100%; font-size: 20px">
                    <div style="float: left; width: 33%">
                        <div style="float: left; width: 10%;padding-top: 3%;">
                            <asp:RadioButton ID="rdbtnPoints" runat="server" OnCheckChanged="rdbtnPoints_CheckedChanged"
                                GroupName="PaymentMode" AutoPostBack="True" />
                        </div>
                        <div class="robot" style="float: left; margin-left: 10px; width: 78%">
                            WorldRewardZ
                        </div>
                    </div>
                    <div style="float: left; width: 33%">
                        <div style="float: left; width: 10%;padding-top: 3%;">
                            <asp:RadioButton ID="rdbtnVoucher" runat="server" OnCheckChanged="rdbtnVoucher_CheckedChanged"
                                GroupName="PaymentMode" AutoPostBack="True" />
                        </div>
                        <div class="robot" style="float: left; margin-left: 10px; width: 78%">
                            Voucher
                        </div>
                    </div>
                    <div style="float: left; width: 33%">
                        <div style="float: left; width: 10%;padding-top: 3%;">
                            <asp:RadioButton ID="rdbtnCash" runat="server" OnCheckChanged="rdbtnCash_CheckedChanged"
                                GroupName="PaymentMode" AutoPostBack="True" />
                        </div>
                        <div class="robot" style="float: left; margin-left: 10px; width: 78%">
                            Cash
                        </div>
                    </div>
                </div>
            </div>
            <div class="divLeft ErrorMsgContainer" style="margin-left: 19%">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </div>
            
        </div>
    </div>
</asp:Content>