﻿using Core.Platform.Authentication.Entities;
using Core.Platform.Member.Entites;
using Core.Platform.Merchant.Entities;
using Core.Platform.PointGateway.Service.Helper.PGService;
using Core.Platform.ProgramMaster.Entities;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.Security;
using Framework.EnterpriseLibrary.Security.Constants;
using Framework.EnterpriseLibrary.UniqueNumberGenerator;
using InfiPlanetModel.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ExtLogin : System.Web.UI.Page
{
    InfiModel lobjWorldRewardModel  = new InfiModel();
    MemberDetails lobjMemberDetails = new MemberDetails();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Form.Count > 0)
            {

                string lstrEncryptedPostUrl = Request.Form["Merchant_Details"];
                LoggingAdapter.WriteLog("Merchant Details = " + lstrEncryptedPostUrl);
                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                string lstrDecryptedPostUrl = RSAEncryptor.DecryptString(lstrEncryptedPostUrl, RSASecurityConstant.KeySize, RSASecurityConstant.PrivateKey);
                LoggingAdapter.WriteLog("Merchant Details1 = " + lstrDecryptedPostUrl);
                string[] lstrMerchantDetails = lstrDecryptedPostUrl.Split("|".ToCharArray());
                lobjMerchantDetails.MerchantCode = lstrMerchantDetails[0].ToString();
                LoggingAdapter.WriteLog("Merchant Details2 = " + lstrMerchantDetails[0].ToString());
                lobjMerchantDetails.Password = lstrMerchantDetails[1].ToString();
                LoggingAdapter.WriteLog("Merchant Details3 = " + lstrMerchantDetails[1].ToString());
                MerchantDetails lobjCurrentlobjMerchantDetails = null;
                hdfContext.Value = lstrMerchantDetails[2].ToString();
                LoggingAdapter.WriteLog("Merchant Details4 = " + hdfContext.Value);
                lobjCurrentlobjMerchantDetails = lobjWorldRewardModel.GetMerchantDetails(lobjMerchantDetails);
                if (lobjCurrentlobjMerchantDetails != null)
                {
                    Session["ExtMerchantDetails"] = lobjCurrentlobjMerchantDetails;
                    LoggingAdapter.WriteLog("Merchant Details = " + Session["ExtMerchantDetails"]);
                }
                else
                {
                    Response.Redirect("InvalidCredential.aspx");
                    LoggingAdapter.WriteLog("InvalidCredential = ");
                }
            }
            else
            {
                Response.Redirect("InvalidCredential.aspx");
            }
        }
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            LoggingAdapter.WriteLog("externalLogin");
            string lstrEncryptPassword = string.Empty;
            string lstrDecryptPassword = string.Empty;
            string lstrPassword = txtPassword.Text;
            string lstrMemberId = txtMemberName.Text;

            string lstrAvailablePoints = string.Empty;
            string lstrResponseDetails = string.Empty;

            string lstrAuthenticationToken = string.Empty;
            int lintStatus = 0;
            string lstrCustomerId = string.Empty;

            ProgramDefinition lobjProgramMaster = HttpContext.Current.Application["ProgramMaster"] as ProgramDefinition;
            LoggingAdapter.WriteLog("externalLogin1");
            SystemParameter lobjPasswordPolicy = HttpContext.Current.Application["SystemParameters"] as SystemParameter;
            Response lobjResponse = new Response();
            PGManagerClient lobjPGManagerClient = new PGManagerClient();

            if (lobjPasswordPolicy != null)
            {
                LoggingAdapter.WriteLog("externalLogin2");
                Regex regex = new Regex((lobjPasswordPolicy.PasswordPolicy));
                if (regex.Match(lstrPassword).Success)
                {
                    lstrEncryptPassword = lobjWorldRewardModel.EncryptPassword(lstrPassword);

                    lobjMemberDetails = lobjWorldRewardModel.CheckMembershipCrediantials(lstrMemberId, lstrEncryptPassword);
                    if (lobjMemberDetails != null)
                    {
                        MerchantDetails lobjMerchantDetails = null;
                        lobjMerchantDetails = Session["ExtMerchantDetails"] as MerchantDetails;
                        LoggingAdapter.WriteLog("externalLogin3 = ");
                        lstrAuthenticationToken = GenerateUniqueNumber.Get12DigitNumberDateTime() ;
                        lstrCustomerId = lobjMemberDetails.MemberRelationsList[0].RelationReference;
                        LoggingAdapter.WriteLog("WorldRewards Authenticationtoken = " + lstrAuthenticationToken + " | Customer ID =" + lstrCustomerId);
                        lintStatus = InsertAuthenticationToken(lstrCustomerId, lstrAuthenticationToken);

                        LoggingAdapter.WriteLog("Insert Authentication Token = " + lintStatus);
                        if (lintStatus > 0)
                        {
                            string lstrCurrency = lobjWorldRewardModel.GetDefaultCurrency();
                            lobjResponse = lobjPGManagerClient.CheckPointsAvailability(lobjMemberDetails.MemberRelationsList[0].RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency, lobjMerchantDetails.MerchantCode, lobjMerchantDetails.UserName, lobjMerchantDetails.Password);
                            if (lobjResponse.IsSucessful)
                            {
                                LoggingAdapter.WriteLog("externalLogin4");
                                lstrAvailablePoints = lobjResponse.ReturnObject;
                            }
                            else
                            {
                                LoggingAdapter.WriteLog("externalLogin5");
                                lstrResponseDetails = "1" + "|" + lobjResponse.ExceptionMessage;
                                PostResponse(lobjMerchantDetails.CallBackURL, lstrResponseDetails);
                            }

                            lstrResponseDetails = "0" + "|" + lobjMemberDetails.MemberRelationsList[0].RelationReference + "|" + lobjMemberDetails.LastName + "|" + lobjMemberDetails.Email + "|" + lstrAvailablePoints + "|" + lstrAuthenticationToken + "|" + hdfContext.Value;

                            PostResponse(lobjMerchantDetails.CallBackURL, lstrResponseDetails);
                        }
                    }
                    else
                    {
                        lblLoginError.Text = "Please check the login details you have entered and try again.";
                    }
                }
                else
                {
                    lblLoginError.Text = "Password must contain one non-alpha character,one upper case character,one lower case character.";
                }
            }
        }
        catch (ApplicationException appEx)
        {
            if (appEx.Message.Equals("AccountLock"))
            {
                lblLoginError.Text = "Your Account is Locked.";
            }
            else
            {
                lblLoginError.Text = "Please check the login details you have entered and try again.";
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.Equals("AccountLock"))
            {
                lblLoginError.Text = "Your Account is Locked.";
            }
            else
            {
                lblLoginError.Text = "Please check the login details you have entered and try again.";
            }
        }
    }

    


    public void PostResponse(string pstrPostURL, string pstrResponseDetails)
    {
        string lstrEncryptedResponseDetails = RSAEncryptor.EncryptString(pstrResponseDetails, RSASecurityConstant.KeySize, RSASecurityConstant.RSAPublicKey);
        LoggingAdapter.WriteLog("externalLogin2+lstrEncryptedResponseDetails");
        StringBuilder sb = new StringBuilder();
        sb.Append("<html>");
        sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
        sb.AppendFormat("<form name='form' action='{0}' method='post'>", pstrPostURL);
        sb.AppendFormat("<input type='hidden' runat='server' name='Response_Details' value='{0}'>", lstrEncryptedResponseDetails);
        sb.Append("</form>");
        sb.Append("</body>");
        sb.Append("</html>");
        Response.Write(sb.ToString());
        Response.End();
    }

    public int InsertAuthenticationToken(string pstrMemberID, string pstrAuthenticationToken)
    {
        int lintStatus = 0;
        try
        {

            int lintMinutes = Convert.ToInt32(ConfigurationSettings.AppSettings["ExpiryMinutes"]);
            LoggingAdapter.WriteLog("externalLogin2+lintMinutes");
            InfiModel lobjWorldRewardModel  = new InfiModel();
            AuthenticationDetails lobjAuthenticationDetails = new AuthenticationDetails();
            lobjAuthenticationDetails.MemberId = pstrMemberID;
            lobjAuthenticationDetails.AuthenticationToken = pstrAuthenticationToken;
            lobjAuthenticationDetails.AuthenticationType = AuthenticationType.PG;
            lobjAuthenticationDetails.CreationDate = DateTime.Now;
            lobjAuthenticationDetails.ExpiryDate = DateTime.Now.AddMinutes(lintMinutes);
            lobjAuthenticationDetails.IsActive = true;
            lobjAuthenticationDetails.CreatedBy = "System";
            lobjAuthenticationDetails.CreatedOn = DateTime.Now;
            lobjAuthenticationDetails.UpdatedOn = DateTime.Now;
            lintStatus = lobjWorldRewardModel.InsertAuthenticationDetails(lobjAuthenticationDetails);
            LoggingAdapter.WriteLog("externalLogin2+lintStatus");
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("InsertAuthenticationToken exception - " + ex.Message + Environment.NewLine + ex.StackTrace);
        }
        return lintStatus;

    }

    protected void btnexternal_Click(object sender, EventArgs e)
    {
        try
        {
            LoggingAdapter.WriteLog("externalLogin");
            string lstrEncryptPassword = string.Empty;
            string lstrDecryptPassword = string.Empty;
            string lstrPassword = txtPassword.Text;
            string lstrMemberId = txtMemberName.Text;

            string lstrAvailablePoints = string.Empty;
            string lstrResponseDetails = string.Empty;

            string lstrAuthenticationToken = string.Empty;
            int lintStatus = 0;
            string lstrCustomerId = string.Empty;

            ProgramDefinition lobjProgramMaster = HttpContext.Current.Application["ProgramMaster"] as ProgramDefinition;
            LoggingAdapter.WriteLog("externalLogin1");
            SystemParameter lobjPasswordPolicy = HttpContext.Current.Application["SystemParameters"] as SystemParameter;
            Response lobjResponse = new Response();
            PGManagerClient lobjPGManagerClient = new PGManagerClient();

            if (lobjPasswordPolicy != null)
            {
                LoggingAdapter.WriteLog("externalLogin2");
                Regex regex = new Regex((lobjPasswordPolicy.PasswordPolicy));
                if (regex.Match(lstrPassword).Success)
                {
                    lstrEncryptPassword = lobjWorldRewardModel.EncryptPassword(lstrPassword);

                    lobjMemberDetails = lobjWorldRewardModel.CheckMembershipCrediantials(lstrMemberId, lstrEncryptPassword);
                    if (lobjMemberDetails != null)
                    {
                        MerchantDetails lobjMerchantDetails = null;
                        lobjMerchantDetails = Session["ExtMerchantDetails"] as MerchantDetails;
                        LoggingAdapter.WriteLog("externalLogin3 = ");
                        lstrAuthenticationToken = GenerateUniqueNumber.Get12DigitNumberDateTime();
                        lstrCustomerId = lobjMemberDetails.MemberRelationsList[0].RelationReference;
                        LoggingAdapter.WriteLog("WorldRewards Authenticationtoken = " + lstrAuthenticationToken + " | Customer ID =" + lstrCustomerId);
                        lintStatus = InsertAuthenticationToken(lstrCustomerId, lstrAuthenticationToken);

                        LoggingAdapter.WriteLog("Insert Authentication Token = " + lintStatus);
                        if (lintStatus > 0)
                        {
                            string lstrCurrency = lobjWorldRewardModel.GetDefaultCurrency();
                            lobjResponse = lobjPGManagerClient.CheckPointsAvailability(lobjMemberDetails.MemberRelationsList[0].RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency, lobjMerchantDetails.MerchantCode, lobjMerchantDetails.UserName, lobjMerchantDetails.Password);
                            if (lobjResponse.IsSucessful)
                            {
                                LoggingAdapter.WriteLog("externalLogin4");
                                lstrAvailablePoints = lobjResponse.ReturnObject;
                            }
                            else
                            {
                                LoggingAdapter.WriteLog("externalLogin5");
                                lstrResponseDetails = "1" + "|" + lobjResponse.ExceptionMessage;
                                PostResponse(lobjMerchantDetails.CallBackURL, lstrResponseDetails);
                                LoggingAdapter.WriteLog("externalLogin6");
                            }

                            lstrResponseDetails = "0" + "|" + lobjMemberDetails.MemberRelationsList[0].RelationReference + "|" + lobjMemberDetails.LastName + "|" + lobjMemberDetails.Email + "|" + lstrAvailablePoints + "|" + lstrAuthenticationToken + "|" + hdfContext.Value;

                            PostResponse(lobjMerchantDetails.CallBackURL, lstrResponseDetails);
                        }
                    }
                    else
                    {
                        lblLoginError.Text = "Please check the login details you have entered and try again.";
                    }
                }
                else
                {
                    lblLoginError.Text = "Password must contain one non-alpha character,one upper case character,one lower case character.";
                }
            }
        }
        catch (ApplicationException appEx)
        {
            if (appEx.Message.Equals("AccountLock"))
            {
                lblLoginError.Text = "Your Account is Locked.";
            }
            else
            {
                lblLoginError.Text = "Please check the login details you have entered and try again.";
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.Equals("AccountLock"))
            {
                lblLoginError.Text = "Your Account is Locked.";
            }
            else
            {
                lblLoginError.Text = "Please check the login details you have entered and try again.";
            }
        }
    }
}