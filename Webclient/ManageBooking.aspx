﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="ManageBooking.aspx.cs" Inherits="ManageBooking" %>

<%@ Register Src="~/UserControl/ActivationMenu.ascx" TagName="ActivationMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <script src="js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="js/jquery.mCustomScrollbar.css" type="text/css" />
    <script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <link href="css/myAccount.css" rel="stylesheet" />
    <script>
		/*tabier scripts*/
		$(document).ready(function() {
			$("#content aside").hide(); // Initially hide all content
			$("#tabs li:first").attr("id","current"); // Activate first tab
			$("#content aside:first").fadeIn(); // Show first tab content
			
			$('#tabs a').click(function(e) {
				e.preventDefault();        
				$("#content aside").hide(); //Hide all content
				$("#tabs li").attr("id",""); //Reset id's
				$(this).parent().attr("id","current"); // Activate this
				$('#' + $(this).attr('title')).fadeIn(); // Show content for current tab
			});
		});
</script>
    <script type="text/javascript">
        $.fn.digits = function () {
            return this.each(function () {
                $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            })
        }
        function ShowAirReceipt(ItineraryTripId) {
            $.ajax({
                type: 'POST',
                url: 'ManageBooking.aspx/ShowAirReceipt',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: "{'TripId':'" + ItineraryTripId + "'}",
                cache: false,
                success: function (msg) {
                    if (msg.d) {
                        window.open('AirPrintReceipt.aspx', '_blank');
                    }
                    else
                        window.location = "Login.aspx";
                },
                error: function (errmsg) {

                }
            });
            return false;
        }

        function ShowHotelVoucher(TransactionReferenceCode) {
            $.ajax({
                type: 'POST',
                url: 'ManageBooking.aspx/ShowHotelVoucher',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: "{'TransactionReferenceCode':'" + TransactionReferenceCode + "'}",
                cache: false,
                success: function (msg) {
                    if (msg.d) {
                        window.open('HotelVoucherPrint.aspx', '_blank');
                    }
                    else
                        window.location = "Login.aspx";
                },
                error: function (errmsg) {

                }
            });
            return false;
        }
        function ShowCarVoucher(BookingReferenceId) {
            $.ajax({
                type: 'POST',
                url: 'ManageBooking.aspx/ShowCarVoucher',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: "{'BookingReferenceId':'" + BookingReferenceId + "'}",
                cache: false,
                success: function (msg) {
                    if (msg.d) {
                        window.open('PrintCarVoucher.aspx', '_blank');
                    }
                    else
                        window.location = "Login.aspx";
                },
                error: function (errmsg) {

                }
            });
            return false;
        }
        function ShowPackage(BookingReferenceId) {
            $.ajax({
                type: 'POST',
                url: 'ManageBooking.aspx/ShowPackage',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: "{'BookingReferenceId':'" + BookingReferenceId + "'}",
                cache: false,
                success: function (msg) {
                    if (msg.d) {
                        window.open('PrintTravelResponse.aspx', '_blank');
                    }
                    else
                        window.location = "Login.aspx";
                },
                error: function (errmsg) {

                }
            });
            return false;
        }

        $(document).ready(function () {
            $(".TotalMiles").digits();

            var strCurrency = $("#Carbookingstatus")[0].innerHTML;
            $("#Carbookingstatus")[0].innerHTML = strCurrency.replace('car', 'Car');
        });

    </script>
     
    <style type="text/css">
        .fl {
            float: left;
        }
        .myacc-bkngtbl{max-height:150px !important;}
    </style>
    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>
    <!--new-->
     <article id="article">
         <div class="wrap">
            <h1>Experience Abundance with WorldRewardZ</h1>
            <p>Below is a categorized listing of your activities at WorldRewardZ. As you glance through them, you can immerse in the joy of having the best choices in the world available to you simply at a click, with WorldRewardZ. Go ahead and multiply your unique experiences.</p>
			<div class="ManyWrap MyAccount">
               <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
                <div id="Details">
                <ul id="tabs">
                <li id="current"><a href="javascript:void(0)" title="tab1">FLIGHT BOOKING DETAILS</a></li>
                <li id=""><a href="javascript:void(0)" title="tab2">HOTEL BOOKING DETAILS</a></li>
                <li id=""><a href="#" title="tab3">CAR RENTAL DETAILS</a></li>
                <li id=""><a href="#" title="tab4">PACKAGE BOOKING DETAILS</a></li>
                </ul>
                <div id="content">
                 <aside id="tab1" style="display: block;">
                    <asp:Repeater ID="rptBookingDetails" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr class="tbl_th">
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Departure Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Return Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Details
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Ref No.
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Trip ID
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        WorldRewardZ
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Action
                                    </div>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr style="color: #000000" align="center">
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%# Convert.ToDateTime(DataBinder.Eval(Container, "DataItem.DepartureDate")).ToString("dd/MM/yyyy")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
<%# (Convert.ToDateTime(DataBinder.Eval(Container, "DataItem.ArrivalDate")).ToString("dd/MM/yyyy")) == "01/01/0001" ? "-" : (Convert.ToDateTime(DataBinder.Eval(Container, "DataItem.ArrivalDate")).ToString("dd/MM/yyyy")) %> 
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%# DataBinder.Eval(Container, "DataItem.OriginLocation")%>
                                    -
                                    <%# DataBinder.Eval(Container, "DataItem.DestinationLocation")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%# DataBinder.Eval(Container, "DataItem.ItineraryReference")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%# DataBinder.Eval(Container, "DataItem.ItineraryTripId")%><asp:HiddenField ID="hdnTripId"
                                        runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ItineraryReference")%>' />
                                    <asp:HiddenField ID="hdnReferrerId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.RefererDetails.Id")%>' />
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl TotalMiles">
                                    <%# DataBinder.Eval(Container, "DataItem.FareDetails.TotalPoints")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <asp:LinkButton ID="BtnViewAir" Text="Print" runat="server" OnClientClick='<%#String.Format("javascript:return ShowAirReceipt(\"{0}\")",Eval("ItineraryTripId").ToString())%>'></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                 <div id="divFlightrecord" runat="server">
                 <asp:Label runat="server" ID="lblFlightrecord" Visible="false"></asp:Label>
                </div>
                 </aside>
                 <aside id="tab2">
                    <asp:Repeater ID="rptHotelCancelBookingDetails" runat="server">
                    <HeaderTemplate>
                       <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr class="tbl_th">
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Check In Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Check Out Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Booking Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Details
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Ref No.
                                    </div>
                                </th>
                                <th class="vmid" style="width:13%">
                                    <div class="th-block-left">
                                        WorldRewardZ
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Status
                                    </div>
                                </th>
                                <th class="vmid" style="width: 6%;">
                                    <div class="th-block-left">
                                        Action
                                    </div>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr style="color: #000000" align="center">
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Convert.ToDateTime(Eval("searchCriteria.CheckInDate")).ToString("dd/MM/yyyy")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Convert.ToDateTime(Eval("searchCriteria.CheckOutDate")).ToString("dd/MM/yyyy")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Convert.ToDateTime(Eval("BookinDate")).ToString("dd/MM/yyyy")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("Hotel.basicinfo.hotelname")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("TransactionReferenceCode")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl TotalMiles">
                                    <%#Eval("TotalPoint")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%# (Convert.ToInt32(Eval("Status")).Equals(1))? "BOOKED" : "CANCELLED" %>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <asp:HiddenField ID="hdnCancelHotelBooking" Value='<%#String.Concat(Eval("ExternalBookingId"), "&bookid=", Eval("BookingId").ToString(),"&transRef=",Eval("TransactionReferenceCode").ToString())%>'
                                        runat="server"></asp:HiddenField>
                                    <asp:HiddenField ID="hdnTransRef" Value='<%#Eval("TransactionReferenceCode").ToString()%>'
                                        runat="server"></asp:HiddenField>
                                    <asp:LinkButton ID="imgBtnHotelVoucher" runat="server" Text="Print" Enabled='<%# Convert.ToInt32(Eval("Status")).Equals(1) %>'
                                        OnClientClick='<%#String.Format("javascript:return ShowHotelVoucher(\"{0}\")",Eval("TransactionReferenceCode").ToString())%>'></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                    <div id="divHotelrecord" runat="server">
                    <asp:Label runat="server" ID="lblHotelrecord" Visible="false"></asp:Label>
                     </div>
                  </aside>
                 <aside id="tab3">
                  <asp:Repeater ID="rptCarBookingDetails" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr class="tbl_th">
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Supplier ID
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Booking Ref ID
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Pick-up Details
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                    Drop-off Details
                                </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Booking Status
                                    </div>
                                </th>
                                <th class="vmid" style="width:13%">
                                    <div class="th-block-left">
                                        WorldRewardZ
                                    </div>
                                </th>
                                <th class="vmid" style="width: 6%;">
                                    <div class="th-block-left">
                                        Action
                                    </div>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("Booking.id")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("BookingReferenceId") %>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("Booking.PickUp[0].Location[0].country")%>,
                                    <%#Eval("Booking.PickUp[0].Location[0].city")%>,<br />
                                    <%#Eval("Booking.PickUp[0].Location[0].locName")%>,<br />
                                    <%#Eval("Booking.PickUp[0].Date[0].day")%>/<%#Eval("Booking.PickUp[0].Date[0].month")%>/<%#Eval("Booking.PickUp[0].Date[0].year")%>,
                                    <%#Eval("Booking.PickUp[0].Date[0].hour")%>:<%#Eval("Booking.PickUp[0].Date[0].minute")%></div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("Booking.DropOff[0].Location[0].country")%>,
                                    <%#Eval("Booking.DropOff[0].Location[0].city")%>,<br />
                                    <%#Eval("Booking.DropOff[0].Location[0].locName")%>,<br />
                                    <%#Eval("Booking.DropOff[0].Date[0].day")%>/<%#Eval("Booking.DropOff[0].Date[0].month")%>/<%#Eval("Booking.DropOff[0].Date[0].year")%>,
                                    <%#Eval("Booking.DropOff[0].Date[0].hour")%>:<%#Eval("Booking.DropOff[0].Date[0].minute")%></div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl" id="Carbookingstatus">
                                    <%#Eval("Booking.status")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl TotalMiles">
                                    <%#Eval("FinalPoints")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <asp:HiddenField ID="hdnBookingRef" Value='<%#Eval("BookingReferenceId").ToString()%>'
                                        runat="server"></asp:HiddenField>
                                    <asp:LinkButton ID="imgBtnCarVoucher" runat="server" Text="Print" OnClientClick='<%#String.Format("javascript:return ShowCarVoucher(\"{0}\")",Eval("BookingReferenceId").ToString())%>'></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                  <div id="divCarrecord" runat="server">
                    <asp:Label runat="server" ID="lblCarrecord" Visible="false"></asp:Label>
                </div>
                 </aside>
                 <aside id="tab4">
                    <asp:Repeater ID="rptPackageBookingDetails" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr class="tbl_th">
                                <th class="vmid">
                                    <div class="th-block-left">
                                       Product Title
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Booking Ref ID
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Travel Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                  Itineray Id 
                                </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Booking Status
                                    </div>
                                </th>
                                <th class="vmid" style="width:13%">
                                    <div class="th-block-left">
                                        WorldRewardZ
                                    </div>
                                </th>
                                <th class="vmid" style="width: 6%;">
                                    <div class="th-block-left">
                                        Action
                                    </div>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("TravelSearchDetails.SupplierName")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                       <%#Eval("BookingRefId")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                   <%#Eval("BookingDate")%>
                                    </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                   <%#Eval("ItineraryId")%>
                                    </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl" id="Carbookingstatus">            

                                    <%# (Convert.ToInt32(Eval("BookingStatus")).Equals(1))? "Confirmed" : "" %>
                                    <%# (Convert.ToInt32(Eval("BookingStatus")).Equals(2))? "Pending" : "" %>
                                    <%# (Convert.ToInt32(Eval("BookingStatus")).Equals(3))? "Rejected" : "" %>
                                     <%# (Convert.ToInt32(Eval("BookingStatus")).Equals(4))? "Available" : "" %>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl TotalMiles">
                                    <%#Eval("TotalPoints")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%--<asp:HiddenField ID="hdnBookingRef" Value='<%#Eval("BookingReferenceId").ToString()%>'
                                        runat="server"></asp:HiddenField>--%>
                                    <asp:LinkButton ID="imgBtnCarVoucher" runat="server" Text="Print" OnClientClick='<%#String.Format("javascript:return ShowPackage(\"{0}\")",Eval("DistributorRef").ToString())%>'></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                   <div id="divPackagetrecord" runat="server">
                    <asp:Label runat="server" ID="lblPackagerecord" Visible="false"></asp:Label>
                   </div>
                 </aside>
                 </div>
                </div>
                </div>
             </div>
         </article>
    <!--End new-->

    <%--<div class="inr-wrap">
        <section class="myaccContent">
    <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
    <div class="myacc-conthdr adCntnr" id="Details1" style="max-height: 490px;">
        <div class="acco2">
            <!-- blk start-->
            <div class="myacc-statmntdtl expand openAd">
                Flight Booking Details</div>
            <div class="sumry-tbl myacc-bkngtbl collapse">
                <asp:Repeater ID="rptBookingDetails" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr class="tbl_th">
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Departure Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Return Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Details
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Ref No.
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Trip ID
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        WorldRewardZ
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Action
                                    </div>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr style="color: #000000" align="center">
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%# Convert.ToDateTime(DataBinder.Eval(Container, "DataItem.DepartureDate")).ToString("dd/MM/yyyy")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
<%# (Convert.ToDateTime(DataBinder.Eval(Container, "DataItem.ArrivalDate")).ToString("dd/MM/yyyy")) == "01/01/0001" ? "-" : (Convert.ToDateTime(DataBinder.Eval(Container, "DataItem.ArrivalDate")).ToString("dd/MM/yyyy")) %> 
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%# DataBinder.Eval(Container, "DataItem.OriginLocation")%>
                                    -
                                    <%# DataBinder.Eval(Container, "DataItem.DestinationLocation")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%# DataBinder.Eval(Container, "DataItem.ItineraryReference")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%# DataBinder.Eval(Container, "DataItem.ItineraryTripId")%><asp:HiddenField ID="hdnTripId"
                                        runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ItineraryReference")%>' />
                                    <asp:HiddenField ID="hdnReferrerId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.RefererDetails.Id")%>' />
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl TotalMiles">
                                    <%# DataBinder.Eval(Container, "DataItem.FareDetails.TotalPoints")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <asp:LinkButton ID="BtnViewAir" Text="Print" runat="server" OnClientClick='<%#String.Format("javascript:return ShowAirReceipt(\"{0}\")",Eval("ItineraryTripId").ToString())%>'></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div id="divFlightrecord" runat="server" style="background-color: #d2d3d4; border-radius: 3px;
                    color: #000; border: 1px solid #ccc;  padding: 4px 1.9%;
                    width: 96%" visible="false">
                    <asp:Label runat="server" ID="lblFlightrecord" Visible="false"></asp:Label>
                </div>
            </div>
            <!-- blk end-->
              <div class="clr">
        </div>
            <!-- blk start-->

            <div class="myacc-statmntdtl expand">
                Hotel Booking Details</div>
            <div class="sumry-tbl myacc-bkngtbl collapse">
                <asp:Repeater ID="rptHotelCancelBookingDetails" runat="server">
                    <HeaderTemplate>
                       <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr class="tbl_th">
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Check In Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Check Out Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Booking Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Details
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Ref No.
                                    </div>
                                </th>
                                <th class="vmid" style="width:13%">
                                    <div class="th-block-left">
                                        WorldRewardZ
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Status
                                    </div>
                                </th>
                                <th class="vmid" style="width: 6%;">
                                    <div class="th-block-left">
                                        Action
                                    </div>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr style="color: #000000" align="center">
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Convert.ToDateTime(Eval("searchCriteria.CheckInDate")).ToString("dd/MM/yyyy")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Convert.ToDateTime(Eval("searchCriteria.CheckOutDate")).ToString("dd/MM/yyyy")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Convert.ToDateTime(Eval("BookinDate")).ToString("dd/MM/yyyy")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("Hotel.basicinfo.hotelname")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("TransactionReferenceCode")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl TotalMiles">
                                    <%#Eval("TotalPoint")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%# (Convert.ToInt32(Eval("Status")).Equals(1))? "BOOKED" : "CANCELLED" %>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <asp:HiddenField ID="hdnCancelHotelBooking" Value='<%#String.Concat(Eval("ExternalBookingId"), "&bookid=", Eval("BookingId").ToString(),"&transRef=",Eval("TransactionReferenceCode").ToString())%>'
                                        runat="server"></asp:HiddenField>
                                    <asp:HiddenField ID="hdnTransRef" Value='<%#Eval("TransactionReferenceCode").ToString()%>'
                                        runat="server"></asp:HiddenField>
                                    <asp:LinkButton ID="imgBtnHotelVoucher" runat="server" Text="Print" Enabled='<%# Convert.ToInt32(Eval("Status")).Equals(1) %>'
                                        OnClientClick='<%#String.Format("javascript:return ShowHotelVoucher(\"{0}\")",Eval("TransactionReferenceCode").ToString())%>'></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div id="divHotelrecord" runat="server" style="background-color: #d2d3d4; border-radius: 3px;
                    color: #000; border: 1px solid #ccc;   margin-bottom: 2%; padding: 4px 1.9%;
                    width: 96%" visible="false">
                    <asp:Label runat="server" ID="lblHotelrecord" Visible="false"></asp:Label>
                </div>
            </div>
            <!-- blk end-->
              <div class="clr">
        </div>
            <!-- blk start-->
            <div class="myacc-statmntdtl expand">
                Car Booking Details</div>
            <div class="sumry-tbl myacc-bkngtbl collapse">
                <asp:Repeater ID="rptCarBookingDetails" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr class="tbl_th">
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Supplier ID
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Booking Ref ID
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Pick-up Details
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                    Drop-off Details
                                </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Booking Status
                                    </div>
                                </th>
                                <th class="vmid" style="width:13%">
                                    <div class="th-block-left">
                                        WorldRewardZ
                                    </div>
                                </th>
                                <th class="vmid" style="width: 6%;">
                                    <div class="th-block-left">
                                        Action
                                    </div>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("Booking.id")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("BookingReferenceId") %>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("Booking.PickUp[0].Location[0].country")%>,
                                    <%#Eval("Booking.PickUp[0].Location[0].city")%>,<br />
                                    <%#Eval("Booking.PickUp[0].Location[0].locName")%>,<br />
                                    <%#Eval("Booking.PickUp[0].Date[0].day")%>/<%#Eval("Booking.PickUp[0].Date[0].month")%>/<%#Eval("Booking.PickUp[0].Date[0].year")%>,
                                    <%#Eval("Booking.PickUp[0].Date[0].hour")%>:<%#Eval("Booking.PickUp[0].Date[0].minute")%></div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("Booking.DropOff[0].Location[0].country")%>,
                                    <%#Eval("Booking.DropOff[0].Location[0].city")%>,<br />
                                    <%#Eval("Booking.DropOff[0].Location[0].locName")%>,<br />
                                    <%#Eval("Booking.DropOff[0].Date[0].day")%>/<%#Eval("Booking.DropOff[0].Date[0].month")%>/<%#Eval("Booking.DropOff[0].Date[0].year")%>,
                                    <%#Eval("Booking.DropOff[0].Date[0].hour")%>:<%#Eval("Booking.DropOff[0].Date[0].minute")%></div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl" id="Carbookingstatus">
                                    <%#Eval("Booking.status")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl TotalMiles">
                                    <%#Eval("FinalPoints")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <asp:HiddenField ID="hdnBookingRef" Value='<%#Eval("BookingReferenceId").ToString()%>'
                                        runat="server"></asp:HiddenField>
                                    <asp:LinkButton ID="imgBtnCarVoucher" runat="server" Text="Print" OnClientClick='<%#String.Format("javascript:return ShowCarVoucher(\"{0}\")",Eval("BookingReferenceId").ToString())%>'></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div id="divCarrecord" runat="server" style="background-color: #d2d3d4; border-radius: 3px;
                    color: #000; border: 1px solid #ccc; margin-bottom: 2%;  padding: 4px 1.9%;
                    width: 96%" visible="false">
                    <asp:Label runat="server" ID="lblCarrecord" Visible="false"></asp:Label>
                </div>
            </div>
            <!-- blk end-->
              <div class="clr">
        </div>
             <div class="myacc-statmntdtl expand">
              Package Booking Details</div>
            <div class="sumry-tbl myacc-bkngtbl collapse">
                <asp:Repeater ID="rptPackageBookingDetails" runat="server">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr class="tbl_th">
                                <th class="vmid">
                                    <div class="th-block-left">
                                       Product Title
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Booking Ref ID
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Travel Date
                                    </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                  Itineray Id 
                                </div>
                                </th>
                                <th class="vmid">
                                    <div class="th-block-left">
                                        Booking Status
                                    </div>
                                </th>
                                <th class="vmid" style="width:13%">
                                    <div class="th-block-left">
                                        WorldRewardZ
                                    </div>
                                </th>
                                <th class="vmid" style="width: 6%;">
                                    <div class="th-block-left">
                                        Action
                                    </div>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <%#Eval("TravelSearchDetails.SupplierName")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                       <%#Eval("BookingRefId")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                   <%#Eval("BookingDate")%>
                                    </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                   <%#Eval("ItineraryId")%>
                                    </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl" id="Carbookingstatus">            

                                    <%# (Convert.ToInt32(Eval("BookingStatus")).Equals(1))? "Confirmed" : "" %>
                                    <%# (Convert.ToInt32(Eval("BookingStatus")).Equals(2))? "Pending" : "" %>
                                    <%# (Convert.ToInt32(Eval("BookingStatus")).Equals(3))? "Rejected" : "" %>
                                     <%# (Convert.ToInt32(Eval("BookingStatus")).Equals(4))? "Available" : "" %>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl TotalMiles">
                                    <%#Eval("TotalPoints")%>
                                </div>
                            </td>
                            <td class="bord_top">
                                <div class="td-block-left fl">
                                    <asp:HiddenField ID="hdnBookingRef" Value='<%#Eval("BookingReferenceId").ToString()%>'
                                        runat="server"></asp:HiddenField>
                                    <asp:LinkButton ID="imgBtnCarVoucher" runat="server" Text="Print" OnClientClick='<%#String.Format("javascript:return ShowPackage(\"{0}\")",Eval("DistributorRef").ToString())%>'></asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div id="divPackagetrecord" runat="server" style="background-color: #d2d3d4; border-radius: 3px;
                    color: #000; border: 1px solid #ccc; margin-bottom: 2%;  padding: 4px 1.9%;
                    width: 96%" visible="false">
                    <asp:Label runat="server" ID="lblPackagerecord" Visible="false"></asp:Label>
                </div>
            </div>
            <!-- blk end-->
              <div class="clr">
        </div>


        </div>
        </div>
        </section>
        <div class="clr">
        </div>
    </div>--%>
    <script>

        $(".myacc-bkngtbl").niceScroll({
            cursorcolor: "#e3443e",
            cursoropacitymin: 0.3,
            background: "#bbb",
            cursorborder: "0",
            autohidemode: false,
            cursorminheight: 30
        });

    </script>
</asp:Content>
