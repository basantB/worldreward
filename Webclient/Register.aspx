﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="css/mystyle.css" rel="stylesheet" />
    <script type="text/javascript">
        //$("#chkAgree").click(function () {
        //    $("#btnSubmit").attr("disabled", !this.checked);
        //});

        jQuery(document).ready(function ($) {
            //debugger;
            BindProgramDetails();
            //$('#btnSubmit').attr('disabled', true);
            //$('#chkAgree').click(function () {
            //    //check if checkbox is checked
            //    if ($(this).is(':checked')) {

            //        $('#btnSubmit').removeAttr('disabled'); //enable input
            //        $("#CP_ErrorMsgContainer").hide();
            //        $("#RegisterValidation")[0].innerHTML = "";
            //        $("#CP_lblLoginError").html('');

            //    } else {
            //        $('#btnSubmit').attr('disabled', true); //disable input
            //        $("#CP_ErrorMsgContainer").hide();
            //        $("#RegisterValidation")[0].innerHTML = "";
            //        $("#CP_lblLoginError").html('');
            //    }
            //});
        });

        function GenerateOTPForRegistration() {

            var msg = "";
            $("#CP_ErrorMsgContainer").hide();
            $("#RegisterValidation")[0].innerHTML = "";
            $("#CP_lblLoginError").html('');

            if ($("#CP_ddlProgram").prop('selectedIndex') == 0) {
                msg += "Please select program.<br/>";
            }
            if ($("#txtCustomerId").val().length == 0 || $("#txtCustomerId").val() == null) {
                msg += "Please Enter Customer Id.<br/>";
            }
            
            //if ($("#chkAgree").is(':checked')) {
            //     msg += "Please select program";
            //}
            //else {
            //    msg += "Please select program";
            //}
            if (msg.length > 0) {
                $("#RegisterValidation").css('color', 'red');
                $("#CP_ErrorMsgContainer").show();
                $("#RegisterValidation")[0].innerHTML = msg;
            }
            else {
                var ProgramId = $("#CP_ddlProgram").val();
                var CustomerId = $("#txtCustomerId").val();
                $.ajax({
                    url: 'Register.aspx/GenerateOTPForRegistration',
                    type: 'POST',  // or get
                    contentType: 'application/json; charset =utf-8',
                    data: "{'pstrCustomerId':'" + CustomerId.toString() + "'" + "," + "'pstrProgramId':'" + ProgramId.toString() + "'}",
                    dataType: 'json',
                    success: function (data) {
                        try {

                            var newData = data.d;

                            if ((newData != "") || newData != null) {
                                if (data.d == "Success") {
                                    $("#RegisterValidation")[0].innerHTML = "";
                                    //window.location.href = data.d;

                                    var form = $('<form action="VerifyOTP.aspx" method="post">' +
                                      '<input type="text" name="CustomerId" value="' + CustomerId + '" />' +
                                      '<input type="text" name="ProgramId" value="' + ProgramId + '" />' +
                                      '</form>');
                                    $('body').append(form);
                                    form.submit();
                                }
                                else {
                                    $("#RegisterValidation").css('color', 'red');
                                    $("#RegisterValidation")[0].innerHTML = "";
                                    $("#CP_ErrorMsgContainer").show();
                                    $("#CP_lblLoginError").html(newData);
                                }
                            }
                            else {
                                $("#RegisterValidation")[0].innerHTML = "";
                                $("#CP_ErrorMsgContainer").show();
                                $("#CP_lblLoginError").html('Please check the login details you have entered and try again.');
                            }
                        }
                        catch (e) {
                            alert(e);
                            return false;
                        }
                        return false;
                    },
                    error: function (errmsg) {

                    }
                });
            }
            return false;
        };
        function GenerateOTPOnEnter(e) {
            var ENTER_KEY = 13;
            var code = "";
            var code = (e.keyCode ? e.keyCode : e.which);

            if (code == ENTER_KEY) {
                GenerateOTPForRegistration();
                return false;
            }
            return true;
        };
        function BindProgramDetails() {
            $.ajax({
                type: 'POST',
                url: 'Register.aspx/BindProgramDetails',
                contentType: 'application/json; charset=utf-8',
                datatype: 'json',
                data: "",
                cache: false,
                success: function (data) {
                    var msg = $.parseJSON(data.d);
                    $("#CP_ddlProgram").empty();
                    $("#CP_ddlProgram").find('option').remove();
                    $("#CP_ddlProgram-button span").html('');
                    $("#CP_ddlProgram").append('<option value=0>Select Program</option>');
                    $("#CP_ddlProgram-button span").html('Select Program');
                    $.each(msg, function (val, msg) {
                        $("#CP_ddlProgram").append(
                $('<option></option>').val(msg.ProgramId).html(msg.ProgramName)
                    );
                    });

                }
            });
        };

    </script>
    <style>
        .myacc-login ul
        {
            margin-top:10px;
        }
        .input select{
            width: 100%;
            padding: 4% 5px;
        }
    </style>
    <div class="brds-cart-cont">
        <div class="brd-crms">
            <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
                <CurrentNodeStyle CssClass="select" />
                <RootNodeStyle CssClass="" />
            </asp:SiteMapPath>
        </div>
        <div class="clr">
        </div>
    </div>
    <div class="inr-wrap">
        <section class="statcpgCont">
        <div class="clr"></div>
      <aside class="pg-banr">
            <img src="images/banner-login.jpg" />
      </aside>
        <div class="flt-contehldr">
        <div class="flt-divR">
                    <div class="row">
                        <h2 class="robot">Register for WorldRewardZ </h2>
                    </div>
                    <div class="clear"></div>
                    <div class="myacc-login rgistrcontent">
                           <ul>
                               <li class="label">Select Program:</li>
                               <li class="input">
                                   <asp:DropDownList ID="ddlProgram" runat="server" ></asp:DropDownList>
                               </li>
                               <li class="clear disblk">&nbsp;</li>
                               <li class="label">Customer ID:</li>
                               <li class="input"><input type="text" id="txtCustomerId" class="robot" onkeypress="var retValue = GenerateOTPOnEnter(event); event.returnValue = retValue; return retValue;"/></li>
                               <li class="clear disblk">&nbsp;</li>
                             <%--  <li class="signup">Mother Maiden Name :</li>
                                  <li class="input"><input type="password" class="robot"/></li>
                               <li class="clear disblk">&nbsp;</li>
                               <li class="signup">Email ID :</li>
                               <li class="input"><input type="text" class="robot"/></li>
                               <li class="clear disblk">&nbsp;</li>
                              <li class="signup">Mobile No. :</li>
                               <li class="input"><input type="text" class="robot"/></li>--%>
                               <li class="clear disblk">&nbsp;</li>
                               <li class="signup">&nbsp;</li>
                               <li>
                                   <ul class="chkbxtxt">
                                       <li><input type="checkbox" id="chkAgree" /></li>
                                       <li><span> I agree to WorldRewardZ's </span><a href="TermsConditions.aspx">Terms & Conditions</a></li>
                                   </ul>
                               </li>
                               <li class="clear disblk">&nbsp;</li>
                               <li class="signup">&nbsp;</li>
                               <li class="input loginbtn"><input id="btnSubmit" type="button" class="robot" value="Submit" onclick="var retvalue = GenerateOTPForRegistration(); event.returnValue = retvalue; return retvalue;"/></li>
                               <li class="clear disblk">&nbsp;</li>
                           </ul>
                         <div id="ErrorMsgContainer" class="ErrorMsgContainer mtop_10" runat="server">
                                    <asp:Label runat="server" ID="lblLoginError" Text="" ForeColor="Red"></asp:Label>
                                    <div id="RegisterValidation">
                                    </div>
                                </div>
                       </div>  
            
                    </div>
        </div>      
    </section>
    </div>
</asp:Content>
