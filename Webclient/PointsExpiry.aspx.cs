﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfiPlanetModel.Model;
using Core.Platform.Member.Entites;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.ExpirySchedule.Entities;

public partial class PointsExpiry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["MemberDetails"] == null)
        {
            Response.Redirect("Login.aspx");
        }
        if (!IsPostBack)
        {
            try
            {
                GetProductProgramCurrencyDefinition();
                //Session["ExpiryProgramCurrency"] = "Accolades";
                if (Session["ExpiryProgramCurrency"].ToString().Equals("REWARDZ"))
                {
                    List<int> lintYearlst = new List<int>();
                    int lintYear = DateTime.Now.Year;
                    lintYearlst.Add(lintYear);
                    for (int i = 0; i < 3; i++)
                    {
                        lintYear = lintYearlst[i] + 1;
                        lintYearlst.Add(lintYear);
                    }

                    dtYear.DataSource = lintYearlst;
                    dtYear.DataBind();

                    InfiModel lobjInfiModel = new InfiModel();
                    ScheduleExpiry lobjScheduleExpiry = new ScheduleExpiry();
                    lobjScheduleExpiry = lobjInfiModel.GetExpirySchedule(Convert.ToInt32(dtYear.SelectedItem.Text));
                    MemberDetails lobjMemberDetails = null;
                    lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
                    if (lobjMemberDetails != null)
                    {
                        MemberRelation lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS));
                        if (lobjMemberRelation != null)
                        {
                            lobjScheduleExpiry.RelationReference = lobjMemberRelation.RelationReference;
                            lobjScheduleExpiry.RelationType = Convert.ToInt32(RelationType.LBMS);
                            lobjScheduleExpiry.TransactionCurrency = Session["ExpiryProgramCurrency"].ToString();
                            lobjScheduleExpiry = lobjInfiModel.GetTransactionExpirySchedule(lobjScheduleExpiry);
                            BindTable(lobjScheduleExpiry.ExpiryPeriod);
                            //rptExpirySchedule.DataSource = lobjScheduleExpiry.ExpiryPeriod;
                            //rptExpirySchedule.DataBind();
                            ScheduleExpiry lobjScheduleExpiryOnDate = new ScheduleExpiry();
                            lobjScheduleExpiryOnDate = lobjInfiModel.GetNextExpiredPointsOnDate(lobjScheduleExpiry, lintYear + 3);

                            if (lobjScheduleExpiryOnDate.ExpiryPeriod[0] == null)
                            {
                                divExpiredon.Style.Add("display", "none");
                            }
                            else
                            {
                                divExpiredon.Style.Add("display", "block");
                                lblMiles.Text = Convert.ToString(lobjInfiModel.FloatToThousandSeperated(lobjScheduleExpiryOnDate.ExpiryPeriod[0].TotalExpiredPoints));
                                lblDate.Text = lobjScheduleExpiryOnDate.ExpiryPeriod[0].ScheduleDate.ToString("d MMMM, yyyy");
                               
                            }
                        }
                    }
                    Session["ExpirySchedule"] = lobjScheduleExpiry;
                }
                else
                {
                    divExpiredon.Style.Add("display", "none");
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Point Expiry Page load -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
            }
        }
    }

    private void BindTable(List<ExpiryPeriod> ExpiryPeriod)
    {
        InfiModel lobjInfiModel = new InfiModel();
        string strExpiryPeriod = "<table  cellpadding='10' bordercolor='#e7e7e8' border='1' style='border-color: #e7e7e8'><tr bgcolor='#939598' style='color: #FFF' align='center'><th style='color: #58595b;'>WorldRewardz Expiry Schedule</th><th style='color: #58595b;'>WorldRewardz Points Due To Expire</th></tr>";
        string strExpiryPoints = "<tr style='color: #939598' align='center'>";
        for (int icount = 0; icount < ExpiryPeriod.Count; icount++)
        {
            strExpiryPoints += "<td>" + setDate(ExpiryPeriod[icount].ScheduleDate) + "</td>";
            strExpiryPoints += "<td>" + lobjInfiModel.FloatToThousandSeperated(ExpiryPeriod[icount].TotalExpiredPoints) + "</td></tr>";
        }

        rptExpirySchedule.InnerHtml = strExpiryPeriod  + strExpiryPoints + "</table>";
    }

    private string setDate(DateTime dateTime)
    {
        string strNewdate = String.Format("{0:d MMM, yyyy}", dateTime);
        return strNewdate;
    }


    public void GetProductProgramCurrencyDefinition()
    {
        string lstrResponse = string.Empty;
        InfiModel lobjInfiModel = new InfiModel();
        List<ProgramCurrencyDefinition> lobjListProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
        ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();
        MemberDetails lobjMemberDetails = new MemberDetails();

        lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
        lobjListProgramCurrencyDefinition = lobjInfiModel.GetProductProgramCurrencyDefinition(lobjMemberDetails);
        if (lobjListProgramCurrencyDefinition.Count > 0)
        {
            lobjProgramCurrencyDefinition = lobjListProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals("REWARDZ"));
            if (lobjProgramCurrencyDefinition != null)
            {
                HttpContext.Current.Session["ExpiryProgramCurrency"] = "REWARDZ";
            }
        }
    }

    protected void dtYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        ScheduleExpiry lobjScheduleExpiry = Session["ExpirySchedule"] as ScheduleExpiry;
        if (dtYear.SelectedItem.Text != Convert.ToString(DateTime.Now.Year))
        {
            MemberDetails lobjMemberDetails = null;
            lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
            MemberRelation lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS));
            InfiModel lobjInfiModel = new InfiModel();
            lobjScheduleExpiry.RelationReference = lobjMemberRelation.RelationReference;
            lobjScheduleExpiry.RelationType = Convert.ToInt32(RelationType.LBMS);
            lobjScheduleExpiry.TransactionCurrency = Session["ExpiryProgramCurrency"].ToString();
            lobjScheduleExpiry = lobjInfiModel.GetNextExpirySchedule(lobjScheduleExpiry, Convert.ToInt32(dtYear.SelectedItem.Text));
        }
        
        BindTable(lobjScheduleExpiry.ExpiryPeriod);
    }
}