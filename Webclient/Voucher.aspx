﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="Voucher.aspx.cs" Inherits="Voucher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link rel="stylesheet" href="css/Voucher.css" type="text/css" />
    <link rel="stylesheet" href="css/dropDown.css" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/Voucher.js"></script>
    <script src="js/jquery.selectric.min.js" type="text/javascript"></script>
    <style type="text/css">
        @media screen and (max-device-width :768px), screen and (max-width:768px)
        {
        
            .pass-desktop1
            {
                display: none;
            }
        
            .pass-mobile1
            {
                display: block;
            }
        }
        
        @media only screen and (min-width:769px) and (orientation:landscape)
        {
            .pass-desktop1
            {
                display: block;
            }
        
            .pass-mobile1
            {
                display: none;
            }
        }
        table td
        {
            text-align: center;
        }
    </style>
    <script src="js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="js/jquery.mCustomScrollbar.css" type="text/css" />
    <script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $("#voucher").niceScroll({
                cursorcolor: "#e3443e",
                cursoropacitymin: 0.3,
                background: "#bbb",
                cursorborder: "0",
                autohidemode: false,
                cursorminheight: 30
            });
            $("#CP_lblVoucherCurrency").text($("#CP_lblVoucherCurrency").text().replace("REWARDZ", "WorldRewardZ"));

            $("#dnarr").on('click', function (event) {
                if (this.hash !== "") {
                    event.preventDefault();
                    var hash = this.hash;
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 800, function () {
                        window.location.hash = hash;
                    });
                }
            });
        });
    </script>
    <script type="text/javascript" language="javascript">
        function preventMultipleSubmissions() {
            $('#CP_btnConfirm').hide();
            setTimeout(function () {
                $('#CP_btnConfirm').show();
            }, 10000);
            return true;
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="banner">
    <div style="max-width: 100%;" class="bx-wrapper"><div style="width: 100%; overflow: hidden; position: relative; height: 436px;" class="bx-viewport"><ul style="width: auto; position: relative;" class="bxslider" id="Indexbanner">
        <li>
       <img src="images/bnr-gift.jpg"></li>
        </ul></div>
	 </div>
    </div>
     
	<div style="text-align: center; margin-top: 10px;">
        <a href="#article" id="dnarr">
            <img style="opacity: .5; height: 50px;" src="images/down-arrow.gif"></a>
    </div>

        <article id="article">
            <div class="wrap">
                <h1>Every Reason To Feel Wonderful.</h1>
                <div class="servindx">
                    <%--<div class="servindx-bnr">
                        <img src="images/bnr-giftvoucher.jpg" />
                    </div>--%>
                    <div id="voucher" class="tabbertabVoucher voucher_cnf">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <script type="text/javascript">
                                    function commaFormat(inputString) {
                                        inputString = inputString.toString();
                                        var decimalPart = "";
                                        if (inputString.indexOf('.') != -1) {
                                            inputString = inputString.split(".");
                                            decimalPart = "." + inputString[1];
                                            inputString = inputString[0];
                                        }
                                        var outputString = "";
                                        var count = 0;
                                        for (var i = inputString.length - 1; i >= 0 && inputString.charAt(i) != '-'; i--) {
                                            if (count == 3) {
                                                outputString += ",";
                                                count = 0;
                                            }
                                            outputString += inputString.charAt(i);
                                            count++;
                                        }
                                        if (inputString.charAt(0) == '-') {
                                            outputString += "-";
                                        }
                                        return outputString.split("").reverse().join("") + decimalPart;
                                    };


                                </script>
                                <p>Let your gift be as special as the thought itself. At WorldRewardZ, you can now redeem your points for a gift voucher from leading brands.
                                     You have the choice of vouchers across various value denominations, to make gifting more joyful and every moment big or small, memorable!</p>
                                  
                                <!--new html-->
                            <div id="DivSubmitContent" runat="server">
                             <div class="vouWrap">
			                 <ul>
				             <li>
					        <h2>WorldRewardz<br>Gift Voucher</h2>
					       <div class="calDiv giftvouchr">
						   <span>100</span> <em>x</em>
						<div class="selectdiv flotihert">
						<asp:DropDownList ID="ddlQAR100" runat="server" class="Location_TextBox_Style_voucher mLeft5p">
                            <asp:ListItem Value="0">Qty</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                        </asp:DropDownList>
						</div>
					 </div>
					<br class="clr">
					<span class="Add" id="divQAR100Add">
					<a href="#" class="addBtn" id="btn100" onclick="var retvalue = selectQty(100, '#CP_ddlQAR100', '#divQAR100Add'); event.returnValue = retvalue; return retvalue;">Add</a>
                    </span>
				  </li>
				 <li>
					<h2>WorldRewardz<br>Gift Voucher</h2>
					<div class="calDiv giftvouchr">
						<span>100</span> <em>x</em>
						<div class="selectdiv flotihert">
						  <asp:DropDownList ID="ddlQAR500" runat="server" class="Location_TextBox_Style_voucher mLeft5p">
                            <asp:ListItem Value="0">Qty</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                        </asp:DropDownList>
						</div>
					</div>
					<br class="clr">
                    <span class="Add" id="divQAR500Add">
					<a href="#" class="addBtn" id="btn500" onclick="var retvalue = selectQty(500, '#CP_ddlQAR500', '#divQAR500Add'); event.returnValue = retvalue; return retvalue;">Add</a>
                     </span> 
				</li>

				<li>
					<h2>WorldRewardz<br>Gift Voucher</h2>
					<div class="calDiv giftvouchr">
						<span>100</span> <em>x</em>
						<div class="selectdiv flotihert">
						  <asp:DropDownList ID="ddlQAR1000" runat="server" class="Location_TextBox_Style_voucher mLeft5p">
                            <asp:ListItem Value="0">Qty</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                        </asp:DropDownList>
						</div>
					</div>
					<br class="clr">
                    <span class="Add" id="divQAR1000Add">
					<a href="#" class="addBtn" id="btn1000" onclick="var retvalue = selectQty(1000, '#CP_ddlQAR1000', '#divQAR1000Add'); event.returnValue = retvalue; return retvalue;">Add</a>
                     </span> 
				</li>
			</ul>
			<div class="btmInfo">
				<div class="divLeft">
					<p>Total Voucher Value in WorldRewardZ: <span><asp:Label runat="server" ID="lblTotalValue" Text=""></asp:Label></span></p>
					<p>No.of WorldRewardZ required to redeem: <span><asp:Label runat="server" ID="lblPointRequired" Text=""></asp:Label></span></p>	
				</div>
				<div class="divRight">
                <span class="Clear" id="divClearAll">
				<a href="#" class="btnBlk" id="imgBtnClearAll" onclick="return ClearFields()">Clear All</a>
                </span>
                <span id="confirmbtn">
				<span id="DivVoucherSubmit"><a href="#">
                    <asp:Button runat="server" ID="Button3" class="btnRed" Text="Submit" OnClientClick="var retvalue = ConfirmVoucher(); event.returnValue= retvalue; return retvalue;" />
                    </a>
				</span>
                </span>
                </div>
                <div class=" msgbxBG mTop20 display_hide" id="validateMsg">
                </div>

				</div>
				<br class="clr">
            <div id="div1" runat="server" class="error-bgfl-way error-bg display_hide">
                <asp:Label ID="Label1" runat="server"></asp:Label>
            </div>
		</div>
       </div>
        <!--end of new html-->

                                <div class="Ourproduct_Rightpannel">
                                    <%--<div class="width100  " id="DivSubmitContent" runat="server">
                
                                        <div class="clr">
                                        </div>
                                        <div class="width100 rightinnercontent ">
                                            <div class="width100 mtop10">
                                                select the following denomination:
                                            </div>
                                            <div class="width100 mtop5p">
                                                <div class="width100 padding10p">
                                                    <div class="width100 mtop5p">
                                                        <div class="width30 ptop5">
                                                            worldrewardz 100 gift voucher
                                                        </div>
                                                        <div class="width10 ptop5 aligncenter">
                                                            x
                                                        </div>
                                                        <div class="width30 ">
                                                            <div class="width100 textfromdiv">
                                                                <div class="input-bg fl">
                                                                    <asp:dropdownlist id="ddlqar100" runat="server" class="location_textbox_style_voucher mleft5p">
                                                                        <asp:listitem value="0">quantity</asp:listitem>
                                                                        <asp:listitem>1</asp:listitem>
                                                                        <asp:listitem>2</asp:listitem>
                                                                        <asp:listitem>3</asp:listitem>
                                                                        <asp:listitem>4</asp:listitem>
                                                                        <asp:listitem>5</asp:listitem>
                                                                    </asp:dropdownlist>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="width15 add" id="divqar100add">
                                                            <input value="add" type="button" id="btn100" onclick="var retvalue = selectqty(100, '#cp_ddlqar100', '#divqar100add'); event.returnvalue = retvalue; return retvalue;"
                                                                style="cursor: pointer" />
                                                        </div>
                                                    </div>
                                                    <div class="width100 mtop5p">
                                                        <div class="width30 ptop5">
                                                            worldrewardz 500 gift voucher
                                                        </div>
                                                        <div class="width10 ptop5 aligncenter">
                                                            x
                                                        </div>
                                                        <div class="width30 ">
                                                            <div class="width100 textfromdiv">
                                                                <div class="input-bg fl">
                                                                    <asp:dropdownlist id="ddlqar500" runat="server" class="location_textbox_style_voucher mleft5p">
                                                                        <asp:listitem value="0">quantity</asp:listitem>
                                                                        <asp:listitem>1</asp:listitem>
                                                                        <asp:listitem>2</asp:listitem>
                                                                        <asp:listitem>3</asp:listitem>
                                                                        <asp:listitem>4</asp:listitem>
                                                                        <asp:listitem>5</asp:listitem>
                                                                    </asp:dropdownlist>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="width15 add" id="divqar500add">
                                                            <input type="button" value="add" id="btn500" onclick="var retvalue = selectqty(500, '#cp_ddlqar500', '#divqar500add'); event.returnvalue = retvalue; return retvalue;"
                                                                style="cursor: pointer" />
                                                        </div>
                                                    </div>
                                                    <div class="width100 mtop5p">
                                                        <div class="width30 ptop5">
                                                            worldrewardz 1000 gift voucher
                                                        </div>
                                                        <div class="width10 ptop5 aligncenter">
                                                            x
                                                        </div>
                                                        <div class="width30 ">
                                                            <div class="width100 textfromdiv">
                                                                <div class="input-bg fl">
                                                                    <asp:dropdownlist id="ddlqar1000" runat="server" class="location_textbox_style_voucher mleft5p">
                                                                        <asp:listitem value="0">quantity</asp:listitem>
                                                                        <asp:listitem>1</asp:listitem>
                                                                        <asp:listitem>2</asp:listitem>
                                                                        <asp:listitem>3</asp:listitem>
                                                                        <asp:listitem>4</asp:listitem>
                                                                        <asp:listitem>5</asp:listitem>
                                                                    </asp:dropdownlist>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="width15 add" id="divqar1000add">
                                                            <input type="button" value="add" id="btn1000" onclick="var retvalue = selectqty(1000, '#cp_ddlqar1000', '#divqar1000add'); event.returnvalue = retvalue; return retvalue;"
                                                                style="cursor: pointer" />
                                                        </div>
                                                    </div>
                                                    <div class="width100  mb10">
                                                        <div class="width30 ptop5">
                                                        </div>
                                                        <div class="width10 ptop5 aligncenter">
                                                        </div>
                                                        <div class="width30">
                                                            <div class="width15 clear" id="divclearall">
                                                                <input type="button" value="clear all" id="imgbtnclearall" onclick="return clearfields()"
                                                                    style="cursor: pointer" />
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divvouchererror" runat="server" class="error-bgfl-way error-bg display_hide">
                                                        <asp:label id="lblvouchererror" runat="server"></asp:label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="width100 mtop20">
                                                <div class="width64 ptop5">
                                                    total voucher value in worldrewardz:
                                                </div>
                                                <div class="width30 textfromdiv">
                                                    <div class="width100" style="padding: 6px 0 0 10px">
                                                        <asp:label runat="server" id="lbltotalvalue" text=""></asp:label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="width100 mtop10">
                                                <div class=" ptop5">
                                                    number of worldrewardz required to redeem:
                                                    <asp:label runat="server" id="lblpointrequired" text=""></asp:label>
                                                </div>
                                                <div class="width30 textfromdiv">
                                                    <div class="width100" style="padding: 6px 0 0 10px">
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="width100 mtop10" id="confirmbtn" style="display:none">
                                                <div class="width30 ">
                                                    <div class="buttoncontainer darkbtn" style="width: 100px">
                                                        <div class="buttonright">
                                                            <div class="buttonleft">
                                                                <div class="buttoncontainer ptop5" id="divvouchersubmit" style="padding-top: 1px;">
                                                                    <asp:button runat="server" id="btnsubmit" text="submit" onclientclick="var retvalue = confirmvoucher(); event.returnvalue= retvalue; return retvalue;" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=" msgbxbg mtop20 display_hide" id="validatemsg">
                                            </div>
                                        </div>
                                    </div>--%>
                                    <div class="width100 display_hide" id="DivGlobalRewardsConfirm" runat="server">
                                         <!--Start Voucher Confirm-->
                                          <div id="divCurrencyDropDown" runat="server" class="Width100 display_hide pb10">
                                                Please select your currency : &nbsp
                                                <asp:DropDownList ID="ddlProgramCurrency" runat="server" AppendDataBoundItems="true"
                                                    OnSelectedIndexChanged="ddlProgramCurrency_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                          <div class="vouWrap vouConfirm">
			                                <table cellspacing="0" cellpadding="0" border="0">
                                             <thead>
                                                    <tr>
                                                    <th>
                                                        Voucher Type
                                                    </th>
                                                    <th>
                                                        Quantity
                                                    </th>
                                                    <th>
                                                        Voucher Value
                                                    </th>
                                                    <th>
                                                        WorldRewardZ Required
                                                    </th>
                                                </tr>
                                            </thead>
        
                                            <tbody>
                                            <tr id="trQAR100" runat="server">
                                                <td>
                                                    WorldRewardZ Gift Voucher - 100
                                                </td>
                                                <td>
                                                   <asp:Label runat="server" ID="lblQAR100Quantity"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblQar100VoucherVal"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblQAR100PointsRequired"></asp:Label>
                                                </td>

                                            </tr>
                                                <tr id="trQAR500" runat="server">
                                                <td>
                                                    WorldRewardZ Gift Voucher - 100
                                                </td>
                                                <td>
                                                   <asp:Label runat="server" ID="lblQAR500Quantity"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblQar500VoucherVal"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblQAR500PointsRequired"></asp:Label>
                                                </td>

                                            </tr>
                                                <tr id="trQAR1000" runat="server">
                                                <td>
                                                    WorldRewardZ Gift Voucher - 100
                                                </td>
                                                <td>
                                                   <asp:Label runat="server" ID="lblQAR1000Quantity"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblQar1000VoucherVal"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblQAR1000PointsRequired"></asp:Label>
                                                </td>

                                            </tr>
                                            </tbody>
        
                                            <tbody>
            
                                             </tbody> 

			                                </table>
                                              <div class="width100 pTop10">
                                                Total Voucher Value is
                                                <asp:Label runat="server" ID="lblTotalVoucherValue"></asp:Label>
                                                WorldRewardZ, to redeem this you require
                                                <asp:Label runat="server" ID="lblRequirPointToRedeem"></asp:Label>
                                                <asp:Label runat="server" ID="lblVoucherCurrency" Text=""></asp:Label>
                                                .
                                            </div>
			                                <div class="btmInfo1">
				                                <div class="divRight">
				                                <a href="#">
                                                <asp:Button runat="server" class="btnBlk" ID="Button2" Text="Back" OnClientClick="var retvalue =BackToDenomination(); event.returnValue= retvalue; return retvalue;"
                                                                    CausesValidation="false" /> </a>
                                                <span id="divBtnConfirm" runat="server">
				                                <a href="#">
                                                <asp:Button CssClass="buttonContainer btnRed"  runat="server" ID="btnConfirm" Text="Confirm"
                                                       CausesValidation="false" OnClick="btnConfirm_Click" OnClientClick="return preventMultipleSubmissions();" />
                                                    </a>
                                                </span>
				                                </div>
				                                <br class="clr">
			                                </div>
		                                    </div>
                                          <!--End of Voucher Confirm-->
                    
                                        <%--<div class="width100 RightInnerContent">
                                            <div id="divCurrencyDropDown" runat="server" class="Width100 display_hide pb10">
                                                Please select your currency : &nbsp
                                                <asp:DropDownList ID="ddlProgramCurrency" runat="server" AppendDataBoundItems="true"
                                                    OnSelectedIndexChanged="ddlProgramCurrency_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="width100 pb10">
                                                Please find below the summary of WorldRewardZ Voucher:
                                            </div>
                                            <div class="Width99" style="border: 2px solid #e3152a;">
                                                <table width="100%" border="0" cellspacing="1" cellpadding="6" bgcolor="#e3152a">
                                                    <tr class="txtColorWhite">
                                                        <td width="31%" align="left">
                                                            Voucher type
                                                        </td>
                                                        <td width="23%" align="center">
                                                            Quantity
                                                        </td>
                                                        <td width="23%" align="center">
                                                            Voucher value
                                                        </td>
                                                        <td width="23%" align="center">
                                                            WorldRewardZ required
                                                        </td>
                                                    </tr>
                                                    <tr id="trQAR100" runat="server">
                                                        <td bgcolor="#FFFFFF">
                                                            WorldRewardZ 100 Gift Voucher
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <asp:Label runat="server" ID="lblQAR100Quantity"></asp:Label>
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <asp:Label runat="server" ID="lblQar100VoucherVal"></asp:Label>
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <asp:Label runat="server" ID="lblQAR100PointsRequired"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trQAR500" runat="server">
                                                        <td bgcolor="#FFFFFF">
                                                            WorldRewardZ 500 Gift Voucher
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <asp:Label runat="server" ID="lblQAR500Quantity"></asp:Label>
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <asp:Label runat="server" ID="lblQar500VoucherVal"></asp:Label>
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <asp:Label runat="server" ID="lblQAR500PointsRequired"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trQAR1000" runat="server">
                                                        <td bgcolor="#FFFFFF">
                                                            WorldRewardZ 1000 Gift Voucher
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <asp:Label runat="server" ID="lblQAR1000Quantity"></asp:Label>
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <asp:Label runat="server" ID="lblQar1000VoucherVal"></asp:Label>
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <asp:Label runat="server" ID="lblQAR1000PointsRequired"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="width100 pTop10">
                                                Total Voucher Value is
                                                <asp:Label runat="server" ID="lblTotalVoucherValue"></asp:Label>
                                                WorldRewardZ, to redeem this you require
                                                <asp:Label runat="server" ID="lblRequirPointToRedeem"></asp:Label>
                                                <asp:Label runat="server" ID="lblVoucherCurrency" Text=""></asp:Label>
                                                .
                                            </div>
                                            <div class="width100 mTop10">
                                                <div class="fl" style="padding-right: 15px;">
                                                    <div class="buttonContainer darkBtn" style="width: 100px">
                                                        <div class="buttonRight">
                                                            <div class="buttonLeft">
                                                                <asp:Button runat="server" ID="Button2" Text="Back" OnClientClick="var retvalue = BackToDenomination(); event.returnValue= retvalue; return retvalue;"
                                                                    CausesValidation="false" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fl">
                                                    <div id="divBtnConfirm" runat="server">
                                                        <div class="buttonRight">
                                                            <div class="buttonLeft">
                                                                <asp:Button CssClass="buttonContainer" runat="server" ID="btnConfirm" Text="Confirm"
                                                                    CausesValidation="false" OnClick="btnConfirm_Click" OnClientClick="return preventMultipleSubmissions();" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="width100 display_hide" id="divCreateVoucherSummary" runat="server">
                                        <div class="font20 fl width100 pTop10 pb10">
                                            Gift Voucher
                                        </div>
                                        <div class="width100 RightInnerContent">
                                            <div class="width100 pb10">
                                                Please find below the summary of WorldRewardZ Voucher:
                                            </div>
                                            <div class="Width99" style="border: 2px solid #e3152a;">
                                                <asp:Repeater ID="RepCreateVoucherSummary" runat="server">
                                                    <HeaderTemplate>
                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="border_Voucher">
                                                            <tr class="stylegridheader">
                                                                <td>
                                                                    Sr. No.
                                                                </td>
                                                                <td>
                                                                    Amount
                                                                </td>
                                                                <td>
                                                                    Status
                                                                </td>
                                                                <td>
                                                                    Voucher No.
                                                                </td>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr class="stylealternate1">
                                                            <td align="center" valign="middle" height="25" class="borderTop_Voucher">
                                                                <%# Container.ItemIndex + 1%>
                                                            </td>
                                                            <td align="center" valign="middle" class="borderTop_Voucher">
                                                                <%# Eval("Amount","{0:0,0}")%>
                                                            </td>
                                                            <td align="center" valign="middle" class="borderTop_Voucher">
                                                                <asp:Label ID="lblStatus" runat="server" Text='<%# (Boolean.Parse(Eval("Status").ToString())) ? "Successful" : "Failed"%>'></asp:Label>
                                                            </td>
                                                            <td align="center" valign="middle" class="borderTop_Voucher">
                                                                <asp:Label ID="lblVoucherNo" runat="server" Text='<%# Eval("VoucherNo")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                        <asp:Label ID="lblErrorMsg" runat="server" CssClass="errMsg" Text="No Records Found."
                                                            Visible="false" />
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="width100 msgbxBG mTop20 display_hide" id="divConfirmationMsg" runat="server">
                                        Dear Customer,<br />
                                        <br />
                                        Congratulations. Your transaction is successful. To view a record of all your Gift
                                        Voucher codes, click on <b>“My Account”</b> and scroll down to <b>“Voucher Summary”</b>
                                        and you may print your Gift Voucher codes. Use your Gift Voucher codes to redeem
                                        at any of our participating partners.
                                    </div>
                                    <div class="width100  msgbxBG mTop20 display_hide" id="divCurrencyNotFound" runat="server">
                                        Dear Customer,<br />
                                        <br />
                                        This currency does not supports Gift Voucher redemption.
                                    </div>
                                    <div class="width100  msgbxBG mTop20 display_hide" id="divAvailability" runat="server">
                                        Dear Customer,<br />
                                        <br />
                                        You do not have sufficient WorldRewardZ.<br />
                                        <a href="PurchasePoints.aspx">Click here </a>to purchase additional WorldRewardZ.
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnConfirm" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="clr">
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
            </div>
       </article>
    </div>
    <asp:HiddenField ID="HDFQARAmount" Value="0" runat="server" />
    <asp:HiddenField ID="HDFPoints" runat="server" />
    <asp:HiddenField ID="HDFVoucherRate" runat="server" />
    <asp:HiddenField ID="HDFQAR100QTY" runat="server" Value="0" />
    <asp:HiddenField ID="HDFQAR500QTY" runat="server" Value="0" />
    <asp:HiddenField ID="HDFQAR1000QTY" runat="server" Value="0" />
</asp:Content>
