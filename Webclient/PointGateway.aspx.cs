﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Member.Entites;
using CB.IBE.Platform.Car.ClientEntities;
using CB.IBE.Platform.Car.Entities;
using Core.Platform.MemberActivity.Entities;
using Core.Platform.MemberActivity.Constants;
using CB.IBE.Platform.Hotels.ClientEntities;
using Framework.EnterpriseLibrary.Adapters;
using CB.IBE.Platform.ClientEntities;
using CB.IBE.Platform.Masters.Entities;
using CB.IBE.Platform.Entities;
using Core.Platform.Booking.Entities;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Transactions.Entites;
using InfiPlanetModel.Model;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;
using InfiPlanet.Entities;
using Framework.Integrations.Hotels.Entities;

public partial class PointGateway : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // PaymentDetails lobjPaymentDetails = Session["PGTransactionDetails"] as PaymentDetails;
       // MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
       // CreateItineraryResponse lobjCreateItineraryResponse = Session["ItineraryResponse"] as CreateItineraryResponse;
       // CreateItineraryRequest lobjCreateItineraryRequest = Session["ItineraryRequest"] as CreateItineraryRequest;
       // SearchRequest lobjSearchRequest = Session["FlightSearchDetails"] as SearchRequest;
       // BookingPaymentDetails lobjFlightBookingPaymentDetails = Session["FlightBookingPaymentDetails"] as BookingPaymentDetails;
       // HotelSearchResponse lobjHotelSearchResponse = Session["BookedHotel"] as HotelSearchResponse;
       // Customer lobjCustomer = Session["CustomerDetails"] as Customer;
       // HotelSearchRequest lobjHotelSearchRequest = Session["SearchDetails"] as HotelSearchRequest;
       // BookingPaymentDetails lobjBookingPaymentDetails = Session["HotelBookingPaymentDetails"] as BookingPaymentDetails;


       // CarSearchRequest lobjCarSearchRequest = HttpContext.Current.Session["CarSearchRequest"] as CarSearchRequest;
       // CarSearchResponse lobjCarSearchResponse = HttpContext.Current.Session["Cars"] as CarSearchResponse;
       // Match lobjCarSelected = Session["CarSelected"] as Match;
       // CarExtrasListResponse lobjCarExtrasListResponse = HttpContext.Current.Session["CarExtraListResponse"] as CarExtrasListResponse;
       // CarMakeBookingRequest lobjCarMakeBookingRequest = HttpContext.Current.Session["CarMakeBookingRequest"] as CarMakeBookingRequest;
       // BookingPaymentDetails lobjCarBookingPaymentDetails = Session["CarBookingPaymentDetails"] as BookingPaymentDetails;

       // CarMakeBookingResponse lobjCarMakeBookingResponse = Session["CarMakeBookingResponse"] as CarMakeBookingResponse;
       // CarMakeBookingRequest lobjCarMadeBookingRequest = Session["CarMadeBookingRequest"] as CarMakeBookingRequest;
       // CarMakeBookingResponse lobjCarMakeFailedBookingResponse = Session["CarBookedFailedResponse"] as CarMakeBookingResponse;

       // CB.IBE.Platform.ClientEntities.BookingResponse lobjBookingResponse = HttpContext.Current.Session["FlightBookedFailedResponse"] as CB.IBE.Platform.ClientEntities.BookingResponse;

       // List<RedemptionDetails> lobjRedemptionDetails = Session["RedemptionDetails"] as List<RedemptionDetails>;
       // string lstrBookingFlag = Convert.ToString(Session["BookingFlag"]);

       // string lstTxnId = Convert.ToString(Guid.NewGuid());

       // List<object> lobj = new List<object>();
       // lobj.Add(lobjMemberDetails);
       // lobj.Add(lobjCreateItineraryResponse);
       // lobj.Add(lobjCreateItineraryRequest);
       // lobj.Add(lobjPaymentDetails);
       // lobj.Add(lobjSearchRequest);

       // lobj.Add(lobjFlightBookingPaymentDetails);
       // lobj.Add(lobjHotelSearchResponse);
       // lobj.Add(lobjCustomer);
       // lobj.Add(lobjHotelSearchRequest);
       // lobj.Add(lobjBookingPaymentDetails);
       // lobj.Add(lobjCarSearchRequest);
       // lobj.Add(lobjCarSearchResponse);
       // lobj.Add(lobjCarSelected);
       // lobj.Add(lobjCarExtrasListResponse);
       // lobj.Add(lobjCarMakeBookingRequest);
       // lobj.Add(lobjCarBookingPaymentDetails);
       // lobj.Add(lobjCarMakeBookingResponse);
       // lobj.Add(lobjCarMadeBookingRequest);
       // lobj.Add(lobjCarMakeFailedBookingResponse);
       // lobj.Add(lobjBookingResponse);
       // lobj.Add(lobjRedemptionDetails);
       // lobj.Add(lstrBookingFlag);

       // CachingAdapter.Add(lstTxnId, lobj);

       // string lstrRedemptionType = string.Empty;
       // float lfltAmount = 1.0F;//lobjRedemptionDetails.Amount;
       // int lintPoints = 10;//lobjRedemptionDetails.Points;

       // ///Program Merchant Config Details///
       // string lstrSourceCurrency = string.Empty;
       // string lstSecretkey = string.Empty;
       // string lstrMerchantId = string.Empty;
       // string lstrMerchantName = string.Empty;

       // if (Session["MemberDetails"] != null)
       // {
       //     LoggingAdapter.WriteLog("PointGateway Session Start:");
       //     InfiModel lobjInfiModel = new InfiModel();
       //     ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
       //     lobjProgramDefinition.ProgramId = lobjMemberDetails.ProgramId;

       //     lobjProgramDefinition = lobjInfiModel.GetProgramDetailsByID(lobjProgramDefinition.ProgramId);
       //     LoggingAdapter.WriteLog("PointGateway ProgramId:" + lobjProgramDefinition.ProgramId);

       //     if (lobjProgramDefinition != null)
       //     {
       //         ProgramAttribute lobjProgramAttribute = new ProgramAttribute();

       //         lobjProgramAttribute = lobjInfiModel.GetProgramAttributes(lobjProgramDefinition.ProgramId);
       //         LoggingAdapter.WriteLog("PointGateway ProgramAttribute:" + lobjProgramDefinition.ProgramId);
       //         if (lobjProgramAttribute != null)
       //         {
       //             lstrSourceCurrency = lobjProgramAttribute.BaseCurrency;
       //             LoggingAdapter.WriteLog("PointGateway Source Currency:" + lstrSourceCurrency);
       //         }


       //         ProgramMerchantConfig lobjProgramMerchantConfig = new ProgramMerchantConfig();
       //         lobjProgramMerchantConfig.ProgramId = lobjMemberDetails.ProgramId;
       //         lobjProgramMerchantConfig.LoyaltyTxnType = LoyaltyTxnType.Air;
       //         LoggingAdapter.WriteLog("PointGateway ProgramId:" + lobjProgramMerchantConfig.ProgramId + "LoyaltyTxnType" + LoyaltyTxnType.Air);
       //         lobjProgramMerchantConfig = lobjInfiModel.GetMerchantDetailsByProgramIdAndLoyaltyTxnType(lobjProgramMerchantConfig);

       //         LoggingAdapter.WriteLog("PointGateway ProgramMerchantConfig:" + lobjProgramMerchantConfig);
       //         if (lobjProgramMerchantConfig != null && lstrSourceCurrency != "")
       //         {
       //             lstSecretkey = lobjProgramMerchantConfig.SecreteKey;
       //             lstrMerchantId = lobjProgramMerchantConfig.MerchantId.ToString();
       //             lstrMerchantName = lobjProgramMerchantConfig.MerchantName;
       //             LoggingAdapter.WriteLog("PointGateway SecreteKey:" + lstSecretkey + ": MerchantId:" + lstrMerchantId + ":MerchantName:" + lstrMerchantName);
       //         }
       //         else
       //         {
       //             lstSecretkey = ConfigurationSettings.AppSettings["lstSecretkey"].ToString();
       //             lstrMerchantId = ConfigurationSettings.AppSettings["MerchantId"].ToString();
       //             lstrMerchantName = ConfigurationSettings.AppSettings["MerchantName"].ToString();
       //             LoggingAdapter.WriteLog("PointGateway else SecreteKey:" + lstSecretkey + ": MerchantId:" + lstrMerchantId + ":MerchantName:" + lstrMerchantName);
       //         }
       //     }
       //     else
       //     {

       //     }
       // }
       // else
       // {
            
       // }

       // //string lstSecureData = lstSecretkey + "|" + ConfigurationSettings.AppSettings["MerchantId"].ToString() + "|" + lfltAmount + "|" + lintPoints + "|" + lstTxnId;
       // string lstSecureData = lstSecretkey + "|" + lstrMerchantId + "|" + lfltAmount + "|" + lintPoints + "|" + lstTxnId;
       // MD5 md5HashAlgo = MD5.Create();
       // byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(lstSecureData);
       // byte[] hashBytes = md5HashAlgo.ComputeHash(inputBytes);

       // StringBuilder secureData = new StringBuilder();
       // for (int i = 0; i < hashBytes.Length; i++)
       // {
       //     secureData.Append(hashBytes[i].ToString("X2"));
       // }

       // string lstSecureHash = Convert.ToString(secureData);
       //// string lstrPostUrl = ConfigurationSettings.AppSettings["POSTURLConfig"].ToString();
       // string lstrPostUrl = ConfigurationSettings.AppSettings["InfiPayPostUrl"].ToString();
       // if (Session["BookingFlag"].Equals(ServiceType.FLIGHT))
       // {
       //     lstrRedemptionType = "5";
       // }
       // else if (Session["BookingFlag"].Equals(ServiceType.HOTEL))
       // {
       //     lstrRedemptionType = "6";
       // }
       // else if (Session["BookingFlag"].Equals(ServiceType.CAR))
       // {
       //     lstrRedemptionType = "7";
       // }

       // Response.Clear();
       // StringBuilder sb = new StringBuilder();
       // sb.Append("<html>");
       // sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
       // sb.AppendFormat("<form name='form' action='{0}' method='post'>", lstrPostUrl);
       // //sb.AppendFormat("<input type='hidden' runat='server' name='PG_MerchantId' value='{0}'>", ConfigurationSettings.AppSettings["MerchantId"].ToString());
       // sb.AppendFormat("<input type='hidden' runat='server' name='PG_MerchantId' value='{0}'>", lstrMerchantId);
       // //sb.AppendFormat("<input type='hidden' runat='server' name='PG_MerchantName' value='{0}'>", ConfigurationSettings.AppSettings["MerchantName"].ToString());
       // sb.AppendFormat("<input type='hidden' runat='server' name='PG_MerchantName' value='{0}'>", lstrMerchantName);
       // sb.AppendFormat("<input type='hidden' runat='server' name='PG_Amount' value='{0}'>", lfltAmount.ToString());
       // sb.AppendFormat("<input type='hidden' runat='server' name='PG_Points' value='{0}'>", lintPoints.ToString());
       // sb.AppendFormat("<input type='hidden' runat='server' name='PG_PaymentType' value='{0}'>", 4);
       // sb.AppendFormat("<input type='hidden' runat='server' name='PG_RedemptionType' value='{0}'>", lstrRedemptionType);
       // //sb.AppendFormat("<input type='hidden' runat='server' name='PG_TxnCurrency' value='{0}'>", ConfigurationSettings.AppSettings["SourceCurrency"].ToString());
       // sb.AppendFormat("<input type='hidden' runat='server' name='PG_TxnCurrency' value='{0}'>", lstrSourceCurrency);
       // sb.AppendFormat("<input type='hidden' runat='server' name='PG_TxnId' value='{0}'>", lstTxnId);
       // sb.AppendFormat("<input type='hidden' runat='server' name='PG_SecureHash' value='{0}'>", lstSecureHash);
       // sb.AppendFormat("<input type='hidden' runat='server' name='AdditionalDetails1' value='{0}'>", "");
       // sb.AppendFormat("<input type='hidden' runat='server' name='AdditionalDetails2' value='{0}'>", "");
       // sb.AppendFormat("<input type='hidden' runat='server' name='AdditionalDetails3' value='{0}'>", "");
       // sb.AppendFormat("<input type='hidden' runat='server' name='AdditionalDetails4' value='{0}'>", "");
       // sb.AppendFormat("<input type='hidden' runat='server' name='AdditionalDetails5' value='{0}'>", "");
       // sb.Append("</form>");
       // sb.Append("</body>");
       // sb.Append("</html>");
       // Response.Write(sb.ToString());
       // Response.End();
    }

    //public static bool BookFlight()
    //{
    //    InfiModel lobjInfiModel = new InfiModel();
    //    if (HttpContext.Current.Session["ItineraryRequest"] != null && HttpContext.Current.Session["MemberDetails"] != null && HttpContext.Current.Session["ItineraryResponse"] != null)
    //    {
    //        CB.IBE.Platform.ClientEntities.SearchRequest lobjSearchRequest = HttpContext.Current.Session["FlightSearchDetails"] as CB.IBE.Platform.ClientEntities.SearchRequest;
    //        CreateItineraryResponse lobjCreateItineraryResponse = HttpContext.Current.Session["ItineraryResponse"] as CreateItineraryResponse;
    //        CreateItineraryRequest lobjCreateItineraryRequest = HttpContext.Current.Session["ItineraryRequest"] as CreateItineraryRequest;
    //        BookingRequest lobjBookingRequest = new BookingRequest();
    //        MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
    //        List<RedemptionDetails> lobjListOfRedemptionDetails = HttpContext.Current.Session["RedemptionDetails"] as List<RedemptionDetails>;
    //        RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererSupplierDetails"] as RefererDetails;

    //        lobjBookingRequest.SupplierDetails.Id = lobjRefererDetails.RefererSupplierProperties.SupplierId;
    //        lobjBookingRequest.SessionId = lobjCreateItineraryRequest.SessionId;

    //        lobjBookingRequest.ItineraryDetails.MemberId = lobjMemberDetails.MemberRelationsList[0].RelationReference;
    //        lobjBookingRequest.ItineraryDetails = lobjCreateItineraryResponse.ItineraryDetails;
    //        lobjBookingRequest.ItineraryDetails.RefererDetails = lobjSearchRequest.RefererDetails;
    //        lobjBookingRequest.PointRate = lobjCreateItineraryRequest.PointRate;
    //        lobjBookingRequest.IPAddress = lobjSearchRequest.IPAddress;
    //        CB.IBE.Platform.ClientEntities.BookingResponse lobjBookingResponse = lobjInfiModel.BookFlight(lobjBookingRequest, lobjMemberDetails, lobjListOfRedemptionDetails);
    //        if (lobjBookingResponse != null && lobjBookingResponse.PNRDetails.TripId != null && lobjBookingResponse.PNRDetails.TripId != string.Empty && lobjBookingResponse.PNRDetails.Status.Equals(1))
    //        {

    //            ItineraryDetails lobjItineraryDetails = new ItineraryDetails();
    //            lobjItineraryDetails = lobjBookingResponse.PNRDetails.ItineraryDetails;
    //            lobjItineraryDetails.FareDetails = lobjBookingResponse.PNRDetails.ItineraryDetails.FareDetails;
    //            lobjItineraryDetails.TravelerInfo = lobjBookingResponse.PNRDetails.ItineraryDetails.TravelerInfo;
    //            lobjItineraryDetails.ItineraryReference = lobjBookingResponse.PNRDetails.BookingReference;
    //            HttpContext.Current.Session["FlightBookedResponse"] = lobjBookingResponse;
    //            HttpContext.Current.Session["FlightBooked"] = lobjItineraryDetails;
    //            HttpContext.Current.Session["ItineraryResponse"] = null;
    //            HttpContext.Current.Session["ItineraryRequest"] = null;
    //            HttpContext.Current.Session["ReviewFlightDetails"] = null;

    //            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlight + " Status - " + "Success"), ActivityType.FlightBooking);
    //            return true;
    //        }
    //        else
    //        {
    //            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlight + " Status - " + "Failed"), ActivityType.FlightBooking);
    //            return false;
    //        }
    //    }
    //    else
    //    {
    //        lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlight + " Status - " + "Failed"), ActivityType.FlightBooking);
    //        return false;
    //    }
    //}

    //[System.Web.Script.Services.ScriptMethod()]
    //[System.Web.Services.WebMethod]
    //public static bool BookHotel()
    //{
    //    InfiModel lobjInfiModel = new InfiModel();
    //    LoggingAdapter.WriteLog("Booking Hotel");
    //    LoggingAdapter.WriteLog((HttpContext.Current.Session["BookedHotel"] != null && HttpContext.Current.Session["CustomerDetails"] != null && HttpContext.Current.Session["SearchDetails"] != null).ToString());

    //    if (HttpContext.Current.Session["MemberDetails"] != null)
    //    {
    //        MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
    //        if (HttpContext.Current.Session["BookedHotel"] != null && HttpContext.Current.Session["CustomerDetails"] != null && HttpContext.Current.Session["SearchDetails"] != null)
    //        {
    //            HotelSearchResponse lobjSearchResponse = HttpContext.Current.Session["BookedHotel"] as HotelSearchResponse;
    //            HotelSearchResponse lobjHotelBooked = new HotelSearchResponse();
    //            lobjHotelBooked = lobjSearchResponse;
    //            HotelSearchRequest lobjSearchRequest = HttpContext.Current.Session["SearchDetails"] as HotelSearchRequest;
    //            Framework.Integrations.Hotels.Entities.Customer lobjCustomer = HttpContext.Current.Session["CustomerDetails"] as Framework.Integrations.Hotels.Entities.Customer;

    //            List<RedemptionDetails> lobjListOfRedemptionDetails = HttpContext.Current.Session["RedemptionDetails"] as List<RedemptionDetails>;

    //            CB.IBE.Platform.Hotels.ClientEntities.HotelBookingResponse lobjBookingResponse = lobjInfiModel.BookHotel(lobjMemberDetails, lobjSearchResponse.SearchResponse.hotels.hotel[0], lobjSearchRequest, lobjCustomer, lobjListOfRedemptionDetails);
    //            HttpContext.Current.Session["BookingResponse"] = lobjBookingResponse;
    //            if (lobjBookingResponse != null && lobjBookingResponse.BookingResponse.bookingid != null && lobjBookingResponse.BookingResponse.confirmationnumber != null && lobjBookingResponse.BookingResponse.bookingid != string.Empty && lobjBookingResponse.BookingResponse.confirmationnumber != string.Empty)
    //            {
    //                HttpContext.Current.Session["BookedHotel"] = null;
    //                HttpContext.Current.Session["HotelBooked"] = lobjHotelBooked;
    //                lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Success"), ActivityType.HotelBooking);
    //                return true;
    //            }
    //            else
    //            {
    //                lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.HotelBooking);
    //                return false;
    //            }
    //        }
    //        else
    //        {
    //            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.HotelBooking);
    //            return false;
    //        }
    //    }
    //    else
    //    {
    //        lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.HotelBooking);
    //        return false;
    //    }
    //}

    //[System.Web.Script.Services.ScriptMethod()]
    //[System.Web.Services.WebMethod]
    //public static bool BookCar()
    //{
    //    InfiModel lobjInfiModel = new InfiModel();

    //    if (HttpContext.Current.Session["MemberDetails"] != null && HttpContext.Current.Session["CarMakeBookingRequest"] != null && HttpContext.Current.Session["CarSearchRequest"] != null && HttpContext.Current.Session["Cars"] != null && HttpContext.Current.Session["CarSelected"] != null && HttpContext.Current.Session["CarExtraListResponse"] != null)
    //    {
    //        MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
    //        CarSearchRequest lobjCarSearchRequest = HttpContext.Current.Session["CarSearchRequest"] as CarSearchRequest;
    //        CarSearchResponse lobjCarSearchResponse = HttpContext.Current.Session["Cars"] as CarSearchResponse;
    //        Match lobjMatch = HttpContext.Current.Session["CarSelected"] as Match;
    //        CarExtrasListResponse lobjCarExtrasListResponse = HttpContext.Current.Session["CarExtraListResponse"] as CarExtrasListResponse;
    //        CarMakeBookingRequest lobjCarMakeBookingRequest = HttpContext.Current.Session["CarMakeBookingRequest"] as CarMakeBookingRequest;

    //        CarMakeBookingResponse lobjCarMakeBookingResponse = new CarMakeBookingResponse();

    //        lobjCarSearchRequest.SearchId = lobjCarSearchResponse.SearchId;

    //        List<RedemptionDetails> lobjListOfRedemptionDetails = HttpContext.Current.Session["RedemptionDetails"] as List<RedemptionDetails>;

    //        lobjCarMakeBookingResponse = lobjInfiModel.BookForCar(lobjCarMakeBookingRequest, lobjCarSearchRequest, lobjMatch, lobjCarExtrasListResponse, lobjMemberDetails, lobjListOfRedemptionDetails);

    //        if (lobjCarMakeBookingResponse != null && lobjCarMakeBookingResponse.MakeBookingRS != null && lobjCarMakeBookingResponse.MakeBookingRS.Booking != null && !string.IsNullOrEmpty(lobjCarMakeBookingResponse.MakeBookingRS.Booking.id))
    //        {
    //            HttpContext.Current.Session["CarMakeBookingResponse"] = lobjCarMakeBookingResponse;
    //            HttpContext.Current.Session["CarMakeBookingRequest"] = null;
    //            HttpContext.Current.Session["CarMadeBookingRequest"] = lobjCarMakeBookingRequest;
    //            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookCar + " Status - " + "Success"), ActivityType.CarBooking);
    //            return true;
    //        }
    //        else
    //        {
    //            HttpContext.Current.Session["CarMakeBookingResponse"] = lobjCarMakeBookingResponse;
    //            HttpContext.Current.Session["CarMakeBookingRequest"] = null;
    //            HttpContext.Current.Session["CarMadeBookingRequest"] = lobjCarMakeBookingRequest;
    //            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookCar + " Status - " + "Failed"), ActivityType.CarBooking);
    //            return false;
    //        }
    //    }
    //    else
    //    {
    //        lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookCar + " Status - " + "Failed"), ActivityType.CarBooking);
    //        return false;
    //    }
    //}

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool GetCashBack()
    {
        LoggingAdapter.WriteLog("GetCashBack Request:");
        InfiModel lobjModel  = new InfiModel();
        bool lblnStatus = false;

        ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();

        if (HttpContext.Current.Session["MemberDetails"] != null && lobjProgramDefinition != null)
        {
            string lstrCurrency = lobjModel.GetDefaultCurrency();
            MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
            AdditionalRedemptionDetails lobjAdditionalRedemptionDetails = new AdditionalRedemptionDetails();
            lobjAdditionalRedemptionDetails = (AdditionalRedemptionDetails)HttpContext.Current.Session["AdditionalRedemptionDetails"];
            HttpContext.Current.Session["MemberMiles"] = Convert.ToInt32(lobjModel.CheckAvailbility(lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency));
            if (lobjAdditionalRedemptionDetails != null)
            {
                if (!lobjAdditionalRedemptionDetails.Amounts.Equals(0) && !lobjAdditionalRedemptionDetails.Points.Equals(0))
                {
                    LoggingAdapter.WriteLog("GetCashBack Request1:");
                    lobjAdditionalRedemptionDetails.LoyaltyTxnType = LoyaltyTxnType.CashBack;
                    lobjAdditionalRedemptionDetails.RedemptionStatus = RedemptionStatus.Pending;
                    //lobjAdditionalRedemptionDetails.Amounts = Convert.ToSingle(strAmount);
                    //lobjAdditionalRedemptionDetails.Points = Convert.ToInt32(strTotalPoints);
                    lobjAdditionalRedemptionDetails.RelationReference = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    lobjAdditionalRedemptionDetails.SourceCurrency = lobjProgramDefinition.lobjProgramAttribute.BaseCurrency;
                    lobjAdditionalRedemptionDetails.TxnCurrency = lobjProgramDefinition.lobjProgramAttribute.BaseCurrency;
                    lobjAdditionalRedemptionDetails.TransactionCurrency = lstrCurrency;
                    lobjAdditionalRedemptionDetails.CreatedBy = "SYSTEM";
                    HttpContext.Current.Session["CashBackData"] = lobjAdditionalRedemptionDetails;
                    lblnStatus = lobjModel.CashBackRedemtions(lobjAdditionalRedemptionDetails, lobjMemberDetails);
                    HttpContext.Current.Session["AdditionalRedemptionDetails"] = null;
                }
                else
                {
                    LoggingAdapter.WriteLog("AdditionalRedemptionDetails Amount and Points Zero");
                    HttpContext.Current.Session["AdditionalRedemptionDetails"] = null;
                }
            }
            else
            {
                LoggingAdapter.WriteLog("AdditionalRedemptionDetails Null");
            }
        }
        LoggingAdapter.WriteLog("GetCashBack Response" + lblnStatus);
        return lblnStatus;
    }

}