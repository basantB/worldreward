﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Member.Entites;
using CB.IBE.Platform.Transactions.Entites;
using CB.IBE.Platform.Masters.Entities;
using System.Configuration;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.InfiVoucher.Entities;
using Core.Platform.Transactions.Entites;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using InfiPlanetModel.Model;
using InfiVoucher.Platform.Entities;

public partial class Voucher : System.Web.UI.Page
{
    static string lstrMerchantId = Convert.ToString(ConfigurationSettings.AppSettings["MerchantId"]);
    static string lstrMerchantUserName = Convert.ToString(ConfigurationSettings.AppSettings["MerchantUserName"]);
    static string lstrMerchantPassword = Convert.ToString(ConfigurationSettings.AppSettings["MerchantPassword"]);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["PayCurrency"] = null;
            Session["PayPoints"] = null;
            if (Session["MemberDetails"] != null)
            {
                GetProductProgramCurrencyDefinition();
                if (Session["VoucherDetails"] != null)
                {
                    BindVoucherDetails();
                }
            }
            else
            {
                //GetProgramCurrency();
                string CallbackUrl = HttpUtility.UrlEncode(Encrypt("Voucher.aspx"));
                Response.Redirect("Login.aspx?CallbackUrl=" + CallbackUrl);
            }
        }
        if (Session["VoucherDetails"] == null)
        {
            DivGlobalRewardsConfirm.Attributes.Add("class", "display_hide");
            DivSubmitContent.Attributes["class"] = DivGlobalRewardsConfirm.Attributes["class"].Replace("display_hide", "");
        }
    }
    public void GetProgramCurrency()
    {
        InfiModel lobjInfiModel = new InfiModel();
        List<ProgramCurrencyDefinition> lobjListProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
        ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
        lobjProgramDefinition = lobjInfiModel.GetProgramMaster();

        lobjListProgramCurrencyDefinition = lobjInfiModel.GetProgramCurrencyDefinition(lobjProgramDefinition.ProgramId);
        ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();

        lobjProgramCurrencyDefinition = lobjListProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals("REWARDZ"));
        Session["VoucherProgramCurrency"] = lobjProgramCurrencyDefinition;
        Session["VoucherRate"] = Convert.ToString(lobjProgramCurrencyDefinition.VoucherRate);
        HDFVoucherRate.Value = Convert.ToString(lobjProgramCurrencyDefinition.VoucherRate);

    }
    public void GetProductProgramCurrencyDefinition()
    {
        Session["VoucherProgramCurrency"] = null;
        Session["VoucherProgramCurrencyList"] = null;
        string lstrResponse = string.Empty;
        InfiModel lobjInfiModel = new InfiModel();
        List<ProgramCurrencyDefinition> lobjListProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
        ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();
        MemberDetails lobjMemberDetails = new MemberDetails();

        lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
        lobjListProgramCurrencyDefinition = lobjInfiModel.GetProductProgramCurrencyDefinition(lobjMemberDetails);

        if (lobjListProgramCurrencyDefinition.Count > 0)
        {
            if (lobjListProgramCurrencyDefinition.Count > 1)
            {
                Session["VoucherProgramCurrencyList"] = lobjListProgramCurrencyDefinition;
                ddlProgramCurrency.DataSource = lobjListProgramCurrencyDefinition;
                ddlProgramCurrency.DataTextField = "Description";
                ddlProgramCurrency.DataValueField = "Currency";
                ddlProgramCurrency.DataBind();
                divCurrencyDropDown.Attributes.Add("Style", "display:block");
                lobjProgramCurrencyDefinition = lobjListProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals("REWARDZ"));
                Session["VoucherProgramCurrency"] = lobjProgramCurrencyDefinition;
                Session["VoucherRate"] = Convert.ToString(lobjProgramCurrencyDefinition.VoucherRate);
                HDFVoucherRate.Value = Convert.ToString(lobjProgramCurrencyDefinition.VoucherRate);

            }
            else
            {
                divCurrencyDropDown.Attributes.Add("Style", "display:none");
                ddlProgramCurrency.Visible = true;
                lobjProgramCurrencyDefinition = lobjListProgramCurrencyDefinition[0];
                if (lobjProgramCurrencyDefinition != null)
                {
                    Session["VoucherProgramCurrency"] = lobjProgramCurrencyDefinition;
                    Session["VoucherRate"] = Convert.ToString(lobjProgramCurrencyDefinition.VoucherRate);
                    HDFVoucherRate.Value = Convert.ToString(lobjProgramCurrencyDefinition.VoucherRate);
                    lblVoucherCurrency.Text = lobjInfiModel.CurrencyDisplayText(lobjProgramCurrencyDefinition.Currency);

                }

            }
        }
        else
        {
            divCurrencyNotFound.Attributes["class"] = divCurrencyNotFound.Attributes["class"].Replace("display_hide", "");
            DivSubmitContent.Attributes.Add("class", "display_hide");
        }
    }

    public void BindVoucherDetails()
    {
        int lintQAR100Qty = 0;
        int lintQAR500Qty = 0;
        int lintQAR1000Qty = 0;

        ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();
        lobjProgramCurrencyDefinition = HttpContext.Current.Session["VoucherProgramCurrency"] as ProgramCurrencyDefinition;
        if (lobjProgramCurrencyDefinition != null)
        {
            List<VoucherDetails> lobjListVoucherDetails = new List<VoucherDetails>();
            lobjListVoucherDetails = HttpContext.Current.Session["VoucherDetails"] as List<VoucherDetails>;

            MemberDetails lobjMemberDetails = new MemberDetails();
            lobjMemberDetails = Session["MemberDetails"] as MemberDetails;

            lintQAR100Qty = lobjListVoucherDetails.FindAll(lobj => lobj.Value.Equals(100)).Count;
            lintQAR500Qty = lobjListVoucherDetails.FindAll(lobj => lobj.Value.Equals(500)).Count;
            lintQAR1000Qty = lobjListVoucherDetails.FindAll(lobj => lobj.Value.Equals(1000)).Count;

            lblQAR100Quantity.Text = Convert.ToString(lintQAR100Qty);
            lblQAR500Quantity.Text = Convert.ToString(lintQAR500Qty);
            lblQAR1000Quantity.Text = Convert.ToString(lintQAR1000Qty);

            trQAR100.Attributes.Add("class", "display_hide");
            trQAR500.Attributes.Add("class", "display_hide");
            trQAR1000.Attributes.Add("class", "display_hide");

            if (lintQAR100Qty != 0)
            {
                trQAR100.Attributes["class"] = trQAR100.Attributes["class"].Replace("display_hide", "");
            }
            if (lintQAR500Qty != 0)
            {
                trQAR500.Attributes["class"] = trQAR500.Attributes["class"].Replace("display_hide", "");
            }
            if (lintQAR1000Qty != 0)
            {
                trQAR1000.Attributes["class"] = trQAR1000.Attributes["class"].Replace("display_hide", "");
            }

            lblQar100VoucherVal.Text = Convert.ToString(lintQAR100Qty * 100);
            lblQar500VoucherVal.Text = Convert.ToString(lintQAR500Qty * 500);
            lblQar1000VoucherVal.Text = Convert.ToString(lintQAR1000Qty * 1000);

            lblQAR100PointsRequired.Text = Convert.ToString(lintQAR100Qty * 100 / Convert.ToSingle(Session["VoucherRate"]));
            lblQAR500PointsRequired.Text = Convert.ToString(lintQAR500Qty * 500 / Convert.ToSingle(Session["VoucherRate"]));
            lblQAR1000PointsRequired.Text = Convert.ToString(lintQAR1000Qty * 1000 / Convert.ToSingle(Session["VoucherRate"]));


            int totalAmount = Convert.ToInt32(lobjListVoucherDetails.Sum(lobj => lobj.Value));
            int totalPoints = Convert.ToInt32(lobjListVoucherDetails.Sum(lobj => lobj.Value) / lobjProgramCurrencyDefinition.VoucherRate);

            lblTotalVoucherValue.Text = Convert.ToString(totalAmount);
            lblRequirPointToRedeem.Text = Convert.ToString(totalPoints);

            DivGlobalRewardsConfirm.Attributes["class"] = DivGlobalRewardsConfirm.Attributes["class"].Replace("display_hide", "");
            DivSubmitContent.Attributes.Add("class", "display_hide");

        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string CalculateAmount(string strbaseAmount, string strAmount, string strQty)
    {
        List<VoucherDetails> lobjListVoucherDetails = null;
        if (HttpContext.Current.Session["VoucherDetails"] != null)
        {

            lobjListVoucherDetails = HttpContext.Current.Session["VoucherDetails"] as List<VoucherDetails>;
        }
        else
        {
            lobjListVoucherDetails = new List<VoucherDetails>();
        }


        VoucherDetails lobjVoucherDetails = null;
        InfiModel lobjModel = new InfiModel();
        MemberDetails lobjMemberDetails = new MemberDetails();
        lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
        ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();
        ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
        int lintBaseAmount = Convert.ToInt32(strbaseAmount);
        int lintAmount = Convert.ToInt32(strAmount);
        int lintQty = Convert.ToInt32(strQty);
        string lstrTotalAmount;
        if (lintBaseAmount > 0)
        {
            lstrTotalAmount = Convert.ToString(lintAmount * lintQty + lintBaseAmount);
        }
        else
        {
            lstrTotalAmount = Convert.ToString(lintAmount * lintQty);
        }
        int lintPoints = 0;

        //lobjProgramDefinition = lobjModel.GetProgramMaster();
        lobjProgramDefinition = lobjModel.GetProgramDetailsByID(lobjMemberDetails.ProgramId);

        lobjProgramCurrencyDefinition = HttpContext.Current.Session["VoucherProgramCurrency"] as ProgramCurrencyDefinition;
        lintPoints = Convert.ToInt32(Convert.ToSingle(lstrTotalAmount) / lobjProgramCurrencyDefinition.VoucherRate);
        for (int i = 0; i < Convert.ToInt32(strQty); i++)
        {
            lobjVoucherDetails = new VoucherDetails();
            lobjVoucherDetails.ProgramId = lobjProgramDefinition.ProgramId;
            lobjVoucherDetails.ProgramName = lobjProgramDefinition.ProgramName;
            lobjVoucherDetails.Value = Convert.ToSingle(strAmount);
            lobjVoucherDetails.EventName = "Default";
            lobjVoucherDetails.DateOfIssue = DateTime.Now;
            lobjVoucherDetails.PaymentType = Convert.ToInt32(Core.Platform.Member.Entites.RelationType.Voucher);
            lobjVoucherDetails.RedemptionType = Convert.ToInt32(LoyaltyTxnType.InfiVoucher);
            lobjVoucherDetails.VoucherDescription = "InfiVoucher";
            //lobjVoucherDetails.Currency = lobjProgramDefinition.lobjProgramAttribute.BaseCurrency;
            lobjVoucherDetails.Currency = lobjProgramCurrencyDefinition.Currency;
            lobjVoucherDetails.MerchantId = lstrMerchantId;
            lobjVoucherDetails.MerchantName = lstrMerchantUserName;
            lobjVoucherDetails.MerchantPassword = lstrMerchantPassword;
            lobjVoucherDetails.RelationType = Convert.ToInt32(Core.Platform.Member.Entites.RelationType.LBMS);
            lobjListVoucherDetails.Add(lobjVoucherDetails);
        }
        HttpContext.Current.Session["VoucherDetails"] = lobjListVoucherDetails;
        return Convert.ToString(lintPoints);

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string BackToDenomination()
    {
        string lstrVoucherRate = string.Empty;
        HttpContext.Current.Session["VoucherDetails"] = null;
        lstrVoucherRate = Convert.ToString(HttpContext.Current.Session["VoucherRate"]);
        return lstrVoucherRate;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string CheckAvailability(string strtotalPoints)
    {
        string lstrResponse = string.Empty;
        int pintAvailablePoints = 0;
        int lintPoints = 0;

        InfiModel lobjInfiModel = new InfiModel();
        MemberDetails lobjMemberDetails = new MemberDetails();
        ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();

        lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;

        if (lobjMemberDetails != null)
        {
            lobjProgramCurrencyDefinition = HttpContext.Current.Session["VoucherProgramCurrency"] as ProgramCurrencyDefinition;
            HttpContext.Current.Session["VoucherRate"] = null;
            HttpContext.Current.Session["VoucherRate"] = lobjProgramCurrencyDefinition.VoucherRate;

            lintPoints = Convert.ToInt32(strtotalPoints);
            MemberRelation lobjMemberRelation = new MemberRelation();
            lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS));

            pintAvailablePoints = lobjInfiModel.CheckAvailbility(lobjMemberRelation.RelationReference, Convert.ToInt32(lobjMemberRelation.RelationType), lobjProgramCurrencyDefinition.Currency);

            if ((pintAvailablePoints - lintPoints) >= 0)
            {
                lstrResponse = Convert.ToString(lintPoints);
            }
            else
            {
                HttpContext.Current.Session["PayCurrency"] = lobjProgramCurrencyDefinition.Currency;
                HttpContext.Current.Session["PayPoints"] = Convert.ToInt32(pintAvailablePoints - lintPoints);
                lstrResponse = "0";
            }
        }
        else
        {
            string CallbackUrl = HttpUtility.UrlEncode(Encrypt("Voucher.aspx"));
            lstrResponse = "Login.aspx?CallbackUrl=" + CallbackUrl;
        }
        return lstrResponse;
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        List<VoucherSummary> lobjListVoucherSummary = new List<VoucherSummary>();

        string lstrResponse = string.Empty;
        InfiModel lobjModel = new InfiModel();
        string lstrCurrency = string.Empty;

        ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();
        lobjProgramCurrencyDefinition = Session["VoucherProgramCurrency"] as ProgramCurrencyDefinition;
        if (lobjProgramCurrencyDefinition != null)
        {
            lstrCurrency = lobjProgramCurrencyDefinition.Currency;
        }
        MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
        try
        {
            if (HttpContext.Current.Session["MemberDetails"] != null)
            {
                List<VoucherDetails> lobjListVoucherDetails = new List<VoucherDetails>();
                if (HttpContext.Current.Session["VoucherDetails"] != null)
                {
                    lobjListVoucherDetails = HttpContext.Current.Session["VoucherDetails"] as List<VoucherDetails>;
                }
                int totalAmount = Convert.ToInt32(lobjListVoucherDetails.Sum(lobj => lobj.Value));
                int totalPoints = Convert.ToInt32(lobjListVoucherDetails.Sum(lobj => lobj.Value) / lobjProgramCurrencyDefinition.VoucherRate);
                int pintAvailablePoints = 0;

                InfiModel lobjInfiModel = new InfiModel();
                MemberRelation lobjMemberRelation = new MemberRelation();
                lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS));

                pintAvailablePoints = lobjInfiModel.CheckAvailbility(lobjMemberRelation.RelationReference, Convert.ToInt32(lobjMemberRelation.RelationType), lobjProgramCurrencyDefinition.Currency);
                if ((pintAvailablePoints - totalPoints) >= 0)
                {
                    if (HttpContext.Current.Application["RefererSupplierDetails"] != null)
                    {
                        IBEAccountTransactionDetails lobjIBEAccountTransactionDetails = null;
                        RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererSupplierDetails"] as RefererDetails;
                        if (lobjRefererDetails.IsDepositAccountConfigured.Equals(true))
                        {
                            lobjIBEAccountTransactionDetails = new IBEAccountTransactionDetails();
                            lobjIBEAccountTransactionDetails.RefererId = lobjRefererDetails.Id;
                            lobjIBEAccountTransactionDetails.AccountId = lobjRefererDetails.Account_Id;
                            lobjIBEAccountTransactionDetails.TransactionType = IBETransactionType.Debit;
                            lobjIBEAccountTransactionDetails.Description = "Redemption InfiVoucher";
                            lobjIBEAccountTransactionDetails.LoyaltyTxnType = IBELoyaltyTxnType.InfiVoucher;
                            //lobjListVoucherSummary = lobjModel.CreateVoucher(lobjMemberDetails, lobjIBEAccountTransactionDetails, lobjListVoucherDetails, lstrCurrency);
                            //Session["VoucherDetails"] = null;
                        }
                        lobjListVoucherSummary = lobjModel.CreateVoucher(lobjMemberDetails, lobjIBEAccountTransactionDetails, lobjListVoucherDetails, lstrCurrency);
                        Session["VoucherDetails"] = null;
                    }
                    if ((lobjListVoucherSummary.FindAll(lobj => lobj.Status.Equals(true)).Count) > 0)
                    {
                        lobjModel.LogActivity(string.Format(ActivityConstants.Voucher + " For Member " + lobjMemberDetails.NationalId + " Status - Success"), ActivityType.Voucher);

                        string lstrFinalTemplate = string.Empty;
                        string lstrVoucherUrl = ConfigurationManager.AppSettings["VoucherUrl"];
                        for (int i = 0; i < lobjListVoucherSummary.Count; i++)
                        {
                            string lstrTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("Templates/VoucherDetails.htm"));
                            lstrFinalTemplate += string.Format(lstrTemplate, lobjListVoucherSummary[i].Amount, lobjListVoucherSummary[i].VoucherNo, lobjListVoucherSummary[i].ExpiryDate.ToString(), lstrVoucherUrl + lobjListVoucherSummary[i].VoucherNo + ".png");
                        }
                        List<string> lstrEmailparameter = new List<string>();
                        lstrEmailparameter.Add(lstrFinalTemplate);
                        lobjModel.SendEmail(lstrEmailparameter, lobjMemberDetails, "WORLDREWARDSCreateMultipleVoucher");

                        RepCreateVoucherSummary.DataSource = lobjListVoucherSummary;
                        RepCreateVoucherSummary.DataBind();
                        DivSubmitContent.Attributes.Add("class", "display_hide");
                        DivGlobalRewardsConfirm.Attributes.Add("class", "display_hide");
                        divCreateVoucherSummary.Attributes["class"] = divCreateVoucherSummary.Attributes["class"].Replace("display_hide", "");
                        divConfirmationMsg.Attributes["class"] = divConfirmationMsg.Attributes["class"].Replace("display_hide", "");

                    }
                    else
                    {
                        lobjModel.LogActivity(string.Format(ActivityConstants.Voucher + " For Member " + lobjMemberDetails.NationalId + " Status - Failed"), ActivityType.Voucher);
                        DivSubmitContent.Attributes.Add("class", "display_hide");
                        divBtnConfirm.Attributes.Add("class", "display_hide");
                        divConfirmationMsg.InnerText = "Sorry voucher not created.Please try again after some time.";
                        divConfirmationMsg.Attributes["class"] = divConfirmationMsg.Attributes["class"].Replace("display_hide", "");                        
                    }
                }
                else
                {
                    Session["PayCurrency"] = lobjProgramCurrencyDefinition.Currency;
                    Session["PayPoints"] = Convert.ToInt32(pintAvailablePoints - totalPoints);

                    divBtnConfirm.Attributes.Add("class", "display_hide");
                    divAvailability.Attributes.Add("class", "display_hide");
                }

                btnConfirm.Attributes.Add("Style", "display:block");

            }

        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("Voucher CreateVoucher :" + Environment.NewLine + ex.StackTrace);
        }

    }

    protected void ddlProgramCurrency_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<ProgramCurrencyDefinition> lobjListProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
        lobjListProgramCurrencyDefinition = Session["VoucherProgramCurrencyList"] as List<ProgramCurrencyDefinition>;

        List<VoucherDetails> lobjListVoucherDetails = new List<VoucherDetails>();
        lobjListVoucherDetails = Session["VoucherDetails"] as List<VoucherDetails>;

        if (ddlProgramCurrency.SelectedItem.Value.Equals("Limited Edition"))
        {
            Session["VoucherProgramCurrency"] = lobjListProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals("Limited Edition"));
        }
        else
        {
            Session["VoucherProgramCurrency"] = lobjListProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals("REWARDZ"));
        }

        ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();
        lobjProgramCurrencyDefinition = Session["VoucherProgramCurrency"] as ProgramCurrencyDefinition;

        Session["VoucherRate"] = Convert.ToString(lobjProgramCurrencyDefinition.VoucherRate);
        HDFVoucherRate.Value = Convert.ToString(lobjProgramCurrencyDefinition.VoucherRate);
        lblVoucherCurrency.Text = ddlProgramCurrency.SelectedItem.Value;

        BindVoucherDetails();
    }

    private static string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }
}