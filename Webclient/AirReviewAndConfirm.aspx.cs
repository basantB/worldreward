﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Member.Entites;
using CB.IBE.Platform.Entities;
using CB.IBE.Platform.ClientEntities;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.OTP.Entities;
using Core.Platform.ProgramMaster.Entities;
using CB.IBE.Platform.Masters.Entities;
using InfiPlanetModel.Model;
using Core.Platform.Booking.Entities;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;
public partial class AirReviewAndConfirm : System.Web.UI.Page
{
    CreateItineraryResponse lobjCreateItineraryResponse = null;
    CreateItineraryRequest lobjCreateItineraryRequest = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InfiModel lobjInfiModel = new InfiModel();

            if (Session["PageTitle"] != null)
            {
                this.Title = Session["PageTitle"].ToString();
            }

            MemberDetails lobjMemberDetails = new MemberDetails();

            if (Session["MemberDetails"].Equals(null))
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
            }

            if (Session["ItineraryResponse"] != null)
            {
                lobjCreateItineraryResponse = Session["ItineraryResponse"] as CreateItineraryResponse;

                string lstrCurrency = lobjInfiModel.GetDefaultCurrency();
                Session["MemberMiles"] = Convert.ToString(lobjInfiModel.CheckAvailbility(lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency));

                if (lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalPoints <= 0)
                {
                    Response.Redirect("BookingFailure.aspx");
                }
                else
                {
                    lblTotalPoints.Text = lobjInfiModel.IntToThousandSeperated(lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalPoints);
                    // lblTotalPointsMob.Text = lobjriyadModel.IntToThousandSeperated(lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalPoints);
                }

                if (Convert.ToDouble(Session["MemberMiles"]) >= Convert.ToDouble(lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalPoints))
                {
                    btnBookNow.Enabled = true;
                    divError.Style.Add("display", "none");
                    lblError.Text = "";
                }
                else
                {
                    btnBookNow.Enabled = false;
                    divError.Style.Add("display", "block");
                    //lblError.Text = "You have insufficient Hadaya Rewards Points to book this itinarary.To purchase the remaining points <a href='PurchasePoints.aspx'>Click Here</a>.";
                    lblError.Text = "You have insufficient WorldRewardZ to book this flight. <a href='PurchasePoints.aspx' style='color:blue'>Click Here</a> to purchase WorldRewardZ.";
                }

                Session["ItineraryResponse"] = lobjCreateItineraryResponse;
            }
        }

    }

    protected void btnBookNow_Click(object sender, EventArgs e)
    {
        LoggingAdapter.WriteLog("btnBookNow_Click : Start");
        try
        {
            InfiModel lobjModel = new InfiModel();
            MemberDetails lobjMemberDetails = new MemberDetails();
            lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
            lobjCreateItineraryRequest = Session["ItineraryRequest"] as CreateItineraryRequest;
            lobjCreateItineraryResponse = Session["ItineraryResponse"] as CreateItineraryResponse;
            Session["FlightSearchPaymode"] = null;
            Session["BookingFlag"] = ServiceType.FLIGHT;

            string lstrCurrency = lobjModel.GetDefaultCurrency();

            LoggingAdapter.WriteLog("btnBookNow_Click : GetDefaultCurrency :" + lstrCurrency);

            ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();
            LoggingAdapter.WriteLog("btnBookNow_Click : GetProgramMaster :" + lobjProgramDefinition.ProgramId);

            //List<RedemptionKeys> lobjRedemptionKeys = new List<RedemptionKeys>();
            //lobjRedemptionKeys = lobjModel.GetAllRedemptionKeys(lobjProgramDefinition.ProgramId);
            int ThreshouldValue = 1000000000;

            //ThreshouldValue = lobjRedemptionKeys.Find(lobj => lobj.RedemptionCode.Equals(RedemptionCodeKeys.AIR.ToString()) && lobj.Currency.Equals(lstrCurrency)).OTPThreshold;
            int lintTotalPoints = Convert.ToInt32(lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalPoints);
            float lftAmount = Convert.ToSingle(lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalBaseFare);

            LoggingAdapter.WriteLog("btnBookNow_Click : ThreshouldValue :" + ThreshouldValue + " lintTotalPoints:" + lintTotalPoints + " lftAmount:" + lftAmount);

            List<RedemptionDetails> lobjListOfRedemptionDetails = new List<RedemptionDetails>();
            RedemptionDetails lobjRedemptionDetails = new RedemptionDetails();
            lobjRedemptionDetails.Currency = lstrCurrency;
            lobjRedemptionDetails.DisplayCurrency = lobjModel.CurrencyDisplayText(lstrCurrency);
            lobjRedemptionDetails.Points = lintTotalPoints;
            lobjRedemptionDetails.RelationReference = lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
            lobjRedemptionDetails.Amount = lftAmount;

            lobjListOfRedemptionDetails.Add(lobjRedemptionDetails);
            Session["RedemptionDetails"] = lobjListOfRedemptionDetails;

            if (ThreshouldValue <= lintTotalPoints && !ThreshouldValue.Equals(-1))
            {
                LoggingAdapter.WriteLog("btnBookNow_Click : ThreshouldValue Less");
                bool Status = false;
                OTPDetails lobjOTPDetails = new OTPDetails();
                lobjOTPDetails.UniquerefID = lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.AIRREVIEWNCONFIRM;
                lobjOTPDetails.OtpType = Convert.ToString(OTPEnumTypes.AIRREVIEWNCONFIRM);
                Status = lobjModel.GenerateReviewnConfirmOTP(lobjOTPDetails, lobjMemberDetails);

                if (Status)
                {
                    lobjModel.LogActivity(string.Format(ActivityConstants.ReviewConfirmOTP, "AIR", lobjMemberDetails.NationalId, "Success"), ActivityType.ActivationOTPSuccess);
                    Response.Redirect("ValidateOTP.aspx?flag=Air", false);
                }
                else
                {
                    lobjModel.LogActivity(string.Format(ActivityConstants.ReviewConfirmOTP, "AIR", lobjMemberDetails.NationalId, "Failed"), ActivityType.ActivationOTPFailed);
                    Response.Redirect("BookingFailure.aspx", false);

                }
            }
            else
            {
                LoggingAdapter.WriteLog("btnBookNow_Click : ThreshouldValue More");
                Response.Redirect("PaymentConfirmation.aspx", false);
            }


        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("AirReviewAndConfirm :" + ex.Message + ex.StackTrace);
            Response.Redirect("BookingFailure.aspx", false);
        }
    }
}