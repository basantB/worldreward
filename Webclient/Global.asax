﻿<%@ Application Language="C#" %>
<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        GetDestinationList();
        getProgramDetails();
        getSystemConfigurations();
        GetAllAirfields();
        getAllCarriers();
        getAllHotelCities();
        getRefererData();
        GetAllCountryList();
        getRefererDetailsWithSupplier();
        getAllAirCraftDetails();
    }
    private void GetDestinationList()
    {
        try
        {
            InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();
            System.Collections.Generic.List<Viator.Platform.ClientEntities.DestinationDetails> lobjDestinationDetailsList = new List<Viator.Platform.ClientEntities.DestinationDetails>();
            lobjDestinationDetailsList = model.GetDestinationList();
            if (lobjDestinationDetailsList != null && lobjDestinationDetailsList.Count > 0)
            {
                Application[InfiPlanet.SessionConstants.SessionFields.TourDestinationList] = lobjDestinationDetailsList;
            }
        }
        catch (Exception ex)
        {
            Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog("GetDestinationList : " + ex.StackTrace);
        }

    }

    private void getAllAirCraftDetails()
    {
        try
        {
            System.Collections.Generic.List<CB.IBE.Platform.Entities.AirCraftDetails>
            AirCraftList = new System.Collections.Generic.List<CB.IBE.Platform.Entities.AirCraftDetails>();
            InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();
            AirCraftList = model.GetAllAirCraftDetails();
            Application["AllAirCraftsDetails"] = AirCraftList;
        }
        catch (Exception ex)
        {
            Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog("getAllAirCraftDetails : " + ex.StackTrace);
        }
    }

    private void getRefererDetailsWithSupplier()
    {
        try
        {
            InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();

            CB.IBE.Platform.Masters.Entities.RefererDetails lobjRefererDetails = new CB.IBE.Platform.Masters.Entities.RefererDetails();
            lobjRefererDetails = Application["RefererData"] as CB.IBE.Platform.Masters.Entities.RefererDetails;
            lobjRefererDetails = model.GetRefererDetailsWithSupplier(lobjRefererDetails);

            Application["RefererSupplierDetails"] = lobjRefererDetails;
        }
        catch (Exception ex)
        {
            Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog("getRefererDetailsWithSupplier : " + ex.StackTrace);
        }
    }

    private void getRefererData()
    {
        try
        {
            InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();

            CB.IBE.Platform.Masters.Entities.RefererDetails lobjRefererDetails = new CB.IBE.Platform.Masters.Entities.RefererDetails();
            lobjRefererDetails = model.GetRefererData();
            Application["RefererData"] = lobjRefererDetails;
        }
        catch (Exception ex)
        {
            Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog("getRefererData : " + ex.StackTrace);
        }
    }

    private void GetAllAirfields()
    {
        try
        {
            System.Collections.Generic.Dictionary<string, CB.IBE.Platform.Entities.AirField> AirfieldsDictionary = new System.Collections.Generic.Dictionary<string, CB.IBE.Platform.Entities.AirField>();
            System.Collections.Generic.List<CB.IBE.Platform.Entities.AirField>
            AirfieldList = new System.Collections.Generic.List<CB.IBE.Platform.Entities.AirField>();
            InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();
            AirfieldList = model.GetAllAirfields();
            Application["AllAirfields"] = AirfieldList;
            if (AirfieldList != null && AirfieldList.Count > 0)
            {
                for (int carrierCount = 0; carrierCount < AirfieldList.Count; carrierCount++)
                {
                    AirfieldsDictionary.Add(AirfieldList[carrierCount].IATACode, AirfieldList[carrierCount]);
                }
                Application["Airfields"] = AirfieldsDictionary;

            }
        }
        catch (Exception ex)
        {
            Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog("GetAllAirfields : " + ex.StackTrace);
        }
    }
    private void getAllCarriers()
    {
        try
        {
            System.Collections.Generic.Dictionary<string, CB.IBE.Platform.Entities.Carrier> CarrierDictionary = new System.Collections.Generic.Dictionary<string, CB.IBE.Platform.Entities.Carrier>();
            System.Collections.Generic.List<CB.IBE.Platform.Entities.Carrier> carrierList = new System.Collections.Generic.List<CB.IBE.Platform.Entities.Carrier>();
            InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();
            carrierList = model.GetAllCarriers();

            for (int carrierCount = 0; carrierCount < carrierList.Count; carrierCount++)
            {
                if (!CarrierDictionary.ContainsKey(carrierList[carrierCount].CarrierCode))
                {
                    CarrierDictionary.Add(carrierList[carrierCount].CarrierCode, carrierList[carrierCount]);
                }
            }
            Application["CarrierList"] = carrierList;
            Application["Carriers"] = CarrierDictionary;
        }
        catch (Exception ex)
        {
            Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog("GetAirField Exception : " + ex.StackTrace);
        }
    }
    private void getAllHotelCities()
    {
        try
        {
            System.Collections.Generic.List<string> lobjListOfCity = new System.Collections.Generic.List<string>();
            InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();
            lobjListOfCity = model.GetAllHotelCities();
            Application["HotelCities"] = lobjListOfCity;
        }
        catch (Exception ex)
        {
            Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog("getAllHotelCities : " + ex.StackTrace);
        }
    }

    private void getProgramDetails()
    {
        try
        {
            InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();
            Core.Platform.ProgramMaster.Entities.ProgramDefinition lobjProgramDefinition = model.GetGlobalProgramDetails(ConfigurationSettings.AppSettings["ProgramName"].ToString());
            Application["ProgramMaster"] = lobjProgramDefinition;
        }
        catch (Exception ex)
        {
            Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog("getProgramDetails : " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
        }
    }

    private void getSystemConfigurations()
    {
        try
        {
            Core.Platform.ProgramMaster.Entities.ProgramDefinition lobjProgramDefinition = Application["ProgramMaster"] as Core.Platform.ProgramMaster.Entities.ProgramDefinition;
            InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();
            Core.Platform.ProgramMaster.Entities.SystemParameter lobjSystemParameter = model.GetSystemParametres(lobjProgramDefinition.ProgramId);
            Application["SystemParameters"] = lobjSystemParameter;
        }
        catch (Exception ex)
        {
            Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog("getSystemConfigurations : " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
        }
    }
    private void GetAllCountryList()
    {
        try
        {
            //System.Collections.Generic.Dictionary<string, CB.IBE.Platform.Car.ClientEntities.CarPickUpCountryListResponse> objCountryDictionary = new System.Collections.Generic.Dictionary<string, CB.IBE.Platform.Car.ClientEntities.CarPickUpCountryListResponse>();
            CB.IBE.Platform.Car.ClientEntities.CarPickUpCountryListResponse CountryList = new CB.IBE.Platform.Car.ClientEntities.CarPickUpCountryListResponse();
            InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();
            CB.IBE.Platform.Car.ClientEntities.CarPickUpCountryListRequest pobjPickUpCountryListRQ = new CB.IBE.Platform.Car.ClientEntities.CarPickUpCountryListRequest();
            CountryList = model.GetAllCountry(pobjPickUpCountryListRQ);
            Application["AllCountry"] = CountryList.CarPickUpCountryListResponce.Items[0].Country;
        }
        catch (Exception ex)
        {
            Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog("GetAllCountryList : " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
        }
    }
    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
        HttpBrowserCapabilities lobjBrowser = HttpContext.Current.Request.Browser;
        string lstrScreenResolution = lobjBrowser.ScreenPixelsWidth.ToString() + "X" + lobjBrowser.ScreenPixelsHeight.ToString();
        // Code that runs when a new session is started
        Core.Platform.MemberActivity.Entities.MemberActivitySession lobjMemberActivitySession = new Core.Platform.MemberActivity.Entities.MemberActivitySession();
        lobjMemberActivitySession.Activity = "Session Start";
        lobjMemberActivitySession.Browser = lobjBrowser.Type;
        lobjMemberActivitySession.BrowserVersion = lobjBrowser.Version;
        lobjMemberActivitySession.DateTimeStamp = DateTime.Now.ToUniversalTime();
        lobjMemberActivitySession.IpAddress = HttpContext.Current.Request.UserHostAddress;
        lobjMemberActivitySession.PageURL = "Global.asax";
        lobjMemberActivitySession.Resolution = lstrScreenResolution;
        lobjMemberActivitySession.ReferenceNumber = "Guest";
        lobjMemberActivitySession.SessionID = Session.SessionID;
        InfiPlanetModel.Model.InfiModel model = new InfiPlanetModel.Model.InfiModel();
        //Core.Platform.MemberActivity.BusinessFacade.ActivityFacade lobjActivationFacade = new Core.Platform.MemberActivity.BusinessFacade.ActivityFacade();
        int lintID = model.InsertMemberActivityWithSessionID(lobjMemberActivitySession);
        lobjMemberActivitySession.Id = lintID;
        Session["MemberActivitySession"] = lobjMemberActivitySession;
    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

</script>
