﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="TermsConditions.aspx.cs" Inherits="TermsConditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" Runat="Server">
    <script>
$(document).ready(function(){
	//toggle the componenet with class accordion_body
	$(".accordion_head").click(function(){
		if ($('.accordion_body').is(':visible')) {
			$(".accordion_body").slideUp(300);
			$(".plusminus").text('+');
		}
		$(this).next(".accordion_body").slideDown(300); 
		$(this).children(".plusminus").text('-');
	});
});
</script>
 <link href="css/jquery.css" rel="stylesheet" />
 <link href="css/myAccount.css" rel="stylesheet" />
        <article id="article">
        <div class="wrap staticPage faqPage">
            <h1>Terms & Conditions</h1>
			<div class="accordion_container">
		<ul>
        <li>
				<div class="accordion_head">Definitions<span class="plusminus">-</span></div>
				<div class="accordion_body" style="display: block;">
				<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				<p>"WorldRewardZ" means the rewards programme established by WorldRewardZ and subject to these Terms and Conditions.</p>
				<p>"WorldRewardZ Account" means the account reflecting all transactions relating to WorldRewardZ for a particular WorldRewardZ Account holder in WorldRewardZ’s records, including earned WorldRewardZ Points, adjusted WorldRewardZ Points, bonus WorldRewardZ Points, redeemed WorldRewardZ Points and expired WorldRewardZ Points.</p>
                <p>"WorldRewardZ Accountholder" means, in relation to each WorldRewardZ Relationship, the customer or, where there is more than one person comprising the customer, the first named signatory holding such relationship.</p>
                <p>"WorldRewardZ Booking Policy" means the booking policy to redeem WorldRewardZ Points as published from time to time on the WorldRewardZ Website.</p>
                <p>"WorldRewardZ Points" means the reward points credited/debited to the WorldRewardZ Account as a result of any WorldRewardZ Relationship or WorldRewardZ Transaction.</p>
                <p>"WorldRewardZ Rewards" has the meaning set out in Clause 6.6.</p>
                <p>"WorldRewardZ Statement" has the meaning set out in Clause 5.1.</p>
				<br>
				</div>
			</div>
            </li>
            <li>
				<div class="accordion_head">BInding Effect<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
				<div class="description" id="discription" style="overflow: hidden;" tabindex="2">			
				<p>The following terms and conditions ("Terms and Conditions") are applicable to WorldRewardZ and, by becoming a WorldRewardZ Accountholder, you agree to be bound by the Terms and Conditions, as the same may be amended, supplemented, updated, replaced or otherwise varied by WorldRewardZ from time to time and you represent and warrant that you are aged at least 18 years.</p>
				<p>All determinations to be made under WorldRewardZ shall be made by WorldRewardZ, at its absolute discretion and on the basis of WorldRewardZ Records, and each such determination shall be final, conclusive and binding on the WorldRewardZ Accountholder, save in the absence of manifest error made known to WorldRewardZ by the WorldRewardZ Accountholder in accordance with the time limit set out in Clause 5.2. </p>
				<p>WorldRewardZ reserves the right, at any time, at its absolute discretion, without notice and without liability whatsoever on its part, to supplement, amend, replace, delete or otherwise vary any of the Terms and Conditions, including the Schedules, as well as the terms and conditions of any other policy referred to in the Terms and Conditions, and to change, vary, modify, terminate or cancel WorldRewardZ or any of its benefits or features, or otherwise do any other act with respect to the whole or any part of WorldRewardZ, or to withdraw or change the membership criteria and/or to limit or change the value / validity of WorldRewardZ and/or the manner of redemption on, WorldRewardZ, . By becoming a WorldRewardZ Accountholder, you acknowledge and accept that the mentioned foregoing acts may diminish the redemption value of WorldRewardZ Points already earned and agree you shall not be entitled to claim compensation for any such loss in value.</p>
				<p>By becoming a WorldRewardZ Accountholder, you acknowledge and agree that the Terms and Conditions, as well as other information regarding WorldRewardZ, are published and may be accessed online at www.WorldRewardZrewards.ae, and you acknowledge and agree to keep yourself updated with the most up to date and current version.</p>
				<p>In addition to these Terms and Conditions, each WorldRewardZ Accountholder continues to be subject to the General Terms and Conditions of Account Opening and the terms and conditions applicable to the relevant WorldRewardZ product which made him/her eligible for participation in WorldRewardZ, including without limitation, personal loan, car loan, mortgage loan, investment products, bancassurance, credit card, debit card, current account, savings account, fixed deposit, electronic transaction channels and such other terms and conditions as may be included by WorldRewardZ from time to time, which are incorporated by reference herein and copies of which shall be provided to the WorldRewardZ Accountholder upon request.</p>
				
				</div>
				</div>
        </li>
		<li>
				<div class="accordion_head">Membership<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
			
				<p>Membership in WorldRewardZ is automatic for each WorldRewardZ customer holding a WorldRewardZ Relationship and who is in good standing and otherwise no tin breach of any terms and conditions relating to such WorldRewardZ Relationship.</p>
				<p>The WorldRewardZ membership is limited to the primary (i.e. first named) customer accountholder. Membership qualifies the WorldRewardZ Accountholder to benefit from special offers and promotions which may be provided by WorldRewardZ from time to time, provided the WorldRewardZ Relationship is in good standing and provided there has been satisfactory conduct of the customer’s account).</p>
				<p>Membership is also subject to the qualifications and conditions (including annual WorldRewardZ Points accrual limits, if any) as determined by WorldRewardZ. WorldRewardZ reserves the right, at any time and without notice, to impose a validity period on membership, as well as to extend or reduce the same.</p>	
				<p>Membership on WorldRewardZ is non-transferable.</p>
				<p>In the case of joint or multiple holders of a WorldRewardZ Relationship, all authorized signatories will be automatically enrolled on WorldRewardZ. However, the use and redemption of WorldRewardZ Points shall only be available to the primary (i.e. first named) WorldRewardZ Relationship signatory as per WorldRewardZ Records and is limited as described in the Terms and Conditions. However, each WorldRewardZ Accountholder will accrue WorldRewardZ Points with respect to his or her related accounts or services to be credited to the WorldRewardZ Account. Each WorldRewardZ Account is identified by the Customer Identification (CIF) Number of the primary (i.e. first named) WorldRewardZ Relationship signatory as established by WorldRewardZ Records.</p>
				
				<p>In the event that a customer account signatory (ies) or a supplementary cardholder(s) is/are removed from the relevant WorldRewardZ Relationship, then such signatory (ies) or supplementary cardholder(s) is/are no longer eligible to participate in WorldRewardZ.</p>
				<p>WorldRewardZ reserves the right, at any time, at its absolute discretion, without notice and without liability whatsoever on its part, to withdraw or discontinue any one or more of the WorldRewardZ Relationships or WorldRewardZ Transactions and terminate membership on WorldRewardZ and revoke any accrued WorldRewardZ Points.</p>
				
				<p>Membership on WorldRewardZ is automatically terminated upon the death, bankruptcy, or unsatisfactory conduct of the account or relationship of the primary (i.e. first named) WorldRewardZ Relationship holder. </p>
				<p>Upon termination of membership, all WorldRewardZ Points accrued in the WorldRewardZ Account shall be immediately forfeited and cancelled by WorldRewardZ. WorldRewardZ may also delete the terminated WorldRewardZ Account on the WorldRewardZ Website.</p>	
			</div>
			</div>
            </li>
			
		<li>
				<div class="accordion_head">WorldRewardZ Points<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				<p>WorldRewardZ shall determine, from time to time, at its absolute discretion, which WorldRewardZ Relationships (including, without limitation, personal loan, car loan, investment products, banc assurance, current / savings account, fixed deposit, debit card or credit card, mortgage loan and electronic channels), and which WorldRewardZ debit card and credit card transactions (each a “WorldRewardZ Transaction”) are eligible for inclusion in WorldRewardZ and how many WorldRewardZ Points each will earn, the associated limits on earnings as well as the number of WorldRewardZ Points required for redemption of WorldRewardZ Rewards. In case of a reversal of any WorldRewardZ Transaction, WorldRewardZ Points credited to the WorldRewardZ Account as a result of such WorldRewardZ Transaction will be deducted from the accrued WorldRewardZ Points credit balance. Debits to the WorldRewardZ Account unrelated to any reversed WorldRewardZ Transaction will reduce the accrued WorldRewardZ credit balance as per the calculation criteria set out for a particular WorldRewardZ Relationship. No WorldRewardZ Points may be earned during a promotion, special offer or other black-out period or through the redemption of any WorldRewardZ Points as determined by WorldRewardZ from time to time and at its absolute discretion.</p>
				<p>Pursuant to Clause 4.1, WorldRewardZ Points are earned for each of the WorldRewardZ Relationship or WorldRewardZ Transactions subject to WorldRewardZ earning criteria, and no WorldRewardZ Points shall be awarded retroactively. Schedule B sets out the list of further conditions attached to the earning of WorldRewardZ Points in relation to each type of WorldRewardZ Relationship or WorldRewardZ Transaction. WorldRewardZ Points are calculated by rounding down the transaction value to the nearest integer.</p>
				
				<p>WorldRewardZ Points are not assignable or otherwise transferable, and are not capable of being mortgaged, charged or pledged, nor can WorldRewardZ Points of one WorldRewardZ Relationship holder or WorldRewardZ Transaction holder be combined with those of another.</p>
				
				<p>In the event of a change to the status of a WorldRewardZ Relationship (such as an upgrade or downgrade), the future calculation of WorldRewardZ Points shall be adjusted by WorldRewardZ accordingly.</p>
				
				<p class="ques">What should I do if the reward merchandise delivered to me is wrong or damaged?</p>
				<p>In the unfortunate event that the merchandise is either incorrect or damaged, please call WorldRewardZ’s Customer Support at +97144277600 or write to support@infiniasns.com, within 24 hours of the merchandise being delivered. In case we do not receive the request within 24 hours, no replacement will be permitted. WorldRewardZ will not be refunded to you. </p>
				
				<p>WorldRewardZ Points are valid only for a period of three (3) years from the start of the month during which such WorldRewardZ Points are earned. Unless used prior to the expiration date, WorldRewardZ Points shall expire on such date. Upon expiration, unused WorldRewardZ Points will be removed from the WorldRewardZ Account as of the expiration date and cannot be re-credited. It is the WorldRewardZ Relationship or WorldRewardZ Transaction holder's responsibility to be aware of both the number of WorldRewardZ Points outstanding in his account and their expiration date. This can be monitored at any time online on the WorldRewardZ Website. </p>
				
			</div>
			</div>
            </li>
			
				<li>
				<div class="accordion_head">WorldRewardZ Statement<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				
				<p>A record of WorldRewardZ activity for each WorldRewardZ Relationship will be mailed electronically or otherwise made electronically available in the form of an electronic or online statement (“WorldRewardZ Statement”) to the WorldRewardZ Accountholder on a regular basis, the regularity to be determined by WorldRewardZ at its absolute discretion. WorldRewardZ Points shall be transferred on a periodic basis as per the WorldRewardZ Relationship or WorldRewardZ Transaction activity into the WorldRewardZ Account for the purpose of accruing WorldRewardZ Points. All the WorldRewardZ Points shall accrue and expire as described in these Terms and Conditions and any additional terms and conditions contained in the WorldRewardZ Statement. A WorldRewardZ Relationship holder may access a copy of his/her WorldRewardZ Statement online on the WorldRewardZ Website.</p>
				
				<p>Any manifest error in the WorldRewardZ Statement must be reported to WorldRewardZ within fourteen (14) days of the WorldRewardZ Statement date.</p>
				
			</div>
			</div>
            </li>
			
			<li>
			<div class="accordion_head">Redemtion OF WorldRewardZ Points<span class="plusminus">+</span></div>
			<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				
				<p>Provided that the WorldRewardZ Relationship is active and in good standing, and subject to the approval of WorldRewardZ, WorldRewardZ Points may be redeemed (i) in exchange for air tickets, hotel stays or other specific merchandise, as may be published from time to time on the WorldRewardZ Website, if available for purchase, (ii) vouchers for any of the items of merchandise mentioned in (i) above, or (iii) to off-set specified WorldRewardZ fees and/or charges, as determined by WorldRewardZ.</p>
				
				<p>Only WorldRewardZ Points that are registered in and credited to the WorldRewardZ Account, at the time of redemption, are eligible for redemption as determined conclusively by WorldRewardZ in accordance with the WorldRewardZ Records and in the absence of manifest error.</p>
				
				<p>The redemption of WorldRewardZ Points by each WorldRewardZ Accountholder is effected by logging onto and using the WorldRewardZ Website. Any instructions conveyed by the WorldRewardZ Relationship holder will be governed by the terms and conditions of the respective mode of redemption. WorldRewardZ, at its absolute discretion, may act upon the instructions received through these modes of redemption.</p>
				
				<p>In the case of joint or multiple WorldRewardZ Relationship holders, only the primary (i.e. first named) signatory is eligible to redeem WorldRewardZ Points. Additional WorldRewardZ Points, however, may be purchased for a non-refundable fee if made available and agreed by WorldRewardZ.</p>
				<p>In the case where WorldRewardZ Points are subtracted from the WorldRewardZ Account and it reduces or eliminates the accumulated WorldRewardZ Points resulting in a negative WorldRewardZ Account balance, WorldRewardZ may, at its absolute discretion, either suspend delivery of the WorldRewardZ Reward or permit the delivery of the WorldRewardZ Reward and either debit the equivalent cash value of the negative balance or use any subsequently accrued WorldRewardZ Points to offset the negative WorldRewardZ Account balance until such balance has returned to zero or positive. </p>
				
				<p>WorldRewardZ Points redemptions (each a "WorldRewardZ Reward") are always subject to WorldRewardZ’s approval, WorldRewardZ’s terms and conditions for the relevant WorldRewardZ Reward, the availability of the WorldRewardZ Reward at the time the redemption is requested, and any restrictions imposed by any supplier or issuer of the good or services the subject of the WorldRewardZ Reward. At any time and at its absolute discretion, WorldRewardZ may, without notice or liability, cancel the WorldRewardZ Reward order and substitute the WorldRewardZ Reward with another of comparable nature and value as determined by WorldRewardZ. </p>
                <p>Details of the WorldRewardZ Rewards are set out on the WorldRewardZ Website. Reasonable efforts have been made to ensure that the information contained in the WorldRewardZ Rewards Catalogue for Goods and Services is accurate. WorldRewardZ is not responsible, and shall not be held responsible, for any errors, inaccuracies or omissions appearing therein.</p>
                <p>Redemption of air tickets, hotel stays, other merchandise and or the issue of vouchers for the same pick up the specified reward/s at / from WorldRewardZ -authorized merchants and are subject to the terms and conditions as may be prescribed by the issuer or supplier thereof from time to time.</p>
                <p>Once issued, redemption of air tickets, hotel stays, other merchandise and vouchers are not exchangeable, returnable, refundable, or redeemable for cash or credit, nor will they be replaced in the event of loss, damage or destruction. Once submitted, an order for a redemption of air tickets, hotel stays or other merchandise or a voucher cannot be cancelled, revoked, transferred or changed by the WorldRewardZ Accountholder in any manner.</p>
                <p>Redemption of air tickets, hotel stays and other merchandise or the issue of vouchers for the same are sent to the WorldRewardZ Accountholder’s email ID or billing address in WorldRewardZ Records. If the WorldRewardZ Accountholder wishes to have them sent to a different e-mail ID or delivery address, the e-mail ID or delivery address contained in the WorldRewardZ Accountholder’s account profile on the WorldRewardZ Website must be updated prior to redeeming any WorldRewardZ Points. WorldRewardZ will not be responsible for any redemption of air tickets, hotel stays or other merchandise or the issue of a voucher sent to the wrong e-mail ID or delivery address or returned as a result of the WorldRewardZ Accountholder’s failure to update his/her account profile information on the WorldRewardZ Website.</p>
                <p>Should the goods or services the subject of a WorldRewardZ Reward be received in a damaged or defective condition, the WorldRewardZ Accountholder must notify the relevant supplier of such damage or defect within the stipulated number of days as mentioned in the purchase receipt. WorldRewardZ shall use its reasonable efforts to convey the WorldRewardZ Accountholder’s complaint to the supplier and arrange, whenever possible, for an appropriate replacement of comparable nature and value as determined by WorldRewardZ. Notwithstanding the foregoing, WorldRewardZ shall not be held responsible in any manner whatsoever for damaged or defective goods or services the subject of a WorldRewardZ Reward.</p>

			</div>
			</div>
            </li>
		
			<li>
				<div class="accordion_head">Forfeiture oF WorldRewardZ Points<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				<p>All WorldRewardZ Points of a WorldRewardZ Accountholder shall be forfeited and shall be cancelled immediately and no additional or unprocessed redemption requests of WorldRewardZ Points shall accrue in the WorldRewardZ Account in the following circumstances: (a) the WorldRewardZ Relationship has been closed or is not in good standing (as determined by WorldRewardZ at its absolute discretion); (b) breach of any of these Terms and Conditions and/or any other terms and conditions expressly incorporated by reference in these Terms and Conditions; or (c) any other event or circumstance exists, which, in the absolute discretion of WorldRewardZ, should result in such forfeiture.</p>
			
			</div>
			</div>
            </li>
            <li>
				<div class="accordion_head">Indemnity<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				<p>WorldRewardZ makes no representation and provides no warranty whatsoever, expressed or implied, and undertakes and assumes no liability whatsoever, in respect of the quality of any goods or services the subject of a WorldRewardZ Reward or their suitability or fitness for any use or purpose. All goods or services the subject of a WorldRewardZ Rewards shall be accepted by the WorldRewardZ Relationship holder at his/her own risk and peril.</p>
			    <p>WorldRewardZ is not responsible for any disputes involving WorldRewardZ Rewards or any other aspect of WorldRewardZ between joint signatories or multiple persons holding the relevant UAE Rewards Relationship. Any personal liability arising out of the delivery or use of WorldRewardZ Rewards is solely the responsibility of the WorldRewardZ Accountholder and any other holders of the relevant UAE Rewards Relationship.</p>
                
			</div>
			</div>
            </li>

            <li>
				<div class="accordion_head">Breach of Terms & Conditions<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
				<p>In the event of a breach by a WorldRewardZ Accountholder of these Terms and Conditions or of any applicable provisions in any terms and conditions incorporated by reference in these Terms and Conditions, or in the event of the failure on the part of a WorldRewardZ Accountholder to pay for any outstanding amounts due to WorldRewardZ within the prescribed time set by WorldRewardZ or for any other reason whatsoever, WorldRewardZ reserves the right, at its absolute discretion, to:</p>
			    <p>Terminate the WorldRewardZ Accountholder’s membership on WorldRewardZ and close his/her WorldRewardZ Account;</p>
                <p>Tefuse to award any future WorldRewardZ Points;</p>
                <p>Withdraw any WorldRewardZ Points earned; and/orRefuse to permit the redemption of any WorldRewardZ Points.</p>
                <p>Such action by WorldRewardZ in respect of a WorldRewardZ Accountholder may result, in WorldRewardZ’s absolute discretion, in the forfeiture of all of the WorldRewardZ Accountholder’s WorldRewardZ Points and any WorldRewardZ Rewards purchased and unused.</p>
                
			</div>
			</div>
            </li>

             <li>
				<div class="accordion_head">Governing Law<span class="plusminus">+</span></div>
				<div class="accordion_body" style="display: none;">
			<div class="description" id="discription" style="overflow: hidden;" tabindex="2">
                <h2>SCHEDULE A</h2>
                <p class="ques">WorldRewardZ Relationship*</p>
				<p>Salary Accounts – where the monthly salary is AED 5,000 or more;</p>
			    <p>Fixed Deposit;</p>
                <p>Debit Card or Credit Card issued by WorldRewardZ; orAny other banker customer relationship as determined by WorldRewardZ from time to time.</p>
                <p>* The WorldRewardZ Rewards Earning Table will be mailed electronically and/or published on the WorldRewardZ Website, and may be amended, supplemented, updated, replaced or otherwise varied from time to time by WorldRewardZ, at its absolute discretion without prior notice. The WorldRewardZ Rewards Earning Table sets out the specific terms and conditions relating to each WorldRewardZ Relationship and each UAE Rewards Transaction and such table, terms and conditions shall be incorporated by reference in these Terms and Conditions.</p>
               <h2>SCHEDULE B</h2>
                <p class="ques">For Debit Cards</p>
                <p>No WorldRewardZ Points shall be earned for WorldRewardZ debit card transactions on domestic ATM withdrawals and related to the following: (a) fees, charges and interest; (b) transactions that WorldRewardZ determines, in its absolute discretion, are disputed, erroneous, unauthorized, illegal and/or fraudulent; and (c) any other transactions that WorldRewardZ may include on this list from time to time.</p>
                <p class="ques">For Credit Cards</p>
                <p>No WorldRewardZ Points shall be earned for WorldRewardZ credit card transactions related to the following: (a) fees, charges and interest; (b) any payments or amounts deposited or credited to the card account (c) transactions that WorldRewardZ determines, in its absolute discretion, are disputed, erroneous, unauthorized, illegal and/or fraudulent; and (d) any other transactions that WorldRewardZ may include on this list from time to time.</p>
                <p class="ques">For Payroll (on monthly salary transfer of AED 5,000 or more)</p>
                <p>WorldRewardZ Points will be credited to the WorldRewardZ Account upon receipt of monthly salary. The amount of the salary against which WorldRewardZ Points can be earned will be at the absolute discretion of WorldRewardZ and may not include allowances, bonuses, reimbursements and other similar salary credits.</p>
                <p class="ques">For Time Deposit</p>
                <p>WorldRewardZ Points shall be earned and credited to the WorldRewardZ Account as per conditions conveyed by WorldRewardZ from time to time, and only if such WorldRewardZ Account remains active and in good standing during such period.</p>
			</div>
			</div>
            </li>
	</div>
        </div>
		</li>
	    </ul>	
	    </div>

</article>
</asp:Content>

