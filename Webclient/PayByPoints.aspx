﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="PayByPoints.aspx.cs" Inherits="PayByPoints" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">

    <link rel="stylesheet" href="css/PayByPoints.css" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript">
        function GetCalculatedPoints() {
            var amount = $("#CP_txtAmount").val();
            var msg = "";

            if (amount.length != 0) {

                if (!RegexFunc(/^-?\d*\.?\d*$/, amount)) {
                    msg += "Please Enter valid amount.</br>";
                }
                else {
                    if (amount.length >= 9) {
                        msg += "Please enter valid amount.</br>";
                    }
                }

            }

            if (msg.length > 0) {
                $("#CP_ErrorMsg_UpperDiv").show();
                $("#CP_ErrorMsg_UpperDiv")[0].innerHTML = msg;
            }
            else {
                $("#CP_ErrorMsg_UpperDiv")[0].innerHTML = "";
                $.ajax({
                    type: 'POST',
                    url: 'PayByPoints.aspx/GetCalculatedPoints',
                    contentType: 'application/json; charset=utf-8',
                    datatype: 'json',
                    data: "{'pstrAmount':'" + amount + "'}",
                    cache: false,
                    success: function (msg) {
                        var Points = $.parseJSON(msg.d);

                        $("#CP_txtPoints").val(Points);
                        return false;
                    }
                });
            }


            return false;

        }

        function GetPayByPoints(Points, Amount) {

            $("#CP_lblMerchant")[0].innerHTML = Amount;
            $("#CP_divNavigation").hide();
            $("#CP_divOldTrnx").hide();
            $("#CP_divNewTrnx").hide();
            $("#CP_divRedeem").show();
            $("#CP_ErrorMsg_UpperDiv")[0].innerHTML = "";
            $.ajax({
                type: 'POST',
                url: 'PayByPoints.aspx/GetCalculatedPoints',
                contentType: 'application/json; charset=utf-8',
                datatype: 'json',
                data: "{'pstrAmount':'" + Amount + "'}",
                cache: false,
                success: function (msg) {
                    var Points = $.parseJSON(msg.d);
                    $("#CP_lblAmountForRedeem")[0].innerHTML = Amount
                    $("#CP_lblPointsForRedeem")[0].innerHTML = Points;
                    $("#CP_hdnPoint").value = Points;
                    $("#CP_txtPoints").val(Points);
                    return false;
                }
            });
            return false;
        }
        function RedeemPoints() {
            var amount = $("#CP_lblAmountForRedeem").text();
            var points = $("#CP_lblPointsForRedeem").text();

            $.ajax({
                type: 'POST',
                url: 'PayByPoints.aspx/RedeemPoints',
                contentType: 'application/json; charset=utf-8',
                datatype: 'json',
                data: "{'pstrPoints':'" + points + "','pstrAmount':'" + amount + "'}",
                cache: false,
                success: function (data) {
                    $('#CP_RdlSearchtype_0').attr('checked', false);
                    $('#CP_RdlSearchtype_1').attr('checked', false);
                    $("#CP_divNavigation").hide();
                    $("#CP_divOldTrnx").hide();
                    $("#CP_divNewTrnx").hide();
                    $("#CP_divRedeem").hide();

                    if (data.d == "Success") {
                        $("#CP_ErrorMsg_UpperDiv").show();
                        $("#CP_ErrorMsg_UpperDiv")[0].innerHTML = "Transaction successful";
                    }
                    else if (data.d == "TransactionFailed") {
                        $("#CP_ErrorMsg_UpperDiv").show();
                        $("#CP_ErrorMsg_UpperDiv")[0].innerHTML = "Transaction failed";
                    }
                    else if (data.d == "InsufficientPoints") {
                        $("#CP_ErrorMsg_UpperDiv").show();
                        $("#CP_ErrorMsg_UpperDiv")[0].innerHTML = "Insufficient Points in your account";
                    }
                    return false;
                }
            });
            return false;
        }

        function RegexFunc(regExprs, value) {
            var pattern = new RegExp(regExprs);
            return pattern.test(value);
        };

        function newRedeem() {
            var amount = $("#CP_txtAmount").val();
            var points = $("#CP_txtPoints").val();
            var msg = "";
            
            if ($('#CP_txtAmount').val() == "") {
                msg += "Please provide a valid Amount." + "<br/>";
            }
            if ($('#CP_txtPoints').val() == "") {
                msg += "Please provide a valid points." + "<br/>";
            }

            if (msg.length > 0) {
                $("#CP_ErrorMsg_UpperDiv").show();
                $("#CP_ErrorMsg_UpperDiv")[0].innerHTML = msg;
            }
            else {

                $("#CP_divNavigation").attr("style", "display:none");
                $("#CP_divOldTrnx").attr("style", "display:none");

                $("#CP_divNewTrnx").attr("style", "display:none");
                $("#CP_divRedeem").attr("style", "display:none");

                if (amount.length != 0) {
                    $.ajax({
                        type: 'POST',
                        url: 'PayByPoints.aspx/NewTransaction',
                        contentType: 'application/json; charset=utf-8',
                        datatype: 'json',
                        data: "{'pstrPoints':'" + points + "','pstrAmount':'" + amount + "'}",
                        cache: false,
                        success: function (data) {
                            var msg = $.parseJSON(data.d);
                            if (msg == "TransactionFailed") {
                                $("#CP_ErrorMsg_UpperDiv").show();
                                $("#CP_ErrorMsg_UpperDiv")[0].innerHTML = "Transaction failed";
                            }
                            else {
                                var str = "Your request for using points for subsequent transaction has been recorded. Your request number is " + msg;
                                $("#CP_ErrorMsg_UpperDiv").show();
                                $("#CP_ErrorMsg_UpperDiv")[0].innerHTML = str;
                            }
                            $('#CP_RdlSearchtype_0').attr('checked', false);
                            $('#CP_RdlSearchtype_1').attr('checked', false);

                            
                            return false;
                        }
                    });

                    return false;
                }
                else {
                    $('#CP_RdlSearchtype_0').attr('checked', false);
                    $('#CP_RdlSearchtype_1').attr('checked', false);
                    $("#CP_ErrorMsg_UpperDiv").show();
                    $("#CP_ErrorMsg_UpperDiv")[0].innerHTML = "Please enter valid amount";
                }
            }
            return false;
        }
    </script>
    <style type="text/css">
        @media screen and (max-device-width :768px), screen and (max-width:768px) {

            .pass-desktop1 {
                display: none;
            }

            .pass-mobile1 {
                display: block;
            }
        }

        @media only screen and (min-width:769px) and (orientation:landscape) {
            .pass-desktop1 {
                display: block;
            }

            .pass-mobile1 {
                display: none;
            }
        }
    </style>
    <script src="js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="js/jquery.mCustomScrollbar.css" type="text/css" />
    <script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $("#voucher").niceScroll({
                cursorcolor: "#e3443e",
                cursoropacitymin: 0.3,
                background: "#bbb",
                cursorborder: "0",
                autohidemode: false,
                cursorminheight: 30
            });
        });
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <div class="Flightindx-bg">
            <div class="wrap">
                <div class="servindx">
                    <div class="servindx-bnr">
                        <img src="images/pay-by-point.jpg" />
                    </div>
                    <div id="PayByPoints" class="tabbertabPayByPoints">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                        <div class="Ourproduct_Rightpannel">
                            <div class="width100  " id="DivSubmitContent" runat="server">
                                <div class="font20 fl width100 pTop10">
                                    Pay By Points
                                </div>
                                <div class="fl width100 pTop10">
                                    Please select the transaction type of pay by Points. 
                                </div>
                                <div class="clr">
                                </div>
                                <div class="width100 RightInnerContent ">
                                    <asp:RadioButtonList ID="RdlSearchtype" runat="server" OnSelectedIndexChanged="RdlSearchtype_SelectedIndexChanged"
                                        AutoPostBack="True" AppendDataBoundItems="True" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="0" Selected="True">Old Transaction	</asp:ListItem>
                                        <asp:ListItem Value="1">New Transaction</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <br />
                            <div class="width100 display_hide" id="divOldTrnx" runat="server">
                                <asp:Repeater ID="RepAccrualSummaryInfo" runat="server">
                                    <HeaderTemplate>
                                        <table width="100%" cellspacing="0">
                                            <tr class="stylegridheader">
                                                <td>Description
                                                </td>
                                                <td>Amount
                                                </td>
                                                <td>Point
                                                </td>
                                                <td>Transaction Date
                                                </td>
                                                <td>Txn Type
                                                </td>
                                                <td>&nbsp;
                                                </td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="stylealternate1">
                                            <td>
                                                <%#Eval("MerchantName")%>
                                            </td>
                                            <td>
                                                <%#Eval("Amounts")%>
                                            </td>
                                            <td>
                                                <%#Eval("Points")%>
                                            </td>
                                            <td>
                                                <%#Eval("TransactionDate", "{0:d MMMM, yyyy}")%>
                                            </td>
                                            <td>
                                                <%#Eval("LoyaltyTxnType").ToString()%>
                                            </td>
                                            <%-- <td>
                                                        <%#Eval("Narration")%>
                                                    </td>--%>
                                            <td>
                                                <asp:LinkButton ID="BtnPayByPoints" Text="Pay By Points" runat="server" OnClientClick='<%# "return GetPayByPoints("+Eval("Points")+","+Eval("Amounts")+ ");" %>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr class="stylealternate2">
                                            <td>
                                                <%#Eval("MerchantName")%>
                                            </td>
                                            <td>
                                                <%#Eval("Amounts")%>
                                            </td>
                                            <td>
                                                <%#Eval("Points")%>
                                            </td>
                                            <td>
                                                <%#Eval("TransactionDate", "{0:d MMMM, yyyy}")%>
                                            </td>
                                            <td>
                                                <%#Eval("LoyaltyTxnType").ToString()%>
                                            </td>
                                            <%--<td>
                                                        <%#Eval("Narration")%>
                                                    </td>--%>
                                            <td>
                                                <asp:LinkButton ID="BtnPayByPoints" Text="Pay By Points" runat="server" OnClientClick='<%# "return GetPayByPoints("+Eval("Points")+","+Eval("Amounts")+ ");" %>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="Clear"></div>
                            <div id="divNavigation" runat="server">
                                <table class="PageingStyle">
                                    <tr>
                                        <td class="repeaterPageingFirstTab">
                                            <asp:LinkButton ID="linkbuttonFirst" runat="server" CssClass="MyAccount_Table_label"
                                                OnClick="lbtnFirst_Click">First</asp:LinkButton>
                                        </td>
                                        <td class="repeaterPageingPreTab">
                                            <asp:LinkButton ID="linkbuttonPrevious" runat="server" CssClass="MyAccount_Table_label"
                                                OnClick="lbtnPrevious_Click">Previous</asp:LinkButton>
                                        </td>
                                        <td class="repeaterPageingNumberTab">
                                            <asp:DataList ID="dlPaging" runat="server" OnItemCommand="dlPaging_ItemCommand" RepeatDirection="Horizontal"
                                                ShowFooter="False" ShowHeader="False" Width="10px" OnItemDataBound="dlPaging_ItemDataBound">
                                                <ItemStyle />
                                                <ItemTemplate>
                                                    <asp:Label ID="LblPageNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' Visible="false"></asp:Label>
                                                    <asp:LinkButton ID="lnkbtnPaging" runat="server" CommandArgument='<%#Eval("PageIndex")%>'
                                                        CommandName="Paging" CssClass="MyAccount_Table_label" Text='<%# Eval("ID")%>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </td>
                                        <td class="repeaterPageingNextTab">
                                            <asp:LinkButton ID="linkbuttonNext" runat="server" CssClass="MyAccount_Table_label"
                                                OnClick="lbtnNext_Click">Next</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                        <td class="repeaterPageingNextTab">
                                            <asp:LinkButton ID="linkbuttonLast" runat="server" CssClass="MyAccount_Table_label"
                                                OnClick="lbtnLast_Click">Last</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <br />
                            <div class="width100 display_hide" id="divNewTrnx" runat="server">
                                <div class="RightInnerContent">
                                    <asp:Label ID="lblAmount" runat="server" Text="Enter Amount :" CssClass=""></asp:Label>
                                </div>

                                <div class="RightInnerContent">
                                    <asp:TextBox ID="txtAmount" runat="server" CssClass=""
                                        onkeyup="var retValue = GetCalculatedPoints(); event.returnValue = retValue; return retValue;"></asp:TextBox>
                                </div>
                                <br style="clear: both;" />
                                <div class="RightInnerContent">
                                    <asp:Label ID="lblPoints" runat="server" Text="Enter Points :" CssClass=""></asp:Label>

                                </div>
                                <div class="RightInnerContent">
                                    <asp:TextBox ID="txtPoints" runat="server" Enabled="false" CssClass=""></asp:TextBox>
                                </div>
                                <br style="clear: both;" />
                                <div class="RightInnerContent">
                                    &nbsp;

                                </div>
                                <div class="RightInnerContent">
                                    <div class="buttonContainer darkBtn" style="width: 100px">
                                        <div class="buttonRight">
                                            <div class="buttonLeft">
                                                <asp:Button CssClass="buttonContainer" runat="server" ID="btnContinue" Text="Continue"
                                                    OnClientClick="var retvalue = newRedeem(); event.returnValue= retvalue; return retvalue;"
                                                    CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="width100 display_hide" id="divRedeem" runat="server">


                                <div class="">
                                    <asp:Label ID="lblMerchant" runat="server" Text="" CssClass="" Style="display: none"></asp:Label>
                                </div>

                                <div class="">
                                    <asp:Label ID="lblAmountForRedeem" runat="server" Text="" CssClass="" Style="display: none"></asp:Label>
                                </div>

                                <div class="">
                                    Number of WorldRewardZ required to redeem:
                                    <asp:Label ID="lblPointsForRedeem" runat="server" Text="" CssClass=""></asp:Label>
                                </div>
                                <div class="Login_FormField_Lable">
                                    <div class="buttonLeft" style="color: black">

                                        <div class="buttonContainer darkBtn" style="width: 100px">
                                            <div class="buttonRight">
                                                <div class="buttonLeft">
                                                    <asp:Button CssClass="buttonContainer" runat="server" ID="btnRedeem" Text="Confirm"
                                                        OnClientClick="var retValue = RedeemPoints(); event.returnValue = retValue; return retValue;" />
                                                    <%--<asp:Button CssClass="buttonContainer" runat="server" ID="btnRedeem" Text="Continue"
                                                OnClientClick="var retvalue = RedeemPoints(); event.returnValue= retvalue; return retvalue;"
                                                />--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="width100 msgbxBG mTop20 display_hide" id="divConfirmationMsg" runat="server">
                                Dear Customer,<br />
                                <br />
                                Congratulations. Your transaction is successful. To view a record of all your Gift
                                        Voucher codes, click on <b>“My Account”</b> and scroll down to <b>“Voucher Summary”</b> and you
                                        may print your Gift Voucher codes. Use your Gift Voucher codes to redeem at any
                                        of our participating partners. 
                            </div>

                            <div class="width100  msgbxBG mTop20 display_hide" id="ErrorMsg_UpperDiv" runat="server">
                            </div>

                            <div class="width100  msgbxBG mTop20 display_hide" id="divAvailability" runat="server">
                                Dear Customer,<br />
                                <br />
                                You do not have sufficient WorldRewardZ.<br />
                                <a href="PurchasePoints.aspx">Click here </a>to purchase additional WorldRewardZ.
                            </div>
                        </div>
                          </ContentTemplate>
                            <Triggers>
                                <%--<asp:AsyncPostBackTrigger ControlID="btnContinue" EventName="Click" />--%>
                                
                                    
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="clr">
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

