﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="HotelResults.aspx.cs" Inherits="HotelResults" %>



<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <%--<script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="js/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/jquery.ui.slider.js" type="text/javascript"></script>--%>

    <script src="js/angular.js"></script>
    <script src="js/angular-animate.js"></script>
    <script src="js/ui-bootstrap-tpls-1.3.3.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/angular.rangeSlider.js"></script>
    <link href="css/datepicker.css" rel="stylesheet" />
    <link href="css/angular.rangeSlider.css" rel="stylesheet" />
    <script src="js/hotelsearch.js" type="text/javascript"></script>
    <link href="css/hotel.css" rel="stylesheet" />
    <asp:HiddenField ID="hdnHotelFilterRange" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="HFNoOfRooms" runat="server" />
    <asp:HiddenField ID="hdnNoAdult" runat="server" />
    <asp:HiddenField ID="hdnNoChild" runat="server" />
    <asp:HiddenField Value="" ID="hdnPaymentType" runat="server"></asp:HiddenField>

    <div class="inrpg">
        <div class="breadcrumbs">
            <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="SiteMap_Root_Stlye"
                PathSeparatorStyle-CssClass="seperator" PathSeparator=" >> ">
                <NodeStyle />
                <RootNodeStyle />
            </asp:SiteMapPath>
        </div>
        <div class="pgcontent flight" ng-app="Danamon" ng-controller="MainCtrl">
            <div>
                <div class="btn-filter" id="btnfliter">
                    <img src="Images/ico-fltr.png" />
                </div>
                <div id="divBlockAll">
                </div>
                <div class="pgFltcol2" id="divSearch">
                    <div class="widget-prfl" id="UserInfo">
                        <div class="widget-user">
                            <ul id="UserDetails">
                            </ul>
                        </div>
                        <div id="Content_Wrapper">
                            <div class="widget-srhpnl">
                                <h3 ng-click="showModifyHotel()">Modify Search</h3>
                                <div id="modifyDetails" style="display: none">
                                    <div class="widget-flds">
                                        <ul>
                                            <li class="autogen">
                                                <label>Destination</label><input class="space4" type="text" id="txtCity" ng-click='empty("txtCity")' runat="server" typeahead-min-length='3' ng-model="Hotelcountry" uib-typeahead="HotelCity for HotelCity in getAllHotelCities($viewValue)" /></li>

                                            <li class="dates">
                                                <ul>
                                                    <li>
                                                        <label>Check-In</label><input id="TextBoxCheckin" runat="server" class="space2" type="text" /></li>
                                                    <li class="mrno">
                                                        <label>Check-Out</label><input id="TextBoxCheckout" runat="server" class="space2" type="text" /></li>
                                                </ul>
                                            </li>
                                            <li class="dates">
                                                <ul>
                                                    <li>
                                                        <label>Room(s)</label>
                                                        <select id="ddlRoomno">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="rooms append"></li>
                                            <div class="clr"></div>

                                        </ul>
                                    </div>
                                    <asp:HiddenField ID="hdnRoomString" runat="server"></asp:HiddenField>
                                    <div class="button" ng-click="SearchRoom()">Search</div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="fliterDetails">
                        <h3>Refine Your Search</h3>
                        <div style="display: block">
                            <div class="rfnsrch">
                                <div class="rwdpnts  range">
                                    <h3>Total WorldRewardZ</h3>
                                    <div range-slider min="priceSlider.range.min" max="priceSlider.range.max" model-min="priceSlider.minPrice" model-max="priceSlider.maxPrice" step="1" on-handle-up="priceSliderStop()"></div>
                                    <div class="clr"></div>
                                    <span id="priceRange"></span>

                                </div>
                                <hr class="style-six">
                                <div class="htlchain">
                                    <h3>Hotel Chains</h3>
                                    <ul>
                                        <li>
                                            <input type="checkbox" id="ChkSelectAll" ng-model="hotelstars" ng-click="showImage()" ng-checked="hotelstars" />&nbsp;Select All

                                        </li>
                                    </ul>
                                    <ul id="divRatings">
                                    </ul>
                                </div>
                                <hr class="style-six">
                                <div class="rwdpnts">
                                    <h3>Amenities</h3>
                                    <ul>
                                        <li>
                                            <input type="checkbox" id="chkSelectAllAmenities" ng-model="master" ng-click="showImage()" ng-checked="master" />&nbsp;Select All
                                        </li>
                                    </ul>
                                    <ul id="divBasicAmenities">
                                        <li>
                                            <input type="checkbox" ng-checked="master" id="amnAirCondition" display="Air Condition" ng-click="showImage()" checked="checked" />
                                            <img src="Images/AC_Car_Icon.png" />Air Conditioning</li>
                                        <li>
                                            <input type="checkbox" ng-checked="master" id="amnBar" display="Bar" ng-click="showImage()" checked="checked" />
                                            <img src="Images/bar.png" />Bar </li>
                                        <li>
                                            <input type="checkbox" ng-checked="master" id="amnBussinessCenter" display="Bussiness Center" ng-click="showImage()" checked="checked" />
                                            <img src="Images/business_center.png" />Business Centre</li>
                                        <li>
                                            <input type="checkbox" ng-checked="master" id="amnCoffee" display="Coffee" ng-click="showImage()" checked="checked" />
                                            <img src="Images/coffee_shop.png" />Coffee Shop </li>
                                        <li>
                                            <input type="checkbox" ng-checked="master" id="amnGym" display="Gym" ng-click="showImage()" checked="checked" />
                                            <img src="Images/gym.png" />Gym </li>
                                        <li>
                                            <input type="checkbox" ng-checked="master" id="amnInternet" display="Internet Access" ng-click="showImage()" checked="checked" />
                                            <img src="Images/internet_access.png" />Internet Access</li>
                                        <li>
                                            <input type="checkbox" ng-checked="master" id="amnPool" display="Pool" ng-click="showImage()" checked="checked" />
                                            <img src="Images/pool.png" />Pool </li>
                                        <li>
                                            <input type="checkbox" ng-checked="master" id="amnRestaurant" display="Restaurant" ng-click="showImage()" checked="checked" />
                                            <img src="Images/restaurant.png" />Restaurant </li>
                                        <li>
                                            <input type="checkbox" ng-checked="master" id="amnRoomService" display="Room Service" ng-click="showImage()" checked="checked" />
                                            <img src="Images/room_service.png" />Room Service </li>
                                        <li>
                                            <input type="checkbox" ng-checked="master" id="amnWifi" display="Wi-fi" ng-click="showImage()" checked="checked" />
                                            <img src="Images/network_blue.png" />Wi-Fi Access </li>
                                    </ul>
                                </div>
                                <hr class="style-six">
                                <div class="rwdpnts">
                                    <h3>Hotels Chains</h3>
                                    <ul>
                                        <li>
                                            <input type="checkbox" id="chkHotelChains" ng-model="hotelchains" ng-click="showImage()" ng-checked="hotelchains" />&nbsp;Select All
                                        </li>
                                    </ul>
                                    <ul id="divHotelChain">
                                    </ul>
                                </div>
                                <hr class="style-six">
                                <div class="rwdpnts">
                                    <h3>Hotels Locations</h3>
                                    <ul>
                                        <li>
                                            <input type="checkbox" id="chkLocation" ng-model="hotellocations" ng-click="showImage()" ng-checked="hotellocations" />&nbsp;Select All</li>
                                    </ul>
                                    <ul id="divLocation">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pgFltcol1">
                    <div class="srchtitle"></div>
                    <div class="alrt" id="chkinchkout"></div>
                    <div class="usrched">
                        <ul id="SearchSummary" runat="server">
                        </ul>

                    </div>
                    <div class="mobusrched">
                        <ul id="mobSearchSummary" runat="server">
                        </ul>
                    </div>
                    <hr />
                    <br class="clr" />
                    <div class="hotels">
                        <asp:Repeater ID="rptHotelList" runat="server" OnItemDataBound="rptHotelList_ItemDataBound">
                            <ItemTemplate>
                                <div class="hltbox hotelrow">
                                    <div class="htlinfo">
                                        <div class="htlimgnm">
                                            <div class="htlimg">
                                                <img src="<%# ((string)Eval("basicinfo.thumbnailimage")).Contains("http") ? Eval("basicinfo.thumbnailimage") : "http://www.cleartrip.com/places/hotels"+Eval("basicinfo.thumbnailimage") %>" alt="Image not Avialable" onerror="this.onerror=null;this.src='images/no-image.png';" onclick='<%# "return getHotelDetails(" + Eval("hotelid") + ");"%>' />
                                            </div>
                                            <div class="htlname">
                                                <h2 onclick='<%# "return getHotelDetails(" + Eval("hotelid") + ");"%>'><%#Eval("basicinfo.hotelname")%>
                                                    <asp:HiddenField ID="hdnHotelID" runat="server" Value='<%#Eval("HotelID")%>' />
                                                </h2>
                                                <ul class="htlservs">
                                                    <li>
                                                        <asp:Image ID="imgMeeting" runat="server" ImageUrl="~/Images/ico-meet.png" ToolTip="Meeting Facilities Not Available" />
                                                    </li>
                                                    <li>
                                                        <asp:Image ID="imgGym" runat="server" ImageUrl="~/Images/ico-gym.png" ToolTip="Gym/Spa Not Available" />
                                                    </li>
                                                    <li>
                                                        <asp:Image ID="imgResturent" runat="server" ImageUrl="~/Images/ico-meet.png" ToolTip="Restaurant/Coffee Shop Not Available" />
                                                    </li>
                                                    <li>
                                                        <asp:Image ID="imgInternet" runat="server" ImageUrl="~/Images/ico-wifi.png" ToolTip="Internet/Wi-Fi Not Available" />
                                                    </li>
                                                    <li>
                                                        <asp:Image ID="imgswimmingPool" runat="server" ImageUrl="~/Images/ico-swim.png" ToolTip="Swimming Pool Not Available" />
                                                    </li>
                                                </ul>
                                                <div class="ratings" ratings='<%#Eval("basicinfo.hotelratings.HotelRating[0].rating")%>'></div>

                                                <%--<img src="images/rdmr-ico.png"></a>--%>
                                            </div>
                                        </div>
                                        <div class="abthtl">
                                            <p>
                                                <%#Eval("basicinfo.address")%>
                                            </p>

                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                    <div class="pntsbtn">

                                        <div class="dpnts">
                                            <ul>
                                                <li class="pnts">
                                                    <asp:Label ID="lblmiles" runat="server"></asp:Label></li>
                                                <li>RewardZ  for<br>
                                                    <asp:Label ID="lblNoOfNights" runat="server" Text=""></asp:Label>
                                                    Night(s)</li>
                                            </ul>
                                        </div>

                                        <div class="dbtns">
                                            <div class="button" onclick='<%# "return getHotelDetails(" + Eval("hotelid") + ");"%>'>Book a Stay</div>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                    <div class="jsondata">
                                        <asp:HiddenField ID="hdnHotelJsonData" runat="server"></asp:HiddenField>
                                    </div>
                                    <div class="clr"></div>
                                </div>

                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    </br>
                    
                    <div class="clr"></div>
                </div>
                <br class="clr" />
            </div>
        </div>
    </div>
</asp:Content>

