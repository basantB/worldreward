﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BookingFailure : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string BookingFail()
    {
        string lstr = Convert.ToString(HttpContext.Current.Session["BookingError"]);
        HttpContext.Current.Session.Abandon();
        return lstr;
    }
}