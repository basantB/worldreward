﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="HotelVoucher.aspx.cs" Inherits="HotelVoucher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <div style="width: 768px; margin: 0 auto">
        <div class="revncofm">
            <div class="wrapper">
                <!--Infi Destination Starts-->
                <div class="body-bdr">
                    <h2>Thank you
                    <asp:Label ID="lblPersonName" runat="server" Text="Nil"></asp:Label>, Your booking
                    is now <strong>confirmed.</strong>
                    </h2>
                    <div class="reference">
                        <div class="col1">
                            Reference No.
                            <asp:Label ID="lblTransactionReference" runat="server" Text=""></asp:Label>
                        </div>
                        <div id="divTotalMiles" runat="server" class="col2">
                        </div>
                    </div>
                    <div class="mt20x">
                        Voucher No. : <strong>
                            <asp:Label ID="lblExternalRefId" runat="server" Text=""></asp:Label>
                        </strong>
                    </div>
                    <div class="mt20x">
                        Booking ID : <strong>
                            <asp:Label ID="lblBookingID" runat="server" Text="Nil"></asp:Label>
                        </strong>
                    </div>
                    <br />
                    <br />
                    <div class="pass-details">
                        <div class="w50P FL">
                            <h3>Booking Details
                            </h3>
                            <div class="tblbody">
                                <ul class="">
                                    <li>
                                        <div class="label">
                                            Your Reservation
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblTotalRoom" runat="server" Text="Nil"></asp:Label><asp:Label ID="lblTotalnight"
                                            runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Check-in
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblCheckinDate" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Check-out
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblCheckoutDate" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Booked by
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblBookPersonName" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            EMail
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblPersonEmail" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Address
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblPersonAddress" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="w50P FL ml1p">
                            <h3 class="tblhdr-blk">Hotel Details
                            </h3>
                            <div class="tblbody">
                                <ul class="">
                                    <li>
                                        <div class="label">
                                            Hotel Name
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblHotelName" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Address
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblAddress" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Contact
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblHotelPhone" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Fax
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblHotelFax" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Website
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblWebsite" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="label">
                                            Rating
                                        </div>
                                        <div class="result">
                                            :
                                        <asp:Label ID="lblRating" runat="server" Text="Nil"></asp:Label>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                    <div class="otherinfo w100P">

                        <div class="pass-details">
                            <div class="w50P FL">
                                <h3>Room Details</h3>
                                <p>
                                    <asp:Label ID="lblRoomtype" runat="server" Text="Nil"></asp:Label>
                                </p>
                            </div>
                            <div class="w50P FL">
                                <h3>Special Request</h3>
                                <p>
                                    <asp:Label ID="lblSpecialRequest" runat="server" Text="Nil"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="pass-details">
                            <div class="w50P FL">
                                <h3>Payment Type</h3>
                                <div style="width: 100%;" id="divPaymentDetails" runat="server">
                                </div>
                                <div class="note">
                                    Please Note: Additional Supplements (e.g. extra bed) are not added
                                to this total.
                                </div>
                            </div>
                            <div class="w50P FL">
                                <asp:Image ID="imgMap" runat="server" />
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                </div>
                <!--Infi Destination Ends-->
            </div>
            <div style="float: right; margin: 10px; width: 100%;text-align:right">
                <a onclick="window.open('HotelVoucherPrint.aspx','','height=1000','width=1000')">
                    <button class="button">
                        Print</button></a>
            </div>
        </div>
    </div>
    <div class="clr"></div>

</asp:Content>
