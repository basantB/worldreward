﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="HotelDetails.aspx.cs" Inherits="HotelDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <script src="js/HotelDetails.js" type="text/javascript"></script>
    <link href="css/hotel.css" rel="stylesheet" />
    <div class="inrpg">
        <div class="pgcontent htldtlspg">


            <div class="pgFltcol2" id="divSearch">
                <div class="smrhtls" id="NextHotelList">
                </div>
            </div>
            <div class="pgFltcol1">

                <div class="htldtls">
                    <div>
                        <div class="hltname"><span id="HotelName"></span></div>
                        <div class="htlstars" id="divrating"></div>
                        <div class="clr"></div>
                    </div>
                    <div class="hltaddrs"><span id="HotelAddress"></span><span id="HotelCity"></span></div>
                    <div class="hidecontent" id="DivAmenities">
                        <div class="Closepopup_btn">
                            <img id="linkbuttonclose" src="Images/ico_close.png" onclick="return CloseDetails();" />
                        </div>
                    </div>
                    <div class="hidecontent" id="divGoogleMaps" style='height: 250px; width: 350px;'>
                        <div class="Closepopup_btn">
                            <img id="divgooglebtnclose" src="Images/ico_close.png" onclick="return CloseDetails();" />
                        </div>
                    </div>
                    <div id="Fade" class="black_overlay"></div>
                    <div class="htldtsamtis" id="BasicAmenities"></div>
                    <a class="ico-map" onclick="return showimage();">
                        <img src="images/ico-map.png" /></a>
                    <div class="">
                        <div class="hltgall" id="BannerImage">
                            <div id="divpre" class="btnPre">Prev</div>
                            <div id="divNext" class="btnNext">Next</div>
                        </div>
                        <div class="htltype">
                            <ul>
                                <li id="ddlhotelroom"></li>
                                <li class="hltprice">
                                    <p id="minrate"></p>
                                    <div id="totalprice" style="display:inline"></div>
                                    <p>Inclusive of all taxes.</p>
                                </li>
                                <li id="appendbtn"></li>
                            </ul>
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="abthtlcnt" id="OverView"></div>
                </div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</asp:Content>

