﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="FlightList.aspx.cs" Inherits="FlightList" %>

<asp:Content ID="ContentFlightList" ContentPlaceHolderID="CP" runat="Server">
    <style>
        .ui-progressbar {
            position: relative;
            height: 20px !important;
        }

        .martop_20 {
            margin-top: -19px;
        }
    </style>
    <asp:HiddenField ID="hdnreturnDate" runat="server"></asp:HiddenField>
    <script src="js/angular.js"></script>
    <script src="js/angular-animate.js"></script>
    <script src="js/ui-bootstrap-tpls-1.3.3.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/angular.rangeSlider.js"></script>
    <link href="css/datepicker.css" rel="stylesheet" />
    <script src="js/FlightSearch.js"></script>
    <link href="css/angular.rangeSlider.css" rel="stylesheet" />
    <link href="css/flight.css" rel="stylesheet" />
    <script type="text/javascript">
        $(window).scroll(function () {
            if ($(this).scrollTop() > 500) {
                $('#divUp').fadeIn();
            }
            else {
                $('#divUp').fadeOut();
            }
        });
        $("#divUp").click(function () {
            $("html, body").animate({ scrollTop: 0 }, 1000);
            return false;
        });
    </script>
   
    <div ng-app="DanamonFlights" ng-controller="MainCtrl">
        <div class="inrpg">
            <div class="pgcontent flight">
                <div id="mainList" style="display: none">
                    <div class="btn-filter" id="btnfliter">
                        <img src="Images/ico-fltr.png" />
                    </div>
                    <div id="divBlockAll">
                    </div>
                    <div class="pgcol1">
                        <div class="usrched">
                            <ul id="LabelYourSearchDetails">
                            </ul>
                        </div>
                        <div class="mobusrched">
                            <ul id="mobSearchSummary">
                            </ul>
                        </div>
                        <hr />

                        <%--<div class="srchtitle" id="LabelYourSearchDetails">
                        </div>--%>
                        <div class="alrt2">All timings are local. Fare not guaranteed until ticketed. Danamon Rewards displayed for redemption include surcharges and taxes.</div>
                        <div id="resultInterNational" style="display: none;">
                            <%--<div class="sorting"><span>Sort By:</span> <a href="#">Price</a> <a href="#">Rating</a> <a href="#">Non Stop</a> <a href="#">1 Stop</a> <a href="#">2 Stops</a></div>--%>
                            <div class="clr"></div>
                            <div ng-repeat="x in results" class="Rightpannel_customDivFlight seq_{{ x.SequenceNo }} flightlists">
                                <div class="">
                                    <div class="fltinfo fltrtn">
                                        <div class="fltbox">
                                            <ul>
                                                <div ng-repeat="ListOfFlightDetails in x.ListOfFlightDetails track by $index" ng-init="rowIndex = $index" class="upper">
                                                    <div ng-if="ListOfFlightDetails.ListOfFlightSegments.length == 1">
                                                        <div ng-repeat="ListOfFlightSegments in ListOfFlightDetails.ListOfFlightSegments">
                                                            <li class="airlines">
                                                                <ul>
                                                                    <li>
                                                                        <img src="{{ListOfFlightSegments.Carrier.CarrierLogoPath}}" title="{{ListOfFlightSegments.Carrier.CarrierName}}" class="img" width="33" height="33" /></li>
                                                                    <%--<li>{{ListOfFlightSegments.Carrier.CarrierName}}</li>--%>
                                                                </ul>
                                                            </li>
                                                            <li class="depart">{{ListOfFlightSegments.DisplayDepartureTime}}<span>({{ListOfFlightSegments.DepartureAirField.IATACode}})</span>
                                                            </li>
                                                            <li class="duration">{{ListOfFlightDetails.TotalDurationDisplay}} direct
                                                            </li>
                                                            <li class="arrive">{{ListOfFlightSegments.DisplayArrivalTime}} <span>({{ListOfFlightSegments.ArrivalAirField.IATACode}})</span></li>
                                                        </div>
                                                    </div>

                                                    <div ng-if="ListOfFlightDetails.ListOfFlightSegments.length == 2">
                                                        <div ng-repeat="ListOfFlightSegments in ListOfFlightDetails.ListOfFlightSegments" ng-init="SegmentIndex = $index">
                                                            <div ng-if="SegmentIndex==0">
                                                                <li class="airlines">
                                                                    <ul>
                                                                        <li>
                                                                            <img src="{{ListOfFlightSegments.Carrier.CarrierLogoPath}}" title="{{ListOfFlightSegments.Carrier.CarrierName}}" class="img" width="33" height="33" /></li>
                                                                        <%--<li>{{ListOfFlightSegments.Carrier.CarrierName}}</li>--%>
                                                                    </ul>
                                                                </li>
                                                                <li class="depart">{{ListOfFlightSegments.DisplayDepartureTime}}<span>({{ListOfFlightSegments.DepartureAirField.IATACode}})</span>
                                                                </li>
                                                                <li class="duration">{{ListOfFlightDetails.TotalDurationDisplay}} 1Stop
                                                                </li>
                                                                <li class="arrive">{{ListOfFlightSegments.DisplayArrivalTime}} <span>({{ListOfFlightSegments.ArrivalAirField.IATACode}})</span></li>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div ng-if="ListOfFlightDetails.ListOfFlightSegments.length == 3">
                                                        <div ng-repeat="ListOfFlightSegments in ListOfFlightDetails.ListOfFlightSegments" ng-init="SegmentIndex = $index">
                                                            <div ng-if="SegmentIndex==0">
                                                                <li class="airlines">
                                                                    <ul>
                                                                        <li>
                                                                            <img src="{{ListOfFlightSegments.Carrier.CarrierLogoPath}}" title="{{ListOfFlightSegments.Carrier.CarrierName}}" class="img" width="33" height="33" /></li>
                                                                        <%--<li>{{ListOfFlightSegments.Carrier.CarrierName}}</li>--%>
                                                                    </ul>
                                                                </li>
                                                                <li class="depart">{{ListOfFlightSegments.DisplayDepartureTime}}<span>({{ListOfFlightSegments.DepartureAirField.IATACode}})</span>
                                                                </li>
                                                                <li class="duration">{{ListOfFlightDetails.TotalDurationDisplay}} 2Stop
                                                                </li>
                                                                <li class="arrive">{{ListOfFlightSegments.DisplayArrivalTime}} <span>({{ListOfFlightSegments.ArrivalAirField.IATACode}})</span></li>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>



                                                <li class="points">
                                                    <ul>
                                                        <li class="pnts">{{Digits(x.FareDetails.TotalPoints)}}</li>
                                                        <li>DPoints<br />
                                                            <p ng-if="x.Type=='Return'">
                                                                Return
                                                            </p>
                                                            <p ng-if="x.Type=='One Way'">
                                                                Oneway
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="btn">
                                                    <div class="button" runat="server" id="btnBookNow" ng-click="BookNowClick(x.SequenceNo)">Book a Flight</div>
                                                    <div><a ng-click='getdetails(x.SequenceNo)'>Flight Details</a></div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clr"></div>
                                    <div class="flt-details detseq_{{ x.SequenceNo }}" style="display: none">
                                        <div class="flt-details-H">
                                            <div class="air-logo">Carrier & Flight No.</div>
                                            <div class="stopDtail-1">From</div>
                                            <div class="total-hrs">Duration</div>
                                            <div class="stopDtail-2">To</div>
                                            <div class="clr"></div>
                                        </div>
                                        <div ng-repeat="ListOfFlightDetails in x.ListOfFlightDetails track by $index" ng-init="rowIndex = $index">
                                            <div class="flt-layover" ng-if="rowIndex == 0">
                                                Departure >>
                                                    <span>Total Duration : {{ListOfFlightDetails.TotalDurationDisplay}} </span>
                                            </div>
                                            <div class="flt-layover" ng-if="rowIndex > 0">
                                                Return <<
                                                    <span>Total Duration : {{ListOfFlightDetails.TotalDurationDisplay}} </span>
                                            </div>
                                            <div ng-repeat="ListOfFlightSegments in ListOfFlightDetails.ListOfFlightSegments">

                                                <div class="flt-details-L">
                                                    <div class="flightinfo">
                                                        <div class="air-logo">
                                                            <img class="img" width="33" height="33" src="{{ListOfFlightSegments.Carrier.CarrierLogoPath}}" title="{{ListOfFlightSegments.Carrier.CarrierName}}" />
                                                        </div>
                                                        <div class="flt-code">
                                                            <div class="flt-comp">{{ListOfFlightSegments.Carrier.CarrierName}}</div>
                                                            <div class="flt-carrier">{{ListOfFlightSegments.Carrier.CarrierCode}} ({{ListOfFlightSegments.FlightNo}})</div>
                                                        </div>
                                                        <div class="stopDtail-1">
                                                            <div class="stopCity">
                                                                {{ListOfFlightSegments.DepartureAirField.AirportName}}
                                                    <span class="bold">({{ListOfFlightSegments.DepartureAirField.IATACode}})</span>
                                                            </div>
                                                            <div class="stopCode">
                                                                <div>{{ListOfFlightSegments.DisplayDepartureDate}}  {{ListOfFlightSegments.DisplayDepartureTime}} </div>
                                                            </div>
                                                        </div>
                                                        <div class="total-hrs">{{ListOfFlightSegments.TotalDurationHrs}} hrs {{ListOfFlightSegments.TotalDurationMins}} mins</div>
                                                        <div class="stopDtail-2">
                                                            <div class="stopCity">
                                                                {{ListOfFlightSegments.ArrivalAirField.AirportName}}  
                                        <span class="bold">({{ListOfFlightSegments.ArrivalAirField.IATACode}})</span>
                                                            </div>
                                                            <div class="stopCode">
                                                                <div>{{ListOfFlightSegments.DisplayArrivalDate}}  {{ListOfFlightSegments.DisplayArrivalTime}}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                        <div id="DomesticOneWay" style="display: none;">
                            <div ng-repeat="x in resultsOnwardData" class="Rightpannel_customDivFlight seq_{{ x.SequenceNo }}">
                                <div class="fltlstbx">
                                    <div class="fltinfo fltrtn">
                                        <div ng-if="x.ListOfFlightSegments.length == 1">
                                            <div ng-repeat="ListOfFlightSegments in x.ListOfFlightSegments">
                                                <div class="flt-time">
                                                    <div class="fltclogo">
                                                        <img src="{{ListOfFlightSegments.Carrier.CarrierLogoPath}}" title="{{ListOfFlightSegments.Carrier.CarrierName}}" class="img" width="33" height="33" />
                                                    </div>
                                                    <div class="time">
                                                        <ul>
                                                            <li>{{ListOfFlightSegments.DisplayDepartureTime}}</li>
                                                            <li>
                                                                <img src="images/ico-flttime.png" /></li>
                                                            <li>{{ListOfFlightSegments.DisplayArrivalTime}}</li>
                                                        </ul>
                                                        <div class="fltname">{{ListOfFlightSegments.Carrier.CarrierName}}</div>
                                                    </div>
                                                </div>
                                                <div class="flt-city">
                                                    <ul>
                                                        <li>{{ListOfFlightSegments.DepartureAirField.City}} {{ListOfFlightSegments.DepartureAirField.IATACode}}</li>
                                                        <li>
                                                            <img src="images/ico-flttime.png" /></li>
                                                        <li>{{ListOfFlightSegments.ArrivalAirField.City}} {{ListOfFlightSegments.ArrivalAirField.IATACode}}</li>
                                                    </ul>
                                                    <div class="fltname">Non Stop - {{x.TotalDurationDisplay}}</div>
                                                </div>
                                                <div class="clr"></div>
                                            </div>
                                        </div>
                                        <div ng-if="x.ListOfFlightSegments.length == 2">
                                            <div ng-repeat="ListOfFlightSegments in x.ListOfFlightSegments" ng-init="SegmentIndex = $index">
                                                <div ng-if="SegmentIndex==0">
                                                    <div class="flt-time">
                                                        <div class="fltclogo">
                                                            <img src="{{ListOfFlightSegments.Carrier.CarrierLogoPath}}" title="{{ListOfFlightSegments.Carrier.CarrierName}}" class="img" width="33" height="33" />
                                                        </div>
                                                        <div class="time">
                                                            <ul>
                                                                <li>{{ListOfFlightSegments.DisplayDepartureTime}}</li>
                                                                <li>
                                                                    <img src="images/ico-flttime.png" /></li>
                                                                <li>{{x.ListOfFlightSegments[1].DisplayArrivalTime}}</li>
                                                            </ul>
                                                            <div class="fltname">{{ListOfFlightSegments.Carrier.CarrierName}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flt-city">
                                                        <ul>
                                                            <li>{{ListOfFlightSegments.DepartureAirField.City}} {{ListOfFlightSegments.DepartureAirField.IATACode}}</li>
                                                            <li>
                                                                <img src="images/ico-flttime.png" /></li>
                                                            <li>{{x.ListOfFlightSegments[1].ArrivalAirField.City}} {{x.ListOfFlightSegments[1].ArrivalAirField.IATACode}}</li>
                                                        </ul>
                                                        <div class="fltname">1 Stop - {{x.TotalDurationDisplay}}</div>
                                                    </div>
                                                    <div class="clr"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div ng-if="x.ListOfFlightSegments.length == 3">
                                            <div ng-repeat="ListOfFlightSegments in x.ListOfFlightSegments" ng-init="SegmentIndex = $index">
                                                <div ng-if="SegmentIndex==0">
                                                    <div class="flt-time">
                                                        <div class="fltclogo">
                                                            <img src="{{ListOfFlightSegments.Carrier.CarrierLogoPath}}" title="{{ListOfFlightSegments.Carrier.CarrierName}}" class="img" width="33" height="33" />
                                                        </div>
                                                        <div class="time">
                                                            <ul>
                                                                <li>{{ListOfFlightSegments.DisplayDepartureTime}}</li>
                                                                <li>
                                                                    <img src="images/ico-flttime.png" /></li>
                                                                <li>{{x.ListOfFlightSegments[2].DisplayArrivalTime}}</li>
                                                            </ul>
                                                            <div class="fltname">{{ListOfFlightSegments.Carrier.CarrierName}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flt-city">
                                                        <ul>
                                                            <li>{{ListOfFlightSegments.DepartureAirField.City}} {{ListOfFlightSegments.DepartureAirField.IATACode}}</li>
                                                            <li>
                                                                <img src="images/ico-flttime.png" /></li>
                                                            <li>{{x.ListOfFlightSegments[2].ArrivalAirField.City}} {{x.ListOfFlightSegments[2].ArrivalAirField.IATACode}}</li>
                                                        </ul>
                                                        <div class="fltname">2 Stop - {{x.TotalDurationDisplay}}</div>
                                                    </div>
                                                    <div class="clr"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a ng-click='getdetails(x.SequenceNo)'>See Fight Details
                                        <img src="images/rdmr-ico.png" /></a>
                                    </div>
                                    <div class="pntsbtn">
                                        <h2>{{Digits(x.FareDetails.TotalPoints)}}</h2>
                                        <p>Rayan Rewards</p>
                                        <%--<img class="submit-button" runat="server" id="btnBookNow" src="Images/Btn_BookNow.png" />--%>
                                        <div class="button" runat="server" ng-click="BookDomesticOneWay(x.SequenceNo)">Book a Flight</div>
                                        <div class="clr"></div>
                                    </div>
                                    <div class="clr"></div>
                                    <div class="flt-details detseq_{{ x.SequenceNo }}" style="display: none">
                                        <div class="flt-details-H">
                                            <div class="air-logo">Carrier & Flight No.</div>
                                            <div class="stopDtail-1">From</div>
                                            <div class="total-hrs">Duration</div>
                                            <div class="stopDtail-2">To</div>
                                            <div class="clr"></div>
                                        </div>
                                        <div ng-repeat="ListOfFlightSegments in x.ListOfFlightSegments">
                                            <div class="flt-layover">
                                                Departure >>
                                            <span>Total Duration : {{x.TotalDurationDisplay}} </span>
                                            </div>
                                            <div class="flt-details-L">
                                                <div class="flightinfo">
                                                    <div class="air-logo">
                                                        <img class="img" width="33" height="33" src="{{ListOfFlightSegments.Carrier.CarrierLogoPath}}" title="{{ListOfFlightSegments.Carrier.CarrierName}}" />
                                                    </div>
                                                    <div class="flt-code">
                                                        <div class="flt-comp">{{ListOfFlightSegments.Carrier.CarrierName}}</div>
                                                        <div class="flt-carrier">{{ListOfFlightSegments.Carrier.CarrierCode}}</div>
                                                    </div>
                                                    <div class="stopDtail-1">
                                                        <div class="stopCity">
                                                            {{ListOfFlightSegments.DepartureAirField.AirportName}}
                                                    <span class="bold">({{ListOfFlightSegments.DepartureAirField.IATACode}})</span>
                                                        </div>
                                                        <div class="stopCode">
                                                            <div>{{ListOfFlightSegments.DisplayDepartureDate}}  {{ListOfFlightSegments.DisplayDepartureTime}} </div>
                                                        </div>
                                                    </div>
                                                    <div class="total-hrs">{{ListOfFlightSegments.TotalDurationHrs}} hrs {{ListOfFlightSegments.TotalDurationMins}} mins</div>
                                                    <div class="stopDtail-2">
                                                        <div class="stopCity">
                                                            {{ListOfFlightSegments.ArrivalAirField.AirportName}}  
                                        <span class="bold">({{ListOfFlightSegments.ArrivalAirField.IATACode}})</span>
                                                        </div>
                                                        <div class="stopCode">
                                                            <div>{{ListOfFlightSegments.DisplayArrivalDate}}  {{ListOfFlightSegments.DisplayArrivalTime}}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                        <br class="clr" />
                        <div id="DomesticTwoWay" style="display: none;">
                            <div>
                                <div class="dosonwy">
                                    <h3>One Way</h3>
                                    <div ng-repeat="x in resultsOnwardData" class="seq_{{ x.SequenceNo }}">
                                        <div class="fltlstbx">
                                            <div>
                                                <div>
                                                    <div>
                                                        <div class="fltclogo">
                                                            <ul>
                                                                <li>
                                                                    <input type="radio" ng-change="showTripSummaryOnward(x.SequenceNo)" name="domonward" ng-model="OnwardDom" /></li>
                                                                <li>
                                                                    <img src="{{x.ListOfFlightSegments[0].Carrier.CarrierLogoPath}}" title="{{x.ListOfFlightSegments[0].Carrier.CarrierName}}" class="img" width="33" height="33" /></li>
                                                                <li>{{x.ListOfFlightSegments[0].Carrier.CarrierName}}<br />
                                                                    ({{x.ListOfFlightSegments[0].Carrier.CarrierCode}}) </li>
                                                            </ul>
                                                        </div>
                                                        <div class="pntsbtn">
                                                            <h2>{{Digits(x.FareDetails.TotalPoints)}}</h2>
                                                            <p>Rayan Rewards</p>

                                                        </div>
                                                        <div class="clr"></div>
                                                    </div>
                                                </div>
                                                <hr />
                                                <div class="domsdtls" ng-repeat="ListOfFlightSegments in x.ListOfFlightSegments">
                                                    <ul>
                                                        <li class="domone">
                                                            <div>
                                                                <b>{{ListOfFlightSegments.DisplayDepartureDate}},</b> {{ListOfFlightSegments.DisplayDepartureTime}}
                                                            </div>
                                                            <div>{{ListOfFlightSegments.DepartureAirField.City}} {{ListOfFlightSegments.DepartureAirField.IATACode}}</div>
                                                        </li>
                                                        <li>
                                                            <img src="images/ico-flttime.png" /></li>
                                                        <li class="domrtn">
                                                            <div><b>{{ListOfFlightSegments.DisplayArrivalDate}},</b>  {{ListOfFlightSegments.DisplayArrivalTime}}</div>
                                                            <div>{{ListOfFlightSegments.ArrivalAirField.City}} {{ListOfFlightSegments.ArrivalAirField.IATACode}}</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <hr />
                                                <div class="domstps">{{x.TotalDurationDisplay}}</div>
                                                <div class="clr"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="dosrtn">
                                    <h3>Return Way</h3>
                                    <div ng-repeat="x in resultsReturnData" class="seq_{{ x.SequenceNo }}">
                                        <div class="fltlstbx">
                                            <div>
                                                <div>
                                                    <div>
                                                        <div class="fltclogo">
                                                            <ul>
                                                                <li>
                                                                    <input type="radio" ng-change="showTripSummaryReturn(x.SequenceNo)" name="domreturn" ng-model="ReturnDom" /></li>
                                                                <li>
                                                                    <img src="{{x.ListOfFlightSegments[0].Carrier.CarrierLogoPath}}" title="{{x.ListOfFlightSegments[0].Carrier.CarrierName}}" class="img" width="33" height="33" /></li>
                                                                <li>{{x.ListOfFlightSegments[0].Carrier.CarrierName}}<br />
                                                                    ({{x.ListOfFlightSegments[0].Carrier.CarrierCode}}) </li>
                                                            </ul>
                                                        </div>
                                                        <div class="pntsbtn">
                                                            <h2>{{Digits(x.FareDetails.TotalPoints)}}</h2>
                                                            <p>Rayan Rewards</p>

                                                        </div>
                                                        <div class="clr"></div>
                                                    </div>
                                                </div>
                                                <hr />
                                                <div class="domsdtls" ng-repeat="ListOfFlightSegments in x.ListOfFlightSegments">
                                                    <ul>
                                                        <li class="domone">
                                                            <div>
                                                                <b>{{ListOfFlightSegments.DisplayDepartureDate}},</b> {{ListOfFlightSegments.DisplayDepartureTime}}
                                                            </div>
                                                            <div>{{ListOfFlightSegments.DepartureAirField.City}} {{ListOfFlightSegments.DepartureAirField.IATACode}}</div>
                                                        </li>
                                                        <li>
                                                            <img src="images/ico-flttime.png" /></li>
                                                        <li class="domrtn">
                                                            <div><b>{{ListOfFlightSegments.DisplayArrivalDate}},</b>  {{ListOfFlightSegments.DisplayArrivalTime}}</div>
                                                            <div>{{ListOfFlightSegments.ArrivalAirField.City}} {{ListOfFlightSegments.ArrivalAirField.IATACode}}</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <hr />
                                                <div class="domstps">{{x.TotalDurationDisplay}}</div>
                                                <div class="clr"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clr"></div>

                            </div>
                        </div>

                        <input type="hidden" id="hdnOnwardSelectedFlight" />
                        <input type="hidden" id="hdnReturnSelectedFlight" />
                        <div id="LoadingResult" class="Wait_bg" ng-hide="hideloadresult">
                            <b>
                                <img src="Images/Loading.GIF" alt="Loading" /></b>
                        </div>
                        <div class="SummaryInfo_div" id="LoadNext" style="text-align: center; display: none; cursor: pointer;" ng-click="LoadNext();">
                            <asp:Label ID="Label1" runat="server" CssClass="SummaryLabel" Text="Show more flights"></asp:Label>
                        </div>
                    </div>
                    <div class="pgcol2" id="divSearch">
                        <div class="widget-prfl" id="UserInfo">
                            <div class="widget-user">
                                <ul id="UserDetails">
                                </ul>
                            </div>
                            <div id="Content_Wrapper">
                                <div class="widget-srhpnl">
                                    <h3 ng-click="showModifyFlight()">Modify Search</h3>
                                    <div id="modifyDetails" style="display: none">
                                        <div class="onround">
                                            <span class="radiobtn" id="oneway" ng-click="showOneway()">One-Way</span>
                                            <span class="radiobtn rdbselect" id="return" ng-click="showReturn()">Return</span>
                                            <input type="hidden" id="hdntrip" value="true" />
                                        </div>
                                        <div class="widget-flds">
                                            <ul>

                                                <li class="autogen">
                                                    <label>From</label><input class="space1" type="text" ng-click='empty("textBoxFrom")' id="textBoxFrom" runat="server" ng-model="Fromcountry" typeahead-min-length='3' uib-typeahead="countryFrom for countryFrom in getLocation($viewValue)" /></li>
                                                <li class="autogen">
                                                    <label>To</label><input class="space1" type="text" runat="server" ng-click='empty("textBoxTo")' id="textBoxTo" ng-model="Tocountry" typeahead-min-length='3' uib-typeahead="countryTo for countryTo in getLocation($viewValue)" /></li>
                                                <li class="dates">
                                                    <ul>
                                                        <li>
                                                            <label>Depart</label><input class="space2 input2 calender hasDatepicker" type="text" id="txtDepart" runat="server" /></li>
                                                        <li class="mrno" id="retnli">
                                                            <label>Reurn</label><input class="space2  calender" type="text" id="txtReturn" runat="server" /></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <label>Class</label>
                                                    <asp:DropDownList CssClass="space3" ID="dropDownListEconomy" runat="server">
                                                        <asp:ListItem Selected="True">Economy</asp:ListItem>
                                                        <asp:ListItem>Business</asp:ListItem>
                                                        <asp:ListItem>First</asp:ListItem>
                                                    </asp:DropDownList>

                                                </li>
                                                <li class="autogen">
                                                    <label>Airlines</label><input class="space1" id="txtAirline" ng-click='empty("txtAirline")' runat="server" type="text" ng-model="GetAllAirlines" uib-typeahead="Carrier.name for Carrier in getAllAirlines($viewValue)" typeahead-min-length='3' placeholder="Enter Airlines" typeahead-on-select="onSelectAirlines($item, $model, $label)" />
                                                    <asp:HiddenField runat="server" ID="hdnCarrier" />
                                                </li>

                                                <li class="passgnr">
                                                    <ul>
                                                        <li>
                                                            <label>Adult</label>
                                                            <asp:DropDownList ID="DropDownListAdult" runat="server" CssClass="space2">
                                                                <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </li>
                                                        <li>
                                                            <label>Child</label>
                                                            <asp:DropDownList ID="DropDownListChild" runat="server" CssClass="space2">
                                                                <asp:ListItem Value="0" Selected="True">0</asp:ListItem>
                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </li>
                                                        <li class="mrno">
                                                            <label>Infant</label>
                                                            <asp:DropDownList ID="DropDownListInfant" runat="server" CssClass="space2">
                                                                <asp:ListItem Value="0" Selected="True">0</asp:ListItem>
                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="requiredValidation"></div>
                                        <div class="button" ng-click='FlightValidation()'>Search</div>
                                        <div class="clr"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rfnsrch" id="fliterDetails">
                            <div class="rfnhdr">
                                <h3>Refine your Search</h3>
                            </div>
                            <div class="stops">
                                <h3>Stops</h3>
                                <ul>
                                    <div class="divStops"></div>
                                </ul>
                            </div>
                            <div class="tldurtn">
                                <h3>Total Points</h3>
                                <div range-slider min="priceSlider.range.min" max="priceSlider.range.max" model-min="priceSlider.minPrice" model-max="priceSlider.maxPrice" step="1" on-handle-up="priceSliderStop()"></div>
                            </div>
                            <div class="rwdpnts">
                                <h3>
                                    <asp:Label ID="lblAirlines" runat="server" Text="Airlines"></asp:Label></h3>
                                <ul id="DivAirLinesList">
                                </ul>
                            </div>


                            <div class="tldurtn">
                                <h3>Total Duration</h3>
                                <div range-slider min="durationSlider.range.min" max="durationSlider.range.max" model-min="durationSlider.minduration" model-max="durationSlider.maxduration" step="15" on-handle-up="durationSliderStop()" filter="hourMinFilter"></div>
                            </div>
                            <div class="deptm">
                                <h3>Departure Time</h3>
                                <div range-slider min="departureSlider.range.min" max="departureSlider.range.max" model-min="departureSlider.minduration" model-max="departureSlider.maxduration" step="15" on-handle-up="departureSliderStop()" filter="hourMinFilter"></div>
                            </div>
                            <div class="arrtm">
                                <h3>Arrival Time</h3>
                                <div range-slider min="arrivalSlider.range.min" max="arrivalSlider.range.max" model-min="arrivalSlider.minduration" model-max="arrivalSlider.maxduration" step="15" on-handle-up="arrivalSliderStop()" filter="hourMinFilter"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                    <div id="Trip_Summary_Main" style="display: none" class="fixedBot-tripSumm">
                        <div class="trpsum-flthdr robot">
                            <div class="trpsum-flthdr-L">
                                <div id="divOnwardFlightDetails">
                                </div>
                            </div>
                            <div class="trpsum-flthdr-R">
                                <div id="divReturnFlightDetails">
                                </div>
                            </div>
                            <div class="trpsum-flthdr-Miles">
                                <div id="divTotal" style="width: 100%; float: left;">
                                </div>
                                <br />
                                <div id="ViewDetails" style="text-decoration: underline; cursor: pointer; color: #fff; font-size: 12px;"
                                    onclick="return toggleInfo();">
                                    View Details
                                </div>
                            </div>
                        </div>
                        <div class="trp-fltsumm-info fL" style="display: none" id="DomflightInfo">
                            <div class="trpsum-flt-oneway">
                                <div id="divOnwardFlightInfo">
                                </div>
                            </div>
                            <div class="trpsum-flt-twoway">
                                <div id="divReturnFlightInfo">
                                </div>
                            </div>
                            <div class="trpsum-btnBook">
                                <ul class="fR">
                                    <li>
                                        <div class="btn" style="width: 100px;">
                                            <a href="#" onclick="var retvalue = BookDomestic(); event.returnValue= retvalue;event.preventDefault(); return retvalue;">
                                                <button class="btn">
                                                    Book Now</button></a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </div>
    <%--</div>--%>
    <%--<div id="divUp" class="DirectionDiv">
        <a href="#">
            <img border="0" src="Images/ScrollToTop.png" />
        </a>
    </div>--%>
    <%--</div>--%>
    <div id="LoadTemplate" runat="server">
    </div>

</asp:Content>
