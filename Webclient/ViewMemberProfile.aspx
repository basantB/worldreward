﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="ViewMemberProfile.aspx.cs" Inherits="ViewMemberProfile" %>

<%@ Register Src="~/UserControl/ActivationMenu.ascx" TagName="ActivationMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <script src="js/Validation.js" type="text/javascript"></script>
    <link href="css/myAccount.css" rel="stylesheet" />
    <script>
		/*tabier scripts*/
		$(document).ready(function() {
			$("#content aside").hide(); // Initially hide all content
			$("#tabs li:first").attr("id","current"); // Activate first tab
			$("#content aside:first").fadeIn(); // Show first tab content
			
			$('#tabs a').click(function(e) {
				e.preventDefault();        
				$("#content aside").hide(); //Hide all content
				$("#tabs li").attr("id",""); //Reset id's
				$(this).parent().attr("id","current"); // Activate this
				$('#' + $(this).attr('title')).fadeIn(); // Show content for current tab
			});
		});
</script>
<div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>
    <!--Article-->
      <article id="article">
         <div class="wrap">
            <h1>Experience Abundance with WorldRewardZ</h1>
            <p>Below is a categorized listing of your activities at WorldRewardZ. As you glance through them, you can immerse in the joy of having the best choices in the world available to you simply at a click, with WorldRewardZ. Go ahead and multiply your unique experiences.</p>
			<div class="ManyWrap MyAccount">
            <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
            <ul id="tabs">
			    <li id="current"><a title="tab1" href="#">PERSONAL DETAILS</a></li>
			    <li id=""><a title="tab2" href="#">CHANGE PASSWORD</a></li>
		    </ul>
             <div id="content">
              <aside id="tab1" style="display: block;">
					<div class="leftCont">

					<div class="persDetail">
						<ul>
							<li>
								<label>Name</label>
								<asp:TextBox runat="server" ID="labelMemberNameValue"></asp:TextBox>
							</li>
							<li>
								<label>Mobile No.</label>
								<a href="#" class="icnEdit"></a>
								<asp:TextBox ID="labelMobileNo" runat="server"></asp:TextBox>
							</li>
							<li>
								<label>Email ID</label>
								<a href="#" class="icnEdit"></a>
								
                                <asp:TextBox ID="labelEmailValue" runat="server"></asp:TextBox>
							</li>
							<li>
								<label>Address</label>
								<a href="#" class="icnEdit"></a>
								<asp:TextBox ID="labelAddressValue" runat="server"></asp:TextBox>
							</li>
					    </ul>
					</div>
						
					</div>
              </aside>

                 <aside id="tab2">
	              <div class="leftCont">
					<div class="persDetail">
						<ul>
							<li>
								<label>Old Password</label>
								<asp:TextBox ID="txtOldPassword" MaxLength="20" runat="server"  TextMode="Password" oncopy="return false" onpaste="return false"
                                               style="margin-bottom:0; margin-top:10px;" oncut="return false" onkeypress="var retValue = ValidateOnEnter(event); event.returnValue = retValue; return retValue;"></asp:TextBox>
							</li>
							<li>
								<label>New Password</label>
								<asp:TextBox ID="txtPassword" MaxLength="20" runat="server" TextMode="Password" oncopy="return false" onpaste="return false"
                                               style="margin-bottom:0; margin-top:10px;"  oncut="return false" onkeypress="var retValue = ValidateOnEnter(event); event.returnValue = retValue; return retValue;"></asp:TextBox>
							</li>
							<li>
								<label>Re-Type Password</label>
								<asp:TextBox ID="txtNewPassword" MaxLength="20" runat="server" oncopy="return false"
                                                style="margin-bottom:0; margin-top:10px;"  onpaste="return false" oncut="return false" TextMode="Password" onkeypress="var retValue = ValidateOnEnter(event); event.returnValue = retValue; return retValue;"></asp:TextBox>
							</li>
							
							<li>
								<label>&nbsp;</label>
								<%--<a href="#" class="btnRed fl" runat="server" ID="Button1" OnClientClick="var retValue = ChangePassword(); event.returnValue = retValue;return retValue;" style="margin-left:0px;">Save</a>--%>
                                <span class="save_btnprofile">
                                <asp:Button runat="server" ID="Button1" OnClientClick="var retValue = ChangePassword(); event.returnValue = retValue;return retValue;" CssClass="button btnRed fl" Text="Save" />
                                    </span>
							</li>
                            <li>
                                <label>&nbsp;</label>
                            <asp:Label runat="server" ID="lblSuccessOrFailure" Style="color: #ff0000; font-size: 13px;float: left; margin-top: 1.2vw" />
                            <div id="ChangePasswordValidation" style="color: #ff0000; font-size: 13px; float: left;">
                            </div>
                           </li>
						</ul>
					</div>
					</div>
					<div class="rightCont">
						<div class="noteBlk">
							<h2>Password Policy:</h2>
							<ul>
						    <li>Minimum 8 characters in length.
                            </li><li>Should contain at least one capital case character.</li>
                            <li>Should contain at least one small case character.</li>
                            <li>Should contain at least one special character (!@#$%0?).</li>
                            <li>Should contain at least one numeric digit.</li>
							</ul>
						</div>
					</div>
             </aside>
              </div>
          </div>
        </div>
      </article>
    <!--End of Article-->
    <%--<div class="inr-wrap">
    <section class="myaccContent">
      <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
      <div class="myacc-conthdr">
                 <div class="adCntnr">
                    <!-- blk start-->
                    <div class="acco2">
                        <div class="myacc-statmntdtl expand openAd" style="background:#666 ; color:#fff; font-weight:bold;"> Contact Details</div>
                        <div class="summary-txt collapse">
                            <table border="0" cellpadding="5" style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td class="vmid"><div class="td-block-left">
                                            Name
                                            </div>
                                        </td>
                                        <td class="row-earn"><div class="td-block-rght"> 
                                            <asp:Label runat="server" ID="labelMemberNameValue"></asp:Label>
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="vmid"><div class="td-block-left">
                                            Mobile No.
                                            </div>
                                        </td>
                                        <td class="row-earn"><div class="td-block-rght"> 
                                            <asp:Label ID="labelMobileNo" runat="server"></asp:Label>
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="vmid"><div class="td-block-left">
                                            Email ID
                                            </div>
                                        </td>
                                         <td class="row-earn"><div class="td-block-rght"> 
                                            <asp:Label ID="labelEmailValue" runat="server"></asp:Label>
                                            </div>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                         <td class="vmid"><div class="td-block-left">
                                            Address
                                            </div>
                                        </td>
                                       <td class="row-earn"><div class="td-block-rght"> 
                                            <asp:Label ID="labelAddressValue" runat="server"></asp:Label>
                                            </div>
                                        </td>
                                        
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="myacc-statmntdtl expand" style="background:#666 ; color:#fff; font-weight:bold;"> Change Password</div>
                        <div class="summary-txt collapse">
                            <table border="0" cellpadding="0" style="width: 100%;border: 1px solid #ccc;" id="tblChangePwd">
                                <tbody>
                                    <tr style="height:50px">
                                       <td class="vmid"><div class="td-block-left">
                                            Current Password
                                            </div>
                                        </td>
                                         <td class="row-earn"><div class="td-block-rght"> 
                                            <asp:TextBox ID="txtOldPassword" MaxLength="20" runat="server"  TextMode="Password" oncopy="return false" onpaste="return false"
                                               style="margin-bottom:0; margin-top:10px;" oncut="return false" onkeypress="var retValue = ValidateOnEnter(event); event.returnValue = retValue; return retValue;"></asp:TextBox>
                                        </div>
                                        </td>
                                        <td width="2%">
                                            &nbsp;&nbsp;
                                        </td>
                                        <td rowspan="3" style="color: #444444; font-size: 12px; line-height: 1.5vw">
                                            <div class="adjust">
                                                Password Policy:<ul>
                                                    <li class="arrowlist2">Minimum 8 characters in length.</li>
                                                    <li class="arrowlist2">Should contain at least one capital case character.</li>
                                                    <li class="arrowlist2">Should contain at least one small case character.</li>
                                                    <li class="arrowlist2">Should contain at least one special character (!@#$%0?).</li>
                                                    <li class="arrowlist2">Should contain at least one numeric digit.</li></ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="height:50px">
                                        <td class="vmid"><div class="td-block-left">
                                            New Password
                                            </div>
                                        </td>
                                       <td class="row-earn"><div class="td-block-rght"> 
                                            <asp:TextBox ID="txtPassword" MaxLength="20" runat="server" TextMode="Password" oncopy="return false" onpaste="return false"
                                               style="margin-bottom:0; margin-top:10px;"  oncut="return false" onkeypress="var retValue = ValidateOnEnter(event); event.returnValue = retValue; return retValue;"></asp:TextBox>
                                        </div>
                                        </td>
                                        <td width="1%">
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr style="height:50px">
                                        <td class="vmid"><div class="td-block-left">
                                            Confirm Password
                                            </div>
                                        </td>
                                         <td class="row-earn"><div class="td-block-rght"> 
                                            <asp:TextBox ID="txtNewPassword" MaxLength="20" runat="server" oncopy="return false"
                                                style="margin-bottom:0; margin-top:10px;"  onpaste="return false" oncut="return false" TextMode="Password" onkeypress="var retValue = ValidateOnEnter(event); event.returnValue = retValue; return retValue;"></asp:TextBox>
                                        </div>
                                        </td>
                                        <td width="1%">
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td colspan="2">
                                        <asp:Button runat="server" ID="Button1" OnClientClick="var retValue = ChangePassword(); event.returnValue = retValue;return retValue;" CssClass="button" Text="Save" />
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td colspan="3">
                                            <asp:Label runat="server" ID="lblSuccessOrFailure" Style="color: #ff0000; font-size: 13px;
                                                float: left; margin-top: 1.2vw" />
                                            <div id="ChangePasswordValidation" style="color: #ff0000; font-size: 13px; float: left;">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- blk end-->
                </div>
                <br />
                
            </div>
     </section>
        <div class="clr"></div>
     </div>--%>
</asp:Content>
