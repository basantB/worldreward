﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintCarVoucher.aspx.cs"
    Inherits="PrintCarVoucher" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Car Voucher</title>
    <link href="Css/main.css" rel="stylesheet" type="text/css" />
    <link href="Css/Car.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        window.print();
    </script>
</head>
<body style="margin: 0 auto; width: 768px">
    <form id="form1" runat="server">
        <div class="revncofm">
            <div class="wrapper">
                <div class="topStrip">
                    <a href="index.html" class="logo">
                        <img src="Images/VoucherHead.jpg" /></a>
                    <!--referece no Starts-->
                    
                    <div class="clr">
                    </div>
                    <!--referece no Ends-->
                </div>
                <!--Infi Destination Starts-->
                <div class="body-bdr">
                    <p>
                        Thank you
                        <asp:Label ID="lblMemberName" runat="server" Text="NIL"> </asp:Label>
                        (Member ID :
                        <asp:Label ID="lblMembershipReference" runat="server" Text="NIL"></asp:Label>) for
                        using Danamon Rewards Points to book your car. Please use your Reference ID for any communication
                        pertaining to this booking.
                    </p>
                    <p>
                        Your Current booking status is Car <strong>
                            <asp:Label ID="lblBookingStatus" runat="server" Text="NIL"></asp:Label>*.</strong>
                    </p>
                    <p>
                        Supplier ID : <strong>
                            <asp:Label ID="lblbookingId" runat="server" Text="NIL"></asp:Label></strong>
                    </p>

                    <div class="reference">
                        <div class="col1">
                            <div>
                                Reference No.
                            </div>
                            <asp:Label ID="lblbookingReferenceNo" runat="server" Text="NIL"></asp:Label>
                        </div>
                        <div class="col2">
                            <div id="PaymentInfo" runat="server">
                            </div>
                        </div>
                    </div>

                    <div class="pass-details">
                        
                        <div class="w50P">
                            <h3>Booking Details</h3>
                            <table class="commonTable" cellspacing="0" cellpadding="1">
                                <tr>
                                    <th>Pick up
                                    </th>
                                    <td>
                                        <asp:Label ID="lblpickUp" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Pick up date
                                    </th>
                                    <td>
                                        <asp:Label ID="lblpickUpDate" runat="server" Text="NIL"></asp:Label>
                                        at
                                    <asp:Label ID="lblPickUpTime" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="w50P">
                            <h3>&nbsp;</h3>
                            <table class="commonTable" cellspacing="0" cellpadding="1">
                                <tr>
                                    <th>Drop Off
                                    </th>
                                    <td>
                                        <asp:Label ID="lbldropOff" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Drop off date
                                    </th>
                                    <td>
                                        <asp:Label ID="lbldropOffDate" runat="server" Text="NIL"></asp:Label>
                                        at
                                    <asp:Label ID="lbldropOffTime" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="w50P">
                            <h3>Car Details</h3>
                            <table class="commonTable" cellspacing="0" cellpadding="1">
                                <tr>
                                    <th>Car Name
                                    </th>
                                    <td>
                                        <asp:Label ID="lblcarName" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Transmission type
                                    </th>
                                    <td>
                                        <asp:Label ID="lblTransmissionType" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Air Condition
                                    </th>
                                    <td>
                                        <asp:Label ID="lblAirCondition" runat="server" Text="No"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="w50P">
                            <h3 class="CarHeading">Driver Details</h3>
                            <table class="commonTable" cellspacing="0" cellpadding="1">
                                <tr>
                                    <th>Name
                                    </th>
                                    <td>
                                        <asp:Label ID="lblDriverTitle" runat="server" Text="NIL"></asp:Label>
                                        <asp:Label ID="lblDriverFName" runat="server" Text="NIL"></asp:Label>
                                        <asp:Label ID="lblDriverLName" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Address
                                    </th>
                                    <td>
                                        <asp:Label ID="lblAddress" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Phone Number
                                    </th>
                                    <td>
                                        <asp:Label ID="lblPhoneNo" runat="server" Text="No"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="w50P">
                            <h3 class="CarHeading">Pick Up location details</h3>
                            <table class="commonTable" cellspacing="0" cellpadding="1">
                                <tr>
                                    <th>Pick up
                                    </th>
                                    <td>
                                        <asp:Label ID="lblPickupPlace" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>City
                                    </th>
                                    <td>
                                        <asp:Label ID="lblCityName" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Country
                                    </th>
                                    <td>
                                        <asp:Label ID="lblCountry" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="w50P">
                            <h3 class="CarHeading">Drop off location details</h3>
                            <table class="commonTable" cellspacing="0" cellpadding="1">
                                <tr>
                                    <th>Drop off
                                    </th>
                                    <td>
                                        <asp:Label ID="lblDropOffLoc" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>City
                                    </th>
                                    <td>
                                        <asp:Label ID="lblDropOffCity" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Country
                                    </th>
                                    <td>
                                        <asp:Label ID="lblDropOffCon" runat="server" Text="NIL"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clr"></div>
                        <p>
                            <ul>
                                <li>In case if booking status is not yet confirmed you will revieve confirmation message
                                in next 24hrs over email. Else all the points will be given back to your account</li>
                                <li>We thank you for using the Danamon Bank redemption service and wish you a safe journey</li>
                            </ul>
                        </p>
                    </div>
                </div>
                <!--Infi Destination Ends-->
            </div>
        </div>
    </form>
</body>
</html>
