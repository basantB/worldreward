﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdultPassangerDetails.ascx.cs"
    Inherits="SourceControl_AdultPassangerDetails" %>
<script type="text/javascript">
    $(document).ready(function () {


        $("#<%=txtDOB.ClientID%>").datepicker({
            numberOfMonths: 1,
            changeMonth: true,
            changeYear: true,
            //showButtonPanel: true,
            yearRange: "-90:-0",
            dateFormat: 'dd/mm/yy',
            maxDate: '-12Y',
            onSelect: function (dateText, inst) {
                $("#<%=txtDOB.ClientID%>").text(""); $("#<%=txtDOB.ClientID%>").text(dateText);
                return false;
            }
        });


        $("#<%=txtEffectiveDate.ClientID%>").datepicker({
            numberOfMonths: 1,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            yearRange: "-90:-0",
            dateFormat: 'dd/mm/yy',
            maxDate: new Date,
            onSelect: function (dateText, inst) {
                $("#<%=txtEffectiveDate.ClientID%>").text(""); $("#<%=txtEffectiveDate.ClientID%>").text(dateText); return false;
            }
        });

        $("#<%=txtExpiryDate.ClientID%>").datepicker({
            numberOfMonths: 1,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            yearRange: ":+20",
            dateFormat: 'dd/mm/yy',
            minDate: new Date,
            onSelect: function (dateText, inst) {
                $("#<%=txtExpiryDate.ClientID%>").text(""); $("#<%=txtExpiryDate.ClientID%>").text(dateText); return false;
            }
        });
    });   
</script>

<div class="title">
    <label>
        Title</label>
        <div class="selectdiv">
        <asp:DropDownList ID="ddlTitle" runat="server">
            <asp:ListItem Value="1" Selected="True">Title</asp:ListItem>
            <asp:ListItem Value="Male">Mr</asp:ListItem>
            <asp:ListItem Value="Female">Ms</asp:ListItem>
            <asp:ListItem Value="Female">Mrs</asp:ListItem>
        </asp:DropDownList>
      </div>
    <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="ddlTitle"
        Display="Dynamic" ErrorMessage="Enter Title" ValidationGroup="WebValidation"
        CssClass="rptErrorMassage" InitialValue="1"></asp:RequiredFieldValidator>&nbsp;
</div>
<div class="name">
    <label>
        First Name</label>
    <asp:TextBox ID="txtFirstName" runat="server" Text="" AutoComplete="off"></asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvAdultFirstName" runat="server" ControlToValidate="txtFirstName"
        Display="Dynamic" ErrorMessage="Enter First Name" ValidationGroup="WebValidation"
        CssClass="rptErrorMassage"></asp:RequiredFieldValidator>&nbsp;
</div>
<div class="name">
    <label>
        Last Name</label>
    <asp:TextBox ID="txtLastName" runat="server" AutoComplete="off"></asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvAdultLastName" runat="server" ControlToValidate="txtLastName"
        Display="Dynamic" ErrorMessage="Enter Last Name" ValidationGroup="WebValidation"
        CssClass="rptErrorMassage"></asp:RequiredFieldValidator>&nbsp;
</div>
<div class="clr">
</div>
<div class="other">
    <label>
        Date of Birth</label>
    <asp:TextBox ID="txtDOB" runat="server" AutoComplete="off"></asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvAdultDOB" runat="server" ControlToValidate="txtDOB"
        Display="Dynamic" ErrorMessage="Enter Date of Birth" ValidationGroup="WebValidation"
        CssClass="rptErrorMassage"></asp:RequiredFieldValidator>&nbsp;
    <asp:CustomValidator ID="CustomValidatorAdultDOB" runat="server" ErrorMessage="Adults (12+ yrs)"
        Display="Dynamic" ValidationGroup="WebValidation" OnServerValidate="IssueAdultDateValidator"
        ControlToValidate="txtDOB" CssClass="rptErrorMassage">&nbsp;
    </asp:CustomValidator>
</div>
<div class="other">
    <label>
        Passport Number</label>
    <asp:TextBox ID="txtPassportNo" runat="server" AutoComplete="off"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPassportNo"
        ErrorMessage="Enter Passport Number" ValidationGroup="WebValidation" CssClass="rptErrorMassage"></asp:RequiredFieldValidator>&nbsp;
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtPassportNo"
        Display="Dynamic" ValidationGroup="WebValidation" CssClass="rptErrorMassage"
        ValidationExpression="^[a-zA-Z0-9]*$" ErrorMessage="Please Enter correct passport no. (no blank space)."></asp:RegularExpressionValidator>&nbsp;
</div>
<div class="other">
    <label>
        Email ID</label>
    <asp:TextBox ID="txtEmailID" runat="server" AutoComplete="off"></asp:TextBox>
    <asp:RequiredFieldValidator ValidationGroup="WebValidation" ID="RequiredFieldValidator2"
        runat="server" ControlToValidate="txtEmailID" Display="Dynamic" ErrorMessage="Enter Email ID"
        CssClass="rptErrorMassage"></asp:RequiredFieldValidator>&nbsp;
    <asp:RegularExpressionValidator ID="revAdultEmail" Display="Dynamic" runat="server"
        ControlToValidate="txtEmailID" ValidationGroup="WebValidation" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
        CssClass="rptErrorMassage" ErrorMessage="Enter Valid Email ID">
    </asp:RegularExpressionValidator>&nbsp;
</div>
<div class="clr">
</div>
<div id="AdditionalInfo" runat="server">
    <div class="title" id="divNationality" runat="server">
        <label>
            Nationality</label>
        <div class="selectdiv">
            <asp:DropDownList ID="drpNationality" runat="server">
                <asp:ListItem Text="UAE" Selected="true" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Afghanistan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Albania" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Algeria" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="American Samoa" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Andorra" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Angola" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Anguilla" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Antarctica" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Antigua and Barbuda" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Argentina" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Armenia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Aruba" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Ascension Island" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Australia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Austria" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Azerbaijan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Bahamas" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Bahrain" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Bangladesh" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Barbados" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Belarus" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Belgium" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Belize" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Benin" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Bermuda" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Bhutan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Bolivia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Bosnia and Herzegovina" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Botswana" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Bouvet Island" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Brazil" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="British Indian Ocean Territory" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Brunei Darussalam" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Bulgaria" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Burkina Faso" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Burundi" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Cambodia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Cameroon" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Canada" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Cape Verde" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Cayman Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Central African Republic" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Chad" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Chile" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="China" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Christmas Island" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Cocos (Keeling) Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Colombia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Comoros" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Congo, Democratic Republic of the" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Congo, Republic of" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Cook Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Costa Rica" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Cote dIvoire" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Croatia/Hrvatska" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Cuba" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Curacao" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Cyprus" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Czech Republic" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Denmark" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Djibouti" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Dominica" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Dominican Republic" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="East Timor" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Ecuador" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Egypt" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="El Salvador" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Equatorial Guinea" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Eritrea" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Estonia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Ethiopia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Falkland Islands (Malvina)" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Faroe Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Fiji" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Finland" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="France" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="French Guiana" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="French Polynesia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="French Southern Territories" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Gabon" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Gambia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Georgia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Germany" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Ghana" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Gibraltar" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Greece" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Greenland" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Grenada" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Guadeloupe" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Guam" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Guatemala" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Guernsey" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Guinea" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Guinea-Bissau" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Guyana" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Haiti" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Heard and McDonald Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Holy See (City Vatican State)" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Honduras" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Hong Kong" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Hungary" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Iceland" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="India" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Indonesia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Iran" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Iraq" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Ireland" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Isle of Man" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Israel" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Italy" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Jamaica" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Japan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Jersey" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Jordan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Kazakhstan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Kenya" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Kiribati" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Korea, Democratic Peoples Republic" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Kuwait" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Kyrgyzstan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Lao Peoples Democratic Republic" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Latvia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Lebanon" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Lesotho" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Liberia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Libya" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Liechtenstein" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Lithuania" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Luxembourg" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Macau" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Macedonia, Former Yugoslav Republic" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Madagascar" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Malawi" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Malaysia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Maldives" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Mali" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Malta" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Marshall Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Martinique" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Mauritania" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Mauritius" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Mayotte" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Mexico" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Micronesia, Federal State of" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Moldova" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Monaco" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Mongolia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Montserrat" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Morocco" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Mozambique" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Myanmar" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Namibia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Nauru" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Nepal" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Netherlands Antilles" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Netherlands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="New Caledonia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="New Zealand" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Nicaragua" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Niger" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Nigeria" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Niue" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Norfolk Island" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Northern Mariana Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Norway" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Oman" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Pakistan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Palau" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Palestinian Territories" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Panama" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Papua New Guinea" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Paraguay" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Peru" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Philippines" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Pitcairn Island" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Poland" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Portugal" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Puerto Rico" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Qatar" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Republic of Korea" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Reunion Island" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Romania" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Russian Federation" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Rwanda" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Saint Helena" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Saint Kitts and Nevis" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Saint Lucia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Saint Vincent and the Grenadines" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="San Marino" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Sao Tome and Principe" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Saudi Arabia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Seborga" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Senegal" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Serbia And Montenegro" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Seychelles" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Sierra Leone" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Singapore" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Slovak Republic" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Slovenia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Solomon Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Somalia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="South Africa" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="South Georgia and Sandwich Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Spain" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Sri Lanka" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="St. Pierre and Miquelon" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Sudan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Suriname" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Svalbard and Jan Mayen Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Swaziland" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Sweden" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Switzerland" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Syrian Arab Republic" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Taiwan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Tajikistan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Tanzania" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Thailand" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Togo" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Tokelau" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Tonga" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Trinidad and Tobago" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Tunisia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Turkey" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Turkmenistan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Turks and Caicos Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Tuvalu" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="UAE" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Uganda" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Ukraine" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="United Kingdom" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Uruguay" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="US Minor Outlying Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="USA" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Uzbekistan" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Vanuatu" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Venezuela" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Vietnam" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Virgin Islands (British)" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Virgin Islands (USA)" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Wallis and Futuna Islands" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Western Sahara" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Western Samoa" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Yemen" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Yugoslavia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Zambia" Value="UAE">
                </asp:ListItem>
                <asp:ListItem Text="Zimbabwe" Value="UAE">
                </asp:ListItem>
            </asp:DropDownList>
        </div>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpNationality"
            ErrorMessage="Enter Nationality" ValidationGroup="WebValidation" CssClass="rptErrorMassage"
            Enabled="false"></asp:RequiredFieldValidator>&nbsp;
    </div>
    <div id="divTelephone" class="name" runat="server">
        <label>
            Telephone</label>
        <asp:TextBox ID="txtTelephone" runat="server" Text="" MaxLength="12" AutoComplete="off"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvAdultTelephone" Display="Dynamic" runat="server"
            ControlToValidate="txtTelephone" ErrorMessage="Enter Mobile Number" ValidationGroup="WebValidation"
            CssClass="rptErrorMassage" Enabled="false"></asp:RequiredFieldValidator>&nbsp;
        <asp:RegularExpressionValidator ID="revAdulttelephone" runat="server" Display="Dynamic"
            ControlToValidate="txtTelephone" ValidationGroup="WebValidation" ValidationExpression="^[0-9]+$"
            CssClass="rptErrorMassage" ErrorMessage="Enter Valid Number.(max 12 digit.)"
            Enabled="false">
        </asp:RegularExpressionValidator>&nbsp;
    </div>
    <div id="divPassportPlace" class="name" runat="server">
        <label>
            Passport issue place</label>
        <asp:TextBox ID="txtPassportIssueLocation" runat="server" Text="" AutoComplete="off"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvAdultLocation" runat="server" ControlToValidate="txtPassportIssueLocation"
            ErrorMessage="Enter Passsport Issue Location" ValidationGroup="WebValidation"
            CssClass="rptErrorMassage" Enabled="false"></asp:RequiredFieldValidator>&nbsp;
    </div>
    <div class="clr">
    </div>
    <div id="divPassportissue" class="other" runat="server">
        <label>
            Passport issue date</label>
        <asp:TextBox ID="txtEffectiveDate" runat="server" Text="" AutoComplete="off"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvAdultEffectiveDate" runat="server" ControlToValidate="txtEffectiveDate"
            Display="Dynamic" ErrorMessage="Enter Date of Issuance" ValidationGroup="WebValidation"
            CssClass="rptErrorMassage" Enabled="false"></asp:RequiredFieldValidator>&nbsp;
        <asp:CustomValidator ID="customIssueDateValidator" ValidationGroup="WebValidation"
            runat="server" ErrorMessage="Issue date must be greater than DOB" CssClass="rptErrorMassage"
            OnServerValidate="IssueDateValidator" ControlToValidate="txtEffectiveDate" Display="Dynamic"
            Enabled="false"></asp:CustomValidator>&nbsp;
    </div>
    <div id="divPassportexpiry" class="other" runat="server">
        <label>
            Passport expiry date</label>
        <asp:TextBox ID="txtExpiryDate" runat="server" Text="" AutoComplete="off"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvAdultExpiryDate" ValidationGroup="WebValidation"
            runat="server" ControlToValidate="txtExpiryDate" ErrorMessage="Enter Expiry Date"
            CssClass="rptErrorMassage" Enabled="false"></asp:RequiredFieldValidator>&nbsp;
        <asp:CustomValidator ID="CustomExpiryDate" ValidationGroup="WebValidation" runat="server"
            ErrorMessage="Expiry date must be greater than issue date" CssClass="rptErrorMassage"
            OnServerValidate="IssueExpiryValidator" ControlToValidate="txtExpiryDate" Display="Dynamic"
            Enabled="false"></asp:CustomValidator>&nbsp;
    </div>
</div>
<div class="clr"></div>
