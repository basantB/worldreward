﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivationMenu.ascx.cs" Inherits="UserControl_ActivationMenu" %>
<script type="text/javascript">
    $(window).resize(function () {
        if ($(window).width() >= 768) {
            $('#divMenu').removeClass('MoveLeft');
            $('#divMenu').addClass('flight-left-m14');
            $('#divBlockAll').css('display', 'none');
            $('#divBlockAll').hide();
            $('#divMenu').show();
        }
        else {
            $('#divMenu').removeClass('flight-left-m14');
            $('#divMenu').addClass('MoveLeft');
            $('#divMenu').hide();
        }
    });

    $(document).ready(function () {


        try {
            var PageName = location.href.split("/").slice(-1)[0]; //location.pathname.lastIndexOf("/") + 1;

            if (PageName == 'StatementSummary.aspx') {

                $('#divStatementSummary').addClass("active");
            }
            if (PageName == 'VoucherTransactionSummary.aspx') {
                $(".InnerTab").find('.active').removeClass("active");
                $('#divVoucherTransactionSummary').addClass("active");
            }
            else if (PageName == 'TransactionSummary.aspx') {
                $(".InnerTab").find('.active').removeClass("active");
                $('#divTransactionSummary').addClass("active");
            }
            else if (PageName == 'PurchasePoints.aspx') {
                $(".InnerTab").find('.active').removeClass("active");
                $('#divPurchasePoints').addClass("active");
            }
            else if (PageName == 'PointsExpiry.aspx') {
                $(".InnerTab").find('.active').removeClass("active");
                $('#divExpiryPoints').addClass("active");
            }
            else if (PageName == 'ManageBooking.aspx') {
                $(".InnerTab").find('.active').removeClass("active");
                $('#divManagebooking').addClass("active");
            }
            else if (PageName == 'ViewMemberProfile.aspx') {
                $(".InnerTab").find('.active').removeClass("active");
                $('#divMemberProfile').addClass("active");
            }
            else if (PageName == 'VoucherClaim.aspx') {
                $(".InnerTab").find('.active').removeClass("active");
                $('#divVoucherClaim').addClass("active");
            }
            else if (PageName == 'CashBackSummary.aspx') {
                $(".InnerTab").find('.active').removeClass("active");
                $('#divKashBackSummary').addClass("select");
            }
            else if (PageName == 'FamilyPoolRequest.aspx') {
                $(".InnerTab").find('.active').removeClass("active");
                $('#divFamily').addClass("active");
            }
            else if (PageName == 'LoanTransactionSummary.aspx') {
                $(".InnerTab").find('.active').removeClass("active");
                $('#divLoanTransactionSummary').addClass("active");
            }
        }
        catch (err) {
        }

        $('#lnkMenu-m').click(function () {

            $('#divMenu').show();
            $('#divMenu').removeClass('flight-left-m14');
            $('#divMenu').addClass('MoveLeft');
            $('#divMenu').show();
            $('#divBlockAll').css('display', 'block');
            $('#divBlockAll').show();
            $('#divMenu').stop().animate({ 'marginLeft': '0px' }, 1000, function () {
            });
        });

        $('#divBlockAll').click(function () {
            $('#divBlockAll').hide();
            $('#divMenu').stop().animate({ 'marginLeft': '-370px' }, 200);

        });
       
    });
</script>


    <%--<div class="myacc-upperhdr">
        <div class="myacc-hdrdiv robothik">My Account</div>
    </div>--%>
  <%--  <div class="myacc-menu">
        <ul>
            <li><a id="divStatementSummary" href="StatementSummary.aspx">Statement Summary</a></li>
            <li><a id="divVoucherTransactionSummary" href="VoucherTransactionSummary.aspx">Voucher Transaction Summary</a></li>
            <li><a id="divTransactionSummary" href="TransactionSummary.aspx">Transaction Summary</a></li>
            <li><a id="divLoanTransactionSummary" href="LoanTransactionSummary.aspx">Loan Transaction Summary</a></li>
            <li><a id="divKashBackSummary" href="CashBackSummary.aspx">KashBack Summary</a></li>
            <li><a id="divPurchasePoints" href="PurchasePoints.aspx">Purchase WorldRewardZ</a></li>
            <li><a id="divManagebooking" href="ManageBooking.aspx">Manage Booking</a></li>
            <li><a id="divExpiryPoints" href="PointsExpiry.aspx">WorldRewardZ Expiry</a></li>
            <li><a id="divMemberProfile" href="ViewMemberProfile.aspx">Profile</a></li>
            <li><a id="divVoucherClaim" href="VoucherClaim.aspx">Claim Voucher</a></li>
            <li><a id="divFamily" href="FamilyPoolRequest.aspx">Family+</a></li>
            <li>
                <asp:LinkButton ID="lnkLogout" OnClick="SignOut"
                    runat="server" CausesValidation="false">Logout</asp:LinkButton></li>
        </ul>
    </div>--%>
    <ul class="InnerTab">
		<li class="active"><a id="divStatementSummary" href="StatementSummary.aspx">STATEMENT SUMMARY</a></li>
		<li id="divVoucherTransactionSummary"><a  href="VoucherTransactionSummary.aspx">VOUCHER SUMMARY</a></li>
		<li id="divTransactionSummary"><a  href="TransactionSummary.aspx">TRANSACTION SUMMARY</a></li>
		<li id="divManagebooking"><a  href="ManageBooking.aspx">MANAGE BOOKING</a></li>
		<li id="divExpiryPoints"><a  href="PointsExpiry.aspx">POINTS EXPIRY</a></li>
		<li id="divMemberProfile"><a  href="ViewMemberProfile.aspx">PROFILE</a></li>
		<li id="divFamily"><a  href="FamilyPoolRequest.aspx">FAMILY POOL</a></li>
	</ul>
