﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.MemberActivity.Entities;
using InfiPlanetModel.Model;

public partial class UserControl_ActivationMenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void SignOut(object sender, EventArgs e)
    {
        InfiModel objInfiModel = new InfiModel();
        objInfiModel.LogActivity(string.Format("Member Logout"), ActivityType.Logout);
        Session.Abandon();
        Response.Redirect("Index.aspx");
    }
}