﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/UserControl/UCCTItinerayDetails.ascx.cs"
    Inherits="UserControl_UCItinerayDetails" %>
<div class="pass-details" style="padding: 0 2%;">
    <asp:Repeater ID="rptPassanger" runat="server">
        <HeaderTemplate>
            <h3>
                Passenger Details:</h3>
            <table width="100%">
                <thead>
                    <tr>
                        <th>
                            Title
                        </th>
                        <th>
                            Passenger Type
                        </th>
                        <th>
                            Passenger Name
                        </th>
                        <th>
                            Gender
                        </th>
                        <th>
                            Age
                        </th>
                    </tr>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <tr>
                    <td>
                        <%#Eval("Prefix")%>
                    </td>
                    <td>
                        <%#Eval("PaxType").ToString() == "ADT" ? "Adult":""%>
                        <%#Eval("PaxType").ToString() == "CHD" ? "Child":""%>
                        <%#Eval("PaxType").ToString() == "INF" ? "Infant":""%>
                    </td>
                    <td>
                        <%#Eval("LastName")%>,&nbsp;
                        <%#Eval("FirstName")%>
                    </td>
                    <td>
                        <%#Eval("Gender")%>
                    </td>
                    <td>
                        <%#Eval("Age")%>
                    </td>
                </tr>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            <tr bgcolor="#FFFFFF" style="color: #343434">
            </tr>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
<br />
<div class="depar-retur" style="padding: 0 2%;">
    <h3>
        Departure Flight
        <img src="images/ico-depart.png"></h3>
    <h4>
        Class:&nbsp;<asp:Label Style="float: right; margin-right: 50px;" ID="LabelClass" runat="server"></asp:Label></h4>
    <div class="clr">
    </div>
    <asp:Repeater ID="rptDeparture" runat="server">
        <HeaderTemplate>
            <table width="100%">
                <thead>
                    <tr>
                        <th>
                            Flight
                        </th>
                        <th>
                            Depart
                        </th>
                        <th>
                            Arrive
                        </th>
                        <th>
                            Depart Time
                        </th>
                        <th>
                            Arrive Time
                        </th>
                        <th>
                            Aircraft Type
                        </th>
                    </tr>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <tr>
                    <td>
                        <div>
                            <img src='<%#Eval("Carrier.CarrierLogoPath")%>' /></div>
                        <%#Eval("Carrier.CarrierName")%><br />
                        <span>
                            <%#Eval("Carrier.CarrierCode")%><%#Eval("FlightNo")%></span>
                    </td>
                    <td>
                        <%#Eval("DepartureAirField.City")%>
                        (<%#Eval("DepartureAirField.IATACode")%>)<br />
                        <%#Eval("DepartureAirField.AirportName")%>
                    </td>
                    <td>
                        <%#Eval("ArrivalAirField.City")%>
                        (<%#Eval("ArrivalAirField.IATACode")%>)<br />
                        <%#Eval("ArrivalAirField.AirportName")%>
                    </td>
                    <td>
                        <%#Convert.ToDateTime(Eval("DepartureDate").ToString()).ToString("dd MMMM yyyy")%>
                        <br />
                        <%#Convert.ToDateTime(Eval("DepartureDate").ToString()).ToString("HH:mm")%>
                    </td>
                    <td>
                        <%#Convert.ToDateTime(Eval("ArrivalDate").ToString()).ToString("dd MMMM yyyy")%>
                        <br />
                        <%#Convert.ToDateTime(Eval("ArrivalDate").ToString()).ToString("HH:mm")%>
                    </td>
                    <td>
                        <%#Eval("Carrier.EquipmentType")%>
                    </td>
                </tr>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            <tr bgcolor="#FFFFFF" style="color: #343434">
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
<br />
<div class="depar-retur" runat="server" visible="false" id="dvReturnFlight" style="padding: 0 2%;">
    <h3>
        Return Flight<img src="images/ico-return.png"></h3>
    <h4>
        Class:&nbsp;<asp:Label ID="lblReturnClass" runat="server" Style="float: right; margin-right: 50px;"></asp:Label></h4>
    <div class="clr">
    </div>
    <asp:Repeater ID="rptArrival" runat="server">
        <HeaderTemplate>
            <table width="100%">
                <thead>
                    <tr>
                        <td>
                            Flight
                        </td>
                        <td>
                            Depart
                        </td>
                        <td>
                            Arrive
                        </td>
                        <td>
                            Depart Time
                        </td>
                        <td>
                            Arrive Time
                        </td>
                        <td>
                            Aircraft Type
                        </td>
                    </tr>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <tr bgcolor="#FFFFFF" style="color: #343434">
                    <td>
                        <div>
                            <img src='<%#Eval("Carrier.CarrierLogoPath")%>' /></div>
                        <%#Eval("Carrier.CarrierName")%><br />
                        <span>
                            <%#Eval("Carrier.CarrierCode")%><%#Eval("FlightNo")%>
                        </span></div> </div>
                    </td>
                    <td>
                        <%#Eval("DepartureAirField.City")%>
                        (<%#Eval("DepartureAirField.IATACode")%>)<br />
                        <%#Eval("DepartureAirField.AirportName")%>
                    </td>
                    <td>
                        <%#Eval("ArrivalAirField.City")%>
                        (<%#Eval("ArrivalAirField.IATACode")%>)<br />
                        <%#Eval("ArrivalAirField.AirportName")%>
                    </td>
                    <td>
                        <%#Convert.ToDateTime(Eval("DepartureDate").ToString()).ToString("dd MMMM yyyy")%>
                        <br />
                        <%#Convert.ToDateTime(Eval("DepartureDate").ToString()).ToString("HH:mm")%>
                    </td>
                    <td>
                        <%#Convert.ToDateTime(Eval("ArrivalDate").ToString()).ToString("dd MMMM yyyy")%>
                        <br />
                        <%#Convert.ToDateTime(Eval("ArrivalDate").ToString()).ToString("HH:mm")%>
                    </td>
                    <td>
                        <%#Eval("Carrier.EquipmentType")%>
                    </td>
                </tr>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            <tr bgcolor="#FFFFFF" style="color: #343434">
            </tr>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
