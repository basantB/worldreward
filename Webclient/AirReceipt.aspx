﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AirReceipt.aspx.cs" Inherits="AirReceipt"
    MasterPageFile="~/InfiPlanetMaster.master" %>

<%@ Register Src="~/UserControl/UCCTItinerayDetails.ascx" TagName="ItineraryDetails"
    TagPrefix="UC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <div style="width: 768px; margin: 0 auto">
        <div class="revncofm">
            <div class="wrapper">

                <div class="body-bdr">
                    <h2>Congratulations for your Danamon Rewards Ticket!</h2>
                    <p>
                        This is your E-ticket. Do present it with a valid photo identification at the airport
                        check-in counter.
                    </p>
                    <p>
                        For international travel: The check-in counters are open 4 hours prior to departure
                        and close strictly 2 hours prior to departure.
                    </p>
                    <div class="reference">
                        <div class="col1">
                            <div>
                                Reference No.
                            </div>
                            <asp:Label ID="lblTransactionRefNo" runat="server" />
                        </div>
                        <div class="col2">
                            <div>
                                DPoints
                            </div>
                            <asp:Label ID="lblTotalMiles" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <UC:ItineraryDetails ID="ucItinarary" runat="server" />
                    <div class="pass-details">
                        <h3>Additional Details</h3>
                        <table width="100%" style="text-align: left;" cellpadding="0" cellspacing="0">
                            <tr>
                                <th>Membership No.
                                </th>
                                <th>Name
                                </th>
                                <th>Mobile
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblMembershipReferenceNo" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerName" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerMobileNo" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2">Address
                                </th>
                                <th>Email
                                </th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblCustomerAddress" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerEmail" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                    <div style="float: right; margin: 10px; padding: 0 2%;text-align:right">
                        <a onclick="window.open('AirPrintReceipt.aspx','','height=1000','width=1000')">
                    <button class="button">
                        Print</button></a>
                        <%--<asp:Button ID="btnBookNow" runat="server" CssClass="button" OnClientClick="var retvalue = redirectLocation('.aspx'); event.returnValue= retvalue;event.preventDefault(); return retvalue;"
                            Text="Print" />--%>
                    </div>
                </div>
                <!--Infi Destination Ends-->
            </div>
        </div>
    </div>
    <div class="clr"></div>


</asp:Content>
