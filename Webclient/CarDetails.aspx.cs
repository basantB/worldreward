﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CB.IBE.Platform.Car.Entities;
using CB.IBE.Platform.Car.ClientEntities;
using InfiPlanetModel.Model;
using System.Globalization;
using Core.Platform.MemberActivity.Entities;

public partial class CarDetails : System.Web.UI.Page
{
    List<Match> car = new List<Match>();
    public int item = 0;
    CarSearchResponse lobjCarSearchResponse = new CarSearchResponse();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CarSearchRequest"] != null && Session["Cars"] != null && Session["CarRefId"] != null)
        {
            // MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
            if (!IsPostBack)
            {
                Session["IsDERAdded"] = null;
                Session["CarSelected"] = null;
                Session["CarExtraListResponse"] = null;

                InfiModel lobjModel = new InfiModel();

                string lstrRelativePath = this.Page.AppRelativeVirtualPath;
                int lintActivityId = lobjModel.LogActivity(lstrRelativePath.Replace("~", string.Empty).Replace("/", string.Empty), ActivityType.PageLoad);               

                CarSearchRequest lobjCarSearchRequest = new CarSearchRequest();

                lobjCarSearchRequest = Session["CarSearchRequest"] as CarSearchRequest;
                lobjCarSearchResponse = Session["Cars"] as CarSearchResponse;
                car = lobjCarSearchResponse.SearchResponse.MatchList[0].Match.ToList();
                Match lobjCarVehicle = new Match();
                List<Match> lobjMatchList = new List<Match>();
                lobjCarVehicle = lobjModel.GetCarVehiclDetails((Session["Cars"] as CarSearchResponse).SearchResponse.MatchList[0].Match.ToList(), Session["CarRefId"].ToString());
                lobjMatchList.Add(lobjCarVehicle);
                Session["CarSelected"] = lobjCarVehicle;


                CarExtrasListResponse lobjCarExtrasListResponse = new CarExtrasListResponse();
                CarExtrasListRequest lobjCarExtrasListRequest = new CarExtrasListRequest();

                List<Vehicle> lobjVehicleList = new List<Vehicle>();
                Vehicle lobjVehicle = new Vehicle();
                lobjVehicleList.Add(lobjVehicle);
                lobjCarExtrasListRequest.ExtrasListRQ.Vehicle = lobjVehicleList.ToArray();

                lobjCarExtrasListRequest.ExtrasListRQ.Vehicle[0].id = lobjCarVehicle.Vehicle[0].id;
                lobjCarExtrasListResponse = lobjModel.CarExtrasListResponse(lobjCarExtrasListRequest, lobjCarSearchRequest);

                for (int i = 0; i < lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo.Length; i++)
                {
                    if (lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[i].Extra[0].Name == Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["ExcesProtection"]))
                    {
                        List<ExtraInfo> lobjListOfExtraInfo = new List<ExtraInfo>();
                        ExtraInfo lobjExtraInfo = new ExtraInfo();
                        lobjExtraInfo = lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[i];
                        lobjListOfExtraInfo.Add(lobjExtraInfo);
                        lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo = lobjListOfExtraInfo.ToArray();
                    }
                }
                Session["CarExtraListResponse"] = lobjCarExtrasListResponse;

                lblPickLocation.Text = lobjCarSearchResponse.SearchResponse.MatchList[0].Match[0].Route[0].PickUp[0].locName.ToString();
                lblPickCountry.Text = lobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].country + ", ";
                TimeSpan Pickts = new TimeSpan(Convert.ToInt32(lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].hour), Convert.ToInt32(lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].minute), 00);
                DateTime dtPick = DateTime.ParseExact(Pickts.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
                string strPickup = dtPick.ToString("hh:mm tt");
                lblPickTime.Text = strPickup;
                string myPickStr = lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].month + "/" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].day + "/" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].year;
                DateTime Pickresult = Convert.ToDateTime(myPickStr);
                lblPickDateTime.Text = Pickresult.ToString("dddd, MMMM dd, yyyy");

                lblDropLocation.Text = lobjCarSearchResponse.SearchResponse.MatchList[0].Match[0].Route[0].DropOff[0].locName.ToString();
                lblDropCountry.Text = lobjCarSearchRequest.SearchRequest.DropOff[0].Location[0].country + ", ";
                string myDropStr = lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].month + "/" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].day + "/" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].year;
                DateTime Dropresult = Convert.ToDateTime(myDropStr);
                lblDropDateTime.Text = Dropresult.ToString("dddd, MMMM dd, yyyy");

                TimeSpan Dropts = new TimeSpan(Convert.ToInt32(lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].hour), Convert.ToInt32(lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].minute), 00);
                DateTime dtDrop = DateTime.ParseExact(Dropts.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
                string strDrop = dtDrop.ToString("hh:mm tt");
                lblDropTime.Text = strDrop;

                lblCarName.Text = lobjCarVehicle.Vehicle[0].Name;
                imgCar.ImageUrl = lobjCarVehicle.Vehicle[0].LargeImageURL;

                hdnPaymentType.Value = Convert.ToString(Session["CarSearchPaymode"]);

                //lobjModel.LogActivity(string.Format("Car Details. PickUpDate : {0}, DropOffDate : {1}, PickUpLocation : {2}, DropOffLocation : {3}, CarName : {4}",lblPickTime.Text + " " + lblPickDateTime.Text,lblDropTime.Text + " " + lblDropDateTime.Text, lblPickLocation.Text + " " + lblPickCountry.Text, lblDropLocation.Text + " " + lblDropCountry + lblCarName.Text), ActivityType.CarBooking);               

                if (hdnPaymentType.Value == "Points")
                {
                    lblDERPoint.Text =  lobjModel.IntToThousandSeperated(lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[0].Price[0].ExtraTotalPoints);
                }
                else
                {
                    lblDERAmount.Text = lobjModel.IntToThousandSeperated(Convert.ToInt32(lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[0].Price[0].ExtraTotalBaseFare));
                    lblDERAmount.Visible = true;
                    lblHDDERAmount.Visible = true;
                    lblHDDERPoint.Visible = false;
                    lblDERPoint.Visible = false;
                }
            }
        }
        else
        {
            Response.Redirect("CarList.aspx");
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool GetIsDERAdded(bool IsDERAdded)
    {
        if (HttpContext.Current.Session["CarSearchRequest"] != null && HttpContext.Current.Session["CarSelected"] != null)
        {
            HttpContext.Current.Session["IsDERAdded"] = null;
            HttpContext.Current.Session["CarExtraListResponse"] = null;
            HttpContext.Current.Session["IsDERAdded"] = IsDERAdded;

            CarExtrasListResponse lobjCarExtrasListResponse = new CarExtrasListResponse();
            CarExtrasListRequest lobjCarExtrasListRequest = new CarExtrasListRequest();
            InfiModel lobjModel = new InfiModel();

            CarSearchRequest lobjCarSearchRequest = HttpContext.Current.Session["CarSearchRequest"] as CarSearchRequest;
            Match lobjCarVehicle = HttpContext.Current.Session["CarSelected"] as Match;

            List<Vehicle> lobjVehicleList = new List<Vehicle>();
            Vehicle lobjVehicle = new Vehicle();
            lobjVehicleList.Add(lobjVehicle);
            lobjCarExtrasListRequest.ExtrasListRQ.Vehicle = lobjVehicleList.ToArray();

            lobjCarExtrasListRequest.ExtrasListRQ.Vehicle[0].id = lobjCarVehicle.Vehicle[0].id;
            lobjCarExtrasListResponse = lobjModel.CarExtrasListResponse(lobjCarExtrasListRequest, lobjCarSearchRequest);

            for (int i = 0; i < lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo.Length; i++)
            {
                if (lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[i].Extra[0].Name == Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["ExcesProtection"]))
                {
                    List<ExtraInfo> lobjListOfExtraInfo = new List<ExtraInfo>();
                    ExtraInfo lobjExtraInfo = new ExtraInfo();
                    lobjExtraInfo = lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[i];
                    lobjListOfExtraInfo.Add(lobjExtraInfo);
                    lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo = lobjListOfExtraInfo.ToArray();
                }
            }
            lobjCarExtrasListResponse.IsDERAdded = IsDERAdded;
            HttpContext.Current.Session["CarExtraListResponse"] = lobjCarExtrasListResponse;

        }
        return true;
    }
}