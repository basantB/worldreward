﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="TransactionSummary.aspx.cs" Inherits="TransactionSummary" %>

<%@ Register Src="~/UserControl/ActivationMenu.ascx" TagName="ActivationMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="js/jquery.ui.position.js" type="text/javascript"></script>
    <link href="css/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <link href="css/dropDown.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.selectric.min.js" type="text/javascript"></script>
    <link href="css/myAccount.css" rel="stylesheet" />
    <script>

        window.onload = function () {
            ApplyCss();
        };
        function ApplyCss() {
            $('select').selectric({
                maxHeight: 100,
                disableOnMobile: false,
                responsive: true
            });
        };
        //On PostBack
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    ApplyCss();
                }
            });
        };
    </script>
    <script type="text/javascript">
        $(function () {
            $(".FromDate").datepicker({
                numberOfMonths: 2,
                showButtonPanel: false,
                nextText: '',
                dateFormat: 'dd/mm/yy',
                onSelect: function (dateText, inst) {
                    $(".Todate").val('');
                    $(".Todate").datepicker("destroy");
                    $(".Todate").datepicker({
                        minDate: dateText,
                        numberOfMonths: 2,
                        showButtonPanel: false,
                        prevText: '',
                        dateFormat: 'dd/mm/yy',
                        nextText: ''
                    });
                }
            });
        });
    </script>
    <style type="text/css">
        .DDLCover{float: left;border:1px solid #ccc;width:18%;margin-top:5px;}
        .fl
        {
            float: left;
        }
        #CP_DivPaging table, #CP_DivPaging td
        {
            border: 0;
        }
        @media screen and (max-device-width :540px)
        {
            .DDLCover{float: left;border:1px solid #ccc;width:94%;margin:5px 0;}
        }
    </style>
    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>

    <!--new -->
     <article id="article">
         <div class="wrap">
            <h1>Experience Abundance with WorldRewardZ</h1>
            <p>Below is a categorized listing of your activities at WorldRewardZ. As you glance through them, you can immerse in the joy of having the best choices in the world available to you simply at a click, with WorldRewardZ. Go ahead and multiply your unique experiences.</p>
			<div class="ManyWrap MyAccount">
                <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
                <div class="InnCont" id="divCurrency" runat="server">
                <div class="myacc-user" style="display:none;">
                <asp:DropDownList ID="ddlProgramCurrency" runat="server" AppendDataBoundItems="true"
                OnSelectedIndexChanged="ddlCurrencyName_SelectedIndexChanged" AutoPostBack="true"
                Style="padding-top: 0.1vw; padding-bottom: 0.1vw; margin-top: 1vw; margin-bottom: 0.7vw;
                color: #bbbcbe"></asp:DropDownList>
                </div>
               <div class="input-bg DDLCover">
               <asp:DropDownList ID="ddlPoolingDetails" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlPoolingDetails_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
                </div>
                <div class="clr"></div>
                <div class="filtRadio">
				<div class="chkBox">
                 <asp:RadioButton runat="server" ID="RdlSearchtypeAll" GroupName="RdlSearchtype" AutoPostBack="true" OnCheckedChanged="RdlSearchtypeAll_CheckedChanged" Checked="true" /><label> Transactions</label>
				</div>
                 <div class="chkBox">
                  <asp:RadioButton runat="server" ID="RdlSearchtypeCust" GroupName="RdlSearchtype" AutoPostBack="true" OnCheckedChanged="RdlSearchtypeCust_CheckedChanged" />
                    <label> Custom Search</label>
                 </div>
                  <br class="clr">
                  </div>
                <ul class="tabInfo" id="tblsearchinfo" runat="server" >
		            <li>
			        <label>From</label>
                        <asp:TextBox ID="FromDate" Style="margin-top: 0.3vw; width:80%;" runat="server" CssClass="FromDate icnCal" />   
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FromDate"
                            ErrorMessage="Enter From Date<br/>" Style="color: #ff0000" Display="Dynamic"
                            ValidationGroup="DateSearch"></asp:RequiredFieldValidator>
		            </li>
		            <li class="immr0">
			        <label>To</label>
                      <asp:TextBox ID="Todate" runat="server" CssClass="Todate icnCal" Style="margin-top: 0.3vw; width:80%;"/>
                        
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Todate"
                            ErrorMessage="Enter To Date." Style="color: #ff0000" Display="Dynamic" ValidationGroup="DateSearch"></asp:RequiredFieldValidator>
		            </li>
		            <li><label>&nbsp;</label><a class="btnRed" href="#"><asp:Button ID="btnSearch" runat="server" ValidationGroup="DateSearch" 
                            CssClass="button" Text="Continue" onclick="btnSearch_Click" /></a></li>
                 </ul>
            <asp:Repeater ID="RepSummaryInfo" runat="server">
            <HeaderTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                     <tr class="tbl_th">
                         <th class="vmid"><div class="th-block-left">
                            Description
                            </div>
                        </th>
                         <th class="vmid"  width="180"><div class="th-block-left">
                            Amount (AED)
                        </div>
                        </th>
                         <th class="vmid" width="120"><div class="th-block-left">
                            WorldRewardZ
                        </div>
                        </th>
                        <th class="vmid"><div class="th-block-left">
                            Transaction Date
                       </div>
                       </th>
                       <th class="vmid"><div class="th-block-left">
                            Processing Date
                        </div>
                        </th>
                        <th class="vmid"><div class="th-block-left">
                            Transaction Type
                        </div>
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr style="color: #000000" align="center">
                    <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("MerchantName")%>
                    </div>
                    </td>
                   <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("Amounts")%>
                        </div>
                    </td>
                    <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("Points")%>
                        </div>
                    </td>
                   <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("TransactionDate", "{0:dd/MM/yyyy}")%>
                        </div>
                    </td>
                    <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("ProcessingDate", "{0:dd/MM/yyyy}")%>
                        </div>
                    </td>
                   <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("LoyaltyTxnType").ToString()%>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

              <div class="btmLinks" id="DivPaging" runat="server">
                        <asp:LinkButton ID="linkbuttonFirst" runat="server" Style="color: #000; text-decoration: underline"
                            OnClick="lbtnFirst_Click">First</asp:LinkButton>
                  
                        <asp:LinkButton ID="linkbuttonPrevious" runat="server" Style="color: #000; text-decoration: underline"
                            OnClick="lbtnPrevious_Click">Previous</asp:LinkButton>
                  
                        <asp:DataList ID="dlPaging" runat="server" OnItemCommand="dlPaging_ItemCommand" RepeatDirection="Horizontal"
                            ShowFooter="False" ShowHeader="False" Width="10px" OnItemDataBound="dlPaging_ItemDataBound">
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="LblPageNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' Visible="false"></asp:Label>
                                <asp:LinkButton ID="lnkbtnPaging" runat="server" CommandArgument='<%#Eval("PageIndex")%>'
                                    CommandName="Paging" Style="color: #000; text-decoration: underline" Text='<%# Eval("ID")%>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:DataList>
                   
                        <asp:LinkButton ID="linkbuttonNext" runat="server" Style="color: #000; text-decoration: underline"
                            OnClick="lbtnNext_Click">Next</asp:LinkButton>
                        &nbsp;
                   
                        <asp:LinkButton ID="linkbuttonLast" runat="server" Style="color: #000; text-decoration: underline"
                            OnClick="lbtnLast_Click">Last</asp:LinkButton>
                         </div>

                    <asp:Label ID="lblNoResult" runat="server"></asp:Label>
                 </div>
                </div>
             </div>
         </article>
    <!--end of new-->

    <%--<div class="inr-wrap">
        <section class="myaccContent">
    <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
    <div class="myacc-conthdr">
        <div id="divCurrency" class="myacc-user" runat="server">
            Select Currency :
            <asp:DropDownList ID="ddlProgramCurrency" runat="server" AppendDataBoundItems="true"
                OnSelectedIndexChanged="ddlCurrencyName_SelectedIndexChanged" AutoPostBack="true"
                Style="padding-top: 0.1vw; padding-bottom: 0.1vw; margin-top: 1vw; margin-bottom: 0.7vw;
                color: #bbbcbe"></asp:DropDownList>
        </div>
        <div class="input-bg DDLCover">
         <asp:DropDownList ID="ddlPoolingDetails" runat="server" AppendDataBoundItems="true"
                                OnSelectedIndexChanged="ddlPoolingDetails_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                            </div>
                            <div class="clr"></div>
        <div class="acc-transactnsumry">
            <div class="acc-trnsactn-type">
                <asp:RadioButton runat="server" ID="RdlSearchtypeAll" GroupName="RdlSearchtype" AutoPostBack="true"
                    OnCheckedChanged="RdlSearchtypeAll_CheckedChanged" Checked="true" />All Transactions</label></div>
            <div class="acc-trnsactn-type">
                <asp:RadioButton runat="server" ID="RdlSearchtypeCust" GroupName="RdlSearchtype"
                    AutoPostBack="true" OnCheckedChanged="RdlSearchtypeCust_CheckedChanged" />Custom
                Search</label></div>
        </div>
        <br />
        <div class="acc-transactnsumry" id="tblsearchinfo" runat="server" style="display:none">
            <table border="0" cellpadding="1" style="width: 100%;">
                <tr  style="width: 100%; float: left">
                    <td width="30%" style=" border:none;">
                        From<br />
                        <asp:TextBox ID="FromDate" Style="margin-top: 0.3vw; width:80%;" runat="server"
                            CssClass="FromDate" />
                        
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FromDate"
                            ErrorMessage="Enter From Date<br/>" Style="color: #ff0000" Display="Dynamic"
                            ValidationGroup="DateSearch"></asp:RequiredFieldValidator>
                    </td>
                    <td width="30%" style="border:none;">
                        To<br />
                    <asp:TextBox ID="Todate" runat="server" CssClass="Todate" Style="margin-top: 0.3vw; width:80%;"/>
                        
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Todate"
                            ErrorMessage="Enter To Date." Style="color: #ff0000" Display="Dynamic" ValidationGroup="DateSearch"></asp:RequiredFieldValidator>
                    </td>
                    <td width="16%" style=" border:none;" align="right">
                    &nbsp;<br />
                    <asp:Button ID="btnSearch" runat="server" ValidationGroup="DateSearch" 
                            CssClass="button" Text="Search" onclick="btnSearch_Click" />
                      
                    </td>
                </tr>
            </table>
        </div>
         <div class="sumry-tbl">
        <asp:Repeater ID="RepSummaryInfo" runat="server">
            <HeaderTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                     <tr class="tbl_th">
                         <th class="vmid"><div class="th-block-left">
                            Description
                            </div>
                        </th>
                         <th class="vmid"  width="180"><div class="th-block-left">
                            Amount (AED)
                        </div>
                        </th>
                         <th class="vmid" width="120"><div class="th-block-left">
                            WorldRewardZ
                        </div>
                        </th>
                        <th class="vmid"><div class="th-block-left">
                            Transaction Date
                       </div>
                       </th>
                       <th class="vmid"><div class="th-block-left">
                            Processing Date
                        </div>
                        </th>
                        <th class="vmid"><div class="th-block-left">
                            Transaction Type
                        </div>
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr style="color: #000000" align="center">
                    <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("MerchantName")%>
                    </div>
                    </td>
                   <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("Amounts")%>
                        </div>
                    </td>
                    <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("Points")%>
                        </div>
                    </td>
                   <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("TransactionDate", "{0:dd/MM/yyyy}")%>
                        </div>
                    </td>
                    <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("ProcessingDate", "{0:dd/MM/yyyy}")%>
                        </div>
                    </td>
                   <td class="bord_top"><div class="td-block-left fl">
                        <%#Eval("LoyaltyTxnType").ToString()%>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
        <div>
            <table class="PageingStyle padding10" id="DivPaging" runat="server">
                <tr>
                    <td class="repeaterPageingFirstTab">
                        <asp:LinkButton ID="linkbuttonFirst" runat="server" Style="color: #000; text-decoration: underline"
                            OnClick="lbtnFirst_Click">First</asp:LinkButton>
                    </td>
                    <td class="repeaterPageingPreTab">
                        <asp:LinkButton ID="linkbuttonPrevious" runat="server" Style="color: #000; text-decoration: underline"
                            OnClick="lbtnPrevious_Click">Previous</asp:LinkButton>
                    </td>
                    <td class="repeaterPageingNumberTab">
                        <asp:DataList ID="dlPaging" runat="server" OnItemCommand="dlPaging_ItemCommand" RepeatDirection="Horizontal"
                            ShowFooter="False" ShowHeader="False" Width="10px" OnItemDataBound="dlPaging_ItemDataBound">
                            <ItemStyle />
                            <ItemTemplate>
                                <asp:Label ID="LblPageNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' Visible="false"></asp:Label>
                                <asp:LinkButton ID="lnkbtnPaging" runat="server" CommandArgument='<%#Eval("PageIndex")%>'
                                    CommandName="Paging" Style="color: #000; text-decoration: underline" Text='<%# Eval("ID")%>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:DataList>
                    </td>
                    <td class="repeaterPageingNextTab">
                        <asp:LinkButton ID="linkbuttonNext" runat="server" Style="color: #000; text-decoration: underline"
                            OnClick="lbtnNext_Click">Next</asp:LinkButton>
                        &nbsp;
                    </td>
                    <td class="repeaterPageingNextTab">
                        <asp:LinkButton ID="linkbuttonLast" runat="server" Style="color: #000; text-decoration: underline"
                            OnClick="lbtnLast_Click">Last</asp:LinkButton>
                    </td>
                </tr>
            </table>
        
        <asp:Label ID="lblNoResult" runat="server"></asp:Label>
        </div>
    </div>
     </section>
        <div class="clr">
        </div>
    </div>--%>
</asp:Content>
