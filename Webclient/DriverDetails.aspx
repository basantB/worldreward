﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="DriverDetails.aspx.cs" Inherits="DriverDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="Css/Car.css" type="text/css" rel="stylesheet" />
    <div class="inrpg">
        <div class="pgcontent ">
            <div class="pgcol1 cktinputs">
                <div id="PointsInfo" runat="server" style="display: none" class="ErrorMsgContainer">
                    <div id="Div1" runat="server">
                        <asp:Label ID="lblMsg" runat="server" Text="" Style="margin-left: 0px!important;"></asp:Label>
                    </div>
                </div>
                <div class="checkoutheader">
                    <h1>Personal Details</h1>
                    <div class="passenger-block">
                        <div class="Flight_Ticket_holder">
                            <div id="ErrorMsgContainer" class="errorMsg"></div>
                            <ul>
                                <li>
                                    <label>
                                        Title</label>
                                    <div class="cktinputbg">
                                        <asp:DropDownList ID="DDLTitle" runat="server">
                                            <asp:ListItem>Mr</asp:ListItem>
                                            <asp:ListItem>Mrs</asp:ListItem>
                                            <asp:ListItem>Dr</asp:ListItem>
                                            <asp:ListItem>Ms</asp:ListItem>
                                            <asp:ListItem>Miss</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </li>
                                <li>
                                    <label>
                                        First Name</label>
                                    <asp:TextBox ID="TextBoxFirstName" runat="server" Text="Enter First name"
                                        onfocus="placeholderOnFocus(this,'Enter First name');" onblur="placeholderOnFocus(this,'Enter First name');"></asp:TextBox>
                                </li>
                                <li>
                                    <label>
                                        Last Name</label>
                                    <asp:TextBox ID="TextBoxSurname" runat="server" Text="Enter Last Name"
                                        onfocus="placeholderOnFocus(this,'Enter Last Name');" onblur="placeholderOnFocus(this,'Enter Last Name');"></asp:TextBox>
                                </li>
                            </ul>
                            <div class="clr">
                            </div>
                            <ul class="mtop2">
                                <li>
                                    <input id="chkHaveFlightNo" type="checkbox" value="Points" runat="server" />
                                    Do you have Flight details at hand
                                </li>
                            </ul>
                            <div class="clr">
                            </div>
                            <ul class="mtop2" id="DivFlightNumber" style="display: none">
                                <li>
                                    <label>
                                        Flight Number</label>
                                    <asp:TextBox ID="TextBoxFlightNumber" runat="server"></asp:TextBox>
                                </li>
                            </ul>
                            <div class="clr">
                            </div>
                            <div class="col-3 fR">
                                <asp:Button ID="ImageButtonBookNow" CssClass="button" runat="server" OnClientClick="var retvalue = BookNowTest(); event.returnValue= retvalue;event.preventDefault(); return retvalue;"
                                    OnClick="ImageButtonBookNow_Click" Text="PROCEED TO PAY" />
                            </div>
                        </div>
                        <div class="clr">
                        </div>

                    </div>
                </div>
                <!--StepsPage-->
            </div>
            <div class="pgcol2">
                <div class="widget-prfl" id="MasterUser">
                    <div class="widget-user">
                        <ul id="UserDetails">
                        </ul>
                    </div>
                    <div class="widget-srhpnl">
                        <h3>
                            <div>
                                Itinerary <span class="edit"><a href="CarList.aspx">edit</a> </span>
                            </div>
                        </h3>
                        <div class="carchkout">
                            <ul>
                                <li class="brdbottom">
                                    <div class="amt">
                                        <asp:Label runat="server" ID="lblCarName"></asp:Label>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </li>
                                <li>
                                    <div class="txtC">
                                        <asp:Image runat="server" ID="imgCar" />
                                    </div>
                                </li>
                                <li>
                                    <div class="itenaryEmptyDiv">
                                        &nbsp;
                                    </div>
                                    <div class="itenaryContent">
                                        <div class="item-Name">
                                            <asp:Label runat="server" ID="lblPickLocation"></asp:Label>
                                            <asp:Label runat="server" ID="lblPickCountry"></asp:Label>
                                        </div>
                                        <p>
                                            <asp:Label runat="server" ID="lblPickDateTime"></asp:Label>
                                        </p>
                                        <p>
                                            <img src="images/time.png" />
                                            <asp:Label runat="server" ID="lblPickTime"></asp:Label>
                                        </p>
                                    </div>
                                    <div class="itenaryEmptyDiv">
                                        &nbsp;
                                    </div>
                                    <div class="itenaryContent">
                                        <div class="item-Name">
                                            <asp:Label runat="server" ID="lblDropLocation"></asp:Label>
                                            <asp:Label runat="server" ID="lblDropCountry"></asp:Label>
                                        </div>
                                        <p>
                                            <asp:Label runat="server" ID="lblDropDateTime"></asp:Label>
                                        </p>
                                        <p>
                                            <img src="images/time.png" />
                                            <asp:Label runat="server" ID="lblDropTime"></asp:Label>
                                        </p>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </li>
                                <li class="total bgcolGrL brdbottom">
                                    <div class="ftr">
                                        <span >Total DPoints: </span>
                                                <span class="amt"><asp:Label runat="server" ID="Total_Value"></asp:Label></span>    
                                        
                                        <div class="clr">
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
            <!--CheckOut Page Ends-->
            <div class="clr">
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function placeholderOnFocus(obj, defaultVal) {
            if (obj.value == "") {
                obj.value = defaultVal;
            } else if (obj.value == defaultVal) {
                obj.value = "";
            } else { }
        };
        function BookNowTest() {

            var msg = "";
            $("#ErrorMsgContainer").hide();
            if ($("#CP_TextBoxFirstName").val().length == 0 || $("#CP_TextBoxFirstName").val() == "Enter First name") {
                msg = "Please Enter First name." + "<br/>";
            }
            else if ($("#CP_TextBoxSurname").val().length == 0 || $("#CP_TextBoxSurname").val() == "Enter Last name") {
                msg = "Please Enter Last name." + "<br/>";
            }

            if (msg.length > 0) {
                $("#LoginValidation")[0].innerHTML = msg;
                $("#CP_ErrorMsgContainer").show();
                return false;
            }
            else {
                e.preventDefault();
            }
        };
        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        };
        $(document).ready(function () {
            $("#DivFlightNumber").hide();
            $("#CP_ErrorMsgContainer").hide();
            $("#CP_chkHaveFlightNo").click(function () {
                if (this.checked)
                    $("#DivFlightNumber").show();
                else
                    $("#DivFlightNumber").hide();
            });
            $("#CP_TextBoxFirstName").keydown(function (event) {
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    return;
                }
                else
                    event.preventDefault();
            });
            $("#CP_TextBoxSurname").keydown(function (event) {
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    return;
                }
                else
                    event.preventDefault();
            });
        });
    </script>
</asp:Content>
