﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <script src="js/Validation.js" type="text/javascript"></script>
    <link href="css/mystyle.css" rel="stylesheet" />
    <script type="text/javascript">
        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        };

        function LoginOnEnter(e) {
            var ENTER_KEY = 13;
            var code = "";
            var code = (e.keyCode ? e.keyCode : e.which);

            if (code == ENTER_KEY) {
                LoginValidationPage();
                return false;
            }
            return true;
        };


        function redirectLocation(url) {
            window.location.href = url;
            return false;
        };

        function LoginValidationPage() {
            var msg = "";
            $("#CP_ErrorMsgContainer").hide();
            $("#LoginValidation")[0].innerHTML = "";
            $("#CP_lblLoginError").html('');
            var vdf = $("#CP_txtMemberName").val();

            if ($("#CP_txtMemberName").val().length == 0) {
                msg += "Please enter WorldRewardZ Member ID.<br/>";
            }
            if ($("#CP_txtPassword").val().length == 0) {
                msg += "Please enter Password.<br/>";
            }
            if ($("#CP_txtMemberName").val().length == 0) {
                if (($('#CP_txtPassword').val().length < 8) && ($('#CP_txtPassword').val().length > 1)) {
                    msg += "Password length has to be minimum eight characters.";
                }
            }

            if (msg.length > 0) {
                $("#LoginValidation").css('color', 'red');
                $("#CP_ErrorMsgContainer").show();
                $("#LoginValidation")[0].innerHTML = msg;
            }

            else {
                var MemberId = $("#CP_txtMemberName").val();
                var password = $("#CP_txtPassword").val();
                var RememberMe = new Boolean();
                RememberMe = $("#CP_chkRememberMe")[0].checked;
                $.ajax({
                    url: 'Login.aspx/SigninUser',
                    type: 'POST',  // or get
                    contentType: 'application/json; charset =utf-8',
                    data: "{'pstrMemberid':'" + MemberId.toString() + "'" + "," + "'pstrPassword':'" + password.toString() + "'" + "," + "'pboolRememberMe':'" + RememberMe + "'}",
                    dataType: 'json',
                    success: function (data) {
                        try {

                            var newData = data.d;

                            if ((newData != "") || newData == null) {
                                if (data.d == "AccountLock") {
                                    $("#LoginValidation")[0].innerHTML = "";
                                    $("#CP_ErrorMsgContainer").show();
                                    $("#CP_lblLoginError").html('Your Account is Locked.Please <a class="ErrorMessageLink" onclick="showOTPDiv();">click here</a> to unlock the same.');
                                }
                                else if (data.d == "AuthenticationFailed") {
                                    $("#LoginValidation").css('color', 'red');
                                    $("#LoginValidation")[0].innerHTML = "";
                                    $("#CP_ErrorMsgContainer").show();
                                    $("#CP_lblLoginError").html('Please check the login details you have entered and try again.');
                                }
                                else if (data.d == "IncorrectFormat") {
                                    $("#LoginValidation")[0].innerHTML = "";
                                    $("#CP_ErrorMsgContainer").show();
                                    $("#CP_lblLoginError").html('Please check the login details you have entered and try again.');
                                    //$("#CP_lblLoginError").html('Password must contain one non-alpha character,one upper case character,one lower case character.');
                                }
                                else {
                                    $("#LoginValidation")[0].innerHTML = "";
                                    window.location.href = data.d;
                                }
                            }
                            else {
                                $("#LoginValidation")[0].innerHTML = "";
                                $("#CP_ErrorMsgContainer").show();
                                $("#CP_lblLoginError").html('Please check the login details you have entered and try again.');
                            }
                        }
                        catch (e) {
                            alert(e);
                            return false;
                        }
                        return false;
                    },
                    error: function (errmsg) {

                    }
                });
            }
            return false;
        };
    </script>
    <!--bread Crums-->
    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>
    <!--bread Crums-->
    <div class="inr-wrap">
        <section class="statcpgCont">
          <div class="clr"></div>
      <aside class="pg-banr">
            <img src="images/banner-login.jpg" class="w100P" />
      </aside>
        <div class="flt-contehldr">
        <div class="flt-divR">
                    <div class="row">
                        <h2 class="robot">Member Login</h2>
                        <h5 class="robot mandatory-info">All fields are mandatory.</h5>
                    </div>
                    <div class="clear"></div>
                    <div class="myacc-login">
                    <div id="divLogin">
                    <ul class="Login">
                               <li class="label">WorldRewardZ ID:</li>
                               <li class="input"><input type="text" class="robot" id="txtMemberName" runat="server" onkeypress="var retValue = LoginOnEnter(event); event.returnValue = retValue; return retValue;"/></li>
                               <li class="clear disblk">&nbsp;</li>
                               <li class="label">Password:</li>
                               <li class="input"><input type="password" runat="server" class="robot" id="txtPassword" onkeypress="var retValue = LoginOnEnter(event); event.returnValue = retValue; return retValue;"/></li>
                               <li class="clear disblk">&nbsp;</li>
                               <li class="label"></li>
                               <li>
                                   <ul class="chkbxtxt">
                                       <li><input type="checkbox" ID="chkRememberMe" runat="server" /></li>
                                       <li><span>Remember me on this computer</span> </li>
                                   </ul>
                               </li>
                                <li class="clear disblk">&nbsp;</li>
                              <li class="label">&nbsp;</li>
                              <li class="input loginbtn"><input type="button" class="robot" value="Login" onclick="var retvalue = LoginValidationPage(); event.returnValue = retvalue; return retvalue;" /></li>
                              <li class="clear disblk">&nbsp;</li>
                              <li class="label">&nbsp;</li>
                              <li>
                                  
                               <asp:LinkButton ID="FormLinkPassword" CausesValidation="false" CssClass="ForgotLine" runat="server" Text="Forgot Password"
                                OnClientClick="var retvalue = redirectLocation('ForgotPassword.aspx'); event.returnValue= retvalue; return retvalue;"
                                ></asp:LinkButton> &nbsp; | &nbsp;
                            <asp:LinkButton ID="FormLinkActiveMembership" CausesValidation="false" runat="server"
                                OnClientClick="var retvalue = redirectLocation('Register.aspx'); event.returnValue= retvalue; return retvalue;"
                                Text="Activate Membership" ></asp:LinkButton>
                                  
                              </li>     
                           </ul>
                    </div>
                    <div id="divOTP" style="display:none;">
                    <ul>
                               <li class="label">WorldRewardZ ID:</li>
                               <li class="input"><input type="text" class="robot" id="txtMemberID" runat="server" onkeypress="var retValue = LoginOTPOnEnter(event); event.returnValue = retValue; return retValue;"/></li>
                               <li class="clear disblk">&nbsp;</li>
                               <li class="label">OTP:</li>
                               <li class="input"><input type="password" runat="server" class="robot" id="txtPwd" maxlength="4" onkeypress="var retValue = LoginOTPOnEnter(event); event.returnValue = retValue; return retValue;"/></li>
                               <li class="clear disblk">&nbsp;</li>
                               <li class="label"></li>
                                <li class="clear disblk">&nbsp;</li>
                              <li class="label">&nbsp;</li>
                              <li class="input loginbtn"><input type="button" class="robot" value="Continue" onclick="var retvalue = UnLockMemberByOTP(); event.returnValue = retvalue; return retvalue;" /></li>
                              <li class="clear disblk">&nbsp;</li>
                              <li class="label">&nbsp;</li>
                                
                           </ul>
                    </div>
                        <div class="clear"></div>
                           <div id="ErrorMsgContainer" class=" ErrorMsgContainer mtop_10" runat="server">
                                    <asp:Label runat="server" ID="lblLoginError" Text="" ForeColor="Red"></asp:Label>
                                    <div id="LoginValidation">
                                    </div>
                                </div>


                            
                       </div>  
                       
                    </div>

        </div>      
<div class="clr"> </div>
    </section>
        <div class="clr">
        </div>
    </div>
    <script type="text/javascript">
        function showMyLoginDiv() {
            $("#divLogin").css("display", "none");
            $("#divOTP").css("display", "block");
            $("#LoginValidation")[0].innerHTML = "";
            $("#ct_SB_lblLoginError").text('');

        }
        function showOTPDiv() {
            $("#divLogin").css("display", "none");
            $("#divOTP").css("display", "block");
            $("#LoginValidation")[0].innerHTML = "";
            $("#ct_SB_lblLoginError").text('');
            GenerateOTPonLogin();
        }
        function showLoginDiv() {
            $("#divLogin").css("display", "block");
            $("#divOTP").css("display", "none");
            $("#LoginValidation")[0].innerHTML = "";
            $("#ct_SB_lblLoginError").text('');
        }
    </script>
</asp:Content>
