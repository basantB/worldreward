﻿using Core.Platform.FileParsing.PromotionCalculations.InfiPayMerchantPromotion;
using Framework.EnterpriseLibrary.Adapters;
using InfiPlanetModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.ProgramMaster.Entities;
using InfiPlanet.SessionConstants;
using Viator.Platform.ClientEntities;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using System.IO;
using System.Globalization;

public partial class TourGradeList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (HttpContext.Current.Session[SessionFields.SelectedTourPackage] != null && HttpContext.Current.Session[SessionFields.BookingAvailabilityRequest] != null && HttpContext.Current.Session[SessionFields.BookingAvailabilityResponse] != null)
            {
                ProductDetails lobjProduct = HttpContext.Current.Session[SessionFields.SelectedTourPackage] as ProductDetails;
                ViatorBookingAvailabilityRequest lobjViatorBookingAvailabilityRequest = HttpContext.Current.Session[SessionFields.BookingAvailabilityRequest] as ViatorBookingAvailabilityRequest;
                ViatorBookingAvailability lobjBookingAvailability = HttpContext.Current.Session[SessionFields.BookingAvailabilityResponse] as ViatorBookingAvailability;
                string lstrBookingDate = (lobjBookingAvailability.BookingDate).Replace('-', '/');
                DateTime ldtBookiingDate = Convert.ToDateTime(lstrBookingDate);
                //ToString("dd/M/yyyy", CultureInfo.InvariantCulture)
                txtBookingDate.Text = ldtBookiingDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                //txtBookingDate.Text = lstrBookingDate;

                lblProductTitle.Text = lobjProduct.Title;
                lblBookingDate.Text = " On " + Convert.ToString(lstrBookingDate);
                for (int lintCount = 0; lintCount < lobjProduct.AgeBandsList.Count; lintCount++)
                {
                    if (lobjProduct.AgeBandsList[lintCount].BandId == 1)
                    {
                        divAdult.Style.Add("display", "block");
                        lblAdultVal.Text = "(" + lobjProduct.AgeBandsList[lintCount].AgeFrom + "-" + lobjProduct.AgeBandsList[lintCount].AgeTo + " Years)";

                    }
                    else if (lobjProduct.AgeBandsList[lintCount].BandId == 2)
                    {
                        divChild.Style.Add("display", "block");
                        lblChildVal.Text = "(" + lobjProduct.AgeBandsList[lintCount].AgeFrom + "-" + lobjProduct.AgeBandsList[lintCount].AgeTo + " Years)";
                    }
                    else if (lobjProduct.AgeBandsList[lintCount].BandId == 3)
                    {
                        divInfant.Style.Add("display", "block");
                        lblInfantval.Text = "(" + lobjProduct.AgeBandsList[lintCount].AgeFrom + "-" + lobjProduct.AgeBandsList[lintCount].AgeTo + " Years)";

                    }
                    else if (lobjProduct.AgeBandsList[lintCount].BandId == 5)
                    {
                        divSenior.Style.Add("display", "block");
                        lblSeniorVal.Text = "(" + lobjProduct.AgeBandsList[lintCount].AgeFrom + "-" + lobjProduct.AgeBandsList[lintCount].AgeTo + " Years)";

                    }
                }

                for (int lintCount = 0; lintCount < lobjViatorBookingAvailabilityRequest.Ageband.Count; lintCount++)
                {
                    if (lobjViatorBookingAvailabilityRequest.Ageband[lintCount].BandId == 1)
                    {

                        lblAdult.Text = Convert.ToString(lobjViatorBookingAvailabilityRequest.Ageband[lintCount].count) + " Adult";
                        ddlAdult.SelectedValue = Convert.ToString(lobjViatorBookingAvailabilityRequest.Ageband[lintCount].count);
                    }
                    else if (lobjViatorBookingAvailabilityRequest.Ageband[lintCount].BandId == 2)
                    {

                        lblChild.Text = ", " + Convert.ToString(lobjViatorBookingAvailabilityRequest.Ageband[lintCount].count) + " Child";
                        ddlChild.SelectedValue = Convert.ToString(lobjViatorBookingAvailabilityRequest.Ageband[lintCount].count);
                    }
                    else if (lobjViatorBookingAvailabilityRequest.Ageband[lintCount].BandId == 3)
                    {

                        lblInfant.Text = ", " + Convert.ToString(lobjViatorBookingAvailabilityRequest.Ageband[lintCount].count) + " Infant"; ;
                        ddlSenior.SelectedValue = Convert.ToString(lobjViatorBookingAvailabilityRequest.Ageband[lintCount].count);
                    }
                    else if (lobjViatorBookingAvailabilityRequest.Ageband[lintCount].BandId == 5)
                    {

                        lblSenior.Text = ", " + Convert.ToString(lobjViatorBookingAvailabilityRequest.Ageband[lintCount].count) + " Senior"; ;
                        ddlInfant.SelectedValue = Convert.ToString(lobjViatorBookingAvailabilityRequest.Ageband[lintCount].count);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("TougradeList :-" + ex.Message + ex.InnerException + "Stack Trace-" + ex.StackTrace);
        }
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] LoadTourGradeList()
    {
        try
        {
            ViatorProductRequest lobjViatorProductRequest = new ViatorProductRequest();
            ViatorAvailableTourGradeResponse lobjViatorAvailableTourGradeResponse = new ViatorAvailableTourGradeResponse();
            List<ViatorAvailableTourGradeList> lobjViatorAvailableTourGradeList = null;
            string[] lobjListOfData = new string[2];
            string lstrProductResult = string.Empty;
            string lstrTemplateResult = string.Empty;

            Core.Platform.ProgramMaster.Entities.ProgramDefinition lobjProgramDefinition = new Core.Platform.ProgramMaster.Entities.ProgramDefinition();
            InfiModel lobjInfiModel = new InfiModel();
            lobjProgramDefinition = lobjInfiModel.GetProgramMaster();
            if (lobjProgramDefinition != null)
            {
                LoggingAdapter.WriteLog("TourGradeList program details not null");
                if (HttpContext.Current.Session[SessionFields.SelectedTourPackage] != null && HttpContext.Current.Session[SessionFields.BookingAvailabilityRequest] != null && HttpContext.Current.Session[SessionFields.BookingAvailabilityResponse] != null)
                {
                    ProductDetails lobjProduct = HttpContext.Current.Session[SessionFields.SelectedTourPackage] as ProductDetails;
                    ViatorBookingAvailabilityRequest lobjViatorBookingAvailabilityRequest = HttpContext.Current.Session[SessionFields.BookingAvailabilityRequest] as ViatorBookingAvailabilityRequest;
                    ViatorBookingAvailability lobjBookingAvailability = HttpContext.Current.Session[SessionFields.BookingAvailabilityResponse] as ViatorBookingAvailability;
                    lobjViatorProductRequest.ProductCodeList.Add(lobjViatorBookingAvailabilityRequest.mstrproductCode);
                    lobjViatorProductRequest.BookingDate = lobjBookingAvailability.BookingDate;
                    lobjViatorProductRequest.CurrencyCode = lobjBookingAvailability.currencyCode;
                    lobjViatorProductRequest.AgebandList = lobjViatorBookingAvailabilityRequest.Ageband;
                    string lstrCurrency = lobjInfiModel.GetDefaultCurrency();
                    HttpContext.Current.Session["SearchCurrency"] = lstrCurrency;
                    lobjViatorProductRequest.PointRate = lobjInfiModel.GetProgramRedemptionRate(lstrCurrency, RedemptionCodeKeys.TOUR.ToString(), lobjProgramDefinition.ProgramId);
                    lobjViatorAvailableTourGradeResponse = lobjInfiModel.GetTourGradesAvailability(lobjViatorProductRequest);
                    if (lobjViatorAvailableTourGradeResponse != null && lobjViatorAvailableTourGradeResponse.ViatorTourGrade.Count > 0)
                    {
                        lobjViatorAvailableTourGradeList = new List<ViatorAvailableTourGradeList>();

                        if (lobjViatorAvailableTourGradeResponse.ViatorTourGrade.Count.Equals(1) && lobjViatorAvailableTourGradeResponse.ViatorTourGrade[0].GradeCode.ToUpper().Trim().Equals("DEFAULT"))
                        {
                            lobjViatorAvailableTourGradeList.Add(lobjViatorAvailableTourGradeResponse.ViatorTourGrade[0]);
                            lobjViatorAvailableTourGradeList[0].GradeDescription = lobjProduct.Title;
                            lobjViatorAvailableTourGradeList[0].GradeTitle = lobjProduct.SupplierName;
                        }
                        else
                        {
                            lobjViatorAvailableTourGradeList = lobjViatorAvailableTourGradeResponse.ViatorTourGrade;

                        }
                        HttpContext.Current.Session[SessionFields.TourGradeList] = lobjViatorAvailableTourGradeList;
                        lstrProductResult = JSONSerialization.Serialize(lobjViatorAvailableTourGradeList);
                        lstrTemplateResult = File.ReadAllText(HttpContext.Current.Server.MapPath("Templates/TourGradeListTemplate.html"));
                        lobjListOfData[0] = lstrProductResult;
                        lobjListOfData[1] = lstrTemplateResult;

                        return lobjListOfData;
                    }
                    else
                    {
                        LoggingAdapter.WriteLog("TourGradeList response null");
                        return null;
                    }
                }
                else
                {
                    LoggingAdapter.WriteLog("TourGradeList SessionFields null");
                    return null;
                }

            }
            else
            {
                LoggingAdapter.WriteLog("TourGradeList - lobjProgramDefinition is null");
                return null;
            }


        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("LoadTourGrade :-" + ex.Message + ex.InnerException + "Stack Trace-" + ex.StackTrace);
            return null;
        }
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool SetTourGradeCode(string pstrGradeCode)
    {
        bool IsTour = false;
        List<ViatorAvailableTourGradeList> lobjViatorAvailableTourGradeList = null;
        ViatorAvailableTourGradeList lobjViatorAvailableTourGrade = new ViatorAvailableTourGradeList();

        if (HttpContext.Current.Session[SessionFields.TourGradeList] != null)
        {
            lobjViatorAvailableTourGradeList = new List<ViatorAvailableTourGradeList>();
            lobjViatorAvailableTourGradeList = HttpContext.Current.Session[SessionFields.TourGradeList] as List<ViatorAvailableTourGradeList>;
            lobjViatorAvailableTourGrade = lobjViatorAvailableTourGradeList.Find(obj => obj.GradeCode.Equals(pstrGradeCode));
            HttpContext.Current.Session[SessionFields.SelectedTourGrade] = lobjViatorAvailableTourGrade;
            IsTour = true;
        }
        return IsTour;
    }
}