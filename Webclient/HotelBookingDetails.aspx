﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="HotelBookingDetails.aspx.cs" Inherits="HotelBookingDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="Css/hotel.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function onAcceptArgument() {
            var msg = "";
            $("#CP_ErrorMsgContainer").hide();
            if (($("#chkAcceptAgreements").prop("checked") == true) && ($("#chkCancellationPolicy").prop("checked") == true)) {
                return true;
            } if (($("#chkAcceptAgreements").prop("checked") == false) && ($("#chkCancellationPolicy").prop("checked") == true)) {
                msg = "Accept Terms And Conditions";

            }
            if ($("#chkCancellationPolicy").prop("checked") == false) {
                msg = "Accept cancellation policy";

            }
            if ($("#chkAcceptAgreements").prop("checked") == false) {
                msg = "Accept Terms And Conditions";

            }
            if (msg.length > 0) {
                $("#LoginValidation")[0].innerHTML = msg;
                $("#CP_ErrorMsgContainer").show();
                return false;
            }
            e.preventDefault();
        }
    </script>

    <div class="inrpg">
        <div class="pgcontent ">

            <!--CheckOut Page Starts-->
            <div class="pgcol1 cktinputs">
                <!--StepsPage-->
                <div>
                    <div id="divError" class="ErrorMsgContainer" runat="server">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </div>
                    <div class="checkoutheader">
                        <h1>Personal Details</h1>

                        <div class="passenger-block">
                            <div class="Flight_Ticket_holder">
                                <ul>
                                    <li>
                                        <label>
                                            Title</label>
                                        <div class="cktinputbg">
                                            <asp:DropDownList ID="ddlPersonalTitle" runat="server">
                                                <asp:ListItem Value="3" Selected="True">Title</asp:ListItem>
                                                <asp:ListItem Text="Mr." Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Ms." Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Mrs." Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="ddlPersonalTitle"
                                            Display="Dynamic" ErrorMessage="Enter Title"
                                            CssClass="rptErrorMassage" InitialValue="3"></asp:RequiredFieldValidator>
                                    </li>
                                    <li>
                                        <label>First Name</label>
                                        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ControlToValidate="txtFirstName" CssClass="rptErrorMassage"
                                            ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter First Name" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator runat="server" ID="cusCustom" CssClass="rptErrorMassage" Display="Dynamic"
                                            ControlToValidate="txtFirstName" OnServerValidate="custom_NameValidate" ErrorMessage="Enter only alpha" />
                                    </li>

                                    <li>
                                        <label>Last Name</label>
                                        <asp:TextBox ID="txtLastname" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ControlToValidate="txtLastname" CssClass="rptErrorMassage"
                                            ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Last Name" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator runat="server" ID="CustomValidatorLastName" CssClass="rptErrorMassage"
                                            Display="Dynamic" ControlToValidate="txtLastname" OnServerValidate="custom_NameValidate"
                                            ErrorMessage="Enter only alpha" />
                                    </li>

                                    <li>
                                        <label>City</label>
                                        <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ControlToValidate="txtCity" CssClass="rptErrorMassage"
                                            ID="RequiredFieldValidator4" runat="server" ErrorMessage="Enter City" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </li>
                                    <li>
                                        <label>Country</label>
                                        <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ControlToValidate="txtCountry" CssClass="rptErrorMassage"
                                            ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter Country" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </li>
                                    <li>
                                        <label>Postal Code</label>
                                        <asp:TextBox ID="txtPostalCode" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ControlToValidate="txtPostalCode" CssClass="rptErrorMassage"
                                            ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter P.O. Box / Postal Code"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter Only Digits"
                                            ControlToValidate="txtPostalCode" CssClass="rptErrorMassage" ValidationExpression="^[0-9]+$">
                                        </asp:RegularExpressionValidator>
                                    </li>

                                    <li>
                                        <label>Mobile No</label>
                                        <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="12"> </asp:TextBox>
                                        <asp:RequiredFieldValidator ControlToValidate="txtMobileNo" CssClass="rptErrorMassage"
                                            ID="RequiredFieldValidator7" runat="server" ErrorMessage="Enter Mobile No" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter Only Digits"
                                            ControlToValidate="txtMobileNo" Display="Dynamic" CssClass="rptErrorMassage" ValidationExpression="^[0-9]+$"> </asp:RegularExpressionValidator>
                                    </li>
                                    <li>
                                        <label>Email Address</label>
                                        <asp:TextBox ID="txtEmailID" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ControlToValidate="txtEmailID" ID="RequiredFieldValidator9"
                                            CssClass="rptErrorMassage" runat="server" ErrorMessage="Enter Email" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Email Address not valid"
                                            CssClass="rptErrorMassage" ControlToValidate="txtEmailID" Display="Dynamic" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>

                                    </li>
                                </ul>
                                <div class="clr"></div>
                                <div class="sperqts">
                                    <label>Special Request</label>
                                    <asp:TextBox ID="txtSpecialRequest" runat="server" class="txtarea" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="clr"></div>
                            <div>
                                <input type="checkbox" id="chkAcceptAgreements" />
                                I have read and agreed 
                                         <a href="#" target="_blank">Terms & Conditions</a> and the <a href="#">Booking & Cancellation policy of the respective service provider</a>.
                            </div>
                            <br />
                            <div>
                                <input type="checkbox" id="chkCancellationPolicy" />
                                I agree to redeem
                                        <asp:Label ID="lblTotalCharge" runat="server" Text=""></asp:Label>
                                . I also understand and accept that the redeemed WorldRewardz cannot be
                                        refunded or credited upon cancellation of a hotel booking.
                            </div>
                            <div class="clr"></div>
                            <div class="col-3 fR">
                                <asp:Button ID="btnBook" Text="Proceed To Pay" runat="server" CssClass="button" OnClientClick="return onAcceptArgument()" OnClick="btnBook_Click" />
                            </div>
                            <div id="ErrorMsgContainer" runat="server" class="rptErrorMassage">
                                <div id="LoginValidation">
                                </div>
                            </div>
                            <div style="display: none">
                                <asp:TextBox ID="txtState" runat="server" CssClass="textBoxHotelBooking" Style="height: 1.7vw; width: 52%"></asp:TextBox>
                                <asp:TextBox ID="txtPhoneNo" runat="server" CssClass="textbox TxtBox_Width85"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pgcol2" id="divSearch">
                <div class="widget-prfl" id="MasterUser">
                    <div class="widget-user">
                        <ul id="UserDetails">
                        </ul>
                    </div>
                    <div class="widget-srhpnl">
                        <h3>Itinerary
                        <span class="edit"><a href="HotelResults.aspx">Edit</a> </span>
                            <div class="clr"></div>
                        </h3>
                        <div class="cont">
                            <ul>
                                <li>
                                    <div class="htlchkimg">
                                        <asp:Image ID="imgHotel" runat="server" />
                                    </div>
                                    <div class="htlchcon">
                                        <asp:Label runat="server" class="htlnm" ID="lblHotelName"></asp:Label>
                                        <br />
                                        <asp:Label class="loca" ID="lblAddress" runat="server"></asp:Label>
                                    </div>
                                    <div class="clr"></div>
                                </li>
                                <li class="htlitnry">

                                    <div class="col-1">
                                        <p>
                                            Check-in
                                        </p>
                                        <div class="item-Name">
                                            <asp:Label runat="server" ID="lblCheckinDate" Style="text-transform: uppercase"></asp:Label>
                                        </div>

                                    </div>
                                    <div class="col-2">
                                        <img src="images/time.png" />
                                        <p>
                                            <asp:Label runat="server" ID="lblNoofNights"></asp:Label>

                                            <asp:Label runat="server" ID="lblNoOfAdult"></asp:Label>
                                        </p>
                                    </div>
                                    <div class="col-3">
                                        <p>
                                            Check-out
                                        </p>
                                        <div class="item-Name">
                                            <asp:Label runat="server" ID="lblCheckoutDate" Style="text-transform: uppercase"></asp:Label>
                                        </div>

                                    </div>
                                    <div class="clr">
                                    </div>
                                </li>

                            </ul>

                        </div>
                        <div class="total bgcolGrL">
                           
                            <div class="ftr">
                                <span>Total WorldRewardz</span>
                                <span class="amt">
                                            <asp:Label runat="server" ID="lblTotalMiles" CssClass="amt"></asp:Label>
                                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clr"></div>
        </div>
    </div>




</asp:Content>
