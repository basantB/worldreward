﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CB.IBE.Platform.Car.ClientEntities;
using CB.IBE.Platform.Car.Entities;
using Core.Platform.Member.Entites;
using CB.IBE.Platform.Masters.Entities;
using InfiPlanetModel.Model;
using Core.Platform.MemberActivity.Entities;

public partial class CarVoucher : System.Web.UI.Page
{
    InfiModel lobjInfiModel = new InfiModel();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["CarMakeBookingResponse"] != null && Session["CarMadeBookingRequest"] != null && Session["CarSelected"] != null && Session["CarExtraListResponse"] != null)
            {                
                string lstrRelativePath = this.Page.AppRelativeVirtualPath;
                int lintActivityId = lobjInfiModel.LogActivity(lstrRelativePath.Replace("~", string.Empty).Replace("/", string.Empty), ActivityType.PageLoad);

                CarMakeBookingResponse lobjCarMakeBookingResponse = Session["CarMakeBookingResponse"] as CarMakeBookingResponse;
                CarMakeBookingRequest lobjCarMakeBookingRequest = Session["CarMadeBookingRequest"] as CarMakeBookingRequest;
                Match lobjMatch = Session["CarSelected"] as Match;
                MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
                string lstrMemberId = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;

                CarSearchRequest lobjCarSearchRequest = Session["CarSearchRequest"] as CarSearchRequest;
                CarExtrasListResponse lobjCarExtrasListResponse = Session["CarExtraListResponse"] as CarExtrasListResponse;

                lblMemberName.Text = lobjMemberDetails.LastName;
                lblMembershipReference.Text = lstrMemberId; //lobjMemberDetails.MembershipReference;
                //lblMemberId.Text=lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference; //lobjMemberDetails.MembershipReference;
                lblbookingId.Text = lobjCarMakeBookingResponse.MakeBookingRS.Booking.id;
                lblbookingReferenceNo.Text = lobjCarMakeBookingResponse.BookingReferenceId;
                lblBookingStatus.Text = lobjCarMakeBookingResponse.MakeBookingRS.Booking.status;

                lblcarName.Text = lobjMatch.Vehicle[0].Name;
                lblTransmissionType.Text = lobjMatch.Vehicle[0].automatic;
                lblAirCondition.Text = lobjMatch.Vehicle[0].aircon;

                lblpickUp.Text = lobjMatch.Route[0].PickUp[0].locName;
                lbldropOff.Text = lobjMatch.Route[0].DropOff[0].locName;
                lblDropOffLoc.Text = lobjMatch.Route[0].DropOff[0].locName;
                lblDropOffCity.Text = lobjMatch.Route[0].DropOff[0].city;
                lblDropOffCon.Text = lobjMatch.Route[0].DropOff[0].country;
                lblpickUpDate.Text = lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].day + "/" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].month + "/" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].year;
                lblPickUpTime.Text = lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].hour + ":" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].minute;
                lbldropOffDate.Text = lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].day + "/" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].month + "/" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].year;
                lbldropOffTime.Text = lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].hour + ":" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].minute;

                // lblTotalMiles.Text = Convert.ToString(lobjMatch.Price[0].TotalPoints + lobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[0].Price[0].ExtraTotalPoints);

                lblPickupPlace.Text = lobjMatch.Route[0].PickUp[0].locName;
                lblCityName.Text = lobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].city;
                lblCountry.Text = lobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].country;
                lblDriverTitle.Text = lobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].title;
                lblDriverFName.Text = lobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].firstname;
                lblDriverLName.Text = lobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].lastname;
                lblAddress.Text = lobjMemberDetails.Address1;
                lblPhoneNo.Text = lobjMemberDetails.MobileNumber;


                int lintActivityId1 = lobjInfiModel.LogActivity(string.Format("Car Voucher. Member Id : {0}, Booking Id : {0}, Booking Reference No : {1}, Booking Status : {2}", lstrMemberId, lobjCarMakeBookingResponse.MakeBookingRS.Booking.id, lobjCarMakeBookingResponse.BookingReferenceId, lobjCarMakeBookingResponse.MakeBookingRS.Booking.status), ActivityType.CarBooking);

                //lblMemberAddress.Text = lobjMemberDetails.Address1 + " " + lobjMemberDetails.Address2;
                //lblMemberPhoneNo.Text = lobjMemberDetails.MobileNumber;
                //lblEmail.Text = lobjMemberDetails.Email;
                //lblName.Text = lobjMemberDetails.FirstName + " " + lobjMemberDetails.LastName;


                string lstrPaymentDetails = string.Empty;
                if (lobjCarMakeBookingResponse.BookingPaymentDetails.BookingPaymentBreakageList.Count > 0)
                {
                    string lstrPaymentDetailsHTML = "<td><table width='361' border='0' cellspacing='0' cellpadding='0'align='left'><tr><td style='font-size: 14px; padding-left: 7px;padding-bottom:3px;' width='130' valign='top'>{0}</td><td width='5px' style='font-size: 14px;padding-bottom:3px;'valign='top'>:</td><td style='font-size: 14px;padding-bottom:3px;' valign='middle'>{1}</td></tr></table></td><td><table width='4' border='0' cellspacing='0' cellpadding='0' align='left'><tr><td></td></tr></table></td>";

                    for (int i = 0; i < lobjCarMakeBookingResponse.BookingPaymentDetails.BookingPaymentBreakageList.Count; i++)
                    {
                        if (i.Equals(0))
                        {
                            lstrPaymentDetails = string.Format(lstrPaymentDetailsHTML, lobjCarMakeBookingResponse.BookingPaymentDetails.BookingPaymentBreakageList[i].Currency + " : ", lobjInfiModel.FloatToThousandSeperated(lobjCarMakeBookingResponse.BookingPaymentDetails.BookingPaymentBreakageList[i].Amount));
                        }
                        else
                        {
                            lstrPaymentDetails += string.Format(lstrPaymentDetailsHTML, lobjCarMakeBookingResponse.BookingPaymentDetails.BookingPaymentBreakageList[i].Currency + " : ", lobjInfiModel.FloatToThousandSeperated(lobjCarMakeBookingResponse.BookingPaymentDetails.BookingPaymentBreakageList[i].Amount));
                        }
                    }

                    lstrPaymentDetails = "<table width='726' border='0' cellspacing='0' cellpadding='0' align='left'><tr>" + lstrPaymentDetails + "</tr></table>";
                    PaymentInfo.InnerHtml = lstrPaymentDetails;
                }
            }
            else
            {
                Response.Redirect("Index.aspx");
            }
        }
    }
}