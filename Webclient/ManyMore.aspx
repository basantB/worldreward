﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="ManyMore.aspx.cs" Inherits="ManyMore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
     <% Response.CacheControl = "no-cache"; %>
    <% Response.AddHeader("Pragma", "no-cache"); %>
    <% Response.Expires = -1;%>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script>
        /*tabier scripts*/
        $(document).ready(function () {

            $('.InnerTab >li >a').click(function () {

                $('.InnerTab >li >a').removeClass('active');
                $(this).addClass('active');
                var content = $(this).attr("data-tab");
                $(".mainInnCont").removeClass("active_rewardz");
                $("#" + content).addClass("active_rewardz");
            });
            $("#content aside").hide(); // Initially hide all content
            $("#tabs li:first").attr("id", "current"); // Activate first tab
            $("#content aside:first").fadeIn(); // Show first tab content

            $('#tabs a').click(function (e) {
                e.preventDefault();
                $("#content aside").hide(); //Hide all content
                $("#tabs li").attr("id", ""); //Reset id's
                $(this).parent().attr("id", "current"); // Activate this
                $('#' + $(this).attr('title')).fadeIn(); // Show content for current tab
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#CP_txtBundles").bind('keypress', function (e) {
                return (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) ? false : true;
            })
            $('#CP_txtBundles').change(function () {
                debugger;
                var multval = $('#CP_lblOldPass').text();

                var points = $('#CP_txtBundles').val();

                if (points != "") {
                    $.ajax({
                        url: 'ManyMore.aspx/CalCulateCashBackPoints',
                        type: 'POST',  // or get
                        contentType: 'application/json; charset =utf-8',
                        data: "{'strPoints':'" + points + "','strMultval':'" + multval + "'}",
                        dataType: 'json',
                        success: function (msg) {
                            if (msg.d) {
                                var totalPoints = points * 1000;

                                $('#CP_txtPoints').val(totalPoints);
                                $('#CP_LabelAmount').text(msg.d);
                                //$('#ct_CBI_LabelAmount').text(function (index, value) {
                                //    return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                //});
                                //$('#ct_CBI_txtPoints').val(function (index, value) {
                                //    return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                //});
                                //$('#ct_CBI_txtPoints').digits();
                                $("#ValidateCashBack")[0].innerHTML = "";
                                if (totalPoints == 0) {
                                    $("#ValidateCashBack").show();
                                    $("#ValidateCashBack")[0].innerHTML = "Number of points bundles should be greater than 0";
                                }
                                else {
                                    $("#ValidateCashBack").hide();
                                }
                            }
                            else
                                window.location = "Login.aspx";
                        },
                        error: function (errmsg) {

                        }
                    });
                }
                else {
                    $('#CP_txtPoints').val("");
                    $("#ValidateCashBack").show();
                    $("#ValidateCashBack")[0].innerHTML = "Please enter the number of points bundles you wish to redeem";
                    return false;
                }
            });
        });
        function redirectLocation(url) {

            var win = window.open(url, '_blank');
            win.focus();
            return false;
        };
    </script>
    <script src="js/ManyMoreScript.js" type="text/javascript"></script>
    <script src="js/TransferPoint.js" type="text/javascript"></script>
    <link href="css/utility.css" rel="stylesheet" />
    <style>
        .marginn_text {
            padding: 13px 0px;
            float: left;
        }
    </style>
    
    <div class="banner">
        <div style="max-width: 100%;" class="bx-wrapper">
            <div style="width: 100%; overflow: hidden; position: relative; height: 436px;" class="bx-viewport">
                <ul style="width: auto; position: relative;" class="bxslider" id="Indexbanner">
                    <li>
                        <img src="images/btn-manymore.jpg"></li>
                </ul>
            </div>
        </div>
    </div>

    <div style="text-align: center; margin-top: 10px;">
        <a href="#article" id="dnarr">
            <img style="opacity: .5; height: 50px;" src="images/down-arrow.gif"></a>
    </div>
    <article id="article">
        <div class="wrap">
            <h1>Discover Possibilities. </h1>
            <p>WorldRewardZ touches every moment of your life, bringing value to all that you consider essential, important and memorable. Experience the advantage of WorldRewardZ when you: &#8226; Pay Utility Bills including Mobile, Telephone, Electricity and Water Bills, &#8226; Avail CashBack benefits. &#8226; Contribute to Charity.</p>
            <div class="ManyWrap">
                <ul class="InnerTab">
                    <li><a href="javascript:void(0)" data-tab="utility" class="active">Utility</a></li>
                    <li><a href="javascript:void(0)" data-tab="CashBack">CashBack</a></li>
                    <li><a href="ExtLogin.aspx">Charity</a></li>
                    <li><a href="javascript:void(0)" data-tab="TransferPoints">Transfer Points</a></li>
                    <li><a href="javascript:void(0)" data-tab="PurchasePoints">Purchase Points</a></li>
                </ul>
                
                <div class="mainInnCont active_rewardz" id="utility">
                    <div class="InnCont" style="margin-top: 0px;">
                        <ul id="tabs">
                            <li id="current"><a href="#" title="tab1">MOBILE BILL</a></li>
                            <li id=""><a href="#" title="tab2">TELEPHONE BILL</a></li>
                            <li id=""><a href="#" title="tab3">ELECTRICITY BILL</a></li>
                            <li id=""><a href="#" title="tab4">WATER BILL</a></li>
                        </ul>
                        <div id="content">
                            <aside id="tab1" style="display: block;">
                                <ul class="tabInfo">
                                    <li>
                                        <label>Service Provider</label>
                                        <div class="selectdiv">
                                            <select>
                                                <option>01</option>
                                                <option>02</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        <label>Service Type</label>
                                        <div class="selectdiv">
                                            <select>
                                                <option>01</option>
                                                <option>02</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="immr0">
                                        <label>Consumer Number</label>
                                        <div class="selectdiv">
                                            <select>
                                                <option>01</option>
                                                <option>02</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li style="display: none">
                                        <label>Balance Amount (AED)</label>
                                        <input type="text" name="">
                                    </li>
                                    <li style="display: none">
                                        <label>WorldRewardZ Points</label>
                                        <input type="text" name="">
                                    </li>
                                    <li style="display: none">
                                        <label>Please enter WorldRewardZ Points for Payment</label>
                                        <input type="text" name="">
                                    </li>
                                    <li class="immr0" style="display: none">
                                        <label>Amount (AED)</label>
                                        <input type="text" name="" class="InputGrey">
                                    </li>
                                </ul>
                            </aside>
                            <aside id="tab2" style="display: none;">
                                <p>Telephone Bill</p>
                            </aside>
                            <aside id="tab3" style="display: none;">
                                <p>Electricity Bill</p>
                            </aside>
                            <aside id="tab4" style="display: none;">
                                <p>Water Bill</p>
                            </aside>
                        </div>
                        <div class="btmInfo">
                            <a href="#" class="btnRed">Continue</a>
                            <%--<asp:ImageButton ImageUrl="~/Images/Confirm.png" onmouseover="this.src='Images/Confirm_Hover.png'"
                onmouseout="this.src='Images/Confirm.png'" runat="server" ID="btnSave" OnClientClick="var varReturn=GetCashBack();event.returnValue=varReturn; (event.preventDefault) ? event.preventDefault() : event.returnValue = false ;return varReturn;" />--%>
                            <br class="clr">
                        </div>
                    </div>
                </div>
                <!--end of utility-->

                <div class="mainInnCont" id="CashBack">
                    <div class="InnCont">
                        <div class="leftCont">
                            <p>Please select the following multiplier to redeem points:</p>
                            <div class="selDiv">

                                <ul class="tabInfo">

                                    <li style="width: auto;">

                                        <span style="float: left; padding: 13px 0px;">1000 Points X </span>
                                        <asp:Label ID="lblOldPass" CssClass="disnone display_hide" runat="server" Text="1000"></asp:Label>
                                        <span style="float: left;">
                                            <asp:TextBox ID="txtBundles" runat="server"></asp:TextBox></span>
                                        <span>
                                            <asp:Label ID="Label1" CssClass="Left_Menu_LinkBtnStyle marginn_text" runat="server" Text="&nbsp;Bundle(s)="></asp:Label></span>
                                    </li>
                                    <li class="immr0" style="width: auto;">
                                        <span style="float: left;">
                                            <asp:TextBox ID="txtPoints" MaxLength="20" runat="server" CssClass="TxtBox_Width41 Banner_textboxStyle"
                                                Enabled="false"></asp:TextBox></span>
                                        <span style="float: left; padding: 13px 0px;">
                                            <asp:Label ID="Label2" CssClass="Left_Menu_LinkBtnStyle" runat="server" Text="&nbsp;Points"></asp:Label></span>
                                    </li>
                                </ul>



                            </div>
                            <div class="chkBox">
                                <asp:CheckBox ID="chkCashBack" runat="server" />
                                Your Cash Back value is &nbsp;<asp:Label ID="LabelAmount" runat="server" Text="0"></asp:Label>&nbsp;AED
							
                            </div>
                            <asp:Button CssClass="btnRed" Text="Confirm" runat="server" ID="btnSave" OnClientClick="var varReturn=GetCashBack();event.returnValue=varReturn; (event.preventDefault) ? event.preventDefault() : event.returnValue = false ;return varReturn;" />

                            <div class="notification">
                                <div class="ErrorMsgContainer" style="width: auto !important" id="ValidateCashBack">
                                </div>

                            </div>
                        </div>
                        <div class="rightCont">
                            <div class="noteBlk">
                                <h2>Please Note:</h2>
                                <ul>
                                    <li>Minimum ‘1,000 Points’can be purchased.</li>
                                    <li>Maximum ‘10,000 Points’can be purchase.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!---end of mainInnCont-->

                <div class="mainInnCont" id="Charity">
                    <div class="InnCont">
                        <ul class="tabInfo">
                            <li>
                                <label>Service Type</label>
                                <div class="selectdiv">
                                    <select>
                                        <option>01</option>
                                        <option>02</option>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <label>Amount (AED)</label>
                                <input type="text" name="">
                            </li>
                            <li class="immr0">
                                <label>WorldRewardZ Points</label>
                                <input type="text" class="InputGrey" name="">
                            </li>
                        </ul>
                        <div class="btmInfo">
                            <a href="#" class="btnRed">Continue</a>
                            <br class="clr">
                        </div>
                    </div>
                </div>
                <!--end of Charity-->

                <div class="mainInnCont" id="TransferPoints">
                    <div class="InnCont">
                        <p>Transfer your points to your favourite loyalty programme to include Global Hotel chains, Airlines and more!</p>
                        <p>Simply select your preferred partner, enter your partner programme details and enter the number of points you wish to transfer.</p>
                        <div class="transfCont">
                            <ul>
                                <li>
                                    <label>Member Id</label>
                                    <input autocomplete="off" type="text"  id="txtMemberId"  runat="server" onchange="var retvalue = LoginValidation()" />
                                 </li>

                                 <li>
                                    <label>Customer Name</label>
                                    <input type="text" name="" id="txtrecipientName"/>
                                </li>
                                <li>
                                    <label>Total Partner Points that will be transferred</label>
                                     <input autocomplete="off" type="text"  id="txtTransferPoint" onkeyup="return calcTransferPAmount();" onkeydown="return (!(event.keyCode>=65) && event.keyCode!=32);" runat="server" /></li>
                                </li>
                                <li>
                                    <label>Amount(AED)</label>
                                    <input type="text" name="" id="txtTransferConvAmt">
                                </li>
                                <%--<li>
                                    <label>Please enter your Frequent Flyer Number</label>
                                    <input type="text" name="">
                                </li>--%>
                                <li class="label" style="width: 100%;margin:15px 0px;">Note: Minimum number of points that can be transferred is 500 and Maximum number of points that can be transferred is 100,000.</li>
                              </ul>
                           
                            <%--<p class="txtNote"> </p>--%>
                            <input type="button" class="btnRed" value="Points Transfer" onclick="var retvalue = TransferPoints(); event.returnValue = retvalue; return retvalue;" />
                            <%--<a href="#" class="btnRed">Submit</a>--%>
                             <div id="LoginValidation" class="notification" style="float:left;width: 75%;">
                             </div>
                        </div>
                         

                    </div>
                </div>
                <!--end of Charity-->

                <div class="mainInnCont" id="PurchasePoints">
                     <div class="InnCont">
                         <div class="leftCont">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="">
                        <ContentTemplate>
                           
                                <%--<div class="leftCont">
						<p>Please select the following multiplier to purchase points:</p>
						<div class="selDiv">
							<span>1000 WorldRewardZ Points  X </span>
							<div class="selectdiv">
								<select>
									<option>01</option>
									<option>02</option>
								</select>
							</div>
							<span>= 1000 WorldRewardZ Points.</span>
						</div>
						<div class="chkBox">
							<input type="checkbox"> 
							<label>Total Purchase Point Value is 400 AED.</label>
						</div>
						<a href="#" class="btnRed">Confirm</a>
						<div class="notification">Your Account will be credited by the bank with 1500 points.</div>
					</div>--%>
                                <%--<div class="rightCont">
						<div class="noteBlk">
							<h2>Please Note:</h2>
							<ul>
								<li>Minimum ‘1,000 Points’can be purchased.</li>
								<li>Maximum ‘10,000 Points’can be purchase.</li>
							</ul>
						</div>
					</div>--%>

                                
                                    <p style="margin-bottom:15px;">Please select the following multiplier to purchase points:</p>
                                    <div class="selDiv" style="margin-bottom:15px;">
                                        <ul class="tabInfo">
                                            <li style="width: auto;">
                                                <span style="float: left; padding: 13px 0px;">1000 WorldRewardZ Points X </span>
                                                <asp:Label ID="Label3" CssClass="disnone display_hide" runat="server" Text="1000"></asp:Label>
                                                <span style="float: left;margin-left: 5px;margin-top: 12px;">

                                                    <asp:DropDownList ID="ddlPointBlocks" runat="server" CssClass="input2" OnSelectedIndexChanged="ddlPointBlocks_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                        <asp:ListItem Value="1">1</asp:ListItem>
                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                        <asp:ListItem Value="3">3</asp:ListItem>
                                                        <asp:ListItem Value="4">4</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%-- <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></span>--%>
                                                    <span></span></li>
                                            <li class="immr0" style="width: auto;">
                                                <span style="float: left;margin-left: 5px;margin-top: 11px;margin-right:5px">=
                                            <asp:Label ID="TextBox2" MaxLength="20" runat="server" CssClass="TxtBox_Width41 Banner_textboxStyle"></asp:Label></span>
                                                <span style="float: left; padding: 13px 0px;">
                                                    <asp:Label ID="lblTotalPoints" CssClass="Left_Menu_LinkBtnStyle" runat="server" Text="&nbsp;Points"></asp:Label></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="chkBox">
                                        <asp:Label ID="lblAmount" runat="server" Text="0" CssClass="disnone display_hide"></asp:Label>&nbsp; 
                                Total Purchase Point Value is &nbsp;<asp:Label ID="lblAEDAmount" runat="server" Text="0"></asp:Label>&nbsp;AED
                                    </div>
                                    
                                
                             </ContentTemplate>
                    </asp:UpdatePanel>
                             <div>
                                <asp:Button CssClass="btnRed" runat="server" ID="btnSubmit" Text="Confirm"
                                        OnClick="btnSubmit_Click" />
                                    <div class="notification">
                                        <div class="ErrorMsgContainer" style="width: auto !important" id="ValidatePurchasePoints">
                                        </div>
                                    </div>
                                    </div>
                         </div>
                                
                                <div class="rightCont">
                                    <div class="noteBlk">
                                        <h2>Please Note:</h2>
                                        <ul>
                                            <li>Minimum ‘1,000 Points’can be purchased.</li>
                                            <li>Maximum ‘10,000 Points’can be purchase.</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                       
                </div>

                <div class="btmInfo">

                    <br class="clr">
                </div>
            </div>
        </div>

    </article>
</asp:Content>

