﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CB.IBE.Platform.Entities;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using System.IO;
using CB.IBE.Platform.Car.ClientEntities;
using CB.IBE.Platform.Car.Entities;
using CB.IBE.Platform.Masters.Entities;
using System.Globalization;
using Core.Platform.MemberActivity.Entities;
using InfiPlanetModel.Model;

public partial class CarList : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CarSearchRequest"] != null && Session["Cars"] != null)
        {
            Session["CarRefId"] = null;
            Session["CarSelected"] = null;
            Session["CarExtraListResponse"] = null;
            Session["CarMakeBookingRequest"] = null;
            Session["CarMakeBookingResponse"] = null;

            if (!IsPostBack)
            {
                Session["CarSearchPaymode"] = PaymentType.Points;

                InfiModel lobjModel = new InfiModel();
                string lstrRelativePath = this.Page.AppRelativeVirtualPath;
                int lintActivityId = lobjModel.LogActivity(lstrRelativePath.Replace("~", string.Empty).Replace("/", string.Empty), ActivityType.PageLoad);
            }
        }
        else
        {
            Response.Redirect("Index.aspx");
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SetCarTemplate()
    {
        CarSearchResponse lobjCarSearchResponse = new CarSearchResponse();
        List<Match> car = new List<Match>();
        string[] lobjListOfData = new string[5];
        FilterRange lobjFilterRange = new FilterRange();
        string lstrResult = string.Empty;

        if (HttpContext.Current.Session["Cars"] != null)
        {
            lobjCarSearchResponse = HttpContext.Current.Session["Cars"] as CarSearchResponse;
            car = lobjCarSearchResponse.SearchResponse.MatchList[0].Match.ToList();
        }
        CarSearchRequest lobjCarSearchRequest = HttpContext.Current.Session["CarSearchRequest"] as CarSearchRequest;

        lstrResult = JSONSerialization.Serialize(car);
        List<string> lobjListOfCarType = new List<string>();
        List<string> lobjListOfCarClass = new List<string>();
        List<string> lobjListOfCarTransmission = new List<string>();
        List<int> lobjListOfTotalMiles = new List<int>();
        for (int i = 0; i < lobjCarSearchResponse.SearchResponse.MatchList[0].Match.Count(); i++)
        {
            Match lobjMatch1 = lobjCarSearchResponse.SearchResponse.MatchList[0].Match[i];
            lobjListOfTotalMiles.Add(Convert.ToInt32(lobjMatch1.Price[0].TotalBaseFare));
            lobjListOfCarTransmission.Add(lobjMatch1.Vehicle[0].automatic);
            lobjListOfCarClass.Add(lobjMatch1.Vehicle[0].aircon);
            lobjListOfCarType.Add(CarClassDescription.CarClass.Find(lobjMatch1.Vehicle[0].group));
        }
        lobjFilterRange.MaxRewards = lobjListOfTotalMiles.Max();
        lobjFilterRange.MinRewards = lobjListOfTotalMiles.Min();
        lobjFilterRange.CarTransmission = lobjListOfCarTransmission.Distinct().ToList();
        lobjFilterRange.CarClass = lobjListOfCarClass.Distinct().ToList();
        lobjFilterRange.CarType = lobjListOfCarType.Distinct().ToList();

        string lstrTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("Templates/CarTemplate.htm"));

        lobjListOfData[0] = lstrTemplate;
        lobjListOfData[1] = lstrResult;
        lobjListOfData[2] = JSONSerialization.Serialize(lobjFilterRange);
        //lobjListOfData[3] = "<span>Cars in </span>" + lobjCarSearchResponse.SearchResponse.MatchList[0].Match[0].Route[0].PickUp[0].locName;
        //lobjListOfData[3] = "<b>Your car Search for : Pick Up</b> : " + lobjCarSearchResponse.SearchResponse.MatchList[0].Match[0].Route[0].PickUp[0].locName.ToString()
        //                          + ", " + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].day + "/" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].month + "/" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].year
        //                          + " - <b>Drop Off:</b> " + lobjCarSearchResponse.SearchResponse.MatchList[0].Match[0].Route[0].DropOff[0].locName.ToString()
        //                          + ", " + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].day + "/" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].month + "/" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].year;


        lobjListOfData[3] = "<li class='frt'><div class='us-from'><p>Pick Up</p><h2>" + lobjCarSearchResponse.SearchResponse.MatchList[0].Match[0].Route[0].PickUp[0].locName + "</h2></div><div class='clr'></div><p class='us-date'>" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].day + "/" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].month + "/" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].year + "&nbsp;" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].hour + ":" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].minute + "</p></li>";
        lobjListOfData[3] += "<li><div class='us-from'><p>Drop Off</p><h2>" + lobjCarSearchResponse.SearchResponse.MatchList[0].Match[0].Route[0].DropOff[0].locName + "</h2></div><div class='clr'></div><p class='us-date'>" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].day + "/" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].month + "/" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].year + "&nbsp;" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].hour + ":" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].minute + "</p></li>";
        lobjListOfData[3] += "<li class='lst'><h1>" + lobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].country + "</h1></li>";

        lobjListOfData[4] = "<li class='frt'><div class='us-from'><p>Pick Up</p><h2>" + lobjCarSearchResponse.SearchResponse.MatchList[0].Match[0].Route[0].PickUp[0].locName + "</h2></div><div class='clr'></div><p class='us-date'>" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].day + "/" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].month + "/" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].year + "&nbsp;" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].hour + ":" + lobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].minute + "</p></li>";
        lobjListOfData[4] += "<li class='frt'><div class='us-from'><p>Drop Off</p><h2>" + lobjCarSearchResponse.SearchResponse.MatchList[0].Match[0].Route[0].DropOff[0].locName + "</h2></div><div class='clr'></div><p class='us-date'>" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].day + "/" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].month + "/" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].year + "&nbsp;" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].hour + ":" + lobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].minute + "</p></li>";
        //lobjListOfData[4] += "<li class='lst'><h2>" + lobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].country + "</h2></li>";


        return lobjListOfData;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool IsMemberLoggedIn(string pstrRefId)
    {
        InfiModel lobjModel = new InfiModel();
        lobjModel.LogActivity(string.Format("Car selected for booking"), ActivityType.CarBooking);
        HttpContext.Current.Session["CarRefId"] = pstrRefId;
        return HttpContext.Current.Session["MemberDetails"] != null;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] ModifySearchData()
    {
        string[] lobjListOfData = new string[4];
        string lstrModifySearchRequest = string.Empty;
        string lstrModifySearchResponse = string.Empty;
        string lstrDepartDate = string.Empty;
        string lstrArrivalDate = string.Empty;

        CarSearchRequest lobjCarSearchRequest = HttpContext.Current.Session["CarSearchRequest"] as CarSearchRequest;
        CarSearchResponse lobjCarSearchResponse = HttpContext.Current.Session["Cars"] as CarSearchResponse;

        lstrModifySearchRequest = JSONSerialization.Serialize(lobjCarSearchRequest);
        lstrModifySearchResponse = JSONSerialization.Serialize(lobjCarSearchResponse);

        string strFromTime = HttpContext.Current.Session["hdnddlFromTime"].ToString();
        string strToTime = HttpContext.Current.Session["hdnddlToTime"].ToString();

        lobjListOfData[0] = lstrModifySearchRequest;
        lobjListOfData[1] = lstrModifySearchResponse;
        lobjListOfData[2] = strFromTime;
        lobjListOfData[3] = strToTime;

        return lobjListOfData;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string GetCarTermsCondtion(string pstrrefId)
    {
        if (HttpContext.Current.Session["Cars"] != null)
        {
            //return JSONSerialization.Serialize(lobjMAFModel.GetCarVehiclDetails((HttpContext.Current.Session["Cars"] as CarAvailabilityResponse).VehAvailRSCore.VehVendorAvails.VehVendorAvail.CarVehicles, pstrrefId));
            InfiModel lobjModel = new InfiModel();

            CarRentalTermsRequest lobjCarRentalTermsRequest = new CarRentalTermsRequest();
            List<Location> lobjLocationList = new List<Location>();
            Location lobjLocation = new Location();
            lobjLocation.id = pstrrefId;
            lobjLocationList.Add(lobjLocation);
            RentalTermsRQ lobjRentalTermsRQ = new RentalTermsRQ();
            lobjRentalTermsRQ.Location = lobjLocationList.ToArray();
            lobjCarRentalTermsRequest.RentalTermsRQ = lobjRentalTermsRQ;
            CarRentalTermsResponse lobjCarRentalTermsResponse = new CarRentalTermsResponse();
            lobjCarRentalTermsResponse = lobjModel.CarRentalTermsResponse(lobjCarRentalTermsRequest);
            //HttpContext.Current.Session["TermsAndConditions"] = lobjCarRentalTermsResponse;
            return JSONSerialization.Serialize(lobjCarRentalTermsResponse);
        }
        else
        {
            return string.Empty;
        }
    }
}