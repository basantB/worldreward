﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="NoResultFound.aspx.cs" Inherits="NoResultFound" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <div style="min-height:350px;">
        <div runat="server" id="divBOOKINGFAILED" style="border: none; text-align: center;
            font-size: 18px; padding:18% 0 5% 0; width:80%; display: none; margin:auto;">
            "Sorry we could not get confirmation PNR OR TicketNumber From Airline
            <br />
            <asp:LinkButton ID="btnRedirect" runat="server" Text="Please try booking again" PostBackUrl="~/Travel.aspx" />
        </div>
        <div runat="server" id="divRESULTNOTFOUND" style="border: none; text-align: center;
            font-size: 18px; padding:18% 0 5% 0; width:80%; display: none; margin:auto;">
            WorldRewardZ could not process your request,
            <asp:LinkButton ID="LinkButton1" runat="server" Text="please try again" PostBackUrl="~/Travel.aspx" />
            changing the travel dates. Thank you!
        </div>
        <div runat="server" id="divHotelNoresult" style="border: none; text-align: center;
            font-size: 18px; padding:18% 0 5% 0; width:80%; display: none; margin:auto;">
            WorldRewardZ could not process your request,
            <asp:LinkButton ID="LinkButton2" runat="server" Text="please try again" PostBackUrl="~/Travel.aspx" />
            after some time. Thank you!
        </div>
    </div>
    <div class="clr"></div>
</asp:Content>
