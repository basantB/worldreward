﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Framework.Integrations.Hotels.Entities;
using CB.IBE.Platform.Hotels.ClientEntities;
using Core.Platform.Member.Entites;
using Core.Platform.OTP.Entities;
using CB.IBE.Platform.Masters.Entities;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Booking.Entities;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;
using System.Text.RegularExpressions;
using InfiPlanetModel.Model;
using Framework.EnterpriseLibrary.Adapters;

public partial class HotelBookingDetails : System.Web.UI.Page
{
    InfiModel lobjModel = new InfiModel();
    public int count = 0;
    static string lstrCurrency = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["HotelSelected"] = "HotelBookingDetails.aspx?hotelid=" + Request.QueryString["hotelid"].ToString() + "&roomtypecode=" + Request.QueryString["roomtypecode"];
            if (Session["MemberDetails"] != null)
            {
                if (Session["hotels"] != null)
                {
                    HotelSearchResponse lobjSearchResponse = Session["hotels"] as HotelSearchResponse;
                    HotelSearchResponse lobjSelectedSearchResponse = this.GetSelectedHotel(lobjSearchResponse);
                    Hotel lobjHotel = lobjSelectedSearchResponse.SearchResponse.hotels.hotel[0];
                    MemberDetails lobjMemberDetails = new MemberDetails();
                    lobjMemberDetails = Session["MemberDetails"] as MemberDetails;

                    lstrCurrency = lobjModel.GetDefaultCurrency();
                    Session["MemberMiles"] = Convert.ToString(lobjModel.CheckAvailbility(lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency));
                    if (Convert.ToDouble(Session["MemberMiles"]) >= Convert.ToDouble(lobjHotel.roomrates.RoomRate[0].TotalPoints))
                    {
                        divError.Style.Add("display", "none");
                        lblError.Text = "";
                        btnBook.Enabled = true;

                    }
                    else
                    {
                        divError.Style.Add("display", "block");
                        double ldblAmount = lobjHotel.roomrates.RoomRate[0].TotalPoints;
                        lblError.Text = "You have insufficient Reward Points to book this hotel. <a href='PurchasePoints.aspx' style='color:blue'>Click Here</a> To purchase points.";

                        btnBook.Enabled = false;
                    }
                    lblTotalCharge.Text = lobjModel.IntToThousandSeperated(lobjHotel.roomrates.RoomRate[0].TotalPoints);
                    lblHotelName.Text = lobjHotel.basicinfo.hotelname;
                    lblAddress.Text = lobjHotel.basicinfo.address + " - " + lobjHotel.basicinfo.city + ",  " + lobjHotel.basicinfo.state + ", " + lobjHotel.basicinfo.country + "  " + lobjHotel.basicinfo.countrycode;
                    string strbaseurl = lobjSearchResponse.SearchResponse.baseurl;
                    if (strbaseurl.Contains("http://www.cleartrip.com"))
                    {
                        imgHotel.ImageUrl = lobjSearchResponse.SearchResponse.baseurl + lobjHotel.basicinfo.thumbnailimage;
                    }
                    else
                    {
                        imgHotel.ImageUrl = "http://www.cleartrip.com" + lobjSearchResponse.SearchResponse.baseurl + lobjHotel.basicinfo.thumbnailimage;
                    }
                    //lblCurrency.Text = " " + lstrCurrency + " Points ";
                    HotelSearchRequest lobjSearchRequest = Session["SearchDetails"] as HotelSearchRequest;
                    lblTotalMiles.Text = lobjModel.IntToThousandSeperated(lobjHotel.roomrates.RoomRate[0].TotalPoints);
                    DateTime Chkin = Convert.ToDateTime(lobjSearchResponse.SearchResponse.searchcriteria.checkindate);
                    lblCheckinDate.Text = Chkin.ToString("MMM dd");
                    DateTime ChkOut = Convert.ToDateTime(lobjSearchResponse.SearchResponse.searchcriteria.checkoutdate);
                    lblCheckoutDate.Text = ChkOut.ToString("MMM dd");

                    lblNoofNights.Text = lobjSearchResponse.SearchResponse.searchcriteria.numberofnights + "Night(s)";

                    int TotalAdult = 0;
                    string[] arrayAdultPerRoom = lobjSearchRequest.SearchRequest.AdultPerRoom.Split(',');
                    for (int i = 0; i < arrayAdultPerRoom.Count(); i++)
                    {
                        TotalAdult = TotalAdult + Convert.ToInt32(arrayAdultPerRoom[i]);
                    }
                    int TotalChild = 0;
                    string[] arrayChildPerRoom = lobjSearchRequest.SearchRequest.ChildrenPerRoom.Split(',');
                    for (int i = 0; i < arrayChildPerRoom.Count(); i++)
                    {
                        TotalChild = TotalChild + Convert.ToInt32(arrayChildPerRoom[i]);
                    }


                    if (TotalChild.Equals(0))
                        lblNoOfAdult.Text = Convert.ToString(TotalAdult) + "Adult";
                    else
                        lblNoOfAdult.Text = Convert.ToString(TotalAdult) + "Adult<br/>" + Convert.ToString(TotalChild) + "Child";

                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

    }

    public HotelSearchResponse GetSelectedHotel(HotelSearchResponse pobjSearchResponse)
    {
        Hotel lobjHotel = new Hotel();
        HotelSearchResponse lobjSearchResponse = new HotelSearchResponse();
        //HotelSearchResponse lobjSearchResponse = (HotelSearchResponse)lobjSearchResponse1.Clone();
        if (Request.QueryString["hotelid"] != null && Request.QueryString["hotelid"].ToString() != "" && Request.QueryString["roomtypecode"] != null && Request.QueryString["roomtypecode"].ToString() != "")
        {
            lobjHotel = pobjSearchResponse.SearchResponse.hotels.hotel.Where(lobjSelectedHotel => lobjSelectedHotel.hotelid.Equals(Request.QueryString["hotelid"].ToString())).FirstOrDefault();
            RoomRate lobjRoom = lobjHotel.roomrates.RoomRate.Where(lobjRoomRate => lobjRoomRate.roomtype.roomtypecode.Equals(Request.QueryString["roomtypecode"])).FirstOrDefault();
            List<RoomRate> lobjListOfRoomRate = new List<RoomRate>();
            lobjListOfRoomRate.Add(lobjRoom);
            RoomRates lobjRoomRates = new RoomRates();
            lobjRoomRates.RoomRate = lobjListOfRoomRate.ToArray();
            lobjHotel.roomrates = lobjRoomRates;
            Framework.Integrations.Hotels.Entities.Hotels lobjHotels = new Framework.Integrations.Hotels.Entities.Hotels();
            List<Hotel> lobjListOfHotel = new List<Hotel>();
            lobjListOfHotel.Add(lobjHotel);
            lobjHotels.hotel = lobjListOfHotel.ToArray();
            lobjSearchResponse.SearchResponse.hotels = lobjHotels;
            lobjSearchResponse.SearchResponse.searchcriteria = pobjSearchResponse.SearchResponse.searchcriteria;
        }
        return lobjSearchResponse;
    }
    public Customer CustomerInfo(string pstrTitle, string pstrFirstName, string pstrLastName, string pstrCity, string pstrState, string pstrAddress1, string pstrAddress2, string pstrCountry, string pstrPostalCode, string pstrPhoneNo, string pstrMobileNo, string pstrEmailID)
    {
        InfiModel lobjModel = new InfiModel();
        Customer lobjCustomer = lobjModel.CustomerInfo(pstrTitle, pstrFirstName, pstrLastName, pstrCity, pstrState, pstrAddress1, pstrAddress2, pstrCountry, pstrPostalCode, pstrPhoneNo, pstrMobileNo, pstrEmailID);
        return lobjCustomer;

    }
    protected void custom_NameValidate(object sender, ServerValidateEventArgs e)
    {
        Regex r = new Regex("^[a-zA-Z]+$");
        if (r.IsMatch(e.Value))
            e.IsValid = true;
        else
            e.IsValid = false; ;
    }
    protected void btnBook_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Session["BookingFlag"] = ServiceType.HOTEL;
            if (Session["MemberDetails"] != null && Session["Hotels"] != null && Session["SearchDetails"] != null)
            {
                MemberDetails lobjMemberDetails = new MemberDetails();
                lobjMemberDetails = Session["MemberDetails"] as MemberDetails;

                HotelSearchResponse lobjSearchResponse = Session["Hotels"] as HotelSearchResponse;
                HotelSearchResponse lobjSelectedSearchResponse = this.GetSelectedHotel(lobjSearchResponse);
                Hotel lobjHotel = lobjSelectedSearchResponse.SearchResponse.hotels.hotel[0];
                lobjHotel.SpecialRequest = txtSpecialRequest.Text.ToString();
                lobjSelectedSearchResponse.SearchResponse.hotels.hotel[0] = lobjHotel;
                Customer lobjCustomer = new Customer();
                lobjCustomer = this.CustomerInfo(ddlPersonalTitle.SelectedItem.Text, txtFirstName.Text.ToString(), txtLastname.Text.ToString(), txtCity.Text.ToString(), txtState.Text.ToString(), txtCity.Text.ToString(), txtState.Text.ToString(), txtCountry.Text.ToString(), txtPostalCode.Text.ToString(), txtPhoneNo.Text.ToString(), txtMobileNo.Text.ToString(), txtEmailID.Text.ToString());
                Session["CustomerDetails"] = lobjCustomer;
                Session["Hotels"] = null;
                Session["BookedHotel"] = lobjSelectedSearchResponse;
                Session["BookingFlag"] = ServiceType.HOTEL;

                ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();
                //List<RedemptionKeys> lobjRedemptionKeys = new List<RedemptionKeys>();
                //lobjRedemptionKeys = lobjModel.GetAllRedemptionKeys(lobjProgramDefinition.ProgramId);
                int ThreshouldValue = 1000000000;

                //ThreshouldValue = lobjRedemptionKeys.Find(lobj => lobj.RedemptionCode.Equals(RedemptionCodeKeys.HOT.ToString()) && lobj.Currency.Equals(lstrCurrency)).OTPThreshold;
                int lintTotalPoints = Convert.ToInt32(lobjSelectedSearchResponse.SearchResponse.hotels.hotel[0].roomrates.RoomRate[0].TotalPoints);
                float lftAmount = Convert.ToSingle(lobjSelectedSearchResponse.SearchResponse.hotels.hotel[0].roomrates.RoomRate[0].BaseAmount);

                List<RedemptionDetails> lobjListOfRedemptionDetails = new List<RedemptionDetails>();
                RedemptionDetails lobjRedemptionDetails = new RedemptionDetails();
                lobjRedemptionDetails.Currency = lstrCurrency;
                lobjRedemptionDetails.DisplayCurrency = lobjModel.CurrencyDisplayText(lstrCurrency);
                lobjRedemptionDetails.Points = lintTotalPoints;
                lobjRedemptionDetails.RelationReference = lobjMemberDetails.MemberRelationsList[0].RelationReference;
                lobjRedemptionDetails.Amount = lftAmount;

                lobjListOfRedemptionDetails.Add(lobjRedemptionDetails);
                Session["RedemptionDetails"] = lobjListOfRedemptionDetails;

                if (ThreshouldValue <= lintTotalPoints && !ThreshouldValue.Equals(-1))
                {
                    bool Status = false;
                    OTPDetails lobjOTPDetails = new OTPDetails();
                    lobjOTPDetails.UniquerefID = lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.HOTELREVIEWNCONFIRM;
                    lobjOTPDetails.OtpType = Convert.ToString(OTPEnumTypes.HOTELREVIEWNCONFIRM);
                    Status = lobjModel.GenerateReviewnConfirmOTP(lobjOTPDetails, lobjMemberDetails);

                    if (Status)
                    {
                        lobjModel.LogActivity(string.Format(ActivityConstants.ReviewConfirmOTP, "HOTEL", lobjMemberDetails.NationalId, "Success"), ActivityType.ActivationOTPSuccess);
                        Response.Redirect("ValidateOTP.aspx?flag=Hotel");
                    }
                    else
                    {
                        lobjModel.LogActivity(string.Format(ActivityConstants.ReviewConfirmOTP, "HOTEL", lobjMemberDetails.NationalId, "Failed"), ActivityType.ActivationOTPFailed);
                        Response.Redirect("BookingFailure.aspx");

                    }
                }
                else
                {
                    Response.Redirect("PaymentConfirmation.aspx", false);
                }

            }
            else
            {
                Response.Redirect("BookingFailure.aspx");
            }
        }
    }

}