﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AirReviewAndConfirm.aspx.cs"
    Inherits="AirReviewAndConfirm" MasterPageFile="~/InfiPlanetMaster.master" %>

<%@ Register Src="~/UserControl/UCCTItinerayDetails.ascx" TagName="ItineraryDetails"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="Css/FlightList.css" rel="Stylesheet" type="text/css" />
    <link href="css/lounge.css" rel="stylesheet" />
    <style>
          
    </style>
    <script type="text/javascript" language="javascript">
        function onAcceptArgument() {
            var msg = "";
            if ((document.getElementById('CP_chkAcceptAgreements').checked == true) && (document.getElementById('CP_chkAcceptPayMiles').checked == true)) {
                $("#CP_btnBookNow").hide();
                return true;
            }
            if ((document.getElementById('CP_chkAcceptAgreements').checked == false) && (document.getElementById('CP_chkAcceptPayMiles').checked == false)) {
                msg = "Accept Terms And Conditions and agree to pay InfiMiles";
            }
            if (document.getElementById('CP_chkAcceptAgreements').checked == false) {
                msg = "Accept Terms And Conditions";
            }
            if (document.getElementById('CP_chkAcceptPayMiles').checked == false) {
                msg = "Accept agree to pay InfiMiles";
            }
            if (msg.length > 0) {
                $("#errorDiv")[0].innerHTML = msg;
                $("#errorDiv").show();
                return false;
            }
        }

    </script>
    <div class="inrpg">
        <div class="pgcontent">
            <div class="reviewConfirm">
                <!--StepsPage-->
                <div id="divError" runat="server" class="ErrorMsgContainer">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </div>
                   
                        <h1>Review & Confirm</h1>
                        <div class="dtls-tbls">
                            <uc:ItineraryDetails ID="ucItinarary" runat="server" />
                            <div class="mt15 checkboxs" style="padding: 0.5% 2% 0;">
                                <ul>
                                    <li>
                                        <span style="float:left;"><input id="chkAcceptAgreements" type="checkbox" value="rewards points" runat="server" /></span>
                                        <span style="float:left; width:97%;margin-bottom: 9px;">I have read and agreed to World Rewardz Program <a href="TermsandConditions.aspx"
                                        target="_blank">Terms & Conditions</a> and the <a href="BookingPolicy.aspx"
                                            target="_blank">Booking
                                            & Cancellation policy</a> of the respective service provider</span>
                                    </li>
                                    <li>
                                        <span style="float:left;"><input id="chkAcceptPayMiles" type="checkbox" value="rewards points" runat="server" /></span>
                                       <span style="float:left; width:97%;margin-bottom: 9px;"> I agree to redeem
                                    <asp:Label ID="lblTotalPoints" runat="server" />
                                        World Rewardz. I also understand and accept that the redeemed World Rewardz cannot be refunded
                                    or credited upon cancellation of a flight booking.</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                        <br />
                        <div class="ErrorMsgContainer" id="errorDiv" style="display: none">
                        </div>
                        <div class="clr">
                        </div>
                        <div style="float: right; margin: 10px; padding: 0 2%;">
                            <asp:Button ID="btnBack" runat="server" CssClass="btnBlk" OnClientClick="var retvalue = redirectLocation('FlightList.aspx'); event.returnValue= retvalue;event.preventDefault(); return retvalue;" Text="Back" />
                            <asp:Button ID="btnBookNow" runat="server" CssClass="btnRed" OnClientClick="return onAcceptArgument();"
         OnClick="btnBookNow_Click" Text="Proceed To Pay" />
                            
                        </div>
                        <div class="clr">
                        </div>
                    
                <!--StepsPage-->
            </div>
        </div>
        <div class="clr">
        </div>
    </div>
</asp:Content>
