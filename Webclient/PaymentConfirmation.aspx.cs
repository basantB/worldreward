﻿using CB.IBE.Platform.Car.ClientEntities;
using CB.IBE.Platform.Car.Entities;
using CB.IBE.Platform.ClientEntities;
using CB.IBE.Platform.Entities;
using CB.IBE.Platform.Hotels.ClientEntities;
using CB.IBE.Platform.Masters.Entities;
using Core.Platform.Booking.Entities;
using Core.Platform.Member.Entites;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;
using Framework.EnterpriseLibrary.Adapters;
using InfiPlanetModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PaymentConfirmation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        rdbtnCash.CheckedChanged += new EventHandler(rdbtnCash_CheckedChanged);
        rdbtnPoints.CheckedChanged += new EventHandler(rdbtnPoints_CheckedChanged);
        rdbtnVoucher.CheckedChanged += new EventHandler(rdbtnVoucher_CheckedChanged);


    }

    //protected void rdbtnPoints_CheckedChanged(object sender, EventArgs e)
    //{
    //    CreateItineraryResponse lobjCreateItineraryResponse = null;
    //    CreateItineraryRequest lobjCreateItineraryRequest = null;
    //    MemberDetails lobjMemberDetails = new MemberDetails();
    //    InfiModel lobjInfiModel=new InfiModel();
    //    if (Session["MemberDetails"].Equals(null))
    //    {
    //        Response.Redirect("login.aspx");
    //    }
    //    else
    //    {
    //        lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
    //    }

    //    if (Session["ItineraryResponse"] != null)
    //    {
    //        lobjCreateItineraryResponse = Session["ItineraryResponse"] as CreateItineraryResponse;

    //        string lstrCurrency = lobjInfiModel.GetDefaultCurrency();
    //        Session["MemberMiles"] = Convert.ToString(lobjInfiModel.CheckAvailbility(lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency));

    //        if (lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalPoints <= 0)
    //        {
    //            Response.Redirect("BookingFailure.aspx");
    //        }
    //        else
    //        {
    //            if (Convert.ToDouble(Session["MemberMiles"]) >= Convert.ToDouble(lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalPoints))
    //            {
    //                if (Session["BookingFlag"].Equals(ServiceType.FLIGHT))
    //                {
    //                    if(BookFlight())
    //                    {
    //                        Response.Redirect("AirReceipt.aspx", false);
    //                    }
    //                    else
    //                    {
    //                        Response.Redirect("BookingFailure.aspx", false);
    //                    }
    //                }
    //                else if (Session["BookingFlag"].Equals(ServiceType.HOTEL))
    //                {
    //                    if (BookHotel())
    //                    {
    //                        Response.Redirect("HotelVoucher.aspx", false);
    //                    }
    //                    else
    //                    {
    //                        Response.Redirect("BookingFailure.aspx", false);
    //                    }
    //                }
    //                else if (Session["BookingFlag"].Equals(ServiceType.CAR))
    //                {
    //                    if (BookCar())
    //                    {
    //                        Response.Redirect("CarVoucher.aspx", false);
    //                    }
    //                    else
    //                    {
    //                        Response.Redirect("BookingFailure.aspx", false);
    //                    }
    //                }

    //            }
    //            else
    //            {
    //                lblError.Text = "You need " + Convert.ToDouble(lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalPoints).ToString() + " RewardZ points to book this flight. Your RewardZ points available is " + Convert.ToDouble(Session["MemberMiles"]).ToString() + "." +
    //                "If you wish to top-up your WorldRewardZ account, <a href='PurchasePoints.aspx'>click here</a> or select different payment mode.";
    //            }
    //        }
    //        Session["ItineraryResponse"] = lobjCreateItineraryResponse;
    //    }
    //}

    protected void rdbtnPoints_CheckedChanged(object sender, EventArgs e)
    {
        if (Session["BookingFlag"].Equals(ServiceType.FLIGHT))
        {
            BookFlight();
        }
        else if (Session["BookingFlag"].Equals(ServiceType.HOTEL))
        {
            BookHotel();
        }
        else if (Session["BookingFlag"].Equals(ServiceType.CAR))
        {
            LoggingAdapter.WriteLog("BookCar Start 1");
            BookCar();
        }
    }

    protected void rdbtnVoucher_CheckedChanged(object sender, EventArgs e)
    {
        Response.Redirect("PointGateway.aspx",false);
    }

    protected void rdbtnCash_CheckedChanged(object sender, EventArgs e)
    {
        Response.Redirect("PointGateway.aspx", false);
    }

    public void BookFlight()
    {
        InfiModel lobjInfiModel = new InfiModel();
        if (HttpContext.Current.Session["ItineraryRequest"] != null && HttpContext.Current.Session["MemberDetails"] != null && HttpContext.Current.Session["ItineraryResponse"] != null)
        {
            CB.IBE.Platform.ClientEntities.SearchRequest lobjSearchRequest = HttpContext.Current.Session["FlightSearchDetails"] as CB.IBE.Platform.ClientEntities.SearchRequest;
            CreateItineraryResponse lobjCreateItineraryResponse = HttpContext.Current.Session["ItineraryResponse"] as CreateItineraryResponse;
            CreateItineraryRequest lobjCreateItineraryRequest = HttpContext.Current.Session["ItineraryRequest"] as CreateItineraryRequest;
            BookingRequest lobjBookingRequest = new BookingRequest();
            MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
            List<RedemptionDetails> lobjListOfRedemptionDetails = HttpContext.Current.Session["RedemptionDetails"] as List<RedemptionDetails>;
            RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererSupplierDetails"] as RefererDetails;

            string lstrCurrency = lobjInfiModel.GetDefaultCurrency();
            Session["MemberMiles"] = Convert.ToString(lobjInfiModel.CheckAvailbility(lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency));

            if (Convert.ToDouble(Session["MemberMiles"]) >= Convert.ToDouble(lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalPoints))
            {
                lobjBookingRequest.SupplierDetails.Id = lobjRefererDetails.RefererSupplierProperties.SupplierId;
                lobjBookingRequest.SessionId = lobjCreateItineraryRequest.SessionId;

                lobjBookingRequest.ItineraryDetails.MemberId = lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjBookingRequest.ItineraryDetails = lobjCreateItineraryResponse.ItineraryDetails;
                lobjBookingRequest.ItineraryDetails.RefererDetails = lobjSearchRequest.RefererDetails;
                lobjBookingRequest.PointRate = lobjCreateItineraryRequest.PointRate;
                lobjBookingRequest.IPAddress = lobjSearchRequest.IPAddress;
                CB.IBE.Platform.ClientEntities.BookingResponse lobjBookingResponse = lobjInfiModel.BookFlight(lobjBookingRequest, lobjMemberDetails, lobjListOfRedemptionDetails);
                if (lobjBookingResponse != null && lobjBookingResponse.PNRDetails.TripId != null && lobjBookingResponse.PNRDetails.TripId != string.Empty && lobjBookingResponse.PNRDetails.Status.Equals(1))
                {

                    ItineraryDetails lobjItineraryDetails = new ItineraryDetails();
                    lobjItineraryDetails = lobjBookingResponse.PNRDetails.ItineraryDetails;
                    lobjItineraryDetails.FareDetails = lobjBookingResponse.PNRDetails.ItineraryDetails.FareDetails;
                    lobjItineraryDetails.TravelerInfo = lobjBookingResponse.PNRDetails.ItineraryDetails.TravelerInfo;
                    lobjItineraryDetails.ItineraryReference = lobjBookingResponse.PNRDetails.BookingReference;
                    HttpContext.Current.Session["FlightBookedResponse"] = lobjBookingResponse;
                    HttpContext.Current.Session["FlightBooked"] = lobjItineraryDetails;
                    HttpContext.Current.Session["ItineraryResponse"] = null;
                    HttpContext.Current.Session["ItineraryRequest"] = null;
                    HttpContext.Current.Session["ReviewFlightDetails"] = null;

                    lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlight + " Status - " + "Success"), ActivityType.FlightBooking);
                    Response.Redirect("AirReceipt.aspx", false);
                }
                else
                {
                    lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlight + " Status - " + "Failed"), ActivityType.FlightBooking);
                    Response.Redirect("BookingFailure.aspx", false);
                }
            }
            else
            {
                lblError.Text = "You need " + Convert.ToDouble(lobjCreateItineraryResponse.ItineraryDetails.FareDetails.TotalPoints).ToString() + " RewardZ points to book this flight. Your RewardZ points available is " + Convert.ToDouble(Session["MemberMiles"]).ToString() + "." +
                "If you wish to top-up your WorldRewardZ account, <a href='PurchasePoints.aspx'>click here</a> or select different payment mode.";
            }
        }
        else
        {
            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlight + " Status - " + "Failed"), ActivityType.FlightBooking);
            Response.Redirect("BookingFailure.aspx", false);
        }
    }


    public void BookHotel()
    {
        InfiModel lobjInfiModel = new InfiModel();
        LoggingAdapter.WriteLog("Booking Hotel");
        LoggingAdapter.WriteLog((HttpContext.Current.Session["BookedHotel"] != null && HttpContext.Current.Session["CustomerDetails"] != null && HttpContext.Current.Session["SearchDetails"] != null).ToString());

        if (HttpContext.Current.Session["MemberDetails"] != null)
        {
            MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
            string lstrCurrency = lobjInfiModel.GetDefaultCurrency();
            Session["MemberMiles"] = Convert.ToString(lobjInfiModel.CheckAvailbility(lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency));
            
            if (HttpContext.Current.Session["BookedHotel"] != null && HttpContext.Current.Session["CustomerDetails"] != null && HttpContext.Current.Session["SearchDetails"] != null)
            {
                HotelSearchResponse lobjSearchResponse = HttpContext.Current.Session["BookedHotel"] as HotelSearchResponse;
                HotelSearchResponse lobjHotelBooked = new HotelSearchResponse();
                lobjHotelBooked = lobjSearchResponse;
                HotelSearchRequest lobjSearchRequest = HttpContext.Current.Session["SearchDetails"] as HotelSearchRequest;
                Framework.Integrations.Hotels.Entities.Customer lobjCustomer = HttpContext.Current.Session["CustomerDetails"] as Framework.Integrations.Hotels.Entities.Customer;

                List<RedemptionDetails> lobjListOfRedemptionDetails = HttpContext.Current.Session["RedemptionDetails"] as List<RedemptionDetails>;
                if (Convert.ToDouble(Session["MemberMiles"]) >= Convert.ToDouble(lobjListOfRedemptionDetails[0].Points))
                {
                    CB.IBE.Platform.Hotels.ClientEntities.HotelBookingResponse lobjBookingResponse = lobjInfiModel.BookHotel(lobjMemberDetails, lobjSearchResponse.SearchResponse.hotels.hotel[0], lobjSearchRequest, lobjCustomer, lobjListOfRedemptionDetails);
                    HttpContext.Current.Session["BookingResponse"] = lobjBookingResponse;
                    if (lobjBookingResponse != null && lobjBookingResponse.BookingResponse.bookingid != null && lobjBookingResponse.BookingResponse.confirmationnumber != null && lobjBookingResponse.BookingResponse.bookingid != string.Empty && lobjBookingResponse.BookingResponse.confirmationnumber != string.Empty)
                    {
                        HttpContext.Current.Session["BookedHotel"] = null;
                        HttpContext.Current.Session["HotelBooked"] = lobjHotelBooked;
                        lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Success"), ActivityType.HotelBooking);
                        Response.Redirect("HotelVoucher.aspx", false);
                    }
                    else
                    {
                        lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.HotelBooking);
                        Response.Redirect("BookingFailure.aspx", false);
                    }
                }
                else
                {
                    lblError.Text = "You need " + Convert.ToDouble(lobjListOfRedemptionDetails[0].Points).ToString() + " RewardZ points to book this hotel. Your RewardZ points available is " + Convert.ToDouble(Session["MemberMiles"]).ToString() + "." +
                    "If you wish to top-up your WorldRewardZ account, <a href='PurchasePoints.aspx'>click here</a> or select different payment mode.";
                }
            }
            else
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.HotelBooking);
                Response.Redirect("BookingFailure.aspx", false);
            }
        }
        else
        {
            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.HotelBooking);
            Response.Redirect("BookingFailure.aspx", false);
        }
    }

    public void BookCar()
    {
        InfiModel lobjInfiModel = new InfiModel();

        if (HttpContext.Current.Session["MemberDetails"] != null && HttpContext.Current.Session["CarMakeBookingRequest"] != null && HttpContext.Current.Session["CarSearchRequest"] != null && HttpContext.Current.Session["Cars"] != null && HttpContext.Current.Session["CarSelected"] != null && HttpContext.Current.Session["CarExtraListResponse"] != null)
        {
            
            MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;

            string lstrCurrency = lobjInfiModel.GetDefaultCurrency();
            Session["MemberMiles"] = Convert.ToString(lobjInfiModel.CheckAvailbility(lobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency));

            CarSearchRequest lobjCarSearchRequest = HttpContext.Current.Session["CarSearchRequest"] as CarSearchRequest;
            CarSearchResponse lobjCarSearchResponse = HttpContext.Current.Session["Cars"] as CarSearchResponse;
            Match lobjMatch = HttpContext.Current.Session["CarSelected"] as Match;
            CarExtrasListResponse lobjCarExtrasListResponse = HttpContext.Current.Session["CarExtraListResponse"] as CarExtrasListResponse;
            CarMakeBookingRequest lobjCarMakeBookingRequest = HttpContext.Current.Session["CarMakeBookingRequest"] as CarMakeBookingRequest;

            CarMakeBookingResponse lobjCarMakeBookingResponse = new CarMakeBookingResponse();

            lobjCarSearchRequest.SearchId = lobjCarSearchResponse.SearchId;

            List<RedemptionDetails> lobjListOfRedemptionDetails = HttpContext.Current.Session["RedemptionDetails"] as List<RedemptionDetails>;

            if (Convert.ToDouble(Session["MemberMiles"]) >= Convert.ToDouble(lobjListOfRedemptionDetails[0].Points))
            {
                lobjCarMakeBookingResponse = lobjInfiModel.BookForCar(lobjCarMakeBookingRequest, lobjCarSearchRequest, lobjMatch, lobjCarExtrasListResponse, lobjMemberDetails, lobjListOfRedemptionDetails);
                if (lobjCarMakeBookingResponse != null && lobjCarMakeBookingResponse.MakeBookingRS != null && lobjCarMakeBookingResponse.MakeBookingRS.Booking != null && !string.IsNullOrEmpty(lobjCarMakeBookingResponse.MakeBookingRS.Booking.id))
                {
                    HttpContext.Current.Session["CarMakeBookingResponse"] = lobjCarMakeBookingResponse;
                    HttpContext.Current.Session["CarMakeBookingRequest"] = null;
                    HttpContext.Current.Session["CarMadeBookingRequest"] = lobjCarMakeBookingRequest;
                    lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookCar + " Status - " + "Success"), ActivityType.CarBooking);
                    Response.Redirect("CarVoucher.aspx", false);
                }
                else
                {
                    LoggingAdapter.WriteLog("BookCar Start 8");
                    HttpContext.Current.Session["CarMakeBookingResponse"] = lobjCarMakeBookingResponse;
                    HttpContext.Current.Session["CarMakeBookingRequest"] = null;
                    HttpContext.Current.Session["CarMadeBookingRequest"] = lobjCarMakeBookingRequest;
                    lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookCar + " Status - " + "Failed"), ActivityType.CarBooking);
                    Response.Redirect("BookingFailure.aspx", false);
                }
            }
            else
            {
                lblError.Text = "You need " + Convert.ToDouble(lobjListOfRedemptionDetails[0].Points).ToString() + " RewardZ points to book this car. Your RewardZ points available is " + Convert.ToDouble(Session["MemberMiles"]).ToString() + "." +
                    "If you wish to top-up your WorldRewardZ account, <a href='PurchasePoints.aspx'>click here</a> or select different payment mode.";
            }
        }
        else
        {
            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookCar + " Status - " + "Failed"), ActivityType.CarBooking);
            Response.Redirect("BookingFailure.aspx", false);
        }
    }
}