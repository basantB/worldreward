﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="TourGradeList.aspx.cs" Inherits="TourGradeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="CSS/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
    <link href="css/Tour.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="js/JTemplateCreator.js" type="text/javascript"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="js/jquery.mCustomScrollbar.css" type="text/css" />
    <script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/TourGradeList.js"></script>
    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>

    <div class="inr-wrap mtop20 vator">
        <div class="hotelinfo">
            <h1>
                <asp:Label ID="lblProductTitle" runat="server"></asp:Label></h1>
        </div>
        <p>
            <span class="bold">Pricing based on &nbsp;</span>
            <asp:Label CssClass="bold" ID="lblAdult" runat="server"></asp:Label>
            <asp:Label CssClass="bold" ID="lblChild" runat="server"></asp:Label>
            <asp:Label CssClass="bold" ID="lblSenior" runat="server"></asp:Label>
            <asp:Label CssClass="bold" ID="lblInfant" runat="server"></asp:Label>
            <asp:Label CssClass="bold" ID="lblBookingDate" runat="server"></asp:Label>
        </p>
        <div class="cl"></div>
        <div class="wrapcover">
            <div id="wr" class="list" style="width: 99%; float: left;">
                <div class="tour-listing">
                    <div class="Gradecover_div">
                        <h1 style="padding: 2% 1%">Tour/Activity Options</h1>
                    </div>
                    <div id="resultTourGrade" class="clr">
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>

        <div class="hd-col1 bdr-rightno">

            <div>
                <ul class="ultraveler">
                    <li class="seltrvdt">
                        <label>
                            Select Travel Date
                        </label>
                        <asp:TextBox ID="txtBookingDate" runat="server"></asp:TextBox>
                    </li>
                    <div class="clr">
                    </div>
                    <li>
                        <div id="divAdult" runat="server" style="display: none;">
                            <label>
                                Adult</label>
                            <asp:Label runat="server" ID="lblAdultVal" CssClass="f10"></asp:Label>
                            <asp:DropDownList ID="ddlAdult" runat="server">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </li>

                    <li>
                        <div id="divChild" runat="server" style="display: none;">
                            <label>
                                Child</label>
                            <asp:Label runat="server" ID="lblChildVal" CssClass="f10"></asp:Label>
                            <asp:DropDownList ID="ddlChild" runat="server">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </li>
                    <li>
                        <div id="divSenior" runat="server" style="display: none;">
                            <label>
                                Senior
                            </label>
                            <asp:Label runat="server" ID="lblSeniorVal" CssClass="f10"></asp:Label>
                            <asp:DropDownList ID="ddlSenior" runat="server">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </li>
                    <li>
                        <div id="divInfant" runat="server" style="display: none;">
                            <label>
                                Infant</label>
                            <asp:Label runat="server" ID="lblInfantval" CssClass="f10"></asp:Label>
                            <asp:DropDownList ID="ddlInfant" runat="server">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </li>

                </ul>
            </div>
            <div class="cl"></div>
            <input type="button" value="Update Option" onclick="return UpdateTourAvailabilityRequest();" />
            <br />
            <br />
            <div id="divProducNotAvailable" style="color: red; width: 100%; margin: 5px 2px 0px 2px; font-size: 16px; display: none;">
            </div>
        </div>
    </div>
    <div id="LoadGradeTemplate" runat="server">
    </div>
</asp:Content>

