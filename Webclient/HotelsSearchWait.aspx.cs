﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfiPlanetModel.Model;
using CB.IBE.Platform.Entities;
using Core.Platform.Member.Entites;
using Core.Platform.Master.Entities;
using CB.IBE.Platform.ClientEntities;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;
using Framework.EnterpriseLibrary.Adapters;
using System.IO;
using CB.IBE.Platform.Hotels.ClientEntities;
using Framework.Integrations.Hotels.Entities;
using Core.Platform.ProgramMaster.Entities;


public partial class HotelsSearchWait : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            InfiModel lobjModel = new InfiModel();
            string lstrRelativePath = this.Page.AppRelativeVirtualPath;
            int lintActivityId = lobjModel.LogActivity(lstrRelativePath.Replace("~", string.Empty).Replace("/", string.Empty), ActivityType.PageLoad);

        }
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool GetHotelSearchResponse(string pstrCity, string pCheckIn, string pCheckOut, string pRoomString, string pisRedeemMiles)
    {

        InfiModel lobjModel = new InfiModel();

        ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();

        HotelSearchRequest lobjHotelSearchRequest = new HotelSearchRequest();
        lobjHotelSearchRequest.SearchRequest = lobjModel.CreateHotelSearchRequest(pstrCity, pCheckIn, pCheckOut, pRoomString);
        lobjHotelSearchRequest.SearchRequest.IpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
        string lstrMemberId = string.Empty;

        if (lobjMemberDetails != null)
        {
            lstrMemberId = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
            lobjHotelSearchRequest.SearchRequest.MembershipReference = lstrMemberId;
        }

        string lstrCurrency = lobjModel.GetDefaultCurrency();
        HttpContext.Current.Session["SearchCurrency"] = lstrCurrency;

        //lobjHotelSearchRequest.PaymentType = PaymentType.Points;
        lobjHotelSearchRequest.SearchRequest.RedemptionRate = lobjModel.GetProgramRedemptionRate(lstrCurrency, RedemptionCodeKeys.HOT.ToString(), lobjProgramDefinition.ProgramId);


        HotelSearchResponse lobjHotelSearchResponse = lobjModel.SearchHotel(lobjHotelSearchRequest);
        lobjHotelSearchRequest.SearchRequest.SearchId = lobjHotelSearchResponse.SearchId;
        HttpContext.Current.Session["SearchDetails"] = lobjHotelSearchRequest;
        HttpContext.Current.Session["Hotels"] = lobjHotelSearchResponse;
        if (lobjHotelSearchResponse != null)
        {
            if (lobjHotelSearchResponse.SearchResponse.hotels.hotel.Count() > 0)
            {
                lobjModel.LogActivity(string.Format("Hotels Found : Member Id : {0}, City : {1}, CheckIn : {2}, CheckOut : {3}", lstrMemberId, pstrCity, pCheckIn, pCheckOut), ActivityType.HotelSearch);
                return true;
            }
            else
            {
                lobjModel.LogActivity(string.Format("Hotels Not Found : Member Id : {0}, City : {1}, CheckIn : {2}, CheckOut : {3}", lstrMemberId, pstrCity, pCheckIn, pCheckOut), ActivityType.HotelSearch);
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}