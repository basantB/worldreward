﻿using CB.IBE.Platform.Car.ClientEntities;
using CB.IBE.Platform.Car.Entities;
using CB.IBE.Platform.ClientEntities;
using CB.IBE.Platform.Entities;
using CB.IBE.Platform.Hotels.ClientEntities;
using CB.IBE.Platform.Masters.Entities;
using Core.Platform.Booking.Entities;
using Core.Platform.Common.Entities;
using Core.Platform.Member.Entites;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using Framework.Integrations.Hotels.Entities;
using InfiPlanetModel.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PaymentResponse : System.Web.UI.Page
{
    string lstrExternalReference = string.Empty;
    string lstrTransactionId = string.Empty;
    Response lobjResponse = new Response();

    protected void Page_Load(object sender, EventArgs e)
    {
        LoggingAdapter.WriteLog("1");
        if (Request.Form.Count > 0)
        {
            LoggingAdapter.WriteLog("2");
            string lstSecretkey = ConfigurationSettings.AppSettings["InfiPaySecretKey"].ToString();
            LoggingAdapter.WriteLog("3 :" + lstSecretkey);
            string lstSecureData = lstSecretkey + "|" + Convert.ToString(Request.Form[0]) + "|" + Convert.ToString(Request.Form[1]);

            if (verifyMd5Hash(lstSecureData, Convert.ToString(Request.Form[2])))
            {
                for (int i = 0; i < Request.Form.Count; i++)
                {
                    if (Request.Form.Keys[i].StartsWith("IV_Response"))
                    {
                        lobjResponse = JSONSerialization.Deserialize<Response>(Request.Form[i]);
                        
                    }
                    if (Request.Form.Keys[i].StartsWith("IV_TxnId"))
                    {
                        lstrTransactionId = Convert.ToString(Request.Form[i]);
                        LoggingAdapter.WriteLog("Payment Response Transaction Id :" + lstrTransactionId);
                    }
                }

                if (lobjResponse.IsSucessful)
                {
                    try
                    {
                        List<object> lobjDict = CachingAdapter.Get(lstrTransactionId) as List<object>;
                        Session["MemberDetails"] = lobjDict[0] as MemberDetails;
                        
                        Session["ItineraryResponse"] = lobjDict[1] as CreateItineraryResponse;
                        Session["ItineraryRequest"] = lobjDict[2] as CreateItineraryRequest;
                        Session["PGTransactionDetails"] = lobjDict[3] as PaymentDetails;
                        Session["FlightSearchDetails"] = lobjDict[4] as SearchRequest;
                        Session["FlightBookingPaymentDetails"] = lobjDict[5] as BookingPaymentDetails;
                        
                        Session["BookedHotel"] = lobjDict[6] as HotelSearchResponse;
                        Session["CustomerDetails"] = lobjDict[7] as Customer;
                        Session["SearchDetails"] = lobjDict[8] as HotelSearchRequest;

                        Session["HotelBookingPaymentDetails"] = lobjDict[9] as BookingPaymentDetails;
                        Session["CarSearchRequest"] = lobjDict[10] as CarSearchRequest;
                        Session["Cars"] = lobjDict[11] as CarSearchResponse;
                        Session["CarSelected"] = lobjDict[12] as Match;
                        Session["CarExtraListResponse"] = lobjDict[13] as CarExtrasListResponse;
                        Session["CarMakeBookingRequest"] = lobjDict[14] as CarMakeBookingRequest;
                        Session["CarBookingPaymentDetails"] = lobjDict[15] as BookingPaymentDetails;
                        Session["CarMakeBookingResponse"] = lobjDict[16] as CarMakeBookingResponse;
                        Session["CarMadeBookingRequest"] = lobjDict[17] as CarMakeBookingRequest;
                        Session["CarBookedFailedResponse"] = lobjDict[18] as CarMakeBookingResponse;
                        Session["FlightBookedFailedResponse"] = lobjDict[19] as BookingResponse;
                        Session["RedemptionDetails"] = lobjDict[20] as List<RedemptionDetails>;
                        Session["BookingFlag"] = (ServiceType)Convert.ToInt32(lobjDict[21]);
                        
                    }
                    catch (Exception ex)
                    {
                        LoggingAdapter.WriteLog("PaymentResponse ExceptionMessage: " + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
                    }
                    
                    if (HttpContext.Current.Session["BookingFlag"].Equals(ServiceType.FLIGHT))
                    {
                        if (HttpContext.Current.Session["ItineraryRequest"] != null && HttpContext.Current.Session["MemberDetails"] != null && HttpContext.Current.Session["ItineraryResponse"] != null)
                        {
                            BookFlightByInfiPay(lstrTransactionId);
                        }
                    }
                    else if (HttpContext.Current.Session["BookingFlag"].Equals(ServiceType.HOTEL))
                    {
                        LoggingAdapter.WriteLog("Booking Flag : Hotel");
                        if (HttpContext.Current.Session["BookedHotel"] != null && HttpContext.Current.Session["CustomerDetails"] != null && HttpContext.Current.Session["SearchDetails"] != null)
                        {
                            LoggingAdapter.WriteLog("Booking Flag : Hotel 2");
                            BookHotelByInfiPay(lstrTransactionId);
                        }
                    }
                    else if (HttpContext.Current.Session["BookingFlag"].Equals(ServiceType.CAR))
                    {
                        LoggingAdapter.WriteLog("BookingFlag : CAR");
                        if (HttpContext.Current.Session["CarSearchRequest"] != null && HttpContext.Current.Session["Cars"] != null && HttpContext.Current.Session["CarSelected"] != null && HttpContext.Current.Session["CarExtraListResponse"] != null)
                        {

                            BookCarByInfiPay(lstrTransactionId);
                        }

                    }
                    else
                    {
                        string lstrInfiPaymentFailureMsg = Convert.ToString(ConfigurationManager.AppSettings["InfiPaymentFailureMsg"]);
                        HttpContext.Current.Session["BookingError"] = lstrInfiPaymentFailureMsg;
                        Response.Redirect("BookingFailure.aspx");
                    }
                }
            }
            else
            {
                Response.Redirect("ErrorPage.aspx");
            }

        }
    }

    private void BookFlightByInfiPay(string pstrTxnReference)
    {
        InfiModel lobjInfiModel = new InfiModel();

        if (HttpContext.Current.Session["ItineraryRequest"] != null && HttpContext.Current.Session["MemberDetails"] != null && HttpContext.Current.Session["ItineraryResponse"] != null)
        {
            CB.IBE.Platform.ClientEntities.SearchRequest lobjSearchRequest = HttpContext.Current.Session["FlightSearchDetails"] as CB.IBE.Platform.ClientEntities.SearchRequest;
            CreateItineraryResponse lobjCreateItineraryResponse = HttpContext.Current.Session["ItineraryResponse"] as CreateItineraryResponse;
            CreateItineraryRequest lobjCreateItineraryRequest = HttpContext.Current.Session["ItineraryRequest"] as CreateItineraryRequest;
            BookingRequest lobjBookingRequest = new BookingRequest();
            MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
            List<RedemptionDetails> lobjListOfRedemptionDetails = HttpContext.Current.Session["RedemptionDetails"] as List<RedemptionDetails>;
            RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererSupplierDetails"] as RefererDetails;

            lobjBookingRequest.SupplierDetails.Id = lobjRefererDetails.RefererSupplierProperties.SupplierId;
            lobjBookingRequest.SessionId = lobjCreateItineraryRequest.SessionId;

            lobjBookingRequest.ItineraryDetails.MemberId = lobjMemberDetails.MemberRelationsList[0].RelationReference;
            lobjBookingRequest.ItineraryDetails = lobjCreateItineraryResponse.ItineraryDetails;
            lobjBookingRequest.ItineraryDetails.RefererDetails = lobjSearchRequest.RefererDetails;

            lobjBookingRequest.ItineraryDetails.TransactionReference = pstrTxnReference;

            lobjBookingRequest.PointRate = lobjCreateItineraryRequest.PointRate;
            lobjBookingRequest.IPAddress = lobjSearchRequest.IPAddress;
            LoggingAdapter.WriteLog("BookingFlag : Flight3");
            CB.IBE.Platform.ClientEntities.BookingResponse lobjBookingResponse = lobjInfiModel.BookFlightByInfiPay(lobjBookingRequest, lobjMemberDetails, lobjListOfRedemptionDetails);
            if (lobjBookingResponse != null && lobjBookingResponse.PNRDetails.TripId != null && lobjBookingResponse.PNRDetails.TripId != string.Empty && lobjBookingResponse.PNRDetails.Status.Equals(1))
            {

                ItineraryDetails lobjItineraryDetails = new ItineraryDetails();
                lobjItineraryDetails = lobjBookingResponse.PNRDetails.ItineraryDetails;
                lobjItineraryDetails.FareDetails = lobjBookingResponse.PNRDetails.ItineraryDetails.FareDetails;
                lobjItineraryDetails.TravelerInfo = lobjBookingResponse.PNRDetails.ItineraryDetails.TravelerInfo;
                lobjItineraryDetails.ItineraryReference = lobjBookingResponse.PNRDetails.BookingReference;
                HttpContext.Current.Session["FlightBookedResponse"] = lobjBookingResponse;
                HttpContext.Current.Session["FlightBooked"] = lobjItineraryDetails;
                HttpContext.Current.Session["ItineraryResponse"] = null;
                HttpContext.Current.Session["ItineraryRequest"] = null;
                HttpContext.Current.Session["ReviewFlightDetails"] = null;

                lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlight + " Status - " + "Success"), ActivityType.FlightBooking);

                Response.Redirect("AirReceipt.aspx", false);
            }
            else
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlight + " Status - " + "Failed"), ActivityType.FlightBooking);
                Response.Redirect("BookingFailure.aspx", false);
            }
        }
        else
        {
            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookFlight + " Status - " + "Failed"), ActivityType.FlightBooking);
            Response.Redirect("BookingFailure.aspx", false);
        }
    }

    private void BookHotelByInfiPay(string pstrTxnReference)
    {
        LoggingAdapter.WriteLog("Booking Flag : Hotel 3");
        InfiModel lobjInfiModel = new InfiModel();
        try
        {
            if (HttpContext.Current.Session["MemberDetails"] != null)
            {
                LoggingAdapter.WriteLog("Booking Flag : Hotel 4");
                MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
                if (HttpContext.Current.Session["BookedHotel"] != null && HttpContext.Current.Session["CustomerDetails"] != null && HttpContext.Current.Session["SearchDetails"] != null)
                {
                    LoggingAdapter.WriteLog("Booking Flag : Hotel 5");
                    HotelSearchResponse lobjSearchResponse = HttpContext.Current.Session["BookedHotel"] as HotelSearchResponse;
                    HotelSearchResponse lobjHotelBooked = new HotelSearchResponse();
                    lobjHotelBooked = lobjSearchResponse;
                    HotelSearchRequest lobjSearchRequest = HttpContext.Current.Session["SearchDetails"] as HotelSearchRequest;
                    Framework.Integrations.Hotels.Entities.Customer lobjCustomer = HttpContext.Current.Session["CustomerDetails"] as Framework.Integrations.Hotels.Entities.Customer;

                    List<RedemptionDetails> lobjListOfRedemptionDetails = HttpContext.Current.Session["RedemptionDetails"] as List<RedemptionDetails>;

                    //CB.IBE.Platform.Hotels.ClientEntities.HotelBookingResponse lobjBookingResponse = lobjInfiModel.BookHotel(lobjMemberDetails, lobjSearchResponse.SearchResponse.hotels.hotel[0], lobjSearchRequest, lobjCustomer, lobjListOfRedemptionDetails);
                    CB.IBE.Platform.Hotels.ClientEntities.HotelBookingResponse lobjBookingResponse = lobjInfiModel.BookHotelByInfiPay(lobjMemberDetails, lobjSearchResponse.SearchResponse.hotels.hotel[0], lobjSearchRequest, lobjCustomer, lobjListOfRedemptionDetails, pstrTxnReference);
                    HttpContext.Current.Session["BookingResponse"] = lobjBookingResponse;
                    if (lobjBookingResponse != null && lobjBookingResponse.BookingResponse.bookingid != null && lobjBookingResponse.BookingResponse.confirmationnumber != null && lobjBookingResponse.BookingResponse.bookingid != string.Empty && lobjBookingResponse.BookingResponse.confirmationnumber != string.Empty)
                    {
                        LoggingAdapter.WriteLog("Booking Hotel5");
                        HttpContext.Current.Session["BookedHotel"] = null;
                        HttpContext.Current.Session["HotelBooked"] = lobjHotelBooked;
                        lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Success"), ActivityType.HotelBooking);
                        Response.Redirect("HotelVoucher.aspx", false);

                    }
                    else
                    {
                        LoggingAdapter.WriteLog("Booking Hotel6");
                        lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.HotelBooking);
                        Response.Redirect("BookingFailure.aspx", false);
                    }
                }
                else
                {
                    lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.HotelBooking);
                    Response.Redirect("BookingFailure.aspx", false);
                }
            }
            else
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.HotelBooking);
                Response.Redirect("BookingFailure.aspx", false);
            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("BookHotelByInfiPay Message : " + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.Source);
            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.HotelBooking);
            Response.Redirect("BookingFailure.aspx", false);
        }
    }

    private void BookCarByInfiPay(string pstrTxnReference)
    {
        InfiModel lobjInfiModel = new InfiModel();
        try
        {
            if (HttpContext.Current.Session["MemberDetails"] != null && HttpContext.Current.Session["CarMakeBookingRequest"] != null && HttpContext.Current.Session["CarSearchRequest"] != null && HttpContext.Current.Session["Cars"] != null && HttpContext.Current.Session["CarSelected"] != null && HttpContext.Current.Session["CarExtraListResponse"] != null)
            {
                MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
                CarSearchRequest lobjCarSearchRequest = HttpContext.Current.Session["CarSearchRequest"] as CarSearchRequest;
                CarSearchResponse lobjCarSearchResponse = HttpContext.Current.Session["Cars"] as CarSearchResponse;
                Match lobjMatch = HttpContext.Current.Session["CarSelected"] as Match;
                CarExtrasListResponse lobjCarExtrasListResponse = HttpContext.Current.Session["CarExtraListResponse"] as CarExtrasListResponse;
                CarMakeBookingRequest lobjCarMakeBookingRequest = HttpContext.Current.Session["CarMakeBookingRequest"] as CarMakeBookingRequest;

                lobjCarMakeBookingRequest.BookingPaymentDetails.PointsTxnRefererence = pstrTxnReference;

                CarMakeBookingResponse lobjCarMakeBookingResponse = new CarMakeBookingResponse();

                lobjCarSearchRequest.SearchId = lobjCarSearchResponse.SearchId;

                List<RedemptionDetails> lobjListOfRedemptionDetails = HttpContext.Current.Session["RedemptionDetails"] as List<RedemptionDetails>;

                lobjCarMakeBookingResponse = lobjInfiModel.BookCarByInfiPay(lobjCarMakeBookingRequest, lobjCarSearchRequest, lobjMatch, lobjCarExtrasListResponse, lobjMemberDetails, lobjListOfRedemptionDetails);

                if (lobjCarMakeBookingResponse != null && lobjCarMakeBookingResponse.MakeBookingRS != null && lobjCarMakeBookingResponse.MakeBookingRS.Booking != null && !string.IsNullOrEmpty(lobjCarMakeBookingResponse.MakeBookingRS.Booking.id))
                {
                    HttpContext.Current.Session["CarMakeBookingResponse"] = lobjCarMakeBookingResponse;
                    HttpContext.Current.Session["CarMakeBookingRequest"] = null;
                    HttpContext.Current.Session["CarMadeBookingRequest"] = lobjCarMakeBookingRequest;
                    lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookCar + " Status - " + "Success"), ActivityType.CarBooking);
                    Response.Redirect("CarVoucher.aspx", false);
                }
                else
                {
                    HttpContext.Current.Session["CarMakeBookingResponse"] = lobjCarMakeBookingResponse;
                    HttpContext.Current.Session["CarMakeBookingRequest"] = null;
                    HttpContext.Current.Session["CarMadeBookingRequest"] = lobjCarMakeBookingRequest;
                    lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookCar + " Status - " + "Failed"), ActivityType.CarBooking);
                    Response.Redirect("BookingFailure.aspx", false);
                }
            }
            else
            {
                lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookCar + " Status - " + "Failed"), ActivityType.CarBooking);
                Response.Redirect("BookingFailure.aspx", false);
            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("BookCarByInfiPay Message : " + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace + Environment.NewLine + ex.InnerException + Environment.NewLine + ex.Source);
            lobjInfiModel.LogActivity(string.Format(ActivityConstants.BookHotel + " Status - " + "Failed"), ActivityType.CarBooking);
            Response.Redirect("BookingFailure.aspx", false);
        }
    }

    public static string getMd5Hash(string pstrInput)
    {
        MD5 md5HashAlgo = MD5.Create();
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(pstrInput);
        byte[] hashBytes = md5HashAlgo.ComputeHash(inputBytes);
        StringBuilder secureData = new StringBuilder();
        for (int i = 0; i < hashBytes.Length; i++)
        {
            secureData.Append(hashBytes[i].ToString("X2"));
        }
        return secureData.ToString();
    }

    public static bool verifyMd5Hash(string input, string hash)
    {
        // Hash the input.
        string hashOfInput = getMd5Hash(input);

        // Create a StringComparer an compare the hashes.
        StringComparer comparer = StringComparer.OrdinalIgnoreCase;

        if (0 == comparer.Compare(hashOfInput, hash))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}