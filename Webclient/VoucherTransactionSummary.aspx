﻿<%@ Page Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="VoucherTransactionSummary.aspx.cs" Inherits="VoucherTransactionSummary" %>


<%@ Register Src="~/UserControl/ActivationMenu.ascx" TagName="ActivationMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <script src="js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="js/jquery.mCustomScrollbar.css" type="text/css" />
    <script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ShowVoucher() {
            $("html").scrollTop(40);
            $("#Voucher").addClass("Hotel_Details_Cotent");
            $("#Voucher").removeClass("hidecontent");
            $("#Fade").show();
            return false;
        }
        function HideVoucher() {
            $("#CP_Voucher").addClass("hidecontent");
            $("#CP_Voucher").removeClass("Hotel_Details_Cotent");
            $("#CP_Fade").hide();
            return false;
        }
        function PrintPanel(voucherNo) {
            var ImgVoucherImage = document.getElementById('CP_imgVoucher');
            ImgVoucherImage.src = 'http://worldrewardz.com/Vouchers/' + voucherNo + '.png';
            var ImgVoucher = document.getElementById('DivimgVoucher');

            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title></title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(ImgVoucher.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;

        }

        function showVoucherSummary() {

            if ($("#btnVoucherSummary")[0].className == "Modify_TabNonActive") {
                $("#btnVoucherSummary").removeClass("Modify_TabNonActive");
                $("#btnVoucherSummary").addClass("Modify_TabActive");
                $("#ContentPlaceHolder1_lblplusminus").text('-');
                $("#ContentPlaceHolder1_divVouchers").slideToggle("slow");
                $("#ContentPlaceHolder1_divVouchers").css("display", "block");
            }
            else {
                $("#ContentPlaceHolder1_divVouchers").slideToggle("slow");
                $("#ContentPlaceHolder1_divVouchers").css("display", "none");
                $("#btnVoucherSummary").addClass("Modify_TabNonActive");
                $("#btnVoucherSummary").removeClass("Modify_TabActive");
                $("#ContentPlaceHolder1_lblplusminus").text('+');
            }


        }
    </script>
    <link href="css/myAccount.css" rel="stylesheet" />
    <style type="text/css">
        #CP_RepeaterPaging table,#CP_RepeaterPaging td {
    border: medium none !important;
}
.Vou_Status_cover > img {
    width: 140px;
}
        .fl {
            float: left;
        }

        .RepeaterContent_LinkFirst {
            float: left;
            width: 40px;
        }

        .RepeaterContent_LinkPrev {
            float: left;
            width: 65px;
        }

        .RepeaterContent_LinkNext {
            float: left;
            width: 40px;
        }

        .RepeaterContent_LinkLast {
            float: left;
            width: 70px;
        }

        .Hotel_Details_Cotent {
            display: block;
            position: fixed;
            margin: auto;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            border: 1px solid black;
            width: 70%;
            height: 450px;
            background-color: #ffffff;
            z-index: 999999;
            color: #000;
        }

        
.Vou_Status_cover {
   position: absolute;
    right: 10px;
    top: 170px;
}

        .Vou_Img_cover {
            float: left;
            /*width: 62%;*/
            padding: 2% 0;
            position:relative;
        }

        .black_overlay {
            background-color: #242424;
            display: none;
            height: 100%;
            left: 0;
            opacity: 0.8;
            overflow: hidden;
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 77788;
        }

        .sumry-tbl table td {
            text-align: center;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="loading">
                <img src="Images/PreLoader.gif" alt="Loading..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="brd-crms">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>

    <!--new -->
     <article id="article">
         <div class="wrap">
            <h1>Experience Abundance with WorldRewardZ</h1>
            <p>Below is a categorized listing of your activities at WorldRewardZ. As you glance through them, you can immerse in the joy of having the best choices in the world available to you simply at a click, with WorldRewardZ. Go ahead and multiply your unique experiences.</p>
			<div class="ManyWrap MyAccount">
                <uc1:ActivationMenu ID="ActivationMenu" runat="server" />
                <div class="InnCont" id="Details">
				 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="PanStatementSummary" runat="server" >
                                
                                <div class="Width100 padding10">
                                    <asp:DropDownList ID="ddlProgramCurrency" runat="server" AppendDataBoundItems="true"
                                        OnSelectedIndexChanged="ddlProgramCurrency_SelectedIndexChanged" AutoPostBack="true" style="display: none;">
                                    </asp:DropDownList>
                                </div>
                                <div class="common_div">
                                    
                                    <div id="divVoucherSummary" runat="server" class="width100" style="display: none;">
                                        <div class="Width100  mb10">
                                            <%--<b onclick="return ShowSummary();"  style="cursor: pointer">Voucher Summary</b>--%>
                                        </div>
                                        <%-- <div class="Width5 fl" onclick="return ShowSummary();" id="plus" style="cursor:pointer;margin-top:2px;">+</div>--%>
                                        <div id="divVouchers" runat="server">
                                            <div id="VoucherSummary" class="mTop10">
                                                <div class="width100">
                                                    <asp:HiddenField ID="HFSrno" runat="server" Value="0" />
                                                    <asp:Repeater ID="RepVoucherSummaryInfo" runat="server" OnItemDataBound="RepVoucherSummaryInfo_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="border_Voucher">
                                                                <tr class="stylegridheader">
                                                                    <th>
                                                                        Sr. No
                                                                    </th>
                                                                    <th>
                                                                        Voucher Code
                                                                    </th>
                                                                   
                                                                    <th>
                                                                        Issue Date
                                                                    </th>
                                                                    <th>
                                                                        Expiry Date
                                                                    </th>
                                                                    <th>
                                                                        Status
                                                                    </th>
                                                                     <th>
                                                                        Voucher Type
                                                                    </th>
                                                                    
                                                                </tr>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr class="stylealternate1">
                                                                <td align="center" valign="middle" height="25" class="borderTop_Voucher">
                                                                    <asp:Label ID="lblSrNo" runat="server" Text='<%# Container.ItemIndex + 1%>'></asp:Label>
                                                                </td>
                                                                <td align="center" valign="middle" class="borderTop_Voucher">
                                                                    <asp:HiddenField ID="HFStatus" runat="server" />
                                                                    <%--<span onclick="return ShowVoucher()" class="ArrowStyle" style="color: #1D87AE; text-decoration: underline">--%>
                                                                    <asp:LinkButton ID="lnkVoucherCode" runat="server" class="ArrowStyle" Text='<%#Eval("RelationReference")%>'
                                                                        Style="color: #1D87AE; text-decoration: underline" OnClick="lnkVoucherCode_Click"
                                                                        CommandArgument='<%#Eval("RelationReference")%>'></asp:LinkButton>
                                                                    <%--  </span>--%>
                                                                </td>
                                                                <%--<td align="center" valign="middle" class="borderTop_Voucher">
                                                                    <%# Eval("MerchantName")%>
                                                                </td>--%>
                                                                <td align="center" valign="middle" class="borderTop_Voucher">
                                                                    <asp:Label ID="lblDateOfIssue" runat="server" Text='<%# Eval("TransactionDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                                </td>
                                                                <td align="center" valign="middle" class="borderTop_Voucher">
                                                                    <asp:Label ID="lblExpiryDate" runat="server" Text='<%# Eval("ExpiryDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                                </td>
                                                                <td align="center" valign="middle" class="borderTop_Voucher">
                                                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                                </td>
                                                               <%-- <td align="center" valign="middle" class="borderTop_Voucher">
                                                                    <asp:ImageButton ID="ImageButtonPrint" runat="server" ImageUrl="~/Images/Btn_Voucher_Print.jpg"
                                                                        OnClientClick='<%# String.Format("javascript:return PrintPanel(\"{0}\")", Eval("RelationReference").ToString()) %>' />
                                                                </td>--%>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                            <asp:Label ID="lblErrorMsg" runat="server" CssClass="errMsg" Text="No Records Found."
                                                                Visible="false" />
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                   
                                                </div>
                                            </div>
                                            <div id="RepeaterPaging" runat="server">
                                                <table class="PageingStyle padding10" style="width: auto !important;">
                                                    <tr>
                                                        <td class="repeaterPageingFirstTab">
                                                        <asp:LinkButton ID="linkbuttonFirst" runat="server" OnClick="linkbuttonFirst_Click"
                                                                Style="color: #000; text-decoration: underline">First</asp:LinkButton>    
                                                        </td>
                                                        <td class="repeaterPageingPreTab">
                                                        <asp:LinkButton ID="linkbuttonPrevious" runat="server" OnClick="linkbuttonPrevious_Click"
                                                                Style="color: #000; text-decoration: underline">Previous</asp:LinkButton>    
                                                        </td>
                                                        <td class="repeaterPageingNumberTab">
                                                        <asp:DataList ID="dlPaging" runat="server" RepeatDirection="Horizontal" OnItemCommand="dlPaging_ItemCommand"
                                                                ShowFooter="False" ShowHeader="False" Width="10px" OnItemDataBound="dlPaging_ItemDataBound">
                                                                <ItemStyle />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkbtnPaging" runat="server" CommandArgument='<%# Eval("PageIndex") %>'
                                                                        CommandName="Paging" Text='<%# Eval("PageText") %>' Style="color: #000; text-decoration: underline"></asp:LinkButton>&nbsp;
                                                                </ItemTemplate>
                                                            </asp:DataList>    
                                                        </td>
                                                        <td class="repeaterPageingNextTab">
                                                            <asp:LinkButton ID="linkbuttonNext" runat="server" OnClick="linkbuttonNext_Click"
                                                                Style="color: #000; text-decoration: underline" >Next</asp:LinkButton>
                                                        </td>
                                                        <td class="repeaterPageingNextTab">
                                                            <asp:LinkButton ID="linkbuttonLast" runat="server" OnClick="linkbuttonLast_Click"
                                                                Style="color: #000; text-decoration: underline">Last</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                            </div>
                                        </div>
                                    </div>

                                     <asp:Label ID="lblResult" runat="server"></asp:Label>
                                </div>

                                

                            </asp:Panel>
                            <asp:Panel ID="Voucher"  runat="server">
                                <div class="Hotel_Details_Cotent">
                                    <div style="overflow:auto;height:400px" class="Popup_Cont">
                                        <div style="float: left; width: 97%; text-align: right;">
                                            <img src="Images/Ico_Close.png" onclick="return HideVoucher()" class="ArrowStyle" style="position:fixed;"/>
                                        </div>
                                       
                                        <div style="padding:8% 5%;">
                                        
                                         <asp:Repeater ID="RptVoucherDetails" runat="server" OnItemDataBound="RptVoucherDetails_ItemDataBound">
                                            <HeaderTemplate>
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0" class="border_Voucher">
                                                    <tr class="stylegridheader">
                                                        <th>
                                                            Date
                                                        </th>
                                                        <th>
                                                            Description
                                                        </th>
                                                        <th>
                                                            Transaction Type
                                                        </th>
                                                        <th>
                                                            Voucher Code
                                                        </th>
                                                        <th>
                                                            Status
                                                        </th>
                                                        <%--<td>
                                                            Details
                                                        </td>--%>
                                                        <th>
                                                            Amount (WorldRewardZ)
                                                        </th>
                                                        <th>
                                                           Remaining Balance (WorldRewardZ)
                                                        </th>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class="stylealternate1">
                                                    <td align="center" valign="middle" height="25" class="borderTop_Voucher">
                                                        <asp:HiddenField ID="HFExpiryDate" runat="server" Value='<%# Eval("ExpiryDate", "{0:dd/MM/yyyy}")%>' />
                                                        <asp:Label runat="server" ID="lblTransactionDate" Text='<%# Eval("TransactionDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                    </td>
                                                    <td align="center" valign="middle" class="borderTop_Voucher">
                                                        <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Narration")%>'></asp:Label>
                                                    </td>
                                                    <td align="center" valign="middle" class="borderTop_Voucher">
                                                        <asp:Label runat="server" ID="lblTranType"></asp:Label>
                                                    </td>
                                                    <td align="center" valign="middle" class="borderTop_Voucher">
                                                        <asp:Label runat="server" ID="lblVoucherCode" Text='<%#Eval("RelationReference")%>'></asp:Label>
                                                    </td>
                                                    <td align="center" valign="middle" class="borderTop_Voucher">
                                                        <asp:Label runat="server" ID="lblStatusDetails"></asp:Label>
                                                    </td>
                                                    <%--<td align="center" valign="middle" class="borderTop_Voucher">
                                                        <asp:Label runat="server" ID="lblDetails" Text='<%#Eval("AdditionalDetail")%>'></asp:Label>
                                                    </td>--%>
                                                    <td align="center" valign="middle" class="borderTop_Voucher">
                                                        <asp:Label runat="server" ID="lblAmount" Text='<%#Eval("Amounts")%>'></asp:Label>
                                                    </td>
                                                    <td align="center" valign="middle" class="borderTop_Voucher">
                                                        <asp:Label runat="server" ID="lblRemainingValue" Text='<%#Eval("AdditionalDetails1")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <%-- <tr>
                                                               <td colspan="7">
                                                                    </td>
                                                                    <asp:Image ID="ImgStatus" runat="server" />
                                                                </tr>--%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <%--paging--%>
                                        <asp:Label ID="lblErrorMsg" runat="server" CssClass="errMsg" Text="No Records Found."
                                            Visible="false" />
                                        <div class="Voucher_ImgDiv" >
                                            <div class="Vou_Img_cover" id='DivimgVoucher' style="display:block">
                                                <img src="" alt="Voucher" id="imgVoucher" style="border: 1px solid #ccc;" runat="server" />
                                                  <div class="Vou_Status_cover">
                                                <img id="imgStatus" runat="server" src="~/Images/Voucher_Active.jpg" alt="Status" />
                                            </div>
                                            </div>
                                          
                                        </div>
                                        
                                        </div>
                                 
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="Fade" class="black_overlay" runat="server">
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
             <!--ManyWrap MyAccount-->
           </div>
     </article>
    <!--end new-->

    <script>

        $("#Details").niceScroll({
            cursorcolor: "#e3443e",
            cursoropacitymin: 0.3,
            background: "#bbb",
            cursorborder: "0",
            autohidemode: false,
            cursorminheight: 30
        });

    </script>
</asp:Content>

