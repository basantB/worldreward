﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/InfiPlanetMaster.master" CodeFile="ExtLogin.aspx.cs" Inherits="ExtLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <div class="wrap">
                <div class="cover">
                    <div class="LoginOuter">
                        <div class="HeaderText">
                            Login</div>
                        <div class="Width100 fontw400 mbot10">
                            Please provide the required information below to log into your WorldRewards Account.</div>
                        <div class="common_div ">
                            <div class="Width100">
                                <div class="LoginContainer">
                                    <div class="">
                                        <div class="Home_Tab_CommonDiv mtop_10">
                                            <div class="fleft Width18 fontw400 pTop_5">
                                                World Rewards Member No</div>
                                            <div class="fleft Width20">
                                                <asp:TextBox ID="txtMemberName" runat="server" CssClass="textboxStyle TxtBox_Width100"
                                                    AutoCompleteType="Disabled" ></asp:TextBox>
                                            </div>
                                           <%-- <div style="float:left;margin-left:4%;margin-top: 1%;color:Red;">
                                               <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtMemberName" runat="server" ErrorMessage="Please Enter Member No"></asp:RequiredFieldValidator>
                                            </div>--%>
                                        </div>
                                        <div class="Home_Tab_CommonDiv mtop_10" id="PasswordDiv">
                                            <div class="fleft Width18 fontw400 pTop_5">
                                                Password</div>
                                            <div class="fleft Width20">
                                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" AutoCompleteType="Disabled"
                                                    CssClass="textboxStyle TxtBox_Width100"></asp:TextBox>
                                            </div>
                                            <%--<div style="float:left;margin-left:4%;margin-top: 1%;color:Red;">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtPassword" runat="server" ErrorMessage="Please Enter Password"></asp:RequiredFieldValidator>
                                            </div>--%>
                                        </div>
                                      <%--  <div class="Home_Tab_CommonDiv mtop_10 Size13" id="RememberDiv" style="display: none;">
                                            <div class="Width34 mL13p mtop_10">
                                                <asp:CheckBox ID="chkRememberMe" runat="server" Text="" CssClass="pRight_3" />
                                                I agree to WorldRewards Terms & Condition.
                                            </div>
                                        </div>--%>
                                        <div class="Home_Tab_CommonDiv mtop_10" id="LoginDiv">
                                            <div class="Width100">
                                                <div class="fleft Width18 fontw400">
                                                    &nbsp;
                                                </div>
                                                <div class=" mtop_10 fL">
                                                    <div class="DivImageButtonContinue">
                                                     <asp:Button ID="btnexternal" Text="Continue" runat="server" OnClick="btnexternal_Click"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <%--<div class="Home_Tab_CommonDiv mtop_10" id="OTPDiv" style="display: none;">
                                            <div class="fleft Width18">
                                                OTP</div>
                                            <div class="fleft Width20">
                                                <asp:TextBox ID="txtOTP" TextMode="Password" AutoCompleteType="Search" MaxLength="4"
                                                    runat="server" Text="" CssClass="textboxStyle TxtBox_Width100"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="Home_Tab_CommonDiv mtop_10" id="UnlockDiv" style="display: none;">
                                            <div class="fleft Width18 fontw400">
                                                &nbsp;
                                            </div>--%>
                                          <%--  <div class="mtop_10 fL">
                                                <div class="DivImageButtonContinue">
                                                    <a onclick="var
                    retvalue = UnLockMemberByOTP(); event.returnValue= retvalue; return retvalue;">Continue</a>
                                                </div>
                                            </div>--%>
                                        </div>
                                        <div class="Home_Tab_CommonDiv mtop_10">
                                            <div class="Width100">
                                                <div class="fleft Width18 fontw400">
                                                    &nbsp;
                                                </div>
                                                <div id="ErrorMsgContainer" class="ErrorMsgContainerExtMerchant mtop_10" runat="server">
                                                    <asp:Label runat="server" ID="lblLoginError" Text=""></asp:Label>
                                                    <div id="LoginValidation">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Home_Tab_CommonDiv mtop_10 " id="ForgetDiv">
                                            <div class="Width100">
                                                <div class="fleft Width18 fontw400">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField ID="hdfContext" runat="server" />
                    </div>
                    <script src="js/Validation.js" type="text/javascript"></script>
                    <script src="js/jquery-1.7.1.min.js"></script>
                    <script type="text/javascript">
                        function showMyLoginDiv() {
                            $("#PasswordDiv").css("display", "none");
                            $("#RememberDiv").css("display", "none");
                            $("#LoginDiv").css("display", "none");
                            $("#OTPDiv").css("display", "block");
                            $("#UnlockDiv").css("display", "block");
                            $("#ForgetDiv").css("display", "none");
                            $("#CP_lblLoginError").html('');
                            GenerateOTPonLogin();
                        }
                        function showLoginDiv() {
                            $("#PasswordDiv").css("display", "block");
                            $("#RememberDiv").css("display", "none");
                            $("#LoginDiv").css("display", "block");
                            $("#OTPDiv").css("display", "none");
                            $("#UnlockDiv").css("display", "none");
                            $("#ForgetDiv").css("display", "block");
                            $("#ct_CBI_ErrorMsgContainer").css("display", "none");
                            $("#LoginValidation")[0].innerHTML = "";
                            $("#CP_txtPassword")[0].value = "";
                        }
                    </script>
                </div>
            
    </asp:Content>
