﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfiPlanetModel.Model;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;
using Core.Platform.Member.Entites;
using Framework.EnterpriseLibrary.PasswordReset.Entities;
using Core.Platform.ProgramMaster.Entities;

public partial class ForgotPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string MemberShipReference(string pstrMemberid)
    {
        InfiModel lobjModel = new InfiModel();
        string lstrPWDReset = string.Empty;
        string lstrResponse = string.Empty;
        try
        {
            InfiModel lobjInfiModel = new InfiModel();
            SearchMember lobjSearchMember = new SearchMember();
          //  lobjSearchMember.RelationReference = pstrMemberid;
            //lobjSearchMember.RelationType = Convert.ToInt32(RelationType.Login);
            lobjSearchMember.SearchColumnName = "Created_By";
            lobjSearchMember.SearchColumnValue = pstrMemberid;
            lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
            //MemberDetails lobjMemberDetails = lobjModel.GetEnrollMemberDetails(lobjSearchMember);
            MemberDetails lobjMemberDetails = lobjModel.GetEnrollMemberDetailsByPrimaryReference(lobjSearchMember);
            if (lobjMemberDetails.LastName != null || lobjMemberDetails.LastName != string.Empty)
            {
                ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramDetailsByID(lobjMemberDetails.ProgramId);
                Framework.EnterpriseLibrary.PasswordReset.Entities.ResetPassword lobjResetPassword = new Framework.EnterpriseLibrary.PasswordReset.Entities.ResetPassword();
                lobjResetPassword.ProgramId = lobjProgramDefinition.ProgramId;
                //lobjResetPassword.RelationType = RelationType.Login;
                lobjResetPassword.RelationType = RelationType.LBMS;
                lobjResetPassword.UniqueIdentifier = pstrMemberid;
                lobjResetPassword.RequestIpAddress = HttpContext.Current.Request.UserHostAddress;
                lstrPWDReset = lobjModel.MemberResetPassword(lobjResetPassword);
                if (lstrPWDReset.Equals("Active"))
                {
                    lstrResponse = "Success";
                }
                else
                {
                    lstrResponse = "Failure";
                }
            }
            else
            {
                lstrResponse = "Failure";
            }
            lobjModel.LogActivity(string.Format(ActivityConstants.ForgotPassword, lstrResponse), ActivityType.ForgotPassword);
        }
        catch (ApplicationException ex)
        {
            lstrResponse = ex.Message;
        }
        catch (Exception)
        {
            lstrResponse = "Failure";
        }
        return lstrResponse;
    }
}