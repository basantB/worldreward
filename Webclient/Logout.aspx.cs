﻿using InfiPlanetModel.Model;
using Core.Platform.MemberActivity.Entities;
using Framework.EnterpriseLibrary.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            InfiModel objInfiModel = new InfiModel();
            objInfiModel.LogActivity(string.Format("Member Logout Page"), ActivityType.Logout);

            Session.Abandon();
            Response.Redirect("AuthMember.aspx");
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("LogOut Exception  : " + ex.Message + ex.StackTrace);
            Session.Abandon();
            Response.Redirect("AuthMember.aspx");
        }
    }
}