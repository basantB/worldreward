﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfiPlanetModel.Model;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using CB.IBE.Platform.Hotels.ClientEntities;
using Framework.Integrations.Hotels.Entities;
using Core.Platform.MemberActivity.Entities;

public partial class HotelDetails : System.Web.UI.Page
{
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string GetHotelInfo()
    {
        string pstrHotelId = Convert.ToString(HttpContext.Current.Session["hotelId"]);
        InfiModel lobjModel = new InfiModel();
        HotelInformationRequest lobjHotelInformationRequest = new HotelInformationRequest();
        lobjHotelInformationRequest.InformationRequest.hotelid = Convert.ToInt32(pstrHotelId);
        HotelInformationResponse lobjReturn = lobjModel.GetHotelInformation(lobjHotelInformationRequest);
        return JSONSerialization.Serialize(lobjReturn.HotelInformation);
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<Hotel> BindHotelDetails()
    {
        string pstrHotelId = Convert.ToString(HttpContext.Current.Session["hotelId"]);
        HotelSearchResponse lobjSearchResponse = HttpContext.Current.Session["hotels"] as HotelSearchResponse;
        List<Hotel> objHotelList = new List<Hotel>();
        objHotelList = lobjSearchResponse.SearchResponse.hotels.hotel.Where(abc => abc.hotelid.Equals(pstrHotelId)).ToList();
        HttpContext.Current.Session["SelectedRoomType"] = objHotelList;
        return objHotelList;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string GetHotelDetails(string hotelroomcode)
    {
        string pstrHotelId = Convert.ToString(HttpContext.Current.Session["hotelId"]);
        HotelSearchResponse lobjSearchResponse = HttpContext.Current.Session["hotels"] as HotelSearchResponse;
        List<Hotel> objHotelList = new List<Hotel>();
        objHotelList = lobjSearchResponse.SearchResponse.hotels.hotel.Where(abc => abc.hotelid.Equals(pstrHotelId)).ToList();
        Hotel objHotel = objHotelList[0];
        //RoomRates lobjroomrates = new RoomRates();
        //lobjroomrates = objHotel.roomrates;
        //List<RoomRate> lobjroomrate = new List<RoomRate>();
        //for (int i = 0; i < lobjroomrates.RoomRate.Count(); i++)
        //{
        //    lobjroomrate.Add(lobjroomrates.RoomRate[i]);
        //}
        //RoomType objroom = new RoomType();
        //int index = 0;
        //for (int i = 0; i < lobjroomrate.Count; i++)
        //{
        //    if (lobjroomrate[i].roomtype.roomtypecode == hotelroomcode)
        //    {
        //        objroom = lobjroomrate[i].roomtype;
        //        index = i;
        //    }
        //}
        string hotelobj = JSONSerialization.Serialize(objHotel);// lobjroomrates.RoomRate[index].ratebreakdown.rate[0].RatePoint.ToString();
        return hotelobj;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string Bookroom(string pstrroomtypecode)
    {
        InfiModel lobjModel = new InfiModel();
        lobjModel.LogActivity(string.Format("Hotel selected and proceed for booking"), ActivityType.HotelBooking);
        List<Hotel> objHotelList = HttpContext.Current.Session["SelectedRoomType"] as List<Hotel>;
        string urlLink = "HotelBookingDetails.aspx?hotelid=" + objHotelList[0].hotelid + "&roomtypecode=" + pstrroomtypecode;
        return urlLink;
    }


    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<Hotel> BindNextHotel()
    {
        string pstrHotelId = Convert.ToString(HttpContext.Current.Session["hotelId"]);
        HotelSearchResponse lobjSearchResponse = HttpContext.Current.Session["hotels"] as HotelSearchResponse;
        List<Hotel> objHotelList = new List<Hotel>();
        objHotelList = lobjSearchResponse.SearchResponse.hotels.hotel.ToList();//lobjSearchResponse.SearchResponse.hotels.hotel.Where(abc => abc.hotelid.Equals(pstrHotelId)).ToList();
        List<Hotel> LobjHotelsToPaint = ProcessHotels(objHotelList, Convert.ToInt32(pstrHotelId));


        return LobjHotelsToPaint;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //HotelSearchRequest lobjSearchRequest = Session["SearchDetails"] as HotelSearchRequest;
        //if (lobjSearchRequest != null)
        //{
        //    HFNoOfRooms.Value = lobjSearchRequest.SearchRequest.NoOfRooms.ToString();
        //    hdnNoAdult.Value = lobjSearchRequest.SearchRequest.AdultPerRoom.ToString();
        //    hdnNoChild.Value = lobjSearchRequest.SearchRequest.ChildrenPerRoom.ToString();
        //    txtCity.Value = lobjSearchRequest.SearchRequest.CountryISOCode.ToString() + ", " + lobjSearchRequest.SearchRequest.Country.ToString() + ", " + lobjSearchRequest.SearchRequest.CityName.ToString();
        //    TextBoxCheckin.Value = lobjSearchRequest.SearchRequest.CheckInDate.ToString("dd/MM/yyyy");
        //    TextBoxCheckout.Value = lobjSearchRequest.SearchRequest.CheckOutDate.ToString("dd/MM/yyyy");
        //}
    }


    private static List<Hotel> ProcessHotels(List<Hotel> lobjHotelList, int pintHotelID)
    {
        List<Hotel> lobjHotelListToPaint = new List<Hotel>();
        bool lblnFirstHotel = false;
        bool lblnLastHotel = false;
        int mintMaxItemsToDisplay = 6;
        int mintCurrentHotelID = pintHotelID;
        int lintCounter = 0;
        //Is the current Hotel the Last Hotel in the list..
        lblnLastHotel = IsCurrentHotelLastHotel(lobjHotelList, mintCurrentHotelID);
        if (lblnLastHotel) //Last Hotel .. and subsequent traversal
        {
            for (int h = 0; h < lobjHotelList.Count; h++)
            {
                if (lintCounter == mintMaxItemsToDisplay)
                    break;
                lobjHotelListToPaint.Add(lobjHotelList[h]);
                lintCounter++;
            }
        }
        else //Not the Last Hotel..that was selected..
        {
            Hotels lobjHotels = null;
            int lintNoOfHotelsToBeConsidered = 0;
            int lintCounterForInsufficientHotels = 0;
            for (int h = 0; h < lobjHotelList.Count; h++)
            {
                if (Convert.ToInt32(lobjHotelList[h].hotelid) == mintCurrentHotelID)
                {
                    lintNoOfHotelsToBeConsidered = ((lobjHotelList.Count - 1) - h);
                    //We have more hotels in the list than MaxRange of hotels to be displayed(ie.3 per grid).
                    if (mintMaxItemsToDisplay < lintNoOfHotelsToBeConsidered)
                    {
                        h++;
                        for (int i = h; i < lobjHotelList.Count; i++)
                        {
                            if (lintCounterForInsufficientHotels == mintMaxItemsToDisplay)
                                break;
                            lobjHotelListToPaint.Add(lobjHotelList[i]);
                            lintCounterForInsufficientHotels++;
                        }
                    }
                    //We have less hotels in the list than MaxRange of hotels to be displayed(ie.3 per grid).
                    else
                    {

                        // Check No of items in the list ,and Display subsequent hotels in succession.
                        int lintRemainingHotelsToDisplay = 0;
                        h++;
                        for (int i = h; i < lobjHotelList.Count; i++)
                        {
                            lobjHotelListToPaint.Add(lobjHotelList[i]);
                            lintRemainingHotelsToDisplay++;
                        }
                        int hotelsToDisplayFromTheBeginning = mintMaxItemsToDisplay - lintRemainingHotelsToDisplay;
                        for (int x = 0; x < hotelsToDisplayFromTheBeginning; x++)
                        {
                            lobjHotelListToPaint.Add(lobjHotelList[x]);
                        }
                    }
                }
            }
        }
        return lobjHotelListToPaint;
    }

    private static bool IsCurrentHotelLastHotel(List<Hotel> pobjHotelList, int pintCurrentHotelID)
    {
        bool lblnIsLastHotel = Convert.ToInt32(pobjHotelList[pobjHotelList.Count - 1].hotelid) == pintCurrentHotelID ? true : false;
        return lblnIsLastHotel;
    }


}