﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CB.IBE.Platform.Entities;
using Core.Platform.Member.Entites;
using Core.Platform.MemberActivity.Entities;
using InfiPlanetModel.Model;
using CB.IBE.Platform.Masters.Entities;
using System.Configuration;

public partial class AirPrintReceipt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            InfiModel objModel = new InfiModel();
            string lstrRelativePath = this.Page.AppRelativeVirtualPath;
            int lintActivityId = objModel.LogActivity(lstrRelativePath.Replace("~", string.Empty).Replace("/", string.Empty), ActivityType.PageLoad);

        }
        if (Session["PageTitle"] != null)
        {
            this.Title = Session["PageTitle"].ToString();
        }
        if (!IsPostBack)
        {
            if (Session["PageTitle"] != null)
            {
                this.Title = Convert.ToString(Session["PageTitle"]);
            }

            if ((Session["RetriveBookingInfo"] != null || Session["FlightBooked"] != null) && Session["MemberDetails"] != null)
            {
                string lstrSourceCurrency = Convert.ToString(ConfigurationManager.AppSettings["SourceCurrency"]);
                ItineraryDetails lobjItineraryDetails = new ItineraryDetails();
                string strPaymentDetails = string.Empty;
                if (Session["FlightBooked"] != null)
                {
                    lobjItineraryDetails = Session["FlightBooked"] as ItineraryDetails;
                    if (lobjItineraryDetails.BookingPaymentDetails.PaymentType.Equals(PaymentType.Points))
                    {
                        lblTotalMiles.Text = Convert.ToString(lobjItineraryDetails.BookingPaymentDetails.Points);
                    }
                }
                else if (Session["RetriveBookingInfo"] != null)
                {
                    lobjItineraryDetails = Session["RetriveBookingInfo"] as ItineraryDetails;

                    if (lobjItineraryDetails.BookingPaymentDetails.PaymentType.Equals(PaymentType.Points))
                    {
                        lblTotalMiles.Text = Convert.ToString(lobjItineraryDetails.BookingPaymentDetails.Points);
                    }
                }
                else
                {
                    lobjItineraryDetails = null;
                }
                MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
                string lstrMemberId = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;

                lblMembershipReferenceNo.Text = lstrMemberId;
                lblCustomerName.Text = lobjMemberDetails.FirstName + " " + lobjMemberDetails.LastName;
                //lblMemberName.Text = lobjMemberDetails.FirstName + " " + lobjMemberDetails.LastName;
                lblCustomerMobileNo.Text = lobjMemberDetails.MobileNumber;
                lblCustomerAddress.Text = lobjMemberDetails.Address1;
                lblCustomerEmail.Text = lobjMemberDetails.Email;

                lblTransactionRefNo.Text = lobjItineraryDetails.ItineraryReference;

                InfiModel lobjModel = new InfiModel();
                int lintActivityId1 = lobjModel.LogActivity(string.Format("Air Reciept. Member Id : {0}, Transaction Reference - {1}", lstrMemberId, lobjItineraryDetails.ItineraryReference), ActivityType.FlightBooking);

            }
            else
            {
                Response.Redirect("Index.aspx");
            }
        }
    }
    static string UppercaseFirst(string s)
    {
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        char[] a = s.ToCharArray();
        a[0] = char.ToUpper(a[0]);
        return new string(a);
    }
}