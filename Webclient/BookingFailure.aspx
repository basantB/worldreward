﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="BookingFailure.aspx.cs" Inherits="BookingFailure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <div>
        <script type="text/javascript" language="javascript">
            $(window).load(function () {
                $.ajax({
                    type: 'POST',
                    url: 'BookingFailure.aspx/BookingFail',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: "",
                    cache: false,
                    success: function (msg) {
                        if (msg.d != "") {
                            $("#<%=lblErrormsg.ClientID%>").text(msg.d);
                        }
                        else {
                            window.location = "index.aspx";
                        }
                    },
                    error: function (errmsg) {
                        window.location = "index.aspx";
                    }
                });
            });
        </script>
    </div>
    <div class="LoginOuter">
        <div class="HeaderText">
            Booking Failure</div>
        <div class="common_div" style="padding: 10% 0; text-align: center; font-size: 20px">
            <asp:Label ID="lblErrormsg" runat="server"></asp:Label>
            <br />
            <a href="Index.aspx" target="_self">Please try again later</a>
        </div>
    </div>
</asp:Content>
