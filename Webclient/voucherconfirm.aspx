﻿ <%@ Page Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="voucherconfirm.aspx.cs" Inherits="voucherconfirm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
<div class="banner">
        <div style="max-width: 100%;" class="bx-wrapper"><div style="width: 100%; overflow: hidden; position: relative; height: 436px;" class="bx-viewport">
            <ul style="width: auto; position: relative;" class="bxslider" id="Indexbanner">
            <li>
                <img src="images/bnr-gift.jpg"></li>
                   </ul>
                   </div>
				   </div>
               </div>
	<div style="text-align: center; margin-top: 10px;">
        <a href="#article" id="dnarr">
            <img style="opacity: .5; height: 50px;" src="images/down-arrow.gif"></a>
    </div>
	<article id="article">
        <div class="wrap">
            <h1>Every Reason To Feel Wonderful.</h1>

            <p>Let your gift be as special as the thought itself. At WorldRewardZ, you can now redeem your points for a gift voucher from leading brands. You have the choice of vouchers across various value denominations, to make gifting more joyful and every moment big or small, memorable!  
</p>
		<div class="vouWrap vouConfirm">
			<table cellspacing="0" cellpadding="0" border="0">
                <thead>
                    <tr>
                        <th>
                            Voucher Type
                        </th>
                        <th>
                            Quantity
                        </th>
                        <th>
                            Voucher Value
                        </th>
                        <th>
                            WorldRewardZ Required
                        </th>
                    </tr>
                </thead>
        
            <tbody>
                <tr>
                    <td>
                        WorldRewardZ Gift Voucher - 100
                    </td>
                    <td>
                        1   
                    </td>
                    <td>
                       100
                    </td>
                    <td>
                        250
                    </td>

                </tr>
            </tbody>
        
            <tbody>
            </tr>
            </tbody> </table>
			
			<div class="btmInfo1">
				
				<div class="divRight">
				<a href="voucher.aspx" class="btnBlk">Back</a>
				<a href="#" class="btnRed">Confirm</a>
				</div>
				<br class="clr">
			</div>
		</div>
</article>
</asp:Content>
