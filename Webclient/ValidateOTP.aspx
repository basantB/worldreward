﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="ValidateOTP.aspx.cs" Inherits="ValidateOTP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <div class="LoginOuter">
        <div class="HeaderText">
            Validate OTP</div>
        <div class="common_div ">
            <div class="Width100">
                <div class="LoginContainer">
                    <div id="OTP1" class="Home_Tab_CommonDiv mtop_10 lineHeight">
                        One Time Password (OTP) has been sent to your registered mobile number.<br />
                        Once received please enter the OTP below to complete your
                        <asp:Label ID="lblmsg" runat="server"></asp:Label>
                    </div>
                    <div class="Home_Tab_CommonDiv mTop_20" id="OTP2">
                        <div class="Width52">
                            <div class="fleft width40p">
                                One Time Password</div>
                            <div class="fleft width40p">
                                <asp:TextBox ID="txtOTP" TextMode="Password" ValidationGroup="Book" runat="server"
                                    Text="" CssClass="textboxStyle TxtBox_Width100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="Width52 mtop_10" id="Div7">
                            <div class="fleft width40p" id="Div8">
                                &nbsp;</div>
                            <div class="fleft">
                                <asp:ImageButton ID="ButtonBook" ImageUrl="~/Images/Confirm.png" onmouseover="this.src='Images/Confirm_Hover.png'"
                                    onmouseout="this.src='Images/Confirm.png'" runat="server" ValidationGroup="Book"
                                    OnClientClick="var retvalue = BookingValidation();event.returnValue= retvalue;if(event.preventDefault)event.preventDefault();  return retvalue;" />
                            </div>
                        </div>
                    </div>
                    <div class="Home_Tab_CommonDiv mtop_10" id="Div1">
                        <div class="Width100 mtop_10" id="Div2">
                            <div class="fleft" id="Div3" style="width: 20.75%">
                                &nbsp;</div>
                            <div class="fleft Width74">
                                <div id="validationResult" class="ErrorMsgContainer Width100">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        //$(document).ready(function () {
        //    var lstrflag = getQuerystring("flag")
        //    alert(lstrflag);
        //    if (lstrflag == 'Cashback') {
        //        alert("CashBack redemption request"+lstrflag);
        //        $("#CP_lblmsg").text("CashBack redemption request.");
        //    }
        //    else {
        //        alert("booking" + lstrflag);
        //        $("#CP_lblmsg").text("booking request.");
        //    }
        //});
        //function showMyDiv() {
        //    $("#OTP1").css("display", "none");
        //    $("#OTP2").css("display", "block");
        //    var lstrflag = getQuerystring("flag")
        //    if (lstrflag == 'Cashback') {
        //        $("#ct_CBI_ButtonBook").attr("src", "Images/Continue.png");
        //    }
        //    else {
        //        $("#ct_CBI_ButtonBook").attr("src", "Images/BookNow.png");
        //    }
        //    return false;
        //}

        //function getQuerystring(key, default_) {
        //    if (default_ == null) default_ = "";
        //    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        //    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
        //    var qs = regex.exec(window.location.href);
        //    if (qs == null)
        //        return default_;
        //    else
        //        alert("url" + qs[1]);
        //        return qs[1];
        //}

       function BookingValidation() {
                debugger;
            $("#validationResult").hide();
            var msg = "";
            $("#validationResult")[0].innerHTML = '';
            if ($('#CP_txtOTP').val() == '') {
                msg += "Please enter received One Time Password.<br/>";
            }
            if (msg.length > 0) {
                $("#validationResult").show();
                $("#validationResult")[0].innerHTML = msg;
                $("#validationResult").css('color', '#ff0000');
                return false;
            }
            else {

                var pstrOTP = $("#CP_txtOTP").val();
                //var strFlag = getQuerystring("flag");
                var strFlag = "Cashback";
                alert("pstrOTP" + pstrOTP);
                $.ajax({
                    url: 'ValidateOTP.aspx/CheckOTP',
                    type: 'POST',  // or get
                    contentType: 'application/json; charset =utf-8',
                    data: "{'pstrOTP':'" + pstrOTP.toString() + "'" + "," + "'strFlag':'" + strFlag + "'}",
                    dataType: 'json',
                    success: function (data) {

                        //if (data.d == 'Air')
                        //    window.location = "PointGateway.aspx";
                        //else if (data.d == 'Hotel')
                        //    window.location = "PointGateway.aspx?flag=Hotel";
                        //else if (data.d == 'Car')
                        //    window.location = "PointGateway.aspx?flag=Car";
                        if (data.d == 'Cashback')
                          window.location = "PointGateway.aspx?flag=Cashback";
                        else if (data.d == 'Invalid Credentials') {
                            $("#validationResult").show();
                            $("#validationResult")[0].innerHTML = data.d + ' <br />';
                            $("#validationResult").css('color', '#ff0000');
                            $("#OTP1").hide();
                            alert("data" + data.d);
                            return false;
                        }
                    },
                    error: function (errmsg) {

                    }
                });
                return false;
            }
            }
            
    </script>
</asp:Content>
