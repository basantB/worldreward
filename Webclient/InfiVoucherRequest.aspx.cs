﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Member.Entites;
using Core.Platform.Merchant.Entities;
using InfiPlanetModel.Model;
using System.Configuration;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.PointGateway.Service.Helper;
using Core.Platform.PointGateway.Service.Helper.PGService;
using System.Text;
using Framework.EnterpriseLibrary.Security;
using Framework.EnterpriseLibrary.Security.Constants;

public partial class InfiVoucherRequest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["MemberDetails"] != null)
            {

                MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                InfiModel lobjModel = new InfiModel();
                lobjMerchantDetails.MerchantCode = ConfigurationSettings.AppSettings["InfiVoucherMerchantId"].ToString();
                lobjMerchantDetails.Password = ConfigurationSettings.AppSettings["InfiVoucherMerchantPwd"].ToString();

                MerchantDetails lobjCurrentMerchantDetails = null;
                LoggingAdapter.WriteLog("MerchantDetails -" + lobjMerchantDetails.MerchantCode + " / " + lobjMerchantDetails.Password);
                lobjCurrentMerchantDetails = lobjModel.GetMerchantDetails(lobjMerchantDetails);
                if (lobjCurrentMerchantDetails == null)
                {
                    Response.Redirect("InvalidCredential.aspx");
                }
                string lstrSecurityKey = Convert.ToString(Guid.NewGuid());
                string lstrResponseDetails = string.Empty;
                Response lobjResponse = new Response();
                PGServiceHelper lobjPGServiceHelper = new PGServiceHelper();
                PGManagerClient lobjPGManagerClient = new PGManagerClient();
                string lstrCurrency = lobjModel.GetDefaultCurrency();
                lobjResponse = lobjPGManagerClient.CheckPointsAvailability(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency, lobjCurrentMerchantDetails.MerchantCode, lobjCurrentMerchantDetails.UserName, lobjCurrentMerchantDetails.Password);

                if (lobjResponse.IsSucessful)
                {
                    lstrResponseDetails = "0" + "|" + lobjMemberDetails.MemberRelationsList[0].RelationReference + "|" + lobjMemberDetails.LastName + "|" + lobjMemberDetails.Email + "|" + lobjResponse.ReturnObject + "|" + lstrSecurityKey + "|" + "Default";
                }
                else
                {
                    lstrResponseDetails = "1" + "|" + lobjResponse.ExceptionMessage;
                }

                string lstrEncryptedResponseDetails = RSAEncryptor.EncryptString(lstrResponseDetails, RSASecurityConstant.KeySize, RSASecurityConstant.RSAPublicKey);
                StringBuilder sb = new StringBuilder();
                sb.Append("<html>");
                sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
                sb.AppendFormat("<form name='form' action='{0}' method='post'>", lobjCurrentMerchantDetails.CallBackURL);
                sb.AppendFormat("<input type='hidden' runat='server' name='Response_Details' value='{0}'>", lstrEncryptedResponseDetails);
                sb.Append("</form>");
                sb.Append("</body>");
                sb.Append("</html>");
                Response.Write(sb.ToString());
                Response.End();

            }
            else
            {
                Response.Redirect("http://103.13.98.7/InfiPlanetvoucher/");
            }
        }
    }
}