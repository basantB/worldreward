﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfiPlanetModel.Model;
using Core.Platform.ProgramMaster.Entities;
using System.Text.RegularExpressions;
using Core.Platform.MemberActivity.Constants;
using Core.Platform.MemberActivity.Entities;
using Core.Platform.Member.Entites;
using Core.Platform.OTP.Entities;
using Core.Platform.Transactions.Entites;
using Core.Platform.ProgramMaster.Entities;
using Framework.EnterpriseLibrary.Adapters;

public partial class FamilyPoolRequest : System.Web.UI.Page
{
    bool lboolUnMerge = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["MemberDetails"] == null)
            {
                //txtAccountNumber.Text = Session["MemberID"].ToString();
                Response.Redirect("Login.aspx");
            }
            else
            {
                BindPoolingDetails();
                BindTransactionSummaryInfo();

                for (int count = 0; count < RepPoolRecords.Items.Count; count++)
                {
                    HyperLink lab = (HyperLink)RepPoolRecords.Items[count].FindControl("HyperLink1");
                    if (lboolUnMerge)
                        lab.Visible = false;
                }
            }
        }
    }

    public void BindTransactionSummaryInfo()
    {

        InfiModel lobjInfiModel = new InfiModel();
        List<MemberDetails> lobjListMemberDetails = new List<MemberDetails>();
        MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
        SearchMember lobjSearchMember = new SearchMember();
        lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
        lobjSearchMember.ProgramId = lobjMemberDetails.ProgramId;

        bool IsChild = false;
        string pstrPrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).PrimaryReference;
        if (pstrPrimaryReference != string.Empty)
            IsChild = true;

        if (pstrPrimaryReference == null || pstrPrimaryReference.Equals(string.Empty))
        {
            lobjSearchMember.PrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).RelationReference;
        }
        else
        {
            lobjSearchMember.PrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).PrimaryReference;
        }
        lobjListMemberDetails = lobjInfiModel.GetMemberDetailByPrimaryReference(lobjSearchMember);
        lobjListMemberDetails = lobjListMemberDetails.GroupBy(c => c.Id, (key, c) => c.FirstOrDefault()).ToList();

        ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramMaster();
        SearchTransactions objSearchTransactions = new SearchTransactions();

        string pstrRelRef = string.Empty;
        List<TransactionDetails> LobjTransactionDetails = new List<TransactionDetails>();
        List<TransactionDetails> LobjTransactionDetailsNew = new List<TransactionDetails>();

        for (int i = 0; i < lobjListMemberDetails.Count; i++)
        {
            //pstrRelRef += "'" + lobjListMemberDetails[i].MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).RelationReference + "'" + ",";

            objSearchTransactions.RelationReference = lobjListMemberDetails[i].MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).RelationReference;
            objSearchTransactions.ProgramId = lobjProgramDefinition.ProgramId;
            objSearchTransactions.RelationType = Convert.ToInt32(RelationType.LBMS);
            LobjTransactionDetailsNew = lobjInfiModel.GetRedeemTransactionDetails(objSearchTransactions);
            if (LobjTransactionDetailsNew != null)
                LobjTransactionDetails.AddRange(LobjTransactionDetailsNew);
        }

        //if (pstrRelRef != string.Empty)
        //    pstrRelRef = pstrRelRef.Substring(0, pstrRelRef.Length - 1);



        if (LobjTransactionDetails != null && LobjTransactionDetails.Count > 0)
        {
            for (int i = 0; i < lobjListMemberDetails.Count; i++)
            {
                string pstrAdd1 = lobjListMemberDetails[i].AdditionalDetails1;
                if (pstrAdd1 != string.Empty && pstrAdd1 != null)
                {
                    DateTime ldate = Convert.ToDateTime(pstrAdd1);
                    for (int k = 0; k < LobjTransactionDetails.Count; k++)
                    {
                        TransactionDetails lobj = LobjTransactionDetails.Find(x => x.TransactionDate >= ldate);
                        if (lobj != null)
                        {
                            lboolUnMerge = true;
                            break;
                        }
                    }
                }
            }
        }

    }

    private void BindPoolingDetails()
    {
        try
        {
            InfiModel lobjInfiModel = new InfiModel();
            List<MemberDetails> lobjListMemberDetails = new List<MemberDetails>();
            MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
            SearchMember lobjSearchMember = new SearchMember();
            lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
            lobjSearchMember.ProgramId = lobjMemberDetails.ProgramId;

            bool IsChild = false;
            string pstrPrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).PrimaryReference;
            if (pstrPrimaryReference != string.Empty && pstrPrimaryReference != null)
                IsChild = true;

            if (pstrPrimaryReference == null || pstrPrimaryReference.Equals(string.Empty))
            {
                lobjSearchMember.PrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).RelationReference;
            }
            else
            {
                lobjSearchMember.PrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).PrimaryReference;
            }
            lobjListMemberDetails = lobjInfiModel.GetMemberDetailByPrimaryReference(lobjSearchMember);
            lobjListMemberDetails = lobjListMemberDetails.GroupBy(c => c.Id, (key, c) => c.FirstOrDefault()).ToList();
            Session["MemberDetailsList"] = lobjListMemberDetails;

            if (lobjListMemberDetails != null && lobjListMemberDetails.Count > 0)
            {
                int lintId = 0;

                if (!IsChild)
                {
                    lintId = lobjListMemberDetails.Find(x => x.MemberRelationsList.Any(p => p.RelationType.Equals(RelationType.LBMS) && p.PrimaryReference.Equals(string.Empty))).Id;
                    lobjListMemberDetails = lobjListMemberDetails.Where(x => x.Id != lintId).ToList();
                }
                else
                {
                    lobjListMemberDetails = lobjListMemberDetails.FindAll(x => x.MemberRelationsList.Find(y => y.RelationType.Equals(RelationType.LBMS)).RelationReference.Equals(lobjMemberDetails.MemberRelationsList[0].PrimaryReference));
                }
                if (lobjListMemberDetails.Count > 0)
                {
                    RepPoolRecords.DataSource = lobjListMemberDetails;
                    RepPoolRecords.DataBind();
                }
                else
                {
                    RepPoolRecords.DataSource = null;
                    RepPoolRecords.DataBind();
                    divRepPoolRecords.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("BindPoolingDetails Ex-" + ex.Message + ex.StackTrace + ex.InnerException);
        }
    }

    protected void RepPoolRecords_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "UnMergeCmd")
        {
            //var personId = (HiddenField)RepPoolRecords.Items[0].FindControl("poolId");
        }
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        Button button = (sender as Button);
        string commandArgument = button.CommandArgument;
        RepeaterItem item = button.NamingContainer as RepeaterItem;
        int index = item.ItemIndex;

        //var personId = (HiddenField)RepPoolRecords.Items[0].FindControl("poolId");
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string CheckFamilyMember()
    {
        string strCheckFamilyMember = string.Empty;
        int intFamilyMemberCount = 4;
        InfiModel lobjInfiModel = new InfiModel();
        MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
        MemberRelation lobjMemberRelation = new MemberRelation();
        lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobjRelation => lobjRelation.RelationType.Equals(RelationType.LBMS));

        if (lobjMemberRelation.PrimaryReference == null || lobjMemberRelation.PrimaryReference == string.Empty)
        {
            List<MemberPoolingDetails> lstlobjChildMemberPoolingDetails = new List<MemberPoolingDetails>();

            MemberPoolingDetails lobjMemberPoolingDetails = new MemberPoolingDetails();
            lobjMemberPoolingDetails.InitiatorDetails = lobjMemberRelation.RelationReference;
            lstlobjChildMemberPoolingDetails = lobjInfiModel.GetMemberPoolingDetails(lobjMemberPoolingDetails);
            lstlobjChildMemberPoolingDetails = lstlobjChildMemberPoolingDetails.FindAll(obj => obj.ActionType.Equals(PoolingStatusEnum.SUCCESS));

            if (lstlobjChildMemberPoolingDetails != null && lstlobjChildMemberPoolingDetails.Count > 0)
            {
                lstlobjChildMemberPoolingDetails = lstlobjChildMemberPoolingDetails.FindAll(x => x.PoolingType.Equals(PoolingTypeEnum.MERGE));
                if (lstlobjChildMemberPoolingDetails != null && lstlobjChildMemberPoolingDetails.Count > 0)
                {
                    //call getAllChild From Parent;
                    if (lstlobjChildMemberPoolingDetails.Count >= intFamilyMemberCount)
                    {
                        strCheckFamilyMember = "Exceed";
                    }
                    else
                    {
                        strCheckFamilyMember = "Success";
                    }
                }
                else
                {
                    strCheckFamilyMember = "Error";
                }
            }
            else
            {
                strCheckFamilyMember = "Success";
            }
        }
        else
        {
            strCheckFamilyMember = "Child";
        }

        return strCheckFamilyMember;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string GenerateOTP(string pstrNationalId)
    {
        InfiModel lobjModel = new InfiModel();

        MemberDetails lobjInitiatorMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;

        MemberDetails lobjRequestorMemberDetails = null;
        string mstrRedirectEmptyURL = string.Empty;
        bool lblstatus = false;
        try
        {
            bool lbolSelf = lobjInitiatorMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).RelationReference.Equals(pstrNationalId);

            lobjRequestorMemberDetails = lobjModel.GetMemberDetails(pstrNationalId);
            if (lobjRequestorMemberDetails.NationalId != string.Empty && !lbolSelf)
            {
                LoggingAdapter.WriteLog("member Detail not null");
                MemberRelation lobjRequestorMemberRelation = lobjRequestorMemberDetails.MemberRelationsList.Find(lobjRelation => lobjRelation.RelationType.Equals(RelationType.LBMS));
                if (lobjRequestorMemberRelation != null)
                {
                    if (lobjRequestorMemberRelation.PrimaryReference == string.Empty || lobjRequestorMemberRelation.PrimaryReference == null)
                    {
                        if (lobjRequestorMemberRelation.IsAccountActivated && lobjRequestorMemberRelation.Status.Equals(Status.Active))
                        {
                            //check whether requestor is parent or not
                            MemberDetails lobjCheckerMemberDetails = new MemberDetails();
                            List<MemberRelation> lstlobjCheckerMemberRelation = new List<MemberRelation>();
                            MemberRelation lobjCheckerMemberRelation = new MemberRelation();

                            lobjCheckerMemberRelation.PrimaryReference = lobjRequestorMemberRelation.RelationReference;
                            lobjCheckerMemberDetails.ProgramId = lobjRequestorMemberDetails.ProgramId;
                            lobjCheckerMemberRelation.RelationType = lobjRequestorMemberRelation.RelationType;
                            lstlobjCheckerMemberRelation.Add(lobjCheckerMemberRelation);
                            lobjCheckerMemberDetails.MemberRelationsList = lstlobjCheckerMemberRelation;


                            bool lblnPrimaryReference = false;
                            lblnPrimaryReference = lobjModel.PrimaryReferenceExist(lobjCheckerMemberDetails);

                            if (!lblnPrimaryReference)
                            {
                                string lstrSourceIpAddress = HttpContext.Current.Request.UserHostAddress;
                                lblstatus = lobjModel.GenerateOTPFamilyPool(lobjRequestorMemberRelation.RelationReference, lstrSourceIpAddress);

                                if (lblstatus)
                                {
                                    mstrRedirectEmptyURL = "Success";
                                    lobjModel.LogActivity(string.Format(ActivityConstants.ActivationOTP, pstrNationalId, mstrRedirectEmptyURL), ActivityType.ActivationOTPSuccess);
                                }
                                else
                                {
                                    mstrRedirectEmptyURL = "Please check the details";
                                    lobjModel.LogActivity(string.Format(ActivityConstants.ActivationOTP, pstrNationalId, mstrRedirectEmptyURL), ActivityType.ActivationOTPFailed);
                                }
                            }
                            else
                            {
                                mstrRedirectEmptyURL = "Not allowed to add Parent as Family Pool.";
                                lobjModel.LogActivity(string.Format(ActivityConstants.ActivationOTP, pstrNationalId, mstrRedirectEmptyURL), ActivityType.ActivationOTPFailed);
                            }
                        }
                        else
                        {
                            mstrRedirectEmptyURL = "Account not activated.";
                            lobjModel.LogActivity(string.Format(ActivityConstants.ActivationOTP, pstrNationalId, mstrRedirectEmptyURL), ActivityType.ActivationOTPFailed);
                        }
                    }
                    else
                    {
                        mstrRedirectEmptyURL = "Already a family Member.";
                        lobjModel.LogActivity(string.Format(ActivityConstants.ActivationOTP, pstrNationalId, mstrRedirectEmptyURL), ActivityType.ActivationOTPFailed);
                    }
                }
                else
                {
                    mstrRedirectEmptyURL = "Invalid credentials.";
                    lobjModel.LogActivity(string.Format(ActivityConstants.ActivationOTP, pstrNationalId, mstrRedirectEmptyURL), ActivityType.ActivationOTPFailed);
                }
            }
            else
            {
                mstrRedirectEmptyURL = "Invalid credentials.";
                lobjModel.LogActivity(string.Format(ActivityConstants.ActivationOTP, pstrNationalId, mstrRedirectEmptyURL), ActivityType.ActivationOTPFailed);
            }
            return mstrRedirectEmptyURL;
        }
        catch (Exception ex)
        {
            mstrRedirectEmptyURL = "Invalid credentials.";
            LoggingAdapter.WriteLog("GenerateOTP Page - " + ex.Message + "StackTrace" + ex.StackTrace);
            return mstrRedirectEmptyURL;
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string CheckOTP(string pstrNationalId, string pstrOTP)
    {
        string mstrStatus = string.Empty;
        MemberDetails lobjInitiatorMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;

        MemberDetails lobjRequestorMemberDetails = null;
        InfiModel lobjModel = new InfiModel();
        lobjRequestorMemberDetails = lobjModel.GetMemberDetails(pstrNationalId);
        if (lobjInitiatorMemberDetails != null)
        {
            OTPDetails lobjOTPDetails = new OTPDetails();
            string lstrDestIpAddress = HttpContext.Current.Request.UserHostAddress;
            string lstrDestAddress = "Web";
            lobjOTPDetails.UniquerefID = lobjRequestorMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
            lobjOTPDetails.OTP = Convert.ToInt32(pstrOTP);
            lobjOTPDetails.DestinationAddress = lstrDestIpAddress;
            lobjOTPDetails.Destination = lstrDestAddress;
            lobjOTPDetails.OtpType = OTPEnumTypes.FAMILYPOOLINGMERGE.ToString();
            lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.FAMILYPOOLINGMERGE;
            bool Status = lobjModel.CheckOTPExist(lobjOTPDetails);
            if (Status)
            {
                MemberPoolingDetails lobjMemberPoolingDetails = new MemberPoolingDetails();
                lobjMemberPoolingDetails.ProgramId = lobjInitiatorMemberDetails.ProgramId;
                lobjMemberPoolingDetails.InitiatorDetails = lobjInitiatorMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjMemberPoolingDetails.RequestorDetails = lobjRequestorMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjMemberPoolingDetails.RequestOn = "Web";
                lobjMemberPoolingDetails.RequestMessage = "GenerateOTP";
                lobjMemberPoolingDetails.PoolingStatus = PoolingStatusEnum.SUCCESS;
                lobjMemberPoolingDetails.PoolingType = PoolingTypeEnum.MERGE;
                lobjMemberPoolingDetails.CreatedOn = DateTime.Now;
                lobjMemberPoolingDetails.CreatedBy = lobjInitiatorMemberDetails.Email;

                int pintMemberPooling = lobjModel.InsertMemberPoolingDetails(lobjMemberPoolingDetails);
                if (pintMemberPooling > 0)
                {
                    MemberDetails lobjNewMemberDetails = new MemberDetails();
                    MemberRelation lobjNewMemberRelation = new MemberRelation();
                    List<MemberRelation> lstlobjNewMemberRelation = new List<MemberRelation>();

                    lobjNewMemberRelation.RelationReference = lobjRequestorMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    lobjNewMemberRelation.PrimaryReference = lobjInitiatorMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    lobjNewMemberRelation.RelationType = RelationType.LBMS;

                    lstlobjNewMemberRelation.Add(lobjNewMemberRelation);
                    lobjNewMemberDetails.MemberRelationsList = lstlobjNewMemberRelation;

                    bool lblnStatus = false;
                    lblnStatus = lobjModel.UpdatePrimaryReference(lobjNewMemberDetails);
                    if (lblnStatus)
                    {
                        mstrStatus = "Success";
                    }
                    else
                    {
                        mstrStatus = "UpdatePrimaryReferenceFailed";
                    }
                }
                else
                {
                    mstrStatus = "MemberPoolingFailed";
                }
            }
            else
            {
                mstrStatus = "AuthenticationFailed";
            }
        }
        else
        {
            mstrStatus = "AuthenticationFailed";
        }
        return mstrStatus;
    }


    protected void RepPoolRecords_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //HyperLink lab = e.Item.FindControl("HyperLink1") as HyperLink;
            //if (lboolUnMerge)
            //    lab.Visible = false;
        }
    }
}