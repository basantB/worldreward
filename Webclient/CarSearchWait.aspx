﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CarSearchWait.aspx.cs" Inherits="CarSearchWait" MasterPageFile="~/InfiPlanetMaster.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <link href="Css/main.css" rel="stylesheet" />--%>
    <script language="javascript" type="text/javascript">

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.search);
            if (results == null)
                return "";
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        function getQuerystring(key, default_) {
            if (default_ == null) default_ = "";
            key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
            var qs = regex.exec(window.location.href);
            if (qs == null)
                return default_;
            else
                return qs[1];
        }
        $(document).ready(function () {
            var pickupCity = getQuerystring("pickupCity");
            pickupCity = pickupCity.replace('%20', ' ');
            pickupCity = pickupCity.replace('%20', ' ');
            var pickupLocation = getQuerystring("pickupLocationName");
            pickupLocation = pickupLocation.replace('%20', ' ');
            pickupLocation = pickupLocation.replace('%20', ' ');
            // var dropofCountry = getQuerystring("dropofCountry");
            var dropofCity = getQuerystring("dropofCity");
            dropofCity = dropofCity.replace('%20', ' ');
            dropofCity = dropofCity.replace('%20', ' ');
            var dropoffLocation = getQuerystring("DropoffLocationName");
            dropoffLocation = dropoffLocation.replace('%20', ' ');
            dropoffLocation = dropoffLocation.replace('%20', ' ');
            var departDate = getQuerystring("departDate");
            departDate = departDate.replace('%20', ' ');
            var departTime = getQuerystring("departTime");
            departTime = departTime.replace('%20', ' ');
            var arrDate = getQuerystring("arrDate");
            arrDate = arrDate.replace('%20', ' ');
            var arrTime = getQuerystring("arrTime");
            arrTime = arrTime.replace('%20', ' ');

            var SearchDetails = pickupLocation;// + ' (' + pickupCity + ') ' + 'To ' + dropoffLocation + '(' + dropofCity + ')' + ',' + departDate + ',' + departTime + ' - ' + arrDate + ',' + arrTime

            $("#CP_LabelYourSearchDetails").text($.trim(SearchDetails));
            $.ajax({
                type: 'POST',
                url: document.URL.toString().replace("CarSearchWait.aspx", "CarSearchWait.aspx/CarSearch"),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: '',
                success: function (msg) {
                    if (msg.d != false)
                        window.location = "CarList.aspx";
                    else
                        window.location = "NoResultFound.aspx?ERR=CarNOTFOUND";
                },
                error: function () {
                    window.location = "ErrorPage.aspx";
                }
            });
        });

    </script>
    <%--<title>Search Wait</title>
</head>

<body class="scrhpg">
    <form id="form2" runat="server">--%>
    <div class="srchcircle">
        <div class="scrhcnt">
            <div>
                <img src="images/icocar.png" />
            </div>
            <h1>Searching Cars</h1>
            <p>
                <asp:Label ID="LabelYourSearchDetails" runat="server"></asp:Label>
            </p>
            <div id="cssload-wrapper">
                <div id="cssload-border">
                    <div id="cssload-whitespace">
                        <div id="cssload-line">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<%--</form>
</body>
</html>--%>
