﻿using Framework.EnterpriseLibrary.Adapters;
using InfiPlanet.SessionConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Viator.Platform.ClientEntities;

public partial class SearchTour : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetDestinationList(string prefixText)
    {
        List<string> lobjLondon = new List<string>();
        try
        {
            List<string> lobjRetValue = new List<string>();
            prefixText = prefixText.ToLower();
            LoggingAdapter.WriteLog("prefixText :-" + prefixText);
            if (HttpContext.Current.Application[SessionFields.TourDestinationList] != null)
            {
                LoggingAdapter.WriteLog("prefixText1 :-" + prefixText);
                List<DestinationDetails> lobjDestinationDetails = new List<DestinationDetails>();
                lobjDestinationDetails = HttpContext.Current.Application[SessionFields.TourDestinationList] as List<DestinationDetails>;

                if (lobjDestinationDetails != null && lobjDestinationDetails.Count > 0)
                {
                    lobjDestinationDetails.ForEach(Destinationfield =>
                    {
                        if (Destinationfield.DestinationName.ToLower().IndexOf(prefixText) >= 0)
                        {
                            lobjRetValue.Add(Destinationfield.DestinationId + "|" + Destinationfield.DestinationName);
                        }

                    });
                }
                lobjRetValue.AddRange(lobjLondon);
                return lobjRetValue;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
          LoggingAdapter.WriteLog("GetDestinationList :-" + ex.Message + ex.InnerException + "Stack Trace-" + ex.StackTrace);
            return null;
        }

    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static bool SetDestinationId(string pstrDestinationId)
    {
        //var list = pstrDestinationId.Split('|');
        // string DestinationId = list[0];
        LoggingAdapter.WriteLog("DestinationId :-" + pstrDestinationId);
        bool lblnSuccess = false;
        try
        {
            if (!string.IsNullOrEmpty(pstrDestinationId))
            {
                HttpContext.Current.Session[SessionFields.SelectedTourDestinationId] = pstrDestinationId;
                lblnSuccess = true;
            }


        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("SetDestinationId :-" + ex.Message + ex.InnerException + "Stack Trace-" + ex.StackTrace);
        }

        return lblnSuccess;
    }
}