﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Member.Entites;
using Core.Platform.Merchant.Entities;
using InfiPlanetModel.Model;
using System.Configuration;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.PointGateway.Service.Helper;
using Core.Platform.PointGateway.Service.Helper.PGService;
using Framework.EnterpriseLibrary.Security;
using Framework.EnterpriseLibrary.Security.Constants;
using System.Text;
using Framework.EnterpriseLibrary.UniqueNumberGenerator;
using Core.Platform.Authentication.Entities;

public partial class InfiShopRequest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string lstRedirectUrl = string.Empty;
        string redirectionURL = "";
        if (Request.QueryString["Url"] != null && Request.QueryString["Url"] != "")
        {
            redirectionURL = Request.QueryString["Url"].ToString();
        }
        else
        {
            redirectionURL = "Default";
        }
        if (Session["MemberDetails"] != null)
        {
            string lstrAuthenticationToken = string.Empty;

            MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;

            MerchantDetails lobjMerchantDetails = new MerchantDetails();
            InfiModel lobjModel = new InfiModel();
            lobjMerchantDetails.MerchantCode = ConfigurationSettings.AppSettings["InfishopMerchantId"].ToString();
            lobjMerchantDetails.Password = ConfigurationSettings.AppSettings["InfishopMerchantPwd"].ToString();

            MerchantDetails lobjCurrentMerchantDetails = null;
            LoggingAdapter.WriteLog("MerchantDetails -" + lobjMerchantDetails.MerchantCode + " / " + lobjMerchantDetails.Password);
            lobjCurrentMerchantDetails = lobjModel.GetMerchantDetails(lobjMerchantDetails);
            if (lobjCurrentMerchantDetails == null)
            {
                Response.Redirect("InvalidCredential.aspx");
            }
           // string lstrSecurityKey = Convert.ToString(Guid.NewGuid());

            lstrAuthenticationToken = GenerateUniqueNumber.Get12DigitNumberDateTime();

            string lstrResponseDetails = string.Empty;
            Response lobjResponse = new Response();
            PGServiceHelper lobjPGServiceHelper = new PGServiceHelper();
            PGManagerClient lobjPGManagerClient = new PGManagerClient();
            string lstrCurrency = lobjModel.GetDefaultCurrency();

            string lstrRelationReference = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;

            LoggingAdapter.WriteLog("infishop Request For Member Id=" + lstrRelationReference);
            lobjResponse = lobjPGManagerClient.CheckPointsAvailability(lstrRelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency, lobjCurrentMerchantDetails.MerchantCode, lobjCurrentMerchantDetails.UserName, lobjCurrentMerchantDetails.Password);

            if (lobjResponse.IsSucessful)
            {
                lstrResponseDetails = "0" + "|" + lstrRelationReference + "|" + lobjMemberDetails.LastName + "|" + lobjMemberDetails.Email + "|" + lobjResponse.ReturnObject + "|" + lstrAuthenticationToken + "|" + redirectionURL; //"Default";
            }
            else
            {
                lstrResponseDetails = "1" + "|" + lobjResponse.ExceptionMessage;
            }
            LoggingAdapter.WriteLog("Member Id = " + lstrRelationReference + " | INfishop response =" + lstrResponseDetails);

            int Token = InsertAuthenticationToken(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, lstrAuthenticationToken);
            if (Token > 0)
            {
                string lstrEncryptedResponseDetails = RSAEncryptor.EncryptString(lstrResponseDetails, RSASecurityConstant.KeySize, RSASecurityConstant.RSAPublicKey);
                StringBuilder sb = new StringBuilder();
                sb.Append("<html>");
                sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
                sb.AppendFormat("<form name='form' action='{0}' method='post'>", lobjCurrentMerchantDetails.CallBackURL);
                sb.AppendFormat("<input type='hidden' runat='server' name='Response_Details' value='{0}'>", lstrEncryptedResponseDetails);
                sb.Append("</form>");
                sb.Append("</body>");
                sb.Append("</html>");
                Response.Write(sb.ToString());
                Response.End();
            }

        }
        else
        {
            string lstrInfishopURL = "";
            if (Request.QueryString["Url"] != null && Request.QueryString["Url"] != "")
            {
                lstrInfishopURL = Request.QueryString["Url"].ToString();
            }
            else
            {
                lstrInfishopURL = ConfigurationSettings.AppSettings["InfishopURL"].ToString();
            }
            Response.Redirect(lstrInfishopURL);
        }

    }

    public int InsertAuthenticationToken(string pstrMemberID, string pstrAuthenticationToken)
    {
        int lintStatus = 0;
        try
        {
            int lintMinutes = Convert.ToInt32(ConfigurationSettings.AppSettings["ExpiryMinutes"]);
           // InfiModel lobjModel = new InfiModel();
            AuthenticationDetails lobjAuthenticationDetails = new AuthenticationDetails();
            lobjAuthenticationDetails.MemberId = pstrMemberID;
            lobjAuthenticationDetails.AuthenticationToken = pstrAuthenticationToken;
            lobjAuthenticationDetails.AuthenticationType = AuthenticationType.PG;
            lobjAuthenticationDetails.CreationDate = DateTime.Now;
            lobjAuthenticationDetails.ExpiryDate = DateTime.Now.AddMinutes(lintMinutes);
            lobjAuthenticationDetails.IsActive = true;
            lobjAuthenticationDetails.CreatedBy = "System";
            lobjAuthenticationDetails.CreatedOn = DateTime.Now;
            lobjAuthenticationDetails.UpdatedOn = DateTime.Now;
            lintStatus = InsertAuthenticationDetails(lobjAuthenticationDetails);
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("InsertAuthenticationToken exception - " + ex.Message + Environment.NewLine + ex.StackTrace);
        }
        return lintStatus;
    }
 
    public int InsertAuthenticationDetails(AuthenticationDetails pobjAuthenticationDetails)
    {
        int lintStatus = 0;
        try
        {
            InfiModel lobjInfiModel = new InfiModel();
            //AuthenticationFacade lobjFacade = new AuthenticationFacade();
            //return lobjFacade.InsertAuthenticationDetails(pobjAuthenticationDetails);
            return lobjInfiModel.InsertAuthenticationDetails(pobjAuthenticationDetails);
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("InsertAuthenticationDetails exception - " + ex.Message + Environment.NewLine + ex.StackTrace);
            return lintStatus;
        }
    }
}