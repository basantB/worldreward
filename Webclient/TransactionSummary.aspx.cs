﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfiPlanetModel.Model;
using Core.Platform.Transactions.Entites;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Member.Entites;
using System.Data;
using Framework.EnterpriseLibrary.Adapters;

public partial class TransactionSummary : System.Web.UI.Page
{
    #region Private Properties
    private int CurrentPage
    {
        get
        {
            object objPage = ViewState["_CurrentPage"];
            int _CurrentPage = 0;
            if (objPage == null)
            {
                _CurrentPage = 0;
            }
            else
            {
                _CurrentPage = (int)objPage;
            }
            return _CurrentPage;
        }
        set { ViewState["_CurrentPage"] = value; }
    }
    private int fistIndex
    {
        get
        {

            int _FirstIndex = 0;
            if (ViewState["_FirstIndex"] == null)
            {
                _FirstIndex = 0;
            }
            else
            {
                _FirstIndex = Convert.ToInt32(ViewState["_FirstIndex"]);
            }
            return _FirstIndex;
        }
        set { ViewState["_FirstIndex"] = value; }
    }
    private int lastIndex
    {
        get
        {

            int _LastIndex = 0;
            if (ViewState["_LastIndex"] == null)
            {
                _LastIndex = 0;
            }
            else
            {
                _LastIndex = Convert.ToInt32(ViewState["_LastIndex"]);
            }
            return _LastIndex;
        }
        set { ViewState["_LastIndex"] = value; }
    }
    #endregion

    #region PagedDataSource
    PagedDataSource _PageDataSource = new PagedDataSource();
    #endregion
    int TotalPagecount = 5;
    string ddlvalue = string.Empty;
    List<TransactionDetails> LobjTransactionDetails = null;
    List<string> LobjTotalpage = new List<string>();
    static string lstrCurrencyName = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {

        // Stop Caching in IE
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);

        // Stop Caching in Firefox
        Response.Cache.SetNoStore();
        if (Session["MemberDetails"] == null)
        {
            Response.Redirect("Login.aspx");
        }
        if (!Page.IsPostBack)
        {
            try
            {
                LoggingAdapter.WriteLog("tranLog1");
                List<ProgramCurrencyDefinition> lobjlistProgramCurrencyDefinition = GetProgramCurrency();
                if (lobjlistProgramCurrencyDefinition != null)
                {
                    LoggingAdapter.WriteLog("tranLog2");
                    if (lobjlistProgramCurrencyDefinition.Count > 0)
                    {
                        LoggingAdapter.WriteLog("tranLog3");
                        lblNoResult.Text = string.Empty;
                        lstrCurrencyName = lobjlistProgramCurrencyDefinition[0].Currency;
                        BindMemberPoolingDetails();
                        BindDataRangeFilter(lobjlistProgramCurrencyDefinition[0].Currency);
                        BindTransactionSummaryInfo(lobjlistProgramCurrencyDefinition[0].Currency);
                    }
                    else
                    {
                        LoggingAdapter.WriteLog("tranLog4");
                        linkbuttonPrevious.Visible = false;
                        linkbuttonNext.Visible = false;
                        linkbuttonFirst.Visible = false;
                        linkbuttonLast.Visible = false;
                        DivPaging.Visible = false;
                        lblNoResult.Text = "Perhaps you don't have any Infi Planet Reward point ,Please Contact Call Center.";
                    }
                    if (lobjlistProgramCurrencyDefinition.Count > 1)
                    {
                        LoggingAdapter.WriteLog("tranLog5");
                        ddlProgramCurrency.DataSource = lobjlistProgramCurrencyDefinition;
                        ddlProgramCurrency.DataTextField = "Currency";
                        ddlProgramCurrency.DataValueField = "Id";
                        ddlProgramCurrency.DataBind();
                        ddlProgramCurrency.Visible = true;
                        divCurrency.Visible = true;
                    }
                    else
                    {
                        LoggingAdapter.WriteLog("tranLog6");
                        divCurrency.Visible = false;
                        ddlProgramCurrency.Visible = false;
                      
                    }
                }
                else
                {
                    LoggingAdapter.WriteLog("tranLog7");
                    InfiModel lobjModel = new InfiModel();
                    List<CustomerSegment> lobjCustomerSegmentList = new List<CustomerSegment>();
                    CustomerSegment lobjCustomerSegment = new CustomerSegment();
                    ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();
                    MemberDetails lobjMemberDetails = null;
                    lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
                    lobjCustomerSegmentList = lobjModel.GetAllCustomerSegmentlst(lobjProgramDefinition.ProgramId);
                    if (lobjCustomerSegmentList.Count > 0)
                    {
                        LoggingAdapter.WriteLog("tranLog8");
                        lobjCustomerSegment = lobjCustomerSegmentList.Find(lobj => lobj.SegmentCode.Equals(lobjMemberDetails.CustomerSegment));
                        lblNoResult.Text = string.Empty;
                        lstrCurrencyName = lobjCustomerSegment.Currency;
                        BindDataRangeFilter(lstrCurrencyName);
                        BindTransactionSummaryInfo(lstrCurrencyName);
                    }
                }
                ViewState["CurrentPage"] = 1;
                CurrentPage = Convert.ToInt32(ViewState["CurrentPage"]);

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("tranLog9");
                Framework.EnterpriseLibrary.Adapters.LoggingAdapter.WriteLog(ex.Message);
            }
        }
    }

    private List<ProgramCurrencyDefinition> GetProgramCurrency()
    {
        LoggingAdapter.WriteLog("tranLog10");
        InfiModel lobjModel = new InfiModel();
        ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();
        MemberDetails lobjMemberDetails = null;
        lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
        List<ProgramCurrencyDefinition> lobjlistProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
        lobjlistProgramCurrencyDefinition = lobjModel.GetProductProgramCurrencyDefinition(lobjMemberDetails);
        return lobjlistProgramCurrencyDefinition;
    }

    public List<string> BindDataRangeFilter(string pstrCurrencyName)
    {
        LoggingAdapter.WriteLog("tranLog11");
        int TotalRowCount = 0;
        InfiModel lobjModel = new InfiModel();
        ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();
        SearchTransactions objSearchTransactions = new SearchTransactions();
        objSearchTransactions.RelationType = Convert.ToInt32(RelationType.LBMS);
        objSearchTransactions.TransactionCurrency = pstrCurrencyName;
        objSearchTransactions.RelationReference = ((Session["MemberDetails"] as MemberDetails).MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
        objSearchTransactions.ProgramId = lobjProgramDefinition.ProgramId;
        LobjTransactionDetails = new List<TransactionDetails>();
        if (FromDate.Text == "" || Todate.Text == "")
        {
            LoggingAdapter.WriteLog("tranLog12");
            TotalRowCount = lobjModel.GetTotalMemberTransaction(objSearchTransactions);
            LoggingAdapter.WriteLog("tranLog12---"+TotalRowCount);
        }
        else
        {
            LoggingAdapter.WriteLog("tranLog13");
            objSearchTransactions.DateFrom = lobjModel.StringToDateTime(FromDate.Text);
            objSearchTransactions.DateTo = lobjModel.StringToDateTime(Todate.Text);
            TotalRowCount = lobjModel.GetTotalMemberTransactionByDate(objSearchTransactions);

        }
        if (TotalRowCount == 0)
        {
            LoggingAdapter.WriteLog("tranLog14");
            lblNoResult.Text = "No Transactions Found.";
        }
        else
        {
            LoggingAdapter.WriteLog("tranLog14A");
            lblNoResult.Text = string.Empty;
        }
        ViewState["TotalPages"] = TotalRowCount;
        List<string> lobjstringList = new List<string>();
        lobjstringList = lobjModel.PageDataRangeList(TotalRowCount, TotalPagecount);
        LobjTotalpage = lobjstringList;
        if (lobjstringList.Count > 0)
        {
            ddlvalue = lobjstringList[0];
        }
        return lobjstringList;
    }

    public void BindTransactionSummaryInfo(string pstrCurrencyName)
    {
        LoggingAdapter.WriteLog("tranLog15");
        InfiModel lobjModel = new InfiModel();
        ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramMaster();
        SearchTransactions objSearchTransactions = new SearchTransactions();
        objSearchTransactions.RelationReference = ((Session["MemberDetails"] as MemberDetails).MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
        objSearchTransactions.ProgramId = lobjProgramDefinition.ProgramId;
        objSearchTransactions.RelationType = Convert.ToInt32(RelationType.LBMS);
        objSearchTransactions.TransactionCurrency = pstrCurrencyName;
        objSearchTransactions.MinimumRange = Convert.ToInt32(ddlvalue.ToString().Split('-')[0]);
        objSearchTransactions.MaximumRange = Convert.ToInt32(ddlvalue.ToString().Split('-')[1]);
        objSearchTransactions.TransactionCurrency = pstrCurrencyName;

        LobjTransactionDetails = new List<TransactionDetails>();
        if (FromDate.Text == "" && Todate.Text == "")
        {
            LoggingAdapter.WriteLog("tranLog15A");
            LobjTransactionDetails = lobjModel.GetMemberTransactionSummary(objSearchTransactions);
        }
        else
        {
            objSearchTransactions.DateFrom = lobjModel.StringToDateTime(FromDate.Text);
            objSearchTransactions.DateTo = lobjModel.StringToDateTime(Todate.Text);
            LobjTransactionDetails = lobjModel.GetMemberTransactionSummaryByDate(objSearchTransactions);
        }
        if (LobjTransactionDetails != null && LobjTransactionDetails.Count > 0)
        {
            LoggingAdapter.WriteLog("tranLog15B");
            RepSummaryInfo.DataSource = LobjTransactionDetails;
        }
        else
        {
            LoggingAdapter.WriteLog("tranLog15C");
            RepSummaryInfo.DataSource = null;
        }
        RepSummaryInfo.DataBind();

        if (LobjTransactionDetails.Count > 0)
        {
            LoggingAdapter.WriteLog("tranLog15D");
            dlPaging.Visible = true;
            if (CurrentPage == 1)
            {
                LoggingAdapter.WriteLog("tranLog15D1");
                linkbuttonPrevious.Enabled = false;
                linkbuttonNext.Enabled = true;
                linkbuttonFirst.Enabled = false;
                linkbuttonLast.Enabled = true;
            }

            if (Convert.ToInt32(LobjTotalpage.Count) == 1)
            {
                LoggingAdapter.WriteLog("tranLog15D2");
                linkbuttonPrevious.Enabled = false;
                linkbuttonNext.Enabled = false;
                linkbuttonFirst.Enabled = false;
                linkbuttonLast.Enabled = false;
              
            }
            else if (CurrentPage > 1 && CurrentPage < Convert.ToInt32(LobjTotalpage.Count))
            {
                LoggingAdapter.WriteLog("tranLog15D3");
                linkbuttonPrevious.Enabled = true;
                linkbuttonNext.Enabled = true;
                linkbuttonFirst.Enabled = true;
                linkbuttonLast.Enabled = true;
            }
            else if (CurrentPage == Convert.ToInt32(LobjTotalpage.Count))
            {
                LoggingAdapter.WriteLog("tranLog15D5");
                linkbuttonPrevious.Enabled = true;
                linkbuttonNext.Enabled = false;
                linkbuttonFirst.Enabled = true;
                linkbuttonLast.Enabled = false;
            }
            doPaging();
        }
        else
        {
            LoggingAdapter.WriteLog("tranLog15D6");
            linkbuttonPrevious.Visible = false;
            linkbuttonNext.Visible = false;
            linkbuttonFirst.Visible = false;
            linkbuttonLast.Visible = false;
            dlPaging.Visible = false;
            DivPaging.Visible = false;
        }

    }
    public void doPaging()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ID");
        dt.Columns.Add("PageIndex");

        fistIndex = CurrentPage - 5;
        if (CurrentPage > 5)
        {
            lastIndex = CurrentPage + 5;
        }
        else
        {
            lastIndex = 10;
        }
        if (lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
        {
            lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
            fistIndex = lastIndex - 10;
        }

        if (fistIndex < 0)
        {
            fistIndex = 0;
        }
        for (int i = fistIndex; i < lastIndex; i++)
        {
            if (LobjTotalpage.Count == i)
            {
                break;
            }
            else
            {
                dt.Rows.Add((i + 1).ToString(), LobjTotalpage[i]);
            }
        }
        this.dlPaging.DataSource = dt;
        this.dlPaging.DataBind();
    }

    protected void lbtnNext_Click(object sender, EventArgs e)
    {
        LobjTotalpage = BindDataRangeFilter(lstrCurrencyName);
        CurrentPage += 1;
        if (LobjTotalpage.Count > 0)
        {
            ddlvalue = LobjTotalpage[CurrentPage - 1];
        }
        else
        {
            ddlvalue = "0-0";
        }
        this.BindTransactionSummaryInfo(lstrCurrencyName);
    }
    protected void lbtnPrevious_Click(object sender, EventArgs e)
    {
        LobjTotalpage = BindDataRangeFilter(lstrCurrencyName);
        CurrentPage -= 1;
        if (LobjTotalpage.Count > 0)
        {
            ddlvalue = LobjTotalpage[CurrentPage - 1];
        }
        else
        {
            ddlvalue = "0-0";
        }
        this.BindTransactionSummaryInfo(lstrCurrencyName);
    }
    protected void dlPaging_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName.Equals("Paging"))
        {
            LobjTotalpage = BindDataRangeFilter(lstrCurrencyName);
            LinkButton objlnkbtnPaging = (LinkButton)e.Item.FindControl("lnkbtnPaging");
            ddlvalue = e.CommandArgument.ToString();
            CurrentPage = Convert.ToInt16(objlnkbtnPaging.Text);
            this.BindTransactionSummaryInfo(lstrCurrencyName);
        }
    }
    protected void dlPaging_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        LinkButton lnkbtnPage = (LinkButton)e.Item.FindControl("lnkbtnPaging");
        if (lnkbtnPage.Text.ToString() == CurrentPage.ToString())
        {
            lnkbtnPage.Enabled = false;
            lnkbtnPage.Style.Add("fone-size", "14px");
            lnkbtnPage.Font.Bold = true;

        }
    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (Convert.ToInt32(ViewState["TotalPages"]));
        LobjTotalpage = BindDataRangeFilter(lstrCurrencyName);
        if (LobjTotalpage.Count > 0)
        {
            ddlvalue = LobjTotalpage[LobjTotalpage.Count - 1];
            CurrentPage = LobjTotalpage.Count;
        }
        else
        {
            ddlvalue = "0-0";
        }
        this.BindTransactionSummaryInfo(lstrCurrencyName);

    }
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {
        LobjTotalpage = BindDataRangeFilter(lstrCurrencyName);
        CurrentPage = 1;
        if (LobjTotalpage.Count > 0)
        {
            ddlvalue = LobjTotalpage[CurrentPage - 1];
        }
        else
        {
            ddlvalue = "0-0";
        }
        this.BindTransactionSummaryInfo(lstrCurrencyName);
    }

    protected void RdlSearchtypeCust_CheckedChanged(object sender, EventArgs e)
    {
        RdlSearchtypeCust.Checked = true;
        RdlSearchtypeAll.Checked = false;
        tblsearchinfo.Attributes.Add("Style", "display:block");
    }
    protected void RdlSearchtypeAll_CheckedChanged(object sender, EventArgs e)
    {
        RdlSearchtypeAll.Checked = true;
        RdlSearchtypeCust.Checked = false;
        tblsearchinfo.Attributes.Add("Style", "display:none");
        FromDate.Text = "";
        Todate.Text = "";
        BindDataRangeFilter(lstrCurrencyName);
        BindTransactionSummaryInfo(lstrCurrencyName);
    }
    protected void ddlCurrencyName_SelectedIndexChanged(object sender, EventArgs e)
    {
        lstrCurrencyName = ddlProgramCurrency.SelectedItem.Text;
        if (RdlSearchtypeAll.Checked)
        {
            BindDataRangeFilter(ddlProgramCurrency.SelectedItem.Text);
            BindTransactionSummaryInfo(ddlProgramCurrency.SelectedItem.Text);
        }
        else
        {
            if (FromDate.Text != string.Empty && Todate.Text != string.Empty)
            {
                BindDataRangeFilter(ddlProgramCurrency.SelectedItem.Text);
                BindTransactionSummaryInfo(ddlProgramCurrency.SelectedItem.Text);
            }
            else
            {
                lblNoResult.Text = "No record found.";
                RepSummaryInfo.DataSource = null;
                RepSummaryInfo.DataBind();
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        // visible_table_status();
        ViewState["CurrentPage"] = 1;
        CurrentPage = Convert.ToInt32(ViewState["CurrentPage"]);
        BindDataRangeFilter(lstrCurrencyName);
        BindTransactionSummaryInfo(lstrCurrencyName);
    }

    protected void ddlPoolingDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        string pstrMemberId = ddlPoolingDetails.SelectedValue.ToString();
        List<MemberDetails> lstlobjMemberDetails = Session["MemberDetailsList"] as List<MemberDetails>;
        MemberDetails lobjNewMemberDetails = new MemberDetails();
        lobjNewMemberDetails = lstlobjMemberDetails.Find(obj => obj.Id.Equals(Convert.ToInt32(pstrMemberId)));
        string pstrRelationReference = lobjNewMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).RelationReference;

        lblNoResult.Text = string.Empty;

        InfiModel lobjInfiModel = new InfiModel();
        List<ProgramCurrencyDefinition> lobjlistProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
        lobjlistProgramCurrencyDefinition = lobjInfiModel.GetProductProgramCurrencyDefinition(lobjNewMemberDetails);
        //GetStatementSummary(lobjlistProgramCurrencyDefinition[0].Currency, pstrRelationReference);
        BindDataRangeFilterForFamilyMember(lobjlistProgramCurrencyDefinition[0].Currency, pstrRelationReference);
        BindTransactionSummaryInfoForFamilyMember(lobjlistProgramCurrencyDefinition[0].Currency, pstrRelationReference);
    }

    public List<string> BindDataRangeFilterForFamilyMember(string pstrCurrencyName, string pstrRelationReference)
    {
        int TotalRowCount = 0;
        InfiModel lobjInfiModel = new InfiModel();
        ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramMaster();
        SearchTransactions objSearchTransactions = new SearchTransactions();
        objSearchTransactions.RelationType = Convert.ToInt32(RelationType.LBMS);
        objSearchTransactions.TransactionCurrency = pstrCurrencyName;
        objSearchTransactions.RelationReference = pstrRelationReference;
        objSearchTransactions.ProgramId = lobjProgramDefinition.ProgramId;
        LobjTransactionDetails = new List<TransactionDetails>();
        if (FromDate.Text == "" || Todate.Text == "")
        {
            TotalRowCount = lobjInfiModel.GetTotalMemberTransaction(objSearchTransactions);

        }
        else
        {
            objSearchTransactions.DateFrom = lobjInfiModel.StringToDateTime(FromDate.Text);
            objSearchTransactions.DateTo = lobjInfiModel.StringToDateTime(Todate.Text);
            TotalRowCount = lobjInfiModel.GetTotalMemberTransactionByDate(objSearchTransactions);

        }
        if (TotalRowCount == 0)
            lblNoResult.Text = "No Transactions Found";
        else
            lblNoResult.Text = string.Empty;
        ViewState["TotalPages"] = TotalRowCount;
        List<string> lobjstringList = new List<string>();
        lobjstringList = lobjInfiModel.PageDataRangeList(TotalRowCount, TotalPagecount);
        LobjTotalpage = lobjstringList;
        if (lobjstringList.Count > 0)
        {
            ddlvalue = lobjstringList[0];
        }
        return lobjstringList;
    }
    public void BindTransactionSummaryInfoForFamilyMember(string pstrCurrencyName, string pstrRelationReference)
    {
        InfiModel lobjInfiModel = new InfiModel();
        ProgramDefinition lobjProgramDefinition = lobjInfiModel.GetProgramMaster();
        SearchTransactions objSearchTransactions = new SearchTransactions();
        objSearchTransactions.RelationReference = pstrRelationReference;
        objSearchTransactions.ProgramId = lobjProgramDefinition.ProgramId;
        objSearchTransactions.RelationType = Convert.ToInt32(RelationType.LBMS);
        objSearchTransactions.TransactionCurrency = pstrCurrencyName;
        objSearchTransactions.MinimumRange = Convert.ToInt32(ddlvalue.ToString().Split('-')[0]);
        objSearchTransactions.MaximumRange = Convert.ToInt32(ddlvalue.ToString().Split('-')[1]);
        objSearchTransactions.TransactionCurrency = pstrCurrencyName;

        LobjTransactionDetails = new List<TransactionDetails>();
        if (FromDate.Text == "" || Todate.Text == "")
        {
            LobjTransactionDetails = lobjInfiModel.GetMemberTransactionSummary(objSearchTransactions);
        }
        else
        {
            objSearchTransactions.DateFrom = lobjInfiModel.StringToDateTime(FromDate.Text);
            objSearchTransactions.DateTo = lobjInfiModel.StringToDateTime(Todate.Text);
            LobjTransactionDetails = lobjInfiModel.GetMemberTransactionSummaryByDate(objSearchTransactions);
        }
        if (LobjTransactionDetails != null && LobjTransactionDetails.Count > 0)
        {
            RepSummaryInfo.DataSource = LobjTransactionDetails;
        }
        else
        {
            RepSummaryInfo.DataSource = null;
        }
        RepSummaryInfo.DataBind();

        if (LobjTransactionDetails.Count > 0)
        {
            dlPaging.Visible = true;
            if (CurrentPage == 1)
            {
                linkbuttonPrevious.Enabled = false;
                linkbuttonNext.Enabled = true;
                linkbuttonFirst.Enabled = false;
                linkbuttonLast.Enabled = true;
            }

            if (Convert.ToInt32(LobjTotalpage.Count) == 1)
            {
                linkbuttonPrevious.Enabled = false;
                linkbuttonNext.Enabled = false;
                linkbuttonFirst.Enabled = false;
                linkbuttonLast.Enabled = false;
            }
            else if (CurrentPage > 1 && CurrentPage < Convert.ToInt32(LobjTotalpage.Count))
            {
                linkbuttonPrevious.Enabled = true;
                linkbuttonNext.Enabled = true;
                linkbuttonFirst.Enabled = true;
                linkbuttonLast.Enabled = true;
            }
            else if (CurrentPage == Convert.ToInt32(LobjTotalpage.Count))
            {
                linkbuttonPrevious.Enabled = true;
                linkbuttonNext.Enabled = false;
                linkbuttonFirst.Enabled = true;
                linkbuttonLast.Enabled = false;
            }
            doPaging();
        }
        else
        {
            linkbuttonPrevious.Visible = false;
            linkbuttonNext.Visible = false;
            linkbuttonFirst.Visible = false;
            linkbuttonLast.Visible = false;
            dlPaging.Visible = false;
        }

    }

    public void BindMemberPoolingDetails()
    {
        try
        {
            LoggingAdapter.WriteLog("BindMemberPoolingDetails");
            InfiModel lobjModel = new InfiModel();

            List<MemberDetails> lobjListMemberDetails = new List<MemberDetails>();
            MemberDetails lobjMemberDetails = null;
            lobjMemberDetails = Session["MemberDetails"] as MemberDetails;


            SearchMember lobjSearchMember = new SearchMember();

            lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
            lobjSearchMember.ProgramId = lobjMemberDetails.ProgramId;


            string pstrPrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).PrimaryReference;

            if (pstrPrimaryReference == null || pstrPrimaryReference.Equals(string.Empty))
            {
                LoggingAdapter.WriteLog("BindMemberPoolingDetails1");
                lobjSearchMember.PrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).RelationReference;
            }
            else
            {
                LoggingAdapter.WriteLog("BindMemberPoolingDetails2");
                lobjSearchMember.PrimaryReference = lobjMemberDetails.MemberRelationsList.Find(x => x.RelationType.Equals(RelationType.LBMS)).PrimaryReference;
            }

            lobjListMemberDetails = lobjModel.GetMemberDetailByPrimaryReference(lobjSearchMember);
            lobjListMemberDetails = lobjListMemberDetails.GroupBy(c => c.Id, (key, c) => c.FirstOrDefault()).ToList();
            var index = lobjListMemberDetails.FindIndex(x => x.LastName.Equals(lobjMemberDetails.LastName));
            var topItem = lobjListMemberDetails[index];
            lobjListMemberDetails.RemoveAt(index);
            lobjListMemberDetails.Insert(0, topItem);
            Session["MemberDetailsList"] = lobjListMemberDetails;

            ddlPoolingDetails.DataSource = lobjListMemberDetails;
            ddlPoolingDetails.DataTextField = "LastName";
            ddlPoolingDetails.DataValueField = "Id";
            ddlPoolingDetails.DataBind();
            ddlPoolingDetails.Visible = true;
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("BindMemberPoolingDetails Ex-" + ex.Message + ex.StackTrace + ex.InnerException);
        }
    }
}