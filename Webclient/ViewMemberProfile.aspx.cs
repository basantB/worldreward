﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Core.Platform.Member.Entites;
using InfiPlanetModel.Model;
using Core.Platform.ProgramMaster.Entities;
using System.Text.RegularExpressions;

public partial class ViewMemberProfile : System.Web.UI.Page
{
    public static string lstrProgramName = ConfigurationSettings.AppSettings["ProgramName"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
            if (lobjMemberDetails == null)
            {
                Response.Redirect("Login.aspx");
            }
            PopulateProfile();
        }
    }

    private void PopulateProfile()
    {
        MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
        labelAddressValue.Text = lobjMemberDetails.Address1;
        labelMobileNo.Text = lobjMemberDetails.MobileNumber;
        labelEmailValue.Text = lobjMemberDetails.Email;
        labelMemberNameValue.Text = lobjMemberDetails.LastName;
    }


    public void ClearTextBoxes(TextBox[] pobjTextBoxArray)
    {
        foreach (TextBox t in pobjTextBoxArray)
        {
            t.Text = string.Empty;
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string ChangePassword(string pstrOldPassword, string pstrNewPassword)
    {
        string mstrRedirectEmptyURL = string.Empty;
        string lstrEncryptPassword = string.Empty;
        bool lobjchangepwd = false;
        MemberDetails lobjMemberDetails = HttpContext.Current.Session["MemberDetails"] as MemberDetails;
        InfiModel lobjModel = new InfiModel();
        lstrEncryptPassword = lobjModel.EncryptPassword(pstrOldPassword);
        //if (lobjModel.CheckOldPassword(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.Login)).RelationReference, lstrEncryptPassword, lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.Login)).WebPassword))
        if (lobjModel.CheckOldPassword(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, lstrEncryptPassword, lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword))
        {
            ProgramDefinition lobjProgramDefinition = lobjModel.GetProgramDetailsByID(lobjMemberDetails.ProgramId);
            if (lobjProgramDefinition != null)
            {
                SystemParameter lobjPasswordPolicy = lobjModel.GetSystemParametres(lobjProgramDefinition.ProgramId);
                Regex regex = new Regex((lobjPasswordPolicy.PasswordPolicy));
                if (regex.Match(pstrNewPassword).Success)
                {
                    lstrEncryptPassword = lobjModel.EncryptPassword(pstrNewPassword);
                    MemberRelation lobjMemberRelation = new MemberRelation();
                    //lobjMemberRelation.RelationReference = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.Login)).RelationReference;
                    lobjMemberRelation.RelationReference = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    lobjMemberRelation.WebPassword = lstrEncryptPassword;
                    //lobjMemberRelation.RelationType = RelationType.Login;
                    lobjMemberRelation.RelationType = RelationType.LBMS;

                    lobjchangepwd = lobjModel.ChangePasswordForMember(lobjMemberRelation);
                    if (lobjchangepwd)
                    {
                        //lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.Login)).WebPassword = lstrEncryptPassword;
                        lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword = lstrEncryptPassword;
                        HttpContext.Current.Session["MemberDetails"] = lobjMemberDetails;
                        mstrRedirectEmptyURL = "Success";
                    }
                    else
                    {
                        mstrRedirectEmptyURL = "Failure.";
                    }
                }
                else
                {
                    mstrRedirectEmptyURL = "IncorrectFormat";
                }
            }
            else
            {
                mstrRedirectEmptyURL = "Failure.";
            }
        }

        return mstrRedirectEmptyURL;
    }
}