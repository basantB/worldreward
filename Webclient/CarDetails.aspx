﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true"
    CodeFile="CarDetails.aspx.cs" Inherits="CarDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="Css/Car.css" type="text/css" rel="stylesheet" />
    <!--Page Content Starts-->
    <div class="inrpg">
        <div class="wrap">
            <div class="checkout">
                <!--CheckOut Page Starts-->
                <div class="pgcol1">
                    <div>
                        <asp:HiddenField Value="" ID="hdnPaymentType" runat="server"></asp:HiddenField>
                        <div class="  ">
                            <h1>Excess Protection</h1>
                            <hr />
                            <div class="">
                                <h3>
                                    <asp:Label ID="Label1" runat="server" Text="Damage Excess refund "></asp:Label>
                                    <asp:Label ID="lblHDDERAmount" Visible="false" runat="server" Text="Amount :"></asp:Label>
                                    <asp:Label ID="lblDERAmount" Visible="false" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblHDDERPoint" runat="server" Text="Points :"></asp:Label>
                                    <asp:Label ID="lblDERPoint" runat="server" Text=""></asp:Label>
                                </h3>
                                <p>
                                    Excess Protection - Excellent Value, Best Cover, Peace of Mind
                                <p>
                                    When you pick up your car a deposit is usually held on your card for your excess
                                    which may be charged if the vehicle is damaged. Take our Damage Excess Refund and
                                    if you have any bumps or scrapes we'll refund any deductions from your excess.
                                    </p>
                                    <div>
                                        <ul class="carlis" >
                                            <li>You're protected - Damage Excess Refund protects you when things go wrong and you
                                            are required to pay out from your excess</li>
                                            <li>Accidents covered - Your excess will be protected if your vehicle is damaged in
                                            an accident</li>
                                            <li>Damage cover - Damage to bodywork, vehicle parts and accessories incurred during
                                            most driving incidents is covered.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <button class="button" onclick="var retValue=CarDetailsBookNow(false); event.returnValue = retValue;event.preventDefault();return retValue;">
                                    No, Continue</button>
                                <button class="button" onclick="var retValue=CarDetailsBookNow(true); event.returnValue = retValue;event.preventDefault();return retValue;">
                                    Yes i'll take it</button>
                                <div class="clr">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--StepsPage-->
                
                <div class="pgcol2">
                    <div class="widget-prfl" id="MasterUser">
                        <div class="widget-user">
                            <ul id="UserDetails">
                            </ul>
                        </div>
                        <div class="widget-srhpnl">
                            <h3>
                                <div>
                                    Itinerary <span class="edit"><a href="CarList.aspx">edit</a> </span>
                                </div>
                            </h3>
                            <div class="carchkout">
                                <ul>
                                    <li class="brdbottom">
                                        <div class="amt">
                                            <asp:Label runat="server" ID="lblCarName"></asp:Label>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="txtC">
                                            <asp:Image runat="server" ID="imgCar" />
                                        </div>
                                    </li>
                                    <li class="brdbottom">
                                        <div class="itenaryEmptyDiv">
                                            &nbsp;
                                        </div>
                                        <div class="itenaryContent">
                                            <div class="item-Name">
                                                <asp:Label runat="server" ID="lblPickLocation"></asp:Label>
                                                <asp:Label runat="server" ID="lblPickCountry"></asp:Label>
                                            </div>
                                            <p>
                                                <asp:Label runat="server" ID="lblPickDateTime"></asp:Label>
                                            </p>
                                            <p>
                                                 <img src="images/time.png" /> <asp:Label runat="server" ID="lblPickTime"></asp:Label>
                                            </p>
                                        </div>
                                        <div class="itenaryEmptyDiv">
                                         &nbsp;
                                            &nbsp;
                                        </div>
                                        <div class="itenaryContent">
                                            <div class="item-Name">
                                                <asp:Label runat="server" ID="lblDropLocation"></asp:Label>
                                                <asp:Label runat="server" ID="lblDropCountry"></asp:Label>
                                            </div>
                                            <p>
                                                <asp:Label runat="server" ID="lblDropDateTime"></asp:Label>
                                            </p>
                                            <p>
                                                <img src="images/time.png" />  <asp:Label runat="server" ID="lblDropTime"></asp:Label>
                                            </p>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div></div>
            </div>
            <div class="clr">
            </div>
            <!--CheckOut Page Ends-->
            <div class="clr">
            </div>
        </div>
    </div>
    <!--Page Content Starts-->
    <script type="text/javascript">
        function CarDetailsBookNow(isReturn) {
            $.ajax({
                url: 'CarDetails.aspx/GetIsDERAdded',
                type: 'POST',  // or get
                contentType: 'application/json; charset =utf-8',
                data: "{'IsDERAdded':'" + isReturn + "'}",
                dataType: 'json',
                success: function (data) {
                    if (data.d == true) {
                        window.location.href = "DriverDetails.aspx";
                    }
                    else {
                        return false;
                    }
                }
            });
        }
    </script>
</asp:Content>
