﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="ProductList.aspx.cs" Inherits="ProductList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <link href="CSS/jquery.ui.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="js/jquery.mCustomScrollbar.css" type="text/css" />
    <script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="js/jquery.ui.autocomplete.js"></script>
    <script src="js/JTemplateCreator.js" type="text/javascript"></script>
    <script src="js/TourListScript.js"></script>
    <script src="js/TourSearchScript.js"></script>
    <link href="css/tour.css" rel="stylesheet" type="text/css" />
    <style>
        .HotelMoveLeft {
            background: #fff none repeat scroll 0 0;
            display: block;
            left: 1px;
            margin-left: -768px;
            padding: 2%;
            position: absolute;
            top: 0px;
            width: 73%;
            z-index: 2147483647;
        }

        #divBlockAll {
            z-index: 22000;
            float: left;
            height: 900px;
            width: 120%;
            background: black;
            opacity: .5;
            margin-left: -15px;
            filter: alpha(opacity=40);
            position: fixed;
            display: none;
            top: -130px;
        }
    </style>
    <div class="srchcircle">
        <div class="scrhcnt">
            
            <h1>Tour Search</h1>
            <p>
                <span id="CP_LabelYourSearchDetails"></span>
            </p>
            <div id="cssload-wrapper">
                <div id="cssload-border">
                    <div id="cssload-whitespace">
                        <div id="cssload-line">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="inrpg">
      <div class="pgcontent flight">
      <div class="tour_list_a">
       <div id="" class="pgFltcol2">
       <div id="UserInfo" class="widget-prfl">
        <div id="Content_Wrapper"> 
        <div class="widget-srhpnl">
            <div class="urscrh" style="display:none;">
                <label class="searchdetail" id="lblSearchSummary"></label>
            </div>
            <h3 class="modify" onclick="return false;">
                Modify Search
            </h3>
            <%--<img src="images/ico-icon.png" class="modify" onclick="return false;" />--%>
            <div class="clr"></div>
        </div>
           </div>
           </div>
        <!-- Start Search -->
        <div class="inside-banner banner-hotel modify-box" style="display:none;">
            <div class="contain">

                <asp:HiddenField runat="server" ID="hdnDestinationid" />
                <div class="containHead">
                    Enter Your Favourite Destination
                </div>
                <div class="txtDest">
                    <asp:TextBox ID="txtDestination" runat="server" Text="Enter Destination" onfocus="placeholderOnFocus(this,'Enter Destination');"
                        CssClass="inputtext" onblur="placeholderOnFocus(this,'Enter Destination');"></asp:TextBox>
                    <input type="button" class="btnSearching" value="LET'S GO" onclick="goTOProducts();" />
                    <div id="auto_seacrht"></div>
                    <div class="clr">
                    </div>
                    <div id="requiredValidation" style="color: red; text-align: left"></div>
                </div>
            </div>
        <div class="clr"></div>
        </div>
       <!---End of search --->
    

    <div class="brd-crms" style="display:none;">
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" Visible="true" CssClass="" PathSeparator="&gt;">
            <CurrentNodeStyle CssClass="select" />
            <RootNodeStyle CssClass="" />
        </asp:SiteMapPath>
        <div class="clr">
        </div>
    </div>
    <div id="divBlockAll">
    </div>
   <div id="fliterDetails" class="rfnsrch">
    <div class="rfnhdr">
        <h3>Refine your Search</h3>
    </div>
    <div class="inr-wrap">
        <div class="btn-filter" id="btnfliter">
            Filters
        </div>
        <div class="stops" id="divSearch">
            <h3>Categories</h3>
            <ul id="DivCategoryList" class="tourCategory">
            </ul>
        </div>
    </div> 
         </div>   
        <!--end of categori-->
    </div>
          <div class="pgFltcol1">
            <div id="wr" class="list">
            <div class="srh-listing hotels">
                <div id="result1OneWay" class="clr">
                </div>
                <div class="clear">
                </div>
                <div style="float: left; width: 100%; padding-bottom: 40px; display: block">
                <div class="bgHeader" id="LoadNext" style="text-align: center; cursor: pointer; margin-right: 2.1%;
                    padding: 1% 0;" onclick="return loadNextRecord();">
                    <asp:Label ID="Label2" runat="server" Text="View More">
                    </asp:Label>
                </div>
            </div>
            </div>
        </div>
        <div class="clear">
        </div>
          </div>
        <asp:HiddenField ID="hfPageCount" runat="server" />        
        <asp:HiddenField ID="hfRecordCount" runat="server" />    
        <asp:HiddenField ID="hfCategoryId" runat="server" />    
    <div id="LoadTemplate" runat="server" style="display:none;">        
    </div>
    </div>
    </div>
   </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#btnfliter').click(function () {
                $('#divSearch').addClass('HotelMoveLeft');
                $('#divSearch').show();
                $('#divBlockAll').css('display', 'block');
                $('#divBlockAll').show();
                $('#divSearch').stop().animate({ 'marginLeft': '0px' }, 1000);
                return false;
            });
            $('#divBlockAll').click(function () {
                $('#divBlockAll').hide();
                $('#divSearch').stop().animate({ 'marginLeft': '-768px' }, 200);

            });
        });
    </script>
</asp:Content>