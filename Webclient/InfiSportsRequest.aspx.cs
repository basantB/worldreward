﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Platform.Member.Entites;
using Core.Platform.Merchant.Entities;
using InfiPlanetModel.Model;
using System.Configuration;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.PointGateway.Service.Helper;
using Core.Platform.PointGateway.Service.Helper.PGService;
using System.Text;
using Framework.EnterpriseLibrary.Security;
using Framework.EnterpriseLibrary.Security.Constants;
using Framework.EnterpriseLibrary.UniqueNumberGenerator;
using Core.Platform.Authentication.Entities;

public partial class InfiSportsRequest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["MemberDetails"] != null)
            {

                MemberDetails lobjMemberDetails = Session["MemberDetails"] as MemberDetails;
                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                InfiModel lobjModel = new InfiModel();
                lobjMerchantDetails.MerchantCode = ConfigurationSettings.AppSettings["InfisportsMerchantId"].ToString();
                lobjMerchantDetails.Password = ConfigurationSettings.AppSettings["InfisportsMerchantPwd"].ToString();

                MerchantDetails lobjCurrentMerchantDetails = null;
                LoggingAdapter.WriteLog("MerchantDetails -" + lobjMerchantDetails.MerchantCode + " / " + lobjMerchantDetails.Password);
                lobjCurrentMerchantDetails = lobjModel.GetMerchantDetails(lobjMerchantDetails);
                if (lobjCurrentMerchantDetails == null)
                {
                    Response.Redirect("InvalidCredential.aspx");
                }
                string lstrSecurityKey = Convert.ToString(Guid.NewGuid());
                string lstrResponseDetails = string.Empty;
                Response lobjResponse = new Response();
                PGServiceHelper lobjPGServiceHelper = new PGServiceHelper();
                PGManagerClient lobjPGManagerClient = new PGManagerClient();
                string lstrCurrency = lobjModel.GetDefaultCurrency();

                bool lblnUpdateStatus = false;
                string lstrAuthenticationToken = string.Empty;
                int lintResponse = 0;
                string lstrRelationReference = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                LoggingAdapter.WriteLog("infisports Request For Member Id=" + lstrRelationReference);

                lblnUpdateStatus = lobjModel.UpdateAuthenticationTokenStatus(lstrRelationReference);
                LoggingAdapter.WriteLog("infisports language en Request Authentication Token Updated");
                lstrAuthenticationToken = GenerateUniqueNumber.Get12DigitNumberDateTime();
                LoggingAdapter.WriteLog("infisports Authentication Token :" + lstrAuthenticationToken);
                lintResponse = InsertAuthenticationToken(lstrRelationReference, lstrAuthenticationToken);
                if (lintResponse > 0)
                {
                    LoggingAdapter.WriteLog("infisports Authentication en Token inserted.");
                    lobjResponse = lobjPGManagerClient.CheckPointsAvailability(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), lstrCurrency, lobjCurrentMerchantDetails.MerchantCode, lobjCurrentMerchantDetails.UserName, lobjCurrentMerchantDetails.Password);

                    if (lobjResponse.IsSucessful)
                    {
                        lstrResponseDetails = "0" + "|" + lobjMemberDetails.MemberRelationsList[0].RelationReference + "|" + lobjMemberDetails.LastName + "|" + lobjMemberDetails.Email + "|" + lobjResponse.ReturnObject + "|" + lstrSecurityKey + "|" + lstrAuthenticationToken + "|"  + "Default";
                    }
                    else
                    {
                        lstrResponseDetails = "1" + "|" + lobjResponse.ExceptionMessage;
                    }

                    string lstrEncryptedResponseDetails = RSAEncryptor.EncryptString(lstrResponseDetails, RSASecurityConstant.KeySize, RSASecurityConstant.RSAPublicKey);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<html>");
                    sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
                    sb.AppendFormat("<form name='form' action='{0}' method='post'>", lobjCurrentMerchantDetails.CallBackURL);
                    sb.AppendFormat("<input type='hidden' runat='server' name='Response_Details' value='{0}'>", lstrEncryptedResponseDetails);
                    sb.Append("</form>");
                    sb.Append("</body>");
                    sb.Append("</html>");
                    Response.Write(sb.ToString());
                    Response.End();
                }
                else
                {
                    LoggingAdapter.WriteLog("AuthenticationToken insert Failed");
                    Response.Redirect("index.aspx");
                }
            }
            else
            {
                string lstrInfishopURL = ConfigurationSettings.AppSettings["InfisportsURL"].ToString();
                Response.Redirect(lstrInfishopURL);
                //Response.Redirect("http://103.13.98.7/InfiPlanetsport/");
            }
        }
    }

    public int InsertAuthenticationToken(string pstrMemberID, string pstrAuthenticationToken)
    {
        int lintStatus = 0;
        try
        {
            int lintMinutes = Convert.ToInt32(ConfigurationSettings.AppSettings["ExpiryMinutes"]);
            InfiModel lobjInfiModel = new InfiModel();
            AuthenticationDetails lobjAuthenticationDetails = new AuthenticationDetails();
            lobjAuthenticationDetails.MemberId = pstrMemberID;
            lobjAuthenticationDetails.AuthenticationToken = pstrAuthenticationToken;
            lobjAuthenticationDetails.AuthenticationType = AuthenticationType.PG;
            lobjAuthenticationDetails.CreationDate = DateTime.Now;
            lobjAuthenticationDetails.ExpiryDate = DateTime.Now.AddMinutes(lintMinutes);
            lobjAuthenticationDetails.IsActive = true;
            lobjAuthenticationDetails.CreatedBy = "System";
            lintStatus = lobjInfiModel.InsertAuthenticationDetails(lobjAuthenticationDetails);
        }
        catch (Exception ex)
        {
            LoggingAdapter.WriteLog("InsertAuthenticationToken exception - " + ex.Message + Environment.NewLine + ex.StackTrace);
        }
        return lintStatus;
    }
}