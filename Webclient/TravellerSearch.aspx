﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InfiPlanetMaster.master" AutoEventWireup="true" CodeFile="TravellerSearch.aspx.cs" Inherits="TravellerSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CP" runat="Server">
    <%--<script src="js/angular.js" type="text/javascript"></script>
    <script src="js/angular-animate.js" type="text/javascript"></script>--%>
    <script src="js/ui-bootstrap-tpls-1.3.3.js" type="text/javascript"></script>
    <%--<script src="js/bootstrap.js" type="text/javascript"></script>--%>
    <script src="js/FlightSearchScript.js" type="text/javascript"></script>
    <script src="js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            var name = $("#CP_travellertabid").val();
            if (name == 1) {
                $("#aflt").click();
                
            }
            else if(name==2)
            {
                $("#ahtl").click();
            }
            else if (name == 3) {
                $("#acar").click();
            }
            else if (name == 4) {
                $("#alou").click();
            }
            
            else if (name == 6) {
                $("#atou").click();
            }
           
                    })
       
    </script>
     <%--<script src="js/TourScript.js"></script>--%>
    <link href="css/datepicker.css" rel="stylesheet" type="text/css" />
    <link href="css/flight.css" rel="stylesheet" type="text/css" />
    <div class="banner">
        <ul class="bxslider">
            <li id="liflt">
                <img src="images/bnrflight.jpg" /></li>
            <li id="lihtl" style="display: none">
                <img src="images/bnrhotel.jpg" /></li>
            <li id="licar" style="display: none">
                <img src="images/bnrcar.jpg" /></li>
            <li id="lilou" style="display: none">
                <img src="images/bnrcar.jpg" /></li>
            <li id="litou" style="display: none">
                <img src="images/bnrcar.jpg" /></li>
        </ul>
        <div class="scrhcont">
            <div class="sertabs">
                <div class="wrap scrollmenu">
                    <a id="aflt" class="select" onclick="TabSlider('flt','htl','car','lou','tou');">
                        <img src="images/ico-flts.png" />Flight</a>
                    <a id="ahtl" onclick="TabSlider('htl','flt','car','lou','tou');">
                        <img src="images/ico-hotel.png" />Hotel</a>
                    <a id="acar" onclick="TabSlider('car','htl','flt','lou','tou');">
                        <img src="images/icocar.png" />Car</a>
                    <a id="alou" onclick="TabSlider('lou','htl','car','flt','tou');">
                        <img src="images/icolounges.png" />Lounge</a>
                    <a href="#">
                        <img src="images/icomeetg.png" />Meet & Greet</a>
                    <a id="atou" onclick="TabSlider('tou','htl','car','lou','flt');">
                        <img src="images/icotour.png" />Tour</a>
                </div>
            </div>
            <asp:TextBox ID="travellertabid" runat="server" Visible="false"></asp:TextBox>
          
            <div ng-app="WDRewards">
                <div ng-controller="MainCtrl">
                    <div class="srchpanel flight" style="display: block;" id="divSflt">
                        <div class="wrap">
                            <div id="requiredValidation" class="ValidationDiv" style="display:none;">
                            </div>
                            <div class="radios clearfix">
                                <div>
                                    <%--<input id="rdbOneWay" class="radio" name="iternary" onchange="return showOneway();" type="radio" />One-Way--%>
                                    <input id="rdbOneWay" class="radio" name="iternary" ng-click="showOneway()" type="radio">One Way
                               
                                </div>
                                <div>
                                    <%--<input class="radio" name="iternary" checked="checked" onchange="return showReturn();" id="rdbRoundTrip" type="radio" />Return--%>
                                     <input class="radio" name="iternary" checked="checked" ng-click="showReturn()" id="rdbRoundTrip" type="radio">Return
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col1">
                                    <div class="row1 clearfix">
                                        <div class="fromto clearfix">
                                            <div class="from autogen">
                                                <input class="space1 schflderr" type="text" ng-click='empty("textBoxFrom")' id="textBoxFrom" autocomplete="off" placeholder="From" runat="server" ng-model="Fromcountry" typeahead-min-length='3' uib-typeahead="countryFrom for countryFrom in getLocation($viewValue)" />
                                            </div>
                                            <div class="ico_switch"></div>
                                            <div class="to autogen">
                                                <input class="space1" type="text" runat="server" ng-click='empty("textBoxTo")' id="textBoxTo" autocomplete="off" placeholder="To" ng-model="Tocountry" typeahead-min-length='3' uib-typeahead="countryTo for countryTo in getLocation($viewValue)" />
                                            </div>
                                        </div>
                                        <div class="class">
                                            <div class="selectdiv">
                                                <asp:DropDownList CssClass="Travel Class" ID="dropDownListEconomy" runat="server">
                                                    <asp:ListItem Selected="True">Economy</asp:ListItem>
                                                    <asp:ListItem>Business</asp:ListItem>
                                                    <asp:ListItem>First</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="airlines">
                                            <div class="autogen">
                                                <input class="space1" id="txtAirline" ng-click='empty("txtAirline")' runat="server" autocomplete="off" type="text" ng-model="GetAllAirlines" uib-typeahead="Carrier.name for Carrier in getAllAirlines($viewValue)" typeahead-min-length='3' placeholder="Airlines Preference" typeahead-on-select="onSelectAirlines($item, $model, $label)" />
                                                <asp:HiddenField runat="server" ID="hdnCarrier" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row2 clearfix">
                                        <div class="depret clearfix">
                                            <div class="departure">
                                                <input autocomplete="off" class="space2" type="text" id="txtDepart" runat="server" placeholder="Departure" />
                                            </div>

                                            <div class="return" id="retnli">
                                                <input autocomplete="off" class="space2" type="text" id="txtReturn" runat="server" placeholder="Return" />
                                            </div>
                                        </div>
                                        <div class="passgnr clearfix">
                                            <div class="adult">
                                                <div class="selectdiv">
                                                    <%--<select>
                                                        <option value="Airlines Prefernce">Adult (+12 yrs)</option>
                                                        <option value="Economy">1</option>
                                                        <option value="Business">2</option>
                                                        <option value="First">3</option>
                                                    </select>--%>
                                                    <asp:DropDownList ID="DropDownListAdult" runat="server" CssClass="space2">
                                                        <asp:ListItem Value="1" Selected="True">Adult</asp:ListItem>
                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                        <asp:ListItem Value="3">3</asp:ListItem>
                                                        <asp:ListItem Value="4">4</asp:ListItem>
                                                        <asp:ListItem Value="5">5</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="adult">
                                                <div class="selectdiv">
                                                    <%--<select>
                                                        <option value="Airlines Prefernce">Child <span>(-12 yrs)</span></option>
                                                        <option value="Economy">1</option>
                                                        <option value="Business">2</option>
                                                        <option value="First">3</option>
                                                    </select>--%>
                                                    <asp:DropDownList ID="DropDownListChild" runat="server" CssClass="space2">
                                                        <asp:ListItem Value="0" Selected="True">Child</asp:ListItem>
                                                        <asp:ListItem Value="1">1</asp:ListItem>
                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                        <asp:ListItem Value="3">3</asp:ListItem>
                                                        <asp:ListItem Value="4">4</asp:ListItem>
                                                        <asp:ListItem Value="5">5</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="adult lst">
                                                <div class="selectdiv">
                                                    <%--<select>
                                                        <option value="Airlines Prefernce">Infant <span>(0-2 yrs)</span></option>
                                                        <option value="Economy">1</option>
                                                        <option value="Business">2</option>
                                                        <option value="First">3</option>
                                                    </select>--%>
                                                    <asp:DropDownList ID="DropDownListInfant" runat="server" CssClass="space2">
                                                        <asp:ListItem Value="0" Selected="True">Infant</asp:ListItem>
                                                        <asp:ListItem Value="1">1</asp:ListItem>
                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                        <asp:ListItem Value="3">3</asp:ListItem>
                                                        <asp:ListItem Value="4">4</asp:ListItem>
                                                        <asp:ListItem Value="5">5</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col2">
                                    <div>
                                        <input type="checkbox" />
                                        Redeem Points
                                    </div>
                                    <br class="clr">
                                    <%--<button ng-click="FlightValidation()">Search Now</button>--%>
                                    <span class="btnS" ng-click="FlightValidation()">Search Now</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="srchpanel hotel" style="display: none;" id="divShtl">
                         <div class="wrap">
                            <div class="clearfix">
                                <div class="col1">
                                    <div class="row1 clearfix">
                                        <div class="destination autogen">
                                            <input placeholder="Destination" type="text" id="txtCity" ng-click='empty("txtCity")' runat="server" typeahead-min-length='3' autocomplete="off" ng-model="Hotelcountry" uib-typeahead="HotelCity for HotelCity in getAllHotelCities($viewValue)" />
                                        </div>
                                        <div class="checkin">
                                            <input placeholder="Check-In" type="text" id="TextBoxCheckin" autocomplete="off" runat="server" />
                                        </div>
                                        <div class="checkout">
                                            <input placeholder="Check-Out" type="text" id="TextBoxCheckout" autocomplete="off" runat="server" />
                                        </div>
                                        <div class="rooms lst">
                                            <div class="selectdiv">
                                                <select id="ddlRoomno">
                                                    <option value="1">1 Room</option>
                                                    <option value="2">2 Room</option>
                                                    <option value="3">3 Room</option>
                                                    <option value="4">4 Room</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="hdnRoomString" runat="server"></asp:HiddenField>
                                    <div class="row2 clearfix" id="RoomCount">
                                        
                                    </div>
                                    
                                </div>
                                <div class="col2">
                                    <div>
                                        <input type="checkbox" />
                                        Redeem Points
                                    </div>
                                    <br class="clr" />
                                    <span class="btnS" ng-click="SearchRoom()">Search Now</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="srchpanel car" style="display: none;" id="divScar">
                        <div class="wrap">

                            <div class="clearfix">
                                <div class="col1">
                                    <div class="row1 clearfix">
                                        <div class="pickuploc clearfix">
                                            <div class="pickupcountry">
                                                <input placeholder="Country" type="text">
                                            </div>
                                            <div class="pickupcity">
                                                <div class="inputbg">
                                                    <select>
                                                        <option>City</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="pickuploction">
                                                <div class="inputbg">
                                                    <select>
                                                        <option>Location</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ico_switch "></div>
                                        <div class="dropoffloc clearfix">
                                            <div class="dropoffcountry">
                                                <input placeholder="Country" type="text">
                                            </div>
                                            <div class="dropoffcity">
                                                <div class="inputbg">
                                                    <select>
                                                        <option>City</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="dropoffloaction">
                                                <div class="inputbg">
                                                    <select>
                                                        <option>Location</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row2 clearfix">
                                        <div class="pickupdttm clearfix">
                                            <div class="pickupdt">
                                                <input placeholder="Pick-Up Date" type="text">
                                            </div>
                                            <div class="pickuptm clearfix">
                                                <label>Pick-Up Time</label>
                                                <div class="inputbg">
                                                    <select>
                                                        <option>00</option>
                                                        <option>00</option>
                                                    </select>
                                                </div>
                                                <div class="inputbg lst">
                                                    <select>
                                                        <option value="Airlines Prefernce">00</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dropoffdttm clearfix">
                                            <div class="pickupdt">
                                                <input placeholder="Drop-Off Date" type="text">
                                            </div>
                                            <div class="pickuptm clearfix">
                                                <label>Drop-Off Time</label>
                                                <div class="inputbg">
                                                    <select>
                                                        <option>00</option>
                                                        <option>00</option>
                                                    </select>
                                                </div>
                                                <div class="inputbg lst">
                                                    <select>
                                                        <option value="Airlines Prefernce">00</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row3 clearfix">
                                        <div class="returnsameloc">
                                            <input type="checkbox" />
                                            Return to Same Locations
                                        </div>
                                        <div class="driverage">
                                            <input type="checkbox" />
                                            Driver aged between 25-70 years?
                                        </div>
                                        <div class="driverinput">
                                            Driver’s age
                                    <input type="text" />
                                            yrs
                                        </div>

                                    </div>
                                </div>
                                <div class="col2">
                                    <div>
                                        <input type="checkbox" />
                                        Redeem Points
                                    </div>
                                    <button>Search Now</button>
                                </div>

                            </div>

                        </div>
                    </div>
        <div class="srchpanel lounge" style="display: none;" id="divSlou">
         <div class="wrap">
          <div class="clearfix">
            <div class="col1">
                <span id="CP_Span1" class="ValidationDiv" style="display:none;"></span>
                <div class="row1 clearfix">
                                        
            <div class="pickuplocsingle clearfix" id="Div1">
                <div class="pickupcity">
                    <div class="selectdiv">
                        <select name="ctl00$CP$Select10" id="CP_Select10" onchange="return PickupCityChange()">
	                    <option selected="selected" value="Select City">Country</option>
                    </select>
                        </div>
                    </div>  
                        <div class="pickupcity">
                            <div class="selectdiv">
                                <select name="ctl00$CP$Select1" id="CP_Select1" onchange="return PickupCityChange()">
	                       <option selected="selected" value="Select City">City</option>
                            </select>
                         </div>
                        </div>
                        <div class="pickuploction">
                            <div class="selectdiv">
                                <select name="ctl00$CP$Select2" id="CP_Select2" onchange="return PickupLocationSelect()">
	                        <option selected="selected" value="Select Location">Airport</option>
                              </select>
                                 </div>
                                </div>
                                </div>
                                    </div>
                                    <div class="row2 clearfix">
                                        <div class="pickupcity">
                                         <div class="selectdiv">
                                          <select name="ctl00$CP$Select3" id="CP_Select3" onchange="return PickupCityChange()">
	                                      <option selected="selected" value="Select City">Lounge</option>
                                         </select>
                                        </div>
                                      </div>  
                                    </div>
                                    
                                </div>

                                <div class="col2">
                                    <div>
                                        <input type="checkbox">
                                        Redeem Points
                                    </div>

                                    <br class="clr">
                                    <span class="btnS" ng-click="CarSearch()">Search Now</span>
                               </div>
                            </div>
                        </div>
                    </div>

                    <div class="srchpanel car" style="display: none;" id="divStou">
                        <div class="wrap">
                            <div class="clearfix">
                                <div class="col1">
                                    <span id="CP_Span2" class="ValidationDiv" style="display:none;"></span>
                                    <div class="row1 clearfix">
                                        <div class="autogen tourSearch">
                                            <input placeholder="Destination" type="text" id="txtDestination" ng-click='empty("txtCity")' runat="server" typeahead-min-length='3' autocomplete="off" ng-model="Hotelcountry" uib-typeahead="HotelCity for HotelCity in getAllTourCities($viewValue)" />
                                            <input type="text" hidden="hidden" id="txtDestinationtourId" />
                                        </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div>
                                     <input type="checkbox">
                                        Redeem Points
                                    </div>
                                    <br class="clr">
                                    <span class="btnS" ng-click="TourValidation()">Search Now</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="text-align: center; margin-top: 10px;">
        <a href="#article">
            <img style="opacity: .5; height: 50px;" src="images/down-arrow.gif" /></a>
    </div>
    <article id="article">
        <div class="wrap">
            <h1>Journey into Something Memorable</h1>
        </div>
    </article>
    <div class="bnrlinks type2">
        <ul class="clearfix">
            <li>
                <a href="#">
                    <h2>The Desert Trail</h2>
                    <button>Explore</button>
                    <img src="images/bnrdesert.jpg">
                </a>
            </li>
        </ul>
        <ul class="clearfix">
            <li class="meetg-tours clearfix">
                <div class="tra-meetg">
                    <a href="#">
                        <h2>Burma, Home of Bliss</h2>
                        <button>Explore</button>
                        <img src="images/bnrburma.jpg">
                    </a>
                </div>
                <div class="tra-tours">
                    <a href="#">
                        <h2>The Alps & its View</h2>
                        <button>Explore</button>
                        <img src="images/bnralps.jpg">
                    </a>
                </div>
            </li>


        </ul>
        <ul class="clearfix">
            <li>
                <a href="#">
                    <h2>Natural Beauty at Hawaii</h2>
                    <button>Explore</button>
                    <img src="images/bnrbeauty.jpg">
                </a>
            </li>
        </ul>
    </div>
</asp:Content>

