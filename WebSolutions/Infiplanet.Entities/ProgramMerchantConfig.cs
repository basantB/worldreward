﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Core.Platform.Transactions.Entites;

namespace InfiPlanet.Entities
{
    [DataContract]
    [Serializable]
   public class ProgramMerchantConfig
   {
       #region "PRIVATE VARIABLES"

        private int mintId=0;
		private int mintProgramId=0;
		private string mstrProgramName=string.Empty;
		private string mstrMerchantId=string.Empty;
		private string mstrMerchantName=string.Empty;
        private LoyaltyTxnType menumLoyaltyTxnType;
		private string mstrMerchantPassword=string.Empty;
		private string mstrSecreteKey=string.Empty;
		private bool mboolIsActive=false;
		private string mstrCreatedBy=string.Empty;
		private DateTime mdtCreatedOn=DateTime.MinValue.ToUniversalTime(); 
		private string mstrUpdatedBy=string.Empty;
        private DateTime mdtUpdatedOn = DateTime.MinValue.ToUniversalTime(); 

       #endregion "PRIVATE VARIABLES"

       #region "PUBLIC PORPERTIES"

        [DataMember]
        public int Id
        {
            get { return mintId; }
            set { mintId = value; }
        }

        [DataMember]
        public int ProgramId
        {
            get { return mintProgramId; }
            set { mintProgramId = value; }
        }

        [DataMember]
        public string   ProgramName
        {
            get { return mstrProgramName; }
            set { mstrProgramName = value; }
        }

        [DataMember]
        public string MerchantName
        {
            get { return mstrMerchantName; }
            set { mstrMerchantName = value; }
        }

        [DataMember]
        public string MerchantId
        {
            get { return mstrMerchantId; }
            set { mstrMerchantId = value; }
        }

        [DataMember]
        public LoyaltyTxnType LoyaltyTxnType
        {
            get { return menumLoyaltyTxnType; }
            set { menumLoyaltyTxnType = value; }
        }

        [DataMember]
        public string MerchantPassword
        {
            get { return mstrMerchantPassword; }
            set { mstrMerchantPassword = value; }
        }

        [DataMember]
        public string SecreteKey
        {
            get { return mstrSecreteKey; }
            set { mstrSecreteKey = value; }
        }

        [DataMember]
        public bool IsActive
        {
            get { return mboolIsActive; }
            set { mboolIsActive = value; }
        }

        [DataMember]
        public DateTime CreatedOn
        {
            get { return mdtCreatedOn; }
            set { mdtCreatedOn = value; }
        }

        [DataMember]
        public DateTime UpdatedOn
        {
            get { return mdtUpdatedOn; }
            set { mdtUpdatedOn = value; }
        }

        [DataMember]
        public string CreatedBy
        {
            get { return mstrCreatedBy; }
            set { mstrCreatedBy = value; }
        }

        [DataMember]
        public string UpdatedBy
        {
            get { return mstrUpdatedBy; }
            set { mstrUpdatedBy = value; }
        }
       #endregion "PUBLIC PORPERTIES"

   }
}
