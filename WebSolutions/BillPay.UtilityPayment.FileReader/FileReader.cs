﻿using BillPay.UtilityPayment.Entities;
using Framework.EnterpriseLibrary.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BillPay.UtilityPayment.FileReader
{
    public class FileReader
    {
        public List<UtilityCompany> MapCompanyList(string pstrFilePath)
        {
            List<UtilityCompany> lobjListUtilityCompany = null;
            try
            {
                string[] strAllCompany = null;
                string strFileContent = System.IO.File.ReadAllText(pstrFilePath);
                strAllCompany = strFileContent.Split('~');
                lobjListUtilityCompany = new List<UtilityCompany>();
                for (int i = 0; i < strAllCompany.Length - 1; i++)
                {

                    if (strAllCompany[i] != null && strAllCompany[i] != "")
                    {
                        UtilityCompany lobjUtilityCompany = new UtilityCompany();

                        string[] strCompany = strAllCompany[i].Split('|');
                        if (strCompany.Length == 3)
                        {
                            lobjUtilityCompany.AccountCode = strCompany[0].ToString().Trim();
                            lobjUtilityCompany.CompanyCode = strCompany[1].ToString().Trim();
                            lobjUtilityCompany.Company = strCompany[2].ToString().Trim();

                        }
                        lobjListUtilityCompany.Add(lobjUtilityCompany);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Map Company  - " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
            }


            return lobjListUtilityCompany;

        }

        public List<UtilityAccountType> MapAccountTypeList(string pstrFilePath)
        {

            List<UtilityAccountType> lobjListUtilityAccountType = null;
            try
            {
                string[] strAllAccountType = null;
                string strFileContent = System.IO.File.ReadAllText(pstrFilePath);
                strAllAccountType = strFileContent.Split('~');
                lobjListUtilityAccountType = new List<UtilityAccountType>();
                for (int i = 0; i < strAllAccountType.Length - 1; i++)
                {

                    if (strAllAccountType[i] != null && strAllAccountType[i] != "")
                    {
                        UtilityAccountType lobjUtilityAccountType = new UtilityAccountType();

                        string[] strAccountType = strAllAccountType[i].Split('|');
                        if (strAccountType.Length == 2)
                        {
                            //lobjUtilityAccountType.CompanyCode = strAccountType[0].ToString().Trim();
                            lobjUtilityAccountType.AccountCode = strAccountType[0].ToString().Trim();
                            lobjUtilityAccountType.AccountType = strAccountType[1].ToString().Trim();
                        }
                        lobjListUtilityAccountType.Add(lobjUtilityAccountType);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Map AccountType  - " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
            }


            return lobjListUtilityAccountType;
        }
    }
}
