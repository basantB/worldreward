﻿using Core.Platform.Member.Entites;
using Core.Platform.MemberManagement.BusinessFacade;
using Core.Platform.OTP.Entities;
using Core.Platform.OTP.Facade;
using Core.Platform.ProgramMaster.Entities;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.CommunicationEngine.Entity;
using Framework.EnterpriseLibrary.CommunicationEngine.Helper;
using InfiPlanet.DataAccess.DataMapper;
using InfiPlanet.DataAccess.SPWrapper;
using InfiPlanet.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfiPlanet.BusinessFacade
{
    public class BusinessFacade
    {

        public bool GenerateOTPForRegistration(OTPDetails pobjOTPDetails)
        {
            bool lbnlStatus = false;
            try
            {
                OTPFacade lobjOTPFacade = new OTPFacade();
                OTPDetails lobjOTPDetails = lobjOTPFacade.GenerateOTP(pobjOTPDetails);
                if (lobjOTPDetails != null)
                {
                    SearchMember lobjSearchMember = new SearchMember();
                    lobjSearchMember.RelationReference = pobjOTPDetails.UniquerefID;
                    lobjSearchMember.ProgramId = pobjOTPDetails.ProgramId;
                    lobjSearchMember.RelationType = pobjOTPDetails.RelationType;

                    MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                    MemberDetails lobjMemberDetails = null;
                    lobjMemberDetails = lobjMemberBusinessFacade.GetmemberDetails(lobjSearchMember);
                    if (lobjMemberDetails != null)
                    {
                        // SendEmail(lobjMemberDetails, lobjOTPDetails.OTP.ToString(), lobjOTPDetails.ExpiryDateTime);
                      lbnlStatus=  SendSMS(lobjMemberDetails, lobjOTPDetails.OTP.ToString(), lobjOTPDetails.ExpiryDateTime);
                        
                    }
                    else
                    {
                        LoggingAdapter.WriteLog("GenerateOTPForRegistration : MemberDetails Not Found - RelationReference :  " + pobjOTPDetails.UniquerefID);
                        lbnlStatus = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GenerateOTPForRegistration : - " + ex.Message.ToString());
                lbnlStatus = false;
            }
            return lbnlStatus;
        }

        private bool SendSMS(MemberDetails lobjMemberDetails, string pstrOTPstirng, DateTime pdtExpirydatetime)
        {
            bool lblnSMSSend = false;
            try
            {
                if (lobjMemberDetails.MobileNumber != string.Empty)
                {
                    string lstrexpirydt = pdtExpirydatetime.ToString("dd/MM/yyyy");
                    //Communication Engine Call For SMS. 
                    CEHelper lobjcehelper = new CEHelper();
                    SmsDetails lobjSmsDetail = new SmsDetails();
                    List<string> lstSmsparameter = new List<string>();
                    lstSmsparameter.Add(pstrOTPstirng);
                    //lstSmsparameter.Add(lstrexpirydt);
                    lobjSmsDetail.TemplateCode = "OTPSMS";
                    lobjSmsDetail.ListParameter = lstSmsparameter;
                    lobjSmsDetail.ReceiverMobile = Convert.ToString(lobjMemberDetails.MobileNumber);
                    lobjSmsDetail.MemberId = Convert.ToString(lobjMemberDetails.MemberRelationsList[0].RelationReference);
                    lobjSmsDetail.ProgramId = Convert.ToString(lobjMemberDetails.ProgramId);
                    lblnSMSSend = lobjcehelper.InsertSmsDetails(lobjSmsDetail);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("OTP SMS ERROR :" + ex.Message);
            }
            return lblnSMSSend;
        }

        private bool SendEmail(MemberDetails lobjMemberDetails, string pstrOTPstirng, DateTime pdtExpirydatetime)
        {
            bool lblnEmailSend = false;
            try
            {
                if (lobjMemberDetails.Email != string.Empty)
                {

                    string lstrexpirydt = pdtExpirydatetime.ToString("dd/MM/yyyy");

                    EmailDetails lobjEmailDetail = new EmailDetails();
                    List<string> lstEmailparameter = new List<string>();
                    List<string> lstAttachment = new List<string>();
                    CEHelper lobjcehelper = new CEHelper();
                    lstEmailparameter.Add(pstrOTPstirng);
                    lstEmailparameter.Add(lobjMemberDetails.LastName);
                    ////lstEmailparameter.Add(lstrexpirydt); //new line of code
                    //if (lobjMemberDetails.MemberRelationsList[0].Status.Equals(Status.Blocked))
                    //{
                    //    lobjEmailDetail.TemplateCode = "SendOTPForRegistration";
                    //}
                    //else if (lobjMemberDetails.MemberRelationsList[0].Status.Equals(Status.InActive))
                    //{
                    //    lobjEmailDetail.TemplateCode = "SendOTP";
                    //}
                    lobjEmailDetail.TemplateCode = "SendOTPForRegistration";

                    LoggingAdapter.WriteLog("OTP Email Parameter" + lstEmailparameter.Count);
                    lobjEmailDetail.ListParameter = lstEmailparameter;
                    lobjEmailDetail.AttachmentList = lstAttachment;
                    lobjEmailDetail.To = lobjMemberDetails.Email;
                    lobjEmailDetail.MemberId = Convert.ToString(lobjMemberDetails.MemberRelationsList[0].RelationReference);
                    lobjEmailDetail.ProgramId = Convert.ToString(lobjMemberDetails.ProgramId);
                    lblnEmailSend = lobjcehelper.InsertEmailDetails(lobjEmailDetail);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("OTP password error :" + ex.Message);
                throw ex;
            }
            return lblnEmailSend;
        }

        public bool VerifyOTPForRegistration(OTPDetails pobjOTPDetails)
        {
            bool lblnIsOTPupdated = false;
            try
            {
                OTPFacade lobjOTPFacade = new OTPFacade();
                lblnIsOTPupdated = lobjOTPFacade.UpdateOTP(pobjOTPDetails);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("VerifyOTPForRegistration - MemberId : " + pobjOTPDetails.UniquerefID);
                lblnIsOTPupdated = false;
            }
            return lblnIsOTPupdated;
        }

        public bool CheckMemberLoginDetails(SearchMember lobjSearchMemberForLogin)
        {
            return SPWrapper.CheckMemberLoginDetails(lobjSearchMemberForLogin);
        }

        public List<ProgramDefinition> GetInfiplanetProgramList()
        {
            return DataMapper.MapInfiplanetProgramList(SPWrapper.GetInfiplanetProgramList());
        }

        public ProgramMerchantConfig GetMerchantDetailsByProgramIdAndLoyaltyTxnType(ProgramMerchantConfig pobjProgramMerchantConfig)
        {
            return DataMapper.MapMerchantDetailsByProgramIdAndLoyaltyTxnType(SPWrapper.GetMerchantDetailsByProgramIdAndLoyaltyTxnType(pobjProgramMerchantConfig));

        }

        public bool IsLoginPrimaryReferenceAlreadyExist(SearchMember pobjSearchMember)
        {
            return SPWrapper.IsLoginPrimaryReferenceAlreadyExist(pobjSearchMember);
        }

        public bool ActivateMemberAccountByPrimaryReference(MemberDetails pobjMemberDetails)
        {
            return SPWrapper.ActivateMemberAccountByPrimaryReference(pobjMemberDetails);
        }

        public MemberDetails GetEnrollMemberDetailsByPrimaryReference(SearchMember pobjSearchMember)
        {
            return DataMapper.MapMemberDetails(SPWrapper.GetEnrollMemberDetailsByPrimaryReference(pobjSearchMember));
        }
    }
}
