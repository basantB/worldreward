﻿using BillPay.UtilityPayment.DataAccess;
using BillPay.UtilityPayment.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillPay.UtilityPayment.BussinessFacade
{
    public class UtilityPaymentFacade
    {
        public int InsertUtilityDetails(UtilityDetails pobjUtilityDetails)
        {
            return SPWrapper.InsertUtilityDetails(pobjUtilityDetails);
        }
        public bool UpdateUtilityDetails(UtilityDetails pobjUtilityDetails)
        {
            return SPWrapper.UpdateUtilityDetails(pobjUtilityDetails);
        }
        public bool UpdateUtilityDetailsStatus(UtilityDetails pobjUtilityDetails)
        {
            return SPWrapper.UpdateUtilityDetailsStatus(pobjUtilityDetails);
        }
    }
}
