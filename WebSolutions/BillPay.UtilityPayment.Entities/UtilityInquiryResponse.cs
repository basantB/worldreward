﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Serializable]
    public class UtilityInquiryResponse
    {
        #region "Private Variables"

        private UtilityHeader mUtilityHeader;
        private string mstrReferenceNumber = string.Empty;
        private string mstrUtilityCompanyCode = string.Empty;
        private string mstrUtilityAccount = string.Empty;
        private string mstrUtilityAccountType = string.Empty;
        private double mdblAmount = 0.0;
        private double mdblAmountMin = 0.0;
        private double mdblAmountMax = 0.0;
        private string mstrBillDate = string.Empty;
        private string mstrCurrentTimeStamp = string.Empty;
        private string mstrReturnCode = string.Empty;
        private string mstrReturnText = string.Empty;


        #endregion  "Private Variables"


        #region "Public Properties"

        [DataMember]
        public UtilityHeader UtilityHeader
        {
            get { return mUtilityHeader; }
            set { mUtilityHeader = value; }
        }
        [DataMember]
        public string ReferenceNumber
        {
            get { return mstrReferenceNumber; }
            set { mstrReferenceNumber = value; }
        }
        [DataMember]
        public string UtilityCompanyCode
        {
            get { return mstrUtilityCompanyCode; }
            set { mstrUtilityCompanyCode = value; }
        }
        [DataMember]
        public string UtilityAccount
        {
            get { return mstrUtilityAccount; }
            set { mstrUtilityAccount = value; }
        }
        [DataMember]
        public string UtilityAccountType
        {
            get { return mstrUtilityAccountType; }
            set { mstrUtilityAccountType = value; }
        }
        [DataMember]
        public double Amount
        {
            get { return mdblAmount; }
            set { mdblAmount = value; }
        }
        [DataMember]
        public double AmountMin
        {
            get { return mdblAmountMin; }
            set { mdblAmountMin = value; }
        }
        [DataMember]
        public double AmountMax
        {
            get { return mdblAmountMax; }
            set { mdblAmountMax = value; }
        }
        [DataMember]
        public string BillDate
        {
            get { return mstrBillDate; }
            set { mstrBillDate = value; }
        }
        [DataMember]
        public string CurrentTimeStamp
        {
            get { return mstrCurrentTimeStamp; }
            set { mstrCurrentTimeStamp = value; }
        }
        [DataMember]
        public string ReturnCode
        {
            get { return mstrReturnCode; }
            set { mstrReturnCode = value; }
        }
        [DataMember]
        public string ReturnText
        {
            get { return mstrReturnText; }
            set { mstrReturnText = value; }
        }

        #endregion  "Private Properties"
    }
}
