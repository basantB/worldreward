﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Serializable]
    public class UtilityInquiryRequest
    {
        #region "PRIVATE Variables"

        private string mstrReferenceNum = string.Empty;
        private string mstrUtilityCompanyCode = string.Empty;
        private string mstrUtilityCompanyName = string.Empty;
        private string mstrUtilityAccount = string.Empty;
        private string mstrUtilityAccountPin = string.Empty;
        private string mstrUtilityAccountType = string.Empty;
        private string mstrUtilityAccountName = string.Empty;
        private string mstrCustomerId = string.Empty;
        private string mstrAmount = string.Empty;
        private string mstrBankAccount = string.Empty;

        private string mstrTimeStamp = string.Empty;

        private UtilityType mEnumUtilityType;

        #endregion "PRIVATE Variables"

        #region "PUBLIC Properties"

        [DataMember]
        public string ReferenceNum
        {
            get { return mstrReferenceNum; }
            set { mstrReferenceNum = value; }
        }
        [DataMember]
        public string UtilityCompanyCode
        {
            get { return mstrUtilityCompanyCode; }
            set { mstrUtilityCompanyCode = value; }
        }
        [DataMember]
        public string UtilityAccount
        {
            get { return mstrUtilityAccount; }
            set { mstrUtilityAccount = value; }
        }
        [DataMember]
        public string UtilityAccountPin
        {
            get { return mstrUtilityAccountPin; }
            set { mstrUtilityAccountPin = value; }
        }
        [DataMember]
        public string UtilityAccountType
        {
            get { return mstrUtilityAccountType; }
            set { mstrUtilityAccountType = value; }
        }
        [DataMember]
        public string CustomerId
        {
            get { return mstrCustomerId; }
            set { mstrCustomerId = value; }
        }
        [DataMember]
        public string Amount
        {
            get { return mstrAmount; }
            set { mstrAmount = value; }
        }
        [DataMember]
        public string BankAccount
        {
            get { return mstrBankAccount; }
            set { mstrBankAccount = value; }
        }
        [DataMember]
        public string TimeStamp
        {
            get { return mstrTimeStamp; }
            set { mstrTimeStamp = value; }
        }
        [DataMember]
        public string UtilityCompanyName
        {
            get { return mstrUtilityCompanyName; }
            set { mstrUtilityCompanyName = value; }
        }
        [DataMember]
        public string UtilityAccountName
        {
            get { return mstrUtilityAccountName; }
            set { mstrUtilityAccountName = value; }
        }
        [DataMember]
        public UtilityType Type
        {
            get { return mEnumUtilityType; }
            set { mEnumUtilityType = value; }
        }
        #endregion "PUBLIC Properties"
    }
}
