﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Serializable]
    public class UtilityDetails
    {
        #region "Private Variables"

        private int mintId = 0;
        private string mstrReferenceNumber = string.Empty;
        private string mstrUtilityCompanyCode = string.Empty;
        private string mstrUtilityAccount = string.Empty;
        private string mstrUtilityAccountType = string.Empty;
        private string mstrCustomerId = string.Empty;
        private string mstrPaymentMode = string.Empty;
        private string mstrCardNoFlag = string.Empty;
        private string mstrBankAccount = string.Empty;
        private double mdblAmount = 0.0;
        private UtilityStatus menumUtilityStatus;
        private UtilityType menumUtilityType;
        private string mstrReturnCode = string.Empty;
        private string mstrAdditionalDetails1 = string.Empty;
        private string mstrAdditionalDetails2 = string.Empty;
        private string mstrAdditionalDetails3 = string.Empty;
        private string mstrAdditionalDetails4 = string.Empty;
        private string mstrAdditionalDetails5 = string.Empty;

        private string mstrCreatedBy = string.Empty;
        private string mstrUpdatedBy = string.Empty;
        private string mstrExternalReference = string.Empty;

        private DateTime mdtCreatedOn = DateTime.MinValue.ToUniversalTime();
        private DateTime mdtUpdatedOn = DateTime.MinValue.ToUniversalTime();
        private DateTime mdtRequestedDate = DateTime.MinValue.ToUniversalTime();
        private DateTime mdtResponseDate = DateTime.MinValue.ToUniversalTime();
        private string mstrTimeStamp = string.Empty;

        private string mstrRequestString = string.Empty;
        private string mstrResponseString = string.Empty;

        #endregion "Private Variables"

        #region "Public Properties"

        [DataMember]
        public int Id
        {
            get { return mintId; }
            set { mintId = value; }
        }
        [DataMember]
        public string ReferenceNumber
        {
            get { return mstrReferenceNumber; }
            set { mstrReferenceNumber = value; }
        }
        [DataMember]
        public string UtilityCompanyCode
        {
            get { return mstrUtilityCompanyCode; }
            set { mstrUtilityCompanyCode = value; }
        }
        [DataMember]
        public string UtilityAccount
        {
            get { return mstrUtilityAccount; }
            set { mstrUtilityAccount = value; }
        }
        [DataMember]
        public string UtilityAccountType
        {
            get { return mstrUtilityAccountType; }
            set { mstrUtilityAccountType = value; }
        }
        [DataMember]
        public string PaymentMode
        {
            get { return mstrPaymentMode; }
            set { mstrPaymentMode = value; }
        }
        [DataMember]
        public string CardNoFlag
        {
            get { return mstrCardNoFlag; }
            set { mstrCardNoFlag = value; }
        }
        [DataMember]
        public string BankAccount
        {
            get { return mstrBankAccount; }
            set { mstrBankAccount = value; }
        }
        [DataMember]
        public string CustomerId
        {
            get { return mstrCustomerId; }
            set { mstrCustomerId = value; }
        }
        [DataMember]
        public Double Amount
        {
            get { return mdblAmount; }
            set { mdblAmount = value; }
        }
        [DataMember]
        public UtilityStatus Status
        {
            get { return menumUtilityStatus; }
            set { menumUtilityStatus = value; }
        }
        [DataMember]
        public UtilityType Type
        {
            get { return menumUtilityType; }
            set { menumUtilityType = value; }
        }
        [DataMember]
        public string ReturnCode
        {
            get { return mstrReturnCode; }
            set { mstrReturnCode = value; }
        }
        [DataMember]
        public string AdditionalDetails1
        {
            get { return mstrAdditionalDetails1; }
            set { mstrAdditionalDetails1 = value; }
        }
        [DataMember]
        public string AdditionalDetails2
        {
            get { return mstrAdditionalDetails2; }
            set { mstrAdditionalDetails2 = value; }
        }
        [DataMember]
        public string AdditionalDetails3
        {
            get { return mstrAdditionalDetails3; }
            set { mstrAdditionalDetails3 = value; }
        }
        [DataMember]
        public string AdditionalDetails4
        {
            get { return mstrAdditionalDetails4; }
            set { mstrAdditionalDetails4 = value; }
        }
        [DataMember]
        public string AdditionalDetails5
        {
            get { return mstrAdditionalDetails5; }
            set { mstrAdditionalDetails5 = value; }
        }
        [DataMember]
        public string CreatedBy
        {
            get { return mstrCreatedBy; }
            set { mstrCreatedBy = value; }
        }
        [DataMember]
        public string UpdatedBy
        {
            get { return mstrUpdatedBy; }
            set { mstrUpdatedBy = value; }
        }
        [DataMember]
        public DateTime CreatedOn
        {
            get { return mdtCreatedOn; }
            set { mdtCreatedOn = value; }
        }
        [DataMember]
        public DateTime UpdatedOn
        {
            get { return mdtUpdatedOn; }
            set { mdtUpdatedOn = value; }
        }
        [DataMember]
        public DateTime RequestedDate
        {
            get { return mdtRequestedDate; }
            set { mdtRequestedDate = value; }

        }
        [DataMember]
        public DateTime ResponseDate
        {
            get { return mdtResponseDate; }
            set { mdtResponseDate = value; }

        }
        [DataMember]
        public string TimeStamp
        {
            get { return mstrTimeStamp; }
            set { mstrTimeStamp = value; }
        }
        [DataMember]
        public string ExternalReference
        {
            get { return mstrExternalReference; }
            set { mstrExternalReference = value; }
        }
        [DataMember]
        public string RequestString
        {
            get { return mstrRequestString; }
            set { mstrRequestString = value; }
        }
        [DataMember]
        public string ResponseString
        {
            get { return mstrResponseString; }
            set { mstrResponseString = value; }
        }
        #endregion "Public Properties"
    }
}
