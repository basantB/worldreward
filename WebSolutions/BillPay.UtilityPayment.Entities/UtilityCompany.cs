﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Serializable]
    public class UtilityCompany
    {
        #region "PRIVATE Variables"

        private string mstrCompanyCode = string.Empty;
        private string mstrCompany = string.Empty;
        private string mstrAccountCode = string.Empty;
        
        #endregion "PRIVATE Variables"

        #region "PUBLIC Properties"
        [DataMember]
        public string CompanyCode
        {
            get { return mstrCompanyCode; }
            set { mstrCompanyCode = value; }
        }
        [DataMember]
        public string Company
        {
            get { return mstrCompany; }
            set { mstrCompany = value; }
        }
        [DataMember]
        public string AccountCode
        {
            get { return mstrAccountCode; }
            set { mstrAccountCode = value; }
        }

        #endregion "PUBLIC Properties"
    }
}
