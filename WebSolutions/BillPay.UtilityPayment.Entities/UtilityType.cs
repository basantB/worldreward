﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Flags]
    public enum UtilityType
    {
        [EnumMember]
        UtilityEnquiry = 1,
        [EnumMember]
        UtilityPayment = 2,
        [EnumMember]
        CharityEnquiry = 3,
        [EnumMember]
        CharityPayment = 4
    }
}
