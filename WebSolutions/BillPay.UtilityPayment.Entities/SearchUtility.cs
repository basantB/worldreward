﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Serializable]
    public class SearchUtility
    {
        #region "Private Variables"

        private string mstrReferenceNo = string.Empty;
        private UtilityStatus menumUtilityStatus;
        private UtilityType menumUtilityType;

        #endregion "Private Variables"

        #region "Public Properties"
        [DataMember]
        public string ReferenceNo
        {
            get { return mstrReferenceNo; }
            set { mstrReferenceNo=value; }
        }

        [DataMember]
        public UtilityStatus Status
        {
            get { return menumUtilityStatus; }
            set { menumUtilityStatus = value; }
        }

        [DataMember]
        public UtilityType Type
        {
            get { return menumUtilityType; }
            set { menumUtilityType = value; }
        }

        #endregion "Public Properties"

    }
}
