﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Serializable]
    public class UtilityAccountType
    {

        #region "PRIVATE Variables"

        private string mstrCompanyCode = string.Empty;
        private string mstrAccountCode = string.Empty;
        private string mstrAccountType = string.Empty;

        #endregion "PRIVATE Variables"

        #region "PUBLIC Properties"
        [DataMember]
        public string CompanyCode
        {
            get { return mstrCompanyCode; }
            set { mstrCompanyCode = value; }
        }
        [DataMember]
        public string AccountCode
        {
            get { return mstrAccountCode; }
            set { mstrAccountCode = value; }
        }
        [DataMember]
        public string AccountType
        {
            get { return mstrAccountType; }
            set { mstrAccountType = value; }
        }

        #endregion "PUBLIC Properties"

    }
}
