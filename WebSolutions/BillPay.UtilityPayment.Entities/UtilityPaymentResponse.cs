﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Serializable]
    public class UtilityPaymentResponse
    {
        #region "PRIVATE Variables"

        private UtilityHeader mUtilityHeader;
        private string mstrReferenceNumber = string.Empty;
        private string mstrUtilityCompanyCode = string.Empty;
        private string mstrUtilityAccount = string.Empty;
        private string mstrUtilityAccountType = string.Empty;
        private string mstrBankAccount = string.Empty;
        private string mstrBankAccountCur = string.Empty;
        private double mdblAmount = 0.0;
        private string mstrReturnCode = string.Empty;
        private string mstrReturnText = string.Empty;


        #endregion "PRIVATE Variables"

        #region "PUBLIC Properties"
        [DataMember]
        public UtilityHeader UtilityHeader
        {
            get { return mUtilityHeader; }
            set { mUtilityHeader = value; }
        }
        [DataMember]
        public string ReferenceNumber
        {
            get { return mstrReferenceNumber; }
            set { mstrReferenceNumber = value; }
        }
        [DataMember]
        public string UtilityCompanyCode
        {
            get { return mstrUtilityCompanyCode; }
            set { mstrUtilityCompanyCode = value; }
        }
        [DataMember]
        public string UtilityAccount
        {
            get { return mstrUtilityAccount; }
            set { mstrUtilityAccount = value; }
        }
        [DataMember]
        public string UtilityAccountType
        {
            get { return mstrUtilityAccountType; }
            set { mstrUtilityAccountType = value; }
        }
        [DataMember]
        public string BankAccount
        {
            get { return mstrBankAccount; }
            set { mstrBankAccount = value; }
        }
        [DataMember]
        public string BankAccountCur
        {
            get { return mstrBankAccountCur; }
            set { mstrBankAccountCur = value; }
        }
        [DataMember]
        public double Amount
        {
            get { return mdblAmount; }
            set { mdblAmount = value; }
        }
        [DataMember]
        public string ReturnCode
        {
            get { return mstrReturnCode; }
            set { mstrReturnCode = value; }
        }
        [DataMember]
        public string ReturnText
        {
            get { return mstrReturnText; }
            set { mstrReturnText = value; }
        }
       
        #endregion "PUBLIC Properties"
    }
}
