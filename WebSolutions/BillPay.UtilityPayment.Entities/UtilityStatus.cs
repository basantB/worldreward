﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Flags]
    public enum UtilityStatus
    {
        [EnumMember]
        Pending = 1,
        [EnumMember]
        Failed = 2,
        [EnumMember]
        Successful = 3
    }
}
