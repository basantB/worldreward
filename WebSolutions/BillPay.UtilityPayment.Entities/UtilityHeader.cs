﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Serializable]
   public class UtilityHeader
   {
       #region "PRIVATE Variables"

       private string mstrMsgFormat = string.Empty;
       private string mstrMsgVersion = string.Empty;
       private string mstrRequestorId = string.Empty;
       private string mstrRequestorChannelId = string.Empty;
       private string mstrRequestorUserId = string.Empty;
       private string mstrRequestorLanguage = string.Empty;
       private string mstrRequestorSecurityInfo = string.Empty;
       private string mstrEaiReference = string.Empty;
       private string mstrReturnCode = string.Empty;

       #endregion "PRIVATE Variables"

       #region "PUBLIC Properties"

       [DataMember]
       public string MsgFormat
       {
           get { return mstrMsgFormat; }
           set { mstrMsgFormat = value; }
       }
       [DataMember]
       public string MsgVersion
       {
           get { return mstrMsgVersion; }
           set { mstrMsgVersion = value; }
       }
       [DataMember]
       public string RequestorId
       {
           get { return mstrRequestorId; }
           set { mstrRequestorId = value; }
       }
       [DataMember]
       public string RequestorChannelId
       {
           get { return mstrRequestorChannelId; }
           set { mstrRequestorChannelId = value; }
       }
       [DataMember]
       public string RequestorUserId
       {
           get { return mstrRequestorUserId; }
           set { mstrRequestorUserId = value; }
       }
       [DataMember]
       public string RequestorLanguage
       {
           get { return mstrRequestorLanguage; }
           set { mstrRequestorLanguage = value; }
       }
       [DataMember]
       public string RequestorSecurityInfo
       {
           get { return mstrRequestorSecurityInfo; }
           set { mstrRequestorSecurityInfo = value; }
       }
       [DataMember]
       public string EaiReference
       {
           get { return mstrEaiReference; }
           set { mstrEaiReference = value; }
       }
       [DataMember]
       public string ReturnCode
       {
           get { return mstrReturnCode; }
           set { mstrReturnCode = value; }
       }

       #endregion "PUBLIC Properties"
   }
}
