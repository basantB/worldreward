﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BillPay.UtilityPayment.Entities
{
    [DataContract]
    [Serializable]
   public class UtilityPaymentRequest
   {
       #region "PRIVATE Variables"

       private string mstrReferenceNum = string.Empty;
       private string mstrCustomerId = string.Empty;
       private string mstrUtilityCompanyCode = string.Empty;
       private string mstrUtilityAccount = string.Empty;
       private string mstrUtilityAccountType = string.Empty;
       private string mstrPaymentMode = string.Empty;
       private string mstrCardNoFlag = string.Empty;
       private string mstrBankAccount = string.Empty;
       private string mstrBankAccountCur = string.Empty;
       private double mdblAmountDue = 0.0;
       private double mdblAmountPaid = 0.0;
       private string mstrOrgRefNumber = string.Empty;
       private string mstrTimeStamp =string.Empty;
       private string mstrExternalReference = string.Empty;
       private string mstrUtilityCompanyName = string.Empty;
       private string mstrUtilityAccountName = string.Empty;

       private UtilityType mEnumUtilityType;
       #endregion "PRIVATE Variables"

       #region "PUBLIC Properties"
        [DataMember]
       public string ReferenceNumber
       {
           get { return mstrReferenceNum; }
           set { mstrReferenceNum = value; }
       }
        [DataMember]
        public string CustomerId
        {
            get { return mstrCustomerId; }
            set { mstrCustomerId = value; }
        }
       [DataMember]
       public string UtilityCompanyCode
       {
           get { return mstrUtilityCompanyCode; }
           set { mstrUtilityCompanyCode = value; }
       }
       [DataMember]
       public string UtilityAccount
       {
           get { return mstrUtilityAccount; }
           set { mstrUtilityAccount = value; }
       }
       [DataMember]
       public string UtilityAccountType
       {
           get { return mstrUtilityAccountType; }
           set { mstrUtilityAccountType = value; }
       }
       [DataMember]
       public string PaymentMode
       {
           get { return mstrPaymentMode; }
           set { mstrPaymentMode = value; }
       }
       [DataMember]
       public string CardNoFlag
       {
           get { return mstrCardNoFlag ; }
           set { mstrCardNoFlag = value; }
       }
       [DataMember]
       public string BankAccount
       {
           get { return mstrBankAccount; }
           set { mstrBankAccount = value; }
       }
       [DataMember]
       public string BankAccountCur
       {
           get { return mstrBankAccountCur; }
           set { mstrBankAccountCur = value; }
       }
       [DataMember]
       public double AmountDue
       {
           get { return mdblAmountDue; }
           set { mdblAmountDue = value; }
       }
       [DataMember]
       public double AmountPaid
       {
           get { return mdblAmountPaid; }
           set { mdblAmountPaid = value; }
       }
       [DataMember]
       public string OrgRefNumber
       {
           get { return mstrOrgRefNumber; }
           set { mstrOrgRefNumber = value; }
       }
       [DataMember]
       public string TimeStamp
       {
           get { return mstrTimeStamp; }
           set { mstrTimeStamp = value; }
       }
       [DataMember]
       public string ExternalReference
       {
           get { return mstrExternalReference; }
           set { mstrExternalReference = value; }
       }
       [DataMember]
       public string UtilityCompanyName
       {
           get { return mstrUtilityCompanyName; }
           set { mstrUtilityCompanyName = value; }
       }
       [DataMember]
       public string UtilityAccountName
       {
           get { return mstrUtilityAccountName; }
           set { mstrUtilityAccountName = value; }
       }
       [DataMember]
       public UtilityType Type
       {
           get { return mEnumUtilityType; }
           set { mEnumUtilityType = value; }
       }
       #endregion "PUBLIC Properties"
   }
}
