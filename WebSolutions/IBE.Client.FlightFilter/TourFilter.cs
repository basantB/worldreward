﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IBE.Client.FlightFilter
{
    [DataContract]
    [Serializable]
    public class TourFilter
    {
        private int mintPageSize;

        [DataMember]
        public int PageSize
        {
            get { return mintPageSize; }
            set { mintPageSize = value; }
        }
    }
}
