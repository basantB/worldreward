﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Core.Platform.SocialTransfer.Entities
{
    [DataContract]
    [Serializable]
    public class SocialTransferPool
    {
        #region Variables

        int mintId = 0;
        string mstrSender_Unique_Reference = string.Empty;
        string mstrSocial_Id = string.Empty;
        int mintSocial_Type = 0;
        int mintPoints = 0;
        string mstrTransaction_Id = string.Empty;
        int mintProgram_Id = 0;
        DateTime mdateCreated_Date = DateTime.MinValue;
        DateTime mdateUpdated_Date = DateTime.MinValue;
        bool mblnIsActive = false;
        string mstrAdditional_Details1 = string.Empty;
        string mstrAdditional_Details2 = string.Empty;
        string mstrAdditional_Details3 = string.Empty;
        string mstrAdditional_Details4 = string.Empty;
        string mstrAdditional_Details5 = string.Empty;

        #endregion


        #region Properties

        [DataMember]
        public int Id
        {
            get { return mintId; }
            set { mintId = value; }
        }

        [DataMember]
        public string SenderUniqueReference
        {
            get { return mstrSender_Unique_Reference; }
            set { mstrSender_Unique_Reference = value; }
        }

        [DataMember]
        public string SocialId
        {
            get { return mstrSocial_Id; }
            set { mstrSocial_Id = value; }
        }

        [DataMember]
        public int SocialType
        {
            get { return mintSocial_Type; }
            set { mintSocial_Type = value; }
        }

        [DataMember]
        public int Points
        {
            get { return mintPoints; }
            set { mintPoints = value; }
        }

        [DataMember]
        public string TransactionId
        {
            get { return mstrTransaction_Id; }
            set { mstrTransaction_Id = value; }
        }

        [DataMember]
        public int ProgramId
        {
            get { return mintProgram_Id; }
            set { mintProgram_Id = value; }
        }

        [DataMember]
        public DateTime CreatedDate
        {
            get { return mdateCreated_Date; }
            set { mdateCreated_Date = value; }
        }

        [DataMember]
        public DateTime UpdatedDate
        {
            get { return mdateUpdated_Date; }
            set { mdateUpdated_Date = value; }
        }

        [DataMember]
        public bool IsActive
        {
            get { return mblnIsActive; }
            set { mblnIsActive = value; }
        }

        [DataMember]
        public string AdditionalDetails1
        {
            get { return mstrAdditional_Details1; }
            set { mstrAdditional_Details1 = value; }
        }

        [DataMember]
        public string AdditionalDetails2
        {
            get { return mstrAdditional_Details2; }
            set { mstrAdditional_Details2 = value; }
        }

        [DataMember]
        public string AdditionalDetails3
        {
            get { return mstrAdditional_Details3; }
            set { mstrAdditional_Details3 = value; }
        }

        [DataMember]
        public string AdditionalDetails4
        {
            get { return mstrAdditional_Details4; }
            set { mstrAdditional_Details4 = value; }
        }

        [DataMember]
        public string AdditionalDetails5
        {
            get { return mstrAdditional_Details5; }
            set { mstrAdditional_Details5 = value; }
        }

        #endregion
    }
}
