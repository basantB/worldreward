﻿using Core.Platform.Member.Entites;
using Core.Platform.PointGateway.Service.Helper;
using Core.Platform.ProgramInterface.ClientHelper;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.SocialConnect.BusinessFacade;
using Core.Platform.SocialConnect.Entities;
using Core.Platform.SocialTransfer.DataAccess.DataMapper;
using Core.Platform.SocialTransfer.DataAccess.SPWrapper;
using Core.Platform.SocialTransfer.Entities;
using Core.Platform.TransactionManagement.BusinessFacade;
using Core.Platform.Transactions.Entites;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.CommunicationEngine.Entity;
using Framework.EnterpriseLibrary.CommunicationEngine.Helper;
using Framework.EnterpriseLibrary.UniqueNumberGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Core.Platform.SocialTransfer.BussinessFacade
{
    public class SocialTransferFacade
    {
        string ProgramName = Convert.ToString(ConfigurationManager.AppSettings["ProgramName"]);
        string lstrMerchantId = Convert.ToString(ConfigurationManager.AppSettings["MerchantId"]);
        string lstrMerchantUserName = Convert.ToString(ConfigurationManager.AppSettings["MerchantUserName"]);
        string lstrMerchantPassword = Convert.ToString(ConfigurationManager.AppSettings["MerchantPassword"]);

        /// <summary>
        /// TransferPoints
        /// </summary>
        /// <param name="pstrSenderExtrenalReference"></param>
        /// <param name="pstrReceiverSocialId"></param>
        /// <param name="pobjSocialType"></param>
        /// <param name="pintTransferPoints"></param>
        /// <param name="pstrProgramCurrency"></param>
        /// <returns></returns>
        public string TransferPoints(MemberDetails pobjSenderMemberDetails, string pstrReceiverSocialId, string pstrReceiverSocialName, SocialType pobjSocialType, int pintTransferPoints, string pstrProgramCurrency)
        {
            int lintExpiryMonth = 0;
            int lintAvailablePoints = 0;
            string lstrResult = string.Empty;
            string pstrSenderExtrenalReference = pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
            try
            {
                InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
                ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
                lobjProgramDefinition = lobjInterfaceHelper.GetProgramDefinition(ProgramName);

                List<ProgramCurrencyDefinition> lobjProgramCurrencyDefinitionlist = new List<ProgramCurrencyDefinition>();
                lobjProgramCurrencyDefinitionlist = lobjInterfaceHelper.GetProgramCurrencyDefinition(lobjProgramDefinition.ProgramId);

                lintExpiryMonth = lobjProgramCurrencyDefinitionlist.Find(lobj => lobj.Currency.Equals(pstrProgramCurrency)).ExpirySchedule;
                string lstrTransactionId = string.Empty;
                lstrTransactionId = GenerateTransactionId();
                if (lstrTransactionId != string.Empty)
                {
                    //check points availability for sender
                    lintAvailablePoints = CheckAvailbility(pstrSenderExtrenalReference, Convert.ToInt32(RelationType.LBMS), pstrProgramCurrency);

                    //if points availability successfull
                    if (lintAvailablePoints > 0 && lintAvailablePoints >= pintTransferPoints)
                    {
                        //debit points from sender
                        if (DebitPointsFromSender(pstrSenderExtrenalReference, pstrReceiverSocialId, pobjSocialType, lobjProgramDefinition.ProgramId, pstrProgramCurrency, lstrTransactionId, pintTransferPoints))
                        {
                            string lstrReceiverExternalReference = GetActiveSocialConnection(pstrReceiverSocialId, pobjSocialType);

                            if (lstrReceiverExternalReference != string.Empty)
                            {
                                //credit points to receipient account
                                if (CreditPointsToRecipient(pstrSenderExtrenalReference, lstrReceiverExternalReference, lobjProgramDefinition.ProgramId, pstrProgramCurrency, lstrTransactionId, lintExpiryMonth, pintTransferPoints))
                                {
                                    lstrResult = "Success";
                                    string lstrMessage = string.Empty;
                                    if (pobjSocialType.Equals(SocialType.EMAIL) || pobjSocialType.Equals(SocialType.MOBILE))
                                    {
                                        lstrMessage = "CreditTransferPoints" + "~" + lobjProgramDefinition.ProgramId + "~" + pstrSenderExtrenalReference + "~" + pstrReceiverSocialName + "~" + pintTransferPoints + "~" + pstrProgramCurrency + "~" + pobjSenderMemberDetails.LastName;
                                    }
                                    else
                                    {
                                        lstrMessage = ConfigurationManager.AppSettings["Transfered"];
                                        lstrMessage = string.Format(lstrMessage, pstrReceiverSocialName, pintTransferPoints, pstrProgramCurrency, pobjSenderMemberDetails.LastName);
                                    }
                                    bool IsSend = SendSocialMessageToReceiver(pstrSenderExtrenalReference, pobjSocialType.ToString(), pstrReceiverSocialId, lstrMessage, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                                }
                                else
                                {
                                    //Rollback Transaction in case of Credit failure
                                    RollBackTransaction(lstrTransactionId, pstrSenderExtrenalReference, Convert.ToInt32(RelationType.LBMS), pstrProgramCurrency);
                                    LoggingAdapter.WriteLog("Points Were Not Credited For Point Transfer Between " + pstrSenderExtrenalReference + " And " + pstrReceiverSocialId + "At " + DateTime.Now);
                                }
                            }
                            else
                            {
                                SocialTransferPool lobjSocialTransferPool = new SocialTransferPool();
                                lobjSocialTransferPool.IsActive = true;
                                lobjSocialTransferPool.Points = pintTransferPoints;
                                lobjSocialTransferPool.ProgramId = lobjProgramDefinition.ProgramId;
                                lobjSocialTransferPool.SenderUniqueReference = pstrSenderExtrenalReference;
                                lobjSocialTransferPool.SocialId = pstrReceiverSocialId;
                                lobjSocialTransferPool.SocialType = Convert.ToInt32(pobjSocialType);
                                lobjSocialTransferPool.TransactionId = lstrTransactionId;

                                bool IsTransfrered = InsertSocialTransferPool(lobjSocialTransferPool);

                                if (IsTransfrered)
                                {
                                    lstrResult = "Success";
                                    string lstrMessage = string.Empty;
                                    if (pobjSocialType.Equals(SocialType.EMAIL) || pobjSocialType.Equals(SocialType.MOBILE))
                                    {
                                        lstrMessage = "CreditTransferPoints" + "~" + lobjProgramDefinition.ProgramId + "~" + pstrSenderExtrenalReference + "~" + pstrReceiverSocialName + "~" + pintTransferPoints + "~" + pstrProgramCurrency + "~" + pobjSenderMemberDetails.LastName;
                                    }
                                    else
                                    {
                                        lstrMessage = ConfigurationManager.AppSettings["PendingToReceive"];
                                        lstrMessage = string.Format(lstrMessage, pstrReceiverSocialName, pintTransferPoints, pstrProgramCurrency, pobjSenderMemberDetails.LastName);
                                    }
                                    bool IsSend = SendSocialMessageToReceiver(pstrSenderExtrenalReference, pobjSocialType.ToString(), pstrReceiverSocialId, lstrMessage, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                                }
                                else
                                {
                                    RollBackTransaction(lstrTransactionId, pstrSenderExtrenalReference, Convert.ToInt32(RelationType.LBMS), pstrProgramCurrency);
                                    LoggingAdapter.WriteLog("Points Were Not Credited For Point Transfer Between " + pstrSenderExtrenalReference + " And " + pstrReceiverSocialId + "At " + DateTime.Now);
                                }
                            }
                        }
                        else
                        {
                            LoggingAdapter.WriteLog("Points Were Not Debited For Point Transfer Between " + pstrSenderExtrenalReference + " And " + pstrReceiverSocialId + "At " + DateTime.Now);
                        }
                    }
                    else
                    {
                        lstrResult = "InSufficientPoints";
                        LoggingAdapter.WriteLog("Insufficient Points For Point Transfer Between " + pstrSenderExtrenalReference + " And " + pstrReceiverSocialId + "At " + DateTime.Now + " Available Points " + lintAvailablePoints);
                    }
                }
                else
                {
                    LoggingAdapter.WriteLog("Transaction Id Is Empty For Point Transfer Between " + pstrSenderExtrenalReference + " And " + pstrReceiverSocialId + "At " + DateTime.Now);
                }
            }
            catch (Exception Ex)
            {
                LoggingAdapter.WriteLog("Exception Occured For Point Transfer Between " + pstrSenderExtrenalReference + " And " + pstrReceiverSocialId + "At " + DateTime.Now);
                LoggingAdapter.WriteLog("Exception " + Ex.InnerException + Environment.NewLine + "Message " + Ex.Message + Environment.NewLine + "StackTrace " + Ex.StackTrace);
            }
            return lstrResult;
        }

        /// <summary>
        /// CheckAvailbility for sender
        /// </summary>
        /// <param name="pstrRelationReference"></param>
        /// <param name="pintRelationType"></param>
        /// <param name="pstrCurrency"></param>
        /// <returns></returns>
        private int CheckAvailbility(string pstrRelationReference, int pintRelationType, string pstrCurrency)
        {
            try
            {
                PGHelper lobjPGHelper = new PGHelper();
                return lobjPGHelper.CheckPointsAvailability(pstrRelationReference, pintRelationType, pstrCurrency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CheckAvailbility -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return 0;
            }
        }

        /// <summary>
        /// RollBack Transaction
        /// </summary>
        /// <param name="pstrRelationReference"></param>
        /// <param name="pintRelationType"></param>
        /// <param name="pstrCurrency"></param>
        /// <returns></returns>
        private bool RollBackTransaction(string pstrExternalReference, string pstrRelationReference, int pintRelationType, string pstrCurrency)
        {
            try
            {
                PGHelper lobjPGHelper = new PGHelper();
                return lobjPGHelper.PGRollBackTransaction(pstrExternalReference, pstrRelationReference, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PGRollBackTransaction -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Generate Transaction Id
        /// </summary>
        /// <returns></returns>
        private string GenerateTransactionId()
        {
            string lstrTransactionId = string.Empty;
            lstrTransactionId = GenerateUniqueNumber.Get16DigitNumberDateTime();
            return lstrTransactionId;
        }

        /// <summary>
        /// Debits Points from Sender
        /// </summary>
        /// <param name="pobjSenderMemberDetails"></param>
        /// <param name="pobjRecipientMemberDetails"></param>
        /// <param name="pstrProgramCurrency"></param>
        /// <param name="pstrTransactionId"></param>
        /// <returns></returns>
        private bool DebitPointsFromSender(string pstrSenderExternalReference, string pstrReceiverSocialId, SocialType pobjSocialType, int pintProgramId, string pstrProgramCurrency, string pstrTransactionId, int pintPoints)
        {
            bool lblnResult = false;
            try
            {
                TransactionDetails lobjTransactionDetails = new TransactionDetails();
                TransactionBusinessFacade lobjTransactionBusinessFacade = new TransactionBusinessFacade();
                lobjTransactionDetails.TransactionType = TransactionType.Debit;
                lobjTransactionDetails.LoyaltyTxnType = LoyaltyTxnType.Transfer;
                lobjTransactionDetails.Points = pintPoints;
                lobjTransactionDetails.RelationReference = pstrSenderExternalReference;
                lobjTransactionDetails.RelationType = RelationType.LBMS;
                lobjTransactionDetails.ProgramId = pintProgramId;
                lobjTransactionDetails.Narration = "Transfer Points(Debit)";
                lobjTransactionDetails.MerchantName = "Transfer Points";
                lobjTransactionDetails.ExternalReference = pstrTransactionId;
                lobjTransactionDetails.ProcessingDate = DateTime.Now;
                lobjTransactionDetails.TransactionDate = DateTime.Now;
                lobjTransactionDetails.ExpiryDate = DateTime.Now;
                lobjTransactionDetails.TransactionCurrency = pstrProgramCurrency;
                lobjTransactionDetails.AdditionalDetails1 = "Points Transfer Between " + pstrSenderExternalReference + " And " + pstrReceiverSocialId + " over " + pobjSocialType.ToString();
                lobjTransactionDetails.TransactionDetailBreakage.IsBillable = false;
                lobjTransactionDetails.TransactionDetailBreakage.Narration = "Transfer Points(Debit)";
                lblnResult = lobjTransactionBusinessFacade.InsertTransactionDetails(lobjTransactionDetails);
            }
            catch (Exception Ex)
            {
                lblnResult = false;
                LoggingAdapter.WriteLog("(DebitPointsFromSender)Points Were Not Debited For Point Transfer Between " + pstrSenderExternalReference + " And " + pstrReceiverSocialId + " over " + pobjSocialType.ToString() + "At " + DateTime.Now);
                LoggingAdapter.WriteLog("DebitPointsFromSender Exception " + Ex.InnerException + Environment.NewLine + "Message " + Ex.Message + Environment.NewLine + "StackTrace " + Ex.StackTrace);
            }

            return lblnResult;
        }

        /// <summary>
        /// Credit Points to Recipient
        /// </summary>
        /// <param name="pobjSenderMemberDetails"></param>
        /// <param name="pobjRecipientMemberDetails"></param>
        /// <param name="pstrProgramCurrency"></param>
        /// <param name="pstrTransactionId"></param>
        /// <param name="pintExpiryMonth"></param>
        /// <returns></returns>
        private bool CreditPointsToRecipient(string pstrSenderExternalReference, string pstrReceiverExternalReference, int pintProgramId, string pstrProgramCurrency, string pstrTransactionId, int pintExpiryMonth, int pintPoints)
        {
            bool lblnResult = false;

            try
            {
                TransactionDetails lobjTransactionDetails = new TransactionDetails();
                TransactionBusinessFacade lobjTransactionBusinessFacade = new TransactionBusinessFacade();
                lobjTransactionDetails.TransactionType = TransactionType.Credit;
                lobjTransactionDetails.LoyaltyTxnType = LoyaltyTxnType.Transfer;
                lobjTransactionDetails.Points = pintPoints;
                lobjTransactionDetails.RelationReference = pstrReceiverExternalReference;
                lobjTransactionDetails.RelationType = RelationType.LBMS;
                lobjTransactionDetails.ProgramId = pintProgramId;
                lobjTransactionDetails.Narration = "Transfer Points(Credit)";
                lobjTransactionDetails.MerchantName = "Transfer Points";
                lobjTransactionDetails.ExternalReference = pstrTransactionId;
                lobjTransactionDetails.ProcessingDate = DateTime.Now;
                lobjTransactionDetails.TransactionDate = DateTime.Now;
                lobjTransactionDetails.ExpiryDate = DateTime.Now.AddMonths(pintExpiryMonth);
                lobjTransactionDetails.TransactionCurrency = pstrProgramCurrency;
                lobjTransactionDetails.AdditionalDetails1 = "Points Transfer Between " + pstrSenderExternalReference + " And " + pstrReceiverExternalReference;
                lobjTransactionDetails.IsDomestic = true;
                lobjTransactionDetails.TransactionDetailBreakage.IsBillable = false;
                lobjTransactionDetails.TransactionDetailBreakage.Narration = "Transfer Points(Credit)";
                lblnResult = lobjTransactionBusinessFacade.InsertTransactionDetails(lobjTransactionDetails);
                if (lblnResult)
                {
                    UpdateTransactionDetails(lobjTransactionDetails);
                }
            }
            catch (Exception Ex)
            {
                lblnResult = false;
                LoggingAdapter.WriteLog("(CreditPointsToRecipient)Points Were Not Credited For Point Transfer Between " + pstrSenderExternalReference + " And " + pstrReceiverExternalReference + "At " + DateTime.Now);
                LoggingAdapter.WriteLog("CreditPointsToRecipient Exception " + Ex.InnerException + Environment.NewLine + "Message " + Ex.Message + Environment.NewLine + "StackTrace " + Ex.StackTrace);
            }

            return lblnResult;
        }

        /// <summary>
        /// Update TransactionDetails - CSData
        /// </summary>
        /// <param name="pobjTransactionDetails"></param>
        /// <returns></returns>
        private bool UpdateTransactionDetails(TransactionDetails pobjTransactionDetails)
        {
            bool status = false;
            //SecureData Contains(Secretkey|IsSuccessFul|JsonString); Format to be followed
            string lstSecureData = pobjTransactionDetails.Id + "|" + pobjTransactionDetails.Points;
            MD5 md5HashAlgo = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(lstSecureData);
            byte[] hashBytes = md5HashAlgo.ComputeHash(inputBytes);
            StringBuilder secureData = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                secureData.Append(hashBytes[i].ToString("X2"));
            }
            string lstSecureHash = Convert.ToString(secureData);
            TransactionBusinessFacade lobjTxnFacade = new TransactionBusinessFacade();
            pobjTransactionDetails.CSData = lstSecureHash;
            status = lobjTxnFacade.UpdateTransactionDetails(pobjTransactionDetails);
            return status;
        }

        public bool InsertSocialTransferPool(SocialTransferPool pobjSocialTransferPool)
        {
            return SPWrapper.InsertSocialTransferPool(pobjSocialTransferPool);
        }

        public bool UpdateStatusSocialTransferPool(SocialTransferPool pobjSocialTransferPool)
        {
            return SPWrapper.UpdateStatusSocialTransferPool(pobjSocialTransferPool);
        }

        public List<SocialTransferPool> GetSocialTransferPoolBySocialId(SocialTransferPool pobjSocialTransferPool)
        {
            return DataMapper.MapSocialTransferPool(SPWrapper.GetSocialTransferPoolBySocialId(pobjSocialTransferPool));
        }

        public string GetActiveSocialConnection(string pstrSocialID, SocialType pobjSocialType)
        {
            string lstrExternalReference = string.Empty;
            try
            {
                SocialFacade lobjSocialFacade = new SocialFacade();
                SocialConnections lobjSocialConnections = lobjSocialFacade.GetActiveSocialConnection(pstrSocialID, pobjSocialType.ToString());
                if (lobjSocialConnections != null)
                {
                    lstrExternalReference = lobjSocialConnections.ExternalReference;
                }

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetActiveSocialConnection Exception:-" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);

            }
            return lstrExternalReference;
        }

        /// <summary>
        /// Send Email SMS For Transfer To Sender
        /// </summary>
        /// <param name="pobjSenderMemberDetails"></param>
        /// <param name="pobjRecipientMemberDetails"></param>
        /// <param name="pintTransferPoints"></param>
        private void SendEmailSMSForTransferToSender(MemberDetails pobjSenderMemberDetails, string pstrReceiverName, int pintTransferPoints, string pstrProgramCurrency)
        {
            int lintSenderAvailablePoints = 0;
            LoggingAdapter.WriteLog("SendEmailSMSForTransferToSender CheckAvailbility started :" + pobjSenderMemberDetails.MemberRelationsList.Find(obj => obj.RelationType.Equals(RelationType.LBMS)).RelationReference + pstrProgramCurrency);
            lintSenderAvailablePoints = CheckAvailbility(pobjSenderMemberDetails.MemberRelationsList.Find(obj => obj.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), pstrProgramCurrency);

            LoggingAdapter.WriteLog("SendEmailSMSForTransferToSender CheckAvailbility :" + lintSenderAvailablePoints.ToString());

            try
            {
                if (pobjSenderMemberDetails.Email != string.Empty)
                {
                    if (lintSenderAvailablePoints > 0)
                    {
                        // Communication Engine Call FOr Email.
                        EmailDetails lobjEmailDetail = new EmailDetails();
                        List<string> lstEmailparameter = new List<string>();
                        List<string> lstAttachment = new List<string>();
                        CEHelper lobjcehelper = new CEHelper();
                        lstEmailparameter.Add(pobjSenderMemberDetails.LastName);
                        lstEmailparameter.Add(Convert.ToString(pintTransferPoints));
                        lstEmailparameter.Add(pstrProgramCurrency);
                        lstEmailparameter.Add(pstrReceiverName);
                        lstEmailparameter.Add(lintSenderAvailablePoints.ToString());
                        lobjEmailDetail.TemplateCode = "DebitTransferPoints";
                        lobjEmailDetail.ListParameter = lstEmailparameter;
                        lobjEmailDetail.MemberId = Convert.ToString(pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
                        lobjEmailDetail.ProgramId = Convert.ToString(pobjSenderMemberDetails.ProgramId);
                        lobjEmailDetail.AttachmentList = lstAttachment;
                        lobjEmailDetail.To = pobjSenderMemberDetails.Email;
                        lobjcehelper.InsertEmailDetails(lobjEmailDetail);

                    }
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Points Transfer Email To Sender: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace + Environment.NewLine + "Message: " + ex.Message);
            }

            try
            {
                if (pobjSenderMemberDetails.MobileNumber != string.Empty)
                {
                    if (lintSenderAvailablePoints > 0)
                    {
                        //Communication Engine Call For SMS. 
                        CEHelper lobjcehelper = new CEHelper();
                        SmsDetails lobjSmsDetail = new SmsDetails();
                        List<string> lstSmsparameter = new List<string>();
                        lobjSmsDetail.TemplateCode = "DebitTransferPoints";
                        lstSmsparameter.Add(pobjSenderMemberDetails.LastName);
                        lstSmsparameter.Add(Convert.ToString(pintTransferPoints));
                        lstSmsparameter.Add(pstrProgramCurrency);
                        lstSmsparameter.Add(pstrReceiverName);
                        lstSmsparameter.Add(lintSenderAvailablePoints.ToString());
                        lobjSmsDetail.ListParameter = lstSmsparameter;
                        lobjSmsDetail.ProgramId = Convert.ToString(pobjSenderMemberDetails.ProgramId);
                        lobjSmsDetail.MemberId = Convert.ToString(pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
                        lobjSmsDetail.ReceiverMobile = Convert.ToString(pobjSenderMemberDetails.MobileNumber);
                        lobjcehelper.InsertSmsDetails(lobjSmsDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Points Transfer SMS To Sender: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace + Environment.NewLine + "Message: " + ex.Message);
            }
        }

        public bool SendSocialMessageToReceiver(string pstrExternalReference, string pstrRecieverSocialType, string pstrRecieverSocialId, string pstrMessage, string pstrSubject, string pstrAdditionalDetails1, string pstrAdditionalDetails2, string pstrAdditionalDetails3, string pstrAdditionalDetails4)
        {
            try
            {
                SocialFacade lobjFacade = new SocialFacade();
                return lobjFacade.SendPrivateMessage(pstrExternalReference, pstrRecieverSocialType, pstrRecieverSocialId, pstrMessage, pstrSubject, pstrAdditionalDetails1, pstrAdditionalDetails2, pstrAdditionalDetails3, pstrAdditionalDetails4);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("SendPrivateMessage Exception:-" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Points Transfer Email Notification
        /// </summary>
        /// <param name="pobjSenderMemberDetails"></param>
        /// <param name="pobjRecipientMemberDetails"></param>
        /// <param name="pintTransferPoints"></param>
        private void SendPointsTransferEmail(MemberDetails pobjSenderMemberDetails, string pstrReceiverSocialId, string pstrReceiverSocialName, int pintTransferPoints, int pintProgramId)
        {
            try
            {
                string mstrEmail = "<p> Dear Admin,</p><p style=margin-left: 40px> Please Find Following Points Transfer Description $$Template_Details$$ </p>";
                string FileList = @"<Table border=1 ><Tr><Th>From Account</Th><Th>To Account</Th><Th>Points</Th><Th>Date</Th></Tr>";
                FileList += @"<Tr><Td>" + pobjSenderMemberDetails.LastName + "(" + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + ")" + "</Td><Td>" + pstrReceiverSocialName + "(" + pstrReceiverSocialId + ")" + "</Td><Td><center>" + pintTransferPoints + "</center></Td><Td><center>" + System.DateTime.Today.ToString("dd/MM/yyyy") + "</center></Td></Tr>";
                List<string> pstrEmailparameter = new List<string>();
                FileList += "</Table>";
                mstrEmail = mstrEmail.Replace("$$Template_Details$$", FileList);
                pstrEmailparameter.Add(mstrEmail);
                EmailDetails lobjEmailDetail = new EmailDetails();
                List<string> lstAttachment = new List<string>();
                CEHelper lobjcehelper = new CEHelper();
                lobjEmailDetail.TemplateCode = "PointsTransfer";
                lobjEmailDetail.ListParameter = pstrEmailparameter;
                lobjEmailDetail.ProgramId = Convert.ToString(pintProgramId);
                lobjEmailDetail.AttachmentList = lstAttachment;
                lobjcehelper.InsertEmailDetails(lobjEmailDetail);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Points Transfer Email Exception: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace + Environment.NewLine + "Message: " + ex.Message);
            }
        }
    }
}
