﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BillPay.UtilityPayment.DataAccess
{
    public class DBFields
    {
        public static string ReferenceNumber = "Reference_Number";
        public static string Account = "Account";
        public static string CompanyCode = "Company_Code";
        public static string AccountType = "Account_Type";
        public static string CustomerId = "Customer_Id";
        public static string PaymentMode = "Payment_Mode";
        public static string CardNoFlag = "Card_No_Flag";
        public static string BankAccount = "Bank_Account";
        public static string Amount = "Amount";
        public static string Status = "Status";
        public static string Type = "Type";
        public static string ReturnCode = "Return_Code";
        public static string AdditionalDetails1 = "Additional_Details1";
        public static string AdditionalDetails2 = "Additional_Details2";
        public static string AdditionalDetails3 = "Additional_Details3";
        public static string AdditionalDetails4 = "Additional_Details4";
        public static string AdditionalDetails5 = "Additional_Details5";
        public static string TimeStamp = "Timestamp";
        public static string RequestedDate = "Requested_Date";
        public static string ResponseDate = "Response_Date";
        public static string CreatedOn = "Created_On";
        public static string CreatedBy = "Created_By";
        public static string UpdatedBy = "Updated_By";
        public static string UpdatedOn = "Updated_On";
        public static string ExternalReference = "External_Reference";
        public static string RequestXML = "Request_XML";
        public static string ResponseXML = "Response_XML";
    }

    public class StoredProcedures
    {
        public static string InsertUtilityDetails = "sp_InsertUtilityDetails";
        public static string UpdateUtilityDetailsStatus = "sp_UpdateUtilityDetailsStatus";
        public static string UpdateUtilityDetails = "sp_UpdateUtilityDetails";

    }
}
