﻿using BillPay.UtilityPayment.Entities;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.DataAccess.BusinessFacade;
using Framework.EnterpriseLibrary.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillPay.UtilityPayment.DataAccess
{
    public class SPWrapper
    {
        public static int InsertUtilityDetails(UtilityDetails pobjUtilityDetails)
        {
            int lintId = 0;
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, Provider.SQL);

            try
            {
                IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.InsertUtilityDetails, CommandType.StoredProcedure);
                ocmd[DBFields.ReferenceNumber] = pobjUtilityDetails.ReferenceNumber;
                ocmd[DBFields.CompanyCode] = pobjUtilityDetails.UtilityCompanyCode;
                ocmd[DBFields.Account] = pobjUtilityDetails.UtilityAccount;
                ocmd[DBFields.AccountType] = pobjUtilityDetails.UtilityAccountType;
                ocmd[DBFields.CustomerId] = pobjUtilityDetails.CustomerId;
                ocmd[DBFields.PaymentMode] = pobjUtilityDetails.PaymentMode;
                ocmd[DBFields.CardNoFlag] = pobjUtilityDetails.CardNoFlag;
                ocmd[DBFields.BankAccount] = pobjUtilityDetails.BankAccount;
                ocmd[DBFields.Amount] = Convert.ToDouble(pobjUtilityDetails.Amount);
                ocmd[DBFields.Status] = Convert.ToInt32(pobjUtilityDetails.Status);
                ocmd[DBFields.Type] = Convert.ToInt32(pobjUtilityDetails.Type);
                ocmd[DBFields.ReturnCode] = pobjUtilityDetails.ReturnCode;
                ocmd[DBFields.AdditionalDetails1] = pobjUtilityDetails.AdditionalDetails1;
                ocmd[DBFields.AdditionalDetails2] = pobjUtilityDetails.AdditionalDetails2;
                ocmd[DBFields.AdditionalDetails3] = pobjUtilityDetails.AdditionalDetails3;
                ocmd[DBFields.AdditionalDetails4] = pobjUtilityDetails.AdditionalDetails4;
                ocmd[DBFields.AdditionalDetails5] = pobjUtilityDetails.AdditionalDetails5;
                ocmd[DBFields.CreatedBy] = pobjUtilityDetails.CreatedBy;
                ocmd[DBFields.RequestedDate] = pobjUtilityDetails.RequestedDate;
                ocmd[DBFields.TimeStamp] = pobjUtilityDetails.TimeStamp;
                ocmd[DBFields.RequestXML] = pobjUtilityDetails.RequestString;


                lintId = Convert.ToInt32(ocmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("InsertUtilityDetails" + ex.Message + Environment.NewLine);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }

            return lintId;

        }

        public static bool UpdateUtilityDetails(UtilityDetails pobjUtilityDetails)
        {
            bool lblnStatus = false;
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, Provider.SQL);

            try
            {
                IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.UpdateUtilityDetails, CommandType.StoredProcedure);

                ocmd[DBFields.ReferenceNumber] = pobjUtilityDetails.ReferenceNumber;
                ocmd[DBFields.ResponseDate] = pobjUtilityDetails.ResponseDate;
                ocmd[DBFields.ExternalReference] = pobjUtilityDetails.ExternalReference;
                ocmd[DBFields.UpdatedBy] = pobjUtilityDetails.UpdatedBy;

                if (ocmd.ExecuteNonQuery() > 0)
                {
                    lblnStatus = true;
                }
                else
                {
                    lblnStatus = false;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("UpdateUtilityDetails" + ex.Message + Environment.NewLine);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }

            return lblnStatus;
        }

        public static bool UpdateUtilityDetailsStatus(UtilityDetails pobjUtilityDetails)
        {
            bool lblnStatus = false;
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, Provider.SQL);

            try
            {
                IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.UpdateUtilityDetailsStatus, CommandType.StoredProcedure);

                ocmd[DBFields.ReferenceNumber] = pobjUtilityDetails.ReferenceNumber;
                ocmd[DBFields.Status] = pobjUtilityDetails.Status;
                ocmd[DBFields.Type] = pobjUtilityDetails.Type;
                ocmd[DBFields.ReturnCode] = pobjUtilityDetails.ReturnCode;
                ocmd[DBFields.ResponseDate] = pobjUtilityDetails.ResponseDate;
                ocmd[DBFields.ResponseXML] = pobjUtilityDetails.ResponseString;

                if (ocmd.ExecuteNonQuery() > 0)
                {
                    lblnStatus = true;
                }
                else
                {
                    lblnStatus = false;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("UpdateUtilityDetailsStatus" + ex.Message + Environment.NewLine);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }

            return lblnStatus;
        }


    }
}
