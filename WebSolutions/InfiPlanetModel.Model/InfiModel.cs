﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Core.Platform.TransactionManagement.BusinessFacade;
using Core.Platform.Transactions.Entites;
using Core.Platform.Member.Entites;
//using Core.Platform.Common.Entities;
using Framework.EnterpriseLibrary.RuleEngine.ClientHelper;
using Framework.EnterpriseLibrary.Security;
using Framework.EnterpriseLibrary.Security.Constants;
using Core.Platform.MemberActivity.Entities;
using Core.Platform.OTP.Entities;
using Framework.EnterpriseLibrary.ExceptionMessages.Entities;
using Framework.EnterpriseLibrary.ExceptionMessages.Facade;
using Core.Platform.MasterManagement.BusinessFacade;
using Framework.EnterpriseLibrary.PasswordReset.Entities;
using Core.Platform.MemberActivity.BusinessFacade;
using CB.IBE.Platform.AirClientModel;
using System.Xml.Serialization;
using Framework.Integration.Flight.Entites.CT;
using CB.IBE.Platform.Entities;
using CB.IBE.Platform.CTFlightMapper;
using CB.IBE.Platform.ClientEntities;
using System.IO;
using Core.Platform.MemberManagement.BusinessFacade;
using Core.Platform.Activation.BusinessFacade;
using Framework.EnterpriseLibrary.PasswordReset.Facade;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using Framework.Integrations.Hotels.Entities;
//using Core.Framework.Booking.Facade;
using Framework.EnterpriseLibrary.EmailComponent;
using Framework.EnterpriseLibrary.EmailComponent.Entities;
using System.Net.Mail;
using Framework.EnterpriseLibrary.Adapters;
using CB.IBE.Platform.Hotels.ClientEntities;
using CB.IBE.Platform.Masters.Entities;
using CB.IBE.Platform.HotelClientModel;
using Core.Platform.OTP.ConfigurationConstants;
using CB.IBE.Platform.Car.ClientEntities;
//using  Core.Framework.Booking.Facade;
//using Framework.CarBooking.Facade;
using CB.IBE.Platform.CarClientModel;
using CB.IBE.Platform.Car.Entities;
using Core.Framework.Booking.Facade;
//using CB.IBE.Platform.CommonConstants;
//using InfiBill.Platform.Services.DataContracts;
//using Framework.CarBooking.Facade;
//using CarBookingIntegrationFacade;
using Core.Platform.Helper.ProgramName;
using Core.Platform.PointGateway.Service.Helper;
using CB.IBE.Platform.IBEClient;
using CB.IBE.SearchHelper.IBEImplementor;
using Core.Platform.Merchant.Entities;
using Core.Platform.MerchantManagement.BusinessFacade;
using Core.Platform.PointGateway.BusinessFacade;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.ProgramInterface.ClientHelper;
using Core.Platform.PointGateway.Service.Helper.PGService;
using System.Globalization;
using Core.Platform.Booking.Entities;
using Core.Platform.RedemptionAuditTrail.Entities;
using CB.IBE.Platform.IBECarClient;
using Core.Platform.ExpirySchedule.Entities;
using CB.IBE.Platform.Transactions.Entites;
using InfiVoucher.Platform.Entities;
using Core.Platform.InfiVoucher.Entities;
using InfiPlanet.BusinessFacade;

using InfiPlanet.Merchant.VoucherClaim.ClientHelper;
using InfiPlanet.Voucher.Claim.EntitiesNew;
using InfiPlanet.Entities;
using Framework.EnterpriseLibrary.PasswordGenerator;
using BillPay.UtilityPayment.Entities;
using BillPay.UtilityPayment.FileReader;
using BillPay.UtilityPayment.ClientHelper;
using Core.Platform.Authentication.BuisnessFacade;
using Core.Platform.Authentication.Entities;
using InfiKashBack.Platform.ClientHelper;
using InfiKashBack.Platform.Entities;
using Viator.Platform.ClientEntities;
using Viator.Platform.ServiceClient;
using Framework.EnterpriseLibrary.CommunicationEngine.Entity;
using Framework.EnterpriseLibrary.CommunicationEngine.Helper;
using Viator.Platform.MasterEntities;
using Core.Platform.OTP.Facade;
using Core.Platform.SocialConnect.Entities;
using Core.Platform.SocialConnect.BusinessFacade;
using Core.Platform.SocialTransfer.BussinessFacade;
using Core.Platform.PointTransferFacade;
using IBE.Platform.Lounge.Entities;
using IBE.Platform.Lounge.ServiceClient;
using InfiPlanet.SessionConstants;
using Core.Platform.Configurations;
using Core.Platform.Loan.BusinessFacade;
using Core.Platform.Loan.Entities;
using Core.Platform.ExpirySchedule.BusinessFacade;


namespace InfiPlanetModel.Model
{
    public class InfiModel
    {
        string lstrMerchantId = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["MerchantId"]);
        string lstrMerchantUserName = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["MerchantUserName"]);
        string lstrMerchantPassword = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["MerchantPassword"]);

        public DateTime StringToDateTime(string pstrDate)
        {
            DateTime dt = DateTime.ParseExact(pstrDate, "dd/MM/yyyy", System.Globalization.CultureInfo.GetCultureInfo("ar-AE"));
            return dt;
        }
        //public DateTime StringToDateTime(string pstrDate)
        //{
        //    DateTime dt = DateTime.ParseExact(pstrDate, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
        //    return dt;
        //}
        public List<AirField> GetAllAirfields()
        {
            try
            {
                IBEModel lobjIBEModel = new IBEModel();
                return lobjIBEModel.GetAllAirField();
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllAirfields -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }

        }
        public string EncryptPassword(string pstrPassword)
        {
            return RSAEncryptor.EncryptString(pstrPassword, RSASecurityConstant.KeySize, RSASecurityConstant.RSAPublicKey);
        }
        public List<ProgramCurrencyDefinition> GetProductProgramCurrencyDefinition(MemberDetails pobjMemberDetails)
        {
            List<MemberSubRelation> lobjMemberSubRelationlst = pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).MemberSubRelationList;
            List<ProgramCurrencyDefinition> lobjreturnProgramCurrencyDefinitionlst = new List<ProgramCurrencyDefinition>();
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = GetProgramDetailsByID(pobjMemberDetails.ProgramId);
            if (lobjProgramDefinition != null)
            {
                List<ProgramProductCode> lobjListProgramProductCode = null;
                List<ProgramCurrencyDefinition> lobjListProgramCurrencyDefinition = null;
                lobjListProgramProductCode = GetProgramProductCodeList(lobjProgramDefinition.ProgramId);
                lobjListProgramCurrencyDefinition = GetAllCurrencyDefinition(lobjProgramDefinition.ProgramId);
                string lstrCurrency = GetDefaultCurrency();
                if (lobjListProgramProductCode != null && lobjListProgramCurrencyDefinition != null)
                {
                    List<ProgramProductCode> lobjListfindProgramProductCode = new List<ProgramProductCode>();
                    ProgramProductCode lobjProgramProductCode = null;
                    for (int i = 0; i < lobjMemberSubRelationlst.Count; i++)
                    {
                        lobjProgramProductCode = new ProgramProductCode();
                        lobjProgramProductCode = lobjListProgramProductCode.Find(lobj => lobj.ProductCode.Equals(lobjMemberSubRelationlst[i].ProductCode));

                        if (lobjProgramProductCode != null)
                        {
                            if (lobjListfindProgramProductCode.FindAll(lobj => lobj.Currency.Equals(lobjProgramProductCode.Currency)).Count == 0)
                            {
                                lobjListfindProgramProductCode.Add(lobjProgramProductCode);
                            }
                        }
                    }

                    if (lobjListfindProgramProductCode != null && lobjListfindProgramProductCode.Count > 0)
                    {
                        lobjListfindProgramProductCode = lobjListfindProgramProductCode.OrderByDescending(x => x.Currency.Equals(lstrCurrency)).ToList();
                    }

                    for (int j = 0; j < lobjListfindProgramProductCode.Count; j++)
                    {
                        lobjreturnProgramCurrencyDefinitionlst.Add(lobjListProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals(lobjListfindProgramProductCode[j].Currency)));
                    }
                }

                if (lobjreturnProgramCurrencyDefinitionlst.Count == 0 && lobjListProgramCurrencyDefinition != null)
                {
                    lobjreturnProgramCurrencyDefinitionlst.Add(lobjListProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals(lstrCurrency)));
                }
            }
            return lobjreturnProgramCurrencyDefinitionlst;
        }
        public List<ProgramProductCode> GetProgramProductCodeList(int pintProgramId)
        {
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            List<Core.Platform.ProgramMaster.Entities.ProgramProductCode> lobjListProgramProductCode = null;
            try
            {
                if (HttpContext.Current.Session["ProgramProductCode"] != null)
                {
                    lobjListProgramProductCode = HttpContext.Current.Session["ProgramProductCode"] as List<Core.Platform.ProgramMaster.Entities.ProgramProductCode>;
                }
                else
                {
                    lobjListProgramProductCode = lobjInterfaceHelper.GetProgramProductCodeList(pintProgramId);
                    HttpContext.Current.Session["ProgramProductCode"] = lobjListProgramProductCode;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetProgramProductCodeList :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
            }

            return lobjListProgramProductCode;
        }
        public string GetDefaultCurrency()
        {
            string lstrCurrency = string.Empty;
            ProgramDefinition lobjProgramDefinition = GetProgramMaster();

            if (HttpContext.Current.Session["DefaultCurrency"] != null)
            {
                lstrCurrency = HttpContext.Current.Session["DefaultCurrency"] as string;
            }
            else
            {
                List<ProgramCurrencyDefinition> lobjListOfProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
                lobjListOfProgramCurrencyDefinition = GetProgramCurrencyDefinition(lobjProgramDefinition.ProgramId);
                //lstrCurrency = lobjListOfProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals("REWARDZ")).Currency;
                lstrCurrency = lobjListOfProgramCurrencyDefinition.Find(lobj => lobj.IsDefault.Equals(true)).Currency;
                HttpContext.Current.Session["DefaultCurrency"] = lstrCurrency;
            }
            return lstrCurrency;
        }
        public List<ProgramCurrencyDefinition> GetProgramCurrencyDefinition(int pintProgramId)
        {
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            List<ProgramCurrencyDefinition> lobjListOfProgramCurrencyDefinition = null;
            try
            {
                if (HttpContext.Current.Session["ProgramCurrency"] != null)
                {
                    lobjListOfProgramCurrencyDefinition = HttpContext.Current.Session["ProgramCurrency"] as List<ProgramCurrencyDefinition>;
                }
                else if (HttpContext.Current.Application["ProgramCurrency"] != null)
                {
                    lobjListOfProgramCurrencyDefinition = HttpContext.Current.Application["ProgramCurrency"] as List<ProgramCurrencyDefinition>;
                }
                else
                {
                    lobjListOfProgramCurrencyDefinition = lobjInterfaceHelper.GetProgramCurrencyDefinition(pintProgramId);
                    HttpContext.Current.Application["ProgramCurrency"] = lobjListOfProgramCurrencyDefinition;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetProgramCurrencyDefinition :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
            }

            return lobjListOfProgramCurrencyDefinition;
        }
        public MemberDetails CheckMembershipCrediantials(string pstrLogin, string pstrPassword)
        {
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = GetProgramMaster();
            if (lobjProgramDefinition != null)
            {
                LoginDetails lobjLoginDetails = new LoginDetails();
                lobjLoginDetails.UserName = pstrLogin;
                lobjLoginDetails.Password = pstrPassword;
                lobjLoginDetails.ProgramId = lobjProgramDefinition.ProgramId;
                lobjLoginDetails.RelationType = Convert.ToInt32(RelationType.LBMS);
                //MemberBusinessFacade lobjfacade = new MemberBusinessFacade();
                //return lobjfacade.CheckMembershipCrediantials(lobjLoginDetails);
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<MemberDetails>(new ExecuteRequest("CheckMembershipCrediantials", "CheckMembershipCrediantials", new List<object>(0) { lobjLoginDetails, null }));
            }
            else
            {
                LoggingAdapter.WriteLog("MODEL login CheckMembershipCrediantials program Detail Invalid");
                return null;
            }
        }
        public bool UpdateMemberShipActivitySession(MemberActivitySession pobjMemberActivitySession)
        {
            ActivityFacade lobjMemberActivityFacade = new ActivityFacade();
            return lobjMemberActivityFacade.UpdateMemberShipActivitySession(pobjMemberActivitySession);
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("UpdateMemberShipActivitySession", "v3UpdateMemberShipActivitySession", new List<object>(0) { pobjMemberActivitySession }));

        }

        public int LogActivity(string pstrActivity, ActivityType pobjActivityType)
        {
            int lintActivityID = 0;
            if (HttpContext.Current.Session["MemberActivitySession"] != null)
            {

                string pstrPageUrl = HttpContext.Current.Request.Url.OriginalString;//.Segments[HttpContext.Current.Request.Url.Segments.Count() - 1];
                ActivityFacade lobjMemberActivityFacade = new ActivityFacade();
                MemberActivitySession lobjMemberActivitySession = HttpContext.Current.Session["MemberActivitySession"] as MemberActivitySession;
                MemberActivityMaster lobjMemberActivity = new MemberActivityMaster();
                lobjMemberActivity.Activity = pstrActivity;
                lobjMemberActivity.PageURL = pstrPageUrl;
                lobjMemberActivity.ActivityType = pobjActivityType;
                List<MemberActivityMaster> lobjMemberActivityList = new List<MemberActivityMaster>();
                lobjMemberActivityList.Add(lobjMemberActivity);
                lintActivityID = lobjMemberActivityFacade.InsertMemberActivity(lobjMemberActivityList, lobjMemberActivitySession.Id, lobjMemberActivitySession.ReferenceNumber);
                //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                //lintActivityID = lobjHelper.ExecuteRuleSet<int>(new ExecuteRequest("InsertMemberActivity", "v3InsertMemberActivity", new List<object>(0) { lobjMemberActivityList, lobjMemberActivitySession.Id, lobjMemberActivitySession.ReferenceNumber }));
                //lintActivityID = lobjMemberActivityFacade.InsertMemberActivity(lobjMemberActivityList, lobjMemberActivitySession.Id, lobjMemberActivitySession.ReferenceNumber);
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                //lintActivityID = lobjHelper.ExecuteRuleSet<int>(new ExecuteRequest("InsertMemberActivity", "v3InsertMemberActivity", new List<object>(0) { lobjMemberActivityList, lobjMemberActivitySession.Id, lobjMemberActivitySession.ReferenceNumber }));

            }
            return lintActivityID;
        }

        public bool UpdateUserLoginAttempts(string pstrMemberid)
        {
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = GetProgramMaster();

            if (lobjProgramDefinition != null)
            {
                LoginDetails lobjLoginDetails = new LoginDetails();
                lobjLoginDetails.UserName = pstrMemberid;
                lobjLoginDetails.ProgramId = lobjProgramDefinition.ProgramId;
                lobjLoginDetails.RelationType = Convert.ToInt32(RelationType.LBMS);
                //MemberBusinessFacade lobjFacade = new MemberBusinessFacade();
                //return lobjFacade.UpdateUserLoginAttempts(lobjLoginDetails);
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("UpdateUserLoginAttempts", "UpdateUserLoginAttempts", new List<object>(0) { lobjLoginDetails }));
            }
            else
            {
                LoggingAdapter.WriteLog("MODEL UpdateUserLoginAttempts program Detail Invalid");
                return false;
            }
        }

        public MemberDetails GetMemberDetails(string pstrMemberId)
        {
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = GetProgramMaster();
            if (lobjProgramDefinition != null)
            {
                ActivationParameters lobjParameters = new ActivationParameters();
                lobjParameters.RelationReference = pstrMemberId;
                lobjParameters.ProgramId = lobjProgramDefinition.ProgramId;
                lobjParameters.RelationType = Convert.ToInt32(RelationType.LBMS);

                ActivationFacade lobjFacade = new ActivationFacade();
                return lobjFacade.GetMemberDetails(lobjParameters);

                //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                //return lobjHelper.ExecuteRuleSet<MemberDetails>(new ExecuteRequest("MemberDetails", "MemberDetails", new List<object>(0) { lobjParameters }));
            }
            else
            {
                LoggingAdapter.WriteLog("MODEL GetMemberDetails (Activation Facade) program Detail Invalid");
                return null;
            }
        }


        public bool GenerateOTP(string pstrMemberid, string pstrSourceIpAddress, int pstrProgramID)
        {
            SystemParameter lobjSystemParameter = null;
            lobjSystemParameter = GetSystemParametres(pstrProgramID);
            try
            {
                if (lobjSystemParameter != null)
                {
                    OTPDetails lobjOTPDetails = new OTPDetails();
                    lobjOTPDetails.UniquerefID = pstrMemberid;
                    lobjOTPDetails.SourceAddress = pstrSourceIpAddress;
                    lobjOTPDetails.SourceCode = Core.Platform.OTP.ConfigurationConstants.SourceCode.Web;
                    lobjOTPDetails.ProgramId = pstrProgramID;
                    lobjOTPDetails.RelationType = Convert.ToInt32(RelationType.Login);
                    lobjOTPDetails.OtpType = Convert.ToString(OTPEnumTypes.UNBLOCKMEMBER);
                    lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.UNBLOCKMEMBER;
                    lobjOTPDetails.AddExpirationTimeInMinutes = Convert.ToString(lobjSystemParameter.OTPExpirationTime);
                    //ActivationFacade lobjFacade = new ActivationFacade();
                    //return lobjFacade.GenerateOTPDetails(lobjOTPDetails);
                    RuleEngineHelper lobjHelper = new RuleEngineHelper();
                    return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("GenerateOTPDetails", "GenerateOTPDetails", new List<object>(0) { lobjOTPDetails }));

                }
                else
                {
                    LoggingAdapter.WriteLog("MODEL GenerateOTP Detail Invalid");
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("MODEL GenerateOTP " + ex.Message + "StackTrace" + ex.StackTrace);
                return false;
            }
        }

        public bool UnlcokMemberByOTP(string pstrID, string pstrOTP)
        {
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = GetProgramMaster();
            if (lobjProgramDefinition != null)
            {
                OTPDetails lobjDetails = new OTPDetails();
                lobjDetails.UniquerefID = pstrID;
                lobjDetails.OTP = Convert.ToInt32(pstrOTP);
                lobjDetails.RelationType = Convert.ToInt32(RelationType.Login);
                lobjDetails.OtpType = Convert.ToString(OTPEnumTypes.UNBLOCKMEMBER);
                lobjDetails.OtpEnumTypes = OTPEnumTypes.UNBLOCKMEMBER;

                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("UnlcokMemberByOTP", "UnlcokMemberByOTP", new List<object>(0) { lobjDetails }));
            }
            else
            {
                LoggingAdapter.WriteLog("MODEL UnlcokMemberByOTP (Activation Facade) program Detail Invalid");
                return false;
            }
        }

        public ProgramDefinition GetGlobalProgramDetails(string pstrProgramName)
        {
            ProgramDefinition lobjProgramDefination = null;
            try
            {
                if (HttpContext.Current.Application["ProgramMaster"] != null)
                {
                    lobjProgramDefination = HttpContext.Current.Application["ProgramMaster"] as ProgramDefinition;
                }
                else
                {
                    lobjProgramDefination = new ProgramDefinition();
                    InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
                    lobjProgramDefination = lobjInterfaceHelper.GetProgramDefinition(pstrProgramName);
                    HttpContext.Current.Application["ProgramMaster"] = lobjProgramDefination;
                }
                return lobjProgramDefination;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetProgramDetails :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
                return null;
            }
        }

        public ProgramDefinition GetProgramDetailsByID(int pintProgramId)
        {
            ProgramDefinition lobjProgramDefination = null;
            try
            {
                if (HttpContext.Current.Session["ProgramMaster"] != null)
                {
                    lobjProgramDefination = HttpContext.Current.Session["ProgramMaster"] as ProgramDefinition;
                }
                else
                {
                    lobjProgramDefination = new ProgramDefinition();
                    InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
                    lobjProgramDefination = lobjInterfaceHelper.GetProgramDefinitionById(pintProgramId);
                }

                return lobjProgramDefination;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetProgramDetailsByID :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
                return null;
            }

        }

        public ProgramDefinition GetProgramMaster()
        {
            string lstrProgramName = string.Empty;
            lstrProgramName = ProgramHelper.ProgramName();
            ProgramDefinition lobjProgramMaster = null;
            lobjProgramMaster = GetProgramDetails(lstrProgramName);
            return lobjProgramMaster;
        }

        public ProgramDefinition GetProgramDetails(string pstrProgramName)
        {
            ProgramDefinition lobjProgramDefination = null;
            try
            {
                if (HttpContext.Current.Session["ProgramMaster"] != null)
                {
                    lobjProgramDefination = HttpContext.Current.Session["ProgramMaster"] as ProgramDefinition;
                }
                else if (HttpContext.Current.Application["ProgramMaster"] != null)
                {
                    lobjProgramDefination = HttpContext.Current.Application["ProgramMaster"] as ProgramDefinition;
                }
                else
                {
                    lobjProgramDefination = new ProgramDefinition();
                    InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
                    lobjProgramDefination = lobjInterfaceHelper.GetProgramDefinition(pstrProgramName);
                    HttpContext.Current.Application["ProgramMaster"] = lobjProgramDefination;
                }
                return lobjProgramDefination;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetProgramDetails :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
                return null;
            }

        }

        public SystemParameter GetSystemParametres(int pintProgramId)
        {
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            SystemParameter lobjSystemParameter = null;
            try
            {
                //if (HttpContext.Current.Application["SystemParameters"] != null)
                //{
                //    lobjSystemParameter = HttpContext.Current.Application["SystemParameters"] as SystemParameter;
                //}
                //else
                //{
                lobjSystemParameter = lobjInterfaceHelper.GetSystemParameter(pintProgramId);
                //HttpContext.Current.Application["SystemParameters"] = lobjSystemParameter;
                //}
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetSystemParametres :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
            }

            return lobjSystemParameter;

        }
        //public List<ExceptionMessage> GetAllExceptionMessages(int pintProgramId)
        //{
        //    try
        //    {
        //        return ExceptionMessagesFacade.GetAllExceptionMessages(pintProgramId);
        //        //RuleEngineHelper lobjHelper = new RuleEngineHelper();
        //        //return lobjHelper.ExecuteRuleSet<List<ExceptionMessage>>(new ExecuteRequest("GetAllExceptionMessage", "GetAllExceptionMessage", new List<object>(0) { pintProgramId }));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("GetAllExceptionMessages -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
        //        return null;
        //    }
        //}

        public int InsertMemberActivityWithSessionID(MemberActivitySession lobjMemberActivitySession)
        {
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<int>(new ExecuteRequest("InsertMemberActivityWithSessionID", "v3InsertMemberActivityWithSessionID", new List<object>(0) { lobjMemberActivitySession }));
            ActivityFacade objActivityFacade = new ActivityFacade();
            return objActivityFacade.InsertMemberActivityWithSessionID(lobjMemberActivitySession);
        }

        public string MemberResetPassword(ResetPassword pobjResetPassword)
        {
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<string>(new ExecuteRequest("MemberResetPassword", "MemberResetPassword", new List<object>(0) { pobjResetPassword }));

        }
        public ResetPassword GetDetailsFromHashKey(ResetPassword pobjResetPassword)
        {
            ResetPasswordFacade lobjResetPasswordFacade = new ResetPasswordFacade();
            return lobjResetPasswordFacade.GetDetailsFromHashKey(pobjResetPassword);
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<ResetPassword>(new ExecuteRequest("LBMSv3GetDetailsFromHashKey", "GetDetailsFromHashKey", new List<object>(0) { pobjResetPassword }));
        }

        public bool ValidateResetToken(ResetPassword pobjResetPassword)
        {
            // ResetPasswordFacade lobjResetPasswordFacade = new ResetPasswordFacade();
            //return lobjResetPasswordFacade.ValidateResetToken(pobjResetPassword);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("LBMSv3ValidateResetToken", "ValidateResetToken", new List<object>(0) { pobjResetPassword }));
        }

        public bool UpdatePassword(ResetPassword pobjResetPassword, MemberRelation pobjMemberRelation)
        {
            //MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            //return lobjMemberBusinessFacade.UpdatePassword(pobjResetPassword, pobjMemberRelation);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("LBMSv3UpdatePassword", "UpdatePassword", new List<object>(0) { pobjResetPassword, pobjMemberRelation }));
        }

        public List<TransactionDetails> GetMemberTransactionSummary(SearchTransactions pobjSearchTransactions)
        {
            //TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            //return objTransactionBusinessFacade.GetMemberTransactionSummary(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<List<TransactionDetails>>(new ExecuteRequest("GetMemberTransactionSummary", "v3GetMemberTransactionSummary", new List<object>(0) { pobjSearchTransactions }));

        }

        public List<TransactionDetails> GetMemberTransactionSummaryByDate(SearchTransactions pobjSearchTransactions)
        {
            //TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            //return objTransactionBusinessFacade.GetMemberTransactionSummaryByDate(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<List<TransactionDetails>>(new ExecuteRequest("GetMemberTransactionSummaryByDate", "v3GetMemberTransactionSummaryByDate", new List<object>(0) { pobjSearchTransactions }));
        }

        public List<string> PageDataRangeList(int TotalRowCount, int Perpagedata)
        {
            List<int> objPerpagedatainfo = new List<int>();
            objPerpagedatainfo.Add(TotalRowCount);
            objPerpagedatainfo.Add(Perpagedata);
            TransactionBusinessFacade lobjTransactionBusinessFacade = new TransactionBusinessFacade();
            return lobjTransactionBusinessFacade.PageDataRangeList(objPerpagedatainfo);
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<List<string>>(new ExecuteRequest("PageDataRangeList", "PageDataRangeList", new List<object>(0) { objPerpagedatainfo }));

        }
        public int GetTotalMemberTransactionByDate(SearchTransactions pobjSearchTransactions)
        {
            //TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            //return objTransactionBusinessFacade.GetTotalMemberTransactionByDate(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<int>(new ExecuteRequest("GetTotalMemberTransactionByDate", "GetTotalMemberTransactionByDate", new List<object>(0) { pobjSearchTransactions }));

        }

        public int GetTotalMemberTransaction(SearchTransactions pobjSearchTransactions)
        {
            TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            return objTransactionBusinessFacade.GetTotalMemberTransaction(pobjSearchTransactions);
           // RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<int>(new ExecuteRequest("GetTotalMemberTransaction", "GetTotalMemberTransaction", new List<object>(0) { pobjSearchTransactions }));

        }
        public List<ProgramCurrencyDefinition> GetAllCurrencyDefinition(int pintProgramId)
        {
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            try
            {
                return lobjInterfaceHelper.GetProgramCurrencyDefinition(pintProgramId);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllCurrencyDefinition :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
                return null;
            }
        }
        public List<TransactionDetails> GetMemberStatementTransaction(SearchTransactions pobjSearchTransactions)
        {
            TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            return objTransactionBusinessFacade.GetMemberStatementTransaction(pobjSearchTransactions);
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            // return lobjHelper.ExecuteRuleSet<List<TransactionDetails>>(new ExecuteRequest("GetMemberStatementTransaction", "GetMemberStatementTransaction", new List<object>(0) { pobjSearchTransactions }));
        }

        public List<Carrier> GetAllCarriers()
        {
            try
            {
                IBEModel lobjIBEModel = new IBEModel();
                return lobjIBEModel.GetAllCarriers();
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllCarriers -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
        }
        public int InsertSearchFlight(SearchRequest pobjSearchFlight)
        {
            IBEModel lobjIBEModel = new IBEModel();
            return lobjIBEModel.InsertSearchFlight(pobjSearchFlight);
        }
        public SearchResponse MapSearchResponse(SearchRequest pobjSearchRequest)
        {
            try
            {
                IBEModel lobjIBEModel = new IBEModel();
                return lobjIBEModel.MapSearchResponse(pobjSearchRequest);

                //SearchResponse lobjSearchResponse = new SearchResponse();
                //pobjSearchRequest.RefererDetails = HttpContext.Current.Application["RefererSupplierDetails"] as RefererDetails;
                //AirSearchHelper lobjAirSearchHelper = new AirSearchHelper();
                //lobjSearchResponse = lobjAirSearchHelper.AirSearchRequest(pobjSearchRequest);

                //return lobjSearchResponse;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("MapSearchResponse -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
        }

        public List<Countries> GetAllCountries()
        {
            IBEModel lobjIBEModel = new IBEModel();
            return lobjIBEModel.GetAllCountries();
        }
        public CreateItineraryResponse GetItineraryResponse(CreateItineraryRequest pobjCreateItineraryRequest)
        {
            IBEModel lobjIBEModel = new IBEModel();
            return lobjIBEModel.CreateItinerary(pobjCreateItineraryRequest);
        }

        public bool ActivateUserByMemberId(MemberDetails pobjMemberDetails)
        {
            //ActivationFacade lobjActivationFacade = new ActivationFacade();
            //return lobjActivationFacade.ActivteUserByMemberId(pobjMemberDetails);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("ActivteUserByMemberId", "ActivteUserByMemberId", new List<object>(0) { pobjMemberDetails }));
        }

        /////////////////////////////// Hotel Related Method Added by Baban /////////////////////////////////////////

        public RequestHotelSearch CreateHotelSearchRequest(string pstrCitySearchText, string pstrCheckInDate, string pstrCheckoutDate, string pstrRoomString)
        {
            RequestHotelSearch lobjSearchRequest = new RequestHotelSearch();
            lobjSearchRequest.CheckInDate = this.StringToDateTime(pstrCheckInDate);
            lobjSearchRequest.CheckOutDate = this.StringToDateTime(pstrCheckoutDate);
            string[] arrayRoomPersonType = pstrRoomString.TrimEnd(':').Split(':');
            lobjSearchRequest.NoOfRooms = arrayRoomPersonType[0].TrimEnd(',').Split(',').Count();
            lobjSearchRequest.AdultPerRoom = arrayRoomPersonType[0].TrimEnd(',');//adult
            lobjSearchRequest.ChildrenPerRoom = arrayRoomPersonType[1].TrimEnd(',');//child
            lobjSearchRequest.CityName = HttpUtility.UrlDecode(pstrCitySearchText.Split(',')[2].ToString());
            lobjSearchRequest.CountryISOCode = pstrCitySearchText.Split(',')[0].ToString();
            lobjSearchRequest.Country = HttpUtility.UrlDecode(pstrCitySearchText.Split(',')[1].ToString());
            RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererData"] as RefererDetails;
            lobjSearchRequest.RefererDetails = lobjRefererDetails;
            return lobjSearchRequest;
        }

        //public RequestHotelSearch InsertSearchDetails(RequestHotelSearch pobjSearchRequest)
        //{
        //    RuleEngineHelper lobjHelper = new RuleEngineHelper();
        //    HotelModel lobjHotelModel = new HotelModel();
        //    RequestHotelSearch lobjSearchRequest = new RequestHotelSearch();

        //    lobjSearchRequest.CheckInDate = pobjSearchRequest.CheckInDate;
        //    lobjSearchRequest.CheckOutDate = pobjSearchRequest.CheckOutDate;
        //    lobjSearchRequest.NoOfRooms = pobjSearchRequest.NoOfRooms;
        //    lobjSearchRequest.AdultPerRoom = pobjSearchRequest.AdultPerRoom;
        //    lobjSearchRequest.ChildrenPerRoom = pobjSearchRequest.ChildrenPerRoom;
        //    lobjSearchRequest.CityName = pobjSearchRequest.CityName;
        //    lobjSearchRequest.CountryISOCode = pobjSearchRequest.CountryISOCode;
        //    lobjSearchRequest.IpAddress = pobjSearchRequest.IpAddress;
        //    lobjSearchRequest.Country = pobjSearchRequest.Country;
        //    //lobjSearchRequest.SearchId = (lobjHotelModel.InsertSearchDetails(pobjSearchRequest));
        //    lobjSearchRequest.SearchId = lobjHelper.ExecuteRuleSet<int>(new ExecuteRequest("DFInsertSearchDetails", "InsertSearchDetails", new List<object>(0) { pobjSearchRequest }));
        //    return lobjSearchRequest;
        //}

        public HotelSearchResponse SearchHotel(HotelSearchRequest pobjSearchRequest)
        {
            IBEHotelModel lobjModel = new IBEHotelModel();
            return lobjModel.GetHotels(pobjSearchRequest);
        }

        public Framework.Integrations.Hotels.Entities.Customer CustomerInfo(string pstrTitle, string pstrFirstName, string pstrLastName, string pstrCity, string pstrState, string pstrAddress1, string pstrAddress2, string pstrCountry, string pstrPostalCode, string pstrPhoneNo, string pstrMobileNo, string pstrEmailID)
        {
            Framework.Integrations.Hotels.Entities.Customer lobjCustomer = new Framework.Integrations.Hotels.Entities.Customer();
            lobjCustomer.title = pstrTitle;
            lobjCustomer.firstname = pstrFirstName;
            lobjCustomer.lastname = pstrLastName;
            lobjCustomer.city = pstrCity;
            lobjCustomer.country = pstrCountry;
            lobjCustomer.email = pstrEmailID;
            lobjCustomer.landline = pstrPhoneNo;
            lobjCustomer.mobile = pstrMobileNo;
            lobjCustomer.streetaddress1 = pstrAddress1;
            lobjCustomer.streetaddress2 = pstrAddress2;
            lobjCustomer.postalcode = pstrPostalCode;
            lobjCustomer.state = pstrState;
            return lobjCustomer;
        }
        //public HotelBookingResponse BookHotel(MemberDetails pobjMemberDetails, Hotel pobjHotel, HotelSearchRequest pobjSearchRequest, Customer pobjCustomer, List<RedemptionDetails> pobjListOfRedemptionDetails)
        //{
        //    List<int> lobjListOfAdult = new List<int>();
        //    string[] arrayAdultPerRoom = pobjSearchRequest.SearchRequest.AdultPerRoom.Split(',');
        //    for (int i = 0; i < arrayAdultPerRoom.Count(); i++)
        //    {
        //        lobjListOfAdult.Add(Convert.ToInt32(arrayAdultPerRoom[i]));
        //    }
        //    List<int> lobjListOfChild = new List<int>();
        //    string[] arrayChildPerRoom = pobjSearchRequest.SearchRequest.ChildrenPerRoom.Split(',');
        //    for (int i = 0; i < arrayChildPerRoom.Count(); i++)
        //    {
        //        lobjListOfChild.Add(Convert.ToInt32(arrayChildPerRoom[i]));
        //    }
        //    HotelBookingRequest lobjBookingRequest = new HotelBookingRequest();
        //    lobjBookingRequest.BookRequest.customer = pobjCustomer;
        //    lobjBookingRequest.BookRequest.checkindate = pobjSearchRequest.SearchRequest.CheckInDate;
        //    lobjBookingRequest.BookRequest.checkoutdate = pobjSearchRequest.SearchRequest.CheckOutDate;
        //    lobjBookingRequest.BookRequest.numberofrooms = pobjSearchRequest.SearchRequest.NoOfRooms;
        //    lobjBookingRequest.BookRequest.nri = false;
        //    lobjBookingRequest.BookRequest.adultsperroom = lobjListOfAdult.ToArray();
        //    lobjBookingRequest.BookRequest.childrenperroom = lobjListOfChild.ToArray();
        //    lobjBookingRequest.BookRequest.bookingcode = pobjHotel.roomrates.RoomRate[0].bookingcode;
        //    lobjBookingRequest.BookRequest.roomtypecode = pobjHotel.roomrates.RoomRate[0].roomtype.roomtypecode;
        //    lobjBookingRequest.BookRequest.TotalPoints = pobjHotel.roomrates.RoomRate[0].TotalPoints;
        //    lobjBookingRequest.BookRequest.customeripaddress = pobjSearchRequest.SearchRequest.IpAddress;
        //    lobjBookingRequest.BookRequest.hotelid = pobjHotel.hotelid;
        //    lobjBookingRequest.BookRequest.bookingamount = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalDefaultAmount);
        //    lobjBookingRequest.BookRequest.totalBaseFare = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalBaseAmount);
        //    lobjBookingRequest.BookRequest.totalDefaulFare = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalDefaultAmount);
        //    pobjHotel.roomrates.RoomRate[0].TotalPoints = lobjBookingRequest.BookRequest.TotalPoints;
        //    pobjSearchRequest.SearchRequest.MembershipReference = pobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
        //    //BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
        //    //HotelBookingResponse lobjHotelBookingResponse = new HotelBookingResponse();
        //    //lobjHotelBookingResponse = lobjBookingIntegrationFacade.BookHotel(lobjBookingRequest, pobjHotel, pobjSearchRequest, pobjMemberDetails, pobjCustomer, pobjListOfRedemptionDetails);
        //    //return lobjHotelBookingResponse;

        //    RuleEngineHelper lobjHelper = new RuleEngineHelper();
        //    return lobjHelper.ExecuteRuleSet<CB.IBE.Platform.Hotels.ClientEntities.HotelBookingResponse>(new ExecuteRequest("BookHotel", "BookHotel", new List<object>(0) { lobjBookingRequest, pobjHotel, pobjSearchRequest, pobjMemberDetails, pobjCustomer, pobjListOfRedemptionDetails }));
        //}

        public HotelBookingResponse BookHotel(MemberDetails pobjMemberDetails, Hotel pobjHotel, HotelSearchRequest pobjSearchRequest, Customer pobjCustomer, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            LoggingAdapter.WriteLog("Booh Hotel 1");
            List<int> lobjListOfAdult = new List<int>();
            string[] arrayAdultPerRoom = pobjSearchRequest.SearchRequest.AdultPerRoom.Split(',');
            for (int i = 0; i < arrayAdultPerRoom.Count(); i++)
            {
                lobjListOfAdult.Add(Convert.ToInt32(arrayAdultPerRoom[i]));
            }
            List<int> lobjListOfChild = new List<int>();
            string[] arrayChildPerRoom = pobjSearchRequest.SearchRequest.ChildrenPerRoom.Split(',');
            for (int i = 0; i < arrayChildPerRoom.Count(); i++)
            {
                lobjListOfChild.Add(Convert.ToInt32(arrayChildPerRoom[i]));
            }
            HotelBookingRequest lobjBookingRequest = new HotelBookingRequest();
            lobjBookingRequest.BookRequest.customer = pobjCustomer;
            lobjBookingRequest.BookRequest.checkindate = pobjSearchRequest.SearchRequest.CheckInDate;
            lobjBookingRequest.BookRequest.checkoutdate = pobjSearchRequest.SearchRequest.CheckOutDate;
            lobjBookingRequest.BookRequest.numberofrooms = pobjSearchRequest.SearchRequest.NoOfRooms;
            lobjBookingRequest.BookRequest.nri = false;
            lobjBookingRequest.BookRequest.adultsperroom = lobjListOfAdult.ToArray();
            lobjBookingRequest.BookRequest.childrenperroom = lobjListOfChild.ToArray();
            lobjBookingRequest.BookRequest.bookingcode = pobjHotel.roomrates.RoomRate[0].bookingcode;
            lobjBookingRequest.BookRequest.roomtypecode = pobjHotel.roomrates.RoomRate[0].roomtype.roomtypecode;
            lobjBookingRequest.BookRequest.TotalPoints = pobjHotel.roomrates.RoomRate[0].TotalPoints;
            lobjBookingRequest.BookRequest.customeripaddress = pobjSearchRequest.SearchRequest.IpAddress;
            lobjBookingRequest.BookRequest.hotelid = pobjHotel.hotelid;
            lobjBookingRequest.BookRequest.bookingamount = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalDefaultAmount);
            lobjBookingRequest.BookRequest.totalBaseFare = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalBaseAmount);
            lobjBookingRequest.BookRequest.totalDefaulFare = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalDefaultAmount);

            //lobjBookingRequest.BookingPaymentDetails.PointsTxnRefererence = pstrTxnReference;

            pobjHotel.roomrates.RoomRate[0].TotalPoints = lobjBookingRequest.BookRequest.TotalPoints;
            pobjSearchRequest.SearchRequest.MembershipReference = pobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
            LoggingAdapter.WriteLog("Booh Hotel 2");
            BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
            HotelBookingResponse lobjHotelBookingResponse = new HotelBookingResponse();
            lobjHotelBookingResponse = lobjBookingIntegrationFacade.BookHotel(lobjBookingRequest, pobjHotel, pobjSearchRequest, pobjMemberDetails, pobjCustomer, pobjListOfRedemptionDetails);
            return lobjHotelBookingResponse;

            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<CB.IBE.Platform.Hotels.ClientEntities.HotelBookingResponse>(new ExecuteRequest("BookHotel", "BookHotel", new List<object>(0) { lobjBookingRequest, pobjHotel, pobjSearchRequest, pobjMemberDetails, pobjCustomer, pobjListOfRedemptionDetails }));
        }

        public HotelBookingResponse BookHotelByInfiPay(MemberDetails pobjMemberDetails, Hotel pobjHotel, HotelSearchRequest pobjSearchRequest, Customer pobjCustomer, List<RedemptionDetails> pobjListOfRedemptionDetails, string pstrTxnReference)
        {
            LoggingAdapter.WriteLog("Booh Hotel 1");
            List<int> lobjListOfAdult = new List<int>();
            string[] arrayAdultPerRoom = pobjSearchRequest.SearchRequest.AdultPerRoom.Split(',');
            for (int i = 0; i < arrayAdultPerRoom.Count(); i++)
            {
                lobjListOfAdult.Add(Convert.ToInt32(arrayAdultPerRoom[i]));
            }
            List<int> lobjListOfChild = new List<int>();
            string[] arrayChildPerRoom = pobjSearchRequest.SearchRequest.ChildrenPerRoom.Split(',');
            for (int i = 0; i < arrayChildPerRoom.Count(); i++)
            {
                lobjListOfChild.Add(Convert.ToInt32(arrayChildPerRoom[i]));
            }
            HotelBookingRequest lobjBookingRequest = new HotelBookingRequest();
            lobjBookingRequest.BookRequest.customer = pobjCustomer;
            lobjBookingRequest.BookRequest.checkindate = pobjSearchRequest.SearchRequest.CheckInDate;
            lobjBookingRequest.BookRequest.checkoutdate = pobjSearchRequest.SearchRequest.CheckOutDate;
            lobjBookingRequest.BookRequest.numberofrooms = pobjSearchRequest.SearchRequest.NoOfRooms;
            lobjBookingRequest.BookRequest.nri = false;
            lobjBookingRequest.BookRequest.adultsperroom = lobjListOfAdult.ToArray();
            lobjBookingRequest.BookRequest.childrenperroom = lobjListOfChild.ToArray();
            lobjBookingRequest.BookRequest.bookingcode = pobjHotel.roomrates.RoomRate[0].bookingcode;
            lobjBookingRequest.BookRequest.roomtypecode = pobjHotel.roomrates.RoomRate[0].roomtype.roomtypecode;
            lobjBookingRequest.BookRequest.TotalPoints = pobjHotel.roomrates.RoomRate[0].TotalPoints;
            lobjBookingRequest.BookRequest.customeripaddress = pobjSearchRequest.SearchRequest.IpAddress;
            lobjBookingRequest.BookRequest.hotelid = pobjHotel.hotelid;
            lobjBookingRequest.BookRequest.bookingamount = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalDefaultAmount);
            lobjBookingRequest.BookRequest.totalBaseFare = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalBaseAmount);
            lobjBookingRequest.BookRequest.totalDefaulFare = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalDefaultAmount);

            lobjBookingRequest.BookingPaymentDetails.PointsTxnRefererence = pstrTxnReference;

            pobjHotel.roomrates.RoomRate[0].TotalPoints = lobjBookingRequest.BookRequest.TotalPoints;
            pobjSearchRequest.SearchRequest.MembershipReference = pobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
            LoggingAdapter.WriteLog("Booh Hotel 2");
            BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
            HotelBookingResponse lobjHotelBookingResponse = new HotelBookingResponse();
            lobjHotelBookingResponse = lobjBookingIntegrationFacade.BookHotelByInfiPay(lobjBookingRequest, pobjHotel, pobjSearchRequest, pobjMemberDetails, pobjCustomer, pobjListOfRedemptionDetails);
            return lobjHotelBookingResponse;
        }


        public HotelBookingResponse BookForHotel(MemberDetails pobjMemberDetails, Hotel pobjHotel, HotelSearchRequest pobjSearchRequest, Customer pobjCustomer, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            List<int> lobjListOfAdult = new List<int>();
            string[] arrayAdultPerRoom = pobjSearchRequest.SearchRequest.AdultPerRoom.Split(',');
            for (int i = 0; i < arrayAdultPerRoom.Count(); i++)
            {
                lobjListOfAdult.Add(Convert.ToInt32(arrayAdultPerRoom[i]));
            }
            List<int> lobjListOfChild = new List<int>();
            string[] arrayChildPerRoom = pobjSearchRequest.SearchRequest.ChildrenPerRoom.Split(',');
            for (int i = 0; i < arrayChildPerRoom.Count(); i++)
            {
                lobjListOfChild.Add(Convert.ToInt32(arrayChildPerRoom[i]));
            }
            HotelBookingRequest lobjBookingRequest = new HotelBookingRequest();
            lobjBookingRequest.BookRequest.customer = pobjCustomer;
            lobjBookingRequest.BookRequest.checkindate = pobjSearchRequest.SearchRequest.CheckInDate;
            lobjBookingRequest.BookRequest.checkoutdate = pobjSearchRequest.SearchRequest.CheckOutDate;
            lobjBookingRequest.BookRequest.numberofrooms = pobjSearchRequest.SearchRequest.NoOfRooms;
            lobjBookingRequest.BookRequest.nri = false;
            lobjBookingRequest.BookRequest.adultsperroom = lobjListOfAdult.ToArray();
            lobjBookingRequest.BookRequest.childrenperroom = lobjListOfChild.ToArray();
            lobjBookingRequest.BookRequest.bookingcode = pobjHotel.roomrates.RoomRate[0].bookingcode;
            lobjBookingRequest.BookRequest.roomtypecode = pobjHotel.roomrates.RoomRate[0].roomtype.roomtypecode;
            lobjBookingRequest.BookRequest.TotalPoints = pobjHotel.roomrates.RoomRate[0].TotalPoints;
            lobjBookingRequest.BookRequest.customeripaddress = pobjSearchRequest.SearchRequest.IpAddress;
            lobjBookingRequest.BookRequest.hotelid = pobjHotel.hotelid;
            lobjBookingRequest.BookRequest.bookingamount = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalDefaultAmount);
            lobjBookingRequest.BookRequest.totalBaseFare = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalBaseAmount);
            lobjBookingRequest.BookRequest.totalDefaulFare = Convert.ToDouble(pobjHotel.roomrates.RoomRate[0].TotalDefaultAmount);
            pobjHotel.roomrates.RoomRate[0].TotalPoints = lobjBookingRequest.BookRequest.TotalPoints;
            pobjSearchRequest.SearchRequest.MembershipReference = pobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
            //BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
            //HotelBookingResponse lobjHotelBookingResponse = new HotelBookingResponse();
            //lobjHotelBookingResponse = lobjBookingIntegrationFacade.BookHotel(lobjBookingRequest, pobjHotel, pobjSearchRequest, pobjMemberDetails, pobjCustomer, pobjListOfRedemptionDetails);
            //return lobjHotelBookingResponse;

            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<CB.IBE.Platform.Hotels.ClientEntities.HotelBookingResponse>(new ExecuteRequest("BookHotel", "BookHotel", new List<object>(0) { lobjBookingRequest, pobjHotel, pobjSearchRequest, pobjMemberDetails, pobjCustomer, pobjListOfRedemptionDetails }));
        }

        public List<string> GetAllHotelCities()
        {
            try
            {
                IBEHotelModel lobjIBEHotelModel = new IBEHotelModel();
                return lobjIBEHotelModel.GetAllHotelCities();
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllHotelCities -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
        }
        public bool CashBackRedemtions(AdditionalRedemptionDetails pobjAdditionalRedemptionDetails, MemberDetails pobjMemberDetails)
        {
            try
            {
                //AdditionalRedemptionFacade lobjAdditionalRedemptionFacade = new AdditionalRedemptionFacade() ;
                //return lobjAdditionalRedemptionFacade.CashBackRedemtions(pobjAdditionalRedemptionDetails, pobjMemberDetails);
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("InsertCashBackRedemtions", "InsertCashBackRedemtions", new List<object>(0) { pobjAdditionalRedemptionDetails, pobjMemberDetails }));
            }

            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CashBackRedemtions -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return false;
            }
        }
        /////////////////////////// Car Related Method Added by Baban //////////////////////////////////

        public string SignInRequest()
        {
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<string>(new ExecuteRequest("DFSignInRequest", "SignInRequest", null));
        }

        //public string CheckAvailbility(MemberDetails pobjMemberDetails)
        //{
        //    RuleEngineHelper lobjHelper = new RuleEngineHelper();
        //    Response lobjResponse = lobjHelper.ExecuteRuleSet<Response>(new ExecuteRequest("DFCheckAvailbility", "CheckAvailbility", new List<object>(0) { pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS) }));
        //    return JSONSerialization.Deserialize<string>(lobjResponse.ReturnObject);
        //}

        //public int CheckAvailbility(string pstrRelationReference, int pintRelationType)
        //{
        //    PGHelper lobjPGHelper = new PGHelper();
        //    return lobjPGHelper.CheckPointsAvailability(pstrRelationReference, pintRelationType, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
        //}
        public int CheckAvailbility(string pstrRelationReference, int pintRelationType, string pstrCurrency)
        {
            try
            {
                PGHelper lobjPGHelper = new PGHelper();
                return lobjPGHelper.CheckPointsAvailability(pstrRelationReference, pintRelationType, pstrCurrency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CheckAvailbility -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return 0;
            }
        }
        public bool ForgotPassword(MemberRelation pobjMemberRelation)
        {
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = GetProgramMaster();

            if (lobjProgramDefinition != null)
            {
                ResetPassword lobjResetPassword = new ResetPassword();
                lobjResetPassword.UniqueIdentifier = pobjMemberRelation.RelationReference;
                lobjResetPassword.ProgramId = lobjProgramDefinition.ProgramId;
                lobjResetPassword.RelationType = RelationType.LBMS;
                //MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                //return lobjMemberBusinessFacade.ForgotPassword(lobjResetPassword);
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("SendForgetPassword", "ForgotPassword", new List<object>(0) { lobjResetPassword }));
            }
            else
            {
                LoggingAdapter.WriteLog("Riyad MODEL Forget Pwd program Detail Invalid");
                return false;
            }
        }

        //Model added by Poonam 3 june 2013
        public GetHotelInfoDetails GetHotelDetails(string pstrMembershipReference)
        {
            RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererData"] as RefererDetails;
            IBECTHotelClient lobjClient = new IBECTHotelClient();
            return lobjClient.GetMemberBookedHotelInfoList(pstrMembershipReference, lobjRefererDetails.Id);
        }

        public HotelInformationResponse GetHotelInformation(HotelInformationRequest pobjHotelInformationRequest)
        {
            IBEHotelModel lobjHotelModel = new IBEHotelModel();
            RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererData"] as RefererDetails;
            pobjHotelInformationRequest.RefererDetails = lobjRefererDetails;
            return lobjHotelModel.GetHotelInformation(pobjHotelInformationRequest);
        }

        public bool CancelBookingRequest(HotelCancellationRequest pobjHotelCancellationRequest, string pstrMembershipReference)
        {
            IBEHotelModel lobjModel = new IBEHotelModel();
            RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererData"] as RefererDetails;
            pobjHotelCancellationRequest.CancellationRequest.RefererDetails = lobjRefererDetails;
            HotelCancellationResponse lobjHotelCancellationResponse = new HotelCancellationResponse();
            lobjHotelCancellationResponse = lobjModel.CancelBooking(pobjHotelCancellationRequest, pstrMembershipReference);
            return lobjHotelCancellationResponse.Status;
        }

        public int GetTotalMemberAccrualTransaction(SearchTransactions pobjSearchTransactions)
        {
            // TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            // return objTransactionBusinessFacade.GetTotalMemberAccrualTransaction(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<int>(new ExecuteRequest("GetTotalMemberAccrualTransaction", "GetTotalMemberAccrualTransaction", new List<object>(0) { pobjSearchTransactions }));
        }

        public int GetTotalMemberAccrualTransactionByDate(SearchTransactions pobjSearchTransactions)
        {
            // TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            // return objTransactionBusinessFacade.GetTotalMemberAccrualTransactionByDate(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<int>(new ExecuteRequest("GetTotalMemberAccrualTransactionByDate", "GetTotalMemberAccrualTransactionByDate", new List<object>(0) { pobjSearchTransactions }));
        }
        public List<TransactionDetails> GetMemberAccrualTransaction(SearchTransactions pobjSearchTransactions)
        {
            //TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            //return objTransactionBusinessFacade.GetMemberAccrualTransaction(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<List<TransactionDetails>>(new ExecuteRequest("GetMemberAccrualTransaction", "v3GetMemberAccrualTransaction", new List<object>(0) { pobjSearchTransactions }));
        }

        public List<TransactionDetails> GetMemberAccrualTransactionByDate(SearchTransactions pobjSearchTransactions)
        {
            // TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            // return objTransactionBusinessFacade.GetMemberAccrualTransactionByDate(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<List<TransactionDetails>>(new ExecuteRequest("GetMemberAccrualTransactionByDate", "v3GetMemberAccrualTransactionByDate", new List<object>(0) { pobjSearchTransactions }));
        }
        public List<TransactionDetails> GetMemberRedemptionTransactionByDate(SearchTransactions pobjSearchTransactions)
        {
            //TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            //return objTransactionBusinessFacade.GetMemberRedemptionTransactionByDate(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<List<TransactionDetails>>(new ExecuteRequest("GetMemberRedemptionTransactionByDate", "v3GetMemberRedemptionTransactionByDate", new List<object>(0) { pobjSearchTransactions }));
        }
        public List<TransactionDetails> GetMemberRedemptionTransaction(SearchTransactions pobjSearchTransactions)
        {
            //TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
            //return objTransactionBusinessFacade.GetMemberRedemptionTransaction(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<List<TransactionDetails>>(new ExecuteRequest("GetMemberRedemptionTransaction", "v3GetMemberRedemptionTransaction", new List<object>(0) { pobjSearchTransactions }));
        }
        public int GetTotalMemberRedemptionTransaction(SearchTransactions pobjSearchTransactions)
        {
            //TransactionBusinessFacade lobjTransactionBusinessFacade = new TransactionBusinessFacade();
            //return lobjTransactionBusinessFacade.GetTotalMemberRedemptionTransaction(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<int>(new ExecuteRequest("GetTotalMemberRedemptionTransaction", "GetTotalMemberRedemptionTransaction", new List<object>(0) { pobjSearchTransactions }));
        }

        public int GetTotalMemberRedemptionTransactionByDate(SearchTransactions pobjSearchTransactions)
        {
            //TransactionBusinessFacade lobjTransactionBusinessFacade = new TransactionBusinessFacade();
            //return lobjTransactionBusinessFacade.GetTotalMemberRedemptionTransactionByDate(pobjSearchTransactions);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<int>(new ExecuteRequest("GetTotalMemberRedemptionTransactionByDate", "GetTotalMemberRedemptionTransactionByDate", new List<object>(0) { pobjSearchTransactions }));
        }
        public bool ChangePasswordForMember(MemberRelation lobjMemberRelation)
        {
            try
            {
                //MemberBusinessFacade lobjFacade = new MemberBusinessFacade();
                //return lobjFacade.ChangePasswordForMember(lobjMemberRelation);
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("ChangePasswordForMember", "ChangePasswordForMember", new List<object>(0) { lobjMemberRelation }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetSystemParametres -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return false;
            }
        }
        public bool CheckOldPassword(string pstrMembershipref, string pstrOldpassword, string pstrMemberPassword)
        {
            MemberRelation lobjMemberRelation = new MemberRelation();
            lobjMemberRelation.RelationReference = pstrMembershipref;
            lobjMemberRelation.WebPassword = pstrOldpassword;
            //MemberBusinessFacade lobjFacade = new MemberBusinessFacade();
            //return lobjFacade.CheckOldPassword(lobjMemberRelation, pstrMemberPassword);
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("CheckOldPassword", "CheckOldPassword", new List<object>(0) { lobjMemberRelation, pstrMemberPassword }));
        }
        public RefererDetails GetRefererData()
        {
            try
            {
                RefererDetails lobjRefererDetails = new RefererDetails();
                lobjRefererDetails.Id = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["RefererId"]);
                lobjRefererDetails.UserName = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["RefererName"]);
                lobjRefererDetails.Password = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["RefererPassword"]);
                return lobjRefererDetails;

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("RefererData -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
        }
        //public BookingResponse BookFlight(BookingRequest pobjBookingRequest, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        //{
        //    //BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
        //    //BookingResponse lobjBookingResponse = lobjBookingIntegrationFacade.BookFlight(pobjBookingRequest, pobjMemberDetails, pobjListOfRedemptionDetails);
        //    //return lobjBookingResponse;

        //    RuleEngineHelper lobjHelper = new RuleEngineHelper();
        //    return lobjHelper.ExecuteRuleSet<BookingResponse>(new ExecuteRequest("BookFlight", "BookFlight", new List<object>(0) { pobjBookingRequest, pobjMemberDetails, pobjListOfRedemptionDetails }));
        //}

        public BookingResponse BookFlight(BookingRequest pobjBookingRequest, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            //BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
            //BookingResponse lobjBookingResponse = lobjBookingIntegrationFacade.BookFlight(pobjBookingRequest, pobjMemberDetails, pobjListOfRedemptionDetails);
            //return lobjBookingResponse;

            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<BookingResponse>(new ExecuteRequest("BookFlight", "BookFlight", new List<object>(0) { pobjBookingRequest, pobjMemberDetails, pobjListOfRedemptionDetails }));
        }
        public BookingResponse BookFlightByInfiPay(BookingRequest pobjBookingRequest, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
            BookingResponse lobjBookingResponse = lobjBookingIntegrationFacade.BookFlightByInfiPay(pobjBookingRequest, pobjMemberDetails, pobjListOfRedemptionDetails);
            return lobjBookingResponse;
        }


        public BookingResponse BookForFlight(BookingRequest pobjBookingRequest, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            //BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
            //BookingResponse lobjBookingResponse = lobjBookingIntegrationFacade.BookFlight(pobjBookingRequest, pobjMemberDetails, pobjListOfRedemptionDetails);
            //return lobjBookingResponse;

            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<BookingResponse>(new ExecuteRequest("BookFlight", "BookFlight", new List<object>(0) { pobjBookingRequest, pobjMemberDetails, pobjListOfRedemptionDetails }));
        }
        //public void SendFailureEmail(string pstrBody, string pstrMailSubject)
        //{
        //    MailComponent lobjComponent = new MailComponent();
        //    EmailParameters lobjParams = new EmailParameters();
        //    lobjParams.MailTo = System.Configuration.ConfigurationSettings.AppSettings["CustomerService"].ToString();
        //    lobjParams.BCC = System.Configuration.ConfigurationSettings.AppSettings["BCC"].ToString();
        //    lobjParams.CC = string.Empty;
        //    lobjParams.MailFrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString(); //"info@commercebay.com";
        //    lobjParams.MailSubject = pstrMailSubject;
        //    lobjParams.MailBody = pstrBody;
        //    lobjParams.SMTPUsername = System.Configuration.ConfigurationSettings.AppSettings["SMTPUsername"].ToString(); //"info@commercebay.com";
        //    lobjParams.SMTPPassword = System.Configuration.ConfigurationSettings.AppSettings["SMTPPassword"].ToString();//"HOOHVMy8";
        //    lobjParams.SMTPServer = System.Configuration.ConfigurationSettings.AppSettings["SMTPServer"].ToString(); //"smtp.commercebay.com";
        //    lobjParams.SMTPPort = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["SMTPPort"].ToString()); //587;
        //    lobjComponent.Send(lobjParams, new List<Attachment>());
        //}
        public void SendFailureEmail(string pstrBody, string pstrMailSubject)
        {
            MailComponent lobjComponent = new MailComponent();
            EmailParameters lobjParams = new EmailParameters();

            lobjParams.MailFrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString(); //"info@commercebay.com";
            lobjParams.MailTo = System.Configuration.ConfigurationSettings.AppSettings["CustomerService"].ToString();
            lobjParams.CC = System.Configuration.ConfigurationSettings.AppSettings["CC"].ToString();
            lobjParams.BCC = System.Configuration.ConfigurationSettings.AppSettings["BCC"].ToString();
            lobjParams.MailSubject = pstrMailSubject;
            lobjParams.MailBody = pstrBody;
            lobjParams.SMTPUsername = System.Configuration.ConfigurationSettings.AppSettings["SMTPUsername"].ToString(); //"info@commercebay.com";
            lobjParams.SMTPPassword = System.Configuration.ConfigurationSettings.AppSettings["SMTPPassword"].ToString();//"HOOHVMy8";
            lobjParams.SMTPServer = System.Configuration.ConfigurationSettings.AppSettings["SMTPServer"].ToString(); //"smtp.commercebay.com";
            lobjParams.SMTPPort = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["SMTPPort"].ToString()); //587;
            lobjComponent.Send(lobjParams, new List<Attachment>());
        }

        //public string createReceiptForFailHotel(MemberDetails pobjMemberDetails, HotelSearchResponse pobjHotelsInfo, HotelSearchRequest pobjSearchRequest, Framework.Integrations.Hotels.Entities.Customer pobjCustomer, HotelBookingResponse pobjBookingResponse, string pstrMailBody)
        //{
        //    string pstrEmailTemplate = pstrMailBody;
        //    object[] lobjArray = new object[18];
        //    lobjArray[0] = pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
        //    lobjArray[1] = pobjMemberDetails.LastName;
        //    lobjArray[2] = Convert.ToString(pobjHotelsInfo.SearchResponse.hotels.hotel[0].roomrates.RoomRate[0].TotalPoints);
        //    lobjArray[3] = pobjMemberDetails.MobileNumber;
        //    lobjArray[4] = pobjHotelsInfo.SearchResponse.hotels.hotel[0].basicinfo.hotelname;
        //    lobjArray[5] = pobjHotelsInfo.SearchResponse.hotels.hotel[0].basicinfo.city;
        //    lobjArray[6] = pobjHotelsInfo.SearchResponse.searchcriteria.numberofrooms;
        //    int TotalAdult = 0;
        //    string[] arrayAdultPerRoom = pobjSearchRequest.SearchRequest.AdultPerRoom.Split(',');
        //    for (int i = 0; i < arrayAdultPerRoom.Count(); i++)
        //    {
        //        TotalAdult = TotalAdult + Convert.ToInt32(arrayAdultPerRoom[i]);
        //    }
        //    int TotalChild = 0;
        //    string[] arrayChildPerRoom = pobjSearchRequest.SearchRequest.ChildrenPerRoom.Split(',');
        //    for (int i = 0; i < arrayChildPerRoom.Count(); i++)
        //    {
        //        TotalChild = TotalChild + Convert.ToInt32(arrayChildPerRoom[i]);
        //    }
        //    lobjArray[7] = Convert.ToString(TotalAdult);
        //    lobjArray[8] = Convert.ToString(TotalChild);
        //    lobjArray[9] = pobjSearchRequest.SearchRequest.CheckInDate.ToString("dd/MM/yyyy");
        //    lobjArray[10] = pobjSearchRequest.SearchRequest.CheckOutDate.ToString("dd/MM/yyyy");
        //    lobjArray[11] = pobjCustomer.title + ". " + pobjCustomer.firstname + " " + pobjCustomer.lastname;
        //    lobjArray[12] = pobjCustomer.city;
        //    lobjArray[13] = pobjCustomer.country;
        //    lobjArray[14] = pobjCustomer.postalcode;
        //    lobjArray[15] = pobjCustomer.mobile;
        //    lobjArray[16] = pobjCustomer.email;
        //    lobjArray[17] = pobjBookingResponse.BookingResponse.TransactionRefCode;
        //    return string.Format(pstrEmailTemplate, lobjArray);
        //}

        public string createReceiptForFailCar(CarMakeBookingResponse pobjCarMakeBookingResponse, CarMakeBookingRequest pobjCarMakeBookingRequest, MemberDetails pobjMemberDetails, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, string pstrMailBody)
        {
            string pstrEmailTemplate = pstrMailBody;
            object[] lobjArray = new object[20];
            lobjArray[0] = Convert.ToString(pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
            //lobjArray[0] = Convert.ToString(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;     pobjMemberDetails.MembershipReference);
            lobjArray[1] = Convert.ToString(pobjMemberDetails.LastName);
            lobjArray[2] = Convert.ToString(pobjMemberDetails.MobileNumber);
            lobjArray[3] = Convert.ToString(pobjCarMakeBookingResponse.BookingReferenceId);
            lobjArray[4] = Convert.ToString(pobjMatch.Price[0].TotalPoints + pobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[0].Price[0].ExtraTotalPoints);
            lobjArray[15] = Convert.ToString(pobjMatch.Vehicle[0].Name);
            lobjArray[5] = Convert.ToString(pobjMatch.Route[0].PickUp[0].locName);
            lobjArray[6] = Convert.ToString(pobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].city);
            lobjArray[7] = Convert.ToString(pobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].country);
            lobjArray[8] = Convert.ToString(pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Date[0].day + "/" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Date[0].month + "/" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Date[0].year);
            lobjArray[9] = Convert.ToString(pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Date[0].hour + ":" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Date[0].minute);
            lobjArray[10] = Convert.ToString(pobjMatch.Route[0].DropOff[0].locName);
            lobjArray[11] = Convert.ToString(pobjCarSearchRequest.SearchRequest.DropOff[0].Location[0].city);
            lobjArray[12] = Convert.ToString(pobjCarSearchRequest.SearchRequest.DropOff[0].Location[0].country);
            lobjArray[13] = Convert.ToString(pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff[0].Date[0].day + "/" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff[0].Date[0].month + "/" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff[0].Date[0].year);
            lobjArray[14] = Convert.ToString(pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Date[0].hour + ":" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Date[0].minute);
            lobjArray[16] = Convert.ToString(pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].title);
            lobjArray[17] = Convert.ToString(pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].firstname);
            lobjArray[18] = Convert.ToString(pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].lastname);
            return string.Format(pstrEmailTemplate, lobjArray);
        }

        public List<ItineraryDetails> GetFlightBookingDetails(string pstrMemberId)
        {
            IBECTClient lobjClient = new IBECTClient();
            RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererData"] as RefererDetails;
            return lobjClient.GetFlightBookingListForMember(pstrMemberId, lobjRefererDetails.Id);
        }

        public RetriveItineraryDetails RetriveItineraryDetails(string strTripId, int pintReferrId)
        {
            IBEModel lobjIBEModel = new IBEModel();
            return lobjIBEModel.GetBookedFlightInformation(strTripId, pintReferrId);
        }

        public HotelItineraryResponse GetBookedHotelInfo(string pstrTransactionRef)
        {
            IBEHotelModel lobjIBEHotelModel = new IBEHotelModel();
            return lobjIBEHotelModel.GetBookedHotelInfo(pstrTransactionRef);
        }

        // Add Car Method by chandan ---------------------------------------------------------------------------------------------------------------
        public CarPickUpCityListResponce GetAllPickUpCity(CarPickUpCityListRequest lobjCarPickUpCityListRequest)
        {
            IBECarModel lobjModel = new IBECarModel();
            CarPickUpCityListResponce lobjCarPickUpCityListResponce = lobjModel.CarPickUpCityListResponse(lobjCarPickUpCityListRequest);
            return lobjCarPickUpCityListResponce;
        }
        public CarPickUpLocationListResponce GetAllPickUpLocation(CarPickUpLocationListRequest lobjCarPickUpLocationListRequest)
        {
            IBECarModel lobjModel = new IBECarModel();
            CarPickUpLocationListResponce lobjCarPickUpLocationListResponce = lobjModel.CarPickUpLocationListResponse(lobjCarPickUpLocationListRequest);
            return lobjCarPickUpLocationListResponce;
        }

        public CarDropOffCountryListResponse GetAllDropOffCountry(CarDropOffCountryListRequest pobjCarDropOffCountryListRequest)
        {
            IBECarModel lobjModel = new IBECarModel();
            CarDropOffCountryListResponse lobjCarDropOffCountryListResponse = lobjModel.CarDropOffCountryListResponse(pobjCarDropOffCountryListRequest);
            return lobjCarDropOffCountryListResponse;
        }

        public CarDropOffCityListResponce GetAllDropOffCity(CarDropOffCityListRequest pobjCarDropOffCityListRequest)
        {
            IBECarModel lobjModel = new IBECarModel();
            CarDropOffCityListResponce lobjCarDropOffCityListResponce = lobjModel.CarDropOffCityListResponce(pobjCarDropOffCityListRequest);
            return lobjCarDropOffCityListResponce;
        }

        public CarDropOffLocationListResponce GetAllDropOffLocation(CarDropOffLocationListRequest pobjCarDropOffLocationListRequest)
        {
            IBECarModel lobjModel = new IBECarModel();
            CarDropOffLocationListResponce lobjCarDropOffLocationListResponce = lobjModel.CarDropOffLocationListResponce(pobjCarDropOffLocationListRequest);
            return lobjCarDropOffLocationListResponce;
        }

        public CarPickUpOpenTimeResponce GetCarPickUpOpenTime(CarPickUpOpenTimeRequest lobjCarPickUpOpenTimeRequest)
        {
            IBECarModel lobjModel = new IBECarModel();
            CarPickUpOpenTimeResponce lobjCarPickUpOpenTimeResponce = lobjModel.CarPickUpOpenTimeResponce(lobjCarPickUpOpenTimeRequest);
            return lobjCarPickUpOpenTimeResponce;
        }

        public string[] DateFormat(string date)
        {
            string[] Date = date.Split('/');
            return Date;
        }
        public string[] TimeFormat(string time)
        {
            string[] Time = time.Split(':');
            return Time;
        }
        public CarPickUpCountryListResponse GetAllCountry(CarPickUpCountryListRequest pobjPickUpCountryListRQ)
        {
            try
            {
                CarPickUpCountryListResponse lobjCarPickUpCountryListResponse = new CarPickUpCountryListResponse();
                IBECarModel lobjModel = new IBECarModel();
                lobjCarPickUpCountryListResponse = lobjModel.CarPickUpCountryListResponse(pobjPickUpCountryListRQ);
                return lobjCarPickUpCountryListResponse;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllCountry -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
        }

        public CarPickUpCityListResponce GetAllCities(string countryname)
        {
            CarPickUpCityListRequest lobjCarPickUpCityListRequest = new CarPickUpCityListRequest();
            CarPickUpCityListResponce lobjCarPickUpCityListResponce = new CarPickUpCityListResponce();
            lobjCarPickUpCityListRequest.PickUpCityListRequest.Country = countryname;
            IBECarModel lobjModel = new IBECarModel();
            lobjCarPickUpCityListResponce = lobjModel.CarPickUpCityListResponse(lobjCarPickUpCityListRequest);
            return lobjCarPickUpCityListResponce;
        }
        public CarRentalTermsResponse CarRentalTermsResponse(CarRentalTermsRequest lobjCarRentalTermsRequest)
        {
            CarRentalTermsResponse lobjCarRentalTermsResponse = new CarRentalTermsResponse();
            IBECarModel lobjModel = new IBECarModel();
            lobjCarRentalTermsResponse = lobjModel.CarRentalTermsResponse(lobjCarRentalTermsRequest);
            return lobjCarRentalTermsResponse;
        }

        public CarPickUpOpenTimeResponce CarPickUpOpenTimeResponce(string Pickupdate, string LocationId)
        {
            CarPickUpOpenTimeResponce lobjCarPickUpOpenTimeResponce = new CarPickUpOpenTimeResponce();
            CarPickUpOpenTimeRequest pstrPickUpOpenTimeRQ = new CarPickUpOpenTimeRequest();
            string[] Date = DateFormat(Pickupdate);
            List<Date> lobjDateList = new List<Date>();
            Date lobjDate = new Date();
            lobjDate.day = Date[0]; ;
            lobjDate.month = Date[1];
            lobjDate.year = Date[2];
            lobjDateList.Add(lobjDate);
            List<CB.IBE.Platform.Car.Entities.Location> lobjListOfLocation = new List<CB.IBE.Platform.Car.Entities.Location>();
            CB.IBE.Platform.Car.Entities.Location lobjLocation = new CB.IBE.Platform.Car.Entities.Location();
            lobjLocation.id = LocationId;
            lobjListOfLocation.Add(lobjLocation);
            pstrPickUpOpenTimeRQ.PickUpOpenTimeRequest.Location = lobjListOfLocation.ToArray();
            pstrPickUpOpenTimeRQ.PickUpOpenTimeRequest.Date = lobjDateList.ToArray();
            IBECarModel lobjModel = new IBECarModel();
            lobjCarPickUpOpenTimeResponce = lobjModel.CarPickUpOpenTimeResponce(pstrPickUpOpenTimeRQ);
            return lobjCarPickUpOpenTimeResponce;
        }

        public CarDropOffOpenTimeResponce CarDropOffOpenTimeResponce(string DropOffDate, string LocationId)
        {
            CarDropOffOpenTimeResponce CarDropOffOpenTimeResponce = new CarDropOffOpenTimeResponce();
            CarDropOffOpenTimeRequest CarDropOffOpenTimeRequest = new CarDropOffOpenTimeRequest();
            string[] Date = DateFormat(DropOffDate);
            List<Date> lobjDateList = new List<Date>();
            Date lobjDate = new Date();
            lobjDate.day = Date[0];
            lobjDate.month = Date[1];
            lobjDate.year = Date[2];
            lobjDateList.Add(lobjDate);
            List<CB.IBE.Platform.Car.Entities.Location> lobjListOfLocation = new List<CB.IBE.Platform.Car.Entities.Location>();
            CB.IBE.Platform.Car.Entities.Location lobjLocation = new CB.IBE.Platform.Car.Entities.Location();
            lobjLocation.id = LocationId;
            lobjListOfLocation.Add(lobjLocation);
            CarDropOffOpenTimeRequest.DropOffOpenTimeRequest.Location = lobjListOfLocation.ToArray();
            CarDropOffOpenTimeRequest.DropOffOpenTimeRequest.Date = lobjDateList.ToArray();
            IBECarModel lobjModel = new IBECarModel();
            CarDropOffOpenTimeResponce = lobjModel.CarDropOffOpenTimeResponce(CarDropOffOpenTimeRequest);
            return CarDropOffOpenTimeResponce;
        }

        public Match GetCarVehiclDetails(List<Match> list, string pstrref)
        {
            return list.ToList<Match>().Find(lobjMatch => lobjMatch.Vehicle[0].id.Equals(pstrref));
        }

        public CarExtrasListResponse CarExtrasListResponse(CarExtrasListRequest pobjCarExtrasListRequest, CarSearchRequest pobjCarSearchRequest)
        {
            IBECarModel lobjModel = new IBECarModel();
            return lobjModel.CarExtrasListResponse(pobjCarExtrasListRequest, pobjCarSearchRequest);
        }

        //public RefererData GetRefererData()
        //{
        //    MasterFacade lojFacade = new MasterFacade();
        //    return lojFacade.GetRefererData();
        //}
        //public ProgramMaster GetProgramDetails(string mstrProgramName)
        //{
        //    MasterFacade lojFacade = new MasterFacade();
        //    return lojFacade.GetProgramDetails(mstrProgramName);
        //    //RuleEngineHelper lobjHelper = new RuleEngineHelper();
        //    //return lobjHelper.ExecuteRuleSet<ProgramMaster>(new ExecuteRequest("GetProgramDetails", "GetProgramDetails", new List<object>(0) { mstrProgramName }));
        //}

        //public SystemParameter GetSystemParametres(int pintProgramId)
        //{
        //    MasterFacade lojFacade = new MasterFacade();
        //    return lojFacade.GetSystemParameter(pintProgramId);
        //    //RuleEngineHelper lobjHelper = new RuleEngineHelper();
        //    //return lobjHelper.ExecuteRuleSet<SystemParameter>(new ExecuteRequest("SystemParameters", "SystemParameters", new List<object>(0) { pintProgramId }));
        //}


        //public CarMakeBookingResponse BookForCar(CarMakeBookingRequest pobjCarMakeBookingRequest, string pstrRecipt, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, MemberDetails pobjMemberDetails)
        //{
        //    CarMakeBookingResponse lobjCarMakeBookingResponse = new CarMakeBookingResponse();
        //    CarBookingIntegrationFacade lobjCarBookingIntegrationFacade = new CarBookingIntegrationFacade();
        //    //lobjCarMakeBookingResponse = lobjCarBookingIntegrationFacade.BookCar(pobjCarMakeBookingRequest, pstrRecipt, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjMemberDetails);
        //    lobjCarMakeBookingResponse = lobjCarBookingIntegrationFacade.BookCar(pobjCarMakeBookingRequest, pstrRecipt, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjMemberDetails);
        //    return lobjCarMakeBookingResponse;
        //}


        public CarSearchResponse CarSearchResponse(CarSearchRequest pstrSearchRQ)
        {
            IBECarModel lobjIBECarModel = new IBECarModel();
            return lobjIBECarModel.CarSearchResponse(pstrSearchRQ);
        }



        //public CarMakeBookingResponse BookForCar(CarMakeBookingRequest pobjCarMakeBookingRequest, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        //{
        //    //CarMakeBookingResponse lobjCarMakeBookingResponse = new CarMakeBookingResponse();
        //    //BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
        //    //lobjCarMakeBookingResponse = lobjBookingIntegrationFacade.BookCar(pobjCarMakeBookingRequest, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjMemberDetails, pobjListOfRedemptionDetails);
        //    //return lobjCarMakeBookingResponse;

        //    RuleEngineHelper lobjHelper = new RuleEngineHelper();
        //    return lobjHelper.ExecuteRuleSet<CarMakeBookingResponse>(new ExecuteRequest("BookCar", "BookCar", new List<object>(0) { pobjCarMakeBookingRequest, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjMemberDetails, pobjListOfRedemptionDetails }));
        //}

        public CarMakeBookingResponse BookForCar(CarMakeBookingRequest pobjCarMakeBookingRequest, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            CarMakeBookingResponse lobjCarMakeBookingResponse = new CarMakeBookingResponse();
            BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
            lobjCarMakeBookingResponse = lobjBookingIntegrationFacade.BookCar(pobjCarMakeBookingRequest, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjMemberDetails, pobjListOfRedemptionDetails);
            return lobjCarMakeBookingResponse;

            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<CarMakeBookingResponse>(new ExecuteRequest("BookCar", "BookCar", new List<object>(0) { pobjCarMakeBookingRequest, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjMemberDetails, pobjListOfRedemptionDetails }));
        }

        public CarMakeBookingResponse BookCarByInfiPay(CarMakeBookingRequest pobjCarMakeBookingRequest, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            CarMakeBookingResponse lobjCarMakeBookingResponse = new CarMakeBookingResponse();
            BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
            lobjCarMakeBookingResponse = lobjBookingIntegrationFacade.BookCarByInfiPay(pobjCarMakeBookingRequest, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjMemberDetails, pobjListOfRedemptionDetails);
            return lobjCarMakeBookingResponse;
        }



        public string CalculatePoints(string pstrAmount)
        {
            int lintPoints = 0;

            ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
            lobjProgramDefinition = GetProgramMaster();

            //ProgramMaster lobjProgramMaster = new ProgramMaster();
            //lobjProgramDefinition = HttpContext.Current.Application["ProgramMaster"] as ProgramDefinition;
            //if (lobjProgramDefinition != null)
            //{
            //    lintPoints = Convert.ToInt32(Math.Ceiling((Convert.ToSingle(pstrAmount) / lobjProgramDefinition.lobjProgramAttribute[0].RedemptionRate)));
            //}
            return Convert.ToString(lintPoints);

        }
        public List<CarBookingDetails> GetCarBookingDetails(string pstrMemberId, int pstrRefererId)
        {
            try
            {
                IBERCCarClient lobjClient = new IBERCCarClient();
                return lobjClient.GetMemberBookedCarList(pstrMemberId, pstrRefererId);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetCarBookingDetails -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
        }
        public CarBookingDetails GetCarInfoDetailsByBookingRefId(string pstrBookingRefId)
        {
            try
            {
                IBECarModel lobjIBECarModel = new IBECarModel();
                return lobjIBECarModel.GetCarInfoDetailsByBookingRefId(pstrBookingRefId);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetBookedHotelInfo -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
        }

        public RefererDetails GetRefererDetailsWithSupplier(RefererDetails lobjRefererDetails)
        {
            try
            {
                IBECTClient lobjIBECTClient = new IBECTClient();
                return lobjIBECTClient.GetRefererDetailsWithSupplier(lobjRefererDetails);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetRefererDetailsWithSupplier -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
        }
        public List<AirCraftDetails> GetAllAirCraftDetails()
        {
            try
            {
                IBECTClient lobjIBECTClient = new IBECTClient();
                return lobjIBECTClient.GetAllAirCraftDetails();
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllAirCraftDetails -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
        }
        public MerchantDetails GetMerchantDetails(MerchantDetails pobjMerchantDetails)
        {
            try
            {
                //MerchantFacade lobjMerchantFacade = new MerchantFacade();
                //return lobjMerchantFacade.GetMerchantDetailsByMerchantCode(pobjMerchantDetails);

                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<MerchantDetails>(new ExecuteRequest("GetMerchantDetailsByMerchantCode", "GetMerchantDetailsByMerchantCode", new List<object>(0) { pobjMerchantDetails }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetMerchantDetailsByMerchantCode - InfiModel - " + ex.Message);
                return null;
            }
        }
        public Core.Platform.PointGateway.Service.Helper.PGService.Response CheckAvailability(string pstrRelationReference, int pintProgramId, int pintRelationType, string pstrTransactionCurrency)
        {
            Core.Platform.PointGateway.Service.Helper.PGService.Response lobjResponse = new Core.Platform.PointGateway.Service.Helper.PGService.Response();
            PGFacade lobjPGFacade = new PGFacade();
            //lobjResponse=lobjPGFacade.CheckAvailability(pstrRelationReference, pintProgramId, pintRelationType, pstrTransactionCurrency);
            return lobjResponse;
        }

        public float GetProgramRedemptionRate(string pstrProgramCurrency, string pstrRedemtionCode, int pintProgramId)
        {
            float lftRedemptionRate = 0.0f;
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            try
            {
                string lstrKey = "RedemptionRate" + pstrProgramCurrency + pstrRedemtionCode;
                if (HttpContext.Current.Application[lstrKey] != null)
                {
                    lftRedemptionRate = Convert.ToSingle(HttpContext.Current.Application[lstrKey]);
                }
                else
                {
                    lftRedemptionRate = lobjInterfaceHelper.GetProgramRedemptionRate(pstrProgramCurrency, pstrRedemtionCode, pintProgramId);
                    HttpContext.Current.Application[lstrKey] = lftRedemptionRate;
                }
            }
            catch (Exception ex)
            {

                LoggingAdapter.WriteLog("GetProgramRedemptionRate :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
            }

            return lftRedemptionRate;
        }
        public MemberDetails GetMemberDetailsUsingNationalID(string pstrNationalId)
        {
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = GetProgramMaster();
            if (lobjProgramDefinition != null)
            {
                ActivationParameters lobjActivationParameters = new ActivationParameters();
                lobjActivationParameters.NationalId = pstrNationalId;
                lobjActivationParameters.ProgramId = lobjProgramDefinition.ProgramId;
                lobjActivationParameters.RelationType = Convert.ToInt32(RelationType.LBMS);
                //ActivationFacade lobjFacade = new ActivationFacade();
                //return lobjFacade.GetMemberDetailsUsingNationalID(lobjActivationParameters);

                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<MemberDetails>(new ExecuteRequest("GetMemberDetailsUsingNationalID", "GetMemberDetailsUsingNationalID", new List<object>(0) { lobjActivationParameters }));
            }
            else
            {
                LoggingAdapter.WriteLog("GetMemberDetailsUsingNationalID - Program null");
                return null;
            }
        }
        public MemberDetails SignUpUser(string pstrMemberid, string lstrEncryptPassword)
        {
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = GetProgramMaster();
            if (lobjProgramDefinition != null)
            {
                LoginDetails lobjLoginDetails = new LoginDetails();
                lobjLoginDetails.UserName = pstrMemberid;
                lobjLoginDetails.Password = lstrEncryptPassword;
                lobjLoginDetails.ProgramId = lobjProgramDefinition.ProgramId;
                lobjLoginDetails.RelationType = Convert.ToInt32(RelationType.LBMS);

                //MemberBusinessFacade lobjFacade = new MemberBusinessFacade();
                //return lobjFacade.SignInUser(lobjLoginDetails);


                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<MemberDetails>(new ExecuteRequest("LoginUser", "SignInUser", new List<object>(0) { lobjLoginDetails }));
            }
            else
            {
                LoggingAdapter.WriteLog("SignUpUser model - program null");
                return null;
            }
        }
        public string IntToThousandSeperated(int pintValue)
        {
            NumberFormatInfo nfo = new NumberFormatInfo();
            nfo.CurrencyGroupSeparator = ",";
            nfo.CurrencyGroupSizes = new int[] { 3, 2 };
            nfo.CurrencySymbol = "";

            return pintValue.ToString("c0", nfo);
        }
        public List<RedemptionKeys> GetAllRedemptionKeys(int pintProgramId)
        {
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            List<RedemptionKeys> lobjListRedemptionKeys = null;
            try
            {
                if (HttpContext.Current.Application["RedemptionKeys"] != null)
                {
                    lobjListRedemptionKeys = HttpContext.Current.Application["RedemptionKeys"] as List<RedemptionKeys>;
                }
                else
                {
                    lobjListRedemptionKeys = lobjInterfaceHelper.GetRedemptionKeyslst(pintProgramId);
                    HttpContext.Current.Application["RedemptionKeys"] = lobjListRedemptionKeys;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllRedemptionKeys :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
            }

            return lobjListRedemptionKeys;
        }
        public string CurrencyDisplayText(string pstrCurrency)
        {
            //string lstrCurrency = string.Empty;
            //try
            //{
            //    ProgramDefinition lobjProgramDefinition = GetProgramMaster();
            //    List<CustomerSegment> lobjListOfCustomerSegment = new List<CustomerSegment>();
            //    lobjListOfCustomerSegment = GetAllCustomerSegmentlst(lobjProgramDefinition.ProgramId);
            //    CustomerSegment lobjCustomerSegment = new CustomerSegment();
            //    lstrCurrency = lobjListOfCustomerSegment.Find(lobj => lobj.Currency.Equals(pstrCurrency)).Description;
            //    return lstrCurrency;
            //}
            //catch (Exception ex)
            //{
            //    LoggingAdapter.WriteLog("CurrencyDisplayText-" + ex.Message + ex.StackTrace);
            //    return lstrCurrency;
            //}

            return "REWARDZ";
        }
        public List<CustomerSegment> GetAllCustomerSegmentlst(int pintProgramId)
        {
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            List<CustomerSegment> lobjListCustomerSegment = null;
            try
            {
                if (HttpContext.Current.Application["CustomerSegment"] != null)
                {
                    lobjListCustomerSegment = HttpContext.Current.Application["CustomerSegment"] as List<CustomerSegment>;
                }
                else
                {
                    lobjListCustomerSegment = lobjInterfaceHelper.GetAllCustomerSegmentlst(pintProgramId);
                    HttpContext.Current.Application["CustomerSegment"] = lobjListCustomerSegment;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllCustomerSegmentlst :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
            }

            return lobjListCustomerSegment;
        }
        public bool GenerateReviewnConfirmOTP(OTPDetails pobjOTPDetails, MemberDetails pobjMemberDetails)
        {
            try
            {
                ProgramDefinition lobjProgramDefinition = null;
                lobjProgramDefinition = GetProgramMaster();
                SystemParameter lobjSystemParameter = null;
                lobjSystemParameter = GetSystemParametres(lobjProgramDefinition.ProgramId);
                if (lobjProgramDefinition != null)
                {
                    OTPDetails lobjOTPDetails = new OTPDetails();
                    lobjOTPDetails.UniquerefID = pobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
                    lobjOTPDetails.SourceAddress = HttpContext.Current.Request.UserHostAddress;
                    lobjOTPDetails.SourceCode = SourceCode.Web;
                    lobjOTPDetails.ProgramId = lobjProgramDefinition.ProgramId;
                    lobjOTPDetails.RelationType = Convert.ToInt32(RelationType.LBMS);
                    lobjOTPDetails.OtpType = Convert.ToString(pobjOTPDetails.OtpEnumTypes);
                    lobjOTPDetails.OtpEnumTypes = pobjOTPDetails.OtpEnumTypes;
                    lobjOTPDetails.AddExpirationTimeInMinutes = Convert.ToString(lobjSystemParameter.OTPExpirationTime);
                    //bool status = false;
                    //BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
                    //status = lobjBookingIntegrationFacade.GenerateReviewnConfirmOTP(lobjOTPDetails, pobjMemberDetails);
                    //return status;
                    RuleEngineHelper lobjHelper = new RuleEngineHelper();
                    return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("GenerateReviewnConfirmOTP", "GenerateReviewnConfirmOTP", new List<object>(0) { lobjOTPDetails, pobjMemberDetails }));
                }
                else
                {
                    LoggingAdapter.WriteLog("MODEL GenerateReviewnConfirmOTP (Activation Facade) program Detail Invalid");
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GenerateReviewnConfirmOTP -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return false;
            }
        }

        public bool CheckWhetherOTPExists(OTPDetails pobjOTPDetails)
        {
            try
            {

                pobjOTPDetails.DestinationAddress = HttpContext.Current.Request.UserHostAddress;
                pobjOTPDetails.Destination = SourceCode.Web.ToString();

                //ActivationFacade lobjFacade = new ActivationFacade();
                //return lobjFacade.CheckWhetherOTPExists(pobjOTPDetails);

                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("CheckWhetherOTPExists", "CheckWhetherOTPExists", new List<object>(0) { pobjOTPDetails }));
            }

            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("BookForCar -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return false;
            }
        }

        public bool GenerateOTPForActivation(string pstrMemberid, string pstrSourceIpAddress, int pintProgramId)
        {
            SystemParameter lobjSystemParameter = null;
            lobjSystemParameter = GetSystemParametres(pintProgramId);
            if (lobjSystemParameter != null)
            {
                OTPDetails lobjOTPDetails = new OTPDetails();
                lobjOTPDetails.UniquerefID = pstrMemberid;
                lobjOTPDetails.SourceAddress = pstrSourceIpAddress;
                lobjOTPDetails.SourceCode = Core.Platform.OTP.ConfigurationConstants.SourceCode.Web;
                lobjOTPDetails.ProgramId = pintProgramId;
                lobjOTPDetails.RelationType = Convert.ToInt32(RelationType.LBMS);
                //lobjOTPDetails.UniqueNationlalID = pstrNationalId;
                lobjOTPDetails.OtpType = OTPEnumTypes.ACTIVATION.ToString();
                lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.ACTIVATION;
                lobjOTPDetails.AddExpirationTimeInMinutes = Convert.ToString(lobjSystemParameter.OTPExpirationTime);
                //ActivationFacade lobjFacade = new ActivationFacade();
                //return lobjFacade.GenerateOTPDetails(lobjOTPDetails);

                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("GenerateOTP", "GenerateOTP", new List<object>(0) { lobjOTPDetails }));
            }
            else
            {
                LoggingAdapter.WriteLog("MODEL GenerateOTP (Activation Facade) program Detail Invalid");
                return false;
            }
        }

        public bool CheckOTPExist(OTPDetails lobjDetails)
        {
            OTPFacade lobjFacade = new OTPFacade();
            return lobjFacade.UpdateOTP(lobjDetails);
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("CheckOTPExist", "CheckOTPExist", new List<object>(0) { lobjDetails }));
        }
        public string GetMemberCurrency(MemberDetails pobjMemberDetails)
        {
            string lstrCurrency = string.Empty;
            ProgramDefinition lobjProgramDefinition = GetProgramMaster();

            if (pobjMemberDetails != null)
            {
                List<CustomerSegment> lobjListOfCustomerSegment = new List<CustomerSegment>();
                lobjListOfCustomerSegment = GetAllCustomerSegmentlst(lobjProgramDefinition.ProgramId);
                CustomerSegment lobjCustomerSegment = new CustomerSegment();
                lobjCustomerSegment = lobjListOfCustomerSegment.Find(lobj => lobj.SegmentCode.Equals(pobjMemberDetails.CustomerSegment));
                lstrCurrency = lobjCustomerSegment.Currency;
            }
            else
            {
                if (HttpContext.Current.Session["DefaultCurrency"] != null)
                {
                    lstrCurrency = HttpContext.Current.Session["DefaultCurrency"] as string;
                }
                else
                {
                    List<ProgramCurrencyDefinition> lobjListOfProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
                    lobjListOfProgramCurrencyDefinition = GetProgramCurrencyDefinition(lobjProgramDefinition.ProgramId);
                    lstrCurrency = lobjListOfProgramCurrencyDefinition.Find(lobj => lobj.IsDefault.Equals(true)).Currency;
                    HttpContext.Current.Session["DefaultCurrency"] = lstrCurrency;
                }
            }
            return lstrCurrency;
        }

        public string FloatToThousandSeperated(float pfltValue)
        {
            //LoggingAdapter.WriteLog("FloatToThousandSeperated IP-" + pfltValue);
            //string lstrstring = string.Empty;
            //NumberFormatInfo nfo = new NumberFormatInfo();
            //nfo.CurrencyGroupSeparator = ",";
            //nfo.CurrencyGroupSizes = new int[] { 3, 2 };
            //nfo.CurrencySymbol = "";

            //lstrstring = pfltValue.ToString("c0", nfo);
            //LoggingAdapter.WriteLog("FloatToThousandSeperated OP-" + lstrstring);
            //return lstrstring;

            string[] lstrArray = null;
            string pstrString = string.Empty;
            NumberFormatInfo nfo = new CultureInfo("en-US", false).NumberFormat;
            pstrString = pfltValue.ToString("N", nfo);
            lstrArray = pstrString.Split('.');
            return lstrArray[0];
        }


        public bool InsertRedemptionAuditTrail(AuditTrailForRedemption pobjAuditTrailForRedemption)
        {
            //AuditTrailFacade lobjAuditTrailFacade = new AuditTrailFacade();
            //return lobjAuditTrailFacade.InsertRedemptionAuditTrail(pobjAuditTrailForRedemption);x
            RuleEngineHelper lobjHelper = new RuleEngineHelper();
            return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("InsertRedemptionAuditTrail", "InsertRedemptionAuditTrail", new List<object>(0) { pobjAuditTrailForRedemption }));
        }

        public MemberDetails GetMemberActivationDetails(SearchMember pobjSearchMember)
        {
            try
            {
                //MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                //MemberDetails lobjMemberDetails = null;
                //lobjMemberDetails = lobjMemberBusinessFacade.GetActivationMemberDetails(pobjSearchMember);
                //return lobjMemberDetails;
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<MemberDetails>(new ExecuteRequest("GetMemberActivationDetails", "GetMemberActivationDetails", new List<object>(0) { pobjSearchMember }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetMemberActivationDetails -  InfiModel - " + ex.Message);

                return null;
            }
        }


        public bool IsLoginRelationAlreadyExist(SearchMember pobjSearchMember)
        {
            //MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            //return lobjMemberBusinessFacade.IsLoginRelationAlreadyExist(lobjSearchMember);
            try
            {
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("IsLoginRelationAlreadyExist", "IsLoginRelationAlreadyExist", new List<object>(0) { pobjSearchMember }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("IsLoginRelationAlreadyExist -  InfiModel - " + ex.Message);

                return false;
            }
        }

        public bool LoginRelationActivation(MemberDetails pobjMemberDetails)
        {
            //ActivationFacade lobjActivationFacade = new ActivationFacade();
            //return lobjActivationFacade.LoginRelationActivation(lobjMemberDetails);
            try
            {
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("LoginRelationActivation", "LoginRelationActivation", new List<object>(0) { pobjMemberDetails }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("LoginRelationActivation -  InfiModel - " + ex.Message);

                return false;
            }
        }

        public MemberDetails GetEnrollMemberDetails(SearchMember pobjSearchMember)
        {
            //MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            //MemberDetails lobjMemberDetails = null;
            //lobjMemberDetails = lobjMemberBusinessFacade.GetEnrollMemberDetails(pobjSearchMember);
            //return lobjMemberDetails;
            try
            {
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<MemberDetails>(new ExecuteRequest("GetEnrollMemberDetails", "GetEnrollMemberDetails", new List<object>(0) { pobjSearchMember }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetEnrollMemberDetails - InfiModel : " + ex.Message);
                return null;
            }
        }
        //// RULE ENGINE PEND////////
        public MemberDetails CheckLoginRelationMembershipCrediantials(string pstrLogin, string pstrPassword, int pintProgramId)
        {
            LoginDetails lobjLoginDetails = new LoginDetails();
            lobjLoginDetails.UserName = pstrLogin;
            lobjLoginDetails.Password = pstrPassword;
            lobjLoginDetails.ProgramId = pintProgramId;
            lobjLoginDetails.RelationType = Convert.ToInt32(RelationType.LBMS);
            MemberBusinessFacade lobjfacade = new MemberBusinessFacade();
            return lobjfacade.CheckLoginRelationMembershipCrediantials(lobjLoginDetails);
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<MemberDetails>(new ExecuteRequest("CheckMembershipCrediantials", "CheckMembershipCrediantials", new List<object>(0) { lobjLoginDetails, null }));

        }
        public ScheduleExpiry GetExpirySchedule(int pintYear)
        {
            try
            {
                ExpiryFacade lobjExpiryFacade = new ExpiryFacade();
                return lobjExpiryFacade.GetExpirySchedule(pintYear);
                //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                //return lobjHelper.ExecuteRuleSet<ScheduleExpiry>(new ExecuteRequest("GetExpirySchedule", "GetExpirySchedule", new List<object>(0) { pintYear }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetExpirySchedule -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }

        }
        public ScheduleExpiry GetTransactionExpirySchedule(ScheduleExpiry lobjScheduleExpiry)
        {
            try
            {
                ExpiryFacade lobjExpiryFacade = new ExpiryFacade();
                return lobjExpiryFacade.GetTransactionExpirySchedule(lobjScheduleExpiry);
                //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                //return lobjHelper.ExecuteRuleSet<ScheduleExpiry>(new ExecuteRequest("GetTransactionExpirySchedule", "GetTransactionExpirySchedule", new List<object>(0) { lobjScheduleExpiry }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetTransactionExpirySchedule -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }

        }
        public ScheduleExpiry GetNextExpirySchedule(ScheduleExpiry lobjScheduleExpiry, int pintYear)
        {
            try
            {
                //ExpiryFacade lobjExpiryFacade = new ExpiryFacade();
                //return lobjExpiryFacade.GetNextYearExpirySchedule(lobjScheduleExpiry, pintYear);
                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<ScheduleExpiry>(new ExecuteRequest("GetNextExpirySchedule", "GetNextExpirySchedule", new List<object>(0) { lobjScheduleExpiry, pintYear }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetTransactionExpirySchedule -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }

        }

        public ScheduleExpiry GetNextExpiredPointsOnDate(ScheduleExpiry lobjScheduleExpiry, int pintYear)
        {
            try
            {

                ExpiryFacade lobjExpiryFacade = new ExpiryFacade();
                return lobjExpiryFacade.GetNextExpiredPointsOnDate(lobjScheduleExpiry, pintYear);
                //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                //return lobjHelper.ExecuteRuleSet<ScheduleExpiry>(new ExecuteRequest("GetNextExpiredPointsOnDate", "GetNextExpiredPointsOnDate", new List<object>(0) { lobjScheduleExpiry, pintYear }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetTransactionExpirySchedule -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }

        }

        public List<VoucherSummary> CreateVoucher(MemberDetails pobjMemberDetails, IBEAccountTransactionDetails pobjIBEAccountTransactionDetails, List<VoucherDetails> pobjListVoucherDetails, string pstrCurrency)
        {
            List<VoucherSummary> lobjVoucherSummary = new List<VoucherSummary>();
            BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
            List<string> lstrVoucherNoList = new List<string>();
            try
            {
                lobjVoucherSummary = lobjBookingIntegrationFacade.CreateVoucher(pobjMemberDetails, pobjIBEAccountTransactionDetails, pobjListVoucherDetails, pstrCurrency);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CBQModel CreateVoucher : " + Environment.NewLine + ex.StackTrace);
            }
            return lobjVoucherSummary;
        }
        public List<ProgramDefinition> GetProgramDefinitionList()
        {
            List<ProgramDefinition> lobjListProgramDefinition = null;
            try
            {
                InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();

                lobjListProgramDefinition = lobjInterfaceHelper.GetProgramDefinitionList();
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetProgramDefinitionList - " + ex.Message + Environment.NewLine + "Stack Trace - " + ex.StackTrace);

            }
            return lobjListProgramDefinition;
        }

        public bool GenerateOTPForRegistration(string pstrMemberid, string pstrSourceIpAddress, int pstrProgramID)
        {
            bool lblnStatus = false;
            SystemParameter lobjSystemParameter = null;
            lobjSystemParameter = GetSystemParametres(pstrProgramID);
            try
            {
                if (lobjSystemParameter != null)
                {
                    OTPDetails lobjOTPDetails = new OTPDetails();
                    lobjOTPDetails.UniquerefID = pstrMemberid;
                    lobjOTPDetails.SourceAddress = pstrSourceIpAddress;
                    lobjOTPDetails.SourceCode = Core.Platform.OTP.ConfigurationConstants.SourceCode.Web;
                    lobjOTPDetails.ProgramId = pstrProgramID;
                    lobjOTPDetails.RelationType = Convert.ToInt32(RelationType.LBMS);
                    lobjOTPDetails.OtpType = Convert.ToString(OTPEnumTypes.ACTIVATION);
                    lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.ACTIVATION;
                    lobjOTPDetails.AddExpirationTimeInMinutes = Convert.ToString(lobjSystemParameter.OTPExpirationTime);

                    BusinessFacade lobjFacade = new BusinessFacade();
                    return lobjFacade.GenerateOTPForRegistration(lobjOTPDetails);
                    //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                    //return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("GenerateOTPForRegistration", "GenerateOTPForRegistration", new List<object>(0) { lobjOTPDetails }));

                }
                else
                {
                    LoggingAdapter.WriteLog("MODEL GenerateOTPForRegister program Detail Invalid");
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("MODEL GenerateOTPForRegister " + ex.Message + "StackTrace" + ex.StackTrace);
                return false;
            }
        }

        public bool VerifyOTPForRegistration(string pstrMemberid, string pstrOTP)
        {
            bool lblnStatus = false;

            OTPDetails lobjOTPDetails = new OTPDetails();
            lobjOTPDetails.UniquerefID = pstrMemberid;
            lobjOTPDetails.OTP = Convert.ToInt32(pstrOTP);
            lobjOTPDetails.RelationType = Convert.ToInt32(RelationType.LBMS);
            lobjOTPDetails.OtpType = Convert.ToString(OTPEnumTypes.ACTIVATION);
            lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.ACTIVATION;

            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("VerifyOTPForRegistration", "VerifyOTPForRegistration", new List<object>(0) { lobjOTPDetails }));
            BusinessFacade lobjBusinessFacade = new BusinessFacade();
            return lobjBusinessFacade.VerifyOTPForRegistration(lobjOTPDetails);
        }
        public MemberDetails GetMemberDetails(SearchMember pobjSearchMember)
        {
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<MemberDetails>(new ExecuteRequest("GetmemberDetails", "GetmemberDetails", new List<object>(0) { pobjSearchMember }));
            MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            return lobjMemberBusinessFacade.GetmemberDetails(pobjSearchMember);
        }

        public bool CheckMemberLoginDetails(SearchMember lobjSearchMemberForLogin)
        {
            BusinessFacade lobjBusinessFacade = new BusinessFacade();
            return lobjBusinessFacade.CheckMemberLoginDetails(lobjSearchMemberForLogin);
        }

        public VoucherClaimResponse VoucherClaim(MemberDetails lobjMemberDetails, string pstrVoucherNo)
        {
            VoucherClaimRequest lobjVoucherClaimRequest = new VoucherClaimRequest();
            VoucherClaimResponse lobjVoucherClaimResponse = new VoucherClaimResponse();
            MemberRelation lobjMemberRelation = new MemberRelation();
            lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS));
            lobjVoucherClaimRequest.InfiPlanetId = lobjMemberRelation.RelationReference;
            lobjVoucherClaimRequest.VoucherNo = pstrVoucherNo;

            InfiPlanetVoucherClaimServiceHelper lobjInfiPlanetVoucherClaimServiceHelper = new InfiPlanetVoucherClaimServiceHelper();
            lobjVoucherClaimResponse = lobjInfiPlanetVoucherClaimServiceHelper.InfiPlanetVoucherClaim(lobjVoucherClaimRequest);
            return lobjVoucherClaimResponse;
        }

        public bool CreateMemberRelation(MemberDetails pobjVoucherMemberDetails)
        {
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("CreateMemberRelation", "CreateMemberRelation", new List<object>(0) { pobjVoucherMemberDetails }));
            MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            return lobjMemberBusinessFacade.CreateMemberRelation(pobjVoucherMemberDetails);
        }

        public bool CreateVoucherTransaction(TransactionDetails pobjTransactionDetails)
        {
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("InsertTransactionDetails", "InsertTransactionDetails", new List<object>(0) { pobjTransactionDetails }));
            TransactionBusinessFacade lobjTransactionBusinessFacade = new TransactionBusinessFacade();
            return lobjTransactionBusinessFacade.InsertTransactionDetails(pobjTransactionDetails);

        }

        public List<ProgramDefinition> GetInfiplanetProgramList()
        {
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<List<ProgramDefinition>>(new ExecuteRequest("GetInfiplanetProgramList", "GetInfiplanetProgramList", new List<object>(0) {  }));
            BusinessFacade lobjBusinessFacade = new BusinessFacade();
            return lobjBusinessFacade.GetInfiplanetProgramList();
        }

        public ProgramDefinition GetProgramDetailsByProgramName(string pstrProgramName)
        {
            ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            lobjProgramDefinition = lobjInterfaceHelper.GetProgramDefinition(pstrProgramName);
            return lobjProgramDefinition;
        }

        public List<ProgramCurrencyDefinition> GetProgramCurrencyDefinitionByProgramId(int pintProgramId)
        {
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            List<ProgramCurrencyDefinition> lobjListOfProgramCurrencyDefinition = null;
            try
            {

                lobjListOfProgramCurrencyDefinition = lobjInterfaceHelper.GetProgramCurrencyDefinition(pintProgramId);
                HttpContext.Current.Application["ProgramCurrency"] = lobjListOfProgramCurrencyDefinition;

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetProgramCurrencyDefinitionByProgramId :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
            }

            return lobjListOfProgramCurrencyDefinition;
        }

        public ProgramMerchantConfig GetMerchantDetailsByProgramIdAndLoyaltyTxnType(ProgramMerchantConfig pobjProgramMerchantConfig)
        {
            BusinessFacade lobjBusinessFacade = new BusinessFacade();
            return lobjBusinessFacade.GetMerchantDetailsByProgramIdAndLoyaltyTxnType(pobjProgramMerchantConfig);
        }

        public bool IsLoginPrimaryReferenceAlreadyExist(SearchMember pobjSearchMember)
        {
            BusinessFacade lobjBusinessFacade = new BusinessFacade();
            return lobjBusinessFacade.IsLoginPrimaryReferenceAlreadyExist(pobjSearchMember);
        }

        public bool ActivateMemberAccountByPrimaryReference(MemberDetails pobjMemberDetails)
        {
            BusinessFacade lobjBusinessFacade = new BusinessFacade();
            PasswordDefinition lobjPasswordDefinition = new PasswordDefinition();
            pobjMemberDetails.MemberRelationsList[0].WebPassword = RSAEncryptor.EncryptString(Convert.ToString(pobjMemberDetails.MemberRelationsList[0].WebPassword), RSASecurityConstant.KeySize, RSASecurityConstant.RSAPublicKey);

            return lobjBusinessFacade.ActivateMemberAccountByPrimaryReference(pobjMemberDetails);
        }

        public MemberDetails GetEnrollMemberDetailsByPrimaryReference(SearchMember pobjSearchMember)
        {
            BusinessFacade lobjBusinessFacade = new BusinessFacade();
            return lobjBusinessFacade.GetEnrollMemberDetailsByPrimaryReference(pobjSearchMember);
        }

        public ProgramAttribute GetProgramAttributes(int pintProgramId)
        {
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            ProgramAttribute lobjProgramAttribute = new ProgramAttribute();
            try
            {

                lobjProgramAttribute = lobjInterfaceHelper.GetProgramAttributes(pintProgramId);
                HttpContext.Current.Application["ProgramAttribute"] = lobjProgramAttribute;

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetProgramAttributes :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
            }

            return lobjProgramAttribute;
        }

        public List<UtilityCompany> MapUtilityCompanyList(string pstrFilePath)
        {
            FileReader lobjFileReader = new FileReader();
            return lobjFileReader.MapCompanyList(pstrFilePath);
        }

        public List<UtilityAccountType> MapUtilityAccountTypeList(string pstrFilePath)
        {
            FileReader lobjFileReader = new FileReader();
            return lobjFileReader.MapAccountTypeList(pstrFilePath);
        }

        public UtilityInquiryResponse UtilityInquiry(UtilityInquiryRequest pobjUtilityInquiryRequest)
        {
            try
            {
                UtilityPaymentHelper lobjHelper = new UtilityPaymentHelper();
                return lobjHelper.UtilityInquiry(pobjUtilityInquiryRequest);
                //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                //return lobjHelper.ExecuteRuleSet<UtilityInquiryResponse>(new ExecuteRequest("UtilityInquiry", "UtilityInquiry", new List<object>(0) { pobjUtilityInquiryRequest }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Utility Inquiry - Model - " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }

        }

        public UtilityInquiryResponse CharityInquiry(UtilityInquiryRequest pobjUtilityInquiryRequest)
        {
            try
            {
                UtilityPaymentHelper lobjHelper = new UtilityPaymentHelper();
                return lobjHelper.UtilityInquiry(pobjUtilityInquiryRequest);
                //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                //return lobjHelper.ExecuteRuleSet<UtilityInquiryResponse>(new ExecuteRequest("UtilityInquiry", "UtilityInquiry", new List<object>(0) { pobjUtilityInquiryRequest }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Charity Inquiry - Model - " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
        }

        public UtilityPaymentResponse UtilityRedemption(UtilityPaymentRequest pobjUtilityPaymentRequest, MemberDetails pobjMemberDetails, RedemptionDetails pobjRedemptionDetails)
        {
            try
            {
                //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                //return lobjHelper.ExecuteRuleSet<UtilityPaymentResponse>(new ExecuteRequest("UtilityRedemption", "UtilityRedemption", new List<object>(0) { pobjUtilityPaymentRequest, pobjMemberDetails, pobjRedemptionDetails }));
                BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
                return lobjBookingIntegrationFacade.UtilityRedemption(pobjUtilityPaymentRequest, pobjMemberDetails, pobjRedemptionDetails);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Utility - Redemption - Model - " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
            // return lobjBookingIntegrationFacade.UtilityRedemption(pobjUtilityPaymentRequest, pobjMemberDetails, pobjRedemptionDetails);
        }

        public UtilityPaymentResponse CharityRedemption(UtilityPaymentRequest pobjUtilityPaymentRequest, MemberDetails pobjMemberDetails, RedemptionDetails pobjRedemptionDetails)
        {
            try
            {
                BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
                return lobjBookingIntegrationFacade.CharityRedemption(pobjUtilityPaymentRequest, pobjMemberDetails, pobjRedemptionDetails);
                //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                //return lobjHelper.ExecuteRuleSet<UtilityPaymentResponse>(new ExecuteRequest("CharityRedemption", "CharityRedemption", new List<object>(0) { pobjUtilityPaymentRequest, pobjMemberDetails, pobjRedemptionDetails }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Charity Redemption - Model - " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return null;
            }
            // return lobjBookingIntegrationFacade.CharityRedemption(pobjUtilityPaymentRequest, pobjMemberDetails, pobjRedemptionDetails);
        }

        public List<TransactionDetails> GetVoucherSummary(int pintProgramId, string pstrRelationReference, int pintRelationType)
        {
            //VoucherHelper lobjVoucherHelper = new VoucherHelper();
            List<TransactionDetails> lobjTransactionDetails = null;
            try
            {
                TransactionBusinessFacade objTransactionBusinessFacade = new TransactionBusinessFacade();
                return objTransactionBusinessFacade.GetVoucherSummary(pintProgramId, pstrRelationReference, pintRelationType);
                //lobjTransactionDetails = lobjVoucherHelper.GetVoucherSummary(pintProgramId, pstrRelationReference, pintRelationType);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetVoucherSummary :" + ex.Message + Environment.NewLine + "Stack Trace :" + ex.StackTrace);
            }
            return lobjTransactionDetails;
        }

        public bool UpdateAuthenticationTokenStatus(string pstrRelationReference)
        {
            AuthenticationFacade lobjAuthenticationFacade = new AuthenticationFacade();
            AuthenticationDetails lobjAuthenticationDetails = new AuthenticationDetails();
            lobjAuthenticationDetails.MemberId = pstrRelationReference;
            lobjAuthenticationDetails.IsActive = false;
            bool lblnStatus = false;
            lblnStatus = lobjAuthenticationFacade.UpdateAllAuthenticationStatus(lobjAuthenticationDetails);
            return lblnStatus;
        }

        public int InsertAuthenticationDetails(AuthenticationDetails pobjAuthenticationDetails)
        {
            int lintStatus = 0;
            try
            {
                AuthenticationFacade lobjFacade = new AuthenticationFacade();
                return lobjFacade.InsertAuthenticationDetails(pobjAuthenticationDetails);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("InsertAuthenticationDetails exception - " + ex.Message + Environment.NewLine + ex.StackTrace);
                return lintStatus;
            }
        }

        public List<InfiKashTransactionDetails> GetInfiKashBackDetailsByMerchantId(string pstrMerchantId, DateTime pdtFromDate, DateTime pdtToDate)
        {
            try
            {
                InfiKashClientHelper lobjClientHelper = new InfiKashClientHelper();
                return lobjClientHelper.GetInfiKashBackDetailsByMerchantId(pstrMerchantId, pdtFromDate, pdtToDate);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetInfiKashBackDetailsByMerchantId exception - " + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        public List<InfiKashTransactionDetails> GetInfiKashBackDetailsByProgramId(int pintProgramId, DateTime pdtFromDate, DateTime pdtToDate)
        {
            try
            {
                InfiKashClientHelper lobjClientHelper = new InfiKashClientHelper();
                return lobjClientHelper.GetInfiKashBackDetailsByProgramId(pintProgramId, pdtFromDate, pdtToDate);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetInfiKashBackDetailsByProgramId exception - " + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        public List<InfiKashTransactionDetails> GetInfiKashBackDetailsByRelationReference(string pstrRelationReference, int pintProgramId, DateTime pdtFromDate, DateTime pdtToDate)
        {
            try
            {
                InfiKashClientHelper lobjClientHelper = new InfiKashClientHelper();
                return lobjClientHelper.GetInfiKashBackDetailsByRelationReference(pstrRelationReference, pintProgramId, pdtFromDate, pdtToDate);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetInfiKashBackDetailsByRelationReference - RelationReference - " + pstrRelationReference + " |exception - " + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }
        #region Viator
        public ViatorCategoryResponse GetCategoryList(int pintCategoryId)
        {
            ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
            return lobjViatorServiceClient.GetCategoryList(pintCategoryId);
        }
        public List<DestinationDetails> GetDestinationList()
        {
            try
            {
                ViatorDestinationResponse lobjViatorDestinationResponse = new ViatorDestinationResponse();
                List<DestinationDetails> lobjDestinationDetailsList = null;
                if (HttpContext.Current.Application[SessionFields.TourDestinationList] != null)
                {
                    lobjDestinationDetailsList = HttpContext.Current.Application[SessionFields.TourDestinationList] as List<DestinationDetails>;
                }
                else
                {
                    ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
                    lobjViatorDestinationResponse = lobjViatorServiceClient.GetDestinationList();
                    if (lobjViatorDestinationResponse != null && lobjViatorDestinationResponse.DestinationList.Count > 0)
                    {
                        lobjDestinationDetailsList = lobjViatorDestinationResponse.DestinationList;
                        HttpContext.Current.Application[SessionFields.TourDestinationList] = lobjDestinationDetailsList;
                    }

                }
                return lobjDestinationDetailsList;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Infimodel GetDestinationList :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace);
                return null;
            }
        }
        public ViatorProductResponse SearchProduct(ViatorProductRequest pobjViatorProductRequest)
        {
            pobjViatorProductRequest.RefererDetails = GetRefererData();
            ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
            return lobjViatorServiceClient.SearchProduct(pobjViatorProductRequest);
        }
        public ViatorProductDetailsResponse GetProductDetails(ViatorProductRequest pobjViatorProductRequest)
        {
            pobjViatorProductRequest.RefererDetails = GetRefererData();
            ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
            return lobjViatorServiceClient.GetProductDetails(pobjViatorProductRequest);
        }

        public ViatorBookResponse BookingDetails(ViatorBookRequest pobjViatorBookRequest, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            LoggingAdapter.WriteLog("Call Model Booking Details");
            try
            {
                //ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
                //return lobjViatorServiceClient.BookingDetails(pobjViatorBookRequest);
                pobjViatorBookRequest.RefererDetails = GetRefererData();
                BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
                return lobjBookingIntegrationFacade.ViatorBookPackages(pobjViatorBookRequest, pobjMemberDetails, pobjListOfRedemptionDetails);

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("InfiModel BookingDetails :" + ex.Message + ex.StackTrace);
                return null;
            }


        }
        public ViatorBookingAvailabilityResponse GetBookingAvailabilityInfo(ViatorBookingAvailabilityRequest pobjViatorBookingAvailabilityRequest)
        {
            pobjViatorBookingAvailabilityRequest.RefererDetails = GetRefererData();
            ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
            return lobjViatorServiceClient.GetBookingAvailabilityInfo(pobjViatorBookingAvailabilityRequest);
        }
        //public ViatorTourGradeResponse CheckTourGradesAvailability(ViatorTourGradesRequest pobjViatorTourGradesRequest)
        //{
        //    ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
        //    return lobjViatorServiceClient.CheckTourGradesAvailability(pobjViatorTourGradesRequest);
        //}
        public ViatorAvailableTourGradeResponse GetTourGradesAvailability(ViatorProductRequest pobjViatorProductRequest)
        {
            pobjViatorProductRequest.RefererDetails = GetRefererData();
            ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
            return lobjViatorServiceClient.CheckTourGradesAvailability(pobjViatorProductRequest);
        }
        public ViatorHotelResponse GetPickUpHotelList(string pstrProductCode, int pintDestId)
        {
            ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
            return lobjViatorServiceClient.GetPickUpHotelList(pstrProductCode, pintDestId);
        }

        public ViatorPastBookingResponse GetPastBookingResponse(ViatorProductRequest pobjViatorProductRequest)
        {
            pobjViatorProductRequest.RefererDetails = GetRefererData();
            ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
            return lobjViatorServiceClient.GetPastBookingResponse(pobjViatorProductRequest);
        }
        public ViatorBookingCalculatePriceResponse BookingCalculatePrice(ViatorBookingCalculatePriceRequest pobjViatorBookingCalculatePriceRequest)
        {
            pobjViatorBookingCalculatePriceRequest.RefererDetails = GetRefererData();
            ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
            return lobjViatorServiceClient.BookingCalculatePrice(pobjViatorBookingCalculatePriceRequest);
        }
        //public bool SendViatorBookingEmail(string pstrBookingReference, string pstrEmailID)
        //{
        //    try
        //    {
        //        EmailDetails lobjEmailDetail = new EmailDetails();
        //        List<string> lstAttachment = new List<string>();
        //        CEHelper lobjcehelper = new CEHelper();
        //        lobjEmailDetail.TemplateCode = "VIATOR";
        //        lobjEmailDetail.ListParameter = new List<string>();
        //        lobjEmailDetail.ListParameter.Add(pstrBookingReference);
        //        lobjEmailDetail.ProgramId = Convert.ToString("1");
        //        lobjEmailDetail.MemberId = Convert.ToString("");
        //        lobjEmailDetail.AttachmentList = lstAttachment;
        //        lobjEmailDetail.To = pstrEmailID;
        //        lobjcehelper.InsertEmailDetails(lobjEmailDetail);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return true;
        //}
        //public Viator.Platform.MasterEntities.RefererDetails GetViatorRefererData()
        //{
        //    try
        //    {
        //        Viator.Platform.MasterEntities.RefererDetails lobjRefererDetails = new Viator.Platform.MasterEntities.RefererDetails();
        //        lobjRefererDetails.Id = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["RefererId"]);
        //        lobjRefererDetails.UserName = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["RefererName"]);
        //        lobjRefererDetails.Password = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["RefererPassword"]);
        //        return lobjRefererDetails;

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("RefererData -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
        //        return null;
        //    }
        //}
        //public string SearchProductByCode(Viator.Platform.MasterEntities.TravelSearchDetails pobjTravelSearchDetails)
        //{
        //    ViatorClientHelper lobjViatorHelperClient = new ViatorClientHelper();
        //    return lobjViatorHelperClient.SearchProductByCode(pobjTravelSearchDetails);
        //}
        //public string GetProductRviews(Viator.Platform.Entities.SearchDetails pobjSearchDetails)
        //{
        //    ViatorClientHelper lobjViatorHelperClient = new ViatorClientHelper();
        //    return lobjViatorHelperClient.GetProductRviews(pobjSearchDetails);
        //}
        //public string GetProductPhotos(Viator.Platform.Entities.SearchDetails pobjSearchDetails)
        //{
        //    ViatorClientHelper lobjViatorHelperClient = new ViatorClientHelper();
        //    return lobjViatorHelperClient.GetProductPhotos(pobjSearchDetails);
        //}
        //public string GetVoucherDetails(Viator.Platform.Entities.SearchDetails pobjSearchDetails)
        //{
        //    ViatorClientHelper lobjViatorHelperClient = new ViatorClientHelper();
        //    return lobjViatorHelperClient.GetVoucherDetails(pobjSearchDetails);
        //}
        //public string GetMyBookingInfo(Viator.Platform.Entities.SearchDetails pobjSearchDetails)
        //{
        //    ViatorClientHelper lobjViatorHelperClient = new ViatorClientHelper();
        //    return lobjViatorHelperClient.GetMyBookingInfo(pobjSearchDetails);
        //}
        //public string GetBookingStatus(Viator.Platform.Entities.SearchBookingStatus pobjSearchBookingStatus)
        //{
        //    ViatorClientHelper lobjViatorHelperClient = new ViatorClientHelper();
        //    return lobjViatorHelperClient.GetBookingStatus(pobjSearchBookingStatus);
        //}

        //public string CalculateBookingPrice(Viator.Platform.Entities.BookingCalculatePriceRequest pobjBookingCalculatePriceRequest)
        //{
        //    ViatorClientHelper lobjViatorHelperClient = new ViatorClientHelper();
        //    return lobjViatorHelperClient.CalculateBookingPrice(pobjBookingCalculatePriceRequest);
        //}


        //public string PostBookingAvailabilityTourGrades(Viator.Platform.Entities.TourGradesRequest pobjTourGradesRequest)
        //{
        //    ViatorClientHelper lobjViatorHelperClient = new ViatorClientHelper();
        //    return lobjViatorHelperClient.PostBookingAvailabilityTourGrades(pobjTourGradesRequest);
        //}
        //public string GetBookingTourGradesPricingMatrix(Viator.Platform.Entities.TPricingMatrixRequest pobjTPricingMatrixRequest)
        //{
        //    ViatorClientHelper lobjViatorHelperClient = new ViatorClientHelper();
        //    return lobjViatorHelperClient.GetBookingTourGradesPricingMatrix(pobjTPricingMatrixRequest);
        //}

        #endregion


        public List<TravelBookingDetails> GetPackageBookingDetails(string pstrRelationReference)
        {
            try
            {
                LoggingAdapter.WriteLog(" model call GetPackageBookingDetails RelationRef :-" + pstrRelationReference);
                ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
                RefererDetails lobjRefererDetails = HttpContext.Current.Application["RefererData"] as RefererDetails;
                return lobjViatorServiceClient.GetPackageBookingDetails(pstrRelationReference, lobjRefererDetails.Id);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("INFI MODEL GetPackageBookingDetails Exception :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace);
                return null;
            }

        }

        public bool PrimaryReferenceExist(MemberDetails pobjMemberDetails)
        {
            try
            {
                MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                return lobjMemberBusinessFacade.PrimaryReferenceExist(pobjMemberDetails);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Model PrimaryReferenceExist" + ex.Message + ex.StackTrace + ex.InnerException);
                return false;
            }
        }

        public bool GenerateOTPFamilyPool(string pstrMemberid, string pstrSourceIpAddress)
        {
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = GetProgramMaster();
            SystemParameter lobjSystemParameter = null;
            lobjSystemParameter = GetSystemParametres(lobjProgramDefinition.ProgramId);
            try
            {
                if (lobjProgramDefinition != null)
                {
                    OTPDetails lobjOTPDetails = new OTPDetails();
                    lobjOTPDetails.UniquerefID = pstrMemberid;
                    lobjOTPDetails.SourceAddress = pstrSourceIpAddress;
                    lobjOTPDetails.SourceCode = Core.Platform.OTP.ConfigurationConstants.SourceCode.Web;
                    lobjOTPDetails.ProgramId = lobjProgramDefinition.ProgramId;
                    lobjOTPDetails.RelationType = Convert.ToInt32(RelationType.LBMS);
                    lobjOTPDetails.OtpType = Convert.ToString(OTPEnumTypes.FAMILYPOOLINGMERGE);
                    lobjOTPDetails.OtpEnumTypes = OTPEnumTypes.FAMILYPOOLINGMERGE;
                    lobjOTPDetails.AddExpirationTimeInMinutes = Convert.ToString(lobjSystemParameter.OTPExpirationTime);
                    //RuleEngineHelper lobjHelper = new RuleEngineHelper();
                    //return lobjHelper.ExecuteRuleSet<bool>(new ExecuteRequest("GenerateOTP", "GenerateOTP", new List<object>(0) { lobjOTPDetails }));
                    ActivationFacade lobjFacade = new ActivationFacade();
                    return lobjFacade.GenerateOTPDetails(lobjOTPDetails);
                }
                else
                {
                    LoggingAdapter.WriteLog("MODEL GenerateOTPFamilyPool program Detail Invalid");
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("MODEL GenerateOTPFamilyPool " + ex.Message + "StackTrace" + ex.StackTrace);
                return false;
            }
        }

        public int InsertMemberPoolingDetails(MemberPoolingDetails pobjMemberPoolingDetails)
        {
            MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            return lobjMemberBusinessFacade.InsertMemberPoolingDetails(pobjMemberPoolingDetails);
        }

        public int UpdateMemberPoolingDetails(MemberPoolingDetails pobjMemberPoolingDetails)
        {
            MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            return lobjMemberBusinessFacade.UpdateMemberPoolingDetails(pobjMemberPoolingDetails);
        }

        public List<MemberPoolingDetails> GetMemberPoolingDetails(MemberPoolingDetails pobjMemberPoolingDetails)
        {
            MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            return lobjMemberBusinessFacade.GetMemberPoolingDetails(pobjMemberPoolingDetails);
        }

        public bool UpdatePrimaryReference(MemberDetails pobjMemberDetails)
        {
            try
            {
                MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                return lobjMemberBusinessFacade.UpdatePrimaryReference(pobjMemberDetails);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Model UpdatePrimaryReference" + ex.Message);
                LoggingAdapter.WriteLog("Model UpdatePrimaryReference stack" + ex.StackTrace);
                throw ex;
            }

        }


        public List<MemberDetails> GetMemberDetailByPrimaryReference(SearchMember pobjSearchMember)
        {
            try
            {
                MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                return lobjMemberBusinessFacade.GetMemberDetailByPrimaryReference(pobjSearchMember);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetMemberDetailByPrimaryReference" + ex.Message + ex.StackTrace + ex.InnerException);
                throw ex;
            }
        }

        public bool UnMerge(string pstrUnMergeMemberId, string pstrPrimaryReference)
        {
            try
            {
                MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                return lobjMemberBusinessFacade.UnMergePoolAccount(pstrUnMergeMemberId, pstrPrimaryReference);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("UnMerge" + ex.Message + ex.StackTrace + ex.InnerException);
                return false;
            }
        }

        public List<TransactionDetails> GetRedeemTransactionDetails(SearchTransactions lobjSearchTransactions)
        {
            try
            {
                TransactionBusinessFacade lobjTransactionBusinessFacade = new TransactionBusinessFacade();
                return lobjTransactionBusinessFacade.GetRedeemTransactionDetails(lobjSearchTransactions);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetRedeemTransactionDetails" + ex.Message + ex.StackTrace + ex.InnerException);
                return null;
            }

        }

        public string RedeemPoints(float pfltAmount, int pintPoints, string pstrRelationReference, string pstrPassword, string pstrNarration, int pintLoyaltyTxnType, string pstrProgramCurrency)
        {
            string strRedeemMilesResponse = string.Empty;
            PGHelper lobjPGHelper = new PGHelper();
            strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption(pfltAmount, pfltAmount, pintPoints, string.Empty, string.Empty, pstrRelationReference, pstrPassword, pstrNarration, Convert.ToInt32(RelationType.LBMS), pintLoyaltyTxnType, pstrProgramCurrency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
            return strRedeemMilesResponse;
        }

        public List<Friend> GetSocialFriends(string pstrExternalReference, string pstrSocialType)
        {
            try
            {
                SocialFacade lobjSocialFacade = new SocialFacade();
                return lobjSocialFacade.GetSocialFriends(pstrExternalReference, pstrSocialType);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetSocialFriends Exception:" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);
                return null;
            }
        }

        public bool AddSocialConnection(SocialConnections pobjSocialConnection)
        {
            try
            {
                SocialFacade lobjSocialFacade = new SocialFacade();
                return lobjSocialFacade.AddSocialConnection(pobjSocialConnection.ExternalReference, pobjSocialConnection.SocialId, pobjSocialConnection.SocialName, pobjSocialConnection.SocialType, pobjSocialConnection.ProfilePhoto, pobjSocialConnection.AccessToken, pobjSocialConnection.AccessTokenSecret, pobjSocialConnection.AdditionalDetails1, pobjSocialConnection.AdditionalDetails2, pobjSocialConnection.AdditionalDetails3, pobjSocialConnection.AdditionalDetails4, pobjSocialConnection.AdditionalDetails5, pobjSocialConnection.AdditionalDetails6);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("AddSocialConnection Exception:" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);
                return false;
            }
        }

        public string TransferPointsSocial(MemberDetails pobjSenderMemberDetails, string pstrReceiverSocialId, string pstrReceiverSocialName, SocialType pobjSocialType, int pintTransferPoints, string pstrProgramCurrency)
        {
            string Result = string.Empty;
            try
            {
                SocialTransferFacade lobjFacade = new SocialTransferFacade();
                Result = lobjFacade.TransferPoints(pobjSenderMemberDetails, pstrReceiverSocialId, pstrReceiverSocialName, pobjSocialType, pintTransferPoints, pstrProgramCurrency);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("TransferPointsSocial Exception:" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);
            }
            return Result;
        }

        public string TransferPoints(MemberDetails pobjSenderMemberDetails, MemberDetails pobjReceiverMemberDetails, int pintTransferPoints, string pstrProgramCurrency)
        {
            string Result = string.Empty;
            TransferFacade lobjFacade = new TransferFacade();
            Result = lobjFacade.TransferPoints(pobjSenderMemberDetails, pobjReceiverMemberDetails, pintTransferPoints, pstrProgramCurrency);
            return Result;
        }

        public SocialConnections GetSocialConnection(string pstrExternalReference, SocialType pobjSocialType)
        {
            SocialConnections lobjSocialConnections = null;
            try
            {
                SocialFacade lobjSocialFacade = new SocialFacade();
                lobjSocialConnections = lobjSocialFacade.GetSocialConnection(pstrExternalReference, pobjSocialType.ToString());

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetSocialConnection Exception:-" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);

            }
            return lobjSocialConnections;
        }

        public List<SocialConnections> GetAllSocialConnections(string pstrExternalReference)
        {
            List<SocialConnections> lobjListOfSocialConnections = null;
            try
            {
                SocialFacade lobjSocialFacade = new SocialFacade();
                lobjListOfSocialConnections = lobjSocialFacade.GetAllSocialConnections(pstrExternalReference);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllSocialConnections Exception:-" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);
            }
            return lobjListOfSocialConnections;
        }

        public bool RemoveSocialConnection(string pstrExternalReference, SocialType pobjSocialType)
        {
            bool Result = false;
            try
            {
                SocialFacade lobjSocialFacade = new SocialFacade();
                Result = lobjSocialFacade.RemoveSocialConnection(pstrExternalReference, pobjSocialType.ToString());
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("RemoveSocialConnection Exception:-" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);
            }
            return Result;
        }

        public void SendEmail(List<string> lstrEmailparameter, MemberDetails pobjMemberDetails, string pstrTemplateCode)
        {
            BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
            lobjBookingIntegrationFacade.SendEmail(lstrEmailparameter, pobjMemberDetails, pstrTemplateCode);
        }
        public int InsertTravelSearchDetails(TravelSearchDetails pobjTravelSearchDetails)
        {

            try
            {
                ProgramDefinition lobjProgramDefinition = GetProgramMaster();
                pobjTravelSearchDetails.PointRate = GetProgramRedemptionRate(GetDefaultCurrency(), RedemptionCodeKeys.LOUNGE.ToString(), lobjProgramDefinition.ProgramId);
                pobjTravelSearchDetails.RefererDetails = GetRefererData();
                ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
                return lobjViatorServiceClient.InsertTravelSearchDetails(pobjTravelSearchDetails);

            }

            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("RemoveSocialConnection Exception:-" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);
                return 0;
            }
        }

        public List<LoungeSearchResponse> GetLoungeSearchResponse()
        {
            try
            {
                ProgramDefinition lobjProgramDefinition = GetProgramMaster();
                IBELoungeServiceClient lobjLoungeServiceClient = new IBELoungeServiceClient();
                LoungeSearchRequest lobjLoungeSearchRequest = new LoungeSearchRequest();

                lobjLoungeSearchRequest.RedemptionRate = GetProgramRedemptionRate(GetDefaultCurrency(), RedemptionCodeKeys.LOUNGE.ToString(), lobjProgramDefinition.ProgramId);

                lobjLoungeSearchRequest.RefererDetails = GetRefererData();
                return lobjLoungeServiceClient.GetLoungeSearchResponse(lobjLoungeSearchRequest);

            }

            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetLoungeSearchResponse Exception:-" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);
                return null;
            }
        }

        public List<QRMaster> GetAllQRMaster()
        {
            try
            {
                IBELoungeServiceClient lobjIBELoungeServiceClient = new IBELoungeServiceClient();
                return lobjIBELoungeServiceClient.GetAllQRMaster();
            }

            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllQRMaster Exception:-" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);
                return null;
            }
        }

        public LoungeBookingResponse BookLounge(MemberDetails pobjMemberDetails, IBEAccountTransactionDetails pobjIBEAccountTransactionDetails, LoungeBookingRequest pobjLoungeBookingRequest, string pstrCurrency, LoungeSearchResponse pobjLoungeSearchResponse)
        {
            try
            {
                BookingIntegrationFacade lobjBookingIntegrationFacade = new BookingIntegrationFacade();
                return lobjBookingIntegrationFacade.BookLounge(pobjMemberDetails, pobjIBEAccountTransactionDetails, pobjLoungeBookingRequest, pstrCurrency, pobjLoungeSearchResponse);
            }

            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("BookLounge Exception:-" + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace);
                return null;
            }
        }

        public List<InfiKashTransactionDetails> GetAllInfiKashBackDetailsByRelationReference(string pstrRelationReference, int pintProgramId)
        {
            try
            {
                InfiKashClientHelper lobjClientHelper = new InfiKashClientHelper();
                return lobjClientHelper.GetAllInfiKashBackDetailsByRelationReference(pstrRelationReference, pintProgramId);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetAllInfiKashBackDetailsByRelationReference - RelationReference - " + pstrRelationReference + " |exception - " + ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        public string MemberCreation(MemberDetails pobjMemberDetails)
        {

            try
            {
                //MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                //return lobjMemberBusinessFacade.MemberCreation(pobjMemberDetails);

                RuleEngineHelper lobjHelper = new RuleEngineHelper();
                return lobjHelper.ExecuteRuleSet<string>(new ExecuteRequest("MemberCreation", "MemberCreation", new List<object>(0) { pobjMemberDetails }));
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("MemberCreation -  InfiModel - " + ex.Message);

                return "";
            }
        }

        public List<LoanTransactionDetails> GetMemberStatementLoanTransaction(LoanSearchTransactions pobjSearchTransactions)
        {
            LoanFacade objLoanFacade = new LoanFacade();
            return objLoanFacade.GetMemberLoanTransaction(pobjSearchTransactions);
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            // return lobjHelper.ExecuteRuleSet<List<TransactionDetails>>(new ExecuteRequest("GetMemberStatementTransaction", "GetMemberStatementTransaction", new List<object>(0) { pobjSearchTransactions }));
        }

        public List<LoanTransactionDetails> GetMemberLoanTransactionByDate(LoanSearchTransactions pobjSearchTransactions)
        {
            LoanFacade objTransactionBusinessFacade = new LoanFacade();
            return objTransactionBusinessFacade.GetMemberLoanTransactionByDate(pobjSearchTransactions);
            //RuleEngineHelper lobjHelper = new RuleEngineHelper();
            //return lobjHelper.ExecuteRuleSet<List<TransactionDetails>>(new ExecuteRequest("GetMemberTransactionSummary", "v3GetMemberTransactionSummary", new List<object>(0) { pobjSearchTransactions }));

        }

        //Cash Back

    }
}
