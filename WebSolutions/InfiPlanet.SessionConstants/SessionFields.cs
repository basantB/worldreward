﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfiPlanet.SessionConstants
{
    public class SessionFields
    {
        public const string TourDestinationList = "TourDestinationList";
        public const string TourPackageList = "TourPackageList";
        public const string SelectedTourPackage = "SelectedTourPackage";
        public const string BookingAvailabilityRequest = "BookingAvailabilityRequest";
        public const string BookingAvailabilityResponse = "BookingAvailabilityResponse";
        public const string MemberDetails = "MemberDetails";
        public const string ViatorSearchRequest = "ViatorSearchRequest";
        public const string MemberMiles = "MemberMiles";
        public const string PackageBookerDetails = "PackageBookerDetails";
        public const string TourTravelleInfo = "TourTravelleInfo";
        public const string ViatorBookResponse="ViatorBookResponse";
        public const string SelectedTourDestinationId = "SelectedTourDestinationId";
        public const string TourGradeList = "TourGradeList";
        public const string SelectedTourGrade = "SelectedTourGrade";
        public const string SearchId = "SearchId";
        public const string BookingCalculatePriceResponse = "ViatorBookingCalculatePriceResponse";



    }
}
