﻿using Core.Platform.Member.Entites;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using Framework.EnterpriseLibrary.CommunicationEngine.BookingInterface;
using Framework.EnterpriseLibrary.DataAccess.BusinessFacade;
using Framework.EnterpriseLibrary.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Viator.Platform.ClientEntities;
using Viator.Platform.MasterEntities;
using Viator.Platform.ServiceClient;

namespace Infi.EmailImplementor
{
    public class ViatorBookingDetails : AirBookingEmail
    {
        string strCommunicationTracing = "Communication Tracing";

        public string AirBookingReceipt(List<string> plstparameter)
        {
            LoggingAdapter.WriteLog("Viator AirBookingReceipt Start ");
            string strStatus = string.Empty;
            LoggingAdapter.WriteLog("Viator BookingRefId:" + plstparameter[0]);
            try
            {
                string lstrPar = plstparameter[0];
                ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();
                TravelBookingDetails lobjTravelBookingDetails = lobjViatorServiceClient.GetMemberBookingDetails(Convert.ToString(plstparameter[0]));

                if (lobjTravelBookingDetails != null && lobjTravelBookingDetails.MemberId != string.Empty)
                {
                    LoggingAdapter.WriteLog("Viator Not Null" + lobjTravelBookingDetails.MemberId);
                    string MemberID = Convert.ToString(lobjTravelBookingDetails.MemberId);
                    LoggingAdapter.WriteLog("MemberID = " + MemberID);
                    MemberDetails lobjMemberDetails = MapMemberDetails(MemberID);
                    if (lobjMemberDetails != null && lobjMemberDetails.LastName != string.Empty)
                    {
                        LoggingAdapter.WriteLog("Member Not Null");
                        LoggingAdapter.WriteLog("Member Not Null 1 = " + lobjMemberDetails.LastName);
                    }
                    else
                    {
                        LoggingAdapter.WriteLog("Member Null");
                    }

                    string template = plstparameter[2];
                    LoggingAdapter.WriteLog("template = " + template);
                    strStatus = CreateReceipt(lobjTravelBookingDetails, lobjMemberDetails, plstparameter[2]);//plstparameter[2] is Template Text 
                }

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("AirBookingReceipt Exception :" + ex.InnerException.Message, strCommunicationTracing);
            }
            return strStatus;

        }

        private MemberDetails MapMemberDetails(string pstrMemberID)
        {
            DataSet dsMemberDetails = getMemberDetails(pstrMemberID);
            if (dsMemberDetails != null)
            {
                if (dsMemberDetails.Tables.Count > 0)
                {
                    if (dsMemberDetails.Tables[0].Rows.Count > 0)
                    {
                        MemberDetails lobjMemberDetails = new MemberDetails();
                        MemberRelation lobjMemberRelation = new MemberRelation();

                        lobjMemberDetails.LastName = Convert.ToString(dsMemberDetails.Tables[0].Rows[0]["Last_Name"]);
                        lobjMemberDetails.MobileNumber = Convert.ToString(dsMemberDetails.Tables[0].Rows[0]["Mobile_Number"]);
                        lobjMemberDetails.Address1 = Convert.ToString(dsMemberDetails.Tables[0].Rows[0]["Address1"]);
                        lobjMemberDetails.Address2 = Convert.ToString(dsMemberDetails.Tables[0].Rows[0]["Address2"]);
                        lobjMemberDetails.Email = Convert.ToString(dsMemberDetails.Tables[0].Rows[0]["Email_Id"]);
                        lobjMemberDetails.NationalId = Convert.ToString(dsMemberDetails.Tables[0].Rows[0]["National_Id"]);
                        lobjMemberRelation.RelationReference = Convert.ToString(dsMemberDetails.Tables[0].Rows[0]["Relation_Reference"]);
                        lobjMemberDetails.MemberRelationsList.Add(lobjMemberRelation);
                        return lobjMemberDetails;
                    }
                }

            }
            return null;


        }       

        public string CreateReceipt(TravelBookingDetails plobjTravelBookingDetails, MemberDetails pobjMemberDetails, string pstrTemplateText)//TemplateCode changes to Template Text
        {
            try
            {

                LoggingAdapter.WriteLog("Create Booking Pacakge");
                string strReceipt = "";
                string strDepartute = "";
                string strArrival = "";
                string strPaxInfo = "";
                string strReturn = "";
                strReceipt = pstrTemplateText;
                string DepartArrivalLoc = "";

                ViatorProductRequest lobjViatorProductRequest = new ViatorProductRequest();
                ViatorBookResponse lobjViatorBookResponse = new ViatorBookResponse();
                lobjViatorBookResponse = JSONSerialization.Deserialize<ViatorBookResponse>(plobjTravelBookingDetails.AdditionalDetails2);
                LoggingAdapter.WriteLog("CreateReceipt lobjViatorBookResponse " + JSONSerialization.Serialize(lobjViatorBookResponse));
                ViatorBookRequest lobjViatorBookRequest = new ViatorBookRequest();
                lobjViatorBookRequest = JSONSerialization.Deserialize<ViatorBookRequest>(plobjTravelBookingDetails.AdditionalDetails1);
                ViatorPastBookingResponse lobjViatorPastBookingResponse = new ViatorPastBookingResponse();
                ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();

                LoggingAdapter.WriteLog("CreateReceipt VoucherKey " + plobjTravelBookingDetails.VoucherKey);

                lobjViatorProductRequest.PointRate = lobjViatorBookRequest.PointRate;

                lobjViatorProductRequest.VoucherKey = plobjTravelBookingDetails.VoucherKey;

                lobjViatorPastBookingResponse = lobjViatorServiceClient.GetPastBookingResponse(lobjViatorProductRequest);
                LoggingAdapter.WriteLog("CreateReceipt lobjViatorPastBookingResponse not null" + JSONSerialization.Serialize(lobjViatorPastBookingResponse));
                // Code for Header
                strReceipt = strReceipt.Replace("$DepartArrivalLoc$", plobjTravelBookingDetails.ProductCode);

                string strGDSPNR = plobjTravelBookingDetails.BookingRefId;
                strReceipt = strReceipt.Replace("$GDSPNR$", strGDSPNR);
                strReceipt = strReceipt.Replace("$BookingDate$", plobjTravelBookingDetails.CreateDate.ToString("dd-MM-yyyy"));
                strReceipt = strReceipt.Replace("$TravelDate$", plobjTravelBookingDetails.BookingDate);
                strReceipt = strReceipt.Replace("$BookingStatus$", plobjTravelBookingDetails.BookingStatus.ToString().Equals("1") ? "Confirmed" : "Failed");
                strReceipt = strReceipt.Replace("$TransactionReferenceCode$", plobjTravelBookingDetails.ItineraryId);
                strReceipt = strReceipt.Replace("$TotalMiles$", Convert.ToString(plobjTravelBookingDetails.TotalPoints));
                strReceipt = strReceipt.Replace("$ShortTitle$", lobjViatorBookRequest.ProductDetails.ShortTitle);
                strReceipt = strReceipt.Replace("$SupplierCode$",lobjViatorBookRequest.ProductDetails.Code);                
                LoggingAdapter.WriteLog("Product img url " + lobjViatorBookRequest.ProductDetails.ThumbnailURL);
                strReceipt = strReceipt.Replace("$ThumbnailURL$", lobjViatorBookRequest.ProductDetails.ThumbnailURL);
                strReceipt = strReceipt.Replace("$ItemId$", Convert.ToString(lobjViatorPastBookingResponse.ViatorPastBookingDetails.ItemSummariesList[0].ItemId));
                strReceipt = strReceipt.Replace("$DeparturePoint$", lobjViatorPastBookingResponse.ViatorPastBookingDetails.ItemSummariesList[0].DeparturePoint);
                strReceipt = strReceipt.Replace("$DeparturePointAddress$", lobjViatorPastBookingResponse.ViatorPastBookingDetails.ItemSummariesList[0].DeparturePointAddress);
                string strDepTime = lobjViatorBookRequest.ProductDetails.DepartureTime;
                strDepTime = strDepTime.Replace("\r\n", "<br />");
                strReceipt = strReceipt.Replace("$DepartureTime$", strDepTime);
                strReceipt = strReceipt.Replace("$DepartureTimeComment$", lobjViatorBookRequest.ProductDetails.DepartureTimeComments);
                strReceipt = strReceipt.Replace("$SpecialOffer$", lobjViatorBookRequest.ProductDetails.SpecialOffer);
                string lstrInclusions = string.Empty;
                lstrInclusions = "<ul>";
                for (int i = 0; i < lobjViatorBookRequest.ProductDetails.InclusionsList.Count; i++)
                {
                    lstrInclusions += "<li>" + lobjViatorBookRequest.ProductDetails.InclusionsList[i] + "</li>";
                }
                lstrInclusions += "</ul>";


                strReceipt = strReceipt.Replace("$Inclusions$", lstrInclusions);
                string lstrExclusions = string.Empty;
                lstrExclusions = "<ul>";
                for (int i = 0; i < lobjViatorBookRequest.ProductDetails.ExclusionsList.Count; i++)
                {
                    lstrExclusions += "<li>" + lobjViatorBookRequest.ProductDetails.ExclusionsList[i] + "</li>";
                }
                lstrExclusions += "</ul>";

                strReceipt = strReceipt.Replace("$Exclusions$", lstrExclusions);

                strReceipt = strReceipt.Replace("$VoucherRequirement$", lobjViatorPastBookingResponse.ViatorPastBookingDetails.ItemSummariesList[0].VoucherRequirements);
                string lstrVoucherLink = Convert.ToString(lobjViatorPastBookingResponse.ViatorPastBookingDetails.ItemSummariesList[0].VoucherURL);
                strReceipt = strReceipt.Replace("$VoucherLink$", "Here is the URL <a href=" + lstrVoucherLink + "> link </a>to your voucher needed for acceptance to your tour, transfer or activity.  Please make sure you provide this voucher via mobile device or print and present to tour operator or attraction, as per the instructions on your voucher");
                //string lstrPayment = string.Empty;
                //string lstrPaymentDetails = "<tr><td width='2%'>&nbsp;</td><td align='left' valign='top' width='96%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: normal; text-transform: inherit; text-align: left; color: #231f20; padding: 4px 0 4px 0;'><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr> {0}</tr></table></td><td width='2%'>&nbsp;</td></tr>";


                //lstrPayment = string.Format(lstrPaymentDetails,  "World Rewardz : " + plobjTravelBookingDetails.TotalPoints);

                //strReceipt = strReceipt.Replace("$PaymentDetails$", lstrPayment);

                // Code For Travellor Info

                List<TravelAdditionalBookingDetails> lobjListOfPassengerDetails = new List<TravelAdditionalBookingDetails>();
                if (plobjTravelBookingDetails.TravelAdditionalBookingDetails != null)
                {
                    LoggingAdapter.WriteLog("TravelAdditionalBookingDetails not null " + plobjTravelBookingDetails.TravelAdditionalBookingDetails.Count);
                    string lstrPaxtype = "";
                    lobjListOfPassengerDetails = plobjTravelBookingDetails.TravelAdditionalBookingDetails;

                    for (int k = 0; k < lobjListOfPassengerDetails.Count; k++)
                    {
                        if (!lobjListOfPassengerDetails[k].IsBooker)
                        {
                            if (Convert.ToString(lobjListOfPassengerDetails[k].BandId) == "1")
                            {
                                lstrPaxtype = "Adult";
                            }
                            else if (Convert.ToString(lobjListOfPassengerDetails[k].BandId) == "2")
                            {
                                lstrPaxtype = "Child";
                            }
                            else if (Convert.ToString(lobjListOfPassengerDetails[k].BandId) == "3")
                            {
                                lstrPaxtype = "Infant";
                            }
                            strPaxInfo += "<tr>";
                            string lstrTd = "<td valign='top' style='padding: 5px; border-right:1px solid #999; border-bottom:1px solid #999; border-left:1px solid #999;'>";
                            strPaxInfo += lstrTd + lstrPaxtype + " </td>";
                            strPaxInfo += lstrTd + UppercaseFirst(lobjListOfPassengerDetails[k].TravellerName) + "</td>";
                            strPaxInfo += lstrTd + Convert.ToString(lobjListOfPassengerDetails[k].TravellerAge) + " </td>";
                            if (lobjListOfPassengerDetails[k].IsLeadTraveller)
                            {
                                strPaxInfo += lstrTd + "Yes</td>";
                            }
                            else
                            {
                                strPaxInfo += lstrTd + "No</td>";
                            }
                            //strPaxInfo += "<td align='left' valign='top' width='10%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; text-align: left; color: #231f20; padding: 4px 0 4px 0;'> </td>";

                            strPaxInfo += "</tr>";
                        }
                    }

                    strReceipt = strReceipt.Replace("$TblPassengerInfo$", strPaxInfo);
                }

                // Code for Departure table

                strDepartute += "<tr>";
                strDepartute = plobjTravelBookingDetails.TravelAdditionalBookingDetails[0].AdditionalDetails1;
                strDepartute += "</tr>";
                strReceipt = strReceipt.Replace("$TblDeparture$", strDepartute);

                ////Code for Member Details 
                strReceipt = strReceipt.Replace("$RelationReference$", pobjMemberDetails.NationalId);
                strReceipt = strReceipt.Replace("$Name$", pobjMemberDetails.LastName);
                strReceipt = strReceipt.Replace("$MobileNo$", pobjMemberDetails.MobileNumber);
                strReceipt = strReceipt.Replace("$Address1$", pobjMemberDetails.Address1);
                strReceipt = strReceipt.Replace("$Address2$", pobjMemberDetails.Address2);
                strReceipt = strReceipt.Replace("$EmailId$", pobjMemberDetails.Email);

                strReceipt = strReceipt.Replace("$ImagePath$", GetAppSettingValue("ClientImageUrl"));
                return strReceipt;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CreateReceipt Exception" + ex.InnerException.Message, strCommunicationTracing);
                return "Error";
            }

        }



        static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public static DataSet getMemberDetails(string pstrMemberId)
        {
            DataSet lobjdsMemberDetails = null;
            IDBConnection oConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, DataAccessSettings.GetProviderName());
            try
            {
                DataSet dsMemberDetails = new DataSet();
                IDBCommand oCmd;
                oCmd = oConn.CreateCommand(StoreProcedures.getMemberDetailsByBookingId, CommandType.StoredProcedure);
                oCmd[DBFields.MemberId] = pstrMemberId;
                oCmd.FillDataSet(dsMemberDetails);

                lobjdsMemberDetails = dsMemberDetails;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("getFlightReceipt Exception" + ex.InnerException.Message, "Communication Tracing");

            }

            finally
            {
                oConn.Close();
                oConn = null;
            }
            return lobjdsMemberDetails;
        }

        public string GetAppSettingValue(string key)
        {

            string arr = System.Configuration.ConfigurationSettings.AppSettings[key].ToString(); // //appSettings.GetValues(key);//System.Configuration.ConfigurationSettings.AppSettings[key].ToString(); // 
            LoggingAdapter.WriteLog("GetAppSettingValue BookingDetails key " + key + " Value " + arr, "Communication Tracing");
            return arr;


        }
    }
}
