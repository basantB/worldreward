﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infi.EmailImplementor
{
    public class DBFields
    {
        public const string CarrierName = "Carrier_Name";
        public const string CarrierLogoPath = "Carrier_Logo_Path";
        public const string CarrierCode = "Carrier_Code";

        //Masters
        public const string IATACode = "IATA_Code";
        public const string AirfieldName = "Airfield_Name";
        public const string CountryCode = "Country_Name";
        public const string CityName = "City_Name";
        public const string EmailID = "ID";
        public const string IsActive = "IsActive";
        public const string IataCode = "IATACode";
        public const string AircraftType = "AircraftType";
        public const string WakeCategory = "WakeCategory";
        public const string ICAOCode = "ICAOCode";


        public const string TemplateText = "TemplateText";
        public const string TemplateCode = "TemplateCode";

        public const string AirBookingId = "AirBookingId";
        public const string MemberId = "MemberId";
    }

    public class StoreProcedures
    {
        public const string getFlightReceipt = "sp_getFlightReceipt";
        public const string getAirlineDetails = "sp_getAirlineDetails";
        public const string getAirFieldDetails = "sp_getAirFieldDetails";
        public const string getMemberDetailsByBookingId = "sp_getMemberDetailsByBookingId";
        public static string SpGetAirCraftDetails = "sp_GetAirCraftDetails";
    }
}
