﻿using Core.Platform.SocialTransfer.DataAccess.Constants;
using Core.Platform.SocialTransfer.Entities;
using Framework.EnterpriseLibrary.Adapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Core.Platform.SocialTransfer.DataAccess.DataMapper
{
    public class DataMapper
    {
        public static List<SocialTransferPool> MapSocialTransferPool(DataSet pobjData)
        {
            List<SocialTransferPool> lobjListOfSocialTransferPool = null;
            if (pobjData != null && pobjData.Tables.Count > 0 && pobjData.Tables[0].Rows.Count > 0)
            {
                lobjListOfSocialTransferPool = new List<SocialTransferPool>();
                for (int count = 0; count < pobjData.Tables[0].Rows.Count; count++)
                {
                    try
                    {
                        SocialTransferPool lobjSocialTransferPool = new SocialTransferPool();
                        lobjSocialTransferPool.SenderUniqueReference = Convert.ToString(pobjData.Tables[0].Rows[count][DBFields.SenderUniqueReference]);
                        lobjSocialTransferPool.SocialId = Convert.ToString(pobjData.Tables[0].Rows[count][DBFields.SocialId]);
                        lobjSocialTransferPool.SocialType = Convert.ToInt32(pobjData.Tables[0].Rows[count][DBFields.SocialType]);
                        lobjSocialTransferPool.Points = Convert.ToInt32(pobjData.Tables[0].Rows[count][DBFields.Points]);
                        lobjSocialTransferPool.TransactionId = Convert.ToString(pobjData.Tables[0].Rows[count][DBFields.TransactionId]);
                        lobjSocialTransferPool.ProgramId = Convert.ToInt32(pobjData.Tables[0].Rows[count][DBFields.ProgramId]);
                        lobjSocialTransferPool.CreatedDate = Convert.ToDateTime(pobjData.Tables[0].Rows[count][DBFields.CreatedDate]);
                        lobjSocialTransferPool.UpdatedDate = Convert.ToDateTime(pobjData.Tables[0].Rows[count][DBFields.UpdatedDate]);
                        lobjSocialTransferPool.IsActive = Convert.ToBoolean(pobjData.Tables[0].Rows[count][DBFields.IsActive]);
                        lobjSocialTransferPool.AdditionalDetails1 = Convert.ToString(pobjData.Tables[0].Rows[count][DBFields.AdditionalDetails1]);
                        lobjSocialTransferPool.AdditionalDetails2 = Convert.ToString(pobjData.Tables[0].Rows[count][DBFields.AdditionalDetails2]);
                        lobjSocialTransferPool.AdditionalDetails3 = Convert.ToString(pobjData.Tables[0].Rows[count][DBFields.AdditionalDetails3]);
                        lobjSocialTransferPool.AdditionalDetails4 = Convert.ToString(pobjData.Tables[0].Rows[count][DBFields.AdditionalDetails4]);
                        lobjSocialTransferPool.AdditionalDetails5 = Convert.ToString(pobjData.Tables[0].Rows[count][DBFields.AdditionalDetails5]);

                        lobjListOfSocialTransferPool.Add(lobjSocialTransferPool);
                    }
                    catch (Exception ex)
                    {
                        LoggingAdapter.WriteLog("MapSocialTransferPool" + ex.Message + Environment.NewLine + ex.StackTrace + ex.Message);
                    }
                }
            }
            return lobjListOfSocialTransferPool;
        }

    }
}
