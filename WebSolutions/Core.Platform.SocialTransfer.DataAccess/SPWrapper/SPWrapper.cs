﻿
using Core.Platform.SocialTransfer.DataAccess.Constants;
using Core.Platform.SocialTransfer.Entities;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.DataAccess.BusinessFacade;
using Framework.EnterpriseLibrary.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Core.Platform.SocialTransfer.DataAccess.SPWrapper
{
    public class SPWrapper
    {
        public static bool InsertSocialTransferPool(SocialTransferPool pobjSocialTransferPool)
        {
            int lintId = 0;
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, Provider.SQL);
            try
            {
                IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.InsertSocialTransferPool, CommandType.StoredProcedure);
                ocmd[DBFields.SenderUniqueReference] = pobjSocialTransferPool.SenderUniqueReference;
                ocmd[DBFields.SocialId] = pobjSocialTransferPool.SocialId;
                ocmd[DBFields.SocialType] = pobjSocialTransferPool.SocialType;
                ocmd[DBFields.Points] = pobjSocialTransferPool.Points;
                ocmd[DBFields.TransactionId] = pobjSocialTransferPool.TransactionId;
                ocmd[DBFields.ProgramId] = pobjSocialTransferPool.ProgramId;
                ocmd[DBFields.IsActive] = pobjSocialTransferPool.IsActive;
                ocmd[DBFields.AdditionalDetails1] = pobjSocialTransferPool.AdditionalDetails1;
                ocmd[DBFields.AdditionalDetails2] = pobjSocialTransferPool.AdditionalDetails2;
                ocmd[DBFields.AdditionalDetails3] = pobjSocialTransferPool.AdditionalDetails3;
                ocmd[DBFields.AdditionalDetails4] = pobjSocialTransferPool.AdditionalDetails4;
                ocmd[DBFields.AdditionalDetails5] = pobjSocialTransferPool.AdditionalDetails5;

                lintId = Convert.ToInt32(ocmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("InsertSocialTransferPool:-" + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }
            return lintId > 0;
        }

        public static bool UpdateStatusSocialTransferPool(SocialTransferPool pobjSocialTransferPool)
        {
            bool lblnStatus = false;
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, Provider.SQL);
            try
            {
                IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.UpdateStatusSocialTransferPool, CommandType.StoredProcedure);
                ocmd[DBFields.TransactionId] = pobjSocialTransferPool.TransactionId;

                if (ocmd.ExecuteNonQuery() > 0)
                {
                    lblnStatus = true;
                }
                else
                {
                    lblnStatus = false;
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("UpdateStatusSocialTransferPool:-" + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }
            return lblnStatus;
        }

        public static DataSet GetSocialTransferPoolBySocialId(SocialTransferPool pobjSocialTransferPool)
        {
            DataSet ds = new DataSet();
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, Provider.SQL);
            try
            {
                IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.GetSocialTransferPoolBySocialId, CommandType.StoredProcedure);
                ocmd[DBFields.SocialId] = pobjSocialTransferPool.SocialId;
                ocmd[DBFields.SocialType] = pobjSocialTransferPool.SocialType;

                ocmd.FillDataSet(ds);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetSocialTransferPoolBySocialId:-" + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.InnerException);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }
            return ds;
        }
    }
}
