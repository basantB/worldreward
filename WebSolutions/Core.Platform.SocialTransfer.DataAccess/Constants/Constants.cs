﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Platform.SocialTransfer.DataAccess.Constants
{
    public class DBFields
    {
        #region SocialTransferPool
        public static string Id = "Id";
        public static string SenderUniqueReference = "Sender_Unique_Reference";
        public static string SocialId = "Social_Id";
        public static string SocialType = "Social_Type";
        public static string Points = "Points";
        public static string TransactionId = "Transaction_Id";
        public static string ProgramId = "Program_Id";
        public static string CreatedDate = "Created_Date";
        public static string UpdatedDate = "Updated_Date";
        public static string IsActive = "IsActive";
        public static string AdditionalDetails1 = "Additional_Details1";
        public static string AdditionalDetails2 = "Additional_Details2";
        public static string AdditionalDetails3 = "Additional_Details3";
        public static string AdditionalDetails4 = "Additional_Details4";
        public static string AdditionalDetails5 = "Additional_Details5";
        #endregion

    }


    public class StoredProcedures
    {
        public static string InsertSocialTransferPool = "Sp_InsertSocialTransferPool";
        public static string UpdateStatusSocialTransferPool = "Sp_UpdateStatusSocialTransferPool";
        public static string GetSocialTransferPoolBySocialId = "Sp_GetSocialTransferPoolBySocialId";

    }
}
