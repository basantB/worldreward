﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Platform.ServiceHelper;
using Core.Platform.Transactions.Entites;
using Core.Platform.Member.Entites;
using Core.Platform.Merchant.Entities;
using Core.Platform.PointGateway.BusinessFacade;
using System.Configuration;
using Core.Platform.PointGateway.Interface;
using Core.Platform.Common.Entities;
using Core.Platform.MerchantManagement.BusinessFacade;
using Core.Platform.MemberManagement.BusinessFacade;
using Framework.EnterpriseLibrary.Adapters;
using Core.Platform.Helper.ProgramName;
using Core.Platform.PointGateway.Exceptions;
using System.ServiceModel;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using Framework.EnterpriseLibrary.UniqueNumberGenerator;
using Core.Platform.ProgramInterface.ClientHelper;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Authentication.Entities;

namespace Core.Platform.PointGateway.Implementor
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class PGManager : IPGManager, IPGPOSManager
    {
        /// </summary>
        /// Check Available Points with Transaction Currency
        /// <param name="pstrRelationReference, pintRelationType, MerchantId, MerchantUserName adn merchantPassword"></param>
        [OperationBehavior(TransactionScopeRequired = false, TransactionAutoComplete = true)]
        public Response CheckPointsAvailability(string pstrRelationReference, int pintRelationType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            LoggingAdapter.WriteLog("Check Point Availability Request = " +  pstrRelationReference + " | " +   pintRelationType + " | " +  pstrProgramCurrency + " | " +  pstrMerchantId + " | " +  pstrMerchantUserName+ " | " +  pstrMerchantPassword);
            ServicesHelper lobjHelper = new ServicesHelper();
            try
            {
                bool lblnMerchantExist = false;                
                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);

                if (lblnMerchantExist)
                {
                    MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                    SearchMember lobjSearchMember = new SearchMember();
                    lobjSearchMember.RelationReference = pstrRelationReference;
                    lobjSearchMember.RelationType = pintRelationType;
                    MemberDetails lobjMemberDetails = lobjMemberBusinessFacade.GetEnrollMemberDetails(lobjSearchMember);

                    if (lobjMemberDetails != null && lobjMemberDetails.ProgramId > 0)
                    {
                        ProgramDefinition lobjProgramDefinition = null;
                        lobjProgramDefinition = GetProgramDefinitionById(lobjMemberDetails.ProgramId);
                        if (lobjProgramDefinition != null)
                        {

                            PGFacade lobjPGFacade = new PGFacade();
                            return lobjPGFacade.CheckAvailability(pstrRelationReference, lobjProgramDefinition.ProgramId, pintRelationType, pstrProgramCurrency);
                        }
                        else
                        {
                            return lobjHelper.createResponseObject(false, PGExceptionMessages.E0102, "E0102", 0);
                        }
                    }
                    else
                    {
                        return lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "E0103", 0);
                    }
                }
                else
                {
                    return lobjHelper.createResponseObject(lblnMerchantExist, PGExceptionMessages.E0101, "E0101", 0);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CheckPointsAvailability :" + ex.Message + " Stack trace : " + ex.StackTrace);
                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0107, "E0107", 0);
            }
        }

        /// </summary>
        /// Insert Block Points with Transaction Currency
        /// <param name="pfltAmount,pintPoints,pstrTxnMerchantName,pstrTxnCurrency,pstrRelationReference,pstrWebPassword,pstrNarration,pintPaymentType,pintRedemptionType,pstrMerchantId,pstrMerchantUserName,pstrMerchantPassword"></param>
        /// <returns></returns>
        [OperationBehavior(TransactionScopeRequired = false, TransactionAutoComplete = true)]
        public Response BlockPoints(float pfltSourceAmt, float pfltTxnAmount, int pintPoints, string pstrTxnMerchantName, string pstrSourceCurrency, string pstrTxnCurrency, string pstrRelationReference, string pstrWebPassword, string pstrNarration, int pintPaymentType, int pintRedemptionType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            ServicesHelper lobjHelper = new ServicesHelper();
            try
            {
                bool lblnMerchantExist = false;

                Response lobjResponse = new Response();
                TransactionDetails lobjTransactionDetails = new TransactionDetails();
                TransactionDetailsBreakage lobjTransactionDetailsBreakage = new TransactionDetailsBreakage();
                lobjTransactionDetails.TransactionType = TransactionType.Blocked;
                lobjTransactionDetails.LoyaltyTxnType = (LoyaltyTxnType)pintRedemptionType;
                lobjTransactionDetails.Amounts = Convert.ToSingle(pfltTxnAmount);
                lobjTransactionDetailsBreakage.Amount = Convert.ToSingle(pfltTxnAmount);
                lobjTransactionDetails.Points = Convert.ToInt32(pintPoints);
                lobjTransactionDetails.Narration = pstrNarration;
                lobjTransactionDetails.RelationType = (RelationType)pintPaymentType;
                lobjTransactionDetailsBreakage.Narration = pstrNarration;
                lobjTransactionDetails.TransactionDate = DateTime.Now;
                lobjTransactionDetails.ProcessingDate = DateTime.Now;
                lobjTransactionDetails.ExpiryDate = DateTime.Now;
                lobjTransactionDetails.MerchantName = pstrTxnMerchantName;
                lobjTransactionDetails.RelationReference = pstrRelationReference;
                lobjTransactionDetailsBreakage.TxnCurrency = pstrTxnCurrency;
                lobjTransactionDetailsBreakage.SourceAmount = pfltSourceAmt;
                lobjTransactionDetailsBreakage.SourceCurrency = pstrSourceCurrency;
                lobjTransactionDetailsBreakage.IsBillable = true;
                lobjTransactionDetails.TransactionDetailBreakage = lobjTransactionDetailsBreakage;
                lobjTransactionDetails.ExternalReference = Get12DigitNumberDateTime();

                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                
                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
                if (lblnMerchantExist)
                {
                    MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                    SearchMember lobjSearchMember = new SearchMember();
                    lobjSearchMember.RelationReference = pstrRelationReference;
                    lobjSearchMember.RelationType = pintPaymentType;
                    MemberDetails lobjMemberDetails = lobjMemberBusinessFacade.GetEnrollMemberDetails(lobjSearchMember);
                    if (lobjMemberDetails != null && lobjMemberDetails.ProgramId > 0)
                    {
                        ProgramDefinition lobjProgramDefinition = null;
                        lobjProgramDefinition = GetProgramDefinitionById(lobjMemberDetails.ProgramId);
                        if (lobjProgramDefinition != null)
                        {
                            if (pintPoints <= 0)
                            {
                                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0113, "E0113", string.Empty);
                                //lobjTransactionDetails.Points = Convert.ToInt32(Math.Ceiling(lobjTransactionDetails.Amounts / lobjProgramMaster.lstProgramAttribute[0].RedemptionRate));
                            }
                            if (AuthenticateLoginMemberRelation(lobjMemberDetails, pstrWebPassword))
                            {
                                PGFacade lobjPGFacade = new PGFacade();
                                lobjResponse = lobjPGFacade.CheckAvailability(pstrRelationReference, lobjProgramDefinition.ProgramId, pintPaymentType, pstrProgramCurrency);
                                if (lobjResponse.IsSucessful)
                                {
                                    if (Convert.ToInt32(lobjResponse.ReturnObject) >= lobjTransactionDetails.Points)
                                    {
                                        lobjTransactionDetails.TransactionCurrency = pstrProgramCurrency;
                                        lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
                                        return lobjPGFacade.BlockPoints(lobjTransactionDetails);
                                    }
                                    else
                                    {
                                        return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty);
                                    }
                                }
                                else
                                {
                                    return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty);
                                }
                            }
                            else
                            {
                                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "E0103", string.Empty);
                            }
                        }
                        else
                        {
                            return lobjHelper.createResponseObject(false, PGExceptionMessages.E0102, "E0102", string.Empty);
                        }
                    }
                    else
                    {
                        return lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "E0103", string.Empty);
                    }
                }
                else
                {
                    return lobjHelper.createResponseObject(lblnMerchantExist, PGExceptionMessages.E0101, "E0101", string.Empty);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("BlockPoints :" + ex.Message + " Stack trace : " + ex.StackTrace);
                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0108, "E0108", string.Empty);
            }
        }

        /// </summary>
        /// Update Redeem Point
        /// <param name="pstrExternalReference,pstrRelationReference,pstrMerchantName,pintRedemptionType,pstrMerchantId,pstrMerchantUserName,pstrMerchantPassword"></param>
        /// <returns></returns>
        [OperationBehavior(TransactionScopeRequired = false, TransactionAutoComplete = true)]
        public Response RedeemPoint(string pstrExternalReference, string pstrRelationReference, string pstrMerchantName, int pintRedemptionType, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            ServicesHelper lobjHelper = new ServicesHelper();
            try
            {
                bool lblnMerchantExist = false;

                TransactionDetails lobjTransactionDetails = new TransactionDetails();
                lobjTransactionDetails.ExternalReference = pstrExternalReference;
                lobjTransactionDetails.RelationReference = pstrRelationReference;
                lobjTransactionDetails.MerchantName = pstrMerchantName;

                lobjTransactionDetails.TransactionType = TransactionType.Debit;
                lobjTransactionDetails.LoyaltyTxnType = (LoyaltyTxnType)pintRedemptionType;


                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
                if (lblnMerchantExist)
                {
                    PGFacade lobjPGFacade = new PGFacade();
                    return lobjPGFacade.RedeemPoint(lobjTransactionDetails);
                }
                else
                {
                    return lobjHelper.createResponseObject(lblnMerchantExist, PGExceptionMessages.E0101, "E0101", lblnMerchantExist);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Redeem Points :" + ex.Message + " Stack trace : " + ex.StackTrace);
                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0109, "E0109", false);
            }
        }

        /// </summary>
        /// Roll Back Blocked Point
        /// <param name="pstrExternalReference,pstrRelationReference,pstrMerchantName,pstrMerchantId,pstrMerchantUserName,pstrMerchantPassword"></param>
        /// <returns></returns>
        [OperationBehavior(TransactionScopeRequired = false, TransactionAutoComplete = true)]
        public Response RollBackTransaction(string pstrExternalReference, string pstrRelationReference, string pstrMerchantName, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            LoggingAdapter.WriteLog("RollBackTransaction Request = " + pstrExternalReference + " | " + pstrRelationReference + " | " + pstrMerchantName + " | " + pstrMerchantId + " | " + pstrMerchantUserName + " | " + pstrMerchantPassword);
            ServicesHelper lobjHelper = new ServicesHelper();
            try
            {
                bool lblnMerchantExist = false;                
                Response lobjResponse = new Response();
                TransactionDetails lobjTransactionDetails = new TransactionDetails();

                lobjTransactionDetails.ExternalReference = pstrExternalReference;
                lobjTransactionDetails.RelationReference = pstrRelationReference;
                lobjTransactionDetails.MerchantName = pstrMerchantName;
                lobjTransactionDetails.TransactionType = TransactionType.None;
                lobjTransactionDetails.LoyaltyTxnType = LoyaltyTxnType.None;


                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
                if (lblnMerchantExist)
                {
                    LoggingAdapter.WriteLog("RollBackTransaction Merchant Validate");
                    PGFacade lobjPGFacade = new PGFacade();
                    lobjResponse = lobjPGFacade.RollBackTransaction(lobjTransactionDetails);
                    LoggingAdapter.WriteLog("Rollback Response = " + JSONSerialization.Serialize(lobjResponse));
                    return lobjResponse;
                }
                else
                {
                    LoggingAdapter.WriteLog("RollBackTransaction Merchant Not Validate");
                    return lobjHelper.createResponseObject(lblnMerchantExist, PGExceptionMessages.E0101, "E0101", lblnMerchantExist);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Roll BAck :" + ex.Message + " Stack trace : " + ex.StackTrace);
                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0110, "E0110", false);
            }
        }


        // POINT GATEWAY FOR DIRECT REDEMPTION

        /// </summary>
        /// PG Redeem Transaction with Transaction Currency
        /// <param name="pfltSourceAmount,pfltTxnAmount,pintPoints,pstrTxnMerchantName,pstrSourceCurrency,pstrTxnCurrency,pstrRelationReference,pstrWebPassword,pstrNarration,pintPaymentType,pintRedemptionType,pstrMerchantId,pstrMerchantUserName,pstrMerchantPassword"></param>
        /// <returns></returns>
        [OperationBehavior(TransactionScopeRequired = false, TransactionAutoComplete = true)]
        public Response PGRedeemTransaction(float pfltSourceAmount, float pfltTxnAmount, int pintPoints, string pstrTxnMerchantName, string pstrSourceCurrency, string pstrTxnCurrency, string pstrRelationReference, string pstrWebPassword, string pstrNarration, int pintPaymentType, int pintRedemptionType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            LoggingAdapter.WriteLog("PGRedeemTransaction Request = " + pfltSourceAmount + " | " + pfltTxnAmount + " | " + pintPoints + " | " + pstrTxnMerchantName + " | " + pstrSourceCurrency + " | " + pstrTxnCurrency + " | " + pstrRelationReference + " | " + pstrWebPassword + " | " + pstrNarration + " | " + pintPaymentType + " | " + pintRedemptionType + " | " + pstrProgramCurrency + " | " + pstrMerchantId + " | " + pstrMerchantUserName + " | " + pstrMerchantPassword);
            ServicesHelper lobjHelper = new ServicesHelper();           
            TransactionDetails lobjTransactionDetails = new TransactionDetails();
            TransactionDetailsBreakage lobjTransactionDetailsBreakage = new TransactionDetailsBreakage();
            Response lobjResponseObject = null;
            try
            {
                bool lblnMerchantExist = false;

                PGFacade lobjPGFacade = new PGFacade();
                Response lobjResponse = new Response();             

                lobjTransactionDetails.RelationType = (RelationType)pintPaymentType;
                LoggingAdapter.WriteLog("Payment Type 1" + lobjTransactionDetails.RelationType);
                lobjTransactionDetails.TransactionType = TransactionType.Debit;
                lobjTransactionDetails.LoyaltyTxnType = (LoyaltyTxnType)pintRedemptionType;
                lobjTransactionDetails.Amounts = Convert.ToSingle(pfltTxnAmount);
                lobjTransactionDetailsBreakage.Amount = Convert.ToSingle(pfltTxnAmount);
                lobjTransactionDetails.Points = Convert.ToInt32(pintPoints);

                lobjTransactionDetails.Narration = "Redemption " + lobjTransactionDetails.LoyaltyTxnType.ToString();
                lobjTransactionDetails.MerchantName = pstrNarration;

                lobjTransactionDetailsBreakage.Narration = "Redemption " + lobjTransactionDetails.LoyaltyTxnType.ToString();
                lobjTransactionDetails.TransactionDate = DateTime.Now;
                lobjTransactionDetails.ProcessingDate = DateTime.Now;
                lobjTransactionDetails.ExpiryDate = DateTime.Now;
                lobjTransactionDetails.RelationReference = pstrRelationReference;
                lobjTransactionDetailsBreakage.SourceAmount = pfltSourceAmount;
                lobjTransactionDetailsBreakage.SourceCurrency = pstrSourceCurrency;
                lobjTransactionDetailsBreakage.TxnCurrency = pstrTxnCurrency;
                lobjTransactionDetailsBreakage.IsBillable = true;
                lobjTransactionDetails.TransactionDetailBreakage = lobjTransactionDetailsBreakage;
                lobjTransactionDetails.ExternalReference = Get12DigitNumberDateTime();

                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
                if (lblnMerchantExist)
                {
                    LoggingAdapter.WriteLog("AuthenticateMerchant : " + lobjMerchantDetails.MerchantCode);
                     MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                    SearchMember lobjSearchMember = new SearchMember();
                    lobjSearchMember.RelationReference = pstrRelationReference;
                    lobjSearchMember.RelationType = pintPaymentType;

                    MemberDetails lobjMemberDetails = lobjMemberBusinessFacade.GetEnrollMemberDetails(lobjSearchMember);
                    if (lobjMemberDetails != null && lobjMemberDetails.ProgramId > 0)
                    {
                        LoggingAdapter.WriteLog("MemberDetails : " + lobjMemberDetails.MemberRelationsList[0].RelationReference);
                        ProgramDefinition lobjProgramDefinition = null;
                        lobjProgramDefinition = GetProgramDefinitionById(lobjMemberDetails.ProgramId);
                        if (lobjProgramDefinition != null)
                        {
                            LoggingAdapter.WriteLog("Program : " + lobjProgramDefinition.ProgramId);
                            if (lobjTransactionDetails.Points <= 0)
                            {
                                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0113, "E0113", string.Empty);
                                //lobjTransactionDetails.Points = Convert.ToInt32(Math.Ceiling(lobjTransactionDetails.Amounts / lobjProgramMaster.lstProgramAttribute[0].RedemptionRate));
                            }

                            if (AuthenticateLoginMemberRelation(lobjMemberDetails,pstrWebPassword))
                            {
                                LoggingAdapter.WriteLog("Autheticate Member" + pstrRelationReference);
                                lobjResponse = lobjPGFacade.CheckAvailability(pstrRelationReference, lobjProgramDefinition.ProgramId, pintPaymentType, pstrProgramCurrency);
                                if (lobjResponse.IsSucessful)
                                {
                                    LoggingAdapter.WriteLog("CHKAVAB : " + lobjResponse.IsSucessful);
                                    if (Convert.ToInt32(lobjResponse.ReturnObject) >= pintPoints)
                                    {
                                        lobjTransactionDetails.TransactionCurrency = pstrProgramCurrency;
                                        lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
                                        LoggingAdapter.WriteLog("PGRedeemTransaction : " + lobjTransactionDetails.TransactionCurrency + "  " + lobjTransactionDetails.ProgramId);
                                        return lobjPGFacade.PGRedeemTransaction(lobjTransactionDetails);
                                    }
                                    else
                                    {
                                        LoggingAdapter.WriteLog("(lobjResponse.ReturnObject) < pintPoints");
                                        return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
                                    }
                                }
                                else
                                {
                                    LoggingAdapter.WriteLog("CHKAVAB - ELSE : " + lobjResponse.IsSucessful);
                                    return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
                                }

                            }
                            else
                            {

                                LoggingAdapter.WriteLog("AuthenticateLoginMemberRelation : false");
                                lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "E0103", string.Empty, string.Empty, lobjTransactionDetails.Points);
                            }
                        }
                        else
                        {
                            LoggingAdapter.WriteLog("PROGRAMDEFIN : false");
                            lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0102, "E0102", string.Empty, string.Empty, lobjTransactionDetails.Points);
                        }
                    }
                }
                else
                {
                    lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0101, "E0101", string.Empty, string.Empty, lobjTransactionDetails.Points);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PG Manager Exception " + ex.Message + ex.StackTrace);
                lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0111, "E0111", string.Empty, string.Empty, lobjTransactionDetails.Points);
            }

            return lobjResponseObject;
        }

        /// </summary>
        /// PG RollBack Transaction
        /// <param name="pstrExternalReference,pstrRelationReference,pstrMerchantId,pstrMerchantUserName,pstrMerchantPassword"></param>
        /// <returns></returns>
        [OperationBehavior(TransactionScopeRequired = false, TransactionAutoComplete = true)]
        public Response PGRollBackTransaction(string pstrExternalReference, string pstrRelationReference, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            bool lblnMerchantExist = false;
            ServicesHelper lobjHelper = new ServicesHelper();
            try
            {
                TransactionDetails lobjTransactionDetails = new TransactionDetails();
                lobjTransactionDetails.TransactionType = TransactionType.None;
                lobjTransactionDetails.ExternalReference = pstrExternalReference;
                //lobjTransactionDetails.MerchantName = pstrMerchantName;
                lobjTransactionDetails.LoyaltyTxnType = LoyaltyTxnType.None;
                lobjTransactionDetails.RelationReference = pstrRelationReference;

                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
                if (lblnMerchantExist)
                {
                    PGFacade lobjPGFacade = new PGFacade();
                    return lobjPGFacade.PGRollBackTransaction(lobjTransactionDetails);
                }
                else
                {
                    return lobjHelper.createResponseObject(lblnMerchantExist, PGExceptionMessages.E0101, "E0101", false, string.Empty, 0);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PGRollBackTransaction Exception " + ex.Message + ex.StackTrace);
                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0112, "E0112", string.Empty, string.Empty, 0);
            }
        }


        //// --- Redemption Method BY National Id

        /// </summary>
        /// PGCheck Available Points with Transaction Currency
        /// <param name="pstrRelationReference, pintRelationType, MerchantId, MerchantUserName adn merchantPassword"></param>
        [OperationBehavior(TransactionScopeRequired = false, TransactionAutoComplete = true)]
        public Response PGCheckPointsAvailabilityNonMember(string pstrNationalId, int pintRelationType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            ServicesHelper lobjHelper = new ServicesHelper();
            try
            {
                bool lblnMerchantExist = false;
                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);

                if (lblnMerchantExist)
                {
                    ProgramDefinition lobjProgramDefinition = null;
                    lobjProgramDefinition = GetProgramDefinition();
                    if (lobjProgramDefinition != null)
                    {
                        MemberDetails lobjMemberDetails = null;
                        MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                        SearchMember lobjSearchMember = new SearchMember();
                        lobjSearchMember.NationalId = pstrNationalId;
                        lobjSearchMember.RelationType = pintRelationType;
                        lobjSearchMember.ProgramId = lobjProgramDefinition.ProgramId;
                        lobjMemberDetails = lobjMemberBusinessFacade.GetMemberDetailsUsingNationalID(lobjSearchMember);
                        if (lobjMemberDetails != null)
                        {
                            PGFacade lobjPGFacade = new PGFacade();
                            LoggingAdapter.WriteLog("Non Member Check Availability Relation Reference = " + lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals((RelationType)pintRelationType)).RelationReference);
                            return lobjPGFacade.CheckAvailability(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals((RelationType)pintRelationType)).RelationReference, lobjProgramDefinition.ProgramId, pintRelationType, pstrProgramCurrency);
                        }
                        else
                        {
                            return lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "PG0103", 0);
                        }
                    }
                    else
                    {
                        return lobjHelper.createResponseObject(false, PGExceptionMessages.E0102, "E0102", 0);
                    }   
                }
                else
                {
                    return lobjHelper.createResponseObject(lblnMerchantExist, PGExceptionMessages.E0101, "E0101", 0);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CheckPointsAvailability :" + ex.Message + " Stack trace : " + ex.StackTrace);
                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0107, "E0107", 0);
            }
        }

        /// </summary>
        /// PG Redeem Transaction with Transaction Currency
        /// <param name="pfltSourceAmount,pfltTxnAmount,pintPoints,pstrTxnMerchantName,pstrSourceCurrency,pstrTxnCurrency,pstrRelationReference,pstrWebPassword,pstrNarration,pintPaymentType,pintRedemptionType,pstrMerchantId,pstrMerchantUserName,pstrMerchantPassword"></param>
        /// <returns></returns>
        [OperationBehavior(TransactionScopeRequired = false, TransactionAutoComplete = true)]
        public Response PGRedeemTransactionNonMember(float pfltSourceAmount, float pfltTxnAmount, int pintPoints, string pstrTxnMerchantName, string pstrSourceCurrency, string pstrTxnCurrency, string pstrNationalId, string pstrWebPassword, string pstrNarration, int pintPaymentType, int pintRedemptionType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            ServicesHelper lobjHelper = new ServicesHelper();
            TransactionDetails lobjTransactionDetails = new TransactionDetails();
            TransactionDetailsBreakage lobjTransactionDetailsBreakage = new TransactionDetailsBreakage();
            Response lobjResponseObject = null;
            try
            {
                bool lblnMerchantExist = false;

                PGFacade lobjPGFacade = new PGFacade();
                Response lobjResponse = new Response();
                string lstrProgramName = ProgramHelper.ProgramName();
                ProgramDefinition lobjProgramDefinition = null;
                lobjProgramDefinition = GetProgramDefinition();                
                lobjTransactionDetails.RelationType = (RelationType)pintPaymentType;
                LoggingAdapter.WriteLog("Payment Type 1" + lobjTransactionDetails.RelationType);
                lobjTransactionDetails.TransactionType = TransactionType.Debit;
                lobjTransactionDetails.LoyaltyTxnType = (LoyaltyTxnType)pintRedemptionType;
                lobjTransactionDetails.Amounts = Convert.ToSingle(pfltTxnAmount);
                lobjTransactionDetailsBreakage.Amount = Convert.ToSingle(pfltTxnAmount);
                lobjTransactionDetails.Points = Convert.ToInt32(pintPoints);

                lobjTransactionDetails.Narration = "Redemption " + lobjTransactionDetails.LoyaltyTxnType.ToString();
                lobjTransactionDetails.MerchantName = pstrNarration;

                lobjTransactionDetailsBreakage.Narration = "Redemption " + lobjTransactionDetails.LoyaltyTxnType.ToString();
                lobjTransactionDetails.TransactionDate = DateTime.Now;
                lobjTransactionDetails.ProcessingDate = DateTime.Now;
                lobjTransactionDetails.ExpiryDate = DateTime.Now;                
                lobjTransactionDetailsBreakage.SourceAmount = pfltSourceAmount;
                lobjTransactionDetailsBreakage.SourceCurrency = pstrSourceCurrency;
                lobjTransactionDetailsBreakage.TxnCurrency = pstrTxnCurrency;
                lobjTransactionDetailsBreakage.IsBillable = true;
                lobjTransactionDetails.TransactionDetailBreakage = lobjTransactionDetailsBreakage;
                lobjTransactionDetails.ExternalReference = Get12DigitNumberDateTime();

                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
                if (lblnMerchantExist)
                {
                    if (lobjProgramDefinition != null)
                    {
                        lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
                        if (lobjTransactionDetails.Points <= 0)
                        {
                            return lobjHelper.createResponseObject(false, PGExceptionMessages.E0113, "E0113", string.Empty);
                            //lobjTransactionDetails.Points = Convert.ToInt32(Math.Ceiling(lobjTransactionDetails.Amounts / lobjProgramMaster.lstProgramAttribute[0].RedemptionRate));
                        }
                        MemberDetails lobjMemberDetails = null;
                        lobjMemberDetails = AuthenticateMemberByNationalId(pstrNationalId, pstrWebPassword, lobjProgramDefinition.ProgramId, pintPaymentType);
                        if (lobjMemberDetails !=null)
                        {
                            lobjTransactionDetails.RelationReference = lobjMemberDetails.MemberRelationsList.Find(lobj=>lobj.RelationType.Equals((RelationType)pintPaymentType)).RelationReference;
                            LoggingAdapter.WriteLog("Autheticate National Id" + pstrNationalId);
                            lobjResponse = lobjPGFacade.CheckAvailability(lobjTransactionDetails.RelationReference, lobjProgramDefinition.ProgramId, pintPaymentType, pstrProgramCurrency);
                            if (lobjResponse.IsSucessful)
                            {
                                if (Convert.ToInt32(lobjResponse.ReturnObject) >= pintPoints)
                                {
                                    lobjTransactionDetails.TransactionCurrency = pstrProgramCurrency;
                                    lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
                                    return lobjPGFacade.PGRedeemTransaction(lobjTransactionDetails);
                                }
                                else
                                {
                                    return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
                                }
                            }
                            else
                            {
                                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
                            }

                        }
                        else
                        {
                            lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "E0103", string.Empty, string.Empty, lobjTransactionDetails.Points);
                        }
                    }
                    else
                    {
                        lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0102, "E0102", string.Empty, string.Empty, lobjTransactionDetails.Points);
                    }
                }
                else
                {
                    lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0101, "E0101", string.Empty, string.Empty, lobjTransactionDetails.Points);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PG Manager Exception " + ex.Message + ex.StackTrace);
                lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0111, "E0111", string.Empty, string.Empty, lobjTransactionDetails.Points);
            }

            return lobjResponseObject;
        }


        /// </summary>
        /// PG RollBack Transaction
        /// <param name="pstrExternalReference,pstrRelationReference,pstrMerchantId,pstrMerchantUserName,pstrMerchantPassword"></param>
        /// <returns></returns>
        [OperationBehavior(TransactionScopeRequired = false, TransactionAutoComplete = true)]
        public Response PGRollBackTransactionNonMember(string pstrExternalReference, string pstrNationalId, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            bool lblnMerchantExist = false;
            ServicesHelper lobjHelper = new ServicesHelper();
            try
            {
                TransactionDetails lobjTransactionDetails = new TransactionDetails();
                lobjTransactionDetails.TransactionType = TransactionType.None;
                lobjTransactionDetails.ExternalReference = pstrExternalReference;
                //lobjTransactionDetails.MerchantName = pstrMerchantName;
                lobjTransactionDetails.LoyaltyTxnType = LoyaltyTxnType.None;

                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
                if (lblnMerchantExist)
                {
                    ProgramDefinition lobjProgramDefinition = null;
                    lobjProgramDefinition = GetProgramDefinition();
                    if (lobjProgramDefinition != null)
                    {
                        PGFacade lobjPGFacade = new PGFacade();
                        return lobjPGFacade.PGRollBackTransactionByExternalReference(lobjTransactionDetails);
                    }
                    else
                    {
                        return lobjHelper.createResponseObject(lblnMerchantExist, PGExceptionMessages.E0102, "0102", false, string.Empty, 0);
                    }
                }
                else
                {
                    return lobjHelper.createResponseObject(lblnMerchantExist, PGExceptionMessages.E0101, "0101", false, string.Empty, 0);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PGRollBackTransaction Exception " + ex.Message + ex.StackTrace);
                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0112, "E0112", string.Empty, string.Empty, 0);
            }
        }

        //[OperationBehavior(TransactionScopeRequired = false)]
        //public Response GetValidateAuthenticationToken(string pstrMemberId, string pstrAuthenticationToken, int pintAuthenticationType, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        //{
        //    LoggingAdapter.WriteLog("GetValidateAuthenticationToken Request = " + pstrMemberId + " | " + pstrAuthenticationToken + " | " + pintAuthenticationType + " | " + pstrMerchantId + " | " + pstrMerchantUserName + " | " + pstrMerchantPassword);
        //    ServicesHelper lobjHelper = new ServicesHelper();
        //    AuthenticationDetails lobjAuthenticationDetails = null;
        //    bool lblnMerchantExist = false;
        //    try
        //    {
        //        string lstrProgramName = ProgramHelper.ProgramName();
        //        ProgramDefinition lobjProgramDefinition = null;
        //        lobjProgramDefinition = GetProgramDefinition();
        //        MerchantDetails lobjMerchantDetails = new MerchantDetails();
        //        lobjMerchantDetails.MerchantCode = pstrMerchantId;
        //        lobjMerchantDetails.UserName = pstrMerchantUserName;
        //        lobjMerchantDetails.Password = pstrMerchantPassword;

        //        lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
        //        if (lblnMerchantExist)
        //        {
        //            PGFacade lobjPGFacade = new PGFacade();
        //            LoggingAdapter.WriteLog("GetValidateAuthenticationToken Request 1");
        //            lobjAuthenticationDetails = lobjPGFacade.GetValidateAuthenticationToken(pstrMemberId, pstrAuthenticationToken, pintAuthenticationType);
        //            if (lobjAuthenticationDetails != null && lobjAuthenticationDetails.MemberId != string.Empty)
        //            {
        //                MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
        //                SearchMember lobjSearchMember = new SearchMember();
        //                lobjSearchMember.RelationReference = lobjAuthenticationDetails.MemberId;
        //                lobjSearchMember.ProgramId = lobjProgramDefinition.ProgramId;
        //                lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);

        //                MemberDetails lobjMemberDetails = lobjMemberBusinessFacade.GetmemberDetails(lobjSearchMember);
        //                if (lobjMemberDetails != null)
        //                {
        //                    MemberRelation lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationReference.Equals(lobjAuthenticationDetails.MemberId) && lobj.RelationType.Equals((RelationType.LBMS)));
        //                    if (lobjMemberRelation.Status.Equals(Status.Active))
        //                    {
        //                        LoggingAdapter.WriteLog("GetValidateAuthenticationToken Response-" + JSONSerialization.Serialize(lobjAuthenticationDetails));
        //                        return lobjHelper.createResponseObject(true, "GetValidateAuthenticationToken", "", lobjAuthenticationDetails);
        //                    }
        //                    else
        //                    {
        //                        return lobjHelper.createResponseObject(false, "Member current Status is not Active", "", string.Empty);
        //                    }
        //                }
        //                else
        //                {
        //                    return lobjHelper.createResponseObject(false, "Member Does not exist", "", string.Empty);
        //                }
        //            }
        //            else
        //            {
        //                return lobjHelper.createResponseObject(false, "GetValidateAuthenticationToken Failed", "", string.Empty);
        //            }
        //        }
        //        else
        //        {
        //            return lobjHelper.createResponseObject(false, "GetValidateAuthenticationToken Merchant Failed", "", string.Empty);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("GetValidateAuthenticationToken exception - " + ex.Message + " | " + ex.StackTrace);
        //        return lobjHelper.createResponseObject(false, "GetValidateAuthenticationToken", "", string.Empty);
        //    }
        //}

        [OperationBehavior(TransactionScopeRequired = false)]
        public Response GetValidateAuthenticationToken(string pstrMemberId, string pstrAuthenticationToken, int pintAuthenticationType, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            LoggingAdapter.WriteLog("GetValidateAuthenticationToken Request = " + pstrMemberId + " | " + pstrAuthenticationToken + " | " + pintAuthenticationType + " | " + pstrMerchantId + " | " + pstrMerchantUserName + " | " + pstrMerchantPassword);
            ServicesHelper lobjHelper = new ServicesHelper();
            AuthenticationDetails lobjAuthenticationDetails = null;
            bool lblnMerchantExist = false;
            try
            {
                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
                if (lblnMerchantExist)
                {
                    LoggingAdapter.WriteLog("GetValidateAuthenticationToken lblnMerchantExist:" + lblnMerchantExist);
                    PGFacade lobjPGFacade = new PGFacade();
                    LoggingAdapter.WriteLog("GetValidateAuthenticationToken Request 1");
                    lobjAuthenticationDetails = lobjPGFacade.GetValidateAuthenticationToken(pstrMemberId, pstrAuthenticationToken, pintAuthenticationType);
                    if (lobjAuthenticationDetails != null && lobjAuthenticationDetails.MemberId != string.Empty)
                    {
                        LoggingAdapter.WriteLog("GetValidateAuthenticationToken lobjAuthenticationDetails MemberId:" + lobjAuthenticationDetails.MemberId);
                        MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                        SearchMember lobjSearchMember = new SearchMember();
                        lobjSearchMember.RelationReference = lobjAuthenticationDetails.MemberId;
                        lobjSearchMember.RelationType = Convert.ToInt32(RelationType.LBMS);
                        MemberDetails lobjMemberDetails = lobjMemberBusinessFacade.GetEnrollMemberDetails(lobjSearchMember);

                        if (lobjMemberDetails != null && lobjMemberDetails.ProgramId > 0)
                        {
                            LoggingAdapter.WriteLog("GetValidateAuthenticationToken MemberDetails:" + lobjMemberDetails.Id.ToString());
                            MemberRelation lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationReference.Equals(lobjAuthenticationDetails.MemberId) && lobj.RelationType.Equals((RelationType.LBMS)));
                            if (lobjMemberRelation.Status.Equals(Status.Active))
                            {
                                LoggingAdapter.WriteLog("GetValidateAuthenticationToken Response-" + JSONSerialization.Serialize(lobjAuthenticationDetails));
                                return lobjHelper.createResponseObject(true, "GetValidateAuthenticationToken", "", lobjAuthenticationDetails);
                            }
                            else
                            {
                                return lobjHelper.createResponseObject(false, "Member current Status is not Active", "", string.Empty);
                            }
                        }
                        else
                        {
                            return lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "E0103", 0);
                        }
                    }
                    else
                    {
                        return lobjHelper.createResponseObject(false, "GetValidateAuthenticationToken Failed", "", string.Empty);
                    }
                }
                else
                {
                    return lobjHelper.createResponseObject(false, "GetValidateAuthenticationToken Merchant Failed", "", string.Empty);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetValidateAuthenticationToken exception - " + ex.Message + " | " + ex.StackTrace);
                return lobjHelper.createResponseObject(false, "GetValidateAuthenticationToken", "", string.Empty);
            }
        }


        //[OperationBehavior(TransactionScopeRequired = false)]
        //public Response PGRedeemAuthTokenTransaction(float pfltSourceAmount, float pfltTxnAmount, int pintPoints, string pstrTxnMerchantName, string pstrSourceCurrency, string pstrTxnCurrency, string pstrRelationReference, string pstrAuthenticationToken, int pintAuthenticationType, string pstrNarration, int pintPaymentType, int pintRedemptionType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        //{
        //    LoggingAdapter.WriteLog("PGRedeemAuthTokenTransaction Request = " + pfltSourceAmount + " |  " + pfltTxnAmount + " |  " + pintPoints + " |  " + pstrTxnMerchantName + " |  " + pstrSourceCurrency + " |  " + pstrTxnCurrency + " |  " + pstrRelationReference + " |  " + pstrAuthenticationToken + " |  " + pintAuthenticationType + " |  " + pstrNarration + " |  " + pintPaymentType + " |  " + pintRedemptionType + " |  " + pstrProgramCurrency + " |  " + pstrMerchantId + " |  " + pstrMerchantUserName + " |  " + pstrMerchantPassword);
        //    ServicesHelper lobjHelper = new ServicesHelper();
        //    TransactionDetails lobjTransactionDetails = new TransactionDetails();
        //    TransactionDetailsBreakage lobjTransactionDetailsBreakage = new TransactionDetailsBreakage();
        //    Response lobjResponseObject = null;
        //    try
        //    {
        //        bool lblnMerchantExist = false;

        //        PGFacade lobjPGFacade = new PGFacade();
        //        Response lobjResponse = new Response();
        //        string lstrProgramName = ProgramHelper.ProgramName();
        //        ProgramDefinition lobjProgramDefinition = null;
        //        lobjProgramDefinition = GetProgramDefinition();
        //        AuthenticationDetails lobjAuthenticationDetails = null;

        //        lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
        //        lobjTransactionDetails.RelationType = (RelationType)pintPaymentType;
        //        LoggingAdapter.WriteLog("PGRedeemAuthTokenTransaction Payment Type 1" + lobjTransactionDetails.RelationType);
        //        lobjTransactionDetails.TransactionType = TransactionType.Debit;
        //        lobjTransactionDetails.LoyaltyTxnType = (LoyaltyTxnType)pintRedemptionType;
        //        lobjTransactionDetails.Amounts = Convert.ToSingle(pfltTxnAmount);
        //        lobjTransactionDetailsBreakage.Amount = Convert.ToSingle(pfltTxnAmount);
        //        lobjTransactionDetails.Points = Convert.ToInt32(pintPoints);

        //        lobjTransactionDetails.Narration = "Redemption " + lobjTransactionDetails.LoyaltyTxnType.ToString();
        //        lobjTransactionDetails.MerchantName = pstrNarration;
        //        lobjTransactionDetailsBreakage.Narration = "Redemption " + lobjTransactionDetails.LoyaltyTxnType.ToString();
        //        lobjTransactionDetails.TransactionDate = DateTime.Now;
        //        lobjTransactionDetails.ProcessingDate = DateTime.Now;
        //        lobjTransactionDetails.ExpiryDate = DateTime.Now;
        //        lobjTransactionDetails.RelationReference = pstrRelationReference;
        //        lobjTransactionDetailsBreakage.SourceAmount = pfltSourceAmount;
        //        lobjTransactionDetailsBreakage.SourceCurrency = pstrSourceCurrency;
        //        lobjTransactionDetailsBreakage.TxnCurrency = pstrTxnCurrency;
        //        lobjTransactionDetailsBreakage.IsBillable = true;
        //        lobjTransactionDetails.TransactionDetailBreakage = lobjTransactionDetailsBreakage;
        //        lobjTransactionDetails.ExternalReference = Get12DigitNumberDateTime();

        //        MerchantDetails lobjMerchantDetails = new MerchantDetails();
        //        lobjMerchantDetails.MerchantCode = pstrMerchantId;
        //        lobjMerchantDetails.UserName = pstrMerchantUserName;
        //        lobjMerchantDetails.Password = pstrMerchantPassword;

        //        lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
        //        if (lblnMerchantExist)
        //        {
        //            if (lobjProgramDefinition != null)
        //            {
        //                if (lobjTransactionDetails.Points <= 0)
        //                {
        //                    return lobjHelper.createResponseObject(false, PGExceptionMessages.E0113, "E0113", string.Empty);
        //                    //lobjTransactionDetails.Points = Convert.ToInt32(Math.Ceiling(lobjTransactionDetails.Amounts / lobjProgramMaster.lstProgramAttribute[0].RedemptionRate));
        //                }
        //                LoggingAdapter.WriteLog("PGRedeemAuthTokenTransaction Request 1");
        //                lobjAuthenticationDetails = lobjPGFacade.GetValidateAuthenticationTokenMemberId(pstrRelationReference, pstrAuthenticationToken, pintAuthenticationType);

        //                if (lobjAuthenticationDetails != null && lobjAuthenticationDetails.MemberId.Equals(pstrRelationReference))
        //                {
        //                    MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
        //                    SearchMember lobjSearchMember = new SearchMember();
        //                    lobjSearchMember.RelationReference = pstrRelationReference;
        //                    lobjSearchMember.ProgramId = lobjProgramDefinition.ProgramId;
        //                    lobjSearchMember.RelationType = pintPaymentType;

        //                    MemberDetails lobjMemberDetails = lobjMemberBusinessFacade.GetmemberDetails(lobjSearchMember);
        //                    if (lobjMemberDetails != null)
        //                    {
        //                        MemberRelation lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationReference.Equals(pstrRelationReference) && lobj.RelationType.Equals((RelationType)pintPaymentType));
        //                        if (lobjMemberRelation.Status.Equals(Status.Active))
        //                        {
        //                            LoggingAdapter.WriteLog("Autheticate Token " + pstrRelationReference + " Token = " + pstrAuthenticationToken);
        //                            lobjResponse = lobjPGFacade.CheckAvailability(pstrRelationReference, lobjProgramDefinition.ProgramId, pintPaymentType, pstrProgramCurrency);
        //                            if (lobjResponse.IsSucessful)
        //                            {
        //                                if (Convert.ToInt32(lobjResponse.ReturnObject) >= pintPoints)
        //                                {
        //                                    lobjTransactionDetails.TransactionCurrency = pstrProgramCurrency;
        //                                    lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
        //                                    return lobjPGFacade.PGRedeemTransaction(lobjTransactionDetails);
        //                                }
        //                                else
        //                                {
        //                                    return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            return lobjHelper.createResponseObject(false, "Member current Status is not Active", "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        return lobjHelper.createResponseObject(false, "Member Does not exist", "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
        //                    }
        //                }
        //                else
        //                {
        //                    lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "E0103", string.Empty, string.Empty, lobjTransactionDetails.Points);
        //                }
        //            }
        //            else
        //            {
        //                lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0102, "E0102", string.Empty, string.Empty, lobjTransactionDetails.Points);
        //            }
        //        }
        //        else
        //        {
        //            lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0101, "E0101", string.Empty, string.Empty, lobjTransactionDetails.Points);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("PG Manager Exception " + ex.Message + ex.StackTrace);
        //        lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0111, "E0111", string.Empty, string.Empty, lobjTransactionDetails.Points);
        //    }

        //    return lobjResponseObject;
        //}

        [OperationBehavior(TransactionScopeRequired = false)]
        public Response PGRedeemAuthTokenTransaction(float pfltSourceAmount, float pfltTxnAmount, int pintPoints, string pstrTxnMerchantName, string pstrSourceCurrency, string pstrTxnCurrency, string pstrRelationReference, string pstrAuthenticationToken, int pintAuthenticationType, string pstrNarration, int pintPaymentType, int pintRedemptionType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword)
        {
            LoggingAdapter.WriteLog("PGRedeemAuthTokenTransaction Request = " + pfltSourceAmount + " |  " + pfltTxnAmount + " |  " + pintPoints + " |  " + pstrTxnMerchantName + " |  " + pstrSourceCurrency + " |  " + pstrTxnCurrency + " |  " + pstrRelationReference + " |  " + pstrAuthenticationToken + " |  " + pintAuthenticationType + " |  " + pstrNarration + " |  " + pintPaymentType + " |  " + pintRedemptionType + " |  " + pstrProgramCurrency + " |  " + pstrMerchantId + " |  " + pstrMerchantUserName + " |  " + pstrMerchantPassword);
            ServicesHelper lobjHelper = new ServicesHelper();
            TransactionDetails lobjTransactionDetails = new TransactionDetails();
            TransactionDetailsBreakage lobjTransactionDetailsBreakage = new TransactionDetailsBreakage();
            Response lobjResponseObject = null;
            try
            {
                bool lblnMerchantExist = false;

                PGFacade lobjPGFacade = new PGFacade();
                Response lobjResponse = new Response();

                AuthenticationDetails lobjAuthenticationDetails = null;

                //lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
                lobjTransactionDetails.RelationType = (RelationType)pintPaymentType;
                LoggingAdapter.WriteLog("PGRedeemAuthTokenTransaction Payment Type 1" + lobjTransactionDetails.RelationType);
                lobjTransactionDetails.TransactionType = TransactionType.Debit;
                lobjTransactionDetails.LoyaltyTxnType = (LoyaltyTxnType)pintRedemptionType;
                lobjTransactionDetails.Amounts = Convert.ToSingle(pfltTxnAmount);
                lobjTransactionDetailsBreakage.Amount = Convert.ToSingle(pfltTxnAmount);
                lobjTransactionDetails.Points = Convert.ToInt32(pintPoints);

                lobjTransactionDetails.Narration = "Redemption " + lobjTransactionDetails.LoyaltyTxnType.ToString();
                lobjTransactionDetails.MerchantName = pstrNarration;
                lobjTransactionDetailsBreakage.Narration = "Redemption " + lobjTransactionDetails.LoyaltyTxnType.ToString();
                lobjTransactionDetails.TransactionDate = DateTime.Now;
                lobjTransactionDetails.ProcessingDate = DateTime.Now;
                lobjTransactionDetails.ExpiryDate = DateTime.Now;
                lobjTransactionDetails.RelationReference = pstrRelationReference;
                lobjTransactionDetailsBreakage.SourceAmount = pfltSourceAmount;
                lobjTransactionDetailsBreakage.SourceCurrency = pstrSourceCurrency;
                lobjTransactionDetailsBreakage.TxnCurrency = pstrTxnCurrency;
                lobjTransactionDetailsBreakage.IsBillable = true;
                lobjTransactionDetails.TransactionDetailBreakage = lobjTransactionDetailsBreakage;
                lobjTransactionDetails.ExternalReference = Get12DigitNumberDateTime();

                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
                if (lblnMerchantExist)
                {
                    if (lobjTransactionDetails.Points <= 0)
                        {
                            return lobjHelper.createResponseObject(false, PGExceptionMessages.E0113, "E0113", string.Empty);
                            //lobjTransactionDetails.Points = Convert.ToInt32(Math.Ceiling(lobjTransactionDetails.Amounts / lobjProgramMaster.lstProgramAttribute[0].RedemptionRate));
                        }
                        LoggingAdapter.WriteLog("PGRedeemAuthTokenTransaction Request 1");
                        lobjAuthenticationDetails = lobjPGFacade.GetValidateAuthenticationTokenMemberId(pstrRelationReference, pstrAuthenticationToken, pintAuthenticationType);

                        if (lobjAuthenticationDetails != null && lobjAuthenticationDetails.MemberId.Equals(pstrRelationReference))
                        {
                            LoggingAdapter.WriteLog("PGRedeemAuthTokenTransaction lobjAuthenticationDetails" + lobjAuthenticationDetails.MemberId);
                            MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                            SearchMember lobjSearchMember = new SearchMember();
                            lobjSearchMember.RelationReference = pstrRelationReference;
                            lobjSearchMember.RelationType = pintPaymentType;
                            MemberDetails lobjMemberDetails = lobjMemberBusinessFacade.GetEnrollMemberDetails(lobjSearchMember);
                            if (lobjMemberDetails != null && lobjMemberDetails.ProgramId > 0)
                            {
                                LoggingAdapter.WriteLog("PGRedeemAuthTokenTransaction MemberDetails" + lobjMemberDetails.Id.ToString());
                                ProgramDefinition lobjProgramDefinition = null;
                                lobjProgramDefinition = GetProgramDefinitionById(lobjMemberDetails.ProgramId);
                                if (lobjProgramDefinition != null)
                            {
                                    LoggingAdapter.WriteLog("PGRedeemAuthTokenTransaction ProgramDefinition" + lobjProgramDefinition.ProgramId.ToString());
                                    MemberRelation lobjMemberRelation = lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationReference.Equals(pstrRelationReference) && lobj.RelationType.Equals((RelationType)pintPaymentType));
                                    if (lobjMemberRelation.Status.Equals(Status.Active))
                                {
                                    LoggingAdapter.WriteLog("Autheticate Token " + pstrRelationReference + " Token = " + pstrAuthenticationToken);
                                    lobjResponse = lobjPGFacade.CheckAvailability(pstrRelationReference, lobjProgramDefinition.ProgramId, pintPaymentType, pstrProgramCurrency);
                                    if (lobjResponse.IsSucessful)
                                    {
                                    LoggingAdapter.WriteLog("CHKAVAB : " + lobjResponse.IsSucessful);
                                        if (Convert.ToInt32(lobjResponse.ReturnObject) >= pintPoints)
                                        {
                                                lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
                                            lobjTransactionDetails.TransactionCurrency = pstrProgramCurrency;
                                            lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
                                        LoggingAdapter.WriteLog("PGRedeemTransaction : " + lobjTransactionDetails.TransactionCurrency + "  " + lobjTransactionDetails.ProgramId);
                                            return lobjPGFacade.PGRedeemTransaction(lobjTransactionDetails);
                                        }
                                        else
                                        {
                                        LoggingAdapter.WriteLog("(lobjResponse.ReturnObject) < pintPoints");
                                            return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
                                        }
                                    }
                                    else
                                    {
                                    LoggingAdapter.WriteLog("CHKAVAB - ELSE : " + lobjResponse.IsSucessful);
                                        return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
                                    }
                                }
                                else
                                {
                                        return lobjHelper.createResponseObject(false, "Member current Status is not Active", "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
                                    }
                                }
                                else
                                {
                                    return lobjHelper.createResponseObject(false, PGExceptionMessages.E0102, "E0102", 0);
                            }
                        }
                        else
                        {
                                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "E0103", 0);
                        }
                    }
                    else
                    {
                            lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "E0103", string.Empty, string.Empty, lobjTransactionDetails.Points);
                    }
                }
                else
                {
                    lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0101, "E0101", string.Empty, string.Empty, lobjTransactionDetails.Points);
                }

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PG Manager Exception " + ex.Message + ex.StackTrace);
                lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0111, "E0111", string.Empty, string.Empty, lobjTransactionDetails.Points);
            }

            return lobjResponseObject;
        }
        /// </summary>
        /// Generate 12 digit unique number
        /// <param name=""></param>
        /// <returns></returns>
        private string Get12DigitNumberDateTime()
        {
            return GenerateUniqueNumber.Get12DigitNumberDateTime();
        }

       
        /// </summary>
        /// Get Authenticate PointGateway Merchant
        /// <param name="pobjMerchantDetails"></param>
        /// <returns></returns>
        private bool AuthenticateMerchant(MerchantDetails pobjMerchantDetails)
        {
            MerchantFacade lobjMerchantFacade = new MerchantFacade();
            return lobjMerchantFacade.AuthenticateMerchant(pobjMerchantDetails);
        }

        /// </summary>
        /// Get Authenticate PointGateway Member
        /// <param name="pobjMerchantDetails"></param>
        /// <returns></returns>
        private bool AuthenticateMember(string pstrUserName, string pstrPassword, int pintProgramId, int pintRelationType)
        {
            SearchMember lobjSearchMember = new SearchMember();
            lobjSearchMember.RelationReference = pstrUserName;
            lobjSearchMember.ProgramId = pintProgramId;
            lobjSearchMember.RelationType = pintRelationType;

            MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            MemberDetails lobjMemberDetails = new MemberDetails();
            lobjMemberDetails = lobjMemberBusinessFacade.GetmemberDetails(lobjSearchMember);

            if (lobjMemberDetails != null)
            {
                if (lobjMemberDetails.MemberRelationsList[0].IsAccountActivated.Equals(true) && lobjMemberDetails.MemberRelationsList[0].Status.Equals(Status.Active))
                {
                    string decryptCurrentPwd = lobjMemberBusinessFacade.DecryptPassword(pstrPassword);
                    string decryptDbPwd = string.Empty;
                    decryptDbPwd = lobjMemberBusinessFacade.DecryptPassword(lobjMemberDetails.MemberRelationsList[0].WebPassword);                    
                    if (decryptDbPwd.Equals(decryptCurrentPwd))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// </summary>
        /// Get Authenticate PointGateway Member by Login Relation Type
        /// <param name="pobjMerchantDetails"></param>
        /// <returns></returns>
        private bool AuthenticateLoginMemberRelation(MemberDetails pobjMemberDetails,string pstrPassword)
        {
            if (pobjMemberDetails != null)
            {
                LoggingAdapter.WriteLog("Member Not NUll");
                MemberRelation lobjMemberRelation = pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS));
                if (lobjMemberRelation != null)
                {
                    LoggingAdapter.WriteLog("Member Relation Not NUll");
                    if (lobjMemberRelation.IsAccountActivated.Equals(true) && lobjMemberRelation.Status.Equals(Status.Active))
                    {
                        MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
                        string decryptCurrentPwd = lobjMemberBusinessFacade.DecryptPassword(pstrPassword);
                        LoggingAdapter.WriteLog("Member pwd Receive" + decryptCurrentPwd);
                        string decryptDbPwd = string.Empty;
                        decryptDbPwd = lobjMemberBusinessFacade.DecryptPassword(lobjMemberRelation.WebPassword);
                        LoggingAdapter.WriteLog("Member DB pwd " + decryptDbPwd);
                        if (decryptDbPwd.Equals(decryptCurrentPwd))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// </summary>
        /// Get Authenticate PointGateway Member By national Id
        /// <param name="pobjMerchantDetails"></param>
        /// <returns></returns>
        private MemberDetails AuthenticateMemberByNationalId(string pstrUserName, string pstrPassword, int pintProgramId, int pintRelationType)
        {
            SearchMember lobjSearchMember = new SearchMember();
            lobjSearchMember.NationalId = pstrUserName;
            lobjSearchMember.ProgramId = pintProgramId;
            lobjSearchMember.RelationType = pintRelationType;

            MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            MemberDetails lobjMemberDetails = null;
            lobjMemberDetails = lobjMemberBusinessFacade.GetMemberDetailsUsingNationalID(lobjSearchMember);

            if (lobjMemberDetails != null)
            {
                if (lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals((RelationType)pintRelationType)).IsAccountActivated.Equals(true) && lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals((RelationType)pintRelationType)).Status.Equals(Status.Active))
                {
                    string decryptCurrentPwd = lobjMemberBusinessFacade.DecryptPassword(pstrPassword);
                    string decryptDbPwd = string.Empty;
                    decryptDbPwd = lobjMemberBusinessFacade.DecryptPassword(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals((RelationType)pintRelationType)).WebPassword);
                    if (decryptDbPwd.Equals(decryptCurrentPwd))
                    {
                        return lobjMemberDetails;
                    }
                }
            }
            return null;
        }

        /// </summary>
        /// GetProgramDetails
        /// <param name=""></param>
        /// <returns></returns>
        private ProgramDefinition GetProgramDefinition()
        {
            string lstrProgramName = ProgramHelper.ProgramName();
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = lobjInterfaceHelper.GetProgramDefinition(lstrProgramName);
            return lobjProgramDefinition;
        }

        
        /// </summary>
        /// GetProgramDetails by Program Id
        /// <param name=""></param>
        /// <returns></returns>
        private ProgramDefinition GetProgramDefinitionById(int pintProgramId)
        {
            try
            {
            string lstrProgramName = ProgramHelper.ProgramName();
            InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
            ProgramDefinition lobjProgramDefinition = null;
            lobjProgramDefinition = lobjInterfaceHelper.GetProgramDefinitionById(pintProgramId);
            return lobjProgramDefinition;
        }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetProgramDefinitionById Exception = " + ex.Message + " | " + ex.StackTrace);
                return null;
            }
        }


        

        ///// </summary>
        ///// GetProgramRedemptionRate
        ///// <param name=""></param>
        ///// <returns></returns>
        //private float GetProgramRedemptionRate()
        //{
        //    InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
        //    ProgramDefinition lobjProgramDefinition = null;
        //    lobjProgramDefinition = lobjInterfaceHelper.GetProgramRedemptionRate(pstrProgramCurrency,pstrRedemtionCode,
        //    return lobjProgramDefinition;
        //}

        public Response RedeemPointsPOS(float pfltTxnAmount, string pstrTxnCurrency, float pfltSourceAmount, string pstrSourceCurrency, int pintPoints, string pstrMemberId, string pstrPOSPIN, string pstrNarration, string pstrMerchantId, string pstrTerminalId, string pstrTransactionId, string pstrMerchantCity, string pstrMerchantCountry, string pstrMCC, int pintPaymentType, string pstrProgramCurrency, string pstrMerchantUserId, string pstrMerchantUserName, string pstrMerchantUserPassword)
        {
            ServicesHelper lobjHelper = new ServicesHelper();
            TransactionDetails lobjTransactionDetails = new TransactionDetails();
            TransactionDetailsBreakage lobjTransactionDetailsBreakage = new TransactionDetailsBreakage();
            Response lobjResponseObject = null;
            try
            {
                bool lblnMerchantExist = false;

                PGFacade lobjPGFacade = new PGFacade();
                Response lobjResponse = new Response();                
                ProgramDefinition lobjProgramDefinition = null;
                lobjProgramDefinition = GetProgramDefinition();


                
                lobjTransactionDetails.RelationType = (RelationType)pintPaymentType;
                LoggingAdapter.WriteLog("Payment Type 1" + lobjTransactionDetails.RelationType);
                lobjTransactionDetails.TransactionType = TransactionType.Debit;
                lobjTransactionDetails.LoyaltyTxnType = LoyaltyTxnType.POS;
                lobjTransactionDetails.Amounts = Convert.ToSingle(pfltTxnAmount);
                lobjTransactionDetailsBreakage.Amount = Convert.ToSingle(pfltTxnAmount);
                lobjTransactionDetails.Points = Convert.ToInt32(pintPoints);

                lobjTransactionDetails.Narration = "Redemption " + lobjTransactionDetails.LoyaltyTxnType.ToString();
                lobjTransactionDetails.MerchantName = pstrNarration;

                lobjTransactionDetailsBreakage.Narration = "Redemption " + lobjTransactionDetails.LoyaltyTxnType.ToString();
                lobjTransactionDetails.TransactionDate = DateTime.Now;
                lobjTransactionDetails.ProcessingDate = DateTime.Now;
                lobjTransactionDetails.ExpiryDate = DateTime.Now;
                lobjTransactionDetails.RelationReference = pstrMemberId;
                lobjTransactionDetailsBreakage.SourceAmount = pfltSourceAmount;
                lobjTransactionDetailsBreakage.SourceCurrency = pstrSourceCurrency;
                lobjTransactionDetailsBreakage.TxnCurrency = pstrTxnCurrency;
                lobjTransactionDetailsBreakage.IsBillable = true;
                lobjTransactionDetailsBreakage.TransactionId = pstrTransactionId;
                lobjTransactionDetailsBreakage.MerchantCity = pstrMerchantCity;
                lobjTransactionDetailsBreakage.MerchantCountry = pstrMerchantCountry;
                lobjTransactionDetailsBreakage.MerchantId = pstrMerchantId;
                lobjTransactionDetailsBreakage.TerminalId = pstrTerminalId;
                lobjTransactionDetailsBreakage.MCC = pstrMCC;
                lobjTransactionDetails.TransactionDetailBreakage = lobjTransactionDetailsBreakage;
                lobjTransactionDetails.ExternalReference = Get12DigitNumberDateTime();

                MerchantDetails lobjMerchantDetails = new MerchantDetails();
                lobjMerchantDetails.MerchantCode = pstrMerchantUserId;
                lobjMerchantDetails.UserName = pstrMerchantUserName;
                lobjMerchantDetails.Password = pstrMerchantUserPassword;

                lblnMerchantExist = AuthenticateMerchant(lobjMerchantDetails);
                if (lblnMerchantExist)
                {
                    if (lobjProgramDefinition != null)
                    {
                        lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
                        if (lobjTransactionDetails.Points <= 0)
                        {
                            return lobjHelper.createResponseObject(false, PGExceptionMessages.E0113, "E0113", string.Empty, string.Empty, lobjTransactionDetails.Points);
                           // lobjTransactionDetails.Points = Convert.ToInt32(Math.Ceiling(lobjTransactionDetails.Amounts / lobjProgramMaster.lstProgramAttribute[0].RedemptionRate));
                        }

                        if (AuthenticateMemberByPOSPIN(pstrMemberId, pstrPOSPIN, lobjProgramDefinition.ProgramId, pintPaymentType))
                        {
                            LoggingAdapter.WriteLog("Autheticate Member" + pstrMemberId);
                            lobjResponse = lobjPGFacade.CheckAvailability(pstrMemberId, lobjProgramDefinition.ProgramId, pintPaymentType, pstrProgramCurrency);
                            if (lobjResponse.IsSucessful)
                            {
                                if (Convert.ToInt32(lobjResponse.ReturnObject) >= pintPoints)
                                {
                                    lobjTransactionDetails.TransactionCurrency = pstrProgramCurrency;
                                    lobjTransactionDetails.ProgramId = lobjProgramDefinition.ProgramId;
                                    return lobjPGFacade.PGRedeemTransaction(lobjTransactionDetails);
                                }
                                else
                                {
                                    return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
                                }
                            }
                            else
                            {
                                return lobjHelper.createResponseObject(false, PGExceptionMessages.E0104, "E0104", string.Empty, string.Empty, lobjTransactionDetails.Points);
                            }

                        }
                        else
                        {
                            lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0103, "E0103", string.Empty, string.Empty, lobjTransactionDetails.Points);
                        }
                    }
                    else
                    {
                        lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0102, "E0102", string.Empty, string.Empty, lobjTransactionDetails.Points);
                    }
                }
                else
                {
                    lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0101, "E0101", string.Empty, string.Empty, lobjTransactionDetails.Points);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PG Manager Exception " + ex.Message + ex.StackTrace);
                lobjResponseObject = lobjHelper.createResponseObject(false, PGExceptionMessages.E0111, "E0111", string.Empty, string.Empty, lobjTransactionDetails.Points);
            }

            return lobjResponseObject;
        }

        /// </summary>
        /// Get Authenticate PointGateway Member
        /// <param name="pstrUserName,pstrPassword,pintProgramId,pintRelationType"></param>
        /// <returns></returns>
        private bool AuthenticateMemberByPOSPIN(string pstrRelationReference, string pstrPOSPIN, int pintProgramId, int pintRelationType)
        {
            SearchMember lobjSearchMember = new SearchMember();
            lobjSearchMember.RelationReference = pstrRelationReference;
            lobjSearchMember.ProgramId = pintProgramId;
            lobjSearchMember.RelationType = pintRelationType;

            MemberBusinessFacade lobjMemberBusinessFacade = new MemberBusinessFacade();
            MemberDetails lobjMemberDetails = new MemberDetails();
            lobjMemberDetails = lobjMemberBusinessFacade.GetmemberDetails(lobjSearchMember);

            if (lobjMemberDetails != null)
            {
                if (lobjMemberDetails.MemberRelationsList[0].IsAccountActivated.Equals(true) && lobjMemberDetails.MemberRelationsList[0].Status.Equals(Status.Active))
                {
                    string decryptCurrentPOSPIN = lobjMemberBusinessFacade.DecryptPassword(pstrPOSPIN);
                    string decryptDbPOSPIN = string.Empty;
                    decryptDbPOSPIN = lobjMemberBusinessFacade.DecryptPassword(lobjMemberDetails.MemberRelationsList[0].POSPIN);

                    if (decryptDbPOSPIN.Equals(decryptCurrentPOSPIN))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
