﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CB.IBE.Platform.ClientEntities;
using Framework.EnterpriseLibrary.Adapters;
using CB.IBE.Platform.IBEClient;
using CB.IBE.Platform.Hotels.ClientEntities;
using Framework.Integrations.Hotels.Entities;
using Core.Platform.PointGateway.Service.Helper;
using Core.Platform.Member.Entites;
using CB.IBE.Platform.Car.ClientEntities;
using CB.IBE.Platform.Car.Entities;
using CB.IBE.Platform.IBECarClient;
using Core.Platform.Transactions.Entites;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using Core.Platform.Common.Entities;
using Core.Platform.OTP.Entities;
using Core.Platform.OTP.Facade;
using System.ServiceModel;
using Framework.EnterpriseLibrary.Common.WCF.Faults;
using CB.IBE.Platform.Entities;
using CB.IBE.Platform.Masters.Entities;
using Framework.EnterpriseLibrary.CommunicationEngine.Entity;
using Framework.EnterpriseLibrary.CommunicationEngine.Helper;
using CB.IBE.DepositAccountService.ClientHelper;
using CB.IBE.Platform.Transactions.Entites;
using System.Runtime.Serialization;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Configurations;
using Core.Platform.Booking.Entities;


using System.Web;
using InfiVoucher.Platform.Entities;
using Core.Platform.InfiVoucher.ClientHelper;
using Core.Platform.InfiVoucher.Entities;
using BillPay.UtilityPayment.Entities;
using BillPay.UtilityPayment.ClientHelper;
using Viator.Platform.ServiceClient;
using Viator.Platform.ClientEntities;
using System.Configuration;
using System.IO;
using IBE.Platform.Lounge.Entities;
using Core.Platform.LoungeVoucher.ClientHelper;
using IBE.Platform.Lounge.ServiceClient;
using Framework.EnterpriseLibrary.UniqueNumberGenerator;
using System.Net;
using System.Drawing;


namespace Core.Framework.Booking.Facade
{
    public class BookingIntegrationFacade
    {

        enum FailureType { FAILUREEMAIL, SMS, CTRESPONSE, BLOCKMILES, REDEEMMILES, ROLLBACKMILES };

        string lstrMerchantId = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["MerchantId"]);
        string lstrMerchantUserName = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["MerchantUserName"]);
        string lstrMerchantPassword = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["MerchantPassword"]);
        string lstrProgramName = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["ProgramName"]);
        string lstrBookingFailureMsg = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["BookingFailureMsg"]);

        #region Flight

        public BookingResponse BookFlight(BookingRequest pobjBookingRequest, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            bool IsBookingConfirm = true;
            string strRedeemMilesResponse = string.Empty;
            string strRollBackMilesResponse = string.Empty;
            bool lboolRollBackResponse = false;
            string strFailureType = "";

            BookingResponse lobjBookingResponse = new BookingResponse();
            PGHelper lobjPGHelper = new PGHelper();

            try
            {
                bool IsRedeem = true;
                List<BookingPaymentBreakage> lobjListOfBookingPaymentBreakage = new List<BookingPaymentBreakage>();
                for (int i = 0; i < pobjListOfRedemptionDetails.Count; i++)
                {
                    BookingPaymentBreakage lobjBookingPaymentBreakage = new BookingPaymentBreakage();
                    strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption((float)(pobjListOfRedemptionDetails[i].Amount), (float)(pobjListOfRedemptionDetails[i].Amount), pobjListOfRedemptionDetails[i].Points, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, pobjBookingRequest.ItineraryDetails.OriginLocation + "-" + pobjBookingRequest.ItineraryDetails.DestinationLocation, Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.Air), pobjListOfRedemptionDetails[i].Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    pobjListOfRedemptionDetails[i].TransactionReference = strRedeemMilesResponse;
                    //pobjListOfRedemptionDetails[i].TransactionReference = pobjBookingRequest.ItineraryDetails.TransactionReference; 
                    IsRedeem = IsRedeem && (pobjListOfRedemptionDetails[i].TransactionReference != "" && pobjListOfRedemptionDetails[i].TransactionReference != string.Empty);

                    lobjBookingPaymentBreakage.Currency = pobjListOfRedemptionDetails[i].DisplayCurrency;
                    lobjBookingPaymentBreakage.Amount = pobjListOfRedemptionDetails[i].Points;
                    lobjBookingPaymentBreakage.TxnReference = strRedeemMilesResponse;
                    //lobjBookingPaymentBreakage.TxnReference = pobjBookingRequest.ItineraryDetails.TransactionReference;
                    lobjListOfBookingPaymentBreakage.Add(lobjBookingPaymentBreakage);
                }

                BookingPaymentDetails lobjBookingPaymentDetails = new BookingPaymentDetails();
                lobjBookingPaymentDetails.BookingPaymentBreakageList = lobjListOfBookingPaymentBreakage;
                lobjBookingPaymentDetails.MemberId = pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjBookingPaymentDetails.PaymentType = PaymentType.Points;
                lobjBookingPaymentDetails.ServiceType = ServiceType.FLIGHT;
                lobjBookingPaymentDetails.Points = pobjListOfRedemptionDetails[0].Points;
                lobjBookingPaymentDetails.PointsTxnRefererence = pobjListOfRedemptionDetails[0].TransactionReference;
                if (IsRedeem)
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.Success;
                }
                else
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.PaymentDone;
                }

                pobjBookingRequest.ItineraryDetails.BookingPaymentDetails = lobjBookingPaymentDetails;

                if (IsRedeem)
                {
                    pobjBookingRequest.ItineraryDetails.TransactionReference = pobjListOfRedemptionDetails[0].TransactionReference;
                    IBECTClient lobjIBECTClient = new IBECTClient();

                    try
                    {
                        LoggingAdapter.WriteLog("RedeemMiles Success : " + strRedeemMilesResponse, "BookingLogCategory");
                        try
                        {
                            lobjBookingResponse = lobjIBECTClient.CreateBooking(pobjBookingRequest);
                        }
                        catch (Exception ex)
                        {
                            LoggingAdapter.WriteLog("CreateBooking Failure BookingFacade:" + ex.Message + ex.StackTrace);
                        }

                        if (lobjBookingResponse != null && lobjBookingResponse.PNRDetails.TripId != null && lobjBookingResponse.PNRDetails.TripId != string.Empty && lobjBookingResponse.PNRDetails.Status.Equals(1))
                        {
                            LoggingAdapter.WriteLog("Booking called Trip Id  : " + lobjBookingResponse.PNRDetails.TripId + "IsBookingConfirm:" + IsBookingConfirm, "BookingLogCategory");
                            IsBookingConfirm = true;
                        }
                        else
                        {
                            LoggingAdapter.WriteLog("IsBookingConfirm:" + IsBookingConfirm, "BookingLogCategory");
                            IsBookingConfirm = false;
                        }
                        if (IsBookingConfirm)
                        {
                            try
                            {
                                //communication Engine call for Email send
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateFlightEmailParameters(lobjBookingResponse);
                                SendEmail(lstEmailparameter, pobjMemberDetails, "FlightBooked");
                                //Communication Engine Call for Sms Sending
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine Email exception in booking ", "BookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                            }
                            try
                            {
                                List<string> lstSMSparameter = new List<string>();
                                lstSMSparameter = GenerateFlightSMSParameters(lobjBookingResponse);
                                SendSms(lstSMSparameter, pobjMemberDetails, "FlightBooked");
                                LoggingAdapter.WriteLog("Mail and SMS sent.", "BookingLogCategory");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine SMS exception in booking", "BookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.SMS);
                            }

                            if (strFailureType != "" && strFailureType != string.Empty)
                            {
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                            }
                        }
                        else
                        {
                            try
                            {
                                for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                                {
                                    strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                    if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                        lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjBookingRequest.ItineraryDetails.OriginLocation + "-" + pobjBookingRequest.ItineraryDetails.DestinationLocation, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                                }

                                LoggingAdapter.WriteLog("RollBackMiles Success : " + lboolRollBackResponse, "BookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine exception in booking: " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "BookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                            {
                                strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                    lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjBookingRequest.ItineraryDetails.OriginLocation + "-" + pobjBookingRequest.ItineraryDetails.DestinationLocation + ", TRIP ID : " + lobjBookingResponse.PNRDetails.TripId + ", GDSPNR : " + lobjBookingResponse.PNRDetails.GDSPNR, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                            }

                            LoggingAdapter.WriteLog("RollBackMiles Success On Exception : " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "BookingLogCategory");

                            strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                            SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                        }
                        catch (Exception ex1)
                        {
                            LoggingAdapter.WriteLog("Communication engine exception in booking: " + lboolRollBackResponse, "BookingLogCategory");
                            strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                            SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                    {
                        strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                        if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                            lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjBookingRequest.ItineraryDetails.OriginLocation + "-" + pobjBookingRequest.ItineraryDetails.DestinationLocation, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    }

                    strFailureType += string.Format("{0}/", FailureType.REDEEMMILES);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(ex.StackTrace);
            }

            return lobjBookingResponse;
        }

        public BookingResponse BookFlightByInfiPay(BookingRequest pobjBookingRequest, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            bool IsBookingConfirm = true;
            string strRedeemMilesResponse = string.Empty;
            string strRollBackMilesResponse = string.Empty;
            bool lboolRollBackResponse = false;
            string strFailureType = "";

            BookingResponse lobjBookingResponse = new BookingResponse();
            PGHelper lobjPGHelper = new PGHelper();

            try
            {
                bool IsRedeem = true;
                List<BookingPaymentBreakage> lobjListOfBookingPaymentBreakage = new List<BookingPaymentBreakage>();
                for (int i = 0; i < pobjListOfRedemptionDetails.Count; i++)
                {
                    BookingPaymentBreakage lobjBookingPaymentBreakage = new BookingPaymentBreakage();
                    //strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption((float)(pobjListOfRedemptionDetails[i].Amount), (float)(pobjListOfRedemptionDetails[i].Amount), pobjListOfRedemptionDetails[i].Points, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, pobjBookingRequest.ItineraryDetails.OriginLocation + "-" + pobjBookingRequest.ItineraryDetails.DestinationLocation, Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.Air), pobjListOfRedemptionDetails[i].Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    pobjListOfRedemptionDetails[i].TransactionReference = pobjBookingRequest.ItineraryDetails.TransactionReference;
                    IsRedeem = IsRedeem && (pobjListOfRedemptionDetails[i].TransactionReference != "" && pobjListOfRedemptionDetails[i].TransactionReference != string.Empty);

                    lobjBookingPaymentBreakage.Currency = pobjListOfRedemptionDetails[i].DisplayCurrency;
                    lobjBookingPaymentBreakage.Amount = pobjListOfRedemptionDetails[i].Points;
                    //lobjBookingPaymentBreakage.TxnReference = strRedeemMilesResponse;
                    lobjBookingPaymentBreakage.TxnReference = pobjBookingRequest.ItineraryDetails.TransactionReference;
                    lobjListOfBookingPaymentBreakage.Add(lobjBookingPaymentBreakage);
                }

                BookingPaymentDetails lobjBookingPaymentDetails = new BookingPaymentDetails();
                lobjBookingPaymentDetails.BookingPaymentBreakageList = lobjListOfBookingPaymentBreakage;
                lobjBookingPaymentDetails.MemberId = pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjBookingPaymentDetails.PaymentType = PaymentType.Points;
                lobjBookingPaymentDetails.ServiceType = ServiceType.FLIGHT;
                lobjBookingPaymentDetails.Points = pobjListOfRedemptionDetails[0].Points;
                lobjBookingPaymentDetails.PointsTxnRefererence = pobjListOfRedemptionDetails[0].TransactionReference;
                if (IsRedeem)
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.Success;
                }
                else
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.PaymentDone;
                }

                pobjBookingRequest.ItineraryDetails.BookingPaymentDetails = lobjBookingPaymentDetails;

                if (IsRedeem)
                {
                    pobjBookingRequest.ItineraryDetails.TransactionReference = pobjListOfRedemptionDetails[0].TransactionReference;
                    IBECTClient lobjIBECTClient = new IBECTClient();

                    try
                    {
                        LoggingAdapter.WriteLog("RedeemMiles Success : " + strRedeemMilesResponse, "BookingLogCategory");
                        try
                        {
                            lobjBookingResponse = lobjIBECTClient.CreateBooking(pobjBookingRequest);
                        }
                        catch (Exception ex)
                        {
                            LoggingAdapter.WriteLog("CreateBooking Failure BookingFacade:" + ex.Message + ex.StackTrace);
                        }

                        if (lobjBookingResponse != null && lobjBookingResponse.PNRDetails.TripId != null && lobjBookingResponse.PNRDetails.TripId != string.Empty && lobjBookingResponse.PNRDetails.Status.Equals(1))
                        {
                            LoggingAdapter.WriteLog("Booking called Trip Id  : " + lobjBookingResponse.PNRDetails.TripId + "IsBookingConfirm:" + IsBookingConfirm, "BookingLogCategory");
                            IsBookingConfirm = true;
                        }
                        else
                        {
                            LoggingAdapter.WriteLog("IsBookingConfirm:" + IsBookingConfirm, "BookingLogCategory");
                            IsBookingConfirm = false;
                        }
                        if (IsBookingConfirm)
                        {
                            try
                            {
                                //communication Engine call for Email send
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateFlightEmailParameters(lobjBookingResponse);
                                SendEmail(lstEmailparameter, pobjMemberDetails, "FlightBooked");
                                //Communication Engine Call for Sms Sending
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine Email exception in booking ", "BookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                            }
                            try
                            {
                                List<string> lstSMSparameter = new List<string>();
                                lstSMSparameter = GenerateFlightSMSParameters(lobjBookingResponse);
                                SendSms(lstSMSparameter, pobjMemberDetails, "FlightBooked");
                                LoggingAdapter.WriteLog("Mail and SMS sent.", "BookingLogCategory");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine SMS exception in booking", "BookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.SMS);
                            }

                            if (strFailureType != "" && strFailureType != string.Empty)
                            {
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                            }
                        }
                        else
                        {
                            try
                            {
                                for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                                {
                                    strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                    if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                        lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjBookingRequest.ItineraryDetails.OriginLocation + "-" + pobjBookingRequest.ItineraryDetails.DestinationLocation, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                                }

                                LoggingAdapter.WriteLog("RollBackMiles Success : " + lboolRollBackResponse, "BookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine exception in booking: " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "BookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                            {
                                strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                    lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjBookingRequest.ItineraryDetails.OriginLocation + "-" + pobjBookingRequest.ItineraryDetails.DestinationLocation + ", TRIP ID : " + lobjBookingResponse.PNRDetails.TripId + ", GDSPNR : " + lobjBookingResponse.PNRDetails.GDSPNR, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                            }

                            LoggingAdapter.WriteLog("RollBackMiles Success On Exception : " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "BookingLogCategory");

                            strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                            SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                        }
                        catch (Exception ex1)
                        {
                            LoggingAdapter.WriteLog("Communication engine exception in booking: " + lboolRollBackResponse, "BookingLogCategory");
                            strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                            SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                    {
                        strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                        if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                            lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjBookingRequest.ItineraryDetails.OriginLocation + "-" + pobjBookingRequest.ItineraryDetails.DestinationLocation, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    }

                    strFailureType += string.Format("{0}/", FailureType.REDEEMMILES);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(ex.StackTrace);
            }

            return lobjBookingResponse;
        }


        public BookingResponse BookFlightByInfiVoucher(BookingRequest pobjBookingRequest, MemberDetails pobjMemberDetails)
        {
            bool IsBookingConfirm = true;
            string strFailureType = "";

            BookingResponse lobjBookingResponse = new BookingResponse();

            try
            {
                IBECTClient lobjIBECTClient = new IBECTClient();

                try
                {
                    lobjBookingResponse = lobjIBECTClient.CreateBooking(pobjBookingRequest);
                }
                catch (Exception ex)
                {
                    LoggingAdapter.WriteLog("CreateBooking Failure BookingFacade:" + ex.Message + ex.StackTrace);
                }

                if (lobjBookingResponse != null && lobjBookingResponse.PNRDetails.TripId != null && lobjBookingResponse.PNRDetails.TripId != string.Empty && lobjBookingResponse.PNRDetails.Status.Equals(1))
                {
                    LoggingAdapter.WriteLog("Booking called Trip Id  : " + lobjBookingResponse.PNRDetails.TripId + "IsBookingConfirm:" + IsBookingConfirm, "BookingLogCategory");
                    IsBookingConfirm = true;
                }
                else
                {
                    LoggingAdapter.WriteLog("IsBookingConfirm:" + IsBookingConfirm, "BookingLogCategory");
                    IsBookingConfirm = false;
                }
                if (IsBookingConfirm)
                {

                    try
                    {
                        //communication Engine call for Email send
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateFlightEmailParameters(lobjBookingResponse);
                        SendEmail(lstEmailparameter, pobjMemberDetails, "FlightBookedInfiVoucher");
                        //Communication Engine Call for Sms Sending
                    }
                    catch (Exception ex)
                    {
                        strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                    }
                    try
                    {
                        List<string> lstSMSparameter = new List<string>();
                        lstSMSparameter = GenerateFlightSMSParameters(lobjBookingResponse);
                        SendSms(lstSMSparameter, pobjMemberDetails, "FlightBooked");

                    }
                    catch (Exception ex)
                    {
                        strFailureType += string.Format("{0}/", FailureType.SMS);
                    }

                    if (strFailureType != "" && strFailureType != string.Empty)
                    {
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                    }
                }
                else
                {
                    try
                    {

                        strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                    }
                    catch (Exception ex)
                    {


                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                    }
                    //throw new ApplicationException(lstrBookingFailureMsg + " Your Transaction Reference Code: " + lobjBookingResponse.PNRDetails.BookingReference);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                }
                catch (Exception ex1)
                {
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                }
                //throw new ApplicationException(lstrBookingFailureMsg);
            }
            return lobjBookingResponse;
        }

        private List<string> GenerateFlightEmailParameters(BookingResponse pobjBookingResponse)
        {
            List<string> lstEmailparameter = new List<string>();
            lstEmailparameter.Add(Convert.ToString(pobjBookingResponse.BookingId));
            lstEmailparameter.Add("FlightBooked");

            return lstEmailparameter;
        }

        private List<string> GenerateFlightBookingFailedEmailParameter(BookingRequest pobjBookingRequest, BookingResponse pobjBookingResponse, MemberDetails pobjMemberDetails)
        {
            List<string> lstEmailparameter = new List<string>();
            //0
            lstEmailparameter.Add((pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference));
            //1
            lstEmailparameter.Add(pobjMemberDetails.FirstName + " " + pobjMemberDetails.LastName);
            //2
            lstEmailparameter.Add(Convert.ToString(pobjBookingRequest.ItineraryDetails.FareDetails.TotalPoints));
            //3
            lstEmailparameter.Add(pobjMemberDetails.MobileNumber);
            //4
            lstEmailparameter.Add(pobjBookingRequest.ItineraryDetails.CabinType);

            // Code for Departure table
            List<FlightSegment> lobjFlightSegmentlst = new List<FlightSegment>();
            lobjFlightSegmentlst = pobjBookingRequest.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments;
            string strDepartute = string.Empty;
            string strImagePath = GetAppSettingValue("ClientImageUrl");

            for (int i = 0; i < lobjFlightSegmentlst.Count; i++)
            {
                strDepartute += "<tr style='font-family:Arial; font-size:14px; color:#000000;' >";
                strDepartute += "<td width='20' bgcolor='#ffffff' >&nbsp;</td>";
                strDepartute += "<td width='5' bgcolor='#ffffff'>&nbsp;</td>";
                strDepartute += "<td bgcolor='#ffffff'  align='center'  ><img width='30' height='30'  src='" + strImagePath + lobjFlightSegmentlst[i].Carrier.CarrierLogoPath + "'> <br/>" + lobjFlightSegmentlst[i].Carrier.CarrierName + " <br/>" + lobjFlightSegmentlst[i].Carrier.CarrierCode + lobjFlightSegmentlst[i].FlightNo + "</td>";
                strDepartute += "<td bgcolor='#ffffff' style='font-family:Arial; font-size:14px; color:#000000;'>" + lobjFlightSegmentlst[i].DepartureAirField.City + " (" + lobjFlightSegmentlst[i].DepartureAirField.IATACode + ")" + "<br/>" + lobjFlightSegmentlst[i].DepartureAirField.AirportName + ", " + lobjFlightSegmentlst[i].DepartureAirField.City + "</td>";
                strDepartute += "<td bgcolor='#ffffff' style='font-family:Arial; font-size:14px; color:#000000;'>" + lobjFlightSegmentlst[i].ArrivalAirField.City + " (" + lobjFlightSegmentlst[i].ArrivalAirField.IATACode + ")" + "<br/>" + lobjFlightSegmentlst[i].ArrivalAirField.AirportName + ", " + lobjFlightSegmentlst[i].ArrivalAirField.City + "</td>";
                strDepartute += "<td bgcolor='#ffffff' style='font-family:Arial; font-size:14px; color:#000000;'>" + lobjFlightSegmentlst[i].DepartureDate.ToString("dd/MM/yyyy") + "<br/>" + lobjFlightSegmentlst[i].DepartureDate.ToString("HH:mm") + "</td>";
                strDepartute += "<td bgcolor='#ffffff' style='font-family:Arial; font-size:14px; color:#000000;'>" + lobjFlightSegmentlst[i].ArrivalDate.ToString("dd/MM/yyyy") + "<br/>" + lobjFlightSegmentlst[i].ArrivalDate.ToString("HH:mm") + "</td>";
                strDepartute += "<td bgcolor='#ffffff' style='font-family:Arial; font-size:14px; color:#000000;'>" + lobjFlightSegmentlst[i].Carrier.EquipmentType + "</td>";
                strDepartute += "</tr>";


            }
            //5
            lstEmailparameter.Add(strDepartute);

            if (pobjBookingRequest.ItineraryDetails.ListOfFlightDetails.Count > 1)
            {
                if (pobjBookingRequest.ItineraryDetails != null && pobjBookingRequest.ItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments != null)
                {
                    // Code for Arrival Table
                    string strReturn = string.Empty;
                    strReturn = "<tr> <td colspan='8' bgcolor='#CCCCCC' style='font-family: Arial; font-size: 14px;color: #000000;'>";
                    strReturn += "<table width='720' border='0' cellpadding='0' cellspacing='0' style='font-family: Arial;font-size: 14px; color: #000000;'>";
                    strReturn += "<tr> <td width='20' bgcolor='#CCCCC' >&nbsp;</td>";
                    strReturn += "<td width='5' bgcolor='#CCCCCC'> &nbsp;</td>";
                    strReturn += "<td width='400' colspan='8'  height='25' bgcolor='#CCCCCC' style='font-family:Arial; font-size:14px; color:#000000;font-weight: bold;'> Itinerary Details (Return) </td>";
                    strReturn += "<td align='left' width='80' bgcolor='#CCCCCC' style='font-family:Arial; font-size:14px; color:#000000;'>Class :</td>";
                    strReturn += "<td align='left' width='150'  bgcolor='#CCCCCC' style='font-family:Arial; font-size:14px; color:#000000;'>" + pobjBookingRequest.ItineraryDetails.CabinType + "</td>";
                    strReturn += "</tr></table>";
                    strReturn += "</tr>";
                    //6
                    lstEmailparameter.Add(strReturn);
                    lobjFlightSegmentlst = pobjBookingRequest.ItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments;

                    if (lobjFlightSegmentlst != null && lobjFlightSegmentlst.Count > 0)
                    {
                        // Arrival Header Row
                        string strArrival = string.Empty;
                        strArrival += "<tr height='25'>";
                        strArrival += "<td width='20' bgcolor='#CCCCCC' >&nbsp;</td>";
                        strArrival += "<td width='11' bgcolor='#CCCCCC' >&nbsp;</td>";
                        strArrival += "<td width='25' bgcolor='#CCCCCC' style='font-family:Arial; font-size:14px; color:#000000;'>Flight</td>";
                        strArrival += "<td width='172' bgcolor='#CCCCCC' style='font-family:Arial; font-size:14px; color:#000000;'>Departure</td>";
                        strArrival += "<td width='192' bgcolor='#CCCCCC' style='font-family:Arial; font-size:14px; color:#000000;'> Arrival </td>";
                        strArrival += "<td width='122' bgcolor='#CCCCCC' style='font-family:Arial; font-size:14px; color:#000000;'> Departure Time </td>";
                        strArrival += "<td width='90' bgcolor='#CCCCCC' style='font-family:Arial; font-size:14px; color:#000000;'> Arrival Time </td>";
                        strArrival += "<td width='90' bgcolor='#CCCCCC' style='font-family:Arial; font-size:14px; color:#000000;'>Aircraft Type</td>";
                        strArrival += "	</tr>";

                        for (int j = 0; j < lobjFlightSegmentlst.Count; j++)
                        {
                            strArrival += "<tr style='font-family:Arial; font-size:14px; color:#000000;' >";
                            strArrival += "<td width='20' bgcolor='#ffffff' >&nbsp;</td>";
                            strArrival += "<td width='11' bgcolor='#ffffff'>&nbsp;</td>";
                            strArrival += "<td width='25' bgcolor='#ffffff'  align='center'><img width='30' height='30' src='" + strImagePath + lobjFlightSegmentlst[j].Carrier.CarrierLogoPath + "'> <br/>" + lobjFlightSegmentlst[j].Carrier.CarrierName + "<br/>" + lobjFlightSegmentlst[j].Carrier.CarrierCode + lobjFlightSegmentlst[j].FlightNo + "</td>";
                            strArrival += "<td width='172' bgcolor='#ffffff' style='font-family:Arial; font-size:14px; color:#000000;'>" + lobjFlightSegmentlst[j].DepartureAirField.City + " (" + lobjFlightSegmentlst[j].DepartureAirField.IATACode + ")" + "<br/>" + lobjFlightSegmentlst[j].DepartureAirField.AirportName + ", " + lobjFlightSegmentlst[j].DepartureAirField.City + "</td>";
                            strArrival += "<td width='192' bgcolor='#ffffff' style='font-family:Arial; font-size:14px; color:#000000;'>" + lobjFlightSegmentlst[j].ArrivalAirField.City + " (" + lobjFlightSegmentlst[j].ArrivalAirField.IATACode + ")" + "<br/>" + lobjFlightSegmentlst[j].ArrivalAirField.AirportName + ", " + lobjFlightSegmentlst[j].ArrivalAirField.City + "</td>";
                            strArrival += "<td width='122' bgcolor='#ffffff' style='font-family:Arial; font-size:14px; color:#000000;'>" + lobjFlightSegmentlst[j].DepartureDate.ToString("dd/MM/yyyy") + "<br/>" + lobjFlightSegmentlst[j].DepartureDate.ToString("HH:mm") + "</td>";
                            strArrival += "<td width='90' bgcolor='#ffffff' style='font-family:Arial; font-size:14px; color:#000000;'>" + lobjFlightSegmentlst[j].ArrivalDate.ToString("dd/MM/yyyy") + "<br/>" + lobjFlightSegmentlst[j].ArrivalDate.ToString("HH:mm") + "</td>";
                            strArrival += "<td  width='90' bgcolor='#ffffff' style='font-family:Arial; font-size:14px; color:#000000;'>" + lobjFlightSegmentlst[j].Carrier.EquipmentType + "</td>";
                            strArrival += "</tr>";

                        }
                        //7
                        lstEmailparameter.Add(strArrival);
                        //8
                        lstEmailparameter.Add("");
                    }
                }
            }
            else
            {
                //6
                lstEmailparameter.Add("");
                //7
                lstEmailparameter.Add("");
                //8
                lstEmailparameter.Add("");

            }
            // Code For Travellor Info

            List<PassengerDetails> lobjListOfPassengerDetails = new List<PassengerDetails>();
            if (pobjBookingRequest.ItineraryDetails.TravelerInfo != null)
            {
                string lstrPaxtype = "";
                string strPaxInfo = string.Empty;
                lobjListOfPassengerDetails = pobjBookingRequest.ItineraryDetails.TravelerInfo;
                for (int k = 0; k < lobjListOfPassengerDetails.Count; k++)
                {

                    if (Convert.ToString(lobjListOfPassengerDetails[k].PaxType) == "ADT")
                    {
                        lstrPaxtype = "Adult";
                    }
                    else if (Convert.ToString(lobjListOfPassengerDetails[k].PaxType) == "CHD")
                    {
                        lstrPaxtype = "Child";
                    }
                    else if (Convert.ToString(lobjListOfPassengerDetails[k].PaxType) == "INF")
                    {
                        lstrPaxtype = "Infant";
                    }

                    strPaxInfo += "<tr>";
                    strPaxInfo += " <td width='20' bgcolor='#ffffff' >&nbsp;</td>";
                    strPaxInfo += " <td width='11' bgcolor='#ffffff'>&nbsp;</td>";
                    strPaxInfo += "<td align='left'  width='100' bgcolor='#ffffff'> " + lstrPaxtype + " </td>";
                    strPaxInfo += "<td align='left' width='280' bgcolor='#ffffff'>" + lobjListOfPassengerDetails[k].LastName + " " + lobjListOfPassengerDetails[k].FirstName + "</td>";
                    strPaxInfo += "<td align='left'  width='145'  bgcolor='#ffffff'>" + lobjListOfPassengerDetails[k].TicketNo + " </td>";
                    strPaxInfo += "<td align='left' width='73'  bgcolor='#ffffff'>" + lobjListOfPassengerDetails[k].Gender + " </td>";
                    strPaxInfo += "<td width='73'  bgcolor='#ffffff'> " + Convert.ToString(lobjListOfPassengerDetails[k].Age) + " </td>";
                    strPaxInfo += "</tr>";
                }
                //9
                lstEmailparameter.Add(strPaxInfo);

            }
            //10
            lstEmailparameter.Add(pobjBookingResponse.PNRDetails.BookingReference);

            return lstEmailparameter;
        }

        private List<string> GenerateFlightSMSParameters(BookingResponse pobjBookingResponse)
        {
            List<string> lstSmsparameter = new List<string>();

            lstSmsparameter.Add(Convert.ToString(pobjBookingResponse.PNRDetails.BookingReference));

            return lstSmsparameter;
        }

        //Added For Infipay
        public BookingResponse BookForFlight(BookingRequest pobjBookingRequest, MemberDetails pobjMemberDetails)
        {
            bool IsBookingConfirm = true;
            string strFailureType = "";

            BookingResponse lobjBookingResponse = new BookingResponse();

            try
            {
                IBECTClient lobjIBECTClient = new IBECTClient();

                try
                {
                    lobjBookingResponse = lobjIBECTClient.CreateBooking(pobjBookingRequest);
                }
                catch (Exception ex)
                {
                    LoggingAdapter.WriteLog("CreateBooking Failure BookingFacade:" + ex.Message + ex.StackTrace);
                }

                if (lobjBookingResponse != null && lobjBookingResponse.PNRDetails.TripId != null && lobjBookingResponse.PNRDetails.TripId != string.Empty && lobjBookingResponse.PNRDetails.Status.Equals(1))
                {
                    LoggingAdapter.WriteLog("Booking called Trip Id  : " + lobjBookingResponse.PNRDetails.TripId + "IsBookingConfirm:" + IsBookingConfirm, "BookingLogCategory");
                    IsBookingConfirm = true;
                }
                else
                {
                    LoggingAdapter.WriteLog("IsBookingConfirm:" + IsBookingConfirm, "BookingLogCategory");
                    IsBookingConfirm = false;
                }
                if (IsBookingConfirm)
                {

                    try
                    {
                        //communication Engine call for Email send
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateFlightEmailParameters(lobjBookingResponse);
                        SendEmail(lstEmailparameter, pobjMemberDetails, "FlightBooked");
                        //Communication Engine Call for Sms Sending
                    }
                    catch (Exception ex)
                    {
                        strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                    }
                    try
                    {
                        List<string> lstSMSparameter = new List<string>();
                        lstSMSparameter = GenerateFlightSMSParameters(lobjBookingResponse);
                        SendSms(lstSMSparameter, pobjMemberDetails, "FlightBooked");

                    }
                    catch (Exception ex)
                    {
                        strFailureType += string.Format("{0}/", FailureType.SMS);
                    }

                    if (strFailureType != "" && strFailureType != string.Empty)
                    {
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                    }
                }
                else
                {
                    try
                    {

                        strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                    }
                    catch (Exception ex)
                    {


                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {


                    strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                }
                catch (Exception ex1)
                {


                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateFlightBookingFailedEmailParameter(pobjBookingRequest, lobjBookingResponse, pobjMemberDetails);
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "FlightBookingFailed");
                }
            }



            return lobjBookingResponse;
        }

        #endregion

        #region Hotel

        public HotelBookingResponse BookHotel(HotelBookingRequest pobjBookingRequest, Hotel pobjHotel, HotelSearchRequest pobjHotelSearchRequest, MemberDetails pobjMemberDetails, Customer pobjCustomer, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            bool IsBookingConfirm = true;
            string strRedeemMilesResponse = string.Empty;
            bool lboolRollBackResponse = false;
            string strRollBackMilesResponse = string.Empty;

            string strFailureType = "";

            HotelBookingResponse lobjBookingResponse = new HotelBookingResponse();
            PGHelper lobjPGHelper = new PGHelper();

            try
            {
                bool IsRedeem = true;
                List<BookingPaymentBreakage> lobjListOfBookingPaymentBreakage = new List<BookingPaymentBreakage>();
                for (int i = 0; i < pobjListOfRedemptionDetails.Count; i++)
                {
                    strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption((float)(pobjListOfRedemptionDetails[i].Amount), (float)(pobjListOfRedemptionDetails[i].Amount), pobjListOfRedemptionDetails[i].Points, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, pobjHotel.basicinfo.hotelname, Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.Hotel), pobjListOfRedemptionDetails[i].Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    pobjListOfRedemptionDetails[i].TransactionReference = strRedeemMilesResponse;
                    //pobjListOfRedemptionDetails[i].TransactionReference = pobjBookingRequest.BookingPaymentDetails.PointsTxnRefererence;
                    IsRedeem = IsRedeem && (pobjListOfRedemptionDetails[i].TransactionReference != "" && pobjListOfRedemptionDetails[i].TransactionReference != string.Empty);
                    BookingPaymentBreakage lobjBookingPaymentBreakage = new BookingPaymentBreakage();
                    lobjBookingPaymentBreakage.Currency = pobjListOfRedemptionDetails[i].DisplayCurrency;
                    lobjBookingPaymentBreakage.Amount = pobjListOfRedemptionDetails[i].Points;
                    lobjBookingPaymentBreakage.TxnReference = strRedeemMilesResponse;
                    //lobjBookingPaymentBreakage.TxnReference = pobjBookingRequest.BookingPaymentDetails.PointsTxnRefererence;
                    lobjListOfBookingPaymentBreakage.Add(lobjBookingPaymentBreakage);
                }

                IBECTHotelClient lobjIBECTClient = new IBECTHotelClient();

                BookingPaymentDetails lobjBookingPaymentDetails = new BookingPaymentDetails();
                lobjBookingPaymentDetails.BookingPaymentBreakageList = lobjListOfBookingPaymentBreakage;
                lobjBookingPaymentDetails.MemberId = pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjBookingPaymentDetails.PaymentType = PaymentType.Points;
                lobjBookingPaymentDetails.ServiceType = ServiceType.HOTEL;
                lobjBookingPaymentDetails.Points = pobjListOfRedemptionDetails[0].Points;
                lobjBookingPaymentDetails.PointsTxnRefererence = pobjListOfRedemptionDetails[0].TransactionReference;

                if (IsRedeem)
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.Success;
                }
                else
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.PaymentDone;
                }

                pobjBookingRequest.BookingPaymentDetails = lobjBookingPaymentDetails;

                if (IsRedeem)
                {
                    try
                    {
                        LoggingAdapter.WriteLog("Hotel RedeemMiles Success : " + strRedeemMilesResponse, "HotelBookingLogCategory");

                        try
                        {
                            lobjBookingResponse = lobjIBECTClient.GetHotelBookingResponse(pobjBookingRequest, pobjHotel, pobjHotelSearchRequest, pobjCustomer, pobjListOfRedemptionDetails[0].TransactionReference);
                        }
                        catch (Exception ex)
                        {
                            LoggingAdapter.WriteLog("Hotel GetHotelBookingResponse in BookingFacade Failure" + ex.Message + ex.StackTrace);
                        }

                        if (lobjBookingResponse != null && lobjBookingResponse.BookingResponse.bookingid != null && lobjBookingResponse.BookingResponse.bookingid != string.Empty)
                        {
                            LoggingAdapter.WriteLog("Booking called Booking Id  : " + lobjBookingResponse.BookingResponse.bookingid, "HotelBookingLogCategory");
                            IsBookingConfirm = lobjBookingResponse.BookingResponse.confirmationnumber != "" && lobjBookingResponse.BookingResponse.confirmationnumber != null && lobjBookingResponse.BookingResponse.bookingid != "" && lobjBookingResponse.BookingResponse.confirmationnumber != null;
                        }
                        else
                            IsBookingConfirm = false;

                        if (IsBookingConfirm)
                        {
                            try
                            {
                                //communication Engine call for Email send
                                List<string> lstEmailparameter = new List<string>();
                                lobjBookingResponse.BookingPaymentDetails = pobjBookingRequest.BookingPaymentDetails;
                                lstEmailparameter = GenerateHotelEmailParameters(lobjBookingResponse, pobjHotel, pobjHotelSearchRequest, pobjCustomer, pobjBookingRequest.BookingPaymentDetails.BookingPaymentBreakageList);
                                SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBooked");
                                LoggingAdapter.WriteLog("Mail sent. ", "HotelBookingLogCategory");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine exception in booking ", "HotelBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                            }
                            try
                            {
                                //Communication Engine Call for Sms Sending
                                List<string> lstSMSparameter = new List<string>();
                                lstSMSparameter = GenerateHotelSMSParameters(lobjBookingResponse);
                                SendSms(lstSMSparameter, pobjMemberDetails, "HotelBooked");
                                LoggingAdapter.WriteLog("SMS sent.", "HotelBookingLogCategory");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine exception in booking ", "HotelBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.SMS);
                            }

                            if (strFailureType != "" && strFailureType != string.Empty)
                            {
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                            }
                        }
                        else
                        {
                            try
                            {

                                for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                                {
                                    strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                    if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                        lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjHotel.basicinfo.hotelname, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                                }

                                LoggingAdapter.WriteLog("RollBackMiles Success : " + lboolRollBackResponse, "HotelBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                                //SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("RollBackMiles Exception : " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "HotelBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                                //SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                            {
                                strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                    lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjHotel.basicinfo.hotelname, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                            }

                            LoggingAdapter.WriteLog("RollBackMiles Success On Exception : " + strRollBackMilesResponse + " \n Exception \n" + ex.StackTrace, "HotelBookingLogCategory");
                            strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                            //SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                            SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                        }
                        catch (Exception exp)
                        {
                            LoggingAdapter.WriteLog("RollBackMiles Exception : " + strRollBackMilesResponse + " \n Exception \n" + exp.StackTrace, "HotelBookingLogCategory");
                            strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                            //SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                            SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                    {
                        strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                        if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                            lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjHotel.basicinfo.hotelname, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    }

                    strFailureType += string.Format("{0}/", FailureType.REDEEMMILES);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                    //SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(ex.StackTrace);
            }

            return lobjBookingResponse;
        }

        public HotelBookingResponse BookHotelByInfiPay(HotelBookingRequest pobjBookingRequest, Hotel pobjHotel, HotelSearchRequest pobjHotelSearchRequest, MemberDetails pobjMemberDetails, Customer pobjCustomer, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            bool IsBookingConfirm = true;
            string strRedeemMilesResponse = string.Empty;
            bool lboolRollBackResponse = false;
            string strRollBackMilesResponse = string.Empty;

            string strFailureType = "";

            HotelBookingResponse lobjBookingResponse = new HotelBookingResponse();
            PGHelper lobjPGHelper = new PGHelper();

            try
            {
                bool IsRedeem = true;
                List<BookingPaymentBreakage> lobjListOfBookingPaymentBreakage = new List<BookingPaymentBreakage>();
                for (int i = 0; i < pobjListOfRedemptionDetails.Count; i++)
                {
                    //strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption((float)(pobjListOfRedemptionDetails[i].Amount), (float)(pobjListOfRedemptionDetails[i].Amount), pobjListOfRedemptionDetails[i].Points, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, pobjHotel.basicinfo.hotelname, Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.Hotel), pobjListOfRedemptionDetails[i].Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    //pobjListOfRedemptionDetails[i].TransactionReference = strRedeemMilesResponse;
                    pobjListOfRedemptionDetails[i].TransactionReference = pobjBookingRequest.BookingPaymentDetails.PointsTxnRefererence;
                    IsRedeem = IsRedeem && (pobjListOfRedemptionDetails[i].TransactionReference != "" && pobjListOfRedemptionDetails[i].TransactionReference != string.Empty);
                    BookingPaymentBreakage lobjBookingPaymentBreakage = new BookingPaymentBreakage();
                    lobjBookingPaymentBreakage.Currency = pobjListOfRedemptionDetails[i].DisplayCurrency;
                    lobjBookingPaymentBreakage.Amount = pobjListOfRedemptionDetails[i].Points;
                    //lobjBookingPaymentBreakage.TxnReference = strRedeemMilesResponse;
                    lobjBookingPaymentBreakage.TxnReference = pobjBookingRequest.BookingPaymentDetails.PointsTxnRefererence;
                    lobjListOfBookingPaymentBreakage.Add(lobjBookingPaymentBreakage);
                }

                IBECTHotelClient lobjIBECTClient = new IBECTHotelClient();

                BookingPaymentDetails lobjBookingPaymentDetails = new BookingPaymentDetails();
                lobjBookingPaymentDetails.BookingPaymentBreakageList = lobjListOfBookingPaymentBreakage;
                lobjBookingPaymentDetails.MemberId = pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjBookingPaymentDetails.PaymentType = PaymentType.Points;
                lobjBookingPaymentDetails.ServiceType = ServiceType.HOTEL;
                lobjBookingPaymentDetails.Points = pobjListOfRedemptionDetails[0].Points;
                lobjBookingPaymentDetails.PointsTxnRefererence = pobjListOfRedemptionDetails[0].TransactionReference;

                if (IsRedeem)
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.Success;
                }
                else
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.PaymentDone;
                }

                pobjBookingRequest.BookingPaymentDetails = lobjBookingPaymentDetails;

                if (IsRedeem)
                {
                    try
                    {
                        LoggingAdapter.WriteLog("Hotel RedeemMiles Success : " + strRedeemMilesResponse, "HotelBookingLogCategory");

                        try
                        {
                            lobjBookingResponse = lobjIBECTClient.GetHotelBookingResponse(pobjBookingRequest, pobjHotel, pobjHotelSearchRequest, pobjCustomer, pobjListOfRedemptionDetails[0].TransactionReference);
                        }
                        catch (Exception ex)
                        {
                            LoggingAdapter.WriteLog("Hotel GetHotelBookingResponse in BookingFacade Failure" + ex.Message + ex.StackTrace);
                        }

                        if (lobjBookingResponse != null && lobjBookingResponse.BookingResponse.bookingid != null && lobjBookingResponse.BookingResponse.bookingid != string.Empty)
                        {
                            LoggingAdapter.WriteLog("Booking called Booking Id  : " + lobjBookingResponse.BookingResponse.bookingid, "HotelBookingLogCategory");
                            IsBookingConfirm = lobjBookingResponse.BookingResponse.confirmationnumber != "" && lobjBookingResponse.BookingResponse.confirmationnumber != null && lobjBookingResponse.BookingResponse.bookingid != "" && lobjBookingResponse.BookingResponse.confirmationnumber != null;
                        }
                        else
                            IsBookingConfirm = false;

                        if (IsBookingConfirm)
                        {
                            try
                            {
                                //communication Engine call for Email send
                                List<string> lstEmailparameter = new List<string>();
                                lobjBookingResponse.BookingPaymentDetails = pobjBookingRequest.BookingPaymentDetails;
                                lstEmailparameter = GenerateHotelEmailParameters(lobjBookingResponse, pobjHotel, pobjHotelSearchRequest, pobjCustomer, pobjBookingRequest.BookingPaymentDetails.BookingPaymentBreakageList);
                                SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBooked");
                                LoggingAdapter.WriteLog("Mail sent. ", "HotelBookingLogCategory");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine exception in booking ", "HotelBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                            }
                            try
                            {
                                //Communication Engine Call for Sms Sending
                                List<string> lstSMSparameter = new List<string>();
                                lstSMSparameter = GenerateHotelSMSParameters(lobjBookingResponse);
                                SendSms(lstSMSparameter, pobjMemberDetails, "HotelBooked");
                                LoggingAdapter.WriteLog("SMS sent.", "HotelBookingLogCategory");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine exception in booking ", "HotelBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.SMS);
                            }

                            if (strFailureType != "" && strFailureType != string.Empty)
                            {
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                            }
                        }
                        else
                        {
                            try
                            {

                                for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                                {
                                    strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                    if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                        lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjHotel.basicinfo.hotelname, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                                }

                                LoggingAdapter.WriteLog("RollBackMiles Success : " + lboolRollBackResponse, "HotelBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                                //SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("RollBackMiles Exception : " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "HotelBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                                //SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                                SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                            {
                                strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                    lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjHotel.basicinfo.hotelname, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                            }

                            LoggingAdapter.WriteLog("RollBackMiles Success On Exception : " + strRollBackMilesResponse + " \n Exception \n" + ex.StackTrace, "HotelBookingLogCategory");
                            strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                            //SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                            SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                        }
                        catch (Exception exp)
                        {
                            LoggingAdapter.WriteLog("RollBackMiles Exception : " + strRollBackMilesResponse + " \n Exception \n" + exp.StackTrace, "HotelBookingLogCategory");
                            strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                            //SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                            SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                    {
                        strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                        if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                            lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjHotel.basicinfo.hotelname, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    }

                    strFailureType += string.Format("{0}/", FailureType.REDEEMMILES);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                    //SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(ex.StackTrace);
            }

            return lobjBookingResponse;
        }


        public HotelBookingResponse BookHotelByInfiVoucher(HotelBookingRequest pobjBookingRequest, Hotel pobjHotel, HotelSearchRequest pobjHotelSearchRequest, MemberDetails pobjMemberDetails, Customer pobjCustomer)
        {
            bool IsBookingConfirm = true;
            string strFailureType = "";
            string pstrReferenceId = pobjBookingRequest.BookingPaymentDetails.CashTxnRefererence;
            HotelBookingResponse lobjBookingResponse = new HotelBookingResponse();
            try
            {
                IBECTHotelClient lobjIBECTClient = new IBECTHotelClient();

                try
                {
                    lobjBookingResponse = lobjIBECTClient.GetHotelBookingResponse(pobjBookingRequest, pobjHotel, pobjHotelSearchRequest, pobjCustomer, pstrReferenceId);
                }
                catch (Exception ex)
                {
                    LoggingAdapter.WriteLog("Hotel GetHotelBookingResponse in BookingFacade Failure" + ex.Message + ex.StackTrace);
                }

                if (lobjBookingResponse != null && lobjBookingResponse.BookingResponse.bookingid != null && lobjBookingResponse.BookingResponse.bookingid != string.Empty)
                {
                    LoggingAdapter.WriteLog("Booking called Booking Id  : " + lobjBookingResponse.BookingResponse.bookingid, "HotelBookingLogCategory");
                    IsBookingConfirm = lobjBookingResponse.BookingResponse.confirmationnumber != "" && lobjBookingResponse.BookingResponse.confirmationnumber != null && lobjBookingResponse.BookingResponse.bookingid != "" && lobjBookingResponse.BookingResponse.bookingid != null;
                }
                else
                    IsBookingConfirm = false;

                if (IsBookingConfirm)
                {
                    try
                    {
                        //communication Engine call for Email send
                        List<string> lstEmailparameter = new List<string>();
                        lobjBookingResponse.BookingPaymentDetails = pobjBookingRequest.BookingPaymentDetails;
                        lstEmailparameter = GenerateHotelEmailParameters(lobjBookingResponse, pobjHotel, pobjHotelSearchRequest, pobjCustomer, pobjBookingRequest.BookingPaymentDetails.BookingPaymentBreakageList);
                        SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBookedInfiVoucher");

                    }
                    catch (Exception ex)
                    {

                        strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                    }
                    try
                    {
                        //Communication Engine Call for Sms Sending
                        List<string> lstSMSparameter = new List<string>();
                        lstSMSparameter = GenerateHotelSMSParameters(lobjBookingResponse);
                        SendSms(lstSMSparameter, pobjMemberDetails, "HotelBooked");

                    }
                    catch (Exception ex)
                    {

                        strFailureType += string.Format("{0}/", FailureType.SMS);
                    }

                    if (strFailureType != "" && strFailureType != string.Empty)
                    {
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);

                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                    }
                }
                else
                {
                    try
                    {

                        strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);

                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                    }
                    catch (Exception ex)
                    {
                        strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);

                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                    }
                    //throw new ApplicationException(lstrBookingFailureMsg + " Your Transaction Reference Code: " + lobjBookingResponse.BookingResponse.TransactionRefCode);
                }
            }
            catch (Exception ex)
            {
                try
                {

                    strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                }
                catch (Exception exp)
                {
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                }
                //throw new ApplicationException(lstrBookingFailureMsg);
            }


            return lobjBookingResponse;
        }

        private List<string> GenerateHotelEmailParameters(HotelBookingResponse pobjBookingResponse, Hotel pobjHotel, HotelSearchRequest pobjSearchRequest, Customer pobjCustomer, List<BookingPaymentBreakage> pobjListOfBookingPaymentBreakage)
        {
            List<string> lstEmailparameter = new List<string>();

            //0
            lstEmailparameter.Add(pobjBookingResponse.BookingResponse.TransactionRefCode);
            //1
            lstEmailparameter.Add(pobjCustomer.city);
            //2
            lstEmailparameter.Add(pobjCustomer.email);
            //3
            lstEmailparameter.Add(pobjCustomer.title + " " + pobjCustomer.firstname + " " + pobjCustomer.lastname);
            //4
            lstEmailparameter.Add(Convert.ToString(pobjSearchRequest.SearchRequest.NoOfRooms));
            //5
            lstEmailparameter.Add(Convert.ToString((pobjSearchRequest.SearchRequest.CheckOutDate - pobjSearchRequest.SearchRequest.CheckInDate).Days));
            //6
            lstEmailparameter.Add(pobjSearchRequest.SearchRequest.CheckInDate.ToString("dd/MM/yyyy"));
            //7
            lstEmailparameter.Add(pobjSearchRequest.SearchRequest.CheckOutDate.ToString("dd/MM/yyyy")); //
            //8
            lstEmailparameter.Add(Convert.ToString(pobjHotel.roomrates.RoomRate[0].TotalPoints));
            //9
            lstEmailparameter.Add(pobjHotel.basicinfo.hotelname);
            //10
            lstEmailparameter.Add(pobjHotel.basicinfo.address);
            //11
            lstEmailparameter.Add(pobjHotel.basicinfo.communicationinfo.phone);
            //12
            lstEmailparameter.Add(pobjHotel.basicinfo.communicationinfo.fax);
            //13
            lstEmailparameter.Add(pobjHotel.basicinfo.starrating);
            //14
            lstEmailparameter.Add(pobjHotel.otherinfo.locationinfo.latitude);
            //15
            lstEmailparameter.Add(pobjHotel.otherinfo.locationinfo.longitude);
            //16
            lstEmailparameter.Add(pobjHotel.roomrates.RoomRate[0].roomtype.roomdescription);
            //17
            lstEmailparameter.Add(pobjHotel.SpecialRequest);
            //18
            lstEmailparameter.Add(pobjBookingResponse.BookingResponse.bookingid + "/" + pobjBookingResponse.BookingResponse.confirmationnumber);
            //19
            string strPaymentDetailsHtml = "<tr><td width='2%'>&nbsp;</td><td width='56%' align='left' valign='top' style='font-family: arial; padding-top: 5px;font-size:12px;'>{0}</td><td width='40%' align='left' valign='middle' style='font-family: arial; font-size: 12px;text-align: left;'>{1}</td><td width='2%'>&nbsp;</td></tr>";
            string strPaymentDetails = string.Empty;

            for (int i = 0; i < pobjListOfBookingPaymentBreakage.Count; i++)
            {
                //if (!pobjListOfBookingPaymentBreakage[i].Currency.Equals("QAR"))
                //{
                //    strPaymentDetails += string.Format(strPaymentDetailsHtml, pobjListOfBookingPaymentBreakage[i].Currency + " Points", Convert.ToInt32(pobjListOfBookingPaymentBreakage[i].Amount));
                //}
                //else
                //{
                strPaymentDetails += string.Format(strPaymentDetailsHtml, pobjListOfBookingPaymentBreakage[i].Currency, pobjListOfBookingPaymentBreakage[i].Amount);
                //}
            }

            lstEmailparameter.Add(strPaymentDetails);

            return lstEmailparameter;
        }

        private List<string> GenerateHotelSMSParameters(HotelBookingResponse plobBookingResponse)
        {
            List<string> lstSMSparameter = new List<string>();
            lstSMSparameter.Add(Convert.ToString(plobBookingResponse.BookingResponse.confirmationnumber));
            return lstSMSparameter;
        }

        private List<string> GenerateHotelBookingFailedEmailParameters(HotelBookingResponse pobjBookingResponse, Hotel pobjHotel, MemberDetails pobjMemberDetails, HotelSearchRequest pobjSearchRequest, Customer pobjCustomer, string pstrFailureType)
        {
            List<string> lstEmailparameter = new List<string>();

            //0
            lstEmailparameter.Add((pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference));
            //1
            lstEmailparameter.Add(pobjMemberDetails.LastName);
            //2
            lstEmailparameter.Add(Convert.ToString(pobjHotel.roomrates.RoomRate[0].TotalPoints));
            //3
            lstEmailparameter.Add(pobjMemberDetails.MobileNumber);
            //4
            lstEmailparameter.Add(pobjHotel.basicinfo.hotelname);
            //5
            lstEmailparameter.Add(pobjHotel.basicinfo.city);
            //6
            lstEmailparameter.Add(Convert.ToString(pobjSearchRequest.SearchRequest.NoOfRooms));
            //7
            int TotalAdult = 0;
            string[] arrayAdultPerRoom = pobjSearchRequest.SearchRequest.AdultPerRoom.Split(',');
            for (int i = 0; i < arrayAdultPerRoom.Count(); i++)
            {
                TotalAdult = TotalAdult + Convert.ToInt32(arrayAdultPerRoom[i]);
            }
            lstEmailparameter.Add(Convert.ToString(TotalAdult));
            //8
            int TotalChild = 0;
            string[] arrayChildPerRoom = pobjSearchRequest.SearchRequest.ChildrenPerRoom.Split(',');
            for (int i = 0; i < arrayChildPerRoom.Count(); i++)
            {
                TotalChild = TotalChild + Convert.ToInt32(arrayChildPerRoom[i]);
            }
            lstEmailparameter.Add(Convert.ToString(TotalChild));
            //9
            lstEmailparameter.Add(pobjSearchRequest.SearchRequest.CheckInDate.ToString("dd/MM/yyyy"));
            //10
            lstEmailparameter.Add(pobjSearchRequest.SearchRequest.CheckOutDate.ToString("dd/MM/yyyy"));
            //11
            lstEmailparameter.Add(pobjCustomer.title + pobjCustomer.firstname + " " + pobjCustomer.lastname);
            //12
            lstEmailparameter.Add(pobjCustomer.city);
            //13
            lstEmailparameter.Add(pobjCustomer.country);
            //14
            lstEmailparameter.Add(pobjCustomer.postalcode);
            //15
            lstEmailparameter.Add(pobjCustomer.mobile);
            //16
            lstEmailparameter.Add(pobjCustomer.email);
            //17
            lstEmailparameter.Add(pobjBookingResponse.BookingResponse.TransactionRefCode);
            //18
            lstEmailparameter.Add(pstrFailureType);

            return lstEmailparameter;
        }


        //Added For InfiPay
        public HotelBookingResponse BookForHotel(HotelBookingRequest pobjBookingRequest, Hotel pobjHotel, HotelSearchRequest pobjHotelSearchRequest, MemberDetails pobjMemberDetails, Customer pobjCustomer, string pstrReferenceId)
        {
            bool IsBookingConfirm = true;
            string strFailureType = "";

            HotelBookingResponse lobjBookingResponse = new HotelBookingResponse();
            try
            {
                IBECTHotelClient lobjIBECTClient = new IBECTHotelClient();

                try
                {
                    lobjBookingResponse = lobjIBECTClient.GetHotelBookingResponse(pobjBookingRequest, pobjHotel, pobjHotelSearchRequest, pobjCustomer, pstrReferenceId);
                }
                catch (Exception ex)
                {
                    LoggingAdapter.WriteLog("Hotel GetHotelBookingResponse in BookingFacade Failure" + ex.Message + ex.StackTrace);
                }

                if (lobjBookingResponse != null && lobjBookingResponse.BookingResponse.bookingid != null && lobjBookingResponse.BookingResponse.bookingid != string.Empty)
                {
                    LoggingAdapter.WriteLog("Booking called Booking Id  : " + lobjBookingResponse.BookingResponse.bookingid, "HotelBookingLogCategory");
                    IsBookingConfirm = lobjBookingResponse.BookingResponse.confirmationnumber != "" && lobjBookingResponse.BookingResponse.confirmationnumber != null && lobjBookingResponse.BookingResponse.bookingid != "" && lobjBookingResponse.BookingResponse.bookingid != null;
                }
                else
                    IsBookingConfirm = false;

                if (IsBookingConfirm)
                {
                    try
                    {
                        //communication Engine call for Email send
                        List<string> lstEmailparameter = new List<string>();
                        lobjBookingResponse.BookingPaymentDetails = pobjBookingRequest.BookingPaymentDetails;
                        lstEmailparameter = GenerateHotelEmailParameters(lobjBookingResponse, pobjHotel, pobjHotelSearchRequest, pobjCustomer, pobjBookingRequest.BookingPaymentDetails.BookingPaymentBreakageList);
                        SendEmail(lstEmailparameter, pobjMemberDetails, "HotelBooked");

                    }
                    catch (Exception ex)
                    {

                        strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                    }
                    try
                    {
                        //Communication Engine Call for Sms Sending
                        List<string> lstSMSparameter = new List<string>();
                        lstSMSparameter = GenerateHotelSMSParameters(lobjBookingResponse);
                        SendSms(lstSMSparameter, pobjMemberDetails, "HotelBooked");

                    }
                    catch (Exception ex)
                    {

                        strFailureType += string.Format("{0}/", FailureType.SMS);
                    }

                    if (strFailureType != "" && strFailureType != string.Empty)
                    {
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);

                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                    }
                }
                else
                {
                    try
                    {

                        strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);

                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                    }
                    catch (Exception ex)
                    {
                        strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);

                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {

                    strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                }
                catch (Exception exp)
                {
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateHotelBookingFailedEmailParameters(lobjBookingResponse, pobjHotel, pobjMemberDetails, pobjHotelSearchRequest, pobjCustomer, strFailureType);
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "HotelBookingFailed");
                }
            }


            return lobjBookingResponse;
        }

        #endregion

        #region SendEmail

        public void SendEmail(List<string> pstrEmailparameter, MemberDetails pobjMemberDetails, string pstrTemplateCode)
        {
            EmailDetails lobjEmailDetail = new EmailDetails();
            List<string> lstAttachment = new List<string>();
            CEHelper lobjcehelper = new CEHelper();
            lobjEmailDetail.TemplateCode = pstrTemplateCode;
            lobjEmailDetail.ListParameter = pstrEmailparameter;
            lobjEmailDetail.ProgramId = Convert.ToString(pobjMemberDetails.ProgramId);
            lobjEmailDetail.MemberId = Convert.ToString(pobjMemberDetails.NationalId);
            lobjEmailDetail.AttachmentList = lstAttachment;
            lobjEmailDetail.To = pobjMemberDetails.Email;
            lobjcehelper.InsertEmailDetails(lobjEmailDetail);
        }

        private void SendFailureEmail(List<string> pstrEmailparameter, MemberDetails pobjMemberDetails, string pstrTemplateCode)
        {
            EmailDetails lobjEmailDetail = new EmailDetails();
            List<string> lstAttachment = new List<string>();
            CEHelper lobjcehelper = new CEHelper();
            lobjEmailDetail.TemplateCode = pstrTemplateCode;
            lobjEmailDetail.ListParameter = pstrEmailparameter;
            lobjEmailDetail.ProgramId = Convert.ToString(pobjMemberDetails.ProgramId);
            lobjEmailDetail.MemberId = Convert.ToString(pobjMemberDetails.NationalId);
            lobjEmailDetail.AttachmentList = lstAttachment;
            lobjEmailDetail.To = String.Empty;
            lobjcehelper.InsertEmailDetails(lobjEmailDetail);
        }



        #endregion

        #region SendSMS

        private void SendSms(List<string> pstrSmsparameter, MemberDetails pobjMemberDetails, string pstrTemplateCode)
        {


            CEHelper lobjcehelper = new CEHelper();
            SmsDetails lobjSmsDetail = new SmsDetails();
            lobjSmsDetail.TemplateCode = pstrTemplateCode;
            lobjSmsDetail.ListParameter = pstrSmsparameter;
            lobjSmsDetail.ReceiverMobile = Convert.ToString(pobjMemberDetails.MobileNumber);
            lobjSmsDetail.ProgramId = Convert.ToString(pobjMemberDetails.ProgramId);
            lobjSmsDetail.MemberId = Convert.ToString(pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
            lobjcehelper.InsertSmsDetails(lobjSmsDetail);

        }

        #endregion

        private void SendEmailOnFailure(BookingResponse pobjBookingResponse, MemberDetails pobjMemberDetails)
        {
            EmailDetails lobjEmailDetail = new EmailDetails();
            List<string> lstEmailparameter = new List<string>();
            List<string> lstAttachment = new List<string>();
            CEHelper lobjcehelper = new CEHelper();
            //0
            lstEmailparameter.Add((pobjMemberDetails.NationalId));
            //1
            lstEmailparameter.Add(pobjMemberDetails.LastName);
            //2
            lstEmailparameter.Add(Convert.ToString(pobjBookingResponse.PNRDetails.ItineraryDetails.FareDetails.TotalPoints));
            //3
            lstEmailparameter.Add(pobjMemberDetails.MobileNumber);
            //4

            lstEmailparameter.Add(pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[0].DepartureAirField.City);
            //5
            lstEmailparameter.Add(pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments.Count - 1].ArrivalAirField.City);
            //6
            if (pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments.Count > 1)
            {
                lstEmailparameter.Add("( Via " + pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[1].DepartureAirField.City + " )");
            }
            else
            {
                lstEmailparameter.Add(string.Empty);
            }
            //7
            if (pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails.Count > 1)
            {
                lstEmailparameter.Add("Return");
            }
            else
            {
                lstEmailparameter.Add("One Way");
            }
            //8
            lstEmailparameter.Add(pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[0].DisplayDepartureDate);
            //9
            lstEmailparameter.Add(pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[0].DisplayDepartureTime);
            //10
            lstEmailparameter.Add(pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments.Count - 1].DisplayArrivalDate);
            //11
            lstEmailparameter.Add(pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments.Count - 1].DisplayArrivalTime);

            if (pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails.Count > 1)
            {
                //12
                lstEmailparameter.Add(pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments[0].DisplayDepartureDate);
                //13
                lstEmailparameter.Add(pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[0].DisplayDepartureTime);
                //14
                lstEmailparameter.Add(pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments.Count - 1].DisplayArrivalDate);
                //15
                lstEmailparameter.Add(pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[pobjBookingResponse.PNRDetails.ItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments.Count - 1].DisplayArrivalTime);
            }
            else
            {
                //12
                lstEmailparameter.Add("");
                //13
                lstEmailparameter.Add("");
                //14
                lstEmailparameter.Add("");
                //15
                lstEmailparameter.Add("");
            }
            string strPaxDetails = "";
            //16
            for (int i = 0; i < pobjBookingResponse.PNRDetails.ItineraryDetails.TravelerInfo.Count; i++)
            {
                strPaxDetails += "Pax No:" + (i + 1) + " ";
                strPaxDetails += (pobjBookingResponse.PNRDetails.ItineraryDetails.TravelerInfo[i].FirstName + " " + pobjBookingResponse.PNRDetails.ItineraryDetails.TravelerInfo[i].LastName) + "<br/>";
                strPaxDetails += (pobjBookingResponse.PNRDetails.ItineraryDetails.TravelerInfo[i].PassportNumber) + "<br/>";
            }
            lstEmailparameter.Add(strPaxDetails);
            //17
            lstEmailparameter.Add(pobjBookingResponse.PNRDetails.BookingReference);
            lobjEmailDetail.ListParameter = lstEmailparameter;
            lobjEmailDetail.ProgramId = Convert.ToString(pobjMemberDetails.ProgramId);
            lobjEmailDetail.MemberId = Convert.ToString(pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
            lobjEmailDetail.TemplateCode = "FlightBookingFailed";
            lobjEmailDetail.AttachmentList = lstAttachment;
            lobjEmailDetail.To = string.Empty;
            lobjcehelper.InsertEmailDetails(lobjEmailDetail);
        }

        public string GetAppSettingValue(string key)
        {

            string arr = System.Configuration.ConfigurationSettings.AppSettings[key].ToString();
            LoggingAdapter.WriteLog("GetAppSettingValue BookingDetails key " + key + " Value " + arr, "Communication Tracing");
            return arr;


        }
        public string BlockPoints(float pfltTotalDefaultFare, float pfltTotalBaseFare, int pintTotalPoints, string pstrRedemptionType, int pintLoyaltyTxnType, MemberDetails pobjMemberDetails, string pstrCurrency)
        {
            string strBlockMilesResponse = string.Empty;
            PGHelper lobjPGHelper = new PGHelper();
            strBlockMilesResponse = lobjPGHelper.BlockPoints(pfltTotalDefaultFare, pfltTotalBaseFare, pintTotalPoints, string.Empty, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, pstrRedemptionType, Convert.ToInt32(RelationType.LBMS), pintLoyaltyTxnType, pstrCurrency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
            return strBlockMilesResponse;
        }

        public bool RedeemPoints(string pstrAdditionalInfo, int pintLoyaltyTxnType, MemberDetails pobjMemberDetails, string pstrBlockMilesResponse)
        {
            bool lboolRedeemResponse = false;
            PGHelper lobjPGHelper = new PGHelper();
            lboolRedeemResponse = lobjPGHelper.RedeemPoint(pstrBlockMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pstrAdditionalInfo, pintLoyaltyTxnType, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
            return lboolRedeemResponse;
        }

        public bool RollBackPoints(string pstrAdditionalInfo, MemberDetails pobjMemberDetails, string pstrBlockMilesResponse)
        {
            bool lboolRollBackResponse = false;
            PGHelper lobjPGHelper = new PGHelper();
            lboolRollBackResponse = lobjPGHelper.RollBackTransaction(pstrBlockMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pstrAdditionalInfo, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
            return lboolRollBackResponse;
        }
        #region Car
        //public CarMakeBookingResponse BookCar(CarMakeBookingRequest pobjCarMakeBookingRequest, string pstrReceipt, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, MemberDetails pobjMemberDetails)
        public CarMakeBookingResponse BookCar(CarMakeBookingRequest pobjCarMakeBookingRequest, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {

            bool lboolRollBackResponse = false;
            bool IsBookingConfirm = true;
            string strRedeemMilesResponse = string.Empty;
            string strRollBackMilesResponse = string.Empty;
            string strFailureType = "";

            CarMakeBookingResponse lobjBookingResponse = new CarMakeBookingResponse();
            PGHelper lobjPGHelper = new PGHelper();
            try
            {

                bool IsRedeem = true;
                List<BookingPaymentBreakage> lobjListOfBookingPaymentBreakage = new List<BookingPaymentBreakage>();

                for (int i = 0; i < pobjListOfRedemptionDetails.Count; i++)
                {
                    strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption((float)(pobjListOfRedemptionDetails[i].Amount), (float)(pobjListOfRedemptionDetails[i].Amount), pobjListOfRedemptionDetails[i].Points, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, pobjMatch.Route[0].PickUp[0].locName + "-" + pobjMatch.Route[0].DropOff[0].locName, Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.Car), pobjListOfRedemptionDetails[i].Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    pobjListOfRedemptionDetails[i].TransactionReference = strRedeemMilesResponse;
                    //pobjListOfRedemptionDetails[i].TransactionReference = pobjCarMakeBookingRequest.BookingPaymentDetails.PointsTxnRefererence;
                    IsRedeem = IsRedeem && (pobjListOfRedemptionDetails[i].TransactionReference != "" && pobjListOfRedemptionDetails[i].TransactionReference != string.Empty);

                    BookingPaymentBreakage lobjBookingPaymentBreakage = new BookingPaymentBreakage();
                    lobjBookingPaymentBreakage.Currency = pobjListOfRedemptionDetails[i].DisplayCurrency;
                    lobjBookingPaymentBreakage.Amount = pobjListOfRedemptionDetails[i].Points;
                    lobjBookingPaymentBreakage.TxnReference = strRedeemMilesResponse;
                    //lobjBookingPaymentBreakage.TxnReference = pobjCarMakeBookingRequest.BookingPaymentDetails.PointsTxnRefererence;
                    lobjListOfBookingPaymentBreakage.Add(lobjBookingPaymentBreakage);
                }

                BookingPaymentDetails lobjBookingPaymentDetails = new BookingPaymentDetails();
                lobjBookingPaymentDetails.BookingPaymentBreakageList = lobjListOfBookingPaymentBreakage;
                lobjBookingPaymentDetails.MemberId = pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjBookingPaymentDetails.PaymentType = PaymentType.Points;
                lobjBookingPaymentDetails.ServiceType = ServiceType.CAR;
                lobjBookingPaymentDetails.Points = pobjListOfRedemptionDetails[0].Points;
                lobjBookingPaymentDetails.PointsTxnRefererence = pobjListOfRedemptionDetails[0].TransactionReference;

                if (IsRedeem)
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.Success;
                }
                else
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.PaymentDone;
                }


                pobjCarMakeBookingRequest.BookingPaymentDetails = lobjBookingPaymentDetails;

                IBERCCarClient lobjIBERCCarClient = new IBERCCarClient();

                if (IsRedeem)
                {
                    try
                    {
                        LoggingAdapter.WriteLog("Car RedeemMiles Success ", "CarBookingLogCategory");

                        List<PaymentInfo> lobjPaymentInfoList = new List<PaymentInfo>();
                        PaymentInfo lobjPaymentInfo = new PaymentInfo();
                        lobjPaymentInfo.depositPayment = "False";
                        lobjPaymentInfoList.Add(lobjPaymentInfo);
                        pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PaymentInfo = lobjPaymentInfoList.ToArray();

                        CreditCard lobjCreditCard = new CreditCard();
                        List<ExpirationDate> lobjListOfExpirationDate = new List<ExpirationDate>();
                        ExpirationDate lobjExpirationDate = new ExpirationDate();
                        lobjListOfExpirationDate.Add(lobjExpirationDate);
                        lobjCreditCard.ExpirationDate = lobjListOfExpirationDate.ToArray();
                        pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PaymentInfo[0].CreditCard = lobjCreditCard;

                        lobjBookingResponse = lobjIBERCCarClient.CarMakeBookingResponse(pobjCarMakeBookingRequest, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);

                        if (lobjBookingResponse != null && lobjBookingResponse.MakeBookingRS.Booking.id != null && lobjBookingResponse.MakeBookingRS.Booking.id != string.Empty)
                        {
                            LoggingAdapter.WriteLog("Booking called Booking Id  : " + lobjBookingResponse.MakeBookingRS.Booking.id, "CarBookingLogCategory");
                            IsBookingConfirm = lobjBookingResponse != null && lobjBookingResponse.MakeBookingRS != null && lobjBookingResponse.MakeBookingRS.Booking != null && !string.IsNullOrEmpty(lobjBookingResponse.MakeBookingRS.Booking.id);
                        }
                        else
                            IsBookingConfirm = false;

                        if (IsBookingConfirm)
                        {
                            try
                            {
                                //communication Engine call for Email send
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateCarEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjCarMakeBookingRequest.BookingPaymentDetails.BookingPaymentBreakageList);
                                CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBooked");
                                LoggingAdapter.WriteLog("Mail sent. : " + strRedeemMilesResponse, "CarBookingLogCategory");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine exception in booking: " + strRedeemMilesResponse, "CarBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                            }
                            try
                            {
                                //Communication Engine Call for Sms Sending
                                List<string> lstSMSparameter = new List<string>();
                                lstSMSparameter = GenerateCarSMSParameters(lobjBookingResponse);
                                SendSms(lstSMSparameter, pobjMemberDetails, "CarBooked");
                                LoggingAdapter.WriteLog("SMS sent. : " + strRedeemMilesResponse, "CarBookingLogCategory");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine exception in booking: " + strRedeemMilesResponse, "CarBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.SMS);
                            }

                            if (strFailureType != "" && strFailureType != string.Empty)
                            {
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                                CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                            }
                        }
                        else
                        {
                            try
                            {
                                for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                                {
                                    strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                    if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                        lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Location[0].locName + "-" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff[0].Location[0].locName, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                                }

                                LoggingAdapter.WriteLog("RollBackMiles Success : " + lboolRollBackResponse, "CarBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                                CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("RollBackMiles Exception : " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "CarBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                                CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                            {
                                strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                    lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Location[0].locName + "-" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff[0].Location[0].locName, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                            }

                            LoggingAdapter.WriteLog("RollBackMiles Success On Exception : " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "CarBookingLogCategory");
                            strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                            CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                        }
                        catch (Exception exp)
                        {
                            LoggingAdapter.WriteLog("RollBackMiles Exception : " + lboolRollBackResponse + " \n Exception \n" + exp.StackTrace, "CarBookingLogCategory");
                            strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                            CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                    {
                        strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                        if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                            lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Location[0].locName + "-" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff[0].Location[0].locName, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    }
                    strFailureType += string.Format("{0}/", FailureType.REDEEMMILES);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                    CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(ex.StackTrace);
            }

            return lobjBookingResponse;
        }

        public CarMakeBookingResponse BookCarByInfiPay(CarMakeBookingRequest pobjCarMakeBookingRequest, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {

            bool lboolRollBackResponse = false;
            bool IsBookingConfirm = true;
            string strRedeemMilesResponse = string.Empty;
            string strRollBackMilesResponse = string.Empty;
            string strFailureType = "";

            CarMakeBookingResponse lobjBookingResponse = new CarMakeBookingResponse();
            PGHelper lobjPGHelper = new PGHelper();
            try
            {

                bool IsRedeem = true;
                List<BookingPaymentBreakage> lobjListOfBookingPaymentBreakage = new List<BookingPaymentBreakage>();

                for (int i = 0; i < pobjListOfRedemptionDetails.Count; i++)
                {
                    //strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption((float)(pobjListOfRedemptionDetails[i].Amount), (float)(pobjListOfRedemptionDetails[i].Amount), pobjListOfRedemptionDetails[i].Points, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.Login)).WebPassword, pobjMatch.Route[0].PickUp[0].locName + "-" + pobjMatch.Route[0].DropOff[0].locName, Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.Car), pobjListOfRedemptionDetails[i].Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    //pobjListOfRedemptionDetails[i].TransactionReference = strRedeemMilesResponse;
                    pobjListOfRedemptionDetails[i].TransactionReference = pobjCarMakeBookingRequest.BookingPaymentDetails.PointsTxnRefererence;
                    IsRedeem = IsRedeem && (pobjListOfRedemptionDetails[i].TransactionReference != "" && pobjListOfRedemptionDetails[i].TransactionReference != string.Empty);

                    BookingPaymentBreakage lobjBookingPaymentBreakage = new BookingPaymentBreakage();
                    lobjBookingPaymentBreakage.Currency = pobjListOfRedemptionDetails[i].DisplayCurrency;
                    lobjBookingPaymentBreakage.Amount = pobjListOfRedemptionDetails[i].Points;
                    //lobjBookingPaymentBreakage.TxnReference = strRedeemMilesResponse;
                    lobjBookingPaymentBreakage.TxnReference = pobjCarMakeBookingRequest.BookingPaymentDetails.PointsTxnRefererence;
                    lobjListOfBookingPaymentBreakage.Add(lobjBookingPaymentBreakage);
                }

                BookingPaymentDetails lobjBookingPaymentDetails = new BookingPaymentDetails();
                lobjBookingPaymentDetails.BookingPaymentBreakageList = lobjListOfBookingPaymentBreakage;
                lobjBookingPaymentDetails.MemberId = pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjBookingPaymentDetails.PaymentType = PaymentType.Points;
                lobjBookingPaymentDetails.ServiceType = ServiceType.CAR;
                lobjBookingPaymentDetails.Points = pobjListOfRedemptionDetails[0].Points;
                lobjBookingPaymentDetails.PointsTxnRefererence = pobjListOfRedemptionDetails[0].TransactionReference;

                if (IsRedeem)
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.Success;
                }
                else
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.PaymentDone;
                }


                pobjCarMakeBookingRequest.BookingPaymentDetails = lobjBookingPaymentDetails;

                IBERCCarClient lobjIBERCCarClient = new IBERCCarClient();

                if (IsRedeem)
                {
                    try
                    {
                        LoggingAdapter.WriteLog("Car RedeemMiles Success ", "CarBookingLogCategory");

                        List<PaymentInfo> lobjPaymentInfoList = new List<PaymentInfo>();
                        PaymentInfo lobjPaymentInfo = new PaymentInfo();
                        lobjPaymentInfo.depositPayment = "False";
                        lobjPaymentInfoList.Add(lobjPaymentInfo);
                        pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PaymentInfo = lobjPaymentInfoList.ToArray();

                        CreditCard lobjCreditCard = new CreditCard();
                        List<ExpirationDate> lobjListOfExpirationDate = new List<ExpirationDate>();
                        ExpirationDate lobjExpirationDate = new ExpirationDate();
                        lobjListOfExpirationDate.Add(lobjExpirationDate);
                        lobjCreditCard.ExpirationDate = lobjListOfExpirationDate.ToArray();
                        pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PaymentInfo[0].CreditCard = lobjCreditCard;

                        lobjBookingResponse = lobjIBERCCarClient.CarMakeBookingResponse(pobjCarMakeBookingRequest, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);

                        if (lobjBookingResponse != null && lobjBookingResponse.MakeBookingRS.Booking.id != null && lobjBookingResponse.MakeBookingRS.Booking.id != string.Empty)
                        {
                            LoggingAdapter.WriteLog("Booking called Booking Id  : " + lobjBookingResponse.MakeBookingRS.Booking.id, "CarBookingLogCategory");
                            IsBookingConfirm = lobjBookingResponse != null && lobjBookingResponse.MakeBookingRS != null && lobjBookingResponse.MakeBookingRS.Booking != null && !string.IsNullOrEmpty(lobjBookingResponse.MakeBookingRS.Booking.id);
                        }
                        else
                            IsBookingConfirm = false;

                        if (IsBookingConfirm)
                        {
                            try
                            {
                                //communication Engine call for Email send
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateCarEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjCarMakeBookingRequest.BookingPaymentDetails.BookingPaymentBreakageList);
                                CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBooked");
                                LoggingAdapter.WriteLog("Mail sent. : " + strRedeemMilesResponse, "CarBookingLogCategory");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine exception in booking: " + strRedeemMilesResponse, "CarBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                            }
                            try
                            {
                                //Communication Engine Call for Sms Sending
                                List<string> lstSMSparameter = new List<string>();
                                lstSMSparameter = GenerateCarSMSParameters(lobjBookingResponse);
                                SendSms(lstSMSparameter, pobjMemberDetails, "CarBooked");
                                LoggingAdapter.WriteLog("SMS sent. : " + strRedeemMilesResponse, "CarBookingLogCategory");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("Communication engine exception in booking: " + strRedeemMilesResponse, "CarBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.SMS);
                            }

                            if (strFailureType != "" && strFailureType != string.Empty)
                            {
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                                CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                            }
                        }
                        else
                        {
                            try
                            {
                                for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                                {
                                    strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                    if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                        lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Location[0].locName + "-" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff[0].Location[0].locName, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                                }

                                LoggingAdapter.WriteLog("RollBackMiles Success : " + lboolRollBackResponse, "CarBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                                CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("RollBackMiles Exception : " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "CarBookingLogCategory");
                                strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                                List<string> lstEmailparameter = new List<string>();
                                lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                                CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                            {
                                strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                    lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Location[0].locName + "-" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff[0].Location[0].locName, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                            }

                            LoggingAdapter.WriteLog("RollBackMiles Success On Exception : " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "CarBookingLogCategory");
                            strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                            CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                        }
                        catch (Exception exp)
                        {
                            LoggingAdapter.WriteLog("RollBackMiles Exception : " + lboolRollBackResponse + " \n Exception \n" + exp.StackTrace, "CarBookingLogCategory");
                            strFailureType += string.Format("{0}/", FailureType.ROLLBACKMILES);
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                            CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                    {
                        strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                        if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                            lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PickUp[0].Location[0].locName + "-" + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DropOff[0].Location[0].locName, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    }
                    strFailureType += string.Format("{0}/", FailureType.REDEEMMILES);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                    CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(ex.StackTrace);
            }

            return lobjBookingResponse;
        }

        public CarMakeBookingResponse BookCarByInfiVoucher(CarMakeBookingRequest pobjCarMakeBookingRequest, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, MemberDetails pobjMemberDetails)
        {
            bool IsBookingConfirm = true;
            string strFailureType = "";

            CarMakeBookingResponse lobjBookingResponse = new CarMakeBookingResponse();

            try
            {
                IBERCCarClient lobjIBERCCarClient = new IBERCCarClient();

                try
                {
                    List<PaymentInfo> lobjPaymentInfoList = new List<PaymentInfo>();
                    PaymentInfo lobjPaymentInfo = new PaymentInfo();
                    lobjPaymentInfo.depositPayment = "False";
                    lobjPaymentInfoList.Add(lobjPaymentInfo);
                    pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PaymentInfo = lobjPaymentInfoList.ToArray();

                    CreditCard lobjCreditCard = new CreditCard();
                    List<ExpirationDate> lobjListOfExpirationDate = new List<ExpirationDate>();
                    ExpirationDate lobjExpirationDate = new ExpirationDate();
                    lobjListOfExpirationDate.Add(lobjExpirationDate);
                    lobjCreditCard.ExpirationDate = lobjListOfExpirationDate.ToArray();
                    pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PaymentInfo[0].CreditCard = lobjCreditCard;


                    lobjBookingResponse = lobjIBERCCarClient.CarMakeBookingResponse(pobjCarMakeBookingRequest, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);

                }
                catch (Exception ex)
                {
                    LoggingAdapter.WriteLog("CreateBooking Failure BookingFacade:" + ex.Message + ex.StackTrace);
                }

                if (lobjBookingResponse != null && lobjBookingResponse.MakeBookingRS.Booking.id != null && lobjBookingResponse.MakeBookingRS.Booking.id != string.Empty)
                {
                    LoggingAdapter.WriteLog("Booking called Booking Id  : " + lobjBookingResponse.MakeBookingRS.Booking.id, "CarBookingLogCategory");
                    IsBookingConfirm = lobjBookingResponse != null && lobjBookingResponse.MakeBookingRS != null && lobjBookingResponse.MakeBookingRS.Booking != null && !string.IsNullOrEmpty(lobjBookingResponse.MakeBookingRS.Booking.id);
                }
                else
                {
                    IsBookingConfirm = false;
                }

                if (IsBookingConfirm)
                {
                    try
                    {
                        //communication Engine call for Email send
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateCarEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjCarMakeBookingRequest.BookingPaymentDetails.BookingPaymentBreakageList);
                        LoggingAdapter.WriteLog("Mail sent. : " + lstEmailparameter, "CarBookingLogCategory");
                        CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBooked");
                    }
                    catch (Exception ex)
                    {
                        strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                    }
                    try
                    {
                        //Communication Engine Call for Sms Sending
                        List<string> lstSMSparameter = new List<string>();
                        lstSMSparameter = GenerateCarSMSParameters(lobjBookingResponse);
                        LoggingAdapter.WriteLog("SMS sent. : " + lstSMSparameter, "CarBookingLogCategory");
                        SendSms(lstSMSparameter, pobjMemberDetails, "CarBookedInfiVoucher");

                    }
                    catch (Exception ex)
                    {
                        strFailureType += string.Format("{0}/", FailureType.SMS);
                    }
                }
                else
                {
                    try
                    {
                        strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                        LoggingAdapter.WriteLog("Failed Email parameter: " + lstEmailparameter, "CarBookingLogCategory");
                        CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                    }
                    catch (Exception ex)
                    {
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                        LoggingAdapter.WriteLog("Failed Email parameter: " + lstEmailparameter, "CarBookingLogCategory");
                        CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                    }
                    //throw new ApplicationException(lstrBookingFailureMsg + " Your Transaction Reference Code: " + lobjBookingResponse.BookingReferenceId);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(ex.StackTrace);
                //throw new ApplicationException(lstrBookingFailureMsg);
            }


            return lobjBookingResponse;

        }

        public List<string> GenerateCarEmailParameters(CarMakeBookingResponse pobjCarBookingResponse, CarMakeBookingRequest pobjCarMakeBookingRequest, MemberDetails pobjMemberDetails, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, List<BookingPaymentBreakage> pobjListOfBookingPaymentBreakage)
        {
            List<string> lstEmailparameter = new List<string>();
            //[0]
            lstEmailparameter.Add(pobjMemberDetails.LastName);
            //[1]
            lstEmailparameter.Add(pobjMemberDetails.NationalId);
            //[2]
            lstEmailparameter.Add(pobjCarBookingResponse.MakeBookingRS.Booking.id);
            //lstEmailparameter.Add("BookId");
            //[3]
            lstEmailparameter.Add(pobjCarBookingResponse.BookingReferenceId);
            //lstEmailparameter.Add("BookRedId");
            //[4]
            lstEmailparameter.Add(pobjCarBookingResponse.MakeBookingRS.Booking.status);
            //lstEmailparameter.Add("BStatus");
            //[5]
            lstEmailparameter.Add(pobjMatch.Route[0].PickUp[0].locName);
            //lstEmailparameter.Add("PLocation");
            //[6]
            lstEmailparameter.Add(pobjMatch.Route[0].DropOff[0].locName);
            // lstEmailparameter.Add("D Location");

            //[7]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].day + "/" + pobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].month + "/" + pobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].year);
            // lstEmailparameter.Add("02/02/2010");
            //[8]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].hour + ":" + pobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].minute);
            //lstEmailparameter.Add("10:10");
            //[9]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].day + "/" + pobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].month + "/" + pobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].year);
            //lstEmailparameter.Add("03/03/2010");
            //[10]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].hour + ":" + pobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].minute);
            //lstEmailparameter.Add("20:20");
            //[11]
            lstEmailparameter.Add(pobjMatch.Vehicle[0].Name);
            //lstEmailparameter.Add("vehicle name");
            //[12]
            lstEmailparameter.Add(pobjMatch.Vehicle[0].automatic);
            //lstEmailparameter.Add("Automatic");
            //[13]
            lstEmailparameter.Add(pobjMatch.Vehicle[0].aircon);
            // lstEmailparameter.Add("aircon");
            //[14]
            lstEmailparameter.Add(pobjMatch.Route[0].PickUp[0].locName);
            //lstEmailparameter.Add("PickupLocation");
            //[15]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].city);
            // lstEmailparameter.Add("City");
            //[16]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].country);
            // lstEmailparameter.Add("Country");
            //[17]
            lstEmailparameter.Add(pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].title + ". " + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].firstname + " " + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].lastname);
            //lstEmailparameter.Add("Driver name");
            //[18]
            lstEmailparameter.Add(pobjMemberDetails.Address1);
            //lstEmailparameter.Add("Address ");
            //[19]
            lstEmailparameter.Add(pobjMemberDetails.MobileNumber);
            //lstEmailparameter.Add("123456780");
            //[20]
            string strPaymentDetails = string.Empty;

            string strPaymentDetailsHTML = "<td style='font-size: 14px;' width='123px' valign='top'>{0}</td><td width='5px'valign='top' style='font-size: 14px;'>:</td><td " +
                    "style='font-size: 14px;' valign='middle'>{1}</td>";

            for (int i = 0; i < pobjListOfBookingPaymentBreakage.Count; i++)
            {
                //if (!pobjListOfBookingPaymentBreakage[i].Currency.Equals("QAR"))
                //{
                //    strPaymentDetails += string.Format(strPaymentDetailsHTML, pobjListOfBookingPaymentBreakage[i].Currency + " Points", Convert.ToInt32(pobjListOfBookingPaymentBreakage[i].Amount));
                //}
                //else
                //{
                strPaymentDetails += string.Format(strPaymentDetailsHTML, pobjListOfBookingPaymentBreakage[i].Currency, pobjListOfBookingPaymentBreakage[i].Amount);
                //}
            }

            strPaymentDetails = "<table width='100%' border='0' cellspacing='0' cellpadding='0' align='left'><tr>" + strPaymentDetails + "</tr></table>";

            lstEmailparameter.Add(strPaymentDetails);
            //lstEmailparameter.Add(Convert.ToString(pobjMatch.Price[0].TotalPoints + pobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[0].Price[0].ExtraTotalPoints));
            //lstEmailparameter.Add("123point");
            return lstEmailparameter;

        }

        private List<string> GenerateCarSMSParameters(CarMakeBookingResponse plobBookingResponse)
        {
            List<string> lstSMSparameter = new List<string>();

            // lstSMSparameter.Add("Your Car has booked");
            lstSMSparameter.Add(Convert.ToString(plobBookingResponse.BookingReferenceId));
            return lstSMSparameter;
        }

        private List<string> GenerateCarBookingFailedEmailParameters(CarMakeBookingResponse pobjCarMakeBookingResponse, CarMakeBookingRequest pobjCarMakeBookingRequest, MemberDetails pobjMemberDetails, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse)
        {
            List<string> lstEmailparameter = new List<string>();

            //[0]
            lstEmailparameter.Add(pobjMemberDetails.NationalId);
            //[1]
            lstEmailparameter.Add(pobjMemberDetails.LastName);
            //[2]
            lstEmailparameter.Add(pobjMemberDetails.MobileNumber);

            //[3]
            lstEmailparameter.Add(pobjCarMakeBookingResponse.BookingReferenceId);

            //[4]
            lstEmailparameter.Add(Convert.ToString(pobjMatch.Price[0].TotalPoints + pobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[0].Price[0].ExtraTotalPoints));

            //[5]
            lstEmailparameter.Add(pobjMatch.Vehicle[0].Name);
            //[6]
            lstEmailparameter.Add(pobjMatch.Route[0].PickUp[0].locName);
            //[7]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].city);
            //[8]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.PickUp[0].Location[0].country);
            //[9]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].day + "/" + pobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].month + "/" + pobjCarSearchRequest.SearchRequest.PickUp[0].Date[0].year);
            //[10]
            lstEmailparameter.Add(pobjMatch.Route[0].DropOff[0].locName);

            //[11]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.DropOff[0].Location[0].city);
            //[12]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.DropOff[0].Location[0].country);


            //[13]
            lstEmailparameter.Add(pobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].day + "/" + pobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].month + "/" + pobjCarSearchRequest.SearchRequest.DropOff[0].Date[0].year);



            //[14]
            lstEmailparameter.Add(pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].title + ". " + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].firstname + " " + pobjCarMakeBookingRequest.MakeBookingRQ.Booking.DriverInfo[0].DriverName[0].lastname);



            string strPaymentDetails = string.Empty;
            if (pobjCarMakeBookingRequest.BookingPaymentDetails.PaymentType.Equals(PaymentType.CashPoints))
            {
                strPaymentDetails = "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td align='left' valign='top' width='50%'><table align='left'" +
                    " cellpadding='0' cellspacing='0' width='100%'><tr><td align='left' valign='top' width='46%' style='font-family: Arial; font-size: 12px;letter-spacing:" +
                    " normal; line-height: 18px; font-weight: bold; text-transform: capitalize;text-align: left; color: #231f20; padding: 4px 0 3px 0;'>Ajman Rewards Points</td>" +
                    "<td width='5px' >:</td><td align='left' valign='top' style='font-family: Arial; font-size: 12px; letter-spacing: normal;line-height: 18px; font-weight:" +
                    " normal; text-transform: inherit; text-align: left;color: #231f20; padding: 4px 0 3px 0;'>"
                    + Convert.ToString(pobjCarMakeBookingRequest.BookingPaymentDetails.Points) + "</td></tr></table></td><td align='left' valign='top'" +
                    " width='50%'><table align='left' cellpadding='0' cellspacing='0' width='100%'><tr><td align='left' valign='top' width='46%' style='font-family: Arial;" +
                    " font-size: 12px;letter-spacing: normal; line-height: 18px; font-weight: bold; text-transform: capitalize;text-align: left; color: #231f20;" +
                    " padding: 4px 0 3px 0;'>Cash Payment</td><td width='5px'>:</td><td align='left' valign='top' style='font-family: Arial; font-size: 12px; letter-spacing:" +
                    " normal;line-height: 18px; font-weight: normal; text-transform: inherit; text-align: left;color: #231f20; padding: 4px 0 3px 0;'>"
                    + Convert.ToString(pobjCarMakeBookingRequest.BookingPaymentDetails.CashAmount) + "</td></tr></table></td></tr></table>";
            }
            else if (pobjCarMakeBookingRequest.BookingPaymentDetails.PaymentType.Equals(PaymentType.Points))
            {


                strPaymentDetails = "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td align='left' valign='top' width='50%'><table align='left'" +
                    " cellpadding='0' cellspacing='0' width='100%'><tr><td align='left' valign='top' width='46%' style='font-family: Arial; font-size: 12px;letter-spacing:" +
                    " normal; line-height: 18px; font-weight: bold; text-transform: capitalize;text-align: left; color: #231f20; padding: 4px 0 3px 0;'>Ajman Rewards Points" +
                    "</td><td width='5px'>:</td><td align='left' valign='top' style='font-family: Arial; font-size: 12px; letter-spacing: normal;line-height: 18px;" +
                    " font-weight: normal; text-transform: inherit; text-align: left;color: #231f20; padding: 4px 0 3px 0;'>"
                    + Convert.ToString(pobjMatch.Price[0].TotalPoints + pobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[0].Price[0].ExtraTotalPoints) +
                    "</td></tr></table></td></tr></table>";


            }
            else if (pobjCarMakeBookingRequest.BookingPaymentDetails.PaymentType.Equals(PaymentType.Cash))
            {

                strPaymentDetails = "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr> <td align='left' valign='top' width='50%'><table align='left' cellpadding='0' cellspacing='0' width='100%'><tr><td align='left'" +
                    " valign='top' width='46%' style='font-family: Arial; font-size: 12px;letter-spacing: normal; line-height: 18px; font-weight: bold; text-transform:" +
                    " capitalize;text-align: left; color: #231f20; padding: 4px 0 3px 0;'>Cash Payment</td><td width='5px'>:</td><td align='left' valign='top'" +
                    " style='font-family: Arial; font-size: 12px; letter-spacing: normal;line-height: 18px; font-weight: normal; text-transform: inherit; text-align:" +
                    " left;color: #231f20; padding: 4px 0 3px 0;'>"
                    + Convert.ToString(pobjMatch.Price[0].TotalBaseFare + pobjCarExtrasListResponse.ExtrasListRS.Items[0].ExtraInfo[0].Price[0].ExtraTotalBaseFare) +
                    "</td></tr></table></td></tr></table>";
            }
            //[15]
            lstEmailparameter.Add(strPaymentDetails);



            return lstEmailparameter;
        }


        //Added For Infipay
        public CarMakeBookingResponse BookCarForCashPlusPoints(CarMakeBookingRequest pobjCarMakeBookingRequest, CarSearchRequest pobjCarSearchRequest, Match pobjMatch, CarExtrasListResponse pobjCarExtrasListResponse, MemberDetails pobjMemberDetails)
        {
            bool IsBookingConfirm = true;
            string strFailureType = "";

            CarMakeBookingResponse lobjBookingResponse = new CarMakeBookingResponse();

            try
            {
                IBERCCarClient lobjIBERCCarClient = new IBERCCarClient();

                try
                {
                    List<PaymentInfo> lobjPaymentInfoList = new List<PaymentInfo>();
                    PaymentInfo lobjPaymentInfo = new PaymentInfo();
                    lobjPaymentInfo.depositPayment = "False";
                    lobjPaymentInfoList.Add(lobjPaymentInfo);
                    pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PaymentInfo = lobjPaymentInfoList.ToArray();

                    CreditCard lobjCreditCard = new CreditCard();
                    List<ExpirationDate> lobjListOfExpirationDate = new List<ExpirationDate>();
                    ExpirationDate lobjExpirationDate = new ExpirationDate();
                    lobjListOfExpirationDate.Add(lobjExpirationDate);
                    lobjCreditCard.ExpirationDate = lobjListOfExpirationDate.ToArray();
                    pobjCarMakeBookingRequest.MakeBookingRQ.Booking.PaymentInfo[0].CreditCard = lobjCreditCard;


                    lobjBookingResponse = lobjIBERCCarClient.CarMakeBookingResponse(pobjCarMakeBookingRequest, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);

                }
                catch (Exception ex)
                {
                    LoggingAdapter.WriteLog("CreateBooking Failure BookingFacade:" + ex.Message + ex.StackTrace);
                }

                if (lobjBookingResponse != null && lobjBookingResponse.MakeBookingRS.Booking.id != null && lobjBookingResponse.MakeBookingRS.Booking.id != string.Empty)
                {
                    LoggingAdapter.WriteLog("Booking called Booking Id  : " + lobjBookingResponse.MakeBookingRS.Booking.id, "CarBookingLogCategory");
                    IsBookingConfirm = lobjBookingResponse != null && lobjBookingResponse.MakeBookingRS != null && lobjBookingResponse.MakeBookingRS.Booking != null && !string.IsNullOrEmpty(lobjBookingResponse.MakeBookingRS.Booking.id);
                }
                else
                {
                    IsBookingConfirm = false;
                }

                if (IsBookingConfirm)
                {
                    try
                    {
                        //communication Engine call for Email send
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateCarEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse, pobjCarMakeBookingRequest.BookingPaymentDetails.BookingPaymentBreakageList);
                        LoggingAdapter.WriteLog("Mail sent. : " + lstEmailparameter, "CarBookingLogCategory");
                        CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBooked");
                    }
                    catch (Exception ex)
                    {
                        strFailureType += string.Format("{0}/", FailureType.FAILUREEMAIL);
                    }
                    try
                    {
                        //Communication Engine Call for Sms Sending
                        List<string> lstSMSparameter = new List<string>();
                        lstSMSparameter = GenerateCarSMSParameters(lobjBookingResponse);
                        LoggingAdapter.WriteLog("SMS sent. : " + lstSMSparameter, "CarBookingLogCategory");
                        SendSms(lstSMSparameter, pobjMemberDetails, "CarBooked");

                    }
                    catch (Exception ex)
                    {
                        strFailureType += string.Format("{0}/", FailureType.SMS);
                    }
                }
                else
                {
                    try
                    {
                        strFailureType += string.Format("{0}/", FailureType.CTRESPONSE);
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                        LoggingAdapter.WriteLog("Failed Email parameter: " + lstEmailparameter, "CarBookingLogCategory");
                        CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                    }
                    catch (Exception ex)
                    {
                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter = GenerateCarBookingFailedEmailParameters(lobjBookingResponse, pobjCarMakeBookingRequest, pobjMemberDetails, pobjCarSearchRequest, pobjMatch, pobjCarExtrasListResponse);
                        LoggingAdapter.WriteLog("Failed Email parameter: " + lstEmailparameter, "CarBookingLogCategory");
                        CarSendEmail(lstEmailparameter, pobjMemberDetails, "CarBookingFailed");
                    }
                }
            }

            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(ex.StackTrace);
            }


            return lobjBookingResponse;

        }
        public void CarSendEmail(List<string> pstrEmailparameter, MemberDetails pobjMemberDetails, string pstrTemplateCode)
        {
            EmailDetails lobjEmailDetail = new EmailDetails();
            List<string> lstAttachment = new List<string>();
            CEHelper lobjcehelper = new CEHelper();
            lobjEmailDetail.TemplateCode = pstrTemplateCode;
            lobjEmailDetail.ListParameter = pstrEmailparameter;
            lobjEmailDetail.ProgramId = Convert.ToString(pobjMemberDetails.ProgramId);
            lobjEmailDetail.MemberId = Convert.ToString(pobjMemberDetails.NationalId);
            lobjEmailDetail.AttachmentList = lstAttachment;
            if (pstrTemplateCode == "CarBookingFailed")
            {
                lobjEmailDetail.To = string.Empty;
            }
            else
            {
                lobjEmailDetail.To = pobjMemberDetails.Email;
            }
            lobjcehelper.InsertEmailDetails(lobjEmailDetail);
        }

        #endregion

        public bool CheckWhetherOTPExists(OTPDetails pobjOTPDetails)
        {
            OTPFacade lobjfacade = new OTPFacade();
            return lobjfacade.UpdateOTP(pobjOTPDetails);
        }

        public bool GenerateReviewnConfirmOTP(OTPDetails pobjOTPDetails, MemberDetails pobjMemberDetails)
        {
            OTPFacade lobjOTPFacade = new OTPFacade();
            OTPDetails lobjOTPDetails = lobjOTPFacade.GenerateOTP(pobjOTPDetails);
            string lstrTemplateCode = string.Empty;
            if (lobjOTPDetails != null)
            {
                if (lobjOTPDetails.OtpEnumTypes.Equals(OTPEnumTypes.AIRREVIEWNCONFIRM))
                {
                    lstrTemplateCode = "FlightOTP";
                }
                else if (lobjOTPDetails.OtpEnumTypes.Equals(OTPEnumTypes.HOTELREVIEWNCONFIRM))
                {
                    lstrTemplateCode = "HotelOTP";
                }
                else if (lobjOTPDetails.OtpEnumTypes.Equals(OTPEnumTypes.CARREVIEWNCONFIRM))
                {
                    lstrTemplateCode = "CarOTP";
                }
                //else if (lobjOTPDetails.OtpEnumTypes.Equals(OTPEnumTypes.CASHBACKCONFIRM))
                //{
                //    lstrTemplateCode = "CashBackOTP";
                //}
                // SendOTPEmail(pobjMemberDetails, lobjOTPDetails.OTP.ToString(), lobjOTPDetails.ExpiryDateTime, lstrTemplateCode);
                SendOTPSMS(pobjMemberDetails, lobjOTPDetails.OTP.ToString(), lobjOTPDetails.ExpiryDateTime, lstrTemplateCode);
                return true;
            }
            return false;
        }

        private bool SendOTPEmail(MemberDetails pobjMemberDetails, string pstrOTPstirng, DateTime pdtExpirydatetime, string pstrTemplateCode)
        {
            bool lblnEmailSend = false;
            try
            {
                if (pobjMemberDetails.Email != string.Empty)
                {

                    string lstrexpirydt = pdtExpirydatetime.ToString("dd/MM/yyyy");

                    EmailDetails lobjEmailDetail = new EmailDetails();
                    List<string> lstEmailparameter = new List<string>();
                    List<string> lstAttachment = new List<string>();
                    CEHelper lobjcehelper = new CEHelper();
                    lstEmailparameter.Add(pstrOTPstirng);
                    //lstEmailparameter.Add(lstrexpirydt); //new line of code
                    lobjEmailDetail.TemplateCode = pstrTemplateCode;
                    lobjEmailDetail.ListParameter = lstEmailparameter;
                    lobjEmailDetail.AttachmentList = lstAttachment;
                    lobjEmailDetail.To = pobjMemberDetails.Email;
                    lobjEmailDetail.ProgramId = Convert.ToString(pobjMemberDetails.ProgramId);
                    lobjEmailDetail.MemberId = Convert.ToString(pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
                    lblnEmailSend = lobjcehelper.InsertEmailDetails(lobjEmailDetail);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("OTP password error :" + ex.Message);
                throw ex;
            }
            return lblnEmailSend;
        }

        private bool SendOTPSMS(MemberDetails lobjMemberDetails, string pstrOTPstring, DateTime pdtExpirydatetime, string pstrTemplateCode)
        {
            bool lblnSMSSend = false;
            try
            {
                if (lobjMemberDetails.MobileNumber != string.Empty)
                {
                    string lstrexpirydt = pdtExpirydatetime.ToString("dd/MM/yyyy");
                    //Communication Engine Call For SMS. 
                    CEHelper lobjcehelper = new CEHelper();
                    SmsDetails lobjSmsDetail = new SmsDetails();
                    List<string> lstSmsparameter = new List<string>();
                    lstSmsparameter.Add(pstrOTPstring);
                    //lstSmsparameter.Add(lstrexpirydt);
                    lobjSmsDetail.TemplateCode = pstrTemplateCode;
                    lobjSmsDetail.ListParameter = lstSmsparameter;
                    lobjSmsDetail.ReceiverMobile = Convert.ToString(lobjMemberDetails.MobileNumber);
                    lobjSmsDetail.ProgramId = Convert.ToString(lobjMemberDetails.ProgramId);
                    lobjSmsDetail.MemberId = Convert.ToString(lobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
                    lblnSMSSend = lobjcehelper.InsertSmsDetails(lobjSmsDetail);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("OTP SMS ERROR :" + ex.Message);
            }
            return lblnSMSSend;
        }

        private void SendVoucherEmail(List<VoucherSummary> lobjListVoucherSummary, MemberDetails pobjMemberDetails)
        {
            string lstrFinalTemplate = string.Empty;
            string lstrVoucherUrl = ConfigurationManager.AppSettings["VoucherUrl"];
            for (int i = 0; i < lobjListVoucherSummary.Count; i++)
            {
                string lstrTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("Templates/VoucherDetails.htm"));
                lstrFinalTemplate += string.Format(lstrTemplate, lobjListVoucherSummary[i].Amount, lobjListVoucherSummary[i].VoucherNo, lobjListVoucherSummary[i].ExpiryDate.ToString(), lstrVoucherUrl + lobjListVoucherSummary[i].VoucherNo + ".png");
            }
            List<string> lstrEmailparameter = new List<string>();
            lstrEmailparameter.Add(lstrFinalTemplate);
            SendEmail(lstrEmailparameter, pobjMemberDetails, "WORLDREWARDSCreateVoucher");
        }

        public List<VoucherSummary> CreateVoucher(MemberDetails pobjMemberDetails, IBEAccountTransactionDetails pobjIBEAccountTransactionDetails, List<VoucherDetails> pobjListVoucherDetails, string pstrCurrency)
        {
            string strRedeemMilesResponse = string.Empty;
            string strRollBackMilesResponse = string.Empty;
            bool lboolRollBackResponse = false;
            bool lboolRedeemResponseDA = false;
            bool lboolRollBackResponseDA = false;
            bool statusFlag = false;
            string lstrBlockDeposiAccount = string.Empty;
            DepositAccountClientHelper lobjDepositAccountClientHelper = new DepositAccountClientHelper();
            PGHelper lobjPGHelper = new PGHelper();
            List<ProgramCurrencyDefinition> lobjListProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
            ProgramConfigs lobjProgramConfigs = new ProgramConfigs();
            ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
            ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();

            lobjProgramDefinition = lobjProgramConfigs.GetProgramDetails(lstrProgramName);
            lobjListProgramCurrencyDefinition = lobjProgramConfigs.GetAllCurrencyDefinition(lobjProgramDefinition.ProgramId);
            lobjProgramCurrencyDefinition = lobjListProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals(pstrCurrency));

            string lstrVoucherNo = string.Empty;
            List<string> lstrVoucherNoList = new List<string>();
            List<VoucherSummary> lobjListVoucherSummary = new List<VoucherSummary>();


            RedemptionDetails lobjRedemptionDetails = new RedemptionDetails();
            lobjRedemptionDetails.Currency = pstrCurrency;
            lobjRedemptionDetails.RelationReference = pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;

            try
            {
                LoggingAdapter.WriteLog("Create Voucher Start Member " + lobjRedemptionDetails.RelationReference + " Time - " + DateTime.Now);
                for (int i = 0; i < pobjListVoucherDetails.Count; i++)
                {
                    VoucherSummary lobjVoucherSummary = new VoucherSummary();
                    lobjVoucherSummary.Amount = Convert.ToInt32(pobjListVoucherDetails[i].Value);

                    lobjVoucherSummary.ExpiryDate = pobjListVoucherDetails[i].DateOfIssue.AddMonths(lobjProgramDefinition.lobjProgramAttribute.VoucherExpirySchedule); // Get Expiry Date for voucher
                    int pintTotalPoints = Convert.ToInt32(pobjListVoucherDetails[i].Value / lobjProgramCurrencyDefinition.VoucherRate);
                    lobjRedemptionDetails.Points = pintTotalPoints;
                    lobjRedemptionDetails.Amount = pobjListVoucherDetails[i].Value;
                    LoggingAdapter.WriteLog("DoOtherRedemption Request Amount " + lobjRedemptionDetails.Amount + " POints - " + lobjRedemptionDetails.Points + " RelationReference -  " + pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " Currency - " + lobjRedemptionDetails.Currency);
                    strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption(lobjRedemptionDetails.Amount, lobjRedemptionDetails.Amount, lobjRedemptionDetails.Points, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, "Redemption InfiVoucher", Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.InfiVoucher), lobjRedemptionDetails.Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    LoggingAdapter.WriteLog(" DoOtherRedemption Response - " + strRedeemMilesResponse);
                    if (strRedeemMilesResponse != string.Empty)
                    {
                        if (pobjIBEAccountTransactionDetails != null)
                        {
                            pobjIBEAccountTransactionDetails.Amount = pobjListVoucherDetails[i].Value;
                            lstrBlockDeposiAccount = lobjDepositAccountClientHelper.BlockAccount(pobjIBEAccountTransactionDetails);
                            LoggingAdapter.WriteLog("BookingIntegrationFacade CreateVoucher DABlockAccount : " + lstrBlockDeposiAccount);


                            if (lstrBlockDeposiAccount != string.Empty)
                            {
                                pobjListVoucherDetails[i].MemberId = pobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;
                                pobjListVoucherDetails[i].EmailId = pobjMemberDetails.Email;
                                pobjListVoucherDetails[i].CustomerName = pobjMemberDetails.LastName;
                                pobjListVoucherDetails[i].MobileNumber = pobjMemberDetails.MobileNumber;
                                pobjListVoucherDetails[i].CreatedBy = pobjMemberDetails.MemberRelationsList.Find(l => l.RelationType.Equals(RelationType.LBMS)).RelationReference;

                                VoucherHelper lobjVoucherHelper = new VoucherHelper();
                                Core.Platform.Common.Entities.Response lobjCreateResponse = null;

                                lobjCreateResponse = new Core.Platform.Common.Entities.Response();

                                try
                                {
                                    lobjCreateResponse = lobjVoucherHelper.CreateVoucher(pobjListVoucherDetails[i]);
                                    LoggingAdapter.WriteLog("Create Voucher Txn Id = " + strRedeemMilesResponse + " Response = " + JSONSerialization.Serialize(lobjCreateResponse));
                                }
                                catch (Exception ex)
                                {
                                    LoggingAdapter.WriteLog("BookingIntegrationFacade CreateVoucher Client : " + ex.Message + Environment.NewLine + "Stack Trace: " + ex.StackTrace);
                                    statusFlag = false;
                                }

                                if (lobjCreateResponse != null && lobjCreateResponse.IsSucessful)
                                {
                                    lstrVoucherNo = JSONSerialization.Deserialize<string>(lobjCreateResponse.ReturnObject);
                                    if (lstrVoucherNo != string.Empty)
                                    {
                                        lobjVoucherSummary.VoucherNo = lstrVoucherNo;
                                        pobjIBEAccountTransactionDetails.Description = pobjListVoucherDetails[i].VoucherDescription;
                                        pobjIBEAccountTransactionDetails.BookingReference = lstrVoucherNo;
                                        pobjIBEAccountTransactionDetails.TransactionReference = lstrBlockDeposiAccount;
                                        pobjIBEAccountTransactionDetails.TransactionType = IBETransactionType.Blocked;
                                        lboolRedeemResponseDA = lobjDepositAccountClientHelper.RedeemAccount(pobjIBEAccountTransactionDetails);

                                        LoggingAdapter.WriteLog("BookingIntegrationFacade CreateVoucher DARedeemAccount : " + lboolRedeemResponseDA);

                                        if (lboolRedeemResponseDA)
                                        {
                                            statusFlag = true;
                                            lobjVoucherSummary.Status = true;
                                            lstrVoucherNoList.Add(lstrVoucherNo);
                                        }
                                        else
                                        {
                                            statusFlag = false;
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            pobjListVoucherDetails[i].MemberId = pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                            pobjListVoucherDetails[i].EmailId = pobjMemberDetails.Email;
                            pobjListVoucherDetails[i].CustomerName = pobjMemberDetails.LastName;
                            pobjListVoucherDetails[i].MobileNumber = pobjMemberDetails.MobileNumber;
                            pobjListVoucherDetails[i].CreatedBy = pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;

                            VoucherHelper lobjVoucherHelper = new VoucherHelper();
                            Core.Platform.Common.Entities.Response lobjCreateResponse = null;

                            lobjCreateResponse = new Core.Platform.Common.Entities.Response();

                            try
                            {
                                lobjCreateResponse = lobjVoucherHelper.CreateVoucher(pobjListVoucherDetails[i]);
                                LoggingAdapter.WriteLog("Create Voucher Txn Id = " + strRedeemMilesResponse + " Response = " + JSONSerialization.Serialize(lobjCreateResponse));
                            }
                            catch (Exception ex)
                            {
                                LoggingAdapter.WriteLog("BookingIntegrationFacade CreateVoucher Client : " + ex.Message + Environment.NewLine + "Stack Trace: " + ex.StackTrace);
                                statusFlag = false;
                            }

                            if (lobjCreateResponse != null && lobjCreateResponse.IsSucessful)
                            {
                                LoggingAdapter.WriteLog("CreateVoucher service response IsSucessful-True - " + strRedeemMilesResponse);
                                lstrVoucherNo = JSONSerialization.Deserialize<string>(lobjCreateResponse.ReturnObject);
                                if (lstrVoucherNo != string.Empty)
                                {
                                    lobjVoucherSummary.VoucherNo = lstrVoucherNo;
                                    statusFlag = true;
                                    lobjVoucherSummary.Status = true;
                                    lstrVoucherNoList.Add(lstrVoucherNo);
                                }
                                else
                                {
                                    statusFlag = false;
                                }

                            }
                            else
                            {
                                LoggingAdapter.WriteLog("CreateVoucher service response IsSucessful-False - " + strRedeemMilesResponse);
                            }
                        }

                        if (!statusFlag)
                        {
                            lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjListVoucherDetails[i].VoucherDescription, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                            if (pobjIBEAccountTransactionDetails != null && lboolRollBackResponse)
                            {
                                pobjIBEAccountTransactionDetails.TransactionReference = lstrBlockDeposiAccount;
                                pobjIBEAccountTransactionDetails.TransactionType = IBETransactionType.Blocked;
                                pobjIBEAccountTransactionDetails.Description = pobjListVoucherDetails[i].VoucherDescription;
                                lboolRollBackResponseDA = lobjDepositAccountClientHelper.RollBackAccount(pobjIBEAccountTransactionDetails);
                            }
                        }

                    }
                    else
                    {
                        LoggingAdapter.WriteLog("Do Other Redemption Response Null");
                    }
                    lobjListVoucherSummary.Add(lobjVoucherSummary);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("BookingintegrationFacade CreateVoucher : " + Environment.NewLine + ex.StackTrace);
            }
            LoggingAdapter.WriteLog("Create Voucher End Member " + lobjRedemptionDetails.RelationReference + " Time - " + DateTime.Now);
            return lobjListVoucherSummary;
            //return lstrVoucherNoList;
        }


        //Code for BillPay

        public UtilityPaymentResponse UtilityRedemption(UtilityPaymentRequest pobjUtilityPaymentRequest, MemberDetails pobjMemberDetails, RedemptionDetails pobjRedemptionDetails)
        {
            UtilityPaymentResponse lobjUtilityPaymentResponse = null;

            string strRedeemMilesResponse = string.Empty;
            string strRollBackMilesResponse = string.Empty;
            bool lboolRollBackResponse = false;
            string strFailureType = "";

            PGHelper lobjPGHelper = new PGHelper();
            UtilityPaymentHelper lobjUtilityPaymentHelper = new UtilityPaymentHelper();
            try
            {
                bool IsRedeem = true;

                strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption((float)(pobjUtilityPaymentRequest.AmountPaid), (float)(pobjUtilityPaymentRequest.AmountPaid), pobjRedemptionDetails.Points, string.Empty, string.Empty,
                                                                                pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference,
                                                                                pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword,
                                                                                Convert.ToString(UtilityType.UtilityPayment) + ", Reference Number : " + pobjUtilityPaymentRequest.ReferenceNumber,
                                                                                Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.BillPayment),
                                                                                pobjRedemptionDetails.Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                pobjRedemptionDetails.TransactionReference = strRedeemMilesResponse;

                IsRedeem = IsRedeem && (pobjRedemptionDetails.TransactionReference != "" && pobjRedemptionDetails.TransactionReference != string.Empty);

                if (IsRedeem)
                {
                    LoggingAdapter.WriteLog("RedeemMiles Success : " + strRedeemMilesResponse, "BillPayment");
                    try
                    {
                        pobjUtilityPaymentRequest.ExternalReference = strRedeemMilesResponse;
                        lobjUtilityPaymentResponse = lobjUtilityPaymentHelper.UtilityPayment(pobjUtilityPaymentRequest);
                        if (lobjUtilityPaymentResponse != null)
                        {
                            if (lobjUtilityPaymentResponse.ReturnCode == "0000")
                            {
                                List<string> lstEmailparameter = new List<string>();
                                List<string> lstSMSparameter = new List<string>();

                                lstEmailparameter = GenerateUtilityPaymentEmailParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                                SendEmail(lstEmailparameter, pobjMemberDetails, "UtilityPaymentSuccess");

                                lstSMSparameter = GenerateUtilityPaymentSMSParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                                SendSms(lstSMSparameter, pobjMemberDetails, "UtilityPaymentSuccess");
                                LoggingAdapter.WriteLog("Booking Facade - UtilityRedemption - Success  : ReferenceNumber - " + pobjUtilityPaymentRequest.ReferenceNumber + " Return Code  - " + lobjUtilityPaymentResponse.ReturnCode);
                            }
                            else
                            {
                                List<string> lstEmailparameter = new List<string>();
                                List<string> lstSMSparameter = new List<string>();

                                lstEmailparameter = GenerateUtilityPaymentFailedEmailParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                                SendEmail(lstEmailparameter, pobjMemberDetails, "UtilityPaymentFailed");

                                lstSMSparameter = GenerateUtilityPaymentFailedSMSParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                                SendSms(lstSMSparameter, pobjMemberDetails, "UtilityPaymentFailed");

                                LoggingAdapter.WriteLog("Booking Facade - UtilityRedemption - Failed : ReferenceNumber - " + pobjUtilityPaymentRequest.ReferenceNumber + " Return Code  - " + lobjUtilityPaymentResponse.ReturnCode);
                                strRedeemMilesResponse = pobjRedemptionDetails.TransactionReference;

                                if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                    lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference,
                                                                                             Convert.ToString(UtilityType.UtilityPayment) + ", Reference Number : " + pobjUtilityPaymentRequest.ReferenceNumber,
                                                                                             lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                                LoggingAdapter.WriteLog("RollBackMiles Success On Failure Payment : " + lboolRollBackResponse, "BillPayment");
                            }
                        }
                        else
                        {
                            List<string> lstEmailparameter = new List<string>();
                            List<string> lstSMSparameter = new List<string>();

                            lstEmailparameter = GenerateUtilityPaymentFailedEmailParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                            SendEmail(lstEmailparameter, pobjMemberDetails, "UtilityPaymentFailed");

                            lstSMSparameter = GenerateUtilityPaymentFailedSMSParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                            SendSms(lstSMSparameter, pobjMemberDetails, "UtilityPaymentFailed");

                            LoggingAdapter.WriteLog("Booking Facade - UtilityRedemption - Failed : ReferenceNumber - " + pobjUtilityPaymentRequest.ReferenceNumber);
                            strRedeemMilesResponse = pobjRedemptionDetails.TransactionReference;

                            if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference,
                                                                                         Convert.ToString(UtilityType.UtilityPayment) + ", Reference Number : " + pobjUtilityPaymentRequest.ReferenceNumber,
                                                                                         lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                            LoggingAdapter.WriteLog("RollBackMiles Success On Failure Payment : " + lboolRollBackResponse, "BillPayment");
                        }

                    }
                    catch (Exception ex)
                    {
                        List<string> lstEmailparameter = new List<string>();
                        List<string> lstSMSparameter = new List<string>();

                        lstEmailparameter = GenerateUtilityPaymentFailedEmailParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                        SendEmail(lstEmailparameter, pobjMemberDetails, "UtilityPaymentFailed");

                        lstSMSparameter = GenerateUtilityPaymentFailedSMSParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                        SendSms(lstSMSparameter, pobjMemberDetails, "UtilityPaymentFailed");

                        LoggingAdapter.WriteLog("Booking Facade - UtilityRedemption - Failed  : ReferenceNumber - " + pobjUtilityPaymentRequest.ReferenceNumber + "   " + ex.Message + ex.StackTrace);
                        strRedeemMilesResponse = pobjRedemptionDetails.TransactionReference;

                        if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                            lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference,
                                                                                     Convert.ToString(UtilityType.UtilityPayment) + ", Reference Number : " + pobjUtilityPaymentRequest.ReferenceNumber,
                                                                                     lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                        LoggingAdapter.WriteLog("RollBackMiles Success On Exception : " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "BillPayment");

                    }



                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Booking Facade - UtilityRedemption - Failed  : ReferenceNumber - " + pobjUtilityPaymentRequest.ReferenceNumber + "   " + ex.Message + ex.StackTrace);
            }

            return lobjUtilityPaymentResponse;
        }

        public List<string> GenerateUtilityPaymentEmailParameters(UtilityPaymentRequest pobjUtilityPaymentRequest, RedemptionDetails pobjRedemptionDetails, MemberDetails pobjMemberDetails)
        {
            List<string> lstEmailparameter = new List<string>();
            ////LastName 
            lstEmailparameter.Add(pobjMemberDetails.LastName);
            ////Company Name - DU
            lstEmailparameter.Add(pobjUtilityPaymentRequest.UtilityCompanyName);
            ////Account Name - PostPaid
            lstEmailparameter.Add(pobjUtilityPaymentRequest.UtilityAccountName);
            ////Account 9768989800
            lstEmailparameter.Add(pobjUtilityPaymentRequest.UtilityAccount);
            /////Amount 100.00
            lstEmailparameter.Add(Convert.ToString(pobjRedemptionDetails.Amount));
            ////Points
            lstEmailparameter.Add(Convert.ToString(pobjRedemptionDetails.Points));
            ////External Reference - Redemption Reference
            lstEmailparameter.Add(pobjUtilityPaymentRequest.ExternalReference);
            ////Reference Number - UtilityPayment
            lstEmailparameter.Add(pobjUtilityPaymentRequest.ReferenceNumber);





            return lstEmailparameter;
        }

        public List<string> GenerateUtilityPaymentFailedEmailParameters(UtilityPaymentRequest pobjUtilityPaymentRequest, RedemptionDetails pobjRedemptionDetails, MemberDetails pobjMemberDetails)
        {
            List<string> lstEmailparameter = new List<string>();
            ////LastName 
            lstEmailparameter.Add(pobjMemberDetails.LastName);
            ////Company Name - DU
            lstEmailparameter.Add(pobjUtilityPaymentRequest.UtilityCompanyName);
            ////Account Name - PostPaid
            lstEmailparameter.Add(pobjUtilityPaymentRequest.UtilityAccountName);
            ////Account 9768989800
            lstEmailparameter.Add(pobjUtilityPaymentRequest.UtilityAccount);
            /////Amount 100.00
            lstEmailparameter.Add(Convert.ToString(pobjRedemptionDetails.Amount));
            ////EmailId
            lstEmailparameter.Add(pobjMemberDetails.Email);

            return lstEmailparameter;
        }

        private List<string> GenerateUtilityPaymentSMSParameters(UtilityPaymentRequest pobjUtilityPaymentRequest, RedemptionDetails pobjRedemptionDetails, MemberDetails pobjMemberDetails)
        {
            List<string> lstSmsparameter = new List<string>();

            ////LastName 
            lstSmsparameter.Add(pobjMemberDetails.LastName);
            ////Company Name - DU
            lstSmsparameter.Add(pobjUtilityPaymentRequest.UtilityCompanyName);
            ////Account Name - PostPaid
            lstSmsparameter.Add(pobjUtilityPaymentRequest.UtilityAccountName);
            ////Account 9768989800
            lstSmsparameter.Add(pobjUtilityPaymentRequest.UtilityAccount);
            /////Amount 100.00
            lstSmsparameter.Add(Convert.ToString(pobjRedemptionDetails.Amount));
            ////Points
            lstSmsparameter.Add(Convert.ToString(pobjRedemptionDetails.Points));
            ////External Reference - Redemption Reference
            lstSmsparameter.Add(pobjUtilityPaymentRequest.ExternalReference);
            ////Reference Number - UtilityPayment
            lstSmsparameter.Add(pobjUtilityPaymentRequest.ReferenceNumber);

            return lstSmsparameter;
        }

        public List<string> GenerateUtilityPaymentFailedSMSParameters(UtilityPaymentRequest pobjUtilityPaymentRequest, RedemptionDetails pobjRedemptionDetails, MemberDetails pobjMemberDetails)
        {
            List<string> lstSmsparameter = new List<string>();
            ////LastName 
            lstSmsparameter.Add(pobjMemberDetails.LastName);
            ////Company Name - DU
            lstSmsparameter.Add(pobjUtilityPaymentRequest.UtilityCompanyName);
            ////Account Name - PostPaid
            lstSmsparameter.Add(pobjUtilityPaymentRequest.UtilityAccountName);
            ////Account 9768989800
            lstSmsparameter.Add(pobjUtilityPaymentRequest.UtilityAccount);
            /////Amount 100.00
            lstSmsparameter.Add(Convert.ToString(pobjRedemptionDetails.Amount));
            ////EmailId
            lstSmsparameter.Add(pobjMemberDetails.Email);

            return lstSmsparameter;
        }

        public UtilityPaymentResponse CharityRedemption(UtilityPaymentRequest pobjUtilityPaymentRequest, MemberDetails pobjMemberDetails, RedemptionDetails pobjRedemptionDetails)
        {
            UtilityPaymentResponse lobjUtilityPaymentResponse = null;

            string strRedeemMilesResponse = string.Empty;
            string strRollBackMilesResponse = string.Empty;
            bool lboolRollBackResponse = false;
            string strFailureType = "";

            PGHelper lobjPGHelper = new PGHelper();
            UtilityPaymentHelper lobjUtilityPaymentHelper = new UtilityPaymentHelper();
            try
            {
                bool IsRedeem = true;

                strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption((float)(pobjUtilityPaymentRequest.AmountPaid), (float)(pobjUtilityPaymentRequest.AmountPaid), pobjRedemptionDetails.Points, string.Empty, string.Empty,
                                                                                pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference,
                                                                                pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword,
                                                                                Convert.ToString(UtilityType.CharityPayment) + ", Reference Number : " + pobjUtilityPaymentRequest.ReferenceNumber,
                                                                                Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.BillPayment),
                                                                                pobjRedemptionDetails.Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                pobjRedemptionDetails.TransactionReference = strRedeemMilesResponse;

                IsRedeem = IsRedeem && (pobjRedemptionDetails.TransactionReference != "" && pobjRedemptionDetails.TransactionReference != string.Empty);

                if (IsRedeem)
                {
                    LoggingAdapter.WriteLog("RedeemMiles Success : " + strRedeemMilesResponse, "CharityPayment");
                    try
                    {
                        pobjUtilityPaymentRequest.ExternalReference = strRedeemMilesResponse;
                        lobjUtilityPaymentResponse = lobjUtilityPaymentHelper.UtilityPayment(pobjUtilityPaymentRequest);
                        if (lobjUtilityPaymentResponse != null)
                        {
                            if (lobjUtilityPaymentResponse.ReturnCode == "0000")
                            {
                                List<string> lstEmailparameter = new List<string>();
                                List<string> lstSMSparameter = new List<string>();

                                lstEmailparameter = GenerateUtilityPaymentEmailParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                                SendEmail(lstEmailparameter, pobjMemberDetails, "CharityPaymentSuccess");

                                lstSMSparameter = GenerateUtilityPaymentSMSParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                                SendSms(lstSMSparameter, pobjMemberDetails, "CharityPaymentSuccess");
                                LoggingAdapter.WriteLog("Booking Facade - CharityRedemption - Success : ReferenceNumber - " + pobjUtilityPaymentRequest.ReferenceNumber + " Return Code  - " + lobjUtilityPaymentResponse.ReturnCode);
                            }
                            else
                            {
                                List<string> lstEmailparameter = new List<string>();
                                List<string> lstSMSparameter = new List<string>();

                                lstEmailparameter = GenerateUtilityPaymentFailedEmailParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                                SendEmail(lstEmailparameter, pobjMemberDetails, "CharityPaymentFailed");

                                lstSMSparameter = GenerateUtilityPaymentFailedSMSParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                                SendSms(lstSMSparameter, pobjMemberDetails, "CharityPaymentFailed");

                                LoggingAdapter.WriteLog("Booking Facade - CharityRedemption - Failed  : ReferenceNumber - " + pobjUtilityPaymentRequest.ReferenceNumber + " Return Code  - " + lobjUtilityPaymentResponse.ReturnCode);
                                strRedeemMilesResponse = pobjRedemptionDetails.TransactionReference;

                                if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                    lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference,
                                                                                             Convert.ToString(UtilityType.CharityPayment) + ", Reference Number : " + pobjUtilityPaymentRequest.ReferenceNumber,
                                                                                             lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                                LoggingAdapter.WriteLog("RollBackMiles Success On Failure Payment : " + lboolRollBackResponse, "CharityPayment");
                            }
                        }
                        else
                        {
                            List<string> lstEmailparameter = new List<string>();
                            List<string> lstSMSparameter = new List<string>();

                            lstEmailparameter = GenerateUtilityPaymentFailedEmailParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                            SendEmail(lstEmailparameter, pobjMemberDetails, "CharityPaymentFailed");

                            lstSMSparameter = GenerateUtilityPaymentFailedSMSParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                            SendSms(lstSMSparameter, pobjMemberDetails, "CharityPaymentFailed");

                            LoggingAdapter.WriteLog("Booking Facade - CharityRedemption - Failed  : ReferenceNumber - " + pobjUtilityPaymentRequest.ReferenceNumber);
                            strRedeemMilesResponse = pobjRedemptionDetails.TransactionReference;

                            if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference,
                                                                                         Convert.ToString(UtilityType.CharityPayment) + ", Reference Number : " + pobjUtilityPaymentRequest.ReferenceNumber,
                                                                                         lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                            LoggingAdapter.WriteLog("RollBackMiles Success On Failure Payment : " + lboolRollBackResponse, "CharityPayment");
                        }

                    }
                    catch (Exception ex)
                    {
                        List<string> lstEmailparameter = new List<string>();
                        List<string> lstSMSparameter = new List<string>();

                        lstEmailparameter = GenerateUtilityPaymentFailedEmailParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                        SendEmail(lstEmailparameter, pobjMemberDetails, "CharityPaymentFailed");

                        lstSMSparameter = GenerateUtilityPaymentFailedSMSParameters(pobjUtilityPaymentRequest, pobjRedemptionDetails, pobjMemberDetails);
                        SendSms(lstSMSparameter, pobjMemberDetails, "CharityPaymentFailed");

                        LoggingAdapter.WriteLog("Booking Facade - CharityRedemption - Failed  : ReferenceNumber - " + pobjUtilityPaymentRequest.ReferenceNumber + "   " + ex.Message + ex.StackTrace);
                        strRedeemMilesResponse = pobjRedemptionDetails.TransactionReference;

                        if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                            lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference,
                                                                                     Convert.ToString(UtilityType.CharityPayment) + ", Reference Number : " + pobjUtilityPaymentRequest.ReferenceNumber,
                                                                                     lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                        LoggingAdapter.WriteLog("RollBackMiles Success On Exception : " + lboolRollBackResponse + " \n Exception \n" + ex.StackTrace, "CharityPayment");

                    }



                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Booking Facade - CharityRedemption - Failed : ReferenceNumber - " + pobjUtilityPaymentRequest.ReferenceNumber + "   " + ex.Message + ex.StackTrace);
            }

            return lobjUtilityPaymentResponse;
        }

        #region ViatorPackages
        public ViatorBookResponse ViatorBookPackages(ViatorBookRequest pobjViatorBookRequest, MemberDetails pobjMemberDetails, List<RedemptionDetails> pobjListOfRedemptionDetails)
        {
            bool IsBookingConfirm = false;
            string strRedeemMilesResponse = string.Empty;
            string strRollBackMilesResponse = string.Empty;
            bool lboolRollBackResponse = false;
            string strFailureType = "";
            LoggingAdapter.WriteLog("ViatorBookPackages Called");
            LoggingAdapter.WriteLog("ViatorBookPackages Called", "BookingLogCategory");

            PGHelper lobjPGHelper = new PGHelper();
            ViatorBookResponse lobjViatorBookResponse = null;
            try
            {
                bool IsRedeem = true;
                List<BookingPaymentBreakage> lobjListOfBookingPaymentBreakage = new List<BookingPaymentBreakage>();
                LoggingAdapter.WriteLog("pobjListOfRedemptionDetails Count " + pobjListOfRedemptionDetails.Count, "BookingLogCategory");
                for (int i = 0; i < pobjListOfRedemptionDetails.Count; i++)
                {
                    BookingPaymentBreakage lobjBookingPaymentBreakage = new BookingPaymentBreakage();
                    strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption((float)(pobjListOfRedemptionDetails[i].Amount), (float)(pobjListOfRedemptionDetails[i].Amount),
                        pobjListOfRedemptionDetails[i].Points, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find
                        (lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find
                        (lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, pobjViatorBookRequest.ProductDetails.SupplierName, Convert.ToInt32(RelationType.LBMS),
                        Convert.ToInt32(LoyaltyTxnType.Packages), pobjListOfRedemptionDetails[i].Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);

                    //strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption((float)(pobjListOfRedemptionDetails[i].Amount), (float)(pobjListOfRedemptionDetails[i].Amount),
                    //                          pobjListOfRedemptionDetails[i].Points, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find
                    //                          (lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find
                    //                          (lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, pobjBookingRequest.ItineraryDetails.OriginLocation + "-" + pobjBookingRequest.ItineraryDetails.DestinationLocation, Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.Air), pobjListOfRedemptionDetails[i].Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);

                    LoggingAdapter.WriteLog("BookingIntegrationFacade(ViatorBookPackages) RedeemMilesResponse - " + strRedeemMilesResponse);
                    pobjListOfRedemptionDetails[i].TransactionReference = strRedeemMilesResponse;
                    IsRedeem = IsRedeem && (pobjListOfRedemptionDetails[i].TransactionReference != "" && pobjListOfRedemptionDetails[i].TransactionReference != string.Empty);
                    lobjBookingPaymentBreakage.Currency = pobjListOfRedemptionDetails[i].DisplayCurrency;
                    lobjBookingPaymentBreakage.Amount = pobjListOfRedemptionDetails[i].Points;
                    //lobjBookingPaymentBreakage.TxnReference = strRedeemMilesResponse;
                    lobjBookingPaymentBreakage.TxnReference = pobjViatorBookRequest.ProductDetails.TransactionReference;
                    lobjListOfBookingPaymentBreakage.Add(lobjBookingPaymentBreakage);
                }
                BookingPaymentDetails lobjBookingPaymentDetails = new BookingPaymentDetails();
                lobjBookingPaymentDetails.BookingPaymentBreakageList = lobjListOfBookingPaymentBreakage;
                lobjBookingPaymentDetails.MemberId = pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjBookingPaymentDetails.PaymentType = PaymentType.Points;
                lobjBookingPaymentDetails.ServiceType = ServiceType.FLIGHT;
                lobjBookingPaymentDetails.Points = pobjListOfRedemptionDetails[0].Points;
                lobjBookingPaymentDetails.PointsTxnRefererence = pobjListOfRedemptionDetails[0].TransactionReference;
                if (IsRedeem)
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.Success;
                }
                else
                {
                    lobjBookingPaymentDetails.PaymentStatus = PaymentStatus.PaymentDone;
                }

                pobjViatorBookRequest.ProductDetails.BookingPaymentDetails = lobjBookingPaymentDetails;

                if (IsRedeem)
                {
                    pobjViatorBookRequest.ProductDetails.TransactionReference = pobjListOfRedemptionDetails[0].TransactionReference;
                    ViatorServiceClient lobjViatorServiceClient = new ViatorServiceClient();


                    try
                    {
                        LoggingAdapter.WriteLog("RedeemMiles Success : " + strRedeemMilesResponse, "BookingLogCategory");
                        try
                        {

                            lobjViatorBookResponse = lobjViatorServiceClient.BookingDetails(pobjViatorBookRequest);
                            LoggingAdapter.WriteLog("Viator Response " + lobjViatorBookResponse.BookingItinerary.BookingStatus.Confirmed, "BookingLogCategory");
                        }
                        catch (Exception ex)
                        {
                            LoggingAdapter.WriteLog("CreateViatorBooking Failure BookingFacade:" + ex.Message + ex.StackTrace);
                        }

                        if (lobjViatorBookResponse != null && lobjViatorBookResponse.BookingItinerary.BookingStatus.Confirmed && lobjViatorBookResponse.BookingItinerary.ItemSummaries[0].BookingStatus.Confirmed && lobjViatorBookResponse.BookingItinerary.ItineraryId != string.Empty && lobjViatorBookResponse.BookingItinerary.ItemSummaries[0].ItemId > 0 && lobjViatorBookResponse.BookingItinerary.VoucherKey != string.Empty && lobjViatorBookResponse.BookingItinerary.VoucherURL != string.Empty)
                        {
                            LoggingAdapter.WriteLog("Booking called ItineraryId Id  : " + lobjViatorBookResponse.BookingItinerary.ItineraryId + "IsBookingConfirm:" + IsBookingConfirm, "BookingLogCategory");
                            IsBookingConfirm = true;
                        }
                        else
                        {
                            LoggingAdapter.WriteLog("IsBookingConfirm:" + IsBookingConfirm, "BookingLogCategory");
                            IsBookingConfirm = false;
                        }

                        if (IsBookingConfirm)
                        {
                            LoggingAdapter.WriteLog("Booking called Trip Id  : " + lobjViatorBookResponse.BookingItinerary.ItineraryId, "BookingLogCategory");
                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter.Add(Convert.ToString(lobjViatorBookResponse.BookingItinerary.BookingRefId));
                            lstEmailparameter.Add("VIATORBOOK");
                            LoggingAdapter.WriteLog("Book Succesful Last Name = " + pobjMemberDetails.LastName, "BookingLogCategory");

                            SendEmail(lstEmailparameter, pobjMemberDetails, "VIATORBOOK");

                        }
                        else
                        {
                            LoggingAdapter.WriteLog("Viator Booking Failure", "BookingLogCategory");

                            for (int i = 0; i < pobjListOfRedemptionDetails.Count; i++)
                            {
                                strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                LoggingAdapter.WriteLog("RollBack Transaction Called : Transaction Ref " + strRedeemMilesResponse + "-RelationReference " + pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference + "-ProductTitle " + pobjViatorBookRequest.ProductDetails.SupplierName + "-MerchantId " + lstrMerchantId + "-MerchantUserName " + lstrMerchantUserName + "-MerchantPwd " + lstrMerchantPassword, "BookingLogCategory");
                                if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                    lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjViatorBookRequest.ProductDetails.SupplierName, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                                LoggingAdapter.WriteLog("RollBack Transaction Response " + lboolRollBackResponse, "BookingLogCategory");
                            }

                            List<string> lstEmailparameter = new List<string>();
                            lstEmailparameter.Add(Convert.ToString(pobjViatorBookRequest.MemberId));
                            lstEmailparameter.Add("VIATORFAIL");
                            SendFailureEmail(lstEmailparameter, pobjMemberDetails, "VIATORFAIL");
                        }

                    }
                    catch (Exception ex)
                    {
                        LoggingAdapter.WriteLog("Viator Response Failure BookingFacade:" + ex.Message + ex.StackTrace, "BookingLogCategory");

                        if (!IsBookingConfirm)
                        {
                            for (int i = 0; i < pobjListOfRedemptionDetails.Count; i++)
                            {
                                strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                                LoggingAdapter.WriteLog("RollBack Transaction Called : Transaction Ref " + strRedeemMilesResponse + "-RelationReference " + pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference + "-ProductTitle " + pobjViatorBookRequest.ProductDetails.SupplierName + "-MerchantId " + lstrMerchantId + "-MerchantUserName " + lstrMerchantUserName + "-MerchantPwd " + lstrMerchantPassword, "BookingLogCategory");
                                if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                                    lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjViatorBookRequest.ProductDetails.SupplierName, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                                LoggingAdapter.WriteLog("RollBack Transaction Response " + lboolRollBackResponse, "BookingLogCategory");
                            }
                        }

                        List<string> lstEmailparameter = new List<string>();
                        lstEmailparameter.Add(Convert.ToString(pobjViatorBookRequest.MemberId));
                        lstEmailparameter.Add("VIATORFAIL");
                        SendFailureEmail(lstEmailparameter, pobjMemberDetails, "VIATORFAIL");
                    }
                }
                else
                {
                    LoggingAdapter.WriteLog("Redeem Miles Unsuccessful", "BookingLogCategory");
                    for (int i = 0; i < pobjListOfRedemptionDetails.Count - 1; i++)
                    {
                        strRedeemMilesResponse = pobjListOfRedemptionDetails[i].TransactionReference;
                        if (strRedeemMilesResponse != "" && strRedeemMilesResponse != string.Empty)
                            LoggingAdapter.WriteLog("RollBack Transaction Called : Transaction Ref " + strRedeemMilesResponse + "-RelationReference " + pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference + "-ProductTitle " + pobjViatorBookRequest.ProductDetails.SupplierName + "-MerchantId " + lstrMerchantId + "-MerchantUserName " + lstrMerchantUserName + "-MerchantPwd " + lstrMerchantPassword, "BookingLogCategory");
                        lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjViatorBookRequest.ProductDetails.SupplierName, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                    }

                    strFailureType += string.Format("{0}/", FailureType.REDEEMMILES);
                    List<string> lstEmailparameter = new List<string>();
                    lstEmailparameter.Add(Convert.ToString(lobjViatorBookResponse.BookingItinerary.DistributorRef));
                    lstEmailparameter.Add("VIATORFAIL");
                    SendFailureEmail(lstEmailparameter, pobjMemberDetails, "VIATORFAIL");
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ViatorBookPackages Exception " + ex.Message.ToString() + Environment.NewLine + "Stack Trace" + ex.StackTrace, "BookingLogCategory");
            }

            return lobjViatorBookResponse;
        }
        #endregion

        //public bool SendViatorBookingEmail(string pstrBookingReference, string pstrEmailID, string pstrUserName, MemberDetails pobjMemberDetails, string pstrTemplateCode)
        //{
        //    LoggingAdapter.WriteLog("BookingReference: " + pstrBookingReference + " Email Id: " + pstrEmailID + " UserName: " + pstrUserName + " TemplateCode: " + pstrTemplateCode, "BookingLogCategory");
        //    try
        //    {
        //        EmailDetails lobjEmailDetail = new EmailDetails();
        //        List<string> lstAttachment = new List<string>();
        //        CEHelper lobjcehelper = new CEHelper();
        //        LoggingAdapter.WriteLog("CE Create object");
        //        lobjEmailDetail.TemplateCode = pstrTemplateCode;
        //        lobjEmailDetail.ListParameter = new List<string>();
        //        LoggingAdapter.WriteLog("CE Set Parameter");
        //        lobjEmailDetail.ListParameter.Add(pstrUserName);
        //        lobjEmailDetail.ListParameter.Add(pstrBookingReference);
        //        lobjEmailDetail.ProgramId = Convert.ToString(pobjMemberDetails.ProgramId);
        //        lobjEmailDetail.MemberId = Convert.ToString(pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)));
        //        LoggingAdapter.WriteLog("CE Parameter = " + lobjEmailDetail.ProgramId + " | " + lobjEmailDetail.MemberId);
        //        lobjEmailDetail.AttachmentList = lstAttachment;
        //        lobjEmailDetail.To = pstrEmailID;
        //        LoggingAdapter.WriteLog("CE paramete 1 = " + lobjEmailDetail.To);
        //        lobjcehelper.InsertEmailDetails(lobjEmailDetail);
        //        LoggingAdapter.WriteLog("CE Call Finish");
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("ViatorEmail SendFailed Exception " + ex.Message.ToString(), "BookingLogCategory");
        //    }
        //    return true;
        //}

        #region "LOUNGE"

        public LoungeBookingResponse BookLounge(MemberDetails pobjMemberDetails, IBEAccountTransactionDetails pobjIBEAccountTransactionDetails, LoungeBookingRequest pobjLoungeBookingRequest, string pstrCurrency, LoungeSearchResponse pobjLoungeSearchResponse)
        {
            LoungeBookingResponse lobjLoungeBookingResponse = new LoungeBookingResponse();
            string strRedeemMilesResponse = string.Empty;
            string strRollBackMilesResponse = string.Empty;
            bool lboolRollBackResponse = false;
            string strFailureType = "";
            
            bool statusFlag = false;
            string lstrBlockDeposiAccount = string.Empty;
            bool lboolRedeemResponseDA = false;
            string lstrTypeCode = string.Empty;
            bool lboolRollBackResponseDA = false;

            PGHelper lobjPGHelper = new PGHelper();
            DepositAccountClientHelper lobjDepositAccountClientHelper = new DepositAccountClientHelper();

            try
            {



                List<ProgramCurrencyDefinition> lobjListProgramCurrencyDefinition = new List<ProgramCurrencyDefinition>();
                ProgramConfigs lobjProgramConfigs = new ProgramConfigs();
                ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
                ProgramCurrencyDefinition lobjProgramCurrencyDefinition = new ProgramCurrencyDefinition();

                lobjProgramDefinition = lobjProgramConfigs.GetProgramDetails(lstrProgramName);
                lobjListProgramCurrencyDefinition = lobjProgramConfigs.GetAllCurrencyDefinition(lobjProgramDefinition.ProgramId);
                lobjProgramCurrencyDefinition = lobjListProgramCurrencyDefinition.Find(lobj => lobj.Currency.Equals(pstrCurrency));





                RedemptionDetails lobjRedemptionDetails = new RedemptionDetails();
                lobjRedemptionDetails.Currency = pstrCurrency;
                lobjRedemptionDetails.RelationReference = pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjRedemptionDetails.Points = pobjLoungeBookingRequest.LoungeVoucherDetails.TotalPoints;
                lobjRedemptionDetails.Amount = pobjLoungeBookingRequest.LoungeVoucherDetails.TotalAmount;

                LoggingAdapter.WriteLog("DoOtherRedemption Request Amount " + lobjRedemptionDetails.Amount + " POints - " + lobjRedemptionDetails.Points + " RelationReference -  " + pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " Currency - " + lobjRedemptionDetails.Currency);
                strRedeemMilesResponse = lobjPGHelper.DoOtherRedemption(lobjRedemptionDetails.Amount, lobjRedemptionDetails.Amount, lobjRedemptionDetails.Points, string.Empty, string.Empty, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).WebPassword, "Lounge Redemption", Convert.ToInt32(RelationType.LBMS), Convert.ToInt32(LoyaltyTxnType.Lounge), lobjRedemptionDetails.Currency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                LoggingAdapter.WriteLog(" DoOtherRedemption Response - " + strRedeemMilesResponse);


                if (strRedeemMilesResponse != string.Empty)
                {
                    pobjLoungeBookingRequest.LoungeVoucherDetails.TransactionReference = strRedeemMilesResponse;

                    if (pobjIBEAccountTransactionDetails != null)
                    {
                        pobjIBEAccountTransactionDetails.Amount = pobjLoungeBookingRequest.LoungeVoucherDetails.TotalAmount;
                        lstrBlockDeposiAccount = lobjDepositAccountClientHelper.BlockAccount(pobjIBEAccountTransactionDetails);
                        LoggingAdapter.WriteLog("BookingIntegrationFacade CreateVoucher DABlockAccount : " + lstrBlockDeposiAccount);


                        if (lstrBlockDeposiAccount != string.Empty)
                        {

                            pobjLoungeBookingRequest.LoungeVoucherDetails.VoucherPath = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["LoungeVoucherUrl"]) + pobjLoungeBookingRequest.LoungeVoucherDetails.VoucherNo +".png";


                            lobjLoungeBookingResponse.LoungeVoucherDetails = pobjLoungeBookingRequest.LoungeVoucherDetails;

                            pobjIBEAccountTransactionDetails.Description = pobjLoungeBookingRequest.LoungeVoucherDetails.VoucherDetails;
                            pobjIBEAccountTransactionDetails.BookingReference = pobjLoungeBookingRequest.LoungeVoucherDetails.VoucherNo;
                            pobjIBEAccountTransactionDetails.TransactionReference = lstrBlockDeposiAccount;
                            pobjIBEAccountTransactionDetails.TransactionType = IBETransactionType.Blocked;
                            lboolRedeemResponseDA = lobjDepositAccountClientHelper.RedeemAccount(pobjIBEAccountTransactionDetails);

                            LoggingAdapter.WriteLog("BookingIntegrationFacade CreateVoucher DARedeemAccount : " + lboolRedeemResponseDA);

                            if (lboolRedeemResponseDA)
                            {
                                IBELoungeServiceClient lobjIBELoungeServiceClient = new IBELoungeServiceClient();
                                lobjLoungeBookingResponse.LoungeVoucherDetails.BookingReferenceId = lobjIBELoungeServiceClient.BookLounge(pobjLoungeBookingRequest);


                                if (lobjLoungeBookingResponse != null && lobjLoungeBookingResponse.LoungeVoucherDetails.BookingReferenceId != string.Empty)
                                {
                                    statusFlag = true;
                                    lobjLoungeBookingResponse.Status = true;
                                    //////Send Email /////
                                    SendEmail(GenerateLoungeEmailParameters(pobjMemberDetails, pobjLoungeBookingRequest, pstrCurrency, pobjLoungeSearchResponse, lobjProgramDefinition), pobjMemberDetails, lobjProgramDefinition.ProgramName + "CreateLoungeVoucher");
                                }
                                else
                                {
                                    statusFlag = false;
                                    lobjLoungeBookingResponse.Status = false;
                                }
                            }
                            else
                            {
                                statusFlag = false;
                                lobjLoungeBookingResponse.Status = false;
                            }
                        }
                    }
                    else
                    {

                        pobjLoungeBookingRequest.LoungeVoucherDetails.VoucherPath = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["LoungeVoucherUrl"]) + pobjLoungeBookingRequest.LoungeVoucherDetails.VoucherNo +".png";
                        lobjLoungeBookingResponse.LoungeVoucherDetails = pobjLoungeBookingRequest.LoungeVoucherDetails;

                            IBELoungeServiceClient lobjIBELoungeServiceClient = new IBELoungeServiceClient();
                            lobjLoungeBookingResponse.LoungeVoucherDetails.BookingReferenceId = lobjIBELoungeServiceClient.BookLounge(pobjLoungeBookingRequest);


                            if (lobjLoungeBookingResponse != null && lobjLoungeBookingResponse.LoungeVoucherDetails.BookingReferenceId != string.Empty)
                            {
                                statusFlag = true;
                                lobjLoungeBookingResponse.Status = true;
                                //////Send Email /////
                                SendEmail(GenerateLoungeEmailParameters(pobjMemberDetails, pobjLoungeBookingRequest, pstrCurrency, pobjLoungeSearchResponse, lobjProgramDefinition), pobjMemberDetails, lobjProgramDefinition.ProgramName + "CreateLoungeVoucher");
                            }
                            else
                            {
                                statusFlag = false;
                                lobjLoungeBookingResponse.Status = false;
                            }
                    }

                    if (!statusFlag)
                    {
                        lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, "Lounge Redemption", lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                        if (pobjIBEAccountTransactionDetails != null && lboolRollBackResponse)
                        {
                            pobjIBEAccountTransactionDetails.TransactionReference = lstrBlockDeposiAccount;
                            pobjIBEAccountTransactionDetails.TransactionType = IBETransactionType.Blocked;
                            pobjIBEAccountTransactionDetails.Description = "Lounge Redemption";
                            lboolRollBackResponseDA = lobjDepositAccountClientHelper.RollBackAccount(pobjIBEAccountTransactionDetails);
                        }
                    }

                }
                else
                {
                    LoggingAdapter.WriteLog("BookLounge : Do Other Redemption Response Null");
                }

            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("BookingintegrationFacade BookLounge : " + Environment.NewLine + ex.StackTrace);
                lboolRollBackResponse = lobjPGHelper.RollBackTransaction(strRedeemMilesResponse, pobjMemberDetails.MemberRelationsList.Find(lob => lob.RelationType.Equals(RelationType.LBMS)).RelationReference, "Lounge Redemption", lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
                if (pobjIBEAccountTransactionDetails != null && lboolRollBackResponse)
                {
                    pobjIBEAccountTransactionDetails.TransactionReference = lstrBlockDeposiAccount;
                    pobjIBEAccountTransactionDetails.TransactionType = IBETransactionType.Blocked;
                    pobjIBEAccountTransactionDetails.Description = "Lounge Redemption";
                    lboolRollBackResponseDA = lobjDepositAccountClientHelper.RollBackAccount(pobjIBEAccountTransactionDetails);
                }
            }



            return lobjLoungeBookingResponse;
        }

        public List<string> GenerateLoungeEmailParameters(MemberDetails pobjMemberDetails, LoungeBookingRequest pobjLoungeBookingRequest, string pstrCurrency, LoungeSearchResponse pobjLoungeSearchResponse, ProgramDefinition pobjProgramDefinition)
        {
            string lstrVoucherUrl = Convert.ToString(ConfigurationManager.AppSettings["LoungeVoucherUrl"]);
            string lstrVoucherQRUrl = Convert.ToString(ConfigurationManager.AppSettings["LoungeVoucherQRUrl"]);

            List<string> lstEmailparameter = new List<string>();
            lstEmailparameter.Add(Convert.ToString(pobjProgramDefinition.ProgramId));//0
            lstEmailparameter.Add(pobjProgramDefinition.ProgramName);//1
            lstEmailparameter.Add(pobjLoungeBookingRequest.LoungeVoucherDetails.VoucherNo);//2
            lstEmailparameter.Add(Convert.ToString(pobjLoungeBookingRequest.LoungeVoucherDetails.TotalPoints));//3
            lstEmailparameter.Add(Convert.ToString(pobjProgramDefinition.lobjProgramAttribute.BaseCurrency));//4

            lstEmailparameter.Add(pobjLoungeBookingRequest.LoungeVoucherDetails.PassangerName);//5
            lstEmailparameter.Add(pobjLoungeBookingRequest.LoungeVoucherDetails.TravelDate.ToString("ddMMyyyy"));//6
            lstEmailparameter.Add(pobjMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);//7

            lstEmailparameter.Add(lstrVoucherUrl + pobjLoungeBookingRequest.LoungeVoucherDetails.VoucherNo + ".png");//8

            lstEmailparameter.Add(lstrVoucherQRUrl + pobjLoungeBookingRequest.LoungeVoucherDetails.QRPath);//9

            lstEmailparameter.Add(pobjLoungeBookingRequest.LoungeVoucherDetails.TravelDate.ToString("ddMMyyyy"));//10
            lstEmailparameter.Add(pobjLoungeBookingRequest.LoungeVoucherDetails.FlightNumber.ToString());//11
            lstEmailparameter.Add(pobjLoungeSearchResponse.LoungeMaster.CityName);//12
            lstEmailparameter.Add(pobjLoungeSearchResponse.LoungeMaster.LoungeName);//13
            lstEmailparameter.Add(pobjLoungeSearchResponse.LoungeMaster.AirportName);//14
            lstEmailparameter.Add(pobjLoungeBookingRequest.LoungeVoucherDetails.NoOfPassangers.ToString());//15
            lstEmailparameter.Add(pobjLoungeBookingRequest.LoungeVoucherDetails.FlightDetails);//16

            return lstEmailparameter;
        }

        #endregion "LOUNGE"
    }
}
