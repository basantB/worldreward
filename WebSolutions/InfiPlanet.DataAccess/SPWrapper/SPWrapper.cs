﻿using Core.Platform.Member.Entites;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.DataAccess.BusinessFacade;
using Framework.EnterpriseLibrary.DataAccess.Entities;
using InfiPlanet.DataAccess.Constants;
using InfiPlanet.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace InfiPlanet.DataAccess.SPWrapper
{
    public class SPWrapper
    {
        public static bool CheckMemberLoginDetails(SearchMember pobjSearchMemberForLogin)
        {
            DataSet ds = new DataSet();
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, DataAccessSettings.GetProviderName());
            IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.CheckMemberLoginDetails, CommandType.StoredProcedure);
            bool lblnMemberExists = false;
            try
            {

                ocmd[DBFields.RelationReference] = pobjSearchMemberForLogin.RelationReference;
                ocmd[DBFields.RelationType] = pobjSearchMemberForLogin.RelationType;
                ocmd.FillDataSet(ds);
                if (ds != null && ds.Tables[0].Rows.Count == 0)
                    lblnMemberExists = false;
                else if (ds != null && ds.Tables[0].Rows.Count > 0)
                    lblnMemberExists = true;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CheckMemberLoginDetails" + ex.Message + Environment.NewLine);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }
            return lblnMemberExists;
        }

        public static DataSet GetInfiplanetProgramList()
        {
            DataSet ds = new DataSet();
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, DataAccessSettings.GetProviderName());
            IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.GetInfiplanetProgramList, CommandType.StoredProcedure);
            try
            {

                ocmd.FillDataSet(ds);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetInfiplanetProgramList" + ex.Message + Environment.NewLine);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }
            return ds;
        }

        public static DataSet GetMerchantDetailsByProgramIdAndLoyaltyTxnType(ProgramMerchantConfig pobjProgramMerchantConfig)
        {
            DataSet ds = new DataSet();
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, DataAccessSettings.GetProviderName());
            IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.GetMerchantDetailsByProgramIdAndLoyaltyTxnType, CommandType.StoredProcedure);
            try
            {
                ocmd[DBFields.ProgramId] = pobjProgramMerchantConfig.ProgramId;
                ocmd[DBFields.LoyaltyTxnType] = Convert.ToInt32(pobjProgramMerchantConfig.LoyaltyTxnType);
                ocmd.FillDataSet(ds);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetMerchantDetailsByProgramIdAndLoyaltyTxnType" + ex.Message + Environment.NewLine);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }
            return ds;
        }

        public static bool IsLoginPrimaryReferenceAlreadyExist(SearchMember pobjSearchMember)
        {
            DataSet ds = new DataSet();
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, DataAccessSettings.GetProviderName());
            IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.IsLoginPrimaryReferenceAlreadyExist, CommandType.StoredProcedure);
            int lobjrowsAffected=0;
            try
            {

                ocmd[pobjSearchMember.SearchColumnName] = pobjSearchMember.SearchColumnValue;
                ocmd[DBFields.RelationType] = pobjSearchMember.RelationType;
                lobjrowsAffected = Convert.ToInt32(ocmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CheckMemberLoginDetails" + ex.Message + Environment.NewLine);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }
            return lobjrowsAffected >0;
        }

        public static bool ActivateMemberAccountByPrimaryReference(MemberDetails pobjMemberDetails)
        {
            int lobjrowsAffected = 0;

            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, DataAccessSettings.GetProviderName());
            IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.ActivateMemberAccountByPrimaryReference, CommandType.StoredProcedure);
            try
            {
                ocmd[DBFields.MemberId] = pobjMemberDetails.Id;
                ocmd[DBFields.WebPassword] = pobjMemberDetails.MemberRelationsList[0].WebPassword;
                ocmd[DBFields.RelationReference] = pobjMemberDetails.MemberRelationsList[0].RelationReference;
                ocmd[DBFields.RelationType] = Convert.ToInt32(pobjMemberDetails.MemberRelationsList[0].RelationType);
                ocmd[DBFields.CreatedBy] = pobjMemberDetails.MemberRelationsList[0].CreatedBy;
                lobjrowsAffected = ocmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("ActivateMemberAccountByPrimaryReference " + ex.Message + Environment.NewLine);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }
            return lobjrowsAffected > 0;
        }

        public static DataSet GetEnrollMemberDetailsByPrimaryReference(SearchMember pobjSearchMember)
        {
            DataSet ds = new DataSet();
            IDBConnection lobjConn = DBManager.Open(DataAccessSettings.GetDataAccessSettings().DefaultDatabase, false, Provider.SQL);
            IDBCommand ocmd = lobjConn.CreateCommand(StoredProcedures.GetEnrollMemberDetailsByPrimaryReference, CommandType.StoredProcedure);

            try
            {
                ocmd[pobjSearchMember.SearchColumnName] = pobjSearchMember.SearchColumnValue;
                ocmd[DBFields.RelationType] = pobjSearchMember.RelationType;
                ocmd.FillDataSet(ds);
                lobjConn.Close();
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("GetEnrollMemberDetails" + ex.Message + Environment.NewLine);
            }
            finally
            {
                lobjConn.Close();
                lobjConn = null;
            }
            return ds;
        }
    }
}
