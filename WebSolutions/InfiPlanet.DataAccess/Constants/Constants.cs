﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InfiPlanet.DataAccess.Constants
{
    public class DBFields
    {
        public static string ProgramName="Program_Name";
        public static string IsActive="Is_Active";
        public static string LoyaltyTxnType = "Loyalty_Txn_Type";
        public static string MerchantId = "Merchant_Id";
        public static string MerchantName="Merchant_Name";
        public static string MerchantPassword="Merchant_Password";
        public static string SecreteKey="Secrete_Key";
        

        // Member Detail DB Fields
        public static string Id = "Id";
        public static string FirstName = "First_Name";
        public static string LastName = "Last_Name";
        public static string EmailId = "Email_Id";
        public static string DOB = "DOB";
        public static string MobileNumber = "Mobile_Number";
        public static string Nationality = "Nationality";
        public static string NationalId = "National_Id";
        public static string PassportNumber = "Passport_Number";
        public static string Address1 = "Address1";
        public static string Address2 = "Address2";
        public static string Gender = "Gender";
        public static string CreatedBy = "Created_By";
        public static string CreatedOn = "Created_On";
        public static string UpdatedBy = "Updated_By";
        public static string UpdatedOn = "Updated_On";
        public static string SourceChannel = "Source_Channel";
        public static string ProgramId = "Program_Id";
        public static string EnrollmentDate = "Enrollment_Date";
        public static string IsMerchant = "IsMerchant";
        public static string MothersMaidenName = "Mothers_Maiden_Name";
        public static string ActivatedDate = "Activated_Date";
        public static string AdditionalDetails = "Additional_Details";
        public static string Password="Password";
        public static string AdditionalDetails1="Additional_Details1";
        public static string CustomerType="Cust_Type";
        public static string PreferredLanguage="Preferred_Language";
        public static string CustomerSegment="Cust_Segment";

        // Member Relation

        public static string RID = "RID";
        public static string MemberId = "Member_Id";
        public static string RelationType = "Relation_Type";
        public static string RelationReference = "Relation_Reference";
        public static string WebPassword = "Web_Password";
        public static string MPIN = "MPIN";
        public static string POSPIN = "POSPIN";
        public static string lastloggedIn = "Last_LoggedIn";
        public static string LoginAttempt = "Login_Attempt";
        public static string IsAccountActivated = "IsAccountActivated";
        public static string Status = "Status";
        public static string SubRelationIdentifier = "SubRelation_Identifier";
        public static string FirstCreditAmount = "First_Credit_Amount";
        public static string IsAccrualAllowed = "IsAccrualAllowed";
        public static string RelationCreatedBy = "RelationCreatedBy";
        public static string RelationCreatedOn = "RelationCreatedOn";
        public static string ForceChangePassword = "ForceChangePassword";
        public static string PrimaryReference = "Primary_Reference";
        public static string ParentStatus = "Parent_Status";

        //sub relations
        public static string SID = "SID";
        public static string ProductCode = "Product_Code";
        public static string Type = "Type";
        public static string LastSixDigits = "Last_Six_Digits";
        public static string ExpiryDate = "Expiry_Date";
        public static string DealCode = "Deal_Code";
        public static string CreationDate = "Creation_Date";
        public static string SRStatus = "SRStatus";
        
    }

    public class StoredProcedures
    {
        public static string CheckMemberLoginDetails="Sp_CheckMemberLoginDetails";
        public static string GetInfiplanetProgramList="Sp_GetAllInfiplanetProgramConfig";
        public static string GetMerchantDetailsByProgramIdAndLoyaltyTxnType="Sp_GetMerchantDetailsByProgramIdAndLoyaltyTxnType";
        public static string IsLoginPrimaryReferenceAlreadyExist="Sp_IsLoginPrimaryReferenceAlreadyExist";
        public static string ActivateMemberAccountByPrimaryReference = "Sp_ActivateMemberAccountByPrimaryReference";
        public static string GetEnrollMemberDetailsByPrimaryReference = "Sp_GetEnrollMemberDetailsByPrimaryReference";
    
    }
}
