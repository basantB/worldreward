﻿using Core.Platform.Member.Entites;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.Transactions.Entites;
using Framework.EnterpriseLibrary.Adapters;
using InfiPlanet.DataAccess.Constants;
using InfiPlanet.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace InfiPlanet.DataAccess.DataMapper
{
    public class DataMapper
    {
        public static List<ProgramDefinition> MapInfiplanetProgramList(DataSet pobjDs)
        {
            List<ProgramDefinition> lobjListProgramDefinition = null;

            if (pobjDs != null && pobjDs.Tables.Count > 0 && pobjDs.Tables[0].Rows.Count > 0)
            {
                lobjListProgramDefinition = new List<ProgramDefinition>();
                for (int i = 0; i < pobjDs.Tables[0].Rows.Count; i++)
                {
                    ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
                    DataRow dr = pobjDs.Tables[0].Rows[i];
                    lobjProgramDefinition.ProgramId = Convert.ToInt32(dr[DBFields.ProgramId]);
                    lobjProgramDefinition.ProgramName = Convert.ToString(dr[DBFields.ProgramName]);
                    lobjProgramDefinition.CreatedBy = Convert.ToString(dr[DBFields.CreatedBy]);
                    lobjProgramDefinition.UpdatedBy = Convert.ToString(dr[DBFields.UpdatedBy]);
                    lobjProgramDefinition.CreatedOn = Convert.ToDateTime(dr[DBFields.CreatedOn]);
                    lobjProgramDefinition.UpdatedOn = Convert.ToDateTime(dr[DBFields.UpdatedOn]);
                    lobjListProgramDefinition.Add(lobjProgramDefinition);
                }
            }
            return lobjListProgramDefinition;
        }

        public static ProgramMerchantConfig MapMerchantDetailsByProgramIdAndLoyaltyTxnType(DataSet pobjDs)
        {
            try
            {
                ProgramMerchantConfig lobjProgramMerchantConfig = null;
                if (pobjDs != null && pobjDs.Tables.Count > 0 && pobjDs.Tables[0].Rows.Count > 0)
                {
                    lobjProgramMerchantConfig=new ProgramMerchantConfig();
                    DataRow dr = pobjDs.Tables[0].Rows[0];
                    lobjProgramMerchantConfig.Id = Convert.ToInt32(dr[DBFields.Id]);
                    lobjProgramMerchantConfig.ProgramId = Convert.ToInt32(dr[DBFields.ProgramId]);
                    lobjProgramMerchantConfig.ProgramName = Convert.ToString(dr[DBFields.ProgramName]);
                    lobjProgramMerchantConfig.IsActive = Convert.ToBoolean(dr[DBFields.IsActive]);
                    lobjProgramMerchantConfig.LoyaltyTxnType = (LoyaltyTxnType)Convert.ToInt32(dr[DBFields.LoyaltyTxnType]);
                    lobjProgramMerchantConfig.MerchantId = Convert.ToString(dr[DBFields.MerchantId]);
                    lobjProgramMerchantConfig.MerchantName = Convert.ToString(dr[DBFields.MerchantName]);
                    lobjProgramMerchantConfig.MerchantPassword = Convert.ToString(dr[DBFields.MerchantPassword]);
                    lobjProgramMerchantConfig.SecreteKey = Convert.ToString(dr[DBFields.SecreteKey]);
                    lobjProgramMerchantConfig.CreatedBy = Convert.ToString(dr[DBFields.CreatedBy]);
                    lobjProgramMerchantConfig.UpdatedBy = Convert.ToString(dr[DBFields.UpdatedBy]);
                    lobjProgramMerchantConfig.CreatedOn = Convert.ToDateTime(dr[DBFields.CreatedOn]);
                    lobjProgramMerchantConfig.UpdatedOn = Convert.ToDateTime(dr[DBFields.UpdatedOn]);
                }
                return lobjProgramMerchantConfig;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("MapMerchantDetailsByProgramIdAndLoyaltyTxnType : " + ex.Message);
                return null;
            }
        }

        public static MemberDetails MapMemberDetails(DataSet pobjdsMemberDetails)
        {
            DataTable dtAllList = pobjdsMemberDetails.Tables[0];
            MemberDetails lobjMemberDetails = null;
            if (pobjdsMemberDetails != null && pobjdsMemberDetails.Tables.Count > 0 && pobjdsMemberDetails.Tables[0].Rows.Count > 0)
            {
                lobjMemberDetails = new MemberDetails();

                lobjMemberDetails.Id = Convert.ToInt32(dtAllList.Rows[0][DBFields.Id]);
                lobjMemberDetails.FirstName = Convert.ToString(dtAllList.Rows[0][DBFields.FirstName]);
                lobjMemberDetails.LastName = Convert.ToString(dtAllList.Rows[0][DBFields.LastName]);
                lobjMemberDetails.Email = Convert.ToString(dtAllList.Rows[0][DBFields.EmailId]);
                lobjMemberDetails.DOB = Convert.ToDateTime((dtAllList.Rows[0][DBFields.DOB] == DBNull.Value) ? DateTime.MinValue.ToUniversalTime() : Convert.ToDateTime(dtAllList.Rows[0][DBFields.DOB]));//Convert.ToDateTime(dtAllList.Rows[0][DBFields.DOB]);
                lobjMemberDetails.MobileNumber = Convert.ToString(dtAllList.Rows[0][DBFields.MobileNumber]);
                lobjMemberDetails.Nationality = Convert.ToString(dtAllList.Rows[0][DBFields.Nationality]);
                lobjMemberDetails.Gender = Convert.ToString(dtAllList.Rows[0][DBFields.Gender]);
                lobjMemberDetails.PassportNumber = Convert.ToString(dtAllList.Rows[0][DBFields.PassportNumber]);
                lobjMemberDetails.Address1 = Convert.ToString(dtAllList.Rows[0][DBFields.Address1]);
                lobjMemberDetails.Address2 = Convert.ToString(dtAllList.Rows[0][DBFields.Address2]);
                lobjMemberDetails.CreatedBy = Convert.ToString(dtAllList.Rows[0][DBFields.CreatedBy]);
                lobjMemberDetails.CreatedOn = Convert.ToDateTime((dtAllList.Rows[0][DBFields.CreatedOn] == DBNull.Value) ? DateTime.MinValue.ToUniversalTime() : Convert.ToDateTime(dtAllList.Rows[0][DBFields.CreatedOn])).ToUniversalTime();//Convert.ToDateTime(dtAllList.Rows[0][DBFields.CreatedOn]);
                lobjMemberDetails.UpdatedBy = Convert.ToString(dtAllList.Rows[0][DBFields.UpdatedBy]);
                lobjMemberDetails.UpdatedOn = Convert.ToDateTime((dtAllList.Rows[0][DBFields.UpdatedOn] == DBNull.Value) ? DateTime.MinValue.ToUniversalTime() : Convert.ToDateTime(dtAllList.Rows[0][DBFields.UpdatedOn])).ToUniversalTime();//Convert.ToDateTime(dtAllList.Rows[0][DBFields.UpdatedOn]);
                lobjMemberDetails.SourceChannel = Convert.ToString(dtAllList.Rows[0][DBFields.SourceChannel]);
                lobjMemberDetails.ProgramId = Convert.ToInt32(dtAllList.Rows[0][DBFields.ProgramId]);
                lobjMemberDetails.EnrollmentDate = Convert.ToDateTime((dtAllList.Rows[0][DBFields.EnrollmentDate] == DBNull.Value) ? DateTime.MinValue.ToUniversalTime() : Convert.ToDateTime(dtAllList.Rows[0][DBFields.EnrollmentDate]));//Convert.ToDateTime(dtAllList.Rows[0][DBFields.EnrollmentDate]);
                lobjMemberDetails.IsMerchant = Convert.ToBoolean(dtAllList.Rows[0][DBFields.IsMerchant]);
                lobjMemberDetails.MothersMaidenName = Convert.ToString(dtAllList.Rows[0][DBFields.MothersMaidenName]);
                lobjMemberDetails.NationalId = Convert.ToString(dtAllList.Rows[0][DBFields.NationalId]);
                lobjMemberDetails.Password = Convert.ToString(dtAllList.Rows[0][DBFields.Password]);

                lobjMemberDetails.AdditionalDetails = Convert.ToString(dtAllList.Rows[0][DBFields.AdditionalDetails]);
                lobjMemberDetails.AdditionalDetails1 = Convert.ToString(dtAllList.Rows[0][DBFields.AdditionalDetails1]);
                lobjMemberDetails.CustomerSegment = Convert.ToString(dtAllList.Rows[0][DBFields.CustomerSegment]);
                lobjMemberDetails.CustomerType = Convert.ToString(dtAllList.Rows[0][DBFields.CustomerType]);
                lobjMemberDetails.PreferredLanguage = Convert.ToString(dtAllList.Rows[0][DBFields.PreferredLanguage]);

                lobjMemberDetails.MemberRelationsList.AddRange(GetMemberRelation(dtAllList, lobjMemberDetails));
            }
            return lobjMemberDetails;
        }

        private static List<MemberRelation> GetMemberRelation(DataTable dtAllList, MemberDetails pobjMemberDetails)
        {
            DataRow[] drMemberRelationList = dtAllList.Select("Member_Id='" + pobjMemberDetails.Id + "' and Relation_Reference <>" + "'NA'");

            List<MemberRelation> lobjMemberRelationList = new List<MemberRelation>();
            if (drMemberRelationList != null && drMemberRelationList.Length > 0)
            {
                for (int i = 0; i < drMemberRelationList.Length; i++)
                {
                    MemberRelation lobjExistMemberRelation = null;
                    lobjExistMemberRelation = lobjMemberRelationList.Find(lobj => lobj.RelationReference.Equals(Convert.ToString(drMemberRelationList[i][DBFields.RelationReference])));
                    if (lobjExistMemberRelation == null)
                    {
                        MemberRelation lobjMemberRelation = new MemberRelation();
                        lobjMemberRelation.Id = Convert.ToInt32((drMemberRelationList[i][DBFields.RID] == DBNull.Value) ? 0 : Convert.ToInt32(drMemberRelationList[i][DBFields.RID]));
                        lobjMemberRelation.RelationType = (RelationType)Convert.ToInt32((drMemberRelationList[i][DBFields.RelationType] == DBNull.Value) ? 0 : Convert.ToInt32(drMemberRelationList[i][DBFields.RelationType]));
                        lobjMemberRelation.RelationReference = Convert.ToString((drMemberRelationList[i][DBFields.RelationReference] == DBNull.Value) ? string.Empty : Convert.ToString(drMemberRelationList[i][DBFields.RelationReference]));
                        lobjMemberRelation.WebPassword = Convert.ToString((drMemberRelationList[i][DBFields.WebPassword] == DBNull.Value) ? string.Empty : Convert.ToString(drMemberRelationList[i][DBFields.WebPassword]));
                        lobjMemberRelation.MPIN = Convert.ToString((drMemberRelationList[i][DBFields.MPIN] == DBNull.Value) ? string.Empty : Convert.ToString(drMemberRelationList[i][DBFields.MPIN]));
                        lobjMemberRelation.POSPIN = Convert.ToString((drMemberRelationList[i][DBFields.POSPIN] == DBNull.Value) ? string.Empty : Convert.ToString(drMemberRelationList[i][DBFields.POSPIN]));
                        lobjMemberRelation.LastLoggedIn = Convert.ToDateTime((drMemberRelationList[i][DBFields.lastloggedIn] == DBNull.Value) ? DateTime.MinValue.ToUniversalTime() : Convert.ToDateTime(drMemberRelationList[i][DBFields.lastloggedIn]));
                        lobjMemberRelation.LoginAttempt = Convert.ToInt32((drMemberRelationList[i][DBFields.LoginAttempt] == DBNull.Value) ? 0 : Convert.ToInt32(drMemberRelationList[i][DBFields.LoginAttempt]));
                        lobjMemberRelation.IsAccountActivated = Convert.ToBoolean((drMemberRelationList[i][DBFields.IsAccountActivated] != DBNull.Value) && Convert.ToBoolean(drMemberRelationList[i][DBFields.IsAccountActivated]));
                        lobjMemberRelation.Status = (Status)Convert.ToInt32((drMemberRelationList[i][DBFields.Status] == DBNull.Value) ? 0 : Convert.ToInt32(drMemberRelationList[i][DBFields.Status]));
                        lobjMemberRelation.IsAccrualAllowed = Convert.ToBoolean((drMemberRelationList[i][DBFields.IsAccrualAllowed] != DBNull.Value) && Convert.ToBoolean(drMemberRelationList[i][DBFields.IsAccrualAllowed]));
                        lobjMemberRelation.ForceChangePassword = Convert.ToBoolean((drMemberRelationList[i][DBFields.ForceChangePassword] == DBNull.Value) ? string.Empty : Convert.ToString(drMemberRelationList[i][DBFields.ForceChangePassword]));
                        lobjMemberRelation.CreatedBy = Convert.ToString((drMemberRelationList[i][DBFields.RelationCreatedBy] == DBNull.Value) ? string.Empty : Convert.ToString(drMemberRelationList[i][DBFields.RelationCreatedBy]));
                        lobjMemberRelation.CreatedOn = (dtAllList.Rows[i][DBFields.RelationCreatedOn] == DBNull.Value) ? DateTime.MinValue.ToUniversalTime() : Convert.ToDateTime(dtAllList.Rows[i][DBFields.RelationCreatedOn]).ToUniversalTime();
                        lobjMemberRelation.ActivatedDate = (dtAllList.Rows[i][DBFields.ActivatedDate] == DBNull.Value) ? DateTime.MinValue.ToUniversalTime() : Convert.ToDateTime(dtAllList.Rows[i][DBFields.ActivatedDate]);
                        lobjMemberRelation.PrimaryReference = Convert.ToString((drMemberRelationList[i][DBFields.PrimaryReference] == DBNull.Value) ? string.Empty : Convert.ToString(drMemberRelationList[i][DBFields.PrimaryReference]));
                        lobjMemberRelation.ParentStatus = (ParentStatus)Convert.ToInt32((drMemberRelationList[i][DBFields.ParentStatus] == DBNull.Value) ? 0 : Convert.ToInt32(drMemberRelationList[i][DBFields.ParentStatus]));
                        lobjMemberRelation.MemberSubRelationList.AddRange(GetMemberSubRelation(dtAllList, pobjMemberDetails, lobjMemberRelation));
                        lobjMemberRelationList.Add(lobjMemberRelation);
                    }
                }
            }
            return lobjMemberRelationList;
        }

        private static List<MemberSubRelation> GetMemberSubRelation(DataTable dtAllList, MemberDetails pobjMemberDetails, MemberRelation pobjMemberRelation)
        {
            DataRow[] drMemberSubRelation = dtAllList.Select("SRelationReference='" + pobjMemberRelation.RelationReference + "' ");
            List<MemberSubRelation> lobjMemberSubRelationList = new List<MemberSubRelation>();
            if (drMemberSubRelation != null && drMemberSubRelation.Length > 0)
            {

                for (int i = 0; i < drMemberSubRelation.Length; i++)
                {
                    MemberSubRelation lobjMemberSubRelation = new MemberSubRelation();
                    lobjMemberSubRelation.Id = Convert.ToInt32(drMemberSubRelation[i][DBFields.SID] == DBNull.Value ? 0 : Convert.ToInt32(drMemberSubRelation[i][DBFields.SID]));
                    lobjMemberSubRelation.ProductCode = Convert.ToString(drMemberSubRelation[i][DBFields.ProductCode] == DBNull.Value ? string.Empty : Convert.ToString(drMemberSubRelation[i][DBFields.ProductCode]));
                    lobjMemberSubRelation.Type = Convert.ToString(drMemberSubRelation[i][DBFields.Type] == DBNull.Value ? string.Empty : Convert.ToString(drMemberSubRelation[i][DBFields.Type]));
                    lobjMemberSubRelation.LastSixDigits = Convert.ToString(drMemberSubRelation[i][DBFields.LastSixDigits] == DBNull.Value ? string.Empty : Convert.ToString(drMemberSubRelation[i][DBFields.LastSixDigits]));
                    lobjMemberSubRelation.ExpiryDate = Convert.ToDateTime(drMemberSubRelation[i][DBFields.ExpiryDate] == DBNull.Value ? DateTime.MinValue.ToUniversalTime() : Convert.ToDateTime(drMemberSubRelation[i][DBFields.ExpiryDate]).ToUniversalTime());
                    lobjMemberSubRelation.DealCode = Convert.ToString(drMemberSubRelation[i][DBFields.DealCode] == DBNull.Value ? string.Empty : Convert.ToString(drMemberSubRelation[i][DBFields.DealCode]));
                    lobjMemberSubRelation.CreationDate = Convert.ToDateTime(drMemberSubRelation[i][DBFields.CreationDate] == DBNull.Value ? DateTime.MinValue.ToUniversalTime() : Convert.ToDateTime(drMemberSubRelation[i][DBFields.CreationDate]).ToUniversalTime());
                    lobjMemberSubRelation.Status = Convert.ToString(drMemberSubRelation[i][DBFields.SRStatus] == DBNull.Value ? string.Empty : Convert.ToString(drMemberSubRelation[i][DBFields.SRStatus]));
                    lobjMemberSubRelation.SubRelationIdentifier = Convert.ToString(drMemberSubRelation[i][DBFields.SubRelationIdentifier] == DBNull.Value ? string.Empty : Convert.ToString(drMemberSubRelation[i][DBFields.SubRelationIdentifier]));
                    lobjMemberSubRelationList.Add(lobjMemberSubRelation);
                }
            }
            return lobjMemberSubRelationList;
        }
    }
}
