﻿using Core.Platform.Member.Entites;
using Core.Platform.PointGateway.Service.Helper;
using Core.Platform.ProgramInterface.ClientHelper;
using Core.Platform.ProgramMaster.Entities;
using Core.Platform.TransactionManagement.BusinessFacade;
using Core.Platform.Transactions.Entites;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.CommunicationEngine.Entity;
using Framework.EnterpriseLibrary.CommunicationEngine.Helper;
using Framework.EnterpriseLibrary.UniqueNumberGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
//using Core.Platform.Report.Entities;
//using Core.Platform.Report.DataAccess.Mapper;
//using Core.Platform.Report.DataAccess.Wrapper;

namespace Core.Platform.PointTransferFacade
{
    public class TransferFacade
    {
        string ProgramName = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["ProgramName"]);
        string lstrMerchantId = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["MerchantId"]);
        string lstrMerchantUserName = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["MerchantUserName"]);
        string lstrMerchantPassword = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["MerchantPassword"]);
        string SendEmailNotification = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SendEmailNotification"]);


        public string TransferPoints(MemberDetails pobjSenderMemberDetails, MemberDetails pobjRecipientMemberDetails, int pintTransferPoints, string pstrProgramCurrency)
        {
            int lintExpiryMonth = 0;
            int lintAvailablePoints = 0;
            string lstrResult = string.Empty;
            try
            {
                InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
                ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
                lobjProgramDefinition = lobjInterfaceHelper.GetProgramDefinition(ProgramName);

                List<ProgramCurrencyDefinition> lobjProgramCurrencyDefinitionlist = new List<ProgramCurrencyDefinition>();
                lobjProgramCurrencyDefinitionlist = lobjInterfaceHelper.GetProgramCurrencyDefinition(lobjProgramDefinition.ProgramId);

                lintExpiryMonth = lobjProgramCurrencyDefinitionlist.Find(lobj => lobj.Currency.Equals(pstrProgramCurrency)).ExpirySchedule;
                string lstrTransactionId = string.Empty;
                lstrTransactionId = GenerateTransactionId();
                if (lstrTransactionId != string.Empty)
                {
                    //check points availability for sender
                    lintAvailablePoints = CheckAvailbility(pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), pstrProgramCurrency);

                    //if points availability successfull
                    if (lintAvailablePoints > 0 && pintTransferPoints <= lintAvailablePoints)
                    {
                        //debit points from sender
                        if (DebitPointsFromSender(pobjSenderMemberDetails, pobjRecipientMemberDetails, pstrProgramCurrency, lstrTransactionId, pintTransferPoints))
                        {
                            //credit points to receipient account
                            if (CreditPointsToRecipient(pobjSenderMemberDetails, pobjRecipientMemberDetails, pstrProgramCurrency, lstrTransactionId, lintExpiryMonth, pintTransferPoints))
                            {
                                lstrResult = "Success";

                                //Send Email/SMS to Sender
                                SendEmailSMSForTransferToSender(pobjSenderMemberDetails, pobjRecipientMemberDetails, pintTransferPoints, pstrProgramCurrency);

                                //Send Email/SMS to Receiver
                                SendEmailSMSForTransferToReceiver(pobjSenderMemberDetails, pobjRecipientMemberDetails, pintTransferPoints, pstrProgramCurrency);

                                if (SendEmailNotification.Equals("Y"))
                                {
                                    //Send Email Notification
                                    SendPointsTransferEmail(pobjSenderMemberDetails, pobjRecipientMemberDetails, pintTransferPoints);
                                }
                            }
                            else
                            {
                                //Rollback Transaction in case of Credit failure
                                RollBackTransaction(lstrTransactionId, pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), pstrProgramCurrency);
                                LoggingAdapter.WriteLog("Points Were Not Credited For Point Transfer Between " + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " And " + pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + "At " + DateTime.Now);
                            }
                        }
                        else
                        {
                            LoggingAdapter.WriteLog("Points Were Not Debited For Point Transfer Between " + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " And " + pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + "At " + DateTime.Now);
                        }
                    }
                    else
                    {
                        lstrResult = "InSufficientPoints";
                        LoggingAdapter.WriteLog("Insufficient Points For Point Transfer Between " + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " And " + pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + "At " + DateTime.Now + " Available Points " + lintAvailablePoints);
                    }
                }
                else
                {
                    LoggingAdapter.WriteLog("Transaction Id Is Empty For Point Transfer Between " + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " And " + pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + "At " + DateTime.Now);
                }
            }
            catch (Exception Ex)
            {
                LoggingAdapter.WriteLog("Exception Occured For Point Transfer Between " + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " And " + pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + "At " + DateTime.Now);
                LoggingAdapter.WriteLog("Exception " + Ex.InnerException + Environment.NewLine + "Message " + Ex.Message + Environment.NewLine + "StackTrace " + Ex.StackTrace);
            }
            return lstrResult;
        }

        /// <summary>
        /// CheckAvailbility for sender
        /// </summary>
        /// <param name="pstrRelationReference"></param>
        /// <param name="pintRelationType"></param>
        /// <param name="pstrCurrency"></param>
        /// <returns></returns>
        private int CheckAvailbility(string pstrRelationReference, int pintRelationType, string pstrCurrency)
        {
            try
            {
                PGHelper lobjPGHelper = new PGHelper();
                return lobjPGHelper.CheckPointsAvailability(pstrRelationReference, pintRelationType, pstrCurrency, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CheckAvailbility -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return 0;
            }
        }

        /// <summary>
        /// RollBack Transaction
        /// </summary>
        /// <param name="pstrRelationReference"></param>
        /// <param name="pintRelationType"></param>
        /// <param name="pstrCurrency"></param>
        /// <returns></returns>
        private bool RollBackTransaction(string pstrExternalReference, string pstrRelationReference, int pintRelationType, string pstrCurrency)
        {
            try
            {
                PGHelper lobjPGHelper = new PGHelper();
                return lobjPGHelper.PGRollBackTransaction(pstrExternalReference, pstrRelationReference, lstrMerchantId, lstrMerchantUserName, lstrMerchantPassword);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("PGRollBackTransaction -" + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Generate Transaction Id
        /// </summary>
        /// <returns></returns>
        private string GenerateTransactionId()
        {
            string lstrTransactionId = string.Empty;
            lstrTransactionId = GenerateUniqueNumber.Get16DigitNumberDateTime();
            return lstrTransactionId;
        }

        /// <summary>
        /// Debits Points from Sender
        /// </summary>
        /// <param name="pobjSenderMemberDetails"></param>
        /// <param name="pobjRecipientMemberDetails"></param>
        /// <param name="pstrProgramCurrency"></param>
        /// <param name="pstrTransactionId"></param>
        /// <returns></returns>
        private bool DebitPointsFromSender(MemberDetails pobjSenderMemberDetails, MemberDetails pobjRecipientMemberDetails, string pstrProgramCurrency, string pstrTransactionId, int pintPoints)
        {
            bool lblnResult = false;
            try
            {
                TransactionDetails lobjTransactionDetails = new TransactionDetails();
                TransactionBusinessFacade lobjTransactionBusinessFacade = new TransactionBusinessFacade();
                lobjTransactionDetails.TransactionType = TransactionType.Debit;
                lobjTransactionDetails.LoyaltyTxnType = LoyaltyTxnType.Transfer;
                lobjTransactionDetails.Points = pintPoints;
                lobjTransactionDetails.RelationReference = pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjTransactionDetails.RelationType = RelationType.LBMS;
                lobjTransactionDetails.ProgramId = pobjSenderMemberDetails.ProgramId;
                lobjTransactionDetails.Narration = "Transfer Points(Debit)";
                lobjTransactionDetails.MerchantName = "Transfer Points";
                lobjTransactionDetails.ExternalReference = pstrTransactionId;
                lobjTransactionDetails.ProcessingDate = DateTime.Now;
                lobjTransactionDetails.TransactionDate = DateTime.Now;
                lobjTransactionDetails.ExpiryDate = DateTime.Now;
                lobjTransactionDetails.TransactionCurrency = pstrProgramCurrency;
                lobjTransactionDetails.AdditionalDetails1 = "Points Transfer Between " + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " And " + pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjTransactionDetails.TransactionDetailBreakage.IsBillable = false;
                lobjTransactionDetails.TransactionDetailBreakage.Narration = "Transfer Points(Debit)";
                lblnResult = lobjTransactionBusinessFacade.InsertTransactionDetails(lobjTransactionDetails);
            }
            catch (Exception Ex)
            {
                lblnResult = false;
                LoggingAdapter.WriteLog("(DebitPointsFromSender)Points Were Not Debited For Point Transfer Between " + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " And " + pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + "At " + DateTime.Now);
                LoggingAdapter.WriteLog("DebitPointsFromSender Exception " + Ex.InnerException + Environment.NewLine + "Message " + Ex.Message + Environment.NewLine + "StackTrace " + Ex.StackTrace);
            }

            return lblnResult;
        }

        /// <summary>
        /// Credit Points to Recipient
        /// </summary>
        /// <param name="pobjSenderMemberDetails"></param>
        /// <param name="pobjRecipientMemberDetails"></param>
        /// <param name="pstrProgramCurrency"></param>
        /// <param name="pstrTransactionId"></param>
        /// <param name="pintExpiryMonth"></param>
        /// <returns></returns>
        private bool CreditPointsToRecipient(MemberDetails pobjSenderMemberDetails, MemberDetails pobjRecipientMemberDetails, string pstrProgramCurrency, string pstrTransactionId, int pintExpiryMonth, int pintPoints)
        {
            bool lblnResult = false;

            try
            {
                TransactionDetails lobjTransactionDetails = new TransactionDetails();
                TransactionBusinessFacade lobjTransactionBusinessFacade = new TransactionBusinessFacade();
                lobjTransactionDetails.TransactionType = TransactionType.Credit;
                lobjTransactionDetails.LoyaltyTxnType = LoyaltyTxnType.Transfer;
                lobjTransactionDetails.Points = pintPoints;
                lobjTransactionDetails.RelationReference = pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjTransactionDetails.RelationType = RelationType.LBMS;
                lobjTransactionDetails.ProgramId = pobjRecipientMemberDetails.ProgramId;
                lobjTransactionDetails.Narration = "Transfer Points(Credit)";
                lobjTransactionDetails.MerchantName = "Transfer Points";
                lobjTransactionDetails.ExternalReference = pstrTransactionId;
                lobjTransactionDetails.ProcessingDate = DateTime.Now;
                lobjTransactionDetails.TransactionDate = DateTime.Now;
                lobjTransactionDetails.ExpiryDate = DateTime.Now.AddMonths(pintExpiryMonth);
                lobjTransactionDetails.TransactionCurrency = pstrProgramCurrency;
                lobjTransactionDetails.AdditionalDetails1 = "Points Transfer Between " + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " And " + pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference;
                lobjTransactionDetails.IsDomestic = true;
                lobjTransactionDetails.TransactionDetailBreakage.IsBillable = false;
                lobjTransactionDetails.TransactionDetailBreakage.Narration = "Transfer Points(Credit)";
                lblnResult = lobjTransactionBusinessFacade.InsertTransactionDetails(lobjTransactionDetails);
                if (lblnResult)
                {
                    UpdateTransactionDetails(lobjTransactionDetails);
                }
            }
            catch (Exception Ex)
            {
                lblnResult = false;
                LoggingAdapter.WriteLog("(CreditPointsToRecipient)Points Were Not Credited For Point Transfer Between " + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + " And " + pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + "At " + DateTime.Now);
                LoggingAdapter.WriteLog("CreditPointsToRecipient Exception " + Ex.InnerException + Environment.NewLine + "Message " + Ex.Message + Environment.NewLine + "StackTrace " + Ex.StackTrace);
            }

            return lblnResult;
        }

        /// <summary>
        /// Update TransactionDetails - CSData
        /// </summary>
        /// <param name="pobjTransactionDetails"></param>
        /// <returns></returns>
        private bool UpdateTransactionDetails(TransactionDetails pobjTransactionDetails)
        {
            bool status = false;
            //SecureData Contains(Secretkey|IsSuccessFul|JsonString); Format to be followed
            string lstSecureData = pobjTransactionDetails.Id + "|" + pobjTransactionDetails.Points;
            MD5 md5HashAlgo = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(lstSecureData);
            byte[] hashBytes = md5HashAlgo.ComputeHash(inputBytes);
            StringBuilder secureData = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                secureData.Append(hashBytes[i].ToString("X2"));
            }
            string lstSecureHash = Convert.ToString(secureData);
            TransactionBusinessFacade lobjTxnFacade = new TransactionBusinessFacade();
            pobjTransactionDetails.CSData = lstSecureHash;
            status = lobjTxnFacade.UpdateTransactionDetails(pobjTransactionDetails);
            return status;
        }

        /// <summary>
        /// Send Email SMS For Transfer To Sender
        /// </summary>
        /// <param name="pobjSenderMemberDetails"></param>
        /// <param name="pobjRecipientMemberDetails"></param>
        /// <param name="pintTransferPoints"></param>
        private void SendEmailSMSForTransferToSender(MemberDetails pobjSenderMemberDetails, MemberDetails pobjRecipientMemberDetails, int pintTransferPoints, string pstrProgramCurrency)
        {
            int lintSenderAvailablePoints = 0;
            LoggingAdapter.WriteLog("SendEmailSMSForTransferToSender CheckAvailbility started :" + pobjSenderMemberDetails.MemberRelationsList.Find(obj => obj.RelationType.Equals(RelationType.LBMS)).RelationReference + pstrProgramCurrency);
            lintSenderAvailablePoints = CheckAvailbility(pobjSenderMemberDetails.MemberRelationsList.Find(obj => obj.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), pstrProgramCurrency);

            LoggingAdapter.WriteLog("SendEmailSMSForTransferToSender CheckAvailbility :" + lintSenderAvailablePoints.ToString());

            try
            {
                if (pobjSenderMemberDetails.Email != string.Empty)
                {
                    if (lintSenderAvailablePoints > 0)
                    {
                        // Communication Engine Call FOr Email.
                        EmailDetails lobjEmailDetail = new EmailDetails();
                        List<string> lstEmailparameter = new List<string>();
                        List<string> lstAttachment = new List<string>();
                        CEHelper lobjcehelper = new CEHelper();
                        lstEmailparameter.Add(pobjSenderMemberDetails.LastName);
                        lstEmailparameter.Add(Convert.ToString(pintTransferPoints));
                        lstEmailparameter.Add(pstrProgramCurrency);
                        lstEmailparameter.Add(pobjRecipientMemberDetails.LastName);
                        lstEmailparameter.Add(lintSenderAvailablePoints.ToString());
                        lobjEmailDetail.TemplateCode = "DebitTransferPoints";
                        lobjEmailDetail.ListParameter = lstEmailparameter;
                        lobjEmailDetail.MemberId = Convert.ToString(pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
                        lobjEmailDetail.ProgramId = Convert.ToString(pobjSenderMemberDetails.ProgramId);
                        lobjEmailDetail.AttachmentList = lstAttachment;
                        lobjEmailDetail.To = pobjSenderMemberDetails.Email;
                        lobjcehelper.InsertEmailDetails(lobjEmailDetail);

                    }
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Points Transfer Email To Sender: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace + Environment.NewLine + "Message: " + ex.Message);
            }

            try
            {
                if (pobjSenderMemberDetails.MobileNumber != string.Empty)
                {
                    if (lintSenderAvailablePoints > 0)
                    {
                        //Communication Engine Call For SMS. 
                        CEHelper lobjcehelper = new CEHelper();
                        SmsDetails lobjSmsDetail = new SmsDetails();
                        List<string> lstSmsparameter = new List<string>();
                        lobjSmsDetail.TemplateCode = "DebitTransferPoints";
                        lstSmsparameter.Add(pobjSenderMemberDetails.LastName);
                        lstSmsparameter.Add(Convert.ToString(pintTransferPoints));
                        lstSmsparameter.Add(pstrProgramCurrency);
                        lstSmsparameter.Add(pobjRecipientMemberDetails.LastName);
                        lstSmsparameter.Add(lintSenderAvailablePoints.ToString());
                        lobjSmsDetail.ListParameter = lstSmsparameter;
                        lobjSmsDetail.ProgramId = Convert.ToString(pobjSenderMemberDetails.ProgramId);
                        lobjSmsDetail.MemberId = Convert.ToString(pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
                        lobjSmsDetail.ReceiverMobile = Convert.ToString(pobjSenderMemberDetails.MobileNumber);
                        lobjcehelper.InsertSmsDetails(lobjSmsDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Points Transfer SMS To Sender: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace + Environment.NewLine + "Message: " + ex.Message);
            }
        }

        /// <summary>
        /// Send Email SMS For Transfer To Receiver
        /// </summary>
        /// <param name="pobjSenderMemberDetails"></param>
        /// <param name="pobjRecipientMemberDetails"></param>
        /// <param name="pintTransferPoints"></param>
        private void SendEmailSMSForTransferToReceiver(MemberDetails pobjSenderMemberDetails, MemberDetails pobjRecipientMemberDetails, int pintTransferPoints, string pstrProgramCurrency)
        {
            int lintRecipientAvailablePoints = 0;
            LoggingAdapter.WriteLog("SendEmailSMSForTransferToReceiver CheckAvailbility started:");
            lintRecipientAvailablePoints = CheckAvailbility(pobjRecipientMemberDetails.MemberRelationsList.Find(obj => obj.RelationType.Equals(RelationType.LBMS)).RelationReference, Convert.ToInt32(RelationType.LBMS), pstrProgramCurrency);

            LoggingAdapter.WriteLog("SendEmailSMSForTransferToReceiver CheckAvailbility:" + lintRecipientAvailablePoints.ToString());

            try
            {
                if (pobjRecipientMemberDetails.Email != string.Empty)
                {
                    if (lintRecipientAvailablePoints > 0)
                    {
                        // Communication Engine Call FOr Email.
                        EmailDetails lobjEmailDetail = new EmailDetails();
                        List<string> lstEmailparameter = new List<string>();
                        List<string> lstAttachment = new List<string>();
                        CEHelper lobjcehelper = new CEHelper();
                        lstEmailparameter.Add(pobjRecipientMemberDetails.LastName);
                        lstEmailparameter.Add(Convert.ToString(pintTransferPoints));
                        lstEmailparameter.Add(pstrProgramCurrency);
                        lstEmailparameter.Add(pobjSenderMemberDetails.LastName);
                                                
                        lobjEmailDetail.TemplateCode = "CreditTransferPoints";
                        lobjEmailDetail.ListParameter = lstEmailparameter;
                        lobjEmailDetail.MemberId = Convert.ToString(pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
                        lobjEmailDetail.ProgramId = Convert.ToString(pobjRecipientMemberDetails.ProgramId);
                        lobjEmailDetail.AttachmentList = lstAttachment;
                        lobjEmailDetail.To = pobjRecipientMemberDetails.Email;
                        lobjcehelper.InsertEmailDetails(lobjEmailDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Points Transfer Email To Receiver: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace + Environment.NewLine + "Message: " + ex.Message);
            }

            try
            {
                if (pobjRecipientMemberDetails.MobileNumber != string.Empty)
                {
                    if (lintRecipientAvailablePoints > 0)
                    {
                        //Communication Engine Call For SMS. 
                        CEHelper lobjcehelper = new CEHelper();
                        SmsDetails lobjSmsDetail = new SmsDetails();
                        List<string> lstSmsparameter = new List<string>();
                        lobjSmsDetail.TemplateCode = "CreditTransferPoints";
                        lstSmsparameter.Add(pobjRecipientMemberDetails.LastName);
                        lstSmsparameter.Add(Convert.ToString(pintTransferPoints));
                        lstSmsparameter.Add(pstrProgramCurrency);
                        lstSmsparameter.Add(pobjSenderMemberDetails.LastName);
                        lobjSmsDetail.ProgramId = Convert.ToString(pobjRecipientMemberDetails.ProgramId);
                        lobjSmsDetail.MemberId = Convert.ToString(pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference);
                        lobjSmsDetail.ReceiverMobile = Convert.ToString(pobjRecipientMemberDetails.MobileNumber);
                        lobjcehelper.InsertSmsDetails(lobjSmsDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Points Transfer SMS To Receiver: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace + Environment.NewLine + "Message: " + ex.Message);
            }
        }

        /// <summary>
        /// Points Transfer Email Notification
        /// </summary>
        /// <param name="pobjSenderMemberDetails"></param>
        /// <param name="pobjRecipientMemberDetails"></param>
        /// <param name="pintTransferPoints"></param>
        private void SendPointsTransferEmail(MemberDetails pobjSenderMemberDetails, MemberDetails pobjRecipientMemberDetails, int pintTransferPoints)
        {
            try
            {
                string mstrEmail = "<p> Dear Admin,</p><p style=margin-left: 40px> Please Find Following Points Transfer Description $$Template_Details$$ </p>";
                string FileList = @"<Table border=1 ><Tr><Th>From Account</Th><Th>To Account</Th><Th>Points</Th><Th>Date</Th></Tr>";
                FileList += @"<Tr><Td>" + pobjSenderMemberDetails.LastName + "(" + pobjSenderMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + ")" + "</Td><Td>" + pobjRecipientMemberDetails.LastName + "(" + pobjRecipientMemberDetails.MemberRelationsList.Find(lobj => lobj.RelationType.Equals(RelationType.LBMS)).RelationReference + ")" + "</Td><Td><center>" + pintTransferPoints + "</center></Td><Td><center>" + System.DateTime.Today.ToString("dd/MM/yyyy") + "</center></Td></Tr>";
                List<string> pstrEmailparameter = new List<string>();
                FileList += "</Table>";
                mstrEmail = mstrEmail.Replace("$$Template_Details$$", FileList);
                pstrEmailparameter.Add(mstrEmail);
                ProgramDefinition lobjProgramDefinition = new ProgramDefinition();
                InterfaceHelper lobjInterfaceHelper = new InterfaceHelper();
                lobjProgramDefinition = lobjInterfaceHelper.GetProgramDefinition(ProgramName);
                EmailDetails lobjEmailDetail = new EmailDetails();
                List<string> lstAttachment = new List<string>();
                CEHelper lobjcehelper = new CEHelper();
                lobjEmailDetail.TemplateCode = "PointsTransfer";
                lobjEmailDetail.ListParameter = pstrEmailparameter;
                lobjEmailDetail.ProgramId = Convert.ToString(lobjProgramDefinition.ProgramId);
                lobjEmailDetail.AttachmentList = lstAttachment;
                lobjcehelper.InsertEmailDetails(lobjEmailDetail);
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Points Transfer Email Exception: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace + Environment.NewLine + "Message: " + ex.Message);
            }
        }

        //public List<TransferPoints> GetPointTransferSummary(SearchReportInfo pobjSearchReportInfo)
        //{
        //    List<TransferPoints> lstlobjTransferPoints = new List<TransferPoints>();
        //    try
        //    {
        //        lstlobjTransferPoints = DataMapper.MapTransferPointsReportByDate(SPWrapper.GetTransferPointsReportByDate(pobjSearchReportInfo), pobjSearchReportInfo); ;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingAdapter.WriteLog("GetPointTransferSummary" + ex.Message);
        //    }
        //    return lstlobjTransferPoints;
        //}
    }
}
