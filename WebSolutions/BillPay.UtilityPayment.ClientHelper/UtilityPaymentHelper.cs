﻿using BillPay.UtilityPayment.BussinessFacade;
using BillPay.UtilityPayment.Entities;
using Framework.EnterpriseLibrary.Adapters;
using Framework.EnterpriseLibrary.Common.SerializationHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using IBM.WMQ;

namespace BillPay.UtilityPayment.ClientHelper
{
    public class UtilityPaymentHelper
    {
        UtilityPaymentFacade lobjUtilityPaymentFacade = new UtilityPaymentFacade();
        public UtilityInquiryResponse UtilityInquiry(UtilityInquiryRequest pobjUtilityInquiryRequest)
        {
            LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityInquiryRequest.Type) + " Start Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum + " Date and Time = " + DateTime.Now, Convert.ToString(pobjUtilityInquiryRequest.Type));

            string lstrUIResponse = string.Empty;
            string lstrResponse = string.Empty;
            string lstrUIRequest = string.Empty;
            UtilityInquiryResponse lobjUtilityInquiryResponse = null;
            UtilityHeader lobjUtilityHeader = null;

            try
            {
                LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityInquiryRequest.Type) + " Request = " + JSONSerialization.Serialize(pobjUtilityInquiryRequest), Convert.ToString(pobjUtilityInquiryRequest.Type));
                
                if (pobjUtilityInquiryRequest != null)
                {
                    UtilityDetails lobjUtilityDetails = new UtilityDetails();
                    lobjUtilityDetails.ReferenceNumber = pobjUtilityInquiryRequest.TimeStamp;
                    lobjUtilityDetails.UtilityCompanyCode = pobjUtilityInquiryRequest.UtilityCompanyCode;
                    lobjUtilityDetails.UtilityAccount = pobjUtilityInquiryRequest.UtilityAccount;
                    lobjUtilityDetails.UtilityAccountType = pobjUtilityInquiryRequest.UtilityAccountType;
                    lobjUtilityDetails.CustomerId = pobjUtilityInquiryRequest.CustomerId;
                    lobjUtilityDetails.Amount = Convert.ToDouble(pobjUtilityInquiryRequest.Amount);
                    lobjUtilityDetails.BankAccount = pobjUtilityInquiryRequest.BankAccount;
                    lobjUtilityDetails.TimeStamp = pobjUtilityInquiryRequest.TimeStamp;
                    lobjUtilityDetails.RequestedDate = DateTime.Now;
                    lobjUtilityDetails.Type = pobjUtilityInquiryRequest.Type;
                    lobjUtilityDetails.Status = UtilityStatus.Pending;
                    // lobjUtilityDetails.CreatedBy = lobjMemberDetails.Email;

                    LoggingAdapter.WriteLog("Time Stamp = " + pobjUtilityInquiryRequest.TimeStamp + " | Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum,Convert.ToString(pobjUtilityInquiryRequest.Type));
                    
                    lstrUIRequest = File.ReadAllText(ConfigurationSettings.AppSettings["UPPath"].ToString() + "UtilityInquiryRequest.txt");

                    lstrUIRequest = string.Format(lstrUIRequest, ConfigurationSettings.AppSettings["MsgFormatUI"].ToString(), ConfigurationSettings.AppSettings["MsgVersion"].ToString()
                                                   , ConfigurationSettings.AppSettings["RequestorId"].ToString(), ConfigurationSettings.AppSettings["RequestorChannelId"].ToString()
                                                   , ConfigurationSettings.AppSettings["RequestorUserId"].ToString(), ConfigurationSettings.AppSettings["RequestorLanguage"].ToString()
                                                  , ConfigurationSettings.AppSettings["RequestorSecurityInfo"].ToString(), ConfigurationSettings.AppSettings["EaiReference"].ToString()
                                                  , ConfigurationSettings.AppSettings["ReturnCode"].ToString()
                                                  , pobjUtilityInquiryRequest.ReferenceNum, pobjUtilityInquiryRequest.UtilityCompanyCode, pobjUtilityInquiryRequest.UtilityAccount
                                                  , pobjUtilityInquiryRequest.UtilityAccountPin
                                                  , pobjUtilityInquiryRequest.UtilityAccountType, pobjUtilityInquiryRequest.TimeStamp);
                    
                    lobjUtilityDetails.RequestString = lstrUIRequest;
                    ////db ENTRIES
                    lobjUtilityDetails.Id = lobjUtilityPaymentFacade.InsertUtilityDetails(lobjUtilityDetails);
                    LoggingAdapter.WriteLog("Insert Utility Request = " + lobjUtilityDetails.Id + " | Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum, Convert.ToString(pobjUtilityInquiryRequest.Type));
                    LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityInquiryRequest.Type) + " Request :- " + lstrUIRequest + " | Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum, Convert.ToString(pobjUtilityInquiryRequest.Type));
                    // lstrUIResponse = SendRequest(lstrUIRequest, Convert.ToString(pobjUtilityInquiryRequest.Type), strURL);
                    lstrUIResponse = SendToQueue(lstrUIRequest, pobjUtilityInquiryRequest.ReferenceNum, Convert.ToString(pobjUtilityInquiryRequest.Type));
                    //lstrUIResponse = System.IO.File.ReadAllText("F:\\Infiworks\\Documents\\Response.txt");
                    LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityInquiryRequest.Type) + " | Response = " + lstrUIResponse + " | Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum, Convert.ToString(pobjUtilityInquiryRequest.Type));
                    lobjUtilityDetails.Status = UtilityStatus.Failed;
                    if (lstrUIResponse != null && lstrUIResponse != "")
                    {
                        lobjUtilityDetails.ResponseDate = DateTime.Now;
                        lobjUtilityDetails.ResponseString = lstrUIResponse;

                        DataSet lobjdsUIResponse = new DataSet();

                        lobjdsUIResponse.ReadXml(new StringReader(lstrUIResponse));
                        if (lobjdsUIResponse.Tables.Count > 0)
                        {

                            DataTable dtUIHeaderResponse = lobjdsUIResponse.Tables["HB_EAI_HEADER"];
                            if (dtUIHeaderResponse.Rows.Count > 0)
                            {
                                lobjUtilityHeader = new UtilityHeader();

                                lobjUtilityHeader.ReturnCode = Convert.ToString(dtUIHeaderResponse.Rows[0]["ReturnCode"]);



                                if (lobjUtilityHeader.ReturnCode == "0000")
                                {
                                    lobjUtilityInquiryResponse = new UtilityInquiryResponse();
                                    lobjUtilityInquiryResponse.UtilityHeader = lobjUtilityHeader;


                                    lobjUtilityHeader.MsgFormat = Convert.ToString(dtUIHeaderResponse.Rows[0]["MsgFormat"]);
                                    lobjUtilityHeader.MsgVersion = Convert.ToString(dtUIHeaderResponse.Rows[0]["MsgVersion"]);
                                    lobjUtilityHeader.RequestorId = Convert.ToString(dtUIHeaderResponse.Rows[0]["RequestorId"]);
                                    lobjUtilityHeader.RequestorChannelId = Convert.ToString(dtUIHeaderResponse.Rows[0]["RequestorChannelId"]);
                                    lobjUtilityHeader.RequestorUserId = Convert.ToString(dtUIHeaderResponse.Rows[0]["RequestorUserId"]);
                                    lobjUtilityHeader.RequestorLanguage = Convert.ToString(dtUIHeaderResponse.Rows[0]["RequestorLanguage"]);
                                    lobjUtilityHeader.RequestorSecurityInfo = Convert.ToString(dtUIHeaderResponse.Rows[0]["RequestorSecurityInfo"]);
                                    lobjUtilityHeader.EaiReference = Convert.ToString(dtUIHeaderResponse.Rows[0]["EaiReference"]);


                                    DataTable dtUIResponse = lobjdsUIResponse.Tables["UtilityInquiryReply"];
                                    if (dtUIResponse.Rows.Count > 0)
                                    {
                                        lobjUtilityInquiryResponse.ReturnCode = Convert.ToString(dtUIResponse.Rows[0]["ReturnCode"]);
                                        lobjUtilityDetails.ReturnCode = lobjUtilityInquiryResponse.ReturnCode;
                                        lobjUtilityInquiryResponse.ReturnText = Convert.ToString(dtUIResponse.Rows[0]["ReturnText"]);



                                        //lobjUtilityDetails.UpdatedBy = lobjMemberDetails.Email;

                                        if (lobjUtilityInquiryResponse.ReturnCode == "0000")
                                        {
                                            lobjUtilityInquiryResponse.ReferenceNumber = Convert.ToString(dtUIResponse.Rows[0]["ReferenceNum"]);
                                            lobjUtilityInquiryResponse.UtilityCompanyCode = Convert.ToString(dtUIResponse.Rows[0]["UtilityCompanyCode"]);
                                            lobjUtilityInquiryResponse.UtilityAccount = Convert.ToString(dtUIResponse.Rows[0]["UtilityAccount"]);
                                            lobjUtilityInquiryResponse.UtilityAccountType = Convert.ToString(dtUIResponse.Rows[0]["UtilityAccountType"]);
                                            lobjUtilityInquiryResponse.Amount = Convert.ToDouble(dtUIResponse.Rows[0]["Amount"]);
                                            lobjUtilityInquiryResponse.AmountMin = Convert.ToDouble(dtUIResponse.Rows[0]["AmountMin"]);
                                            lobjUtilityInquiryResponse.AmountMax = Convert.ToDouble(dtUIResponse.Rows[0]["AmountMax"]);
                                            lobjUtilityInquiryResponse.BillDate = Convert.ToString(dtUIResponse.Rows[0]["BillDate"]);
                                            lobjUtilityInquiryResponse.CurrentTimeStamp = Convert.ToString(dtUIResponse.Rows[0]["CurrentTimeStamp"]);

                                            lobjUtilityDetails.Status = UtilityStatus.Successful;
                                        }
                                    }
                                }
                                else
                                {
                                    lobjUtilityDetails.Status = UtilityStatus.Failed;
                                    lobjUtilityDetails.ReturnCode = lobjUtilityHeader.ReturnCode;
                                    LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityInquiryRequest.Type) + " Header Return Code Not equal to 0000 for Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum, Convert.ToString(pobjUtilityInquiryRequest.Type));
                                }
                            }
                        }
                        else
                        {
                            lobjUtilityDetails.Status = UtilityStatus.Failed;
                            lobjUtilityDetails.ResponseDate = DateTime.Now;

                            LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityInquiryRequest.Type) + " Response Null Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum, Convert.ToString(pobjUtilityInquiryRequest.Type));
                        }
                    }
                    else
                    {
                        lobjUtilityDetails.Status = UtilityStatus.Failed;
                        lobjUtilityDetails.ResponseDate = DateTime.Now;

                        LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityInquiryRequest.Type) + " Response Null Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum, Convert.ToString(pobjUtilityInquiryRequest.Type));
                    }

                    ////db ENTRIES
                    bool lblnStatu = lobjUtilityPaymentFacade.UpdateUtilityDetailsStatus(lobjUtilityDetails);
                    LoggingAdapter.WriteLog("Update Utility Details = " + lblnStatu + " | Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum, Convert.ToString(pobjUtilityInquiryRequest.Type));
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Utility Payment - Helper -  " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace + " | Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum, Convert.ToString(pobjUtilityInquiryRequest.Type));
            }

            LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityInquiryRequest.Type) + " response End for Reference Number = " + pobjUtilityInquiryRequest.ReferenceNum + " Date Time = " + DateTime.Now, Convert.ToString(pobjUtilityInquiryRequest.Type));
            return lobjUtilityInquiryResponse;
        }

        public UtilityPaymentResponse UtilityPayment(UtilityPaymentRequest pobjUtilityPaymentRequest)
        {
            LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityPaymentRequest.Type) + " Start Reference Number = " + pobjUtilityPaymentRequest.ReferenceNumber + " Date and Time = " + DateTime.Now, Convert.ToString(pobjUtilityPaymentRequest.Type));
            string lstrUPResponse = string.Empty;
            string lstrResponse = string.Empty;
            string lstrUPRequest = string.Empty;
            UtilityPaymentResponse lobjUtilityPaymentResponse = null;
            UtilityHeader lobjUtilityHeader = null;
            try
            {
                if (pobjUtilityPaymentRequest != null)
                {
                    LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityPaymentRequest.Type) + " Request JSON :- " + JSONSerialization.Serialize(pobjUtilityPaymentRequest) + " | Reference Number = " + pobjUtilityPaymentRequest.ReferenceNumber, Convert.ToString(pobjUtilityPaymentRequest.Type));

                    UtilityDetails lobjUtilityDetails = new UtilityDetails();
                    lobjUtilityDetails.ReferenceNumber = pobjUtilityPaymentRequest.ReferenceNumber;
                    lobjUtilityDetails.UtilityCompanyCode = pobjUtilityPaymentRequest.UtilityCompanyCode;
                    lobjUtilityDetails.UtilityAccount = pobjUtilityPaymentRequest.UtilityAccount;
                    lobjUtilityDetails.UtilityAccountType = pobjUtilityPaymentRequest.UtilityAccountType;
                    lobjUtilityDetails.CustomerId = pobjUtilityPaymentRequest.CustomerId;
                    lobjUtilityDetails.Amount = pobjUtilityPaymentRequest.AmountPaid;
                    lobjUtilityDetails.BankAccount = pobjUtilityPaymentRequest.BankAccount;
                    lobjUtilityDetails.TimeStamp = pobjUtilityPaymentRequest.TimeStamp;
                    lobjUtilityDetails.RequestedDate = DateTime.Now;
                    lobjUtilityDetails.Type = pobjUtilityPaymentRequest.Type;
                    lobjUtilityDetails.Status = UtilityStatus.Pending;
                   
                    lstrUPRequest = File.ReadAllText(ConfigurationSettings.AppSettings["UPPath"].ToString() + "UtilityPaymentRequest.txt");

                    lstrUPRequest = string.Format(lstrUPRequest, ConfigurationSettings.AppSettings["MsgFormatUP"].ToString(), ConfigurationSettings.AppSettings["MsgVersion"].ToString()
                                                   , ConfigurationSettings.AppSettings["RequestorId"].ToString(), ConfigurationSettings.AppSettings["RequestorChannelId"].ToString()
                                                   , ConfigurationSettings.AppSettings["RequestorUserId"].ToString(), ConfigurationSettings.AppSettings["RequestorLanguage"].ToString()
                                                  , ConfigurationSettings.AppSettings["RequestorSecurityInfo"].ToString(), ConfigurationSettings.AppSettings["EaiReference"].ToString()
                                                  , ConfigurationSettings.AppSettings["ReturnCode"].ToString()
                                                  , pobjUtilityPaymentRequest.ReferenceNumber, pobjUtilityPaymentRequest.UtilityCompanyCode
                                                  , pobjUtilityPaymentRequest.UtilityAccount, pobjUtilityPaymentRequest.UtilityAccountType
                                                  , pobjUtilityPaymentRequest.BankAccountCur
                                                  , pobjUtilityPaymentRequest.BankAccount, pobjUtilityPaymentRequest.BankAccountCur
                                                  , pobjUtilityPaymentRequest.AmountPaid, pobjUtilityPaymentRequest.TimeStamp);
                    lobjUtilityDetails.RequestString = lstrUPRequest;
                    ////db ENTRIES
                    lobjUtilityDetails.Id = lobjUtilityPaymentFacade.InsertUtilityDetails(lobjUtilityDetails);
                    LoggingAdapter.WriteLog("Utility Payment Insert Utility = " + lobjUtilityDetails.Id + " | Reference Number = " + pobjUtilityPaymentRequest.ReferenceNumber, Convert.ToString(pobjUtilityPaymentRequest.Type));
                    LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityPaymentRequest.Type) + " Request :- " + lstrUPRequest + " | Reference Number = " + pobjUtilityPaymentRequest.ReferenceNumber, Convert.ToString(pobjUtilityPaymentRequest.Type));
                    //lstrUPResponse = SendRequest(lstrUPRequest, Convert.ToString(pobjUtilityPaymentRequest.Type), strURL);
                    lstrUPResponse = SendToQueue(lstrUPRequest, pobjUtilityPaymentRequest.ReferenceNumber, Convert.ToString(pobjUtilityPaymentRequest.Type));
                    // lstrUPResponse = System.IO.File.ReadAllText("F:\\Infiworks\\Documents\\Response1.txt");
                    LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityPaymentRequest.Type) + " | Response  :- " + lstrUPResponse + " | Reference Number = " + pobjUtilityPaymentRequest.ReferenceNumber, Convert.ToString(pobjUtilityPaymentRequest.Type));
                    lobjUtilityDetails.Status = UtilityStatus.Failed;
                    if (lstrUPResponse != null && lstrUPResponse != "")
                    {
                        lobjUtilityDetails.ResponseString = lstrUPResponse;
                        DataSet lobjdsUPResponse = new DataSet();

                        lobjdsUPResponse.ReadXml(new StringReader(lstrUPResponse));
                        if (lobjdsUPResponse.Tables.Count > 0)
                        {
                            DataTable dtUPHeaderResponse = lobjdsUPResponse.Tables["HB_EAI_HEADER"];
                            if (dtUPHeaderResponse.Rows.Count > 0)
                            {

                                lobjUtilityHeader.ReturnCode = Convert.ToString(dtUPHeaderResponse.Rows[0]["ReturnCode"]);


                                if (lobjUtilityHeader.ReturnCode == "0000")
                                {
                                    lobjUtilityHeader = new UtilityHeader();
                                    lobjUtilityHeader.MsgFormat = Convert.ToString(dtUPHeaderResponse.Rows[0]["MsgFormat"]);
                                    lobjUtilityHeader.MsgVersion = Convert.ToString(dtUPHeaderResponse.Rows[0]["MsgVersion"]);
                                    lobjUtilityHeader.RequestorId = Convert.ToString(dtUPHeaderResponse.Rows[0]["RequestorId"]);
                                    lobjUtilityHeader.RequestorChannelId = Convert.ToString(dtUPHeaderResponse.Rows[0]["RequestorChannelId"]);
                                    lobjUtilityHeader.RequestorUserId = Convert.ToString(dtUPHeaderResponse.Rows[0]["RequestorUserId"]);
                                    lobjUtilityHeader.RequestorLanguage = Convert.ToString(dtUPHeaderResponse.Rows[0]["RequestorLanguage"]);
                                    lobjUtilityHeader.RequestorSecurityInfo = Convert.ToString(dtUPHeaderResponse.Rows[0]["RequestorSecurityInfo"]);
                                    lobjUtilityHeader.EaiReference = Convert.ToString(dtUPHeaderResponse.Rows[0]["EaiReference"]);

                                    lobjUtilityPaymentResponse = new UtilityPaymentResponse();
                                    lobjUtilityPaymentResponse.UtilityHeader = lobjUtilityHeader;

                                    DataTable dtUpResponse = lobjdsUPResponse.Tables["UtilityPaymentReply"];
                                    if (dtUpResponse.Rows.Count > 0)
                                    {
                                        lobjUtilityPaymentResponse.ReturnCode = Convert.ToString(dtUpResponse.Rows[0]["ReturnCode"]);
                                        lobjUtilityPaymentResponse.ReturnText = Convert.ToString(dtUpResponse.Rows[0]["ReturnText"]);
                                        lobjUtilityDetails.ResponseDate = DateTime.Now;


                                        if (lobjUtilityPaymentResponse.ReturnCode == "0000")
                                        {
                                            lobjUtilityPaymentResponse.ReferenceNumber = Convert.ToString(dtUpResponse.Rows[0]["ReferenceNum"]);
                                            lobjUtilityPaymentResponse.UtilityCompanyCode = Convert.ToString(dtUpResponse.Rows[0]["UtilityCompanyCode"]);
                                            lobjUtilityPaymentResponse.UtilityAccount = Convert.ToString(dtUpResponse.Rows[0]["UtilityAccount"]);
                                            lobjUtilityPaymentResponse.UtilityAccountType = Convert.ToString(dtUpResponse.Rows[0]["UtilityAccountType"]);
                                            lobjUtilityPaymentResponse.BankAccount = Convert.ToString(dtUpResponse.Rows[0]["BankAccount"]);
                                            lobjUtilityPaymentResponse.BankAccountCur = Convert.ToString(dtUpResponse.Rows[0]["BankAccountCur"]);
                                            lobjUtilityPaymentResponse.Amount = Convert.ToDouble(dtUpResponse.Rows[0]["Amount"]);
                                            lobjUtilityDetails.ExternalReference = pobjUtilityPaymentRequest.ExternalReference;
                                            lobjUtilityHeader.ReturnCode = lobjUtilityPaymentResponse.ReturnCode;
                                            lobjUtilityDetails.Status = UtilityStatus.Successful;
                                        }
                                    }
                                }
                                else
                                {

                                    lobjUtilityDetails.ResponseDate = DateTime.Now;
                                    lobjUtilityDetails.ReturnCode = lobjUtilityHeader.ReturnCode;
                                    LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityPaymentRequest.Type) + " Header Return Code Not equal to 0000 for Reference Number = " + pobjUtilityPaymentRequest.ReferenceNumber, Convert.ToString(pobjUtilityPaymentRequest.Type));
                                }
                            }
                        }
                    }
                    else
                    {
                        lobjUtilityDetails.Status = UtilityStatus.Failed;
                        lobjUtilityDetails.ResponseDate = DateTime.Now;

                        LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityPaymentRequest.Type) + " Response Null Reference Number = " + pobjUtilityPaymentRequest.ReferenceNumber, Convert.ToString(pobjUtilityPaymentRequest.Type));
                    }


                    ////db ENTRIES
                    bool lblnStatu = lobjUtilityPaymentFacade.UpdateUtilityDetailsStatus(lobjUtilityDetails);
                    ////db ENTRIES
                    lblnStatu = lobjUtilityPaymentFacade.UpdateUtilityDetails(lobjUtilityDetails);
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("Utility Payment - Helper -  " + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace + " | Reference Number = " + pobjUtilityPaymentRequest.ReferenceNumber, Convert.ToString(pobjUtilityPaymentRequest.Type));
            }
            LoggingAdapter.WriteLog(Convert.ToString(pobjUtilityPaymentRequest.Type) + " Response End for Reference Number = " + pobjUtilityPaymentRequest.ReferenceNumber + " Date Time = " + DateTime.Now, Convert.ToString(pobjUtilityPaymentRequest.Type));
            return lobjUtilityPaymentResponse;
        }

        private string SendRequest(string pstrRequest, string pstrMethodName, string strURL)
        {
            string lstrResponse = string.Empty;
            string url = string.Empty;
            try
            {
                url = strURL;
                var req = WebRequest.Create(url);
                req.Timeout = 300000;
                req.Method = "POST";
                req.ContentType = "text/xml;charset=UTF-8";
                req.PreAuthenticate = true;



                using (var writer = new StreamWriter(req.GetRequestStream()))
                {
                    writer.WriteLine(pstrRequest);
                    writer.Close();
                }

                using (var rsp = req.GetResponse())
                {
                    req.GetRequestStream().Close();
                    if (rsp != null)
                    {
                        using (var lobjStreamReader =
                                    new StreamReader(rsp.GetResponseStream()))
                        {
                            lstrResponse = lobjStreamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog(pstrMethodName + "  - Send Request - " + url + Environment.NewLine
                    + ex.Message + Environment.NewLine + "Stack Trace-" + ex.StackTrace);
            }
            return lstrResponse;
        }

        private string SendToQueue(string pstrRequest, string pstrReferenceNumber, string pstrLoggingType)
        {
            string lstrResponse = string.Empty;
            LoggingAdapter.WriteLog("Queue Request start" + pstrRequest + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));
            MQQueueManager mqQMgr;
            MQQueue requestQueue;
            MQQueue responseQueue;
            MQRCText mqrcText = new MQRCText();
            MQMessage requestMessage;
            MQMessage responseMessage;

            string strRequestQueueName = ConfigurationSettings.AppSettings["RequestQueue"];
            string strResponseQueueName = ConfigurationSettings.AppSettings["ResponseQueue"];
            string strQueueManagerName = ConfigurationSettings.AppSettings["QueueManager"];
            string strChannelName = ConfigurationSettings.AppSettings["QueueChannel"];
            string strConnectionName = ConfigurationSettings.AppSettings["QueueHostIPAndPort"];

            string strRequestText = pstrRequest;

            LoggingAdapter.WriteLog("Queue Request => Queue Manager -" + strQueueManagerName + " | Channel - " + strChannelName + " | connectionName - " + strConnectionName + " | Request Queue - " + strRequestQueueName + " | Resposne Queue - " + strResponseQueueName + " | Request String - " + strRequestText + " | Reference Number = " + pstrReferenceNumber,Convert.ToString(pstrLoggingType));


            //Step 1. Create Queue Manager Object. This will also CONNECT the Queue Manager
            try
            {
                mqQMgr = new MQQueueManager(strQueueManagerName, strChannelName, strConnectionName);
                LoggingAdapter.WriteLog("Successful Create Queue Manager Object" + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));
                requestQueue = mqQMgr.AccessQueue(strRequestQueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
                LoggingAdapter.WriteLog("Successful Open Request Queue for writing our request" + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));

                requestMessage = new MQMessage();
                //requestMessage.WriteString(strRequestText);
                string lstrFinalRequestString = System.Text.Encoding.Default.GetString(Encoding.UTF8.GetBytes(strRequestText));
                LoggingAdapter.WriteLog("Request Message UTF 8 string = " + lstrFinalRequestString + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));
                requestMessage.WriteBytes(lstrFinalRequestString);

                requestMessage.Format = MQC.MQFMT_STRING;
                requestMessage.MessageType = MQC.MQMT_REQUEST;
                requestMessage.Report = MQC.MQRO_COPY_MSG_ID_TO_CORREL_ID;
                requestMessage.ReplyToQueueName = strResponseQueueName;
                requestMessage.ReplyToQueueManagerName = strQueueManagerName;

                requestQueue.Put(requestMessage);
                if (requestQueue.OpenStatus)
                    requestQueue.Close();
                LoggingAdapter.WriteLog("Request put into queue successfully." + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));

                LoggingAdapter.WriteLog("Open Response Queue for reading the response." + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));
                responseQueue = mqQMgr.AccessQueue(strResponseQueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
                LoggingAdapter.WriteLog("Successful Open Response Queue for reading the response." + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));

                LoggingAdapter.WriteLog("Request Message start" + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));
                string abc = string.Empty;
                for (int i = 0; i < requestMessage.MessageId.Length; i++)
                {
                    abc += requestMessage.MessageId[i] + " |";

                }
                LoggingAdapter.WriteLog("Request Message Id = " + abc + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));

                responseMessage = new MQMessage();
                responseMessage.CorrelationId = requestMessage.MessageId;

                LoggingAdapter.WriteLog("Response Message start" + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));
                string abcd = string.Empty;
                for (int j = 0; j < responseMessage.MessageId.Length; j++)
                {
                    abcd += responseMessage.CorrelationId[j] + " |";

                }
                LoggingAdapter.WriteLog("Response Message Id = " + abcd + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));

                MQGetMessageOptions gmo = new MQGetMessageOptions();
                gmo.Options = MQC.MQGMO_WAIT;

                gmo.WaitInterval = Convert.ToInt32(ConfigurationSettings.AppSettings["WaitInSec"]);

                gmo.MatchOptions = MQC.MQMO_MATCH_CORREL_ID;


                responseQueue.Get(responseMessage, gmo);
                lstrResponse = responseMessage.ReadString(responseMessage.MessageLength);
                LoggingAdapter.WriteLog("UtilityPaymentHelper Resposne = " + lstrResponse + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));
            }
            catch (MQException mqe)
            {
                string strError = mqrcText.getMQRCText(mqe.Reason);
                LoggingAdapter.WriteLog("MQException  = " + mqe.Reason + " | " + strError + " | " + mqe.Message + " | " + mqe.StackTrace + " | Reference Number = " + pstrReferenceNumber, Convert.ToString(pstrLoggingType));

            }
            return lstrResponse;
        }


    }
}
