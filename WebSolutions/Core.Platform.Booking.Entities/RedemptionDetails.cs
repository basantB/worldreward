﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Platform.Booking.Entities
{
    [DataContract]
    [Serializable]
    public class RedemptionDetails
    {
        #region Private Variables

        string mstrCurrency = string.Empty;
        string mstrDisplayCurrency = string.Empty;
        int mintPoints = 0;
        string mstrTransactionReference = string.Empty;
        string mstrRelationReference = string.Empty;
        float mfltAmount = 0.0f;

        #endregion

        #region Properties

        /// <summary>
        /// Displays Currency
        /// </summary>
        [DataMember]
        public string Currency
        {
            get { return mstrCurrency; }
            set { mstrCurrency = value; }
        }

        /// <summary>
        /// Displays DisplayCurrency
        /// </summary>
        [DataMember]
        public string DisplayCurrency
        {
            get { return mstrDisplayCurrency; }
            set { mstrDisplayCurrency = value; }
        }

        /// <summary>
        /// Displays Points
        /// </summary>
        [DataMember]
        public int Points
        {
            get { return mintPoints; }
            set { mintPoints = value; }
        }

        /// <summary>
        /// Displays TransactionReference
        /// </summary>
        [DataMember]
        public string TransactionReference
        {
            get { return mstrTransactionReference; }
            set { mstrTransactionReference = value; }
        }

        /// <summary>
        /// Displays RelationReference
        /// </summary>
        [DataMember]
        public string RelationReference
        {
            get { return mstrRelationReference; }
            set { mstrRelationReference = value; }
        }

        /// <summary>
        /// Displays Amount
        /// </summary>
        [DataMember]
        public float Amount
        {
            get { return mfltAmount; }
            set { mfltAmount = value; }
        }

        #endregion

    }
}
