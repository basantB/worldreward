﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Framework.EnterpriseLibrary.CommunicationEngine.BookingInterface;
using CB.IBE.Platform.IBEClient;
using CB.IBE.Platform.Entities;
using Framework.EnterpriseLibrary.Adapters;

namespace Infi.OfflineEmailImplementor
{
    public class BookingDetails : AirBookingEmail
    {
        string strCommunicationTracing = "Communication Tracing";

        public string AirBookingReceipt(List<string> plstparameter)
        {

            string strStatus = string.Empty;
            IBECTClient lobjIBECTClient = new IBECTClient();

            try
            {
                ItineraryDetails lobjItineraryDetails = lobjIBECTClient.GetFlightReceiptWithPaymentBreakage(Convert.ToInt32(plstparameter[0]));
                strStatus = CreateReceipt(lobjItineraryDetails, plstparameter);//plstparameter[2] is Template Text 
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("AirBookingReceipt Exception :" + ex.InnerException.Message, strCommunicationTracing);
            }
            return strStatus;

        }

        public string CreateReceipt(ItineraryDetails plobjItineraryDetails, List<string> plstparameter)//TemplateCode changes to Template Text
        {
            try
            {
                string strReceipt = "";
                string strDepartute = "";
                string strArrival = "";
                string strPaxInfo = "";
                string strReturn = "";
                strReceipt = plstparameter[7];
                string DepartArrivalLoc = "";

                if (plobjItineraryDetails != null && plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments != null)
                {
                    DepartArrivalLoc = plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[0].DepartureAirField.City;
                }
                if (plobjItineraryDetails.ListOfFlightDetails.Count > 1)
                {
                    if (plobjItineraryDetails != null && plobjItineraryDetails.ListOfFlightDetails[1] != null && plobjItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments.Count > 0)
                    {
                        DepartArrivalLoc = DepartArrivalLoc + " To " + plobjItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments[0].DepartureAirField.City;
                    }
                }
                else
                {
                    DepartArrivalLoc = DepartArrivalLoc + " To " + plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments.Count - 1].ArrivalAirField.City;

                }
                if (plobjItineraryDetails != null && plobjItineraryDetails.ListOfFlightDetails[0] != null && plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments.Count > 1)
                {
                    if (plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[1] != null)
                        DepartArrivalLoc += " via " + plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[1].DepartureAirField.City;
                }
                if (plobjItineraryDetails.ListOfFlightDetails.Count > 1)
                {

                    if (plobjItineraryDetails != null && plobjItineraryDetails.ListOfFlightDetails[1] != null)
                    {
                        if (plobjItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments.Count > 0)
                        {
                            DepartArrivalLoc += "<br/><span>(Return)</span><br/>";
                        }
                    }
                }
                string strAirlinePNR = string.Empty;

                if (plobjItineraryDetails.IsMultipleCarrier)
                {
                    List<string> CarrierCodeList = new List<string>();
                    // For Departure
                    if (plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments != null)
                    {
                        for (int i = 0; i < plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments.Count; i++)
                        {
                            if (CarrierCodeList.Contains(plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[i].Carrier.CarrierCode))
                            {
                                strAirlinePNR += plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[i].Carrier.CarrierCode + " : " + plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[i].AirlinePNR + "<br/>";
                                CarrierCodeList.Add(plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[i].Carrier.CarrierCode);
                            }
                        }
                    }
                    // For Return
                    if (plobjItineraryDetails.ListOfFlightDetails.Count > 1)
                    {

                        if (plobjItineraryDetails.ListOfFlightDetails[1] != null)
                        {
                            for (int i = 0; i < plobjItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments.Count; i++)
                            {

                                if (CarrierCodeList.Contains(plobjItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments[i].Carrier.CarrierCode))
                                {
                                    strAirlinePNR += plobjItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments[i].Carrier.CarrierCode + " : " + plobjItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments[i].AirlinePNR + "<br/>";
                                    CarrierCodeList.Add(plobjItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments[i].Carrier.CarrierCode);
                                }
                            }
                        }
                    }

                }

                else
                {
                    if (plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments != null)
                    {
                        if (plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments != null && plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[0].AirlinePNR != null)
                            strAirlinePNR = plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[0].AirlinePNR + "</b>";
                    }
                }
                // Code for Header
                strReceipt = strReceipt.Replace("$DepartArrivalLoc$", DepartArrivalLoc);
                string strGDSPNR = plobjItineraryDetails.PaxPricingInfoList.PaxPricingInfo[0].BookingInfoList.BookingInfo[0].gdspnr;
                strReceipt = strReceipt.Replace("$GDSPNR$", strGDSPNR);

                strReceipt = strReceipt.Replace("$TransactionReferenceCode$", plobjItineraryDetails.ItineraryReference);

                string lstrPayment = string.Empty;
                string lstrPaymentDetails = "<tr><td width='2%'>&nbsp;</td><td align='left' valign='top' width='96%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: normal; text-transform: inherit; text-align: left; color: #231f20; padding: 4px 0 4px 0;'><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr> {0}</tr></table></td><td width='2%'>&nbsp;</td></tr>";

                for (int i = 0; i < plobjItineraryDetails.BookingPaymentDetails.BookingPaymentBreakageList.Count; i++)
                {
                    lstrPayment += string.Format(lstrPaymentDetails, plobjItineraryDetails.BookingPaymentDetails.BookingPaymentBreakageList[i].Currency + " : " + plobjItineraryDetails.BookingPaymentDetails.BookingPaymentBreakageList[i].Amount);
                }


                strReceipt = strReceipt.Replace("$PaymentDetails$", lstrPayment);
                string strClass = (plobjItineraryDetails.CabinType != null && Convert.ToString(plobjItineraryDetails.CabinType) != "") ? Convert.ToString(plobjItineraryDetails.CabinType) : "";
                strReceipt = strReceipt.Replace("$Class$", strClass);

                // Code For Travellor Info

                List<PassengerDetails> lobjListOfPassengerDetails = new List<PassengerDetails>();
                if (plobjItineraryDetails.TravelerInfo != null)
                {
                    string lstrPaxtype = "";
                    lobjListOfPassengerDetails = plobjItineraryDetails.TravelerInfo;
                    for (int k = 0; k < lobjListOfPassengerDetails.Count; k++)
                    {

                        if (Convert.ToString(lobjListOfPassengerDetails[k].PaxType) == "ADT")
                        {
                            lstrPaxtype = "Adult";
                        }
                        else if (Convert.ToString(lobjListOfPassengerDetails[k].PaxType) == "CHD")
                        {
                            lstrPaxtype = "Child";
                        }
                        else if (Convert.ToString(lobjListOfPassengerDetails[k].PaxType) == "INF")
                        {
                            lstrPaxtype = "Infant";
                        }
                        strPaxInfo += "<tr>";
                        strPaxInfo += "<td align='left' valign='top' width='19%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; text-align: left; color: #231f20; padding: 4px 0 4px 0;'> " + lstrPaxtype + " </td>";
                        strPaxInfo += "<td align='left' valign='top' width='40%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; text-align: left; color: #231f20; padding: 4px 0 4px 0;'>" + UppercaseFirst(lobjListOfPassengerDetails[k].LastName) + " " + UppercaseFirst(lobjListOfPassengerDetails[k].FirstName) + "</td>";
                        strPaxInfo += "<td align='left' valign='top' width='23%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; text-align: left; color: #231f20; padding: 4px 0 4px 0;'>" + lobjListOfPassengerDetails[k].TicketNo + " </td>";
                        strPaxInfo += "<td align='left' valign='top' width='10%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; text-align: left; color: #231f20; padding: 4px 0 4px 0;'>" + lobjListOfPassengerDetails[k].Gender + " </td>";
                        strPaxInfo += "<td align='left' valign='top' width='8%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; text-align:center; color: #231f20; padding: 4px 0 4px 0;'>" + Convert.ToString(lobjListOfPassengerDetails[k].Age) + " </td>";
                        strPaxInfo += "</tr>";
                    }
                    strReceipt = strReceipt.Replace("$TblPassengerInfo$", strPaxInfo);
                }

                // Code for Departure table
                List<FlightSegment> lobjFlightSegmentlst = new List<FlightSegment>();
                lobjFlightSegmentlst = plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments;
                for (int i = 0; i < lobjFlightSegmentlst.Count; i++)
                {
                    strDepartute += "<tr>";
                    strDepartute += "<td align='left' valign='top' width='12%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0' align='center'><img width='30' height='30'  src='$ImagePath$" + lobjFlightSegmentlst[i].Carrier.CarrierLogoPath + "'> <br/>" + lobjFlightSegmentlst[i].Carrier.CarrierName + "<br/>" + lobjFlightSegmentlst[i].Carrier.CarrierCode + lobjFlightSegmentlst[i].FlightNo + "</td>";
                    strDepartute += "<td align='left' valign='top' width='22%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'>" + lobjFlightSegmentlst[i].DepartureAirField.City + " (" + lobjFlightSegmentlst[i].DepartureAirField.IATACode + ")" + "<br/>" + lobjFlightSegmentlst[i].DepartureAirField.AirportName + ", " + lobjFlightSegmentlst[i].DepartureAirField.City + "</td>";
                    strDepartute += "<td align='left' valign='top' width='22%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'>" + lobjFlightSegmentlst[i].ArrivalAirField.City + " (" + lobjFlightSegmentlst[i].ArrivalAirField.IATACode + ")" + "<br/>" + lobjFlightSegmentlst[i].ArrivalAirField.AirportName + ", " + lobjFlightSegmentlst[i].ArrivalAirField.City + "</td>";
                    strDepartute += "<td align='left' valign='top' width='15%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'>" + lobjFlightSegmentlst[i].DepartureDate.ToString("dd/MM/yyyy") + "<br/>" + lobjFlightSegmentlst[i].DepartureDate.ToString("HH:mm") + "</td>";
                    strDepartute += "<td align='left' valign='top' width='15%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'>" + lobjFlightSegmentlst[i].ArrivalDate.ToString("dd/MM/yyyy") + "<br/>" + lobjFlightSegmentlst[i].ArrivalDate.ToString("HH:mm") + "</td>";
                    strDepartute += "<td align='left' valign='top' width='14%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'>" + lobjFlightSegmentlst[i].Carrier.EquipmentType + "</td>";
                    strDepartute += "</tr>";


                    //For AirLine PNR
                    strDepartute += "<tr><td colspan='6' height='5' bgcolor='#f0f0f0'>&nbsp;</td></tr>";
                    strDepartute += "<tr>";
                    strDepartute += "<td  style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'> PNR:&nbsp;" + lobjFlightSegmentlst[i].AirlinePNR + "</td>";
                    strDepartute += "<td  bgcolor='#f0f0f0'>&nbsp;</td>";
                    strDepartute += "<td  bgcolor='#f0f0f0'>&nbsp;</td>";
                    strDepartute += "<td  bgcolor='#f0f0f0'>&nbsp;</td>";
                    strDepartute += "<td  bgcolor='#f0f0f0'>&nbsp;</td>";
                    strDepartute += "<td  bgcolor='#f0f0f0'>&nbsp;</td>";
                    strDepartute += "</tr>";
                }

                strReceipt = strReceipt.Replace("$TblDeparture$", strDepartute);
                if (plobjItineraryDetails.IsMultipleCarrier)
                {
                    strReceipt = strReceipt.Replace("$ImgCarrierlogo$", "<img src='$ImagePath$Images/AirlinesLogo/MultipleCarrier.png'/>");
                }
                else
                {
                    strReceipt = strReceipt.Replace("$ImgCarrierlogo$", "<img src='$ImagePath$" + plobjItineraryDetails.ListOfFlightDetails[0].ListOfFlightSegments[0].Carrier.CarrierLogoPath + "'/>");
                }

                if (plobjItineraryDetails.ListOfFlightDetails.Count > 1)
                {


                    if (plobjItineraryDetails != null && plobjItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments != null)
                    {
                        // Code for Arrival Table
                        strReturn += "<td align='left' valign='top' width='100%' style='border:1px solid #d6d6d6; padding:4px 0px'><table cellpadding='0' cellspacing='0' width='100%' border='0'>";
                        strReturn += "<tr><td width='2%'>&nbsp;</td><td align='left' valign='top' width='96%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                        strReturn += "<tr><td align='left' valign='top' width='71%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: normal; text-transform: inherit;  text-align: left; color: #231f20; padding: 4px 0 4px 0;'><strong> Itinerary Details</strong> <span style='color: #231f20;'>(Return)</span></td>";
                        strReturn += "<td align='left' valign='top' width='7%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: normal; text-transform: capitalize; text-align: left; color: #231f20; padding: 4px 0 4px 0;'><strong>Class :</strong></td>";
                        strReturn += "<td align='left' valign='top' width='21%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: normal; text-transform: capitalize; text-align: left; color: #231f20; padding: 4px 0 4px 0;'>" + strClass + "</td>";
                        strReturn += "</tr></table></td><td width='2%'>&nbsp;</td></tr></table></td>";

                        strReceipt = strReceipt.Replace("$ReturnFlight$", strReturn);
                        lobjFlightSegmentlst = plobjItineraryDetails.ListOfFlightDetails[1].ListOfFlightSegments;

                        if (lobjFlightSegmentlst != null && lobjFlightSegmentlst.Count > 0)
                        {
                            // Arrival Header Row
                            strArrival += "<td align='left' valign='top' width='100%' style='border-left: 1px solid #0e76bc; border-right: 1px solid #0e76bc;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                            strArrival += "<tr style='background:#0e76bc'><td width='2%' >&nbsp;</td>";
                            strArrival += "<td align='left' valign='top' width='96%' ><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>";
                            strArrival += "<td align='left' valign='top' width='12%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: bold; text-transform: capitalize; text-align: left; color: #fff; padding: 4px 0 4px 0;'> Flight </td>";
                            strArrival += "<td align='left' valign='top' width='22%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: bold; text-transform: capitalize; text-align: left; color: #fff; padding: 4px 0 4px 0;'> Departure </td>";
                            strArrival += "<td align='left' valign='top' width='22%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: bold; text-transform: capitalize; text-align: left; color: #fff; padding: 4px 0 4px 0;'> Arrival </td>";
                            strArrival += "<td align='left' valign='top' width='15%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: bold; text-transform: capitalize; text-align: left; color: #fff; padding: 4px 0 4px 0;'> Departure Time </td>";
                            strArrival += "<td align='left' valign='top' width='15%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: bold; text-transform: capitalize; text-align: left; color: #fff; padding: 4px 0 4px 0;'> Arrival Time </td>";
                            strArrival += "<td align='left' valign='top' width='14%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: bold; text-transform: capitalize; text-align: right; color: #fff; padding: 4px 0 4px 0;'> Aircraft Type </td>";
                            strArrival += "</tr></table></td><td width='2%' >&nbsp;</td></tr></table></td></tr><tr>";
                            strArrival += "<td align='left' valign='top'  bgcolor='#f0f0f0' style='border:1px solid #d6d6d6;'><table cellpadding='0' cellspacing='0' width='100%' border='0'>";
                            strArrival += "<tr><td width='2%'>&nbsp;</td>";
                            strArrival += "<td align='left' valign='top' width='96%' style='font-family: Calibri; font-size: 14px; letter-spacing: normal; line-height: 18px; font-weight: normal; text-transform: capitalize; text-align: left; color: #231f20; padding: 4px 0 4px 0;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";

                            for (int j = 0; j < lobjFlightSegmentlst.Count; j++)
                            {
                                strArrival += "<tr>";
                                strArrival += "<td align='left' valign='top' width='12%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0' align='center'><img width='30' height='30'  src='$ImagePath$" + lobjFlightSegmentlst[j].Carrier.CarrierLogoPath + "'> <br/>" + lobjFlightSegmentlst[j].Carrier.CarrierName + "<br/>" + lobjFlightSegmentlst[j].Carrier.CarrierCode + lobjFlightSegmentlst[j].FlightNo + "</td>";
                                strArrival += "<td align='left' valign='top' width='22%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'>" + lobjFlightSegmentlst[j].DepartureAirField.City + " (" + lobjFlightSegmentlst[j].DepartureAirField.IATACode + ")" + "<br/>" + lobjFlightSegmentlst[j].DepartureAirField.AirportName + ", " + lobjFlightSegmentlst[j].DepartureAirField.City + "</td>";
                                strArrival += "<td align='left' valign='top' width='22%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'>" + lobjFlightSegmentlst[j].ArrivalAirField.City + " (" + lobjFlightSegmentlst[j].ArrivalAirField.IATACode + ")" + "<br/>" + lobjFlightSegmentlst[j].ArrivalAirField.AirportName + ", " + lobjFlightSegmentlst[j].ArrivalAirField.City + "</td>";
                                strArrival += "<td align='left' valign='top' width='15%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'>" + lobjFlightSegmentlst[j].DepartureDate.ToString("dd/MM/yyyy") + "<br/>" + lobjFlightSegmentlst[j].DepartureDate.ToString("HH:mm") + "</td>";
                                strArrival += "<td align='left' valign='top' width='15%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'>" + lobjFlightSegmentlst[j].ArrivalDate.ToString("dd/MM/yyyy") + "<br/>" + lobjFlightSegmentlst[j].ArrivalDate.ToString("HH:mm") + "</td>";
                                strArrival += "<td align='left' valign='top' width='14%' style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'>" + lobjFlightSegmentlst[j].Carrier.EquipmentType + "</td>";
                                strArrival += "</tr>";


                                //For AirLine PNR
                                strArrival += "<tr><td colspan='6' height='5' bgcolor='#f0f0f0'>&nbsp;</td></tr>";
                                strArrival += "<tr>";
                                strArrival += "<td  style='font-family: Calibri; font-size: 14px;letter-spacing: normal; line-height: 18px;text-align: left; color: #231f20; padding: 4px 0 4px 0;' bgcolor='#f0f0f0'> PNR:&nbsp;" + lobjFlightSegmentlst[j].AirlinePNR + "</td>";
                                strArrival += "<td  bgcolor='#f0f0f0'>&nbsp;</td>";
                                strArrival += "<td  bgcolor='#f0f0f0'>&nbsp;</td>";
                                strArrival += "<td  bgcolor='#f0f0f0'>&nbsp;</td>";
                                strArrival += "<td  bgcolor='#f0f0f0'>&nbsp;</td>";
                                strArrival += "<td  bgcolor='#f0f0f0'>&nbsp;</td>";
                                strArrival += "</tr>";

                            }

                            strArrival += "</table></td><td width='2%'>&nbsp;</td></tr></table></td>";


                            strReceipt = strReceipt.Replace("$TblArrival$", strArrival);
                            strReceipt = strReceipt.Replace("$TblArrivalHeading$", "");

                        }
                        else
                        {
                            strReceipt = strReceipt.Replace("$TblArrival$", "");
                            strReceipt = strReceipt.Replace("$TblArrivalHeading$", "");
                        }
                    }
                }
                else
                {
                    strReceipt = strReceipt.Replace("$TblArrival$", "");
                    strReceipt = strReceipt.Replace(" $TblArrivalHeading$", "");
                    strReceipt = strReceipt.Replace("$ReturnFlight$", "");
                }

                ////Code for Member Details 
                strReceipt = strReceipt.Replace("$RelationReference$", plstparameter[2]);
                strReceipt = strReceipt.Replace("$Name$", plstparameter[3]);
                strReceipt = strReceipt.Replace("$MobileNo$", plstparameter[4]);
                strReceipt = strReceipt.Replace("$Address1$", plstparameter[5]);
                strReceipt = strReceipt.Replace("$Address2$", string.Empty);
                strReceipt = strReceipt.Replace("$EmailId$", plstparameter[6]);

                strReceipt = strReceipt.Replace("$ImagePath$", GetAppSettingValue("ClientImageUrl"));
                return strReceipt;
            }
            catch (Exception ex)
            {
                LoggingAdapter.WriteLog("CreateReceipt Exception" + ex.InnerException.Message, strCommunicationTracing);
                return "Error";
            }
        }
        static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public string GetAppSettingValue(string key)
        {

            string arr = System.Configuration.ConfigurationSettings.AppSettings[key].ToString(); // //appSettings.GetValues(key);//System.Configuration.ConfigurationSettings.AppSettings[key].ToString(); // 
            LoggingAdapter.WriteLog("GetAppSettingValue BookingDetails key " + key + " Value " + arr, "Communication Tracing");
            return arr;


        }
    }
}
