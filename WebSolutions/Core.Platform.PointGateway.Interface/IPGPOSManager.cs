﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Framework.EnterpriseLibrary.Common.WCF.Faults;
using Core.Platform.Common.Entities;

namespace Core.Platform.PointGateway.Interface
{
    [ServiceContract]
    public interface IPGPOSManager
    {
        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response RedeemPointsPOS(float pfltTxnAmount, string pstrTxnCurrency, float pfltSourceAmount, string pstrSourceCurrency, int pintPoints, string pstrMemberId, string pstrPOSPIN, string pstrNarration, string pstrMerchantId, string pstrTerminalId, string pstrTransactionId, string pstrMerchantCity, string pstrMerchantCountry, string pstrMCC, int pintPaymentType, string pstrProgramCurrency, string pstrMerchantUserId, string pstrMerchantUserName, string pstrMerchantUserPassword);
    }
}
