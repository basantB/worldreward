﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Core.Platform.Common.Entities;
using Framework.EnterpriseLibrary.Common.WCF.Faults;

namespace Core.Platform.PointGateway.Interface
{
    [ServiceContract]
    public interface IPGManager
    {
        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response CheckPointsAvailability(string pstrRelationReference, int pintRelationType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);

        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response BlockPoints(float pfltSourceAmt, float pfltTxnAmount, int pintPoints, string pstrTxnMerchantName, string pstrSourceCurrency, string pstrTxnCurrency, string pstrRelationReference, string pstrWebPassword, string pstrNarration, int pintPaymentType, int pintRedemptionType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);

        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response RedeemPoint(string pstrExternalReference, string pstrRelationReference, string pstrMerchantName, int pintRedemptionType, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);

        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response RollBackTransaction(string pstrExternalReference, string pstrRelationReference, string pstrMerchantName, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);


        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response PGRedeemTransaction(float pfltSourceAmount, float pfltTxnAmount, int pintPoints, string pstrTxnMerchantName, string pstrSourceCurrency, string pstrTxnCurrency, string pstrRelationReference, string pstrWebPassword, string pstrNarration, int pintPaymentType, int pintRedemptionType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);

        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response PGRollBackTransaction(string pstrExternalReference, string pstrRelationReference, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);


        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response PGCheckPointsAvailabilityNonMember(string pstrNationalId, int pintRelationType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);

        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response PGRedeemTransactionNonMember(float pfltSourceAmount, float pfltTxnAmount, int pintPoints, string pstrTxnMerchantName, string pstrSourceCurrency, string pstrTxnCurrency, string pstrNationalId, string pstrWebPassword, string pstrNarration, int pintPaymentType, int pintRedemptionType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);

        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response PGRollBackTransactionNonMember(string pstrExternalReference, string pstrNationalId, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);

        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response GetValidateAuthenticationToken(string pstrMemberId, string pstrAuthenticationToken, int pintAuthenticationType, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);


        [OperationContract]
        [FaultContract(typeof(ExceptionFault))]
        [FaultContract(typeof(ApplicationFault))]
        Response PGRedeemAuthTokenTransaction(float pfltSourceAmount, float pfltTxnAmount, int pintPoints, string pstrTxnMerchantName, string pstrSourceCurrency, string pstrTxnCurrency, string pstrRelationReference, string pstrAuthenticationToken, int pintAuthenticationType, string pstrNarration, int pintPaymentType, int pintRedemptionType, string pstrProgramCurrency, string pstrMerchantId, string pstrMerchantUserName, string pstrMerchantPassword);

    }
}
